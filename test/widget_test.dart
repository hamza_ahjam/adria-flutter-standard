// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/Beneficiaire/AddBeneficiaire.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:LBA/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  // Test TextFormField
  testWidgets('Entrer nom du benef', (WidgetTester tester) async {
    final nomBenef = find.byKey(ValueKey("nomBenef"));

    await tester.pumpWidget(
        MaterialApp(home: MultiBlocProvider(
            providers: [
              BlocProvider(
                  create: (context) =>
                      BeneficiaireBloc(BeneficiaireStateBloc(
                        requestState: StateStatus.NONE,
                        requestStateAdd: StateStatus.NONE,
                        requestStateDelete: StateStatus.NONE,
                        requestStateAbon: StateStatus.NONE,
                      ))),
            ],
            child: AddBeneficiaireWidget())));
    await tester.enterText(nomBenef, "test");
    await tester.pump();

    expect(find.text("test"), findsOneWidget);
  });
}
