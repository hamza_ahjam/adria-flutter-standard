# Adria Flutter Standard

The goal of this project is to create a Flutter version of Adria's standard, which will be parametrable and flexible to fill the needs of any new clients.


## Supported clients apps

So far this project supports two clients apps :

- LBA
- NSIA
