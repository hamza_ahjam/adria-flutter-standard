
import 'package:LBA/agences/agencies_list_page.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:LBA/pages/bancassurance/produits_bancassurance.page.dart';
import 'package:LBA/pages/carnet_des_effets/carnet_des_effets.page.dart';
import 'package:LBA/pages/cartes/carte.page.dart';
import 'package:LBA/pages/cheques/liste-cheque.page.dart';
import 'package:LBA/pages/credits/credits.page.dart';
import 'package:LBA/pages/credits/demandeCredit/demande_credit_habitat_form1.dart';
import 'package:LBA/pages/credits/simulateur-credit.page.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/dat/dat.page.dart';
import 'package:LBA/pages/effets/effets.page.dart';
import 'package:LBA/pages/factures/accueil_facture_page.dart';
import 'package:LBA/pages/faq/faq.page.dart';
import 'package:LBA/pages/messaging/messaging.page.dart';
import 'package:LBA/pages/mouvement/mouvement.compte.dart';
import 'package:LBA/pages/produit/produit.page.dart';
import 'package:LBA/pages/remise_cheque/remise_cheque.page.dart';
import 'package:LBA/pages/rib/rib.page.dart';
import 'package:LBA/pages/titres_comptes/titre_compte.page.dart';
import 'package:LBA/pages/transferts/transfert_menu.dart';
import 'package:LBA/pages/virements/historique-virement.page.dart';
import 'package:LBA/pages/virements/liste-virement.page.dart';
import 'package:LBA/pages/virements/virement_navigation.widget.dart';
import 'package:LBA/widgets/changePassword.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'assistance/assistance.page.dart';
//BuildContext context;

const URL =
    "http://197.230.233.107:9080/auth/realms/standard-adria/protocol/openid-connect/token";

const client_id = "client-front";
const codeBanque = "00225";
const typeProfil = "CL";
const baseUrl = "http://172.18.1.80:9080/";
const langue = "fr";
const codePays = "MA";
const deviseBanque = "504";
const url_cam = "197.230.233.107:9080";

// wallet
const url_wallet = "edpagent.int.staging.bnpparibas.ma";
const url_wallet_dp = "www.dabapay-recette.bmcebank.co.ma";

const codeBanque_sf = "00013";
const codeBanque_dp = "00000";

const KPrimaryColor = Colors.purple;
const KSecandaryColor = Color(0xFF6A6A6A);
const KprimaryFont = "TitilliumWeb";
const KprimaryWeight = FontWeight.bold;
const KprimarySize = 20.0;

const mouvementsTextColor = Color.fromRGBO(112, 102, 113, 1);

const pcOrange = Color.fromRGBO(253, 92, 0, 1);
const scSilver = Color.fromRGBO(106, 106, 106, 1);

const principaleColor1 = Color.fromRGBO(204, 214, 33, 1);
const principaleColor5 = Color.fromRGBO(89, 43, 95, 1);
const principaleColor6 = Color.fromRGBO(213, 7, 88, 1);
const principaleColor8 = Color.fromRGBO(91, 102, 112, 1);

const secondaryColor2 = Color.fromRGBO(46, 170, 83, 1);
const secondaryColor3 = Color.fromRGBO(38, 180, 176, 1);
const secondaryColor4 = Color.fromRGBO(6, 132, 194, 1);
const secondaryColor7 = Color.fromRGBO(242, 131, 34, 1);

const appStoreID = "284882215";
const playStoreID = "com.facebook.katana";
const appVersion = 2.0;

final phoneNumberRegex = new RegExp("(0[67]([0-9]{8}))|((0{2}|\\+)212[67]([0-9]{8}))");
// int maxInactivityTimeout = 20;

int maxTimeOut = 25;

const String banque_id = "NSIA";

bool isActive = false;

bool modeDemo = true;

// final routes = {
//     '/login': (context) => LoginPage(),
// };

// var timerExpired = false;
// startTimer(context) {
//   maxInactivityTimeout = 15;
//   print("start timer");
//   Timer.periodic(Duration(seconds: 1), (timer) {
//     --maxInactivityTimeout;
//     if (maxInactivityTimeout == 0) {
//       print("expired");
//       errorAlert(context, "App Expired");
//       Future.delayed(Duration(seconds: 1), () {
//         // Navigator.pop(context);
//         // Navigator.of(context).pushAndRemoveUntil(
//         //     MaterialPageRoute(builder: (context) => LoginPage()),
//         //     (Route<dynamic> route) => false);
//       });
//       maxInactivityTimeout = 30;
//     }
//     print(maxInactivityTimeout);
//   });
// }

agnecInfos(BuildContext context) {
  return [
    {
      "titre": AppLocalizations.of(context).perte_carte,
      "numero": "05 22 00 00 00",
      "icon": true
    },
    {
      "titre": "ASSISTANCE AU SÉNÉGALAIS ET ESPACE SCHENGEN",
      "numero": "05 22 00 00 00",
      "icon": true
    },
    {
      "titre": "ASSISTANCE AUX SÉNÉGALAIS RÉSIDENTS À L’ETRANGER",
      "numero": "05 22 00 00 00",
      "icon": false
    }
  ];
}

onlineDrawer(BuildContext context) {
  return [
    {
      "categoryName": AppLocalizations.of(context).category_espace,
      "roles": [],
      "items": [
        {
          "title": AppLocalizations.of(context).changement_mp,
          "route": (context) => ChangePassword(),
          "image": "chmdp"
        },
        {
          "title": AppLocalizations.of(context).comptes,
          "route": (context) => DashboardComptePage(),
          "image": "Comptes"
        },
        {
          "title": AppLocalizations.of(context).cartes,
          "route": (context) => CartePage(),
          "image": "Cartes"
        },
        {
          "title": AppLocalizations.of(context).mes_virements,
          "route": (context) => HistoriqueVirementPage(
                isComingFromMenu: true,
              ),
          "image": "virement"
        },
        {
          "title": AppLocalizations.of(context).mes_mouvements,
          "route": (context) => MouvementComptePage(),
          "image": "money"
        },
        // {"title": "Produits", "route": "/", "image": "Produits"},
        {
          "title": AppLocalizations.of(context).mes_credits,
          "route": (context) => CreditsPage(),
          "image": "Credit"
        },
        {
          "title": AppLocalizations.of(context).sim_credit,
          "route": (context) => SimulateurCreditPage(),
          "image": "simulateur"
        },
        {
          "title": AppLocalizations.of(context).rel_client,
          "route": (context) => MessagingPage(),
          "image": "messagerie"
        },
        {
          "title": AppLocalizations.of(context).prods,
          "route": (context) => ProduitPage(),
          "image": "produits"
        },
        {
          "title": AppLocalizations.of(context).titres_comptes,
          "route": (context) => TitreComptePage(),
          "image": "produits"
        },
      ]
    },
    {
      "categoryName": AppLocalizations.of(context).category_op,
      "roles": [],
      "items": [
        // {
        //   "title": AppLocalizations.of(context).wallet,
        //   "route": (context) => WalletPage(),
        //   "image": "produits"
        // },
        {
          "title": AppLocalizations.of(context).banc_assurance,
          "route": (context) => ProduitsBanceassurancePage(),
          "image": "produits"
        },
        {
          "title": AppLocalizations.of(context).dat,
          "route": (context) => DatPage(),
          "image": "dat"
        },
        {
          "title": AppLocalizations.of(context).facts,
          "route": (context) => AccueilFacturePage(),
          "image": "Factures"
        },
        {
          "title": AppLocalizations.of(context).benefs,
          "route": (context) => ListeVirementPage(
                isComingFromMenu: true,
              ),
          "image": "mbenef"
        },
        {
          "title": AppLocalizations.of(context).virements,
          "route": (context) => VirementNavigationPage(),
          "image": "virement"
        },
        {
          "title": AppLocalizations.of(context).cheques,
          "route": (context) => ListChequePage(),
          "image": "ChequesLCN"
        },
        {
          "title": AppLocalizations.of(context).remise_cheques,
          "route": (context) => RemiseChequePage(),
          "image": "ChequesLCN"
        },
        {
          "title": AppLocalizations.of(context).carnets_effets,
          "route": (context) => CarnetDesEffetsPage(),
          "image": "ChequesLCN"
        },
        {
          "title": AppLocalizations.of(context).effets,
          "route": (context) => EffetsPage(),
          "image": "ChequesLCN"
        },
        {
          "title": AppLocalizations.of(context).rib_titre,
          "route": (context) => RibPage(),
          "image": "rib"
        },
        // {"title": "Transferts", "route": "/", "image": "Transferts"},
        {
          "title": AppLocalizations.of(context).transferts,
          "route": (context) => TransfertMenuPage(),
          "image": "virement"
        },

        {
          "title": AppLocalizations.of(context).dem_credit,
          "route": (context) => DemandeCreditHabitatForm1Page(),
          "image": "virement"
        },
        {
          "title": AppLocalizations.of(context).dem_credit_hab,
          "route": (context) => DemandeCreditHabitatForm1Page(),
          "image": "Credit"
        },
        // {"title": "Titres", "route": "/", "image": "titre"},
        // {"title": "Dépot à terme", "route": "/", "image": "depot"},
        // {
        //   "title": "Relevés bancaires",
        //   "route": (context) => RelevePage(context: context),
        //   "image": "releve"
        // },
        // {"title": "Bancassurance", "route": "/", "image": "assurence"},
        // {"title": "Bon de caisse", "route": "/", "image": "bon"},
      ]
    },
    {
      "categoryName": AppLocalizations.of(context).category_contact,
      "roles": [],
      "items": [
        // {
        //   "title": "Cours devises",
        //   "route": (context) => DevisesPage(),
        //   "image": "Comptes"
        // },
        {
          "title": AppLocalizations.of(context).agences,
          "route": (context) => ListAgenciesPage(),
          "image": "Agences"
        },
        {
          "title": AppLocalizations.of(context).assistance,
          "route": (context) => AssistancePage(),
          "image": "Assistance"
        },
        {
          "title": AppLocalizations.of(context).infos,
          "route": (context) => FAQPage(),
          "image": "Informations"
        },
      ]
    },
  ];
}

void moveToDashboard(BuildContext context, bool isComingFromMenu) {
  isComingFromMenu
      ? Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => DashboardComptePage()))
      : Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => VirementNavigationPage()));
}

void moveToLogin(BuildContext context) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
}
