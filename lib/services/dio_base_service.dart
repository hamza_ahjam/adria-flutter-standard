import 'package:alice/alice.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';

import '../constants.dart';
import '../main.dart';

class DioBaseService {
  var dio = Dio(BaseOptions(baseUrl: "https://$url_wallet_dp"));
  var cookieJar = CookieJar();

  Alice alice = Alice(
    navigatorKey: MyApp.navigatorKey,
    showNotification: true,
    showInspectorOnShake: true,
    darkTheme: false,
    maxCallsCount: 1000,
  );
}