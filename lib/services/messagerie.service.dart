
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';

import '../constants.dart';

class MessagerieService{

  Future<dynamic> saveMessageService(
      tokenState,
      identifiantInterne,
      corps,
      destinataires,
      sujet,
      piecesJointes
      ) async {
    var qParams = {
      'type': 'multipart/form-data',
      'codeLangue': 'fr',
      'codePays': 'MA',
      'corps' : "$corps",
      'destinataires': "$destinataires",
      'status': 'false',
      'sujet': "$sujet",
      'tousClients': 'false'
    };
    Map<String, dynamic> params = {'fichierjointes': '$piecesJointes',};

    var url = Uri.http(
        url_cam, "/app/digital-marketing/message/saveMessage", qParams);

    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("save vir service ***");

      if (res.body.isEmpty)
        return print(" save message return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save msg service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveMessageServiceMP(
      tokenState,
      identifiantInterne,
      corps,
      destinataires,
      sujet,
      piecesJointes
      ) async {
    var qParams = {
      'type': 'multipart/form-data',
      'codeLangue': 'fr',
      'codePays': 'MA',
      'corps' : "$corps",
      'destinataires': "$destinataires",
      'status': 'false',
      'sujet': "$sujet",
      'tousClients': 'false'
    };

    var url = Uri.http(
        url_cam, "/app/digital-marketing/message/saveMessage", qParams);

    var request = http.MultipartRequest('POST', url);

    var user = tokenState.getUserDetails();


    interceptRequest(request, tokenState, user);

    for(var i= 0; i < piecesJointes.length ; i++){
      request.files.add(await http.MultipartFile.fromPath(
          'fichierjointes',
          piecesJointes[i].path
      ));
    }

    var response = await request.send().timeout(
        Duration(seconds: maxTimeOut * 2),
        onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });
    return response.statusCode;
  }

  void interceptRequest(http.MultipartRequest request, tokenState, user) {
    request.headers['Accept'] = 'application/json; charset=UTF-8';
    request.headers['Authorization'] = 'Bearer ${tokenState.accessToken}';

    var map = new Map<String, String>();
    map["abonne"] = "${user["abonne"]}";
    map["userAgence"] = "${user["agence"]}";
    map["IntituleClient"] = "${user["intitule"]}";
    map["userAgence"] = "${user["agence"]}";
    map["userCodeBanque"] = codeBanque;
    map["codeBanqueContrat"] = codeBanque;
    map["userCodeLangue"] = langue;
    map["userCodePays"] = codePays;
    map["userContratId"] = "${user["contratId"]}";
    map["defaultLocale"] = "${user["defaultLocale"]}";
    map["dateExpirationProfileMetier"] =
    "${user["dateExpirationProfileMetier"]}";
    map["dateExpirationProfileSegnature"] =
    "${user["dateExpirationProfileMetier"]}";
    map["userDeviseBanque"] = deviseBanque;
    map["userEmail"] = "${user["email"]}";
    map["userFuseauHoraire"] = "${user["fuseauHoraire"]}";
    map["userId"] = "${user["id"]}";
    map["userIdentifiantContrat"] = "${user["identifiantContrat"]}";
    map["userIdentifiantRC"] = "${user["identifiantRC"]}";
    map["userQualite"] = "${user["qualite"]}";
    map["userTypeProfile"] = typeProfil;
    map["userUsername"] = "${user["username"]}";

    request.fields.addAll(map);
  }
}