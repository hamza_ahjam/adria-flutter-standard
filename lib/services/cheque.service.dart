import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:LBA/models/hsitoriqueCheque.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';

class ChequeService {
  CompteService get compteService => GetIt.I<CompteService>();

  Future<HistoriqueCheque> getHistoriqueCheque(
      tokenState, identifiantInterne, page) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    if (page == null) page = 1;
    var qParams = {
      "ncompteRecherche": "$identifiantInterne",
      "max": "20",
      "page": "$page"
    };
    var url = Uri.http(url_cam, "/app/compte/DAT/listObject", qParams);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get list cheques service service **");
      // data = json.decode(res.body);

      // data = listHistoriqueCheques;

      return HistoriqueCheque.fromJson(data);
    } catch (e) {
      print("compte service get list cheques err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveChequeServie(context, tokenState, numChequiers, crossed,
      endorsable, numeroCompte, typeChequier) async {
    // CompteState compteState = Provider.of<CompteState>(context, listen: false);

    var qParams = {
      "nombreChequiers": "$numChequiers",
      "crossed": "$crossed",
      "endorsable": "$endorsable",
      "numeroCompte": "$numeroCompte",
      "intituleCompte": "${compteService.currentCompte.intitule}",
      "agenceCompte": "${compteService.currentCompte.agence}",
      "deviseCompte": deviseBanque,
      "radical": "${compteService.currentCompte.radical}",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "codeProduitCompte": "2222",
      "typeChequier": "$typeChequier"
    };
    var url =
        Uri.http(url_cam, "/app/moyen-paiement/demandeChequier/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("save cheque service ***");

      if (res.body.isEmpty)
        return print(" save cheque return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save cheque service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeChequeService(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {"taskId": "$taskId", "jpassword": "$psd", "id": "$id"};

    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeChequier/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe demande chequier return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe demande chequier service err *** $e");
      throw e;
    }
  }
}
