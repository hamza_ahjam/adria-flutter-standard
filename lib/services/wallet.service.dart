import 'dart:async';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/models/loginDpRS.model.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:intl/intl.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'dio_base_service.dart';

class WalletService extends DioBaseService{

  String token;

  Future<dynamic> saveWalletPaiementAchat(montant, telephone,
      transactionIndicator, intituleBeneficiaire) async {
    var dateVir = DateFormat('dd-MM-yyyy').format(DateTime.now());
    print(dateVir);
    var params = {
      'beanDateExecution': "$dateVir",
      'agenceCompte': '78016',
      'codeProduitCompte': '2222',
      'intituleBeneficiaire': intituleBeneficiaire == null
          ? 'Commerçant, $telephone'
          : '$intituleBeneficiaire',
      'miseADisposition': 'false',
      'montant': '$montant',
      'numeroCompte': '011780000016200000011368',
      'qrCode': '',
      'telephone': '$telephone',
      'transactionIndicator': '$transactionIndicator',
      '_csrf': '$token',
      '_alifAgent': 'Mobile',
      'ajax': 'true',
      'canal_': 'Mobile',
      'codeBanque': '$codeBanque_dp',
      'codeLangue': langue,
      'codePays': '$codePays',
      'format': 'json',
      'lang': langue,
      'radical': '0669872784123669872784',
      'version_application': '4.08',
      'version_os': '5.0'
    };

    var headers = Map<String, String>();

    headers['X-CSRF-TOKEN'] = '$token';
    headers['Accept'] = 'application/json';
    headers['content-type'] = 'application/x-www-form-urlencoded';

    String encodedBody =
        params.keys.map((key) => "$key=${params[key]}").join("&");

    // print(url);

    try {
      dio.options.queryParameters.clear();

      var res = await dio
          .post("/WalletClient/transfertP2P/save",
              data: encodedBody, options: Options(headers: headers))
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      print('+++++++++++++++++ ${res.statusCode}');

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.data.isEmpty)
        return print(
            " save wallet paiement achat return is Empty *** ${res.statusCode}");

      print("data *** ${res.data}");
      return res.data;
    } catch (e) {
      print("wallet paiement achat service err *** $e");
      throw e;
    }
  }

  Future<dynamic> otpWalletPaiementAchat(id, taskId,
        demandRef) async {
      var params = {
        'demandId': '$id',
        'demandRef': '$demandRef',
        'typeOperation': 'TP2P',
        "taskId": "$taskId",
        "_alifAgent": "Mobile",
        "id": "$id",
        "codeBanque": codeBanque,
        "codeBanqueContrat": codeBanque,
        '_csrf': '$token',
        'ajax': 'true',
        'canal_': 'Mobile',
        'codeLangue': langue,
        'codePays': codePays,
        'format': 'json',
        'lang': langue,
        'radical': '5200007362855669872784',
        'segment': 'PART',
        'version_application': '4.08',
        'version_os': '5.0'
      };

      var headers = Map<String, String>();

      headers['X-CSRF-TOKEN'] = '$token';
      headers['Accept'] = 'application/json';
      headers['content-type'] = 'application/x-www-form-urlencoded';

      String encodedBody = params.keys.map((key) => "$key=${params[key]}").join(
          "&");

      try {
        dio.options.queryParameters.clear();

        var res = await dio
            .post("/WalletClient/transfertP2P/sendOtp",
            data: encodedBody,
            options: Options(headers: headers))
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        print('+++++++++++++++++ ${res.statusCode}');

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");

        if (res.data.isEmpty)
          return print(
              " send otp wallet paiement achat return is Empty *** ${res
                  .statusCode}");

        print("data *** ${res.data}");
        return res.data;
      } catch (e) {
        print("send otp paiement achat service err *** $e");
        throw e;
      }
    }

    Future<dynamic> signWalletPaiementAchat(id, taskId, psd) async {
      var params = {
        'id': '$id',
        "taskId": "$taskId",
        "jpassword": "$psd",
        "_alifAgent": "Mobile",
        "version": "0",
        "codeBanque": codeBanque_dp,
        '_csrf': '$token',
        'ajax': 'true',
        'canal_': 'Mobile',
        'codeLangue': langue,
        'codePays': codePays,
        'format': 'json',
        'lang': langue,
        'radical': '5200007362855669872784',
        'segment': 'PART',
        'version_application': '4.08',
        'version_os': '5.0'
      };
      var headers = Map<String, String>();

      headers['X-CSRF-TOKEN'] = '$token';
      headers['Accept'] = 'application/json';
      headers['content-type'] = 'application/x-www-form-urlencoded';

      String encodedBody = params.keys.map((key) => "$key=${params[key]}").join(
          "&");

      try {
        dio.options.queryParameters.clear();

        var res = await dio
            .post("/WalletClient/transfertP2P/signer",
            data: encodedBody,
            options: Options(headers: headers))
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        print('+++++++++++++++++ ${res.statusCode}');

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");

        if (res.data.isEmpty)
          return print(
              " send otp wallet paiement achat return is Empty *** ${res
                  .statusCode}");

        print("data *** ${res.data}");
        return res.data;
      } catch (e) {
        print("send otp paiement achat service err *** $e");
        throw e;
      }
    }

    Future<dynamic> scanQr(qr, scanType) async {
      var qParams = {
        'qrCode': '$qr',
        'scanType': '$scanType'
      };

      var headers = Map<String, String>();

      headers['X-CSRF-TOKEN'] = '$token';

      var data;

      try {
        dio.options.queryParameters = qParams;

        var res = await dio
            .get("/WalletClient/api/qrCode/scanQrCode")
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");
        if (res.data.isEmpty)
          return print("qr code service return is Empty ***");
        data = res.data;
        // print("data signe *** $data");
        return data['qrResult'];
      } catch (e) {
        print("qr code service service err *** $e");
        throw e;
      }
    }

  Future<dynamic> entreEnRelationOffline() async {

    var qParams = {
      'hash':
      'ac64a95e29010f2bb94230af205e4a09845836c86fb8fe4ce7d1bb094b6a7eb25cd941cd233213dab6a8219a402cf52bbe4542807823947d4ecd4b689644b86e'
    };

      var data;

      dio.interceptors.add(CookieManager(cookieJar));
      dio.interceptors.add(alice.getDioInterceptor());
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 90));
      dio.options.queryParameters = qParams;

      try {
        print("entrer en relation service");

        var res = await dio
            .get("/WalletClient/client/entreEnRelationOffline")
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");
        if (res.data.isEmpty)
          return print("enter en relation service return is Empty ***");
        data = res.data;

        print("entrer en relation token: $data['csrfToken']");
        token = data['csrfToken'];

        return data;
      } catch (e) {
        print("enter en relation service service err *** $e");
        throw e;
      }
    }

    Future<LoginDpRS> loginDP(String id, String psd) async {
      var params = {
        "j_username": "$id",
        "j_password": "$psd",
        "_csrf": '$token',
        '_alifAgent': 'Mobile',
        'ajax': 'true',
        'canal_': 'Mobile',
        'codeBanque': '$codeBanque_dp',
        'codeLangue': '$langue',
        'codePays': '$codePays',
        'format': 'json',
        'lang': '$langue',
        'radical': '5200007362855669872784',
        'segment': 'PART',
        'version_application': '4.08',
        'version_os': '5.0'
      };

      // var url = Uri.https(
      //     url_wallet_dp, "/WalletClient/login/mobile");

      var headers = Map<String, String>();

      // Map<String, dynamic> data;

      headers['X-CSRF-TOKEN'] = '$token';
      headers['Accept'] = 'application/json';
      headers['content-type'] = 'application/x-www-form-urlencoded';

      String encodedBody = params.keys.map((key) => "$key=${params[key]}").join("&");

      try {
        dio.options.queryParameters.clear();

        var res = await dio
            .post("/WalletClient/login/mobile",
            data: encodedBody,
            options: Options(headers: headers))
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        print('------------------------------ ${res.statusCode}');

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");

        return LoginDpRS.fromJson(res.data);
      } catch (e) {
        print(e);
      }
    }

    Future<dynamic> decryptPhone(cryptedPhone) async {
      var qParams = {
        'strToDecrypt': '$cryptedPhone'
      };

      dio.options.queryParameters = qParams;

      try {
        var res = await dio
            .get("/WalletClient/security/decrypt")
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });

        if (!(res.statusCode >= 200 && res.statusCode < 300))
          throw HttpException("Http Exception ${res.statusCode}");
        if (res.data.isEmpty)
          return print("decryptPhone service return is Empty ***");

        return res.data;
      } catch (e) {
        print("decryptPhone service service err *** $e");
        throw e;
      }
    }

    Future<dynamic> getBeneficiaryName(telephone) async {
      if (!modeDemo) {
        var qParams = {
          'telephone': '$telephone',
          '_csrf': '$token',
          '_alifAgent': 'Mobile',
          'ajax': 'true',
          'canal_': 'Mobile',
          'codeBanque': '$codeBanque_dp',
          'codeLangue': langue,
          'codePays': '$codePays',
          'format': 'json',
          'lang': langue,
          'radical': '5200007362855669872784',
          'segment': 'PART',
          'version_application': '4.08',
          'version_os': '5.0'
        };

        dio.options.queryParameters = qParams;

        // print(url);
        var data;


        var headers = Map<String, String>();

        headers['X-CSRF-TOKEN'] = '$token';

        print("token: $token");

        try {
          var res = await dio
              .get("/WalletClient/transfertP2P/getBeneficiaryName",
              options: Options(headers: headers))
              .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
            throw TimeoutException(
                'connection has timed out, Please try again!');
          });

/*        final res = await httpClient
            .get(url, headers: headers)
            .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
          throw TimeoutException('connection has timed out, Please try again!');
        });*/

          if (!(res.statusCode >= 200 && res.statusCode < 300))
            throw HttpException("Http Exception ${res.statusCode}");

          print("wallet get beneficiary name service ***");

          if (res.data.isEmpty)
            return print(" wallet get beneficiary name return is Empty *** ${res
                .statusCode}");
          data = res.data;
          print("result: code: ${res.statusCode} \ndata: $data");
          // print("data *** $data");
          return data;
        } catch (e) {
          print("wallet get beneficiary name service err *** $e");
          throw e;
        }
      } else {
        await Future.delayed(Duration(seconds: 1));
        print("mock");
        var data = {'name': 'Bénéficiaire confrère ', 'VERSION': '4.08'};
        print("mock returned $data");
        return data;
      }
    }

    Future<dynamic> getQrCodeData({amount, motif}) async {
      if (!modeDemo) {
        var qParams = {
          'amount': '$amount',
          'isStatic': false,
          'reason': motif != null ? '$motif' : '',
          'typeOperation': 'W'
        };

        dio.options = BaseOptions(
            baseUrl: "https://$url_wallet_dp", queryParameters: qParams);

        // print(url);
        var data;


        var headers = Map<String, String>();

        headers['X-CSRF-TOKEN'] = '$token';

        print("token: $token");

        try {
          var res = await dio
              .get("/WalletClient/api/qrCode/generateMobile",
              options: Options(headers: headers))
              .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
            throw TimeoutException(
                'connection has timed out, Please try again!');
          });

          if (!(res.statusCode >= 200 && res.statusCode < 300))
            throw HttpException("Http Exception ${res.statusCode}");

          print("wallet get QR code data service ***");

          print("amount: $amount, motif: $motif");

          if (res.data.isEmpty)
            return print(" wallet get QR code data return is Empty *** ${res
                .statusCode}");
          data = res.data;
          print("result: code: ${res.statusCode} \ndata: $data");
          // print("data *** $data");
          return data;
        } catch (e) {
          print("wallet get QR code data service err *** $e");
          throw e;
        }
      } else {
        await Future.delayed(Duration(seconds: 1));
        print("mock");
        var data = {
          "qrCode": "000201010212269100325bb66a92d69c0ea742dd4f754590fa0a02011050100624ydcfGi1jazxwRQQUAV/ROQ==0713+2126######8453035045405100.062080804test8046003237b3a355b830b3bf0974d23608a6f16201010040106304E7EF",
          "success": true
        };
        print("mock returned $data");
        return data;
      }
    }

  Future<dynamic> abondonnerWalletPaiementAchat(id, taskId) async {
    var params = {
      'id': '$id',
      "taskId": "$taskId",
      "_alifAgent": "Mobile",
      "version": "0",
      "codeBanque": codeBanque_dp,
      '_csrf': '$token',
      'ajax': 'true',
      'canal_': 'Mobile',
      'codeLangue': langue,
      'codePays': codePays,
      'format': 'json',
      'lang': langue,
      'radical': '5200007362855669872784',
      'segment': 'PART',
      'version_application': '4.08',
      'version_os': '5.0'
    };
    var headers = Map<String, String>();

    headers['X-CSRF-TOKEN'] = '$token';
    headers['Accept'] = 'application/json';
    headers['content-type'] = 'application/x-www-form-urlencoded';

    String encodedBody = params.keys.map((key) => "$key=${params[key]}").join("&");

    try {
      dio.options.queryParameters.clear();

      var res = await dio
          .post("/WalletClient/transfertP2P/abandonnerPhaseSignature",
          data: encodedBody,
          options: Options(headers: headers))
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      print('+++++++++++++++++ ${res.statusCode}');

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.data.isEmpty)
        return print(
            " abondonner wallet paiement return is Empty *** ${res
                .statusCode}");

      print("data *** ${res.data}");
      return res.data;
    } catch (e) {
      print("abondonner paiement service err *** $e");
      throw e;
    }
  }
}
