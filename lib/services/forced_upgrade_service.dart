import 'dart:async';
import 'dart:convert';

import 'package:LBA/models/forced_upgrade_model.dart';
import 'package:http/http.dart' as http;

class ForcedUpgradeService {
  final String dabaPay = "www.dabapay-recette.bmcebank.co.ma";

  Future<ForcedUpgradeResponse> getAppVersion() async {
    var url =
        Uri.http(dabaPay, "/WalletClient/login/mobile/getForceUpgradeVersion");
    try {
      final res = await http.get(url);
      if (!(res.statusCode >= 200 && res.statusCode < 300)) {
        print("request failed status: ${res.statusCode}");
        return null;
      }
      print("getAppVersion service **");
      var data = res.body;
      print("response $data");
      return ForcedUpgradeResponse.fromJson(jsonDecode(data));
    } catch (e) {
      print("request error, $e");
      return null;
    }
  }
}
