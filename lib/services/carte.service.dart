import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:LBA/models/carte.model.dart';
import 'package:LBA/models/mouvementCarte.model.dart';
import 'package:LBA/models/typeCarte.model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:intl/intl.dart';

class CarteService {
  ListCarteInstance currentCarte;

  bool prepaye = false;

  Future<Carte> getCarte(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam, "/app/compte/carte/carteOfAccount");

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    // httpClient.badCertificateCallback =
    //     (X509Certificate cert, String host, int port) => true;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("Carte service **");
      final data = json.decode(res.body);

      return Carte.fromJson(data);
    } catch (e) {
      print("carte service get carte error ** $e");
      throw e;
    }
  }

  Future<dynamic> getPlafonds(tokenState, numCarte, rib) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "rib": "$rib",
      "numeroCarte": "$numCarte",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "format": "json",
    };
    var url = Uri.http(url_cam, "/app/compte/carte/listCardCeilings", qParams);
    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    // httpClient.badCertificateCallback =
    //     (X509Certificate cert, String host, int port) => true;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("Carte service get plafond**");
      final data = json.decode(res.body);

      return data;
    } catch (e) {
      print("carte service get plafond error ** $e");
      throw e;
    }
  }

  Future<dynamic> getDemandeUpdatePlafonds(
      tokenState, numCarte, rib, codePlafond) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "codePlafond": "$codePlafond",
      "numeroCarte": "$numCarte",
      "rib": "$rib",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "format": "json",
    };
    var url = Uri.http(url_cam, "/app/compte/carte/modifierPlafond", qParams);
    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    // httpClient.badCertificateCallback =
    //     (X509Certificate cert, String host, int port) => true;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("Carte service modifier plafond**");
      final data = json.decode(res.body);

      return data;
    } catch (e) {
      print("carte service modifier plafond error ** $e");
      throw e;
    }
  }

  Future<MouvementCarte> getOperationCarte(
    tokenState,
    id,
    numComp,
    numCarte,
  ) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "numeroCompte": "$numComp",
      "numeroCarte": "$id",
      "_dc": "$numCarte",
      "format": "json",
    };
    var url = Uri.http(url_cam, "/app/compte/carte/listObject", qParams);
    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    // print("in service carte service  ***");
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("get Operation Carte service **");
      var data = json.decode(res.body);
      // if (data["Map"]["mouvementCarteList"].length == 0)
      // data = mouvementCartMock;
      // print(data);
      return MouvementCarte.fromJson(data);
    } catch (e) {
      print("carte service get operation carte error ** $e");
      throw e;
    }
  }

  Future<dynamic> getPlafondCarte(
    tokenState,
    cleRib,
    numCarte,
    numComp,
  ) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "cleRib": "$cleRib",
      "numeroCarte": "$numCarte",
      "numeroCompte": "$numComp",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "format": "json",
    };
    var url = Uri.http(url_cam, "/app/compte/carte/afficherPlafond", qParams);
    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    // print("in service carte service  ***");
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("get plafond Carte service **");
      var data = json.decode(res.body);
      // print(data);
      return data;
    } catch (e) {
      print("carte service get plafond carte error ** $e");
      throw e;
    }
  }

  Future<TypeCarte> getTypeCarte(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url =
        Uri.http(url_cam, "/app/administration/parametrageCarte/listObject");

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    // httpClient.badCertificateCallback =
    //     (X509Certificate cert, String host, int port) => true;
    print(url);
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("Carte service **");
      final data = json.decode(res.body);

      return TypeCarte.fromJson(data);
    } catch (e) {
      print("carte service get carte error ** $e");
      throw e;
    }
  }

  Future<dynamic> saveCommandeCarte(
      tokenState,
      identifiantInterne,
      email,
      intituleCompte,
      fullName,
      paysBeneficiaire,
      telephoneBeneficiaire,
      typeCatreLibelle,
      typeCarteCode,
      rad) async {
    var code = deviseBanque;

    var qParams = {
      'format': 'json',
      'agenceCompte': '100',
      'codeBanqueTransaction': '00225',
      'codeConvertibiliteCompte': '1',
      'codeProduitCompte': "2222",
      'deviseCompte': "$code",
      'deviseCompteCrediter': "$code",
      'deviseOperation': "$code",
      'email': "$email",
      'fullName': "$fullName",
      'fonction': "FONCTION_DIRECTEUR",
      'intituleCompte': '$intituleCompte',
      'paysBeneficiaire': '$paysBeneficiaire',
      'numeroCompte': "$identifiantInterne",
      'codeBanque': codeBanque,
      'codeBanqueContrat': codeBanque,
      'radical': '$rad',
      'standardPlafondAchatInternetDom': '100000',
      'standardPlafondAchatInternetEtranger': '100000',
      'standardPlafondAchatsDomestique': '100000',
      'standardPlafondAchatsInternationnal': '100000',
      'standardPlafondRetraitDomestiqueGABBanque': '100000',
      'standardPlafondRetraitDomestiqueGABConfrere': '100000',
      'standardPlafondRetraitGABInternationnal': '100000',
      'telephoneBeneficiaire': "$telephoneBeneficiaire",
      'typeCarteCode': "$typeCarteCode",
      'typeCatreLibelle': "$typeCatreLibelle",
      'typePieceIdentiteBeneficiaire': "CIN",
      'version': "0"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeSouscriptionCarte/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("commande carte service ***");

      if (res.body.isEmpty)
        return print(
            "save commande carte return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save commande vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeCommandeCarte(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(url_cam,
        "/app/moyen-paiement/demandeSouscriptionCarte/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty) return print(" signe virement return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe commande carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> getMotifOpposition(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam,
        "/app/administration/nomenclatureCodification/getMotifOpposition");
    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    // print("in service carte service  ***");
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("get Motif Opposition Carte service **");
      var data = json.decode(res.body);

      return data;
    } catch (e) {
      print("carte service get Motif Opposition carte error ** $e");
      throw e;
    }
  }

  Future<dynamic> oppositionCarte(
    tokenState,
    agenceCompte,
    codeConvertibiliteCompte,
    codeProduitCompte,
    fullName,
    motifOpposition,
    radical,
  ) async {
    var qParams = {
      "agenceCompte": agenceCompte,
      "cleRibCompte": currentCarte.rib,
      "codeConvertibiliteCompte": codeConvertibiliteCompte,
      "codeProduitCompte": codeProduitCompte,
      "dateVolOuPerte": DateFormat('dd-MM-yyyy').format(DateTime.now()),
      "deviseCompte": deviseBanque,
      "deviseOperation": deviseBanque,
      "fullName": fullName,
      "idCarte": currentCarte.id,
      "motifOpposition": motifOpposition,
      "numeroCarte": currentCarte.numero,
      "prepayeeCarte": currentCarte.prepayee.toString(),
      "radical": radical,
      "rib": currentCarte.rib,
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "format": "json",
      "codePays": codePays,
      "codeLangue": langue
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeOppositionCarte/save", qParams);

    // print(url);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    // print("in service carte service  ***");
    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("get opposition Carte Carte service **");
      var data = json.decode(res.body);

      return data;
    } catch (e) {
      print("carte service get opposition carte error ** $e");
      throw e;
    }
  }

  Future<dynamic> signeOppositionCarte(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeOppositionCarte/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe Opposition Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe Opposition Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> rechargeCarte(
    tokenState,
    agenceCompte,
    // beanDateDebut,
    // beanDateFin,
    codeProduitCompte,
    fullName,
    montantRecharge,
    numeroCompte,
    // nomBeneficiaire,
    radical,
  ) async {
    var qParams = {
      "agenceCompte": agenceCompte,
      "beanDateDebut": "23-05-2020",
      "beanDateFin": "23-05-2021",
      "codeBanqueTransaction": codeBanque,
      "codeProduitCompte": codeProduitCompte,
      "deviseCompte": deviseBanque,
      "deviseCompteCrediter": deviseBanque,
      "deviseOperation": deviseBanque,
      "fullName": fullName,
      "idCarte": currentCarte.id,
      "montantRecharge": montantRecharge,
      "motif": "motif",
      "nomBeneficiaire": "ADRIA",
      "numeroCarte": currentCarte.numero,
      "numeroCompte": numeroCompte,
      "numeroCompteCrediter": numeroCompte,
      "radical": radical,
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRechargeCarte/save", qParams);

    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe recharge Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe recharge Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeRechargeCarte(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRechargeCarte/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe RechargeCarte Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe RechargeCarte Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> recalculePin(
    tokenState,
    agenceCompte,
    intituleCompte,
    fullName,
    numeroCompte,
    radical,
  ) async {
    var qParams = {
      "agenceCompte": agenceCompte,
      "codeBanqueTransaction": codeBanque,
      "deviseCompte": deviseBanque,
      "deviseOperation": deviseBanque,
      "fullName": fullName,
      "intituleCompte": intituleCompte,
      "modeEnvoi": "SMS",
      "numeroCarte": currentCarte.numero,
      "numeroCarteDemandeRecalcul": currentCarte.numero,
      "numeroCompte": numeroCompte,
      "radical": radical,
      "rib": currentCarte.rib,
      "typeCarteDemandeRecalcul": currentCarte.typeCarte,
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRecalculPin/save", qParams);
    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe recalcule Pin Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe recalcule Pin Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeRecalculPin(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "id": "$id",
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRecalculPin/signer", qParams);
    // print(url);
    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe RecalculPin Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe RecalculPin Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> bloquerCarte(tokenState, userDetail, typeBloque) async {
    var qParams = {
      "cleRib": currentCarte.rib,
      "numeroCarte": currentCarte.numero,
      "numeroCompte": currentCarte.numeroCompte,
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "lang": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(url_cam, "/app/compte/carte/$typeBloque", qParams);
    print(url);

    var params = jsonEncode({"userDetails": userDetail});

    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty) return print(" bloquer Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("bloquer Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> cardStatus(tokenState, userDetail) async {
    var qParams = {
      "cleRib": currentCarte.rib,
      "numeroCarte": currentCarte.numero,
      "numeroCompte": currentCarte.numeroCompte,
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "lang": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(url_cam, "/app/compte/carte/cardStatus", qParams);
    // print(url);

    var params = jsonEncode({"userDetails": userDetail});

    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty) return print(" bloquer Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("bloquer Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> generercvv(
    tokenState,
  ) async {
    var qParams = {
      "numeroCarte": currentCarte.numero,
      "rib": currentCarte.rib,
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRecalculPin/save", qParams);
    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe recalcule Pin Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe recalcule Pin Carte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeGenerercvv(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "id": "$id",
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "ajax": "true"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeRecalculPin/signer", qParams);
    // print(url);
    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe Generer cvv Carte return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe Generer cvv Carte service err *** $e");
      throw e;
    }
  }
}
