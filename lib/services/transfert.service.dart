import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:intl/intl.dart';

import '../constants.dart';

class TransfertService{

  Future<dynamic> saveTransfert(
      tokenState,
      identifiantInterne,
      numeroPieceIdentite,
      montant,
      motif,
      gsm,
      dateTransfert,
      intituleCompte,
      numCompteCrediter,
      typePieceIdentite,
      typeTransfer,
      referenceClient,
      rad,
      nomCre) async {
    var code = deviseBanque;
    dateTransfert = DateFormat('dd-MM-yyyy').format(dateTransfert);

    var qParams = {
      'intituleCompte': '$intituleCompte',
      'debiter': '$identifiantInterne',
      'deviseCompte': "$code",
      'numeroCompte': "$identifiantInterne",
      'gsm': '$gsm',
      'montant': "$montant",
      'motif': "$motif",
      'deviseOperation': "$code",
      'beanDateExecution': "$dateTransfert",
      'codeBanque': "$codeBanque",
      'codeBanqueContrat': "$codeBanque",
      'codeProduitCompte': "2222",
      'ajax': "true",
      'codeLangue': '$langue',
      'format': 'json',
      'codePays': '$codePays',
      'radical': '$rad',
      'intituleCompteCrediter': '$nomCre',
      'numeroPieceIdentite': '$numeroPieceIdentite',
      'referenceClient': '$referenceClient',
      'typePieceIdentite': '$typePieceIdentite',
      'typeTransfer': '$typeTransfer',
      '_alifAgent': 'Mobile',
      'lang': '$langue'
    };
    var url = Uri.http(
        url_cam, "/app/transaction/demandeTransfertArgent/save", qParams);

    print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("save vir service ***");

      if (res.body.isEmpty)
        return print(" save virement return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeTransfert(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "ajax": "true",
      "codeLangue": "$langue",
      "codePays": '$codePays',
      "format": "json",
      "lang": "$langue"
    };
    var url = Uri.http(
        url_cam, "/app/transaction/demandeTransfertArgent/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient.post(url, body: params);
      if (res.body.isEmpty) return print(" signe virement return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe vir service err *** $e");
      throw e;
    }
  }


}