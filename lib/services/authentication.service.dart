import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/models/token.model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/extensions/string.extensions.dart';

class AuthenticationService {
  Future<dynamic> login(String id, String psd) async {
    var params = {
      "client_id": client_id,
      "grant_type": "password",
      "username": "$id",
      "password": "$psd",
      "codeBanque": codeBanque,
      "typeProfil": typeProfil,
      "baseUrl": baseUrl,
      "langue": langue,
    };

    try {
      final res = await http
          .post(URL.toUri(),
              headers: <String, String>{
                'Content-Type':
                    'application/x-www-form-urlencoded; charset=UTF-8',
              },
              body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.statusCode == 200) {
        final data = json.decode(res.body);
        // print("data auth ${res.statusCode}");
        return Token.fromJson(data);
      } else {
        print("error auth");
        throw "error auth";
      }
    } catch (e) {
      print("error auth *** $e");
      throw e;
    }
  }

  Future<dynamic> activateCompte(String rad) async {
    var qParams = {
      "radical": "$rad",
      "codeBanque": codeBanque,
      "codeLangue": langue,
    };
    var url = Uri.http(url_cam, "/app/contrats/auth/activateAbonne");

    try {
      final res = await http.post(url, body: qParams, headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      }).timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      final data = json.decode(res.body);
      // print("data auth ${res.statusCode}");
      return data;
    } catch (e) {
      print("error activated auth *** $e");
      throw e;
    }
  }

  Future<dynamic> activateCompteOtp(otp, token) async {
    var qParams = {
      "canal": "mobile",
      "fh": "0",
      "token": "$token",
      "token_sms_temp": "$otp",
      "_alifAgent": "Mobile",
      "ajax": "true",
      "codeBanque": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
    };
    var url = Uri.http(url_cam, "/app/contrats/auth/verifierOTP", qParams);
    // print(url);
    try {
      final res = await http.post(url).timeout(Duration(seconds: maxTimeOut),
          onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      final data = json.decode(res.body);
      // print("data auth ${res.statusCode}");
      // print(data);
      return data;
    } catch (e) {
      print("error activated auth *** $e");
      throw e;
    }
  }
  // refreshToken(token) async {
  //   print("refresh service");
  //   var params = {
  //     "client_id": client_id,
  //     "grant_type": "refresh_token",
  //     "codeBanque": codeBanque,
  //     "refresh_token": "${token['refresh_token']}",
  //     "token": "${token['access_token']}",
  //     "typeProfil": typeProfil,
  //     "baseUrl": baseUrl,
  //   };
  //   try {
  //     var res = await http.post(URL,
  //         headers: <String, String>{
  //           'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  //           // 'Authorization': 'Bearer $token',
  //         },
  //         body: params);
  //     final data = json.decode(res.body);
  //     return Token.fromJson(data);
  //   } catch (e) {
  //     print("error authentication refresh token **** $e");
  //   }
  // }
}
