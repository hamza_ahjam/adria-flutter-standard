import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:LBA/models/historique-virement.model.dart';
import 'package:LBA/models/periodicites.model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:intl/intl.dart';

class VirementService {
  Future<dynamic> getPeriodicites(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    var qParams = {
      "_alifAgent": "Mobile",
      "ajax": "true",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "lang": langue
    };
    var url = Uri.http(
        url_cam,
        "/app/administration/nomenclatureCodification/getPeriodicites",
        qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get periodicite service ***");

      if (res.body.isEmpty)
        return print(" periodicite return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return Periodicites.fromJson(data);
    } catch (e) {
      print("periodicite service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveVirementComptePermServie(
      tokenState,
      identifiantInterne,
      montant,
      motif,
      dateVir,
      dateFin,
      benifNom,
      intituleCompte,
      numCompteCrediter,
      // rad,
      nomCre,
      period,
      rad) async {
    var code = deviseBanque;
    dateVir = DateFormat('dd-MM-yyyy').format(dateVir);
    dateFin = DateFormat('dd-MM-yyyy').format(dateFin);

    var qParams = {
      'format': 'json',
      'beanDateExecution': "$dateVir",
      // 'codeProduitCompte': "2222",
      'deviseCompte': "$code",
      'deviseCompteCrediter': "$code",
      'deviseOperation': "$code",
      'dateFin': "$dateFin",
      // 'intituleBenificiaire': "$benifNom",
      'intituleCompte': '$intituleCompte',
      'intituleCompteCrediter': '$benifNom',
      'isBeneficiaire': '0',
      'isExternal': 'false',
      'isPermanent': '0',
      'montant': "$montant",
      'motif': "$motif",
      'numeroCompte': "$identifiantInterne",
      'numeroCompteCrediter': "$numCompteCrediter",
      'periodicite': '$period',
      'dateDebut': '$dateVir',
      'transfersType': 'VIREMENT_TYPE_P',
      // 'typeCompteCrediter': '$typeBenef',
      'virementUrgentCode': "86",
      'codeBanque': codeBanque,
      'codeBanqueContrat': codeBanque,
      // 'ajax': "true",
      'codeLangue': langue,
      'codePays': codePays,
      'radical': '$rad',
    };
    var url =
        Uri.http(url_cam, "/app/transaction/virPermanentCAC/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("virPermanentCAC vir service ***");

      if (res.body.isEmpty)
        return print(" save virement return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("virPermanentCAC vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveVirementBenefPermServie(
      tokenState,
      identifiantInterne,
      montant,
      motif,
      dateVir,
      dateFin,
      benifNom,
      intituleCompte,
      numCompteCrediter,
      // rad,
      nomCre,
      period,
      typeBenef,
      rad) async {
    var code = deviseBanque;
    dateVir = DateFormat('dd-MM-yyyy').format(dateVir);
    dateFin = DateFormat('dd-MM-yyyy').format(dateFin);

    var qParams = {
      'format': 'json',
      'beanDateExecution': "$dateVir",
      'codeProduitCompte': "2222",
      'deviseCompte': "$code",
      'deviseCompteCrediter': "$code",
      'deviseOperation': "$code",
      'dateFin': "$dateFin",
      'intituleBenificiaire': "$benifNom",
      'intituleCompte': '$intituleCompte',
      'intituleCompteCrediter': '$benifNom',
      'isBeneficiaire': '0',
      'isExternal': 'true',
      'isPermanent': '0',
      'montant': "$montant",
      'motif': "$motif",
      'numeroCompte': "$identifiantInterne",
      'numeroCompteCrediter': "$numCompteCrediter",
      'periodicite': '$period',
      'dateDebut': '$dateVir',
      'transfersType': 'VIREMENT_TYPE_P',
      'typeCompteCrediter': '$typeBenef',
      'virementUrgentCode': "86",
      'codeBanque': codeBanque,
      'codeBanqueContrat': codeBanque,
      // 'ajax': "true",
      'codeLangue': langue,
      'codePays': codePays,
      'radical': '$rad',
    };
    var url = Uri.http("197.230.233.107:9080",
        "/app/transaction/virPermanentMultiDevise/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("virPermanentMultiDevise vir service ***");

      if (res.body.isEmpty)
        return print(" save virement return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("virPermanentMultiDevise vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveVirementServie(
      tokenState,
      identifiantInterne,
      montant,
      motif,
      dateVir,
      dateFin,
      benifNom,
      intituleCompte,
      numCompteCrediter,
      rad,
      nomCre,
      typeCompteCredite) async {
    var code = deviseBanque;
    dateVir = DateFormat('dd-MM-yyyy').format(dateVir);
    dateFin = DateFormat('dd-MM-yyyy').format(dateFin);

    var qParams = {
      'deviseCompte': "$code",
      'numeroCompte': "$identifiantInterne",
      'montant': "$montant",
      'motif': "$motif",
      'deviseOperation': "$code",
      'typeCompteCrediter': '$typeCompteCredite',
      'beanDateExecution': "$dateVir",
      'dateFin': "$dateFin",
      'codeBanque': "$code",
      'intituleBenificiaire': "$benifNom",
      'codeProduitCompte': "2222",
      'ajax': "true",
      'deviseCompteCrediter': "$code",
      'virementUrgentCode': "86",
      'isExternal': "true",
      'codeLangue': 'fr',
      'format': 'json',
      'codePays': 'MA',
      'numeroCompteCrediter': "$numCompteCrediter",
      'radical': '$rad',
      'intituleCompteCrediter': '$nomCre'
    };
    var url = Uri.http(
        url_cam, "/app/transaction/virBeneficiaireMultiDevise/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("save vir service ***");

      if (res.body.isEmpty)
        return print(" save virement return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveVirementCompteServie(
      tokenState,
      identifiantInterne,
      montant,
      motif,
      dateVir,
      dateFin,
      benifNom,
      intituleCompte,
      numCompteCrediter,
      rad,
      nomCre) async {
    var code = deviseBanque;
    dateVir = DateFormat('dd-MM-yyyy').format(dateVir);
    dateFin = DateFormat('dd-MM-yyyy').format(dateFin);

    var qParams = {
      'deviseCompte': "$code",
      'numeroCompte': "$identifiantInterne",
      'montant': "$montant",
      'motif': "$motif",
      'deviseOperation': "$code",
      // 'typeCompteCrediter': 'TYPE_ALL',
      "transfersType": "VIREMENT_TYPE_N",
      'beanDateExecution': "$dateVir",
      'dateFin': "$dateFin",
      'codeBanque': "00225",
      'intituleBenificiaire': "$benifNom",
      'codeProduitCompte': "2222",
      'ajax': "true",
      'deviseCompteCrediter': "$code",
      'virementUrgentCode': "86",
      'isExternal': "true",
      'codeLangue': 'fr',
      'format': 'json',
      'codePays': 'MA',
      'numeroCompteCrediter': "$numCompteCrediter",
      'lang': 'fr',
      'radical': '$rad',
      'intituleCompteCrediter': '$nomCre'
    };
    var url = Uri.http(
        url_cam, "/app/transaction/virCompteCompteMultiDevise/save", qParams);

    // print(url);
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("save vir Compte service ***");

      if (res.body.isEmpty)
        return print(
            " save virement Compte return is Empty *** ${res.statusCode}");
      data = json.decode(res.body);
      // print("data *** $data");
      return data['Map'];
    } catch (e) {
      print("save vir Compte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeVirCompte(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam, "/app/transaction/virCompteCompteMultiDevise/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty) return print(" signe virement return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeVirBeneficiaire(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam, "/app/transaction/virBeneficiaireMultiDevise/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient.post(url, body: params);
      if (res.body.isEmpty) return print(" signe virement return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeVirBeneficiairePerm(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam, "/app/transaction/virPermanentMultiDevise/signer", qParams);

    // print(url);
    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.body.isEmpty)
        return print(" signe virement benef perm return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe vir benef perm service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeVirComptePerm(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url =
        Uri.http(url_cam, "/app/transaction/virPermanentCAC/signer", qParams);

    // print(url);
    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.body.isEmpty)
        return print(" signe virement benef perm return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe vir benef perm service err *** $e");
      throw e;
    }
  }

  Future<HistoriqueVirement> historiqueVirementBenif(
      tokenState, user, numCompte, page) async {
    // print(numCompte.runtimeType);
    // var url =
    //     "http://197.230.233.107:9080/app/transaction/virBeneficiaireMultiDevise/listObject?offset=0&ncompteRecherche=$numCompte&max=20&page=$page&numeroCompteRecherche=$numCompte&format=json&_alifAgent=Mobile";

    var qParams = {
      "offset": "0",
      "ncompteRecherche": "$numCompte",
      "max": "20",
      "page": "$page",
      "numeroCompteRecherche": "$numCompte",
      "format": "json",
      "_alifAgent": "Mobile"
    };

    var url = Uri.http(url_cam,
        "/app/transaction/virBeneficiaireMultiDevise/listObject", qParams);

    var params = jsonEncode({"userDetails": user});
    var data;
    // print(page);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      data = json.decode(res.body);
      // if (data['Map']['liste'].length == 0) {
      // data = historiqueMock;
      // }
      // print("data *** $data");
      return HistoriqueVirement.fromJson(data);
    } catch (e) {
      print("service load historique vir benif err **** $e");
      throw e;
    }

    // print(url);
  }

  Future<HistoriqueVirement> searchHistoriqueVirementBenif(tokenState, user,
      numCompte, page, String datemin, String datemax, String libelle) async {
    // print(numCompte.runtimeType);
    // var url =
    //     "http://197.230.233.107:9080/app/transaction/virBeneficiaireMultiDevise/listObject?offset=0&ncompteRecherche=$numCompte&max=20&page=$page&numeroCompteRecherche=$numCompte&format=json&_alifAgent=Mobile";

    var qParams = {
      "offset": "0",
      "ncompteRecherche": "$numCompte",
      "max": "20",
      "page": "$page",
      "numeroCompteRecherche": "$numCompte",
      "format": "json",
      "_alifAgent": "Mobile",
      if (datemin.isNotEmpty) "dateDebutRecherche": "$datemin",
      if (datemax.isNotEmpty) "dateFinRecherche": "$datemax",
      if (libelle.isNotEmpty) "motifRecherche": "$libelle"
    };

    var url = Uri.http(url_cam,
        "/app/transaction/virBeneficiaireMultiDevise/listObject", qParams);
    // print(url);
    var params = jsonEncode({"userDetails": user});
    var data;
    // print(page);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      data = json.decode(res.body);
      // if (data['Map']['liste'].length == 0) {
      // data = historiqueMock;
      // }
      // print("data *** $data");
      return HistoriqueVirement.fromJson(data);
    } catch (e) {
      print("service load search historique vir benif err **** $e");
      throw e;
    }

    // print(url);
  }

  Future<HistoriqueVirement> historiqueVirementMultiDevise(
      tokenState, user, numCompte, page) async {
    // print(numCompte.runtimeType);
    // print("compte a compte");
    // var url =
    //     "http://197.230.233.107:9080/app/transaction/virCompteCompteMultiDevise/listObject?offset=0&ncompteRecherche=$numCompte&max=20&page=$page&numeroCompteRecherche=$numCompte&format=json&_alifAgent=Mobile";

    var qParams = {
      "offset": "0",
      "ncompteRecherche": "$numCompte",
      "max": "20",
      "page": "$page",
      "numeroCompteRecherche": "$numCompte",
      "format": "json",
      "_alifAgent": "Mobile",
    };

    var url = Uri.http(url_cam,
        "/app/transaction/virCompteCompteMultiDevise/listObject", qParams);
    var params = jsonEncode({"userDetails": user});
    var data;
    // print(url);
    // print(page);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      // .then((res) {
      data = json.decode(res.body);
      // if (data['Map']['liste'].length == 0) {
      //   data = historiqueMock;
      // }
      // print("data *** $data");
      return HistoriqueVirement.fromJson(data);
    } catch (e) {
      print("service load historique compte err **** $e");
      throw e;
    }
    // }).catchError(
    //     (err) => print("service load historique compte err **** $err"));
  }

  Future<HistoriqueVirement> searchHistoriqueVirementMultiDevise(
      tokenState,
      user,
      numCompte,
      page,
      String datemin,
      String datemax,
      String libelle) async {
    // print(numCompte.runtimeType);
    // print("compte a compte");
    // var url =
    //     "http://197.230.233.107:9080/app/transaction/virCompteCompteMultiDevise/listObject?offset=0&ncompteRecherche=$numCompte&max=20&page=$page&numeroCompteRecherche=$numCompte&format=json&_alifAgent=Mobile";

    var qParams = {
      "offset": "0",
      "ncompteRecherche": "$numCompte",
      "max": "20",
      "page": "$page",
      "numeroCompteRecherche": "$numCompte",
      "format": "json",
      "_alifAgent": "Mobile",
      if (datemin.isNotEmpty) "dateDebutRecherche": "$datemin",
      if (datemax.isNotEmpty) "dateFinRecherche": "$datemax",
      if (libelle.isNotEmpty) "motifRecherche": "$libelle"
    };

    var url = Uri.http(url_cam,
        "/app/transaction/virCompteCompteMultiDevise/listObject", qParams);
    var params = jsonEncode({"userDetails": user});
    var data;
    // print(url);
    // print(page);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      // .then((res) {
      data = json.decode(res.body);
      // if (data['Map']['liste'].length == 0) {
      //   data = historiqueMock;
      // }
      // print("data *** $data");
      return HistoriqueVirement.fromJson(data);
    } catch (e) {
      print("service load search historique compte err **** $e");
      throw e;
    }
    // }).catchError(
    //     (err) => print("service load historique compte err **** $err"));
  }

  Future<dynamic> abandonnerVirBenefPhaseSignature(
      tokenState, userDetail, id, taskId) async {
    var qParams = {
      "format": "json",
      "id": "$id",
      "taskId": "$taskId",
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam,
        "/app/transaction/virBeneficiaireMultiDevise/abandonnerPhaseSignature",
        qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient.post(url, body: params);
      if (res.body.isEmpty)
        return print(
            " abandonner vir benef phase signature return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("abandonner vir bene phase signature vir service err *** $e");
      throw e;
    }
  }

  Future<dynamic> abandonnerVirComptePhaseSignature(
      tokenState, userDetail, id, taskId) async {
    var qParams = {
      "format": "json",
      "id": "$id",
      "taskId": "$taskId",
      "_alifAgent": "Mobile",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque
    };
    var url = Uri.http(
        url_cam,
        "/app/transaction/virCompteCompteMultiDevise/abandonnerPhaseSignature",
        qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient.post(url, body: params);
      if (res.body.isEmpty)
        return print(
            "abandonner vir compte phase signature return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("abandonner vir compte phase signature vir service err *** $e");
      throw e;
    }
  }
}
