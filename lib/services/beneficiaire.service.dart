import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:LBA/models/beneficiaire.model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';

class BeneficiaireService {
  var resDelet;

  Future<Beneficiaire> getBeneficaireList(userDetail, tokenState) async {
    var params = jsonEncode({"userDetails": userDetail});

    var qParams = {
      "typeOperation": "CR",
      "type": "VIREMENT",
    };

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var url = Uri.http(
        url_cam, "app/contrats/benificiaire/mesBeneficiairesList", qParams);
// /app/contrats/benificiaire/mesBeneficiaireList
    var data;
    // print(url);
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      data = json.decode(res.body);
      // print(data);
      return Beneficiaire.fromJson(data);
    } catch (e) {
      print("get beneficiare service err *** $e");
      throw e;
    }
  }

  Future<Beneficiaire> getBeneficaireListMAD(userDetail, tokenState) async {
    var params = jsonEncode({"userDetails": userDetail});

    var qParams = {
      "type": "ALL",
    };

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var url = Uri.http(
        url_cam, "app/contrats/benificiaire/mesBeneficiairesListMAD", qParams);
// /app/contrats/benificiaire/mesBeneficiaireList
    var data;
    // print(url);
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      data = json.decode(res.body);
      // print(data);
      return Beneficiaire.fromJson(data);
    } catch (e) {
      print("get beneficiare service err *** $e");
      throw e;
    }
  }

  Future<dynamic> abondonnerPhaseSignature(
      userDetail, tokenState, id, taskId) async {
    var params = jsonEncode({"userDetails": userDetail});

    var qParams = {
      "format": "json",
      "id": "$id",
      "taskId": "$taskId",
      "ajax": "true",
      "codeBanque": "00225",
      "codeBanqueContrat": "00225",
      "codeLangue": langue,
      "codePays": codePays
    };

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var url = Uri.http(
        url_cam,
        "app/contrats/ordreValidationBeneficiaire/abandonnerPhaseSignature",
        qParams);
// /app/contrats/benificiaire/mesBeneficiaireList
    var data;
    // print(url);
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      data = json.decode(res.body);
      // print(data);
      return data["Map"];
    } catch (e) {
      print("abandonner beneficiare phase signe service err *** $e");
      throw e;
    }
  }

  Future<dynamic> saveBenificiaire(tokenState, token, nom, rib) async {
    var qParams = {
      "type": "TYPE_ALL",
      "numeroCompte": "$rib",
      "deviseCompte": deviseBanque,
      "codeLangue": langue,
      "nature": "DOMESTIQUE_NatureBeneficiaire",
      "_alifAgent": "Mobile",
      "genre": "Genre_Beneficiaire_public",
      "codeBanque": codeBanque,
      "ajax": "true",
      "lang": langue,
      "codePays": codePays,
      "format": "json",
      "intitule": "$nom"
    };

    var url = Uri.http(
        url_cam, "/app/contrats/ordreValidationBeneficiaire/save", qParams);

    var data;

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      data = json.decode(res.body);

      return data['Map'];
    } catch (e) {
      print("service save benif err **** $e");
      throw e;
    }
  }

  Future<dynamic> signeBeneficiaire(
      tokenState, userDetail, id, taskId, psd) async {
    var qParams = {
      "taskId": "$taskId",
      "jpassword": "$psd",
      "_alifAgent": "Mobile",
      "id": "$id"
    };
    var url = Uri.http(
        url_cam, "/app/contrats/ordreValidationBeneficiaire/signer", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe beneficiaire return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      return data['Map'];
    } catch (e) {
      print("signe beneficiaire service err *** $e");
      throw e;
    }
  }

  Future<dynamic> signeBeneficiaireOTP(
      tokenState, userDetail, id, taskId) async {
    var qParams = {
      "taskId": "$taskId",
      "_alifAgent": "Mobile",
    };
    var url = Uri.http(url_cam,
        "/app/contrats/ordreValidationBeneficiaire/sendOTPSMS/$id", qParams);

    var params = jsonEncode({"userDetails": userDetail});
    var data;
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      if (res.body.isEmpty)
        return print(" signe otp beneficiaire return is Empty ***");
      data = json.decode(res.body);
      // print("data signe *** $data");
      // print("data otp *** ${data['Map']}");
      return data['Map'];
    } catch (e) {
      print("signe otp beneficiaire service err *** $e");
      throw e;
    }
  }

  Future<dynamic> daleteBenificiaire(
      tokenState, userDetail, rib, nature) async {
    // var url =
    //     "http://197.230.233.107:9080/app/contrats/ordreValidationBeneficiaire/delete/$rib?NATURE=DOMESTIQUE_NatureBeneficiaire&format=json";

    var params = jsonEncode({"userDetails": userDetail});

    var qParams = {
      "id": "$rib",
      "NATURE": "DOMESTIQUE_NatureBeneficiaire",
      "format": "json"
    };
    var url = Uri.http(url_cam,
        "/app/contrats/ordreValidationBeneficiaire/delete/$rib", qParams);
    var data;
    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      if (res.body.isEmpty)
        return print("return daleteBenificiaire is Empty***");
      data = json.decode(res.body);
      resDelet = data['Map'];
      return data['Map'];
    } catch (e) {
      print("delete benif service err ** $e");
      throw e;
    }
  }
}
