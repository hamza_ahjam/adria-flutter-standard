import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/constants.dart';
import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/iterceptors/httpSave.interceptor.dart';
import 'package:LBA/models/CreditHabitatForm.dart';
import 'package:LBA/models/bancAssurance_cotisation.model.dart';
import 'package:LBA/models/bancAssurance_situation.model.dart';
import 'package:LBA/models/banceassurance.model.dart';
import 'package:LBA/models/categorieCreancier.model.dart';
import 'package:LBA/models/client.compte.dart';
import 'package:LBA/models/compte.model.dart';
import 'package:LBA/models/contrat.model.dart';
import 'package:LBA/models/creanceDetails.model.dart';
import 'package:LBA/models/creanceForm.model.dart';
import 'package:LBA/models/creancier.model.dart';
import 'package:LBA/models/credit.model.dart';
import 'package:LBA/models/dat.model.dart';
import 'package:LBA/models/destinataire.model.dart';
import 'package:LBA/models/detail_message.model.dart';
import 'package:LBA/models/devise.model.dart';
import 'package:LBA/models/facture_history.dart';
import 'package:LBA/models/graphique.model.dart';
import 'package:LBA/models/historique-chequier.model.dart';
import 'package:LBA/models/historique.model.dart';
import 'package:LBA/models/list_assurance.model.dart';
import 'package:LBA/models/messagerie.model.dart';
import 'package:LBA/models/rechercheMouvement.model.dart';
import 'package:LBA/models/rib.model.dart';
import 'package:LBA/models/subjectsMessagerie.model.dart';
import 'package:LBA/models/titre_model.dart';
import 'package:LBA/models/token.model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';

// import 'package:alice/alice.dart';
// import 'package:alice/core/alice_http_client_extensions.dart';
// import 'package:alice/core/alice_http_extensions.dart';

class CompteService {
  // Alice _alice = Alice(
  //     showNotification: true,
  //     showInspectorOnShake: false,
  //     darkTheme: true,
  //     navigatorKey: navigatorKey);

  Token tokenState;
  ListCompte listCompte;
  ListCompte listCompteChequier;
  Compte currentCompte;
  Compte currentCompteChequier;

  Future<dynamic> forgetPsd(identifiant) async {
    var params = {
      "canal": "1",
      "codeUserForm": "$identifiant",
      "_alifAgent": "Mobile",
      "ajax": "true",
      "codeBanque": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "lang": langue,
    };

    var url = Uri.http(url_cam, "/app/contrats/auth/handleForgotPassword");

    try {
      final res = await http
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("fotget password compte service **");
      var data = json.decode(res.body);
      // data = compteMock;

      return data["Map"];
    } catch (e) {
      print(" forget password compte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> changePsd(tokenState, password, pass1, pass2) async {
    var params = jsonEncode({
      "RequestAdriaDto": {
        "userDetails": tokenState.getUserDetails(),
        "wrapper": {
          "canal": "1",
          "pass2": "$pass2",
          "password": "$password",
          "pass1": "$pass1"
        }
      }
    });

    var url = Uri.http(
        url_cam, "/app/contrats/contratAbonnementClient/changePassword");
    // print(params);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("change password compte service **");
      var data = json.decode(res.body);
      // data = compteMock;

      return data["Map"];
    } catch (e) {
      print(" chnage password compte service err *** $e");
      throw e;
    }
  }

  Future<dynamic> getContrat(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    var url = Uri.http(url_cam,
        "/app/contrats/contratAbonnementClient/listRattachementscontracts");

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("compte service get contrat ***");
      final data = json.decode(res.body);
      return Contrat.fromJson(data);
    } catch (e) {
      print("error get contrat *** $e");
      throw e;
    }
  }

  Future<dynamic> getCompte(String type) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "typeOperation": type,
    };
    var url = Uri.http(
        url_cam, "/app/compte/compte/comptesPourConsultation", qParams);

    http.Client httpClient = InterceptedClient.build(
        // requestTimeout: Duration(seconds: 2),
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      // if (kDebugMode) _alice.onHttpResponse(res);
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("compte service **");
      var data = json.decode(res.body);
      // print(data);
      return ListCompte.fromJson(data);
    } catch (e) {
      print("get compte service err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<dynamic> getComptesChequie() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "typeOperation": "DM",
      "type": "DEMANDE",
      "includeBenef": "non"
    };
    var url =
        Uri.http(url_cam, "/app/compte/compte/comptesPourTransaction", qParams);

    http.Client httpClient = InterceptedClient.build(
        // requestTimeout: Duration(seconds: 2),
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("compte cheque service **");
      var data = json.decode(res.body);

      return ListCompte.fromJson(data);
    } catch (e) {
      print("get compte chequier service err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<RechercheMouvement> getMouvements(page) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    // int page = 0;
    var qParams = {
      "ncompte": "${currentCompte.id}",
      "max": "10",
      "page": "$page",
    };
    // page++;
    // print("$page");
    var url = Uri.http(
        url_cam, "/app/compte/mouvement/rechercherMouvements", qParams);

    // print(url);
    // print("page : $page");
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      print("mouvement service **");
      data = json.decode(res.body);

      // if (data['Map']['results'].length == 0) {
      //   // print("return mouvement get all is Empty***");
      //   data = listMouvement;
      // }

      // data = listMouvement;

      RechercheMouvement historiqueMouvements =
          RechercheMouvement.fromJson(data);

      historiqueMouvements.map.transform(Mode.MONTH);
      return historiqueMouvements;
    } catch (e) {
      print("get mouvement service err *** $e");
      throw e;
    }
  }

  Future<RechercheMouvement> getRechercheMouvements(
      datemax, datemin, String libelle) {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    var page = 0;

    // print(pageSize);
    var qParams = {
      // "offset": "0",
      "ncompte": "${currentCompte.id}",
      // "format": "json",
      "max": "10",
      // "_dc": "$compteIdentifiant",
      "page": "$page",
      "bean_dateOperationMax": "$datemax",
      "bean_dateOperationMin": "$datemin",
      // "montantMin": "100",
      // "montantMax": "1000",
      // "sens":"D"
      if (libelle.isNotEmpty) "libelle": "$libelle"
    };
    ++page;
    // print("$page");
    var url = Uri.http(
        url_cam, "/app/compte/mouvement/rechercherMouvements", qParams);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;

    return httpClient.post(url, body: params).then((res) {
      print("mouvement service **");
      data = json.decode(res.body);
      // print(data);

      // if (data['Map']['results'].length == 0) {
      //   // print("return mouvement get all is Empty***");
      //   data = listMouvement;
      // }

      RechercheMouvement historiqueMouvements =
          RechercheMouvement.fromJson(data);

      historiqueMouvements.map.transform(Mode.MONTH);
      // print(historiqueMouvements.map.orderedlistMouvements.keys.length);
      return historiqueMouvements;
    }).catchError(
        (err) => print("get recherche mouvement service err *** $err"));
  }

  Future<dynamic> getDataGrpahique() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "success": "false",
      "reference": "${currentCompte.identifiantInterne}",
    };
    var url = Uri.http(
        url_cam, "/app/compte/compte/graphiqueCompte30DaysAgo", qParams);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");
      // if (kDebugMode) _alice.onHttpResponse(res);
      print("graph service **");
      data = json.decode(res.body);
      // print(data);
      //

      // data = graphMock;
      // print(data.toString());
      return Graphique.fromJson(data);
    } catch (e) {
      print("get graph service err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }

    // double avgDebit =
    //     data['Map']['debits'].map((m) => m.reduce((a, b) => a + b)) /
    //         data['Map']['debits'].length;
    // double avgCredit =
    //     data['Map']['credits'].map((m) => m.reduce((a, b) => a + b)) /
    //         data['Map']['credits'].length;
  }

  Future<Dat> getListDat(identifiantInterne, page) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    if (page == null) page = 1;
    var qParams = {
      "ncompteRecherche": "$identifiantInterne",
      "max": "20",
      "page": "$page",
      "offset": "0"
    };
    var url = Uri.http(url_cam, "/app/compte/DAT/listObject", qParams);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get list dat compte service service **");
      data = json.decode(res.body);

      // if (data['Map']['liste'].length == 0) {
      //   print("return dat Empty***");
      //   data = listDat;
      // }

      return Dat.fromJson(data);
    } catch (e) {
      print("compte service get list dat err *** $e");
      throw e;
    }
  }

  Future<HistoriqueChequier> getDemandeChequier(
      identifiantInterne, page) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});
    if (page == null) page = 1;
    var qParams = {
      "ncompteRecherche": "$identifiantInterne",
      "max": "20",
      "page": "$page"
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/demandeChequier/listObject", qParams);

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("demande cheqier service **");
      final data = json.decode(res.body);
      return HistoriqueChequier.fromJson(data);
    } catch (e) {
      print("compte service get demande chequier err *** $e");
      throw e;
    }
  }

  Future<FacturesHistory> getFacturesHistory() {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url =
        Uri.http(url_cam, "/app/moyen-paiement/paiementFacture/listObject");

    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    return httpClient.post(url, body: params).then((res) {
      print("FacturesHistory service **");
      data = json.decode(res.body);

      return FacturesHistory.fromJson(data);
    }).catchError((err) => print("get factures history service err *** $err"));
  }

  Future<Titres> getTitres() {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(
        url_cam, "/app/compte/portefeuilleTitre/portefeuilleTitreList");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    return httpClient.post(url, body: params).then((res) {
      print("Credit service **");
      data = json.decode(res.body);

      return Titres.fromJson(data);
    }).catchError((err) => print("get titres compte service err *** $err"));
  }

  Future<Credits> getCredits() {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam, "/app/compte/creditClient/listObjects");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    return httpClient.post(url, body: params).then((res) {
      print("Credit service **");
      data = json.decode(res.body);

      // if (data["Map"]["credits"].length == 0) data = listCreditMock;

      return Credits.fromJson(data);
    }).catchError((err) => print("get credits compte service err *** $err"));
  }

  Future<Rib> getRib(tokenState, numComp) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "numeroCompte": "$numComp",
      "identifiantContrat":
          "${tokenState.getUserDetails()["identifiantContrat"]}",
    };

    var url = Uri.http(url_cam, "/app/reportings/rib/pdf", qParams);

    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient.post(url, body: params);

      print("rib service **");
      data = json.decode(res.body);
      // print(data);
      return Rib.fromJson(data);
    } catch (e) {
      print("compte service get rib err *** $e");
      throw e;
    }
  }

  Future<Rib> getMouvementPdf(dateDeb, dateFin) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "dateOperationMax": "$dateFin",
      "dateOperationMin": "$dateDeb",
      "ncompte": "${currentCompte.id}",
      "format": "pdf",
    };

    var url = Uri.http(
        url_cam, "/app/reportings/report/rechercherMouvementPDF", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient.post(url, body: params);

      print("mouvement pdf service **");
      data = json.decode(res.body);
      // print(data);
      return Rib.fromJson(data);
    } catch (e) {
      print("compte service get mouvement pdf err *** $e");
      throw e;
    }
  }

  Future<dynamic> getsearchMouvement(
      tokenState, idComp, dateDeb, dateFin, max, page) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "max": "$max",
      "page": "$page",
      "bean_dateOperationMax": "$dateFin",
      "bean_dateOperationMin": "$dateDeb",
      "ncompte": "$idComp",
    };

    var url = Uri.http(
        url_cam, "/app/compte/mouvement/rechercherMouvements", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient.post(url, body: params);

      print("compte service search mouvement service **");
      data = json.decode(res.body);
      // print(data);
      return data["Map"];
    } catch (e) {
      print("compte service search mouvement err *** $e");
      throw e;
    }
  }

  Future<Messagerie> getMessagerieEnv(page) async {
    print('getMessagerieEnv + page: $page');
    var params = jsonEncode({
      "RequestAdriaDto": {
        "userDetails": tokenState.getUserDetails(),
        "wrapper": {
          "page": "$page",
        }
      }
    });

    var qParams = {
      "page": "$page",
      "limite": "10",
      // "start": "$start",
    };

    var url = Uri.http(
        url_cam, "/app/digital-marketing/message/getMessageEnvoye", qParams);
    // print(params);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("message env compte service **");
      var data = json.decode(res.body);
      // data = compteMock;
      // print(data);
      return Messagerie.fromJson(data);
    } catch (e) {
      print("message env compte service err *** $e");
      throw e;
    }
  }

  Future<Messagerie> getMessagerieRc(page) async {
    var params = jsonEncode({
      "RequestAdriaDto": {
        "userDetails": tokenState.getUserDetails(),
        "wrapper": {
          "page": "$page",
        }
      }
    });

    var qParams = {
      "page": "$page",
      "limite": "10",
      // "start": "$start",
    };

    var url = Uri.http(
        url_cam, "/app/digital-marketing/message/getMessageRecus", qParams);
    // print(params);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("message Rc compte service **");
      var data = json.decode(res.body);
      // data = compteMock;
      // print(data);
      return Messagerie.fromJson(data);
    } catch (e) {
      print("message Rc compte service err *** $e");
      throw e;
    }
  }

  Future<SubjectsMessagerie> getSubjectsMessagerie() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam,
        "/app/administration/nomenclatureCodification/subjectsMessagerie");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("Subjects Messagerie compte service **");
      data = json.decode(res.body);
      // print(data);
      return SubjectsMessagerie.fromJson(data);
    } catch (e) {
      print("compte service get Subjects Messagerie err *** $e");
      throw e;
    }
  }

  Future<Destinataire> getDestinataire() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url =
        Uri.http(url_cam, "/app/digital-marketing/message/getDestinataires");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("destinataire Messagerie compte service **");
      data = json.decode(res.body);
      // print(data);
      return Destinataire.fromJson(data);
    } catch (e) {
      print("compte service get destinataire Messagerie err *** $e");
      throw e;
    }
  }

  Future<DetailMessage> getDetailMessage(String id) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam, "/app/digital-marketing/message/history/$id");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("getDetailMessage Messagerie compte service **");
      data = json.decode(res.body);
      // print(data);
      return DetailMessage.fromJson(data);
    } catch (e) {
      print("compte service  getDetailMessage Messagerie err *** $e");
      throw e;
    }
  }

  Future<Devise> getDevise() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "page": "1",
      "max": "30",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
    };

    var url =
        Uri.http(url_cam, "/app/compte/coursdevise/getGridDevises", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("cours devises compte service **");
      data = json.decode(res.body);
      // print(data);
      return Devise.fromJson(data);
    } catch (e) {
      print("compte service get cours devises err *** $e");
      throw e;
    }
  }

  Future<CategorieCreancier> getCategorieCreanciers() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam,
        "/app/administration/nomenclatureCodification/getCategorieCreanciers");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("categorie creancier compte service **");
      data = json.decode(res.body);
      // print(data);
      return CategorieCreancier.fromJson(data);
    } catch (e) {
      print("compte service get categorie creancier err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<Creanciers> listCreanciers() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(
        url_cam, "/app/moyen-paiement/paiementFacture/api/listCreanciers");

    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("listCreanciers compte service **");
      data = json.decode(res.body);
      print(data);
      return Creanciers.fromJson(data);
    } catch (e) {
      print("compte service get listCreanciers err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<CreanceDetails> listCreancierChamp(codeC) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {"codeCreancier": "$codeC"};
    var url = Uri.http(url_cam,
        "/app/moyen-paiement/paiementFacture/api/listCreances", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("listCreancierChamp compte service **");
      data = json.decode(res.body);
      // print(data);
      return CreanceDetails.fromJson(data);
    } catch (e) {
      print("compte service get listCreancierChamp err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<CreancierForm> listCreancierForm(codeC, cd) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "codeCreancier": "$codeC",
      "codeCreance": "$cd",
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/paiementFacture/api/getForm", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("listCreancierForm compte service **");
      data = json.decode(res.body);
      // print(data);
      return CreancierForm.fromJson(data);
    } catch (e) {
      print("compte service get listCreancierForm err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<dynamic> creancierFormCreate(bodyObj) async {
    var params = jsonEncode({
      "RequestAdriaDto": {
        "userDetails": tokenState.getUserDetails(),
        "wrapper": bodyObj
      }
    });

    var url =
        Uri.http(url_cam, "/app/moyen-paiement/paiementFacture/api/create");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("creancierFormCreate compte service **");
      data = json.decode(res.body);

      return data;
    } catch (e) {
      print("compte service get creancierFormCreate err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<dynamic> saveFacture(
    agenceCmp,
    codeCreance,
    codeCreancier,
    codeProduitCompte,
    impayesJson,
    logo,
    referencePartenaire,
    type,
    intituleCompte,
    libelleCreance,
    libelleCreancier,
    montantTotalTTC,
    numeroCompte,
    paramsGlobal,
    paiementTotal,
  ) async {
    var qParams = {
      "agenceCompte": "$agenceCmp",
      "codeCreance": "$codeCreance",
      "codeCreancier": "$codeCreancier",
      "codeProduitCompte": "$codeProduitCompte",
      "creanceId": "$codeCreance",
      "creancierId": "$codeCreancier",
      "deviseCompte": deviseBanque,
      "deviseOperation": deviseBanque,
      "impayesJson": "$impayesJson",
      "intituleCompte": "$intituleCompte",
      "libelleCreance": "$libelleCreance",
      "libelleCreancier": "$libelleCreancier",
      "montantTotalTTC": "$montantTotalTTC",
      "montantTotalTTCAPayer": "$montantTotalTTC",
      "montantTotalTTCToString": "$montantTotalTTC MAD",
      "numeroCompte": "$numeroCompte",
      "paiementTotal": "$paiementTotal",
      "paramsGlobal": "$paramsGlobal",
      "pathLogo": "$logo",
      "radical": "${tokenState.getUserDetails()["username"]}",
      "referencePartenaire": "$referencePartenaire",
      "seuilMinimal": "0.0",
      "type": "$type",
      "valeurFrais": "0.0",
      "_alifAgent": "Mobile",
      "ajax": "true",
      "codeBanque": codeBanque,
      "codeBanqueContrat": codeBanque,
      "codeLangue": langue,
      "codePays": codePays,
      "format": "json",
      "lang": langue,
    };

    var url =
        Uri.http(url_cam, "/app/moyen-paiement/paiementFacture/save", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpSaveInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("save Facture compte service **");
      data = json.decode(res.body);
      // print(data);
      return data;
    } catch (e) {
      print("compte service get save Facture err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  // Banceassurance

  Future<Bancassurance> getBanceassurance() async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(url_cam,
        "/app/administration/nomenclatureCodification/getBankAssurance");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get Banceassurance compte service **");
      data = json.decode(res.body);
      return Bancassurance.fromJson(data);
    } catch (e) {
      print("compte service get Banceassurance err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<ListAssurance> getListAssurance(code) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "numeroCompte": "",
      "typeAssurance": "$code",
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/bankAssurance/listAssurances", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get ListAssurance compte service **");
      data = json.decode(res.body);
      // print(data);
      return ListAssurance.fromJson(data);
    } catch (e) {
      print("compte service get ListAssurance err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<BancAssuranceSituation> getSituation(
      productNumber, policeNumber) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "productNumber": "$productNumber",
      "policeNumber": "$policeNumber",
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/bankAssurance/getSituation", qParams);

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get Situation bancAssurance service **");
      data = json.decode(res.body);
      print(data);
      return BancAssuranceSituation.fromJson(data);
    } catch (e) {
      print("compte service get Situation bancAssurance err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<BancAssuranceCotisation> getCotisations(
      idContrat, typeAssurance) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "idContrat": "$idContrat",
      "max": "20",
      "ffset": "0",
      "page": "1",
      "typeAssurance": "$typeAssurance",
      "_alifAgent": "Mobile",
      "ajax": "true",
      "codeBanque": "$codeBanque",
      "codeBanqueContrat": "$codeBanque",
      "codeLangue": "$langue",
      "codePays": "$codePays",
      "format": "json",
      "lang": "$langue",
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/bankAssurance/listCotisations", qParams);

    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get Cotisations bancAssurance service **");
      data = json.decode(res.body);
      print(data);
      return BancAssuranceCotisation.fromJson(data);
    } catch (e) {
      print("compte service get Cotisations bancAssurance err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<CreditHabitatForm> getCreditHabitatForm(product, step) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      "product": "$product",
      "step": "$step",
    };
    var url = Uri.http(
        url_cam, "/app/moyen-paiement/credit/fields-configuration", qParams);

    print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("get crédit habitat form service **");
      data = json.decode(res.body);
      print(data);
      return CreditHabitatForm.fromJson(data);
    } catch (e) {
      print("compte service get crédit habitat form err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }
}
