import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/models/favoris_model.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';

import '../constants.dart';

class FactureService {
  Future<Favoris> getFavoris(tokenState) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var url = Uri.http(
        url_cam, "/app/moyen-paiement/paiementFacture/api/listFavoris");

    // print(url);
    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("liste favoris compte service **");
      data = json.decode(res.body);
      // print(data);
      return Favoris.fromJson(data);
    } catch (e) {
      print("compte service get liste favoris err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }

  Future<dynamic> deleteFavori(tokenState, idFav) async {
    var params = jsonEncode({"userDetails": tokenState.getUserDetails()});

    var qParams = {
      'id': '$idFav',
    };

    var url = Uri.http(url_cam,
        "/app/moyen-paiement/paiementFacture/api/deleteFavoris", qParams);

    print('url $url');

    http.Client httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(tokenState));

    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });
      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("delete facture compte service **");
      data = json.decode(res.body);
      // print(data);
      return data;
    } catch (e) {
      print("facture service delete Facture err *** $e");
      throw e;
    } finally {
      httpClient.close();
    }
  }
}
