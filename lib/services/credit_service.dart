import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:LBA/iterceptors/http.interceptor.dart';
import 'package:LBA/models/credits_step_two.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:get_it/get_it.dart';
import 'package:http_interceptor/http/intercepted_client.dart';

import '../constants.dart';

class CreditService{

  CompteService get compteService => GetIt.I<CompteService>();

  Future<CreditsStepTwo> getForm() async {
    var params = jsonEncode({"userDetails": compteService.tokenState.getUserDetails()});

    var qParams = {
      "product" :"177219876",
      "step": "2",
    };

    var url =
    Uri.http(url_cam, "/app/moyen-paiement/credit/fields-configuration", qParams);

    // print(url);
    var httpClient = InterceptedClient.build(
        interceptors: [HttpInterceptor(compteService.tokenState)],
        retryPolicy: ExpiredTokenRetryPolicy(compteService.tokenState));
    var data;
    try {
      final res = await httpClient
          .post(url, body: params)
          .timeout(Duration(seconds: maxTimeOut), onTimeout: () {
        throw TimeoutException('connection has timed out, Please try again!');
      });

      if (!(res.statusCode >= 200 && res.statusCode < 300))
        throw HttpException("Http Exception ${res.statusCode}");

      print("credit step2 service **");
      data = json.decode(res.body);
      // print(data);
      return CreditsStepTwo.fromJson(data);
    } catch (e) {
      print("credit step2 err *** $e");
      throw e;
    }
  }
}