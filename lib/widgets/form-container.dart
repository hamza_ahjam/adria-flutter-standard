import 'package:flutter/material.dart';

class FormContainer extends StatelessWidget {
  const FormContainer({
    Key key,
    @required this.child,
  }) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 15, 15, 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            // BoxShadow(
            //     spreadRadius: 2,
            //     blurRadius: 0.5,
            //     color: Color.fromRGBO(91, 102, 112, 0.3),
            //     offset: Offset(2, 2))
            BoxShadow(
              color: Color.fromRGBO(91, 102, 112, 0.1),
              offset: const Offset(
                0.0,
                0.0,
              ),
              blurRadius: 8,
              spreadRadius: 2.0,
            ), //BoxShadow
            BoxShadow(
              color: Color.fromRGBO(91, 102, 112, 0.1),
              offset: const Offset(0.0, 0.0),
              blurRadius: 0.0,
              spreadRadius: 0.0,
            ),
          ]),
      child: child,
    );
  }
}
