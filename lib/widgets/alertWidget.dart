
import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.bloc.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.bloc.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/rib/rib.bloc.dart';
import 'package:LBA/bloc/rib/rib.event.dart';
import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../shared_data.dart';

errorAlert(context, txt) {
  return showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/$banque_id/images/error.png",
                  width: 50,
                  height: 50,
                  color: GlobalParams.themes["$banque_id"].appBarColor,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "$txt",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(color: Colors.black, fontSize: 20)),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(AppLocalizations.of(context).fermer,
                            style:
                                TextStyle(color: Colors.white, fontSize: 16)),
                      )),
                ),
              ],
            ),
          ),
        );
      });
}

// Alert to show after a period of inactivity
inactivityAlert(context) {
  return showDialog(
      barrierDismissible: false,
      context: context,
      // barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/$banque_id/images/error.png",
                  width: 50,
                  height: 50,
                  color: GlobalParams.themes["$banque_id"].appBarColor,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "votre session a expiré",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle:
                        TextStyle(color: Colors.black, fontSize: 20)),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      onPressed: () {
                        context.read<CompteBloc>().add(ResetAccountEvent());

                        context.read<BeneficiaireBloc>().add(ResetBenefEvent());

                        context.read<CarteBloc>().add(ResetCarteEvent());

                        context.read<ChequeBloc>().add(ResetChequeEvent());

                        context.read<CreditBloc>().add(InitCreditEvent());

                        context
                            .read<HistoriqueVirementBloc>()
                            .add(ResetHistoriqueVirementEvent());

                        context
                            .read<HistoriqueVirementCompteBloc>()
                            .add(ResetHistoriqueVirementCompteEvent());

                        context.read<RibBloc>().add(InitRibEvent());

                        context.read<DatBloc>().add(InitDatEvent());

                        context.read<MessagerieBloc>().add(ResetMessageEvent());

                        context.read<TokenBloc>().add(LogoutEvent());

                        appData.isLogout = true;

                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (context) => LoginPage()),
                                (Route<dynamic> route) => false);
                      },
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      child: Text("OK",
                            style:
                            TextStyle(color: Colors.white, fontSize: 16)),
                      ),
                ),
              ],
            ),
          ),
        );
      });
}

okAlert(context, txt) {
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                "assets/$banque_id/images/imo.png",
                width: 50,
                height: 50,
                color: GlobalParams.themes["$banque_id"].appBarColor,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  "$txt",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(color: Colors.black, fontSize: 20)),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(AppLocalizations.of(context).fermer,
                          style: TextStyle(color: Colors.white, fontSize: 16)),
                    )),
              ),
            ],
          ),
        );
      });
}

showHistoriqueDetail(nom, montant, devise, motif, date, ref, status, context) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50.0))),
        title: Center(
          child: Text("$status",
              style: TextStyle(
                  color: KPrimaryColor[700],
                  fontFamily: KprimaryFont,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
        ),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.25,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Bénéficiaire : ",
                        style: TextStyle(
                            color: KSecandaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    Text("$nom",
                        style: TextStyle(
                            color: KPrimaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Montant : ",
                        style: TextStyle(
                            color: KSecandaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    Text("$montant $devise",
                        style: TextStyle(
                            color: KPrimaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Motif : ",
                        style: TextStyle(
                            color: KSecandaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    Text("$motif",
                        style: TextStyle(
                            color: KPrimaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Date création : ",
                        style: TextStyle(
                            color: KSecandaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    Text("$date",
                        style: TextStyle(
                            color: KPrimaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Référence : ",
                        style: TextStyle(
                            color: KSecandaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    Text("$ref",
                        style: TextStyle(
                            color: KPrimaryColor,
                            fontFamily: KprimaryFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold))
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

showLoading(context, {dismiss}) {
  return showDialog(
    context: context,
    barrierDismissible: dismiss != null ? dismiss : true,
    builder: (context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.2,
          alignment: Alignment.center,
          child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          ),
        ),
      );
    },
  );
}
