import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../main.dart';

class PageWrapperWidget extends StatelessWidget {
  Widget child;
  PageWrapperWidget({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<TokenBloc, TokenStateBloc>(
      builder: (context, state) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (state.token != null) ? MyApp.handleUserInteraction : (){ print('tap');},
          onPanDown: (state.token != null) ? MyApp.handleUserInteraction : ([_]){ print('pan down');},
            child: child,);
      }
    );
  }
}
