import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppBarWidget extends AppBar {
  BuildContext context;
  String text;

  AppBarWidget({this.context, this.text})
      : super(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
            elevation: 0,
      title: text != null ? Text(text, style: TextStyle(
          color: GlobalParams.themes["$banque_id"].infoColor,
          fontSize: 20,
          fontWeight: FontWeight.w600
      ),) : Text(""),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_rounded,
                  size: 30,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                }));
}
