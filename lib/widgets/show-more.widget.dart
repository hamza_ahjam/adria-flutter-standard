import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class ShowMore extends StatelessWidget {
  final Function() showMoreAction;
  const ShowMore({this.showMoreAction});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 15),
      child: GestureDetector(
        onTap: () {
          showMoreAction();
        },
        child: Text(
          "${AppLocalizations.of(context).afficher_plus} ...",
          textAlign: TextAlign.center,
          style: GoogleFonts.roboto(
              color: secondaryColor2,
              fontSize: 16,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
