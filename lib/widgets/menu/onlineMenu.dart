import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/cartes/carte.page.dart';
import 'package:LBA/pages/cheques/liste-cheque.page.dart';
import 'package:LBA/pages/credits/credits.page.dart';
import 'package:LBA/pages/factures/accueil_facture_page.dart';
import 'package:LBA/pages/rib/rib.page.dart';
import 'package:LBA/pages/virements/liste-virement.page.dart';
import 'package:LBA/pages/virements/virement_navigation.widget.dart';
import 'package:expandable_bottom_bar/expandable_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnlineMenu extends StatefulWidget {
  const OnlineMenu({Key key}) : super(key: key);

  @override
  _OnlineMenuState createState() => _OnlineMenuState();
}

class _OnlineMenuState extends State<OnlineMenu> {
  bool isUp = false;
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Container(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
      child: BottomExpandableAppBar(
        horizontalMargin: 0,
        bottomOffset: 0,

        expandedHeight: MediaQuery.of(context).size.height * 0.055,
        // horizontalMargin: 26,
        // shape: AutomaticNotchedShape(
        //     RoundedRectangleBorder(), StadiumBorder(side: BorderSide())),
        expandedBackColor: Colors.white,
        expandedBody: Padding(
          padding: EdgeInsets.only(bottom: 40),
          child: Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.016),
            alignment: Alignment.topCenter,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  // topRight: Radius.circular(20)
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(91, 102, 112, 0.1),
                    offset: const Offset(
                      0.0,
                      0.0,
                    ),
                    blurRadius: 10,
                    spreadRadius: 3.0,
                  ), //BoxShadow
                  BoxShadow(
                    color: Color.fromRGBO(91, 102, 112, 0.1),
                    offset: const Offset(0.0, 0.0),
                    blurRadius: 10,
                    spreadRadius: 3.0,
                  ),
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Transform.translate(
                  offset: Offset(-5, 0),
                  child: GestureDetector(
                    onTap: () {
                      DefaultBottomBarController.of(context).swap();
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListChequePage()));
                    },
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/mlcn.svg",
                      width: width * 0.02,
                      height: height * 0.02,
                      // color: principaleColor8,
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(-15, 0),
                  child: GestureDetector(
                    onTap: () {
                      DefaultBottomBarController.of(context).swap();
                      Navigator.pop(context);

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreditsPage()));
                    },
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/mcredit.svg",
                      width: width * 0.03,
                      height: height * 0.03,
                      // color: principaleColor6,
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(-15, 0),
                  child: GestureDetector(
                    onTap: () {
                      DefaultBottomBarController.of(context).swap();
                      Navigator.pop(context);

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListeVirementPage(
                                    isComingFromMenu: true,
                                  )));
                    },
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/vir_benef.svg",
                      width: width * 0.03,
                      height: height * 0.03,
                      // color: GlobalParams.themes["$banque_id"].otherButtonColor,
                    ),
                    // Image.asset(
                    //   "assets/images/bénéficiaires.png",
                    //   // width: 100,
                    //   // height: 100,
                    //   color: Color.fromRGBO(196, 0, 255, 1),
                    // ),
                  ),
                ),
                banque_id != "LBA"
                    ? Transform.translate(
                        offset: Offset(-5, 0),
                        child: GestureDetector(
                          onTap: () {
                            DefaultBottomBarController.of(context).swap();
                            Navigator.pop(context);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AccueilFacturePage()));
                          },
                          child: SvgPicture.asset(
                            "assets/$banque_id/images/bt_Factures.svg",
                            width: width * 0.03,
                            height: height * 0.03,
                            // color: GlobalParams
                            //     .themes["$banque_id"].otherButtonColor,
                          ),
                        ),
                      )
                    : Container(
                        width: MediaQuery.of(context).size.width * 0.03,
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                // GestureDetector(
                //   onTap: () {
                //     DefaultBottomBarController.of(context).swap();
                //     Navigator.pop(context);

                //     Navigator.push(context,
                //         MaterialPageRoute(builder: (context) => RibPage()));
                //   },
                //   child: SvgPicture.asset(
                //     "assets/$banque_id/images/ribf.svg",
                //     width: width * 0.035,
                //     height: height * 0.035,
                //     // color: principaleColor5,
                //   ),
                // ),
              ],
            ),
          ),
        ),
        bottomAppBarBody: Container(
          margin: EdgeInsets.only(top: 15),
          padding: EdgeInsets.only(top: 11),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                // topRight: Radius.circular(20)
              ),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(91, 102, 112, 0.1),
                  offset: const Offset(0, 0),
                  blurRadius: 5,
                  spreadRadius: 3,
                ), //BoxShadow
              ]),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // GestureDetector(
              //   onTap: () {
              //     Navigator.pop(context);

              //     Navigator.push(context,
              //         MaterialPageRoute(builder: (context) => FacturePage()));
              //   },
              //   child: Image.asset(
              //     "assets/$banque_id/images/bt_Factures.png",
              //     width: 50,
              //     height: 50,
              //   ),
              // ),
              GestureDetector(
                onTap: () {
                  DefaultBottomBarController.of(context).swap();
                  Navigator.pop(context);

                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RibPage()));
                },
                child: SvgPicture.asset(
                  "assets/$banque_id/images/ribf.svg",
                  width: width * 0.03,
                  height: height * 0.03,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VirementNavigationPage()));
                },
                child: SvgPicture.asset(
                  "assets/$banque_id/images/bt_virements.svg",
                  width: width * 0.03,
                  height: height * 0.03,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);

                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CartePage()));
                },
                child: SvgPicture.asset(
                  "assets/$banque_id/images/bt_cartes.svg",
                  width: width * 0.03,
                  height: height * 0.03,
                ),
              ),
              GestureDetector(
                  onVerticalDragUpdate:
                      DefaultBottomBarController.of(context).onDrag,
                  onVerticalDragEnd:
                      DefaultBottomBarController.of(context).onDragEnd,
                  onTap: () {
                    DefaultBottomBarController.of(context).swap();
                    // print("+");
                    setState(() {
                      isUp = !isUp;
                    });
                  },
                  child: Container(
                    width: 35,
                    height: 35,
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].otherButtonColor,
                        shape: BoxShape.circle),
                    child: Icon(
                      !isUp
                          ? Icons.arrow_drop_up_sharp
                          : Icons.arrow_drop_down_sharp,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                  // Image.asset(
                  //   "assets/$banque_id/images/bt_+.png",
                  //   width: 50,
                  //   height: 50,
                  // ),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}
