import 'package:LBA/agences/agency_page.dart';
import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/infomations/information.page.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/numero_agence/numeroAgence.dart';
// import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class OffLineMenu extends StatefulWidget {
  int index;
  OffLineMenu(this.index);

  @override
  _OffLineMenuState createState() => _OffLineMenuState();
}

class _OffLineMenuState extends State<OffLineMenu> {
  int _page = 0;

  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    // print(ModalRoute.of(context).settings.name);
    // print(_index);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.1,
      child:
          //  CurvedNavigationBar(
          //   key: _bottomNavigationKey,
          //   index: widget.index,
          //   backgroundColor: Colors.purple[200],
          //   color: Colors.white,
          //   buttonBackgroundColor: Colors.white,
          //   // animationCurve: Curves.bounceInOut,
          //   animationDuration: Duration(milliseconds: 600),
          //   letIndexChange: (index) => true,
          // onTap: (index) {
          //   setState(() {
          //     _page = _index;
          //   });
          //   print(" page $_page");
          // }
          // child: BottomAppBar(
          Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          GestureDetector(
              onTap: () {
                setState(() {
                  widget.index = 0;
                });
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NumeroAgence()));
              },
              child: SvgPicture.asset(
                "assets/$banque_id/images/call.svg",
                width: width * 0.035,
                height: height * 0.035,
                color: widget.index == 0
                    ? GlobalParams.themes["$banque_id"].otherButtonColor
                    : GlobalParams.themes["$banque_id"].cancelButtonTextColor,
              )),

          GestureDetector(
            onTap: () {
              setState(() {
                widget.index = 1;
              });
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AgencyPage()));
            },
            child: SvgPicture.asset(
              "assets/$banque_id/images/bt_agence.svg",
              width: width * 0.035,
              height: height * 0.035,
              color: widget.index == 1
                  ? GlobalParams.themes["$banque_id"].otherButtonColor
                  : GlobalParams.themes["$banque_id"].cancelButtonTextColor,
            ),
          ),

          // GestureDetector(
          //   onTap: () {
          //     // setState(() {
          //     //   _index = 2;
          //     // });
          //     print("demo");
          //   },
          //   child: Image.asset("assets/images/bt_demo.png"),
          // ),

          GestureDetector(
            onTap: () {
              setState(() {
                widget.index = 2;
              });
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => InformationPage()));
            },
            child: SvgPicture.asset(
              "assets/$banque_id/images/bt_Info.svg",
              width: width * 0.035,
              height: height * 0.035,
              color: widget.index == 2
                  ? GlobalParams.themes["$banque_id"].otherButtonColor
                  : GlobalParams.themes["$banque_id"].cancelButtonTextColor,
            ),
          ),

          GestureDetector(
              onTap: () {
                modeDemo = true;
                context.read<TokenBloc>().add(LoginEvent());
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardComptePage()));
              },
              child: Icon(
                Icons.developer_mode,
                size: 35,
                color: GlobalParams.themes["$banque_id"].cancelButtonTextColor,
              )),

          // GestureDetector(
          //   onTap: () {
          //     // setState(() {
          //     //   _index = 4;
          //     // });
          //     Navigator.pop(context);
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => LoginPage()));
          //     // print(ModalRoute.of(context).settings.name);
          //   },
          //   child: Image.asset(
          //     "assets/$banque_id/images/bt_afficher+.png",
          //     width: 60,
          //     height: 60,
          //   ),
          // ),

          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceAround,
          //   children: [
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       _page = 0;
          //     });
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => NumeroAgence()));
          //   },
          //   child: Image.asset("assets/images/call.png"),
          // ),
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       _page = 1;
          //     });
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => AgencyPage()));
          //   },
          //   child: Image.asset("assets/images/bt_agence.png"),
          // ),
          // GestureDetector(
          //   onTap: () {
          //     print("demo");
          //   },
          //   child: Image.asset("assets/images/bt_demo.png"),
          // ),
          // GestureDetector(
          //   onTap: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => InformationPage()));
          //   },
          //   child: Image.asset("assets/images/bt_Info.png"),
          // ),
          // GestureDetector(
          //   onTap: () {
          //     Navigator.pop(context);
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => LoginPage()));
          //   },
          //   child: Image.asset("assets/images/bt_afficher.png"),
          // ),
          // //   ],
          // ),
        ],
      ),
      // ),
    );
  }
}
