
import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.bloc.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.bloc.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/rib/rib.bloc.dart';
import 'package:LBA/bloc/rib/rib.event.dart';
import 'package:LBA/bloc/titres/titre_bloc.dart';
import 'package:LBA/bloc/titres/titre_event.dart';
import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/models/token.model.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import '../../shared_data.dart';

class OnlineDrawer extends StatelessWidget {
  Token tokenState;
  // CompteState compteState;
  // BeneficiaireState beneficiaireState;
  // CarteState carteState;
  // var userDetail = null;
  OnlineDrawer({this.tokenState}) {
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState = Provider.of<CompteState>(context, listen: false);
    // beneficiaireState = Provider.of<BeneficiaireState>(context, listen: false);
    // carteState = Provider.of<CarteState>(context, listen: false);

    // if (tokenState.token != null) {
    //   userDetail = tokenState.getUserDetails();
    // }
  }

//   @override
//   _OnlineDrawerState createState() => _OnlineDrawerState();
// }

// class _OnlineDrawerState extends State<OnlineDrawer> {
  // PickedFile _imageFile;

  // final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.only(
            topRight: MyApp.of(context).getLocale().languageCode == 'ar' ? Radius.circular(0) : Radius.circular(70.0),
            topLeft: MyApp.of(context).getLocale().languageCode == 'ar' ? Radius.circular(70.0) : Radius.circular(0),
        ),
        child: Drawer(
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                DrawerHeader(
                    padding: EdgeInsets.zero,
                    margin: EdgeInsets.zero,
                    decoration: BoxDecoration(color: Colors.white),
                    child: banque_id == "ADRIA" || banque_id == "NSIA"
                        ? Image.asset(
                            "assets/$banque_id/images/drawerHeader.png",
                            // fit: BoxFit.cover,
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.width * 0.8,
                            // color:
                            //     GlobalParams.themes["$banque_id"].appBarColor,
                          )
                        : SvgPicture.asset(
                            "assets/$banque_id/images/drawerHeader.svg",
                            fit: BoxFit.scaleDown,
                            // height: MediaQuery.of(context).size.height * 0.08,
                            width: MediaQuery.of(context).size.width * 0.65,
                          )),
                Container(
                  width: double.infinity,
                  //height: MediaQuery.of(context).size.height * 0.14,
                  //  banque_id == "ADRIA"
                  //     ? Color.fromRGBO(253, 92, 0, 0.7)
                  //     : Color.fromRGBO(213, 7, 88, 0.4),

                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage(
                      "assets/$banque_id/images/bgdrawer.png",
                    ),
                    fit: BoxFit.fill,
                  )),
                  child: Row(
                    // mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      // GestureDetector(
                      //   onTap: () {
                      //     // showImagePickerDialof(context);
                      //   },
                      //   child: Padding(
                      //     padding: const EdgeInsets.only(left: 5),
                      //     child: CircleAvatar(
                      //         radius: 30,
                      //         backgroundImage:
                      //             //  _imageFile == null
                      //             //     ?
                      //             AssetImage(
                      //           "assets/$banque_id/images/avatar.png",
                      //         )
                      //         // : FileImage(File(_imageFile.path)),
                      //         ),
                      //   ),
                      // ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02,
                        ),
                        child: tokenState.getUserDetails() != null
                            ? Column(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    child: Text(
                                      tokenState.getUserDetails()['fullName'] ??
                                          "",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .userInfo,
                                              fontSize: 15)),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 3, top: 3),
                                    child: Text(
                                      tokenState.getUserDetails()['email'] ??
                                          "",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .userInfo,
                                              fontSize: 13)),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 25),
                                    child: Text(
                                      tokenState.getUserDetails()['gsm'] ?? "",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .userInfo,
                                              fontSize: 11)),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.02,
                                  ),
                                ],
                              )
                            : Container(),
                      ),
                      // GestureDetector(
                      //   onTap: () {
                      //     print("setting");
                      //   },
                      //   child: CircleAvatar(
                      //     backgroundImage:
                      //         AssetImage("assets/$banque_id/images/reg.png"),
                      //     radius: 15,
                      //   ),
                      // ),
                      GestureDetector(
                        onTap: () {
                          showDeconnectionDialog(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: SvgPicture.asset(
                              "assets/$banque_id/images/decon.svg"),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                      child: ListView(
                        padding: EdgeInsets.all(0),
                        children: <Widget>[
                          ...(onlineDrawer(context) as List).map((nav) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                        margin: const EdgeInsets.only(
                                            left: 0.0, right: 3),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.02,
                                        child: Divider(
                                          color: Colors.black26,
                                          height: 15,
                                        )),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10),
                                      child: Text(
                                        nav["categoryName"]
                                            .toString()
                                            .toUpperCase(),
                                        textAlign: TextAlign.right,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                          color: GlobalParams
                                              .themes["$banque_id"]
                                              .intituleCmpColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        )),
                                      ),
                                    ),
                                  ],
                                ),
                                ...(nav['items'] as List).map((item) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                      // Navigator.pop(context);
                                      item['route'] != '/'
                                          ?
                                          // print( item['route'](context: context))
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType
                                                      .leftToRight,
                                                  // childCurrent: this,
                                                  duration: Duration(
                                                      milliseconds: 500),
                                                  // curve: Curves.easeIn,
                                                  child:
                                                      (item['route'](context))))
                                          : print(item['title'].toString());
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 10, 8),
                                          child: SvgPicture.asset(
                                            "assets/$banque_id/images/${item['image']}.svg",
                                            fit: BoxFit.contain,
                                            // width: 16,
                                            // height: 16,
                                          ),
                                        ),
                                        Text(
                                          item['title'].toString(),
                                          textAlign: TextAlign.right,
                                          style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .appBarColor,
                                            fontSize: 14,
                                          )),
                                        ),
                                      ],
                                    ),
                                  );
                                })
                              ],
                            );
                          }),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  // showImagePickerDialof(context) {
  //   return showDialog(
  //     context: context,
  //     barrierDismissible: true,
  //     builder: (context) => AlertDialog(
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.all(Radius.circular(25))),
  //       title: Text("Choisir une image de profil"),
  //       content: Row(
  //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //         children: <Widget>[
  //           TextButton.icon(
  //               onPressed: () {
  //                 takeImage(ImageSource.camera);
  //               },
  //               icon: Icon(
  //                 Icons.camera_alt_outlined,
  //                 color: GlobalParams.themes["$banque_id"].iconsColor,
  //               ),
  //               label: Text(
  //                 "Camera",
  //                 style: GoogleFonts.roboto(
  //                     textStyle: TextStyle(color: Colors.black)),
  //               )),
  //           TextButton.icon(
  //               onPressed: () {
  //                 takeImage(ImageSource.gallery);
  //               },
  //               icon: Icon(Icons.image,
  //                   color: GlobalParams.themes["$banque_id"].iconsColor),
  //               label: Text(
  //                 "Gallery",
  //                 style: GoogleFonts.roboto(
  //                     textStyle: TextStyle(color: Colors.black)),
  //               ))
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // void takeImage(ImageSource source) async {
  //   final pickedFile = await _picker.getImage(source: source);
  //   setState(() {
  //     _imageFile = pickedFile;
  //   });
  // }
}

void showDeconnectionDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        titlePadding: EdgeInsets.zero,
        title: Container(
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0),
              topRight: Radius.circular(25.0),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 8, 5),
                child: Icon(
                  Icons.logout,
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  size: 33,
                ),
              ),
              Text(
                AppLocalizations.of(context).deconnexion.toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  bottom: MediaQuery.of(context).size.height * 0.03),
              child: Text(
                  AppLocalizations.of(context).deconnexion_msg,
                  textAlign: TextAlign.center,
                  style:
                      GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18))),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  onPressed: () => Navigator.pop(context),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  child: Text(AppLocalizations.of(context).non,
                      style: GoogleFonts.roboto(
                          textStyle:
                              TextStyle(fontSize: 17, color: Colors.white))),
                ),
                RaisedButton(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor)),
                  color: Colors.white,
                  child: Text(AppLocalizations.of(context).oui,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              fontSize: 17,
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor))),
                  onPressed: () {
                    context.read<CompteBloc>().add(ResetAccountEvent());

                    context.read<BeneficiaireBloc>().add(ResetBenefEvent());

                    context.read<CarteBloc>().add(ResetCarteEvent());

                    context.read<ChequeBloc>().add(ResetChequeEvent());

                    context.read<CreditBloc>().add(InitCreditEvent());

                    context
                        .read<HistoriqueVirementBloc>()
                        .add(ResetHistoriqueVirementEvent());

                    context
                        .read<HistoriqueVirementCompteBloc>()
                        .add(ResetHistoriqueVirementCompteEvent());

                    context.read<RibBloc>().add(InitRibEvent());

                    context.read<DatBloc>().add(InitDatEvent());

                    context.read<MessagerieBloc>().add(ResetMessageEvent());

                    context.read<FactureBloc>().add(ResetFactureEvent());

                    context.read<TitreBloc>().add(InitTitreEvent());

                    context.read<TokenBloc>().add(LogoutEvent());

                    // changer la valeur de cette variable qui va etre utilisé pour s'assurer que l'utilisateur n'est pas connecté
                    appData.isLogout = true;

                    //Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => LoginPage()),
                        (Route<dynamic> route) => false);
                  },
                ),
              ],
            )
          ],
        ),
      );
    },
  );
}
