import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/cheques/liste-cheque.page.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OnlineAppBarWidget extends AppBar {
  String appBarTitle;
  BuildContext context;
  Route destination;
  bool isPop;
  bool isHisto;
  bool isChq;
  bool isWallet;
  Function walletDeconnexion;

  OnlineAppBarWidget({
    this.appBarTitle,
    this.context,
    this.isPop = false,
    this.isHisto = false,
    this.destination,
    this.isChq = false,
    this.isWallet = false,
    this.walletDeconnexion
  })
      : super(
    systemOverlayStyle: SystemUiOverlayStyle.light,
    title: Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Text(
        "$appBarTitle".toUpperCase(),
        style: GoogleFonts.roboto(
            textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
      ),
    ),
    centerTitle: true,
    elevation: 0,
    backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
    actions: [
            isHisto
                ? Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, destination);
                        },
                        child: SvgPicture.asset(
                            'assets/$banque_id/images/histo_icon.svg')),
                  )
                : isWallet
                    ? Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: GestureDetector(
                            onTap: () {
                              walletDeconnexion();
                            },
                            child: SvgPicture.asset(
                                'assets/$banque_id/images/decon.svg')),
                      )
                    : Container()
          ],
    leading:  !isWallet ? IconButton(
        icon: Icon(
                      Icons.arrow_back_rounded,
                      color: Colors.white,
                    ),
              onPressed: () {
                if (isChq)
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ListChequePage()));
                else
                  !isPop
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DashboardComptePage()))
                      : Navigator.pop(context);
              }) : Container(),
        );
}
