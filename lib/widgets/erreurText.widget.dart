import 'package:LBA/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class ErreurTextWidget extends StatelessWidget {
  final Function actionEvent;
  String errorMessage;
  ErreurTextWidget({this.actionEvent, this.errorMessage});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "${AppLocalizations.of(context).error_msg}. ",
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(color: principaleColor6)),
              ),
              TextSpan(
                  text: AppLocalizations.of(context).ressayer_msg,
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(color: secondaryColor2)),
              recognizer:  TapGestureRecognizer()
                ..onTap = () {
                  actionEvent();
                }
              ),
              if (kDebugMode)
              TextSpan(
                  text: " -$errorMessage",
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(color: principaleColor6))),
            ],
          ),
        ),
      ),
    );
  }
}
