import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Header extends StatelessWidget {
  bool isFromAgences;

  Header({Key key, this.isFromAgences = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: isFromAgences
          ? MediaQuery.of(context).size.height * 0.15
          : MediaQuery.of(context).size.height * 0.2,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          banque_id == 'NSIA' || banque_id == 'Adria'
              ? Image.asset(
                  "assets/$banque_id/images/A_LOGO.png",
                  fit: BoxFit.cover,
                )
              : SvgPicture.asset(
                  "assets/$banque_id/images/bg.svg",
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: MediaQuery.of(context).size.height * 0.09,
                ),
        ],
      ),
    );
  }
}
