
import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/bloc/token/token.state.dart';
import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/main.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/wallet/wallet_page.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
// import 'package:flutter_secure_keyboard/flutter_secure_keyboard.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';

import '../../shared_data.dart';

class LoginForm extends StatefulWidget {
  // TokenState tokenState;
  LoginForm() {
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // tokenState.loadAuthenticatedUser().then((value) {}).catchError((err) {});
  }

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final storage = new FlutterSecureStorage();

  static String dropdownType = 'fr';

  bool eerLoaded = false;

  @override
  void initState() {
    super.initState();
    storage.read(key: "login").then((res) {
      userNameController.text = res;
    }).catchError((err) => print("storage err $err"));

    storage.read(key: "psd").then((res) {
      setState(() {
        psd = res;
      });
    }).catchError((err) {
      psd = null;
      print("storage psd err $err");
    });
  }

  @override
  void dispose() {
    super.dispose();
    userNameController.dispose();
    passwordController.dispose();
    idController.dispose();
    emController.dispose();
    passwordTextFieldFocusNode.dispose();
  }

  String psd;
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController emController = TextEditingController();

  // var token;
  final passwordTextFieldFocusNode = FocusNode();

  GlobalKey<FormState> _formLoginKey = GlobalKey<FormState>();
  GlobalKey<FormState> _formChangePsdKey = GlobalKey<FormState>();
  String identifiant = "";
  String password = "";
  bool remember = true;
  bool isWallet = false;
  bool showPsd = false;
  bool logged = false;
  bool secureKeyboard = false;
  final List<String> errors = [];

  static final auth = LocalAuthentication();

  static Future<bool> hasBiometrics() async {
    try {
      return await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print("local auth hasBiometrics ** $e");
      return false;
    }
  }

  static Future<List<BiometricType>> getBiometrics() async {
    try {
      return await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print("local auth getBiometrics ** $e");
      return <BiometricType>[];
    }
  }

  static Future<bool> authenticate() async {
    final isAvailable = await hasBiometrics();
    if (!isAvailable) return false;

    try {
      return await auth.authenticateWithBiometrics(
        localizedReason: 'Scan Fingerprint to Authenticate',
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    var keyboard = new List<int>.generate(10, (int index) => index);
    keyboard.shuffle();
    // SystemChrome.setEnabledSystemUIOverlays([]);
    return Container(
      child: Form(
        key: _formLoginKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.065,
                  decoration: BoxDecoration(
                      color: GlobalParams.themes["$banque_id"].inputBackground,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    textAlignVertical: TextAlignVertical.center,
                    controller: userNameController,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(color: Colors.white)),
                    onChanged: (v) {
                      if (v.length == 0) {
                        setState(() {
                          psd = null;
                        });
                      }
                    },
/*                    validator: (v) {
                      if (v.isEmpty) return "Identifiant ne peut pas etre vide";
                      return null;
                    },*/
                    keyboardType: TextInputType.visiblePassword,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).identifiant,
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.white, fontSize: 16),
                      prefixIcon: Icon(Icons.person, color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.065,
                  decoration: BoxDecoration(
                      color: GlobalParams.themes["$banque_id"].inputBackground,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          textAlignVertical: TextAlignVertical.center,
                          controller: passwordController,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(color: Colors.white)),
                          enableInteractiveSelection: false,
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());

                            showClaVir(context, keyboard);
                          },
/*                          validator: (v) {
                            if (v.isEmpty)
                              return "Mot de passe ne peut pas etre vide";
                            return null;
                          },*/
                          keyboardType: TextInputType.number,
                          obscureText: showPsd == false ? true : false,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).mot_de_passe,
                            hintStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                            prefixIcon: Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(right: 18, left: 18),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                showPsd = !showPsd;
                              });
                            },
                            child: showPsd == false
                                ? Icon(Icons.visibility_off,
                                    color: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor)
                                : Icon(Icons.visibility,
                                    color: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor),
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: double.infinity,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(AppLocalizations.of(context).se_connecter,
                          style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          )),
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      onPressed: () {
                        if (userNameController.text.isEmpty ||
                            passwordController.text.isEmpty) {
                          errorAlert(
                              context, AppLocalizations.of(context).champs_vides);
                        } else {
                          if (remember) {
                            storage.write(
                                key: "login", value: userNameController.text);
                            if (passwordController.text.isNotEmpty) {
                              storage.write(
                                  key: "psd", value: passwordController.text);

                              storage.read(key: "psd").then((res) {
                                setState(() {
                                  psd = res;
                                });
                              });
                            }
                          } else {
                            storage.deleteAll();
                          }

                          if (_formLoginKey.currentState.validate()) {
                            modeDemo = false;

                            if (isWallet) {
                              context.read<WalletBloc>().add(EntrerEnRelationOfflineEvent(isLogin: true));
                            } else {
                              context.read<TokenBloc>().add(LoginEvent(
                                  userName: userNameController.text,
                                  password: passwordController.text));
                            }
                          }
                        }
                      }),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () => forgetPsd(context, idController,
                          emController, _formChangePsdKey),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).mp_oublie,
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  decoration: TextDecoration.underline,
                                ),
                              )),
                        ),
                      ),
                    ),
                    Transform.translate(
                      offset: Offset(8, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            alignment: Alignment.centerRight,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(AppLocalizations.of(context).se_souvenir,
                                  maxLines: 1,
                                  textAlign: TextAlign.end,
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        color: Colors.white, fontSize: 12),
                                  )),
                            ),
                          ),
                          Switch(
                            activeColor: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            activeTrackColor: Colors.grey.withOpacity(0.7),
                            value: remember,
                            onChanged: (value) {
                              setState(() {
                                remember = value;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.width * 0.3,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      decoration: BoxDecoration(
                          color: GlobalParams.themes[banque_id].intituleCmpColor,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            color: GlobalParams.themes[banque_id].appBarColor,
                          ),
                          isExpanded: true,
                          hint: Text(
                            AppLocalizations.of(context).langue,
                            style: TextStyle(
                                fontSize: 14
                            ),
                          ),
                          style: GoogleFonts.roboto(
                              textStyle:
                              TextStyle(color: Colors.black45, fontSize: 16)),
                          value: dropdownType,
                          items: <DropdownMenuItem>[
                            DropdownMenuItem(
                              value: 'ar',
                              child: Text(
                                AppLocalizations.of(context).ar,
                                style: TextStyle(
                                  color: GlobalParams.themes[banque_id].appBarColor,
                                ),
                              ),
                            ),
                            DropdownMenuItem(
                              value: 'fr',
                              child: Text(
                                AppLocalizations.of(context).fr,
                                style: TextStyle(
                                  color: GlobalParams.themes[banque_id].appBarColor,
                                ),
                              ),
                            ),
                            DropdownMenuItem(
                              value: 'en',
                              child: Text(
                                AppLocalizations.of(context).en,
                                style: TextStyle(
                                  color: GlobalParams.themes[banque_id].appBarColor,
                                ),
                              ),
                            ),
                          ],
                          onChanged: (value) {
                            setState(() {
                              dropdownType = value;
                              MyApp.of(context).setLocale(Locale.fromSubtags(languageCode: '$value'));
                            });
                          },
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          alignment: Alignment.centerRight,
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text("Wallet",
                                maxLines: 1,
                                textAlign: TextAlign.end,
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                )),
                          ),
                        ),
                        Transform.translate(
                          offset: Offset(8, 0),
                          child: Switch(
                            activeColor: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            activeTrackColor: Colors.grey.withOpacity(0.7),
                            value: isWallet,
                            onChanged: (value) {
                              setState(() {
                                isWallet = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.12,
                ),
                psd == null
                    ? Container()
                    :
                    // local Auth
                    FutureBuilder<bool>(
                        future: hasBiometrics(),
                        builder: (context, snapshot) {
                          return snapshot.data == true
                              ? Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.06,
                                  width: double.infinity,
                                  child: RaisedButton.icon(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      color: Color.fromRGBO(204, 214, 44, 1),
                                      elevation: 0,
                                      onPressed: () async {
                                        final isAuthenticated =
                                            await authenticate();
                                        if (isAuthenticated) {
                                          context.read<TokenBloc>().add(
                                              LoginEvent(
                                                  userName:
                                                      userNameController.text,
                                                  password: psd));
                                        }
                                      },
                                      icon: Icon(
                                        Icons.fingerprint,
                                        size: 35,
                                        color: banque_id == 'ADRIA'
                                            ? GlobalParams.themes["$banque_id"]
                                                .intituleCmpColor
                                            : GlobalParams.themes["$banque_id"]
                                                .appBarColor,
                                      ),
                                      label: Text(
                                        AppLocalizations.of(context)
                                            .emprinte_digitale,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontSize: 16,
                                                color: banque_id == 'ADRIA'
                                                    ? GlobalParams
                                                        .themes["$banque_id"]
                                                        .intituleCmpColor
                                                    : GlobalParams
                                                        .themes["$banque_id"]
                                                        .appBarColor)),
                                      )),
                                )
                              : Container();
                        },
                      ),
                BlocListener<TokenBloc, TokenStateBloc>(
                  listener: (context, state) {
                    if (state.requestState == StateStatus.LOADING) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        showLoading(context, dismiss: true);
                      });
                    } else if (state.requestState == StateStatus.LOADED) {
                      // changer la valeur de cette variable qui va etre utilisé pour s'assurer que l'utilisateur est connecté
                      appData.isLogout = false;

                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DashboardComptePage()));

                        passwordController.text = "";
                      });
                    } else if (state.requestState == StateStatus.ERROR) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        Navigator.pop(context);
                        errorAlert(context, "Invalid user credential");

                        passwordController.text = "";
                      });
                    } else {
                      return Container();
                    }
                  },
                  child: Container(),
                ),
                BlocListener<WalletBloc, WalletState>(
                  listener: (context, state) {
                    print(state.requestStateEnterEnRelation);
                    print(eerLoaded);
                    if (state.requestStateEnterEnRelationLogin == StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateEnterEnRelationLogin ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStateEnterEnRelationLogin ==
                        StateStatus.LOADED &&
                        !eerLoaded) {

                      Navigator.pop(context);
                      eerLoaded = true;
                      context.read<WalletBloc>().add(LoginDPEvent(id: '0669872784000', psd: 'YjBiYzQ1ODFkM2ZlY2I3NTA2NDhiODAxODM2ZWVhOTM6Ojk4YjRmZGI2MTlkMGJmZTUxOTc0ZDY4N2Y4YmFiYzcwOjpHd0h5MDgzeDNxaks5UHQzL2hlNE9RPT0='));
                    } else if (state.requestStateLoginDP ==
                        StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateLoginDP ==
                        StateStatus.ERROR) {
                      eerLoaded = false;
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStateLoginDP ==
                        StateStatus.LOADED) {
                      eerLoaded = false;
                      // okAlert(context, "Success Wallet login");
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        Navigator.pop(context);
                        // Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WalletPage()));

                        passwordController.text = "";
                      });
                      // Navigator.pop(context);
                    }
                  },
                  child: Container(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future showClaVir(BuildContext context, List<int> keyboard) {
    return showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return Directionality(
            textDirection: TextDirection.ltr,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.43,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                  )),
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                    40, MediaQuery.of(context).size.height * 0.03, 15, 0),
                child: Stack(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      color: Colors.white,
                      //height: MediaQuery.of(context).size.height * 0.43,
                      child: GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: 12,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 1.5,
                            crossAxisSpacing: 4,
                            mainAxisSpacing: 4,
                          ),
                          itemBuilder: (_, index) {
                            return Stack(
                              children: [
                                index == 9
                                    ? Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: RaisedButton(
                                          color: Colors.white,
                                          elevation: 0,
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Icon(
                                            Icons.arrow_drop_down,
                                            size: 40,
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .otherButtonColor,
                                          ),
                                        ),
                                      )
                                    : index == 11
                                        ? Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: RaisedButton(
                                              color: Colors.white,
                                              elevation: 0,
                                              onPressed: () {
                                                passwordController.text =
                                                    passwordController.text
                                                        .substring(
                                                            0,
                                                            passwordController
                                                                    .text.length -
                                                                1);
                                              },
                                              child: Icon(
                                                Icons.arrow_back,
                                                size: 35,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .otherButtonColor,
                                              ),
                                            ),
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: RaisedButton(
                                              color: Colors.white,
                                              elevation: 0,
                                              onPressed: () {
                                                passwordController.text +=
                                                    "${index == 10 ? keyboard[index - 1] : keyboard[index]}";
                                              },
                                              child: Text(
                                                "${index == 10 ? keyboard[index - 1] : keyboard[index]}",
                                                style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        fontSize: 30,
                                                        color: GlobalParams
                                                            .themes["$banque_id"]
                                                            .otherButtonColor)),
                                              ),
                                            ))
                              ],
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

forgetPsd(context, idController, emController, key) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Center(
                child: Text("Mot de passe oublié",
                    style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(color: principaleColor5, fontSize: 20),
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    )),
              ),
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Form(
                key: key,
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 8, 8, 10),
                          child: Text(
                            "Veuillez saisir votre identifiant :",
                            style: TextStyle(color: Colors.grey[600]),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              controller: idController,
                              cursorColor: principaleColor5,
                              validator: (v) {
                                if (v.isEmpty)
                                  return "Identifiant ne peut pas etre vide";
                                return null;
                              },
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                    left: 10.0, top: 15.0, bottom: 15.0),
                                hintText: 'Identifiant',
                                hintStyle: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        fontFamily: KprimaryFont)),
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: GlobalParams
                                      .themes["$banque_id"].iconsColor,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color.fromRGBO(89, 43, 95, 0.35)),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            Color.fromRGBO(89, 43, 95, 0.35)),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              RaisedButton(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  child: Text(
                                    'Annuler',
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    )),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                              RaisedButton(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: BorderSide(
                                          color: GlobalParams
                                              .themes["$banque_id"]
                                              .intituleCmpColor)),
                                  color: Colors.white,
                                  child: Text(
                                    'Confirmer',
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 16,
                                    )),
                                  ),
                                  onPressed: () {
                                    if (key.currentState.validate()) {
                                      Navigator.pop(context);
                                      idController.text = "";
                                      errorAlert(context,
                                          "Un problème est survenu. réessayez plus tard.");
                                    }
                                  }),
                            ],
                          ),
                        ),
                        BlocListener<TokenBloc, TokenStateBloc>(
                          listener: (context, state) {
                            if (state.requestStatePsd == StateStatus.LOADING) {
                              WidgetsBinding.instance.addPostFrameCallback((_) {
                                return showLoading(context);
                              });
                              return Container();
                            } else if (state.requestStatePsd ==
                                StateStatus.LOADED) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              idController.text = "";
                              okAlert(
                                  context,
                                  state.res["message"] == null
                                      ? "Votre nouveau mot de passe à été envoyé avec succès, vérifier votre boite mail"
                                      : state.res["message"]);
                            } else if (state.requestStatePsd ==
                                StateStatus.ERROR) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              idController.text = "";

                              errorAlert(
                                  context,
                                  state.errorMessage == null
                                      ? "Un problème est survenu. réessayez plus tard."
                                      : state.errorMessage);
                            } else {
                              return Container();
                            }
                          },
                          child: Container(),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      });
}
