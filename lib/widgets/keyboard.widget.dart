import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';

class KeyboardWidget extends StatelessWidget {
  // final VoidCallback onButtonClicked;
  final Function(String) onButtonClicked;
  final Function() onDeleteClicked;
  final Function() onOkClicked;
  KeyboardWidget(
      {this.onButtonClicked, this.onDeleteClicked, this.onOkClicked});

// final Function(int) onButtonClick;
  @override
  Widget build(BuildContext context) {
    var keyboard = new List<int>.generate(10, (int index) => index);
    keyboard.shuffle();
    // print(keyboard);
    return Container(
      decoration: BoxDecoration(
        color: GlobalParams.themes["$banque_id"].bgKeyboard,
        //  banque_id == "ADRIA"
        //     ? Color.fromRGBO(253, 92, 0, 0.3)
        //     : Color.fromRGBO(89, 43, 95, 0.3)
      ),
      margin: EdgeInsets.only(bottom: 4), 
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.1,
            // width: MediaQuery.of(context).size.width * 0.6,
            child: GridView.builder(
                itemCount: keyboard.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5),
                itemBuilder: (_, index) {
                  return Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: RaisedButton(
                        color: Colors.white,
                        onPressed: () {
                          onButtonClicked("${keyboard[index]}");
                          // print("${keyboard[index]}");
                        },
                        child: Text("${keyboard[index]}")),
                  );
                }),
          ),
          Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: ListTile(
                leading: RaisedButton(
                  // color: principaleColor1,
                  color: Colors.white,

                  onPressed: () {
                    onOkClicked();
                  },
                  child:
                      //  Text("Confirmer",
                      //     style: GoogleFonts.roboto(
                      //         textStyle: TextStyle(
                      //             color: principaleColor5,
                      //             fontWeight: FontWeight.w600)))
                      Icon(
                    Icons.check_box_outlined,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
                trailing: RaisedButton(
                  // color: principaleColor1,
                  color: Colors.white,

                  onPressed: () {
                    onDeleteClicked();
                  },
                  child: Icon(
                    Icons.backspace_outlined,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
