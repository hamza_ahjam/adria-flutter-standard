import 'dart:async';

import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class ChangePassword extends StatefulWidget {
  // CompteState compteState;
  // TokenState tokenState;
  ChangePassword() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
  }

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  GlobalKey<FormState> _formKeyForgetPsd = GlobalKey<FormState>();

  TextEditingController oldPassword = TextEditingController();

  TextEditingController newPassword = TextEditingController();

  TextEditingController confiNewPassword = TextEditingController();

  bool showPsd = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.05,
            left: MediaQuery.of(context).size.width * 0.1,
            right: MediaQuery.of(context).size.width * 0.1,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width * 0.03),
                height: MediaQuery.of(context).size.height * 0.062,
                width: MediaQuery.of(context).size.width * 0.35,
                child: RaisedButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DashboardComptePage()));
                    },
                    color: Colors.white,
                    child: Text(AppLocalizations.of(context).annuler,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                fontSize: 17)))),
              ),
              Container(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.03),
                height: MediaQuery.of(context).size.height * 0.062,
                width: MediaQuery.of(context).size.width * 0.35,
                child: RaisedButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      if (_formKeyForgetPsd.currentState.validate()) {
                        showLoading(context);

                        context.read<CompteBloc>().add(ChangePasswordEvent(
                            oldPassword.text,
                            newPassword.text,
                            confiNewPassword.text));
                      }
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).valider,
                        style: GoogleFonts.roboto(
                            textStyle:
                                TextStyle(color: Colors.white, fontSize: 17)))),
              )
            ],
          ),
        ),
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).changement_mp.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()))),
        ),
        body: Center(
          child: Container(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.12),
            width: MediaQuery.of(context).size.width * 0.8,
            child: Form(
              key: _formKeyForgetPsd,
              child: ListView(
                children: [
                  TextFormField(
                    controller: oldPassword,
                    textAlignVertical: TextAlignVertical.bottom,
                    cursorColor:
                        GlobalParams.themes["$banque_id"].appBarColor,
                    // onSaved: (v) {
                    //   setState(() => {identifiant = v});
                    // },
                    validator: (v) {
                      if (v.isEmpty) return AppLocalizations.of(context).ancien_mp;
                      if (v.length < 6)
                        return AppLocalizations.of(context).mp_error_length;
                      return null;
                    },
                    obscureText: !showPsd ? true : false,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).ancien_mp,
                        isDense: true,
                      hintStyle: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 16)),
                      suffixIcon: Icon(Icons.remove_red_eye,
                          color:
                              GlobalParams.themes["$banque_id"].iconsColor),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10))),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(89, 43, 95, 0.35)),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10))),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromRGBO(89, 43, 95, 0.35)),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10))),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.04),
                    child: TextFormField(
                      controller: newPassword,
                      textAlignVertical: TextAlignVertical.bottom,
                      cursorColor:
                          GlobalParams.themes["$banque_id"].appBarColor,
                      // onSaved: (v) {
                      //   setState(() => {identifiant = v});
                      // },
                      validator: (v) {
                        if (v.isEmpty) return AppLocalizations.of(context).nv_mp;
                        if (v.length < 6)
                          return AppLocalizations.of(context).mp_error_length;

                        return null;
                      },
                      obscureText: !showPsd ? true : false,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: AppLocalizations.of(context).nv_mp,
                        isDense: true,
                        hintStyle: GoogleFonts.roboto(
                            textStyle: TextStyle(fontSize: 16)),
                        suffixIcon: Icon(Icons.remove_red_eye,
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(89, 43, 95, 0.35)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(89, 43, 95, 0.35)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.04),
                    child: TextFormField(
                      controller: confiNewPassword,
                      textAlignVertical: TextAlignVertical.bottom,
                      cursorColor:
                          GlobalParams.themes["$banque_id"].appBarColor,
                      // onSaved: (v) {
                      //   setState(() => {identifiant = v});
                      // },
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).confirmer_mp;
                        if (v.length < 6)
                          return AppLocalizations.of(context).mp_error_length;
                        if (v != newPassword.text)
                          return AppLocalizations.of(context).error_mp_conforme;
                        return null;
                      },
                      obscureText: !showPsd ? true : false,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: AppLocalizations.of(context).confirmer_mp,
                        isDense: true,
                        hintStyle: GoogleFonts.roboto(
                            textStyle: TextStyle(fontSize: 16)),
                        suffixIcon: Icon(Icons.remove_red_eye,
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(89, 43, 95, 0.35)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(89, 43, 95, 0.35)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        showPsd = !showPsd;
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.04),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            alignment: MyApp.of(context).getLocale().languageCode == 'ar'
                                ? Alignment.centerRight
                                : Alignment.centerLeft,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                AppLocalizations.of(context).afficher_mp,
                                maxLines: 1,
                                style: GoogleFonts.roboto(fontSize: 16),
                              ),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(
                                MyApp.of(context).getLocale().languageCode == 'ar' ? -13 : 13
                                , 0),
                            child: Transform.scale(
                              scale: 0.55,
                              child: CupertinoSwitch(
                                activeColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                value: showPsd,
                                onChanged: (value) {
                                  setState(() {
                                    showPsd = value;
                                  });
                                },
                              ),
                            ),
                          ),
                          // Icon(
                          //   !showPsd
                          //       ? Icons.lock_outline
                          //       : Icons.lock_open_rounded,
                          //   color: GlobalParams.themes["$banque_id"].iconsColor,
                          // )
                        ],
                      ),
                    ),
                  ),
                  BlocListener<CompteBloc, CompteStateBloc>(
                    listener: (context, state) {
                      if (state.requestStatePsd == StateStatus.LOADED) {
                        Navigator.pop(context);

                        okAlert(
                            context,
                            state.res["message"] == null
                                ? AppLocalizations.of(context).changement_mp_message
                                : state.res["message"]);
                        Future.delayed(Duration(seconds: 2), () {
                          // cs.initState();
                          // widget.tokenState.logout();
                          oldPassword.text = "";
                          newPassword.text = "";
                          confiNewPassword.text = "";
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()));
                        });
                      } else if (state.requestStatePsd == StateStatus.ERROR) {
                        Navigator.pop(context);

                        errorAlert(context, state.errorMessage);
                      }
                    },
                    child: Container(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
