import 'dart:convert';

import 'package:LBA/agences/agencies_service.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:url_launcher/url_launcher.dart';

class Footer extends StatefulWidget {
  const Footer({Key key}) : super(key: key);

  @override
  _FooterState createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  BannerAdsService get serviceAds => GetIt.I<BannerAdsService>();

  int currentPage = 0;
  var banners;

  var ban = [1, 2, 3];
  // @override
  // void initState() {
  //   super.initState();
  //   final block = Provider.of<BannerAdsState>(context, listen: false);

  //   WidgetsBinding.instance.addPostFrameCallback((_) async {
  //     if (block.getAds() != null) {
  //       setState(() {
  //         banners = block.getAds();
  //       });
  //     }
  //     getAds(block);
  //   });
  // }

  // getAds(block) async {
  //   print("start get ads");
  //   try {
  //     var res = await serviceAds.getAds();

  //     if (this.mounted) {
  //       setState(() {
  //         block.setAds(res['Map']['data']);
  //         banners = block.getAds();
  //       });
  //     }
  //   } catch (e) {
  //     print(" can't get ads : $e");
  //   }
  //   print("get ads");
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.18,
      width: double.infinity,
      color: Colors.grey[100],
      child: Column(
        children: <Widget>[
          banners != null
              ? Expanded(
                  child: PageView.builder(
                    // scrollDirection: Axis.horizontal,
                    onPageChanged: (value) {
                      setState(() {
                        currentPage = value;
                      });
                    },
                    itemCount: banners.length,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () async {
                        var url = banners[index]["lien"];
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          print('Could not launch $url');
                        }
                        print('$url');
                      },
                      child: Image.memory(
                        base64Decode(
                          banners[index]["image"],
                        ),
                      ),
                    ),
                  ),
                )
              : Expanded(
                  child: CarouselSlider(
                    options: CarouselOptions(
                      height: 400.0,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enableInfiniteScroll: true,
                      onPageChanged: (page, CarouselPageChangedReason) {
                        setState(() {
                          currentPage = page;
                        });
                      },
                    ),
                    items: ban.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              // decoration: BoxDecoration(color: Colors.amber),
                              child: Image.asset(
                                "assets/$banque_id/images/banner1.png",
                                fit: BoxFit.fill,
                                // height: MediaQuery.of(context).size.height * 0.12,
                              ));
                        },
                      );
                    }).toList(),
                  ),
                  // child: Image.asset(
                  //   "assets/images/Banner.png", fit: BoxFit.fill,
                  //   // height: MediaQuery.of(context).size.height * 0.12,
                  // ),
                ),
          Wrap(children: <Widget>[
            Padding(
                padding: EdgeInsets.all(0),
                child: DotsIndicator(
                  // dotsCount: ban.length,
                  dotsCount: ban.length,
                  // dotsCount: banners != null ? banners.length : 1,
                  position: currentPage.toDouble(),
                  decorator: DotsDecorator(
                    color: Color.fromRGBO(89, 43, 95, 0.4),
                    activeColor: GlobalParams.themes["$banque_id"].appBarColor,
                    size: const Size.square(9.0),
                    activeSize: const Size(18.0, 9.0),
                    activeShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                  ),
                )),
          ]),
        ],
      ),
    );
  }
}
