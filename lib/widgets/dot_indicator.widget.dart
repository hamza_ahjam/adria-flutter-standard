import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

class DotIndicatorWidget extends StatefulWidget {
  var lenght;
  var page;
  DotIndicatorWidget({Key key, @required this.lenght, @required this.page})
      : super(key: key);

  @override
  _DotIndicatorWidgetState createState() => _DotIndicatorWidgetState();
}

class _DotIndicatorWidgetState extends State<DotIndicatorWidget> {
  @override
  Widget build(BuildContext context) {
    return DotsIndicator(
        dotsCount: widget.lenght,
        position: widget.page.toDouble(),
        decorator: DotsDecorator(
          activeColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
          size: const Size.square(9.0),
          activeSize: const Size(18.0, 9.0),
          activeShape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        ),
    );
  }
}
