

/*
Cette classe est accecible à partir des diffrentes widgets du projet.
Il permet de sauvgarder l'état de l'utilisateur (connecté ou non connecté)
 */
class AppData {
  static final AppData _appData = new AppData._internal();

  bool isLogout = false;

  factory AppData() {
    return _appData;
  }
  AppData._internal();
}
final appData = AppData();