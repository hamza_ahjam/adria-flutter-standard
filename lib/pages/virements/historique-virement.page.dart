import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/virements/widgets/historique-beneficiaire.widget.dart';
import 'package:LBA/pages/virements/widgets/historique-compte.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class HistoriqueVirementPage extends StatefulWidget {
  // TokenState tokenState;
  // CompteState compteState;
  bool isComingFromMenu = false;
  int index;
  HistoriqueVirementPage({this.isComingFromMenu, this.index = 0}) {
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState = Provider.of<CompteState>(context, listen: false);
    // compteState.loadHistoriqueVirement(tokenState, 1);
    // compteState.loadHistoriqueCompte(tokenState);
  }

  @override
  _HistoriqueVirementPageState createState() => _HistoriqueVirementPageState();
}

class _HistoriqueVirementPageState extends State<HistoriqueVirementPage> with SingleTickerProviderStateMixin{
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 2);

    _tabController.index = widget.index;
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   // context
    //   //     .read<HistoriqueVirementBloc>()
    //   //     .add(LoadHistoriqueVirementEvent(page: page));

    //   // context
    //   //     .read<HistoriqueVirementCompteBloc>()
    //   //     .add(LoadHistoriqueVirementCompteEvent(page: page));
    // });
    return DefaultTabController(
      length: 2,
      child: WillPopScope(
        onWillPop: (){
          moveToDashboard(context, widget.isComingFromMenu);
          return Future.value(true);
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: OnlineAppBarWidget(
            appBarTitle: AppLocalizations.of(context).mes_virements,
            context: context),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  // bottomLeft: Radius.circular(40.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: const Offset(
                      0.0,
                      0.0,
                    ),
                    blurRadius: 10,
                    spreadRadius: 1,
                  ),
                ]),
            padding: widget.isComingFromMenu ? EdgeInsets.only(top: 0) : EdgeInsets.only(top: 2),
            height: widget.isComingFromMenu ? MediaQuery.of(context).size.height * 0.07 : 113,
            alignment: widget.isComingFromMenu ? Alignment.center : Alignment.topCenter,
            child: TabBar(
              controller: _tabController,
                indicatorColor: Colors.transparent,
                labelColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
                unselectedLabelColor:
                    GlobalParams.themes["$banque_id"].appBarColor,
                onTap: (v) {
                  setState(() {
                    widget.index = v;
                  });
                },
                tabs: [
                  Tab(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).vir_benef,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 0
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 0
                              ? GlobalParams.themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                  Tab(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).vir_cac,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 1
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 1
                              ? GlobalParams.themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ]),
          ),
          body: Stack(
            children: <Widget>[
              Container(
                  child: TabBarView(
                    controller: _tabController,
                    physics: NeverScrollableScrollPhysics(),
                    children: <Widget>[
                      Container(child: HistoriqueBeneciaire(isFromMenu: widget.isComingFromMenu,)),
                      Container(
                        child: HistoriqueCompteWidget(isFromMenu: widget.isComingFromMenu,),
                      )
                    ],
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}
