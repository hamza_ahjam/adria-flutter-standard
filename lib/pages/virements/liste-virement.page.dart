import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/Beneficiaire/AddBeneficiaire.widget.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/virements/virement_navigation.widget.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ListeVirementPage extends StatelessWidget {
  // BeneficiaireState beneficiaireState;
  // TokenState tokenState;
  bool isComingFromMenu = false;
  ListeVirementPage({this.isComingFromMenu}) {
    // beneficiaireState = Provider.of<BeneficiaireState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // if (beneficiaireState.beneficiaire == null)
    //   beneficiaireState.loadBeneficiaire(tokenState);
  }

//   @override
//   _ListeVirementPageState createState() => _ListeVirementPageState();
// }

// class _ListeVirementPageState extends State<ListeVirementPage> {
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  CompteService get service => GetIt.I<CompteService>();

  final _formKey = GlobalKey<FormState>();
  final ScrollController _scrollController = ScrollController();

  var listBeneficaire;

  bool load = false;

  var deleteRes;
  bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    // print(load);

    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, isComingFromMenu);
        return Future.value(true);
      },
      child: Scaffold(
          // key: _scaffoldKey,
          resizeToAvoidBottomInset: false,
          // drawer: OnlineDrawer(context: context),
          // bottomNavigationBar: Container(
          //     color: Color(0xFFE6543F),
          //     height: 70,
          //     child: RaisedButton.icon(
          //         onPressed: () {
          //           // showAddBenifForm(
          //           //     tokenState,
          //           //     context,
          //           //     tokenState.token.accessToken,
          //           //     tokenState.getUserDetails());
          //           Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                   builder: (context) => AddBeneficiaireWidget()));

          //           // showModalBottomSheet(
          //           //     shape: RoundedRectangleBorder(
          //           //       borderRadius: BorderRadius.only(
          //           //         topLeft: Radius.circular(40),
          //           //       ),
          //           //     ),
          //           //     isDismissible: true,
          //           //     context: context,
          //           //     builder: (context) {
          //           //       return AddBeneficiaireWidget();
          //           //     });
          //         },
          //         color: GlobalParams.themes["$banque_id"].intituleCmpColor,
          //         icon: Icon(
          //           Icons.person_add_alt_1_sharp,
          //           color: Colors.white,
          //         ),
          //         // icon: Image.asset(
          //         //   "assets/images/bénéficiaires.png",
          //         //   color: Colors.white,
          //         //   width: 50,
          //         //   height: 50,
          //         // ),
          //         label: Text("Ajouter bénéficiaire",
          //             style: GoogleFonts.roboto(
          //                 textStyle:
          //                     TextStyle(color: Colors.white, fontSize: 16))))),
          appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            shape: ContinuousRectangleBorder(
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(60))),
            toolbarHeight: MediaQuery.of(context).size.height * 0.085,
            title: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                AppLocalizations.of(context).benefs.toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                )),
              ),
            ),
            centerTitle: true,
            backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.white,
                ),
                onPressed: () => isComingFromMenu
                    ? Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DashboardComptePage()))
                    : Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => VirementNavigationPage()))),
          ),
          backgroundColor: Colors.white,
          floatingActionButton: FloatingActionButton(
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddBeneficiaireWidget(
                          isComingFromMenu: isComingFromMenu,
                        ))),
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            elevation: 4,
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
          ),
          body: BlocConsumer<BeneficiaireBloc, BeneficiaireStateBloc>(
              builder: (context, state) {
            if (state.requestState == StateStatus.NONE) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<BeneficiaireBloc>().add(LoadBeneficiaire());
              });
              return Container();
            } else if (state.requestState == StateStatus.LOADING) {
              return Center(
                  child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
            } else if (state.requestState == StateStatus.ERROR) {
              return ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<BeneficiaireBloc>().add(state.currentAction);
                },
              );
            } else if (state.requestState == StateStatus.LOADED) {
              isLoaded = true;
              if (state.beneficiaire.map.beneficiaireList.length == 0)
                return Center(
                  child: Text(
                    AppLocalizations.of(context).aucun_benef,
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(fontSize: 16, color: principaleColor5)),
                  ),
                );
              else
                return Padding(
                    padding: const EdgeInsets.fromLTRB(10, 30, 10, 15),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          // Padding(
                          //   padding: const EdgeInsets.fromLTRB(0, 10, 10, 15),
                          //   child: Container(
                          //     color: Colors.grey[100],
                          //     child: TextFormField(
                          //       style: TextStyle(color: Colors.black),
                          //       decoration: InputDecoration(
                          //         hintText: 'Rechercher un bénéficiaire',
                          //         hintStyle: TextStyle(fontSize: 16, color: Colors.grey),
                          //         border: OutlineInputBorder(),
                          //         suffixIcon: Icon(
                          //           Icons.search,
                          //           color: Colors.grey,
                          //         ),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(AppLocalizations.of(context).liste.toUpperCase(),
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(AppLocalizations.of(context).benefs.toUpperCase(),
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ))),
                          ),
                          Container(
                              height: MediaQuery.of(context).size.height * 0.7,
                              padding: EdgeInsets.only(top: 5),
                              color: Colors.white,
                              child: ListView.separated(
                                controller: _scrollController,
                                shrinkWrap: true,
                                separatorBuilder: (context, index) => ListTile(
                                  visualDensity: VisualDensity(
                                      horizontal: -4, vertical: -4),
                                  leading: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  trailing: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                ),
                                itemCount: state
                                    .beneficiaire.map.beneficiaireList.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.07,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.7,
                                            child: ListTile(
                                              title: Text(
                                                  "${state.beneficiaire.map.beneficiaireList[index].intitule ?? ""}",
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight: FontWeight
                                                              .bold))),
                                              subtitle: Text(
                                                  "RIB : ${state.beneficiaire.map.beneficiaireList[index].numeroCompte}",
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                    fontSize: 14,
                                                  ))),
                                              // Divider(
                                              //   color: Colors.black,
                                              // ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 6, 0, 6),
                                            child: GestureDetector(
                                                onTap: () {
                                                  updateBenef(
                                                      context,
                                                      state.beneficiaire.map
                                                              .beneficiaireList[
                                                          index]);
                                                },
                                                child: Icon(
                                                  Icons.mode_edit,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor,
                                                )),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 6, 6, 6),
                                            child: GestureDetector(
                                                onTap: () {
                                                  deleteBenif(
                                                      state
                                                          .beneficiaire
                                                          .map
                                                          .beneficiaireList[
                                                              index]
                                                          .id,
                                                      state
                                                          .beneficiaire
                                                          .map
                                                          .beneficiaireList[
                                                              index]
                                                          .nature,
                                                      context);
                                                },
                                                child: Icon(
                                                  Icons.delete,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor,
                                                )),
                                          ),
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              )
                              // : Center(
                              //     child: CircularProgressIndicator(
                              //     backgroundColor: principaleColor5,
                              //     valueColor: AlwaysStoppedAnimation<Color>(
                              //         principaleColor6),
                              //   ))),
                              )
                        ],
                      ),
                    ));
            } else {
              return Container();
            }
          }, listener: (context, state) {
            if (state.requestStateDelete == StateStatus.LOADING) {
              showLoading(context);
            } else if (state.requestStateDelete == StateStatus.ERROR) {
              Navigator.pop(context);
              Navigator.pop(context);
              errorAlert(
                  context,
                  state.errorMessage == null
                      ? "Erreur technique"
                      : state.errorMessage);
            } else if (state.requestStateDelete == StateStatus.LOADED) {
              Navigator.pop(context);
              Navigator.pop(context);
              okAlert(
                  context,
                  state.res["message"] == null
                      ? "Bénéficiaire est supprimé avec succès"
                      : state.res["message"]);

              context.read<BeneficiaireBloc>().add(LoadBeneficiaire());
            } else {
              return Container();
            }
          })),
    );
  }

  updateBenef(context, benef) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: Icon(
                      Icons.edit,
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    ),
                  ),
                  Text(
                    "Modifier bénéficiaire".toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: TextFormField(
                    initialValue: benef.intitule,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintStyle:
                          TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: TextFormField(
                    initialValue: benef.numeroCompte,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintStyle:
                          TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor)),
                      color: Colors.white,
                      onPressed: () => Navigator.pop(context),
                      child: Text("Annuler",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  fontSize: 17))),
                    ),
                    RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {},
                        child: Text("modifier",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            )))),
                  ],
                )
              ],
            ),
          );
        });
  }

  deleteBenif(id, nature, context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 5, 8, 5),
                    child: Icon(
                      Icons.dangerous,
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    ),
                  ),
                  Text("Suppression".toUpperCase(),
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontSize: 18,
                              fontWeight: FontWeight.bold))),
                ],
              )),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Text("Voulez-vous supprimer ce bénéficiaire ?",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 16))),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)),
                    color: Colors.white,
                    onPressed: () => Navigator.pop(context),
                    child: Text("Annuler",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 17,
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor))),
                  ),
                  RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      onPressed: () {
                        context.read<BeneficiaireBloc>().add(
                            DeleteBeneficiaireEvent(id: id, nature: nature));
                      },
                      child: Text("Supprimer",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          )))),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
