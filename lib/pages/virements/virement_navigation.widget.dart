import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/Beneficiaire/AddBeneficiaire.widget.dart';
import 'package:LBA/pages/virements/historique-virement.page.dart';
import 'package:LBA/pages/virements/liste-virement.page.dart';
import 'package:LBA/pages/virements/virement.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class VirementNavigationPage extends StatefulWidget {
  const VirementNavigationPage({Key key}) : super(key: key);

  @override
  _VirementNavigationPageState createState() => _VirementNavigationPageState();
}

class _VirementNavigationPageState extends State<VirementNavigationPage> {
  int _currentIndex = 3;

  final tabs = [
    AddBeneficiaireWidget(
      isComingFromMenu: false,
    ),
    ListeVirementPage(
      isComingFromMenu: false,
    ),
    HistoriqueVirementPage(
      isComingFromMenu: false,
    ),
    VirementPage(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: _currentIndex == 2 ?? true,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      bottomNavigationBar: Container(
        height: 65,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(50)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: const Offset(
                  -15.0,
                  0.0,
                ),
                blurRadius: 10,
                spreadRadius: 1,
              ),
            ]),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            selectedFontSize: 0,
            items: [
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.005),
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/vir_benef.svg",
                      fit: BoxFit.fill,
                      color: _currentIndex == 0
                          ? GlobalParams.themes["$banque_id"].intituleCmpColor
                          : GlobalParams.themes["$banque_id"].appBarColor,
                    ),
                  ),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.005),
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/liste_benef.svg",
                      fit: BoxFit.fill,
                      color: _currentIndex == 1
                          ? GlobalParams.themes["$banque_id"].intituleCmpColor
                          : GlobalParams.themes["$banque_id"].appBarColor,
                    ),
                  ),
                  label: ""),
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.005),
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/historique_vir.svg",
                      fit: BoxFit.fill,
                      color: _currentIndex == 2
                          ? GlobalParams.themes["$banque_id"].intituleCmpColor
                          : GlobalParams.themes["$banque_id"].appBarColor,
                    ),
                  ),
                  label: ""),
            ].toList(),
            // selectedItemColor:
            //     GlobalParams.themes["$banque_id"].intituleCmpColor,
            // unselectedItemColor: GlobalParams.themes["$banque_id"].appBarColor,
            backgroundColor: Colors.white,
            elevation: 4.0,
            onTap: (int idx) {
              setState(() {
                _currentIndex = idx;
              });
            },
          ),
        ),
      ),
      body: tabs[_currentIndex],
    );
  }
}
