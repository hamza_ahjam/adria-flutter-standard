import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../main.dart';

class VirementDetail extends StatelessWidget {
  final dynamic  ref, compte, benef, rib, montant, motif, date, status;
  VirementDetail(
      {this.ref,
      this.compte,
      this.benef,
      this.rib,
      this.montant,
      this.motif,
      this.date,
      this.status});
  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp
        .of(context)
        .getLocale()
        .languageCode;

    return Scaffold(
      appBar: AppBar(
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(80))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          onPressed: () => Navigator.pop(context),),
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(AppLocalizations.of(context).detail_vir.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ))),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
      ),
      body: Center(
        child: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width * 0.9,
          height: MediaQuery.of(context).size.height * 0.9,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: <Widget>[
                ListTile(
                  leading: Container(
                    width: 40,
                    height: 40,
                    color: Colors.grey[300],
                    child: Image.asset(
                      "assets/$banque_id/images/virement.png",
                      width: 10,
                      height: 10,
                    ),
                  ),
                  title: Text(
                    "${AppLocalizations.of(context).num_ref} : $ref",
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(color: Colors.grey[700], fontSize: 15)),
                  ),
                ),
                // ListTile(
                //   title: Text(
                //     "Type de virement",
                //     style: GoogleFonts.roboto(
                //         textStyle:
                //             TextStyle(color: Colors.grey[700], fontSize: 15)),
                //   ),
                //   trailing: Text(
                //     "Simple",
                //     style: GoogleFonts.roboto(
                //         textStyle:
                //             TextStyle(color: Colors.grey[700], fontSize: 15)),
                //   ),
                // ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "${AppLocalizations.of(context).num_compte_em} :",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700], fontSize: 15)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "$compte",
                            textAlign: TextAlign.end,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700], fontSize: 15)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).benef} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "$benef",
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.end,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).rib} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "$rib",
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.end,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).montant} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "$montant",
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.end,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    //mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).motif} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "$motif",
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.clip,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).date_exec} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "${DateFormat.yMMMMd(languageCode).format(DateFormat("dd-MM-yyyy").parse(date))}",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${AppLocalizations.of(context).statut} :",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700], fontSize: 15)),
                      ),
                      Expanded(
                        child: Text(
                          "$status",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.grey[700], fontSize: 15)),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
