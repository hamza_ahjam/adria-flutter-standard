import 'dart:ui';

import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.bloc.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.bloc.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.event.dart';
import 'package:LBA/bloc/virement/virement.bloc.dart';
import 'package:LBA/bloc/virement/virement.event.dart';
import 'package:LBA/bloc/virement/virement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/compte.model.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/virements/historique-virement.page.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/virement.service.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';

class VirementPage extends StatefulWidget {
  // CompteState compteState;
  // TokenState tokenState;
  // BeneficiaireState beneficiaireState;

  VirementPage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // beneficiaireState = Provider.of<BeneficiaireState>(context, listen: false);
    // if (beneficiaireState.beneficiaire == null)
    //   beneficiaireState.loadBeneficiaire(tokenState);
    // beneficiaireState.loadPeriodicite(tokenState);
  }

  @override
  _VirementPageState createState() => _VirementPageState();
}

class _VirementPageState extends State<VirementPage> {
  CompteService get service => GetIt.I<CompteService>();
  VirementService get serviceVir => GetIt.I<VirementService>();
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKeyVir = GlobalKey<FormState>();
  final _formPsdKey = GlobalKey<FormState>();
  bool permanent = false;
  // var compteslist = List<DropdownMenuItem>();
  String dropdownValue;
  bool load = false;
  List compteslist = [];
  var compte;
  var listBeneficaire;
  var benif;
  var cmpt;
  bool isBenifSelected = false;
  var selectedAccount;
  var accessToken;
  var user;
  var virementRes;
  // var beneficiare;
  DateFormat _dateFormat = DateFormat.yMd('fr_FR');
  TextEditingController mycontroller = TextEditingController();
  TextEditingController mycontrollerFinDate = TextEditingController();
  DateTime dateVirement = DateTime.now();
  DateTime dateFinVirement = DateTime.now();
  bool isDateSelected = false;
  bool isDateFinSelected = false;

  var compteDebiterController = TextEditingController();
  var beneficaireController = TextEditingController();

  TextEditingController montantController = TextEditingController();
  TextEditingController motifController = TextEditingController();
  var passwordController = TextEditingController();

  // String compteDebiter = "";
  // String beneficaire = "";
  String montant = "0";
  String motif = "";
  DateTime date = DateTime.now();
  DateTime datefin = DateTime.now();
  var virement;
  bool secureKeyboard = false;

  bool isLoaded = false;
  bool isBenefLoaded = false;
  bool isPeriodLoaded = false;

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    mycontroller.text = DateFormat('dd/MM/yyyy').format(DateTime.now());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIOverlays([]);

    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   context.read<CompteBloc>().add(LoadComptesVirEvent());
    //   context.read<BeneficiaireBloc>().add(LoadBeneficiaire());
    // });

    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        // key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        // drawer: OnlineDrawer(context: context),
        // bottomNavigationBar: Padding(
        //   padding: const EdgeInsets.fromLTRB(16, 8, 16, 20),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //     children: <Widget>[
        //       // Container(
        //       //   height: 50,
        //       //   width: MediaQuery.of(context).size.width * 0.4,
        //       //   child: RaisedButton(
        //       //       onPressed: () {
        //       //         Navigator.pop(context);
        //       //         Navigator.push(
        //       //             context,
        //       //             MaterialPageRoute(
        //       //                 builder: (context) => DashboardComptePage()));
        //       //       },
        //       //       color: Colors.white,
        //       //       child: Text("Annuler",
        //       //           style: GoogleFonts.roboto(
        //       //               textStyle: TextStyle(
        //       //                   color:
        //       //                       GlobalParams.themes["$banque_id"].appBarColor,
        //       //                   fontSize: 16)))),
        //       // ),
        //       Container(
        //         height: 50,
        //         width: MediaQuery.of(context).size.width * 0.7,
        //         child: RaisedButton(
        //             shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.circular(15.0),
        //             ),
        //             onPressed: () {
        //               if (_formKeyVir.currentState.validate()) {
        //                 // confirmVir();
        //                 permanent ? confirVirPerm() : confirmVir();
        //               }
        //             },
        //             color: GlobalParams.themes["$banque_id"].intituleCmpColor,
        //             child: Text("Valider",
        //                 style: GoogleFonts.roboto(
        //                     textStyle:
        //                         TextStyle(color: Colors.white, fontSize: 16)))),
        //       )
        //     ],
        //   ),
        // ),
        // bottomNavigationBar: BottomNavigationVirWidget(),
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).virements.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardComptePage()));
              }),
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.05,
                MediaQuery.of(context).size.height * 0.01,
                MediaQuery.of(context).size.width * 0.05,
                MediaQuery.of(context).size.height * 0.01,
              ),
              // color: Colors.white,
              // height: MediaQuery.of(context).size.height * 0.8,
              child: Form(
                key: _formKeyVir,
                child: ListView(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  controller: _scrollController,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding:
                            EdgeInsets.fromLTRB(20, permanent ? 0 : 25, 20, 20),
                        child: Column(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                WidgetsBinding.instance
                                    .addPostFrameCallback((_) {
                                  if (!isLoaded)
                                    context
                                        .read<CompteBloc>()
                                        .add(LoadComptesVirEvent());
                                });
                                showCompteDialog(context);
                              },
                              child: Container(
                                height: 50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 5),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black26),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: TextFormField(
                                  controller: compteDebiterController,
                                  /*validator: (v) {
                                    if (v.isEmpty)
                                      return "Compte à débiter ne peut pas etre vide";
                                    return null;
                                  },*/
                                  enabled: false,
                                  // initialValue:
                                  //     compteDebiterController.text,
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: Theme.of(context)
                                          .errorColor, // or any other color
                                    ),
                                    // errorText:
                                    //     "Compte à débiter ne peut pas etre vide",
                                    hintText: AppLocalizations.of(context).compte_debiter,
                                    hintStyle: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black45,
                                    )),
                                    suffixIcon: Icon(
                                        Icons.arrow_drop_down_circle_outlined,
                                        color: GlobalParams
                                            .themes["$banque_id"].iconsColor),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            BlocBuilder<BeneficiaireBloc,
                                BeneficiaireStateBloc>(
                              builder: (context, state) {
                                if (state.requestState == StateStatus.NONE) {
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    if (!isBenefLoaded)
                                      context
                                          .read<BeneficiaireBloc>()
                                          .add(LoadBeneficiaire());
                                    if (!isPeriodLoaded)
                                      context
                                          .read<VirementBloc>()
                                          .add(GetPeriodicite());
                                  });
                                  return TextFormField(
                                    controller: beneficaireController,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      hintText:
                                          AppLocalizations.of(context).chargement_benef,
                                      hintStyle: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black45,
                                      )),
                                      suffixIcon: Icon(Icons.refresh,
                                          color: GlobalParams
                                              .themes["$banque_id"].iconsColor),
                                      border: InputBorder.none,
                                    ),
                                  );
                                } else if (state.requestState ==
                                        StateStatus.LOADED ||
                                    state.requestState == StateStatus.ERROR) {
                                  isBenefLoaded = true;
                                  return GestureDetector(
                                    onTap: () {
                                      showBenifDialog(context);
                                    },
                                    child: Container(
                                      height: 50,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 5),
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Colors.black26),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                      child: TextFormField(
                                        /*validator: (v) {
                                          if (v.isEmpty)
                                            return "Bénéficiaire ne peut pas etre vide";
                                          return null;
                                        },*/
                                        controller: beneficaireController,
                                        enabled: false,
                                        decoration: InputDecoration(
                                          errorStyle: TextStyle(
                                            color: Theme.of(context)
                                                .errorColor, // or any other color
                                          ),
                                          hintText: AppLocalizations.of(context).benef,
                                          hintStyle: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black45,
                                          )),
                                          suffixIcon: Icon(Icons.person,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .iconsColor),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  );
                                } else if (state.requestState ==
                                    StateStatus.LOADING) {
                                  return Container(
                                    height: 50,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 5),
                                    decoration: BoxDecoration(
                                        border:
                                            Border.all(color: Colors.black26),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: TextFormField(
                                      controller: beneficaireController,
                                      enabled: false,
                                      decoration: InputDecoration(
                                        hintText:
                                            AppLocalizations.of(context).chargement_benef,
                                        hintStyle: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black45,
                                        )),
                                        suffixIcon: Icon(Icons.refresh,
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .iconsColor),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            Container(
                              height: 50,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black26),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: TextFormField(
                                controller: montantController,
                                onSaved: (v) {
                                  setState(() => {montant = v});
                                },
                                /*validator: (v) {
                                  if (v.isEmpty)
                                    return errorAlert(
                                        context, "Veuillez remplir les champs vides");
                                  return null;
                                },*/
                                cursorColor: secondaryColor7,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  fillColor: secondaryColor7,
                                  focusColor: secondaryColor7,
                                  hoverColor: secondaryColor7,
                                  errorStyle: TextStyle(
                                    color: Theme.of(context)
                                        .errorColor, // or any other color
                                  ),
                                  hintText: AppLocalizations.of(context).montant,
                                  hintStyle: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black45,
                                  )),
                                  suffixIcon: Icon(Icons.money,
                                      color: GlobalParams
                                          .themes["$banque_id"].iconsColor),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            Container(
                              height: 50,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black26),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: TextFormField(
                                controller: motifController,
                                onSaved: (v) {
                                  setState(() => {motif = v});
                                },
                                /* {
                                  if (v.isEmpty)
                                    return errorAlert( context, "Veuillez remplir les champs vides");
                                  if (v.length <= 2)
                                    return errorAlert( context, "3 caractere au minimum");
                                  return null;
                                },*/
                                cursorColor: secondaryColor7,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                    color: Theme.of(context)
                                        .errorColor, // or any other color
                                  ),
                                  hintText: AppLocalizations.of(context).motif,
                                  hintStyle: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black45,
                                  )),
                                  suffixIcon: Icon(Icons.description,
                                      color: GlobalParams
                                          .themes["$banque_id"].iconsColor),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            GestureDetector(
                              onTap: () async {
                                // showDateDialog(context);
                                // dialogShowInput1(
                                //     "Date d'exécution", dateVirement, context);

                                var res =
                                    await selectDate(context, dateVirement);
                                setState(() {
                                  dateVirement = res;
                                });

                                // dialogShowInput1(
                                //     "Exécuter aujourd’hui", dateVirement);
                              },
                              child: Container(
                                height: 50,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black26),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: TextFormField(
                                  controller: mycontroller,
                                  // onSaved: (v) {
                                  //   setState(() => {});
                                  // },
                                  keyboardType: TextInputType.datetime,
                                  enabled: false,

                                  decoration: InputDecoration(
                                    // labelText: "Date d'exécution : ",
                                    labelStyle: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.black45,
                                            fontSize: 16)),
                                    hintText: AppLocalizations.of(context).date_exec,
                                    hintStyle: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black45,
                                    )),
                                    suffixIcon: Icon(Icons.calendar_today,
                                        color: GlobalParams
                                            .themes["$banque_id"].iconsColor),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            permanent
                                ? AnimatedContainer(
                                    duration: Duration(seconds: 1),
                                    curve: Curves.fastOutSlowIn,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 15, bottom: 15),
                                      child: GestureDetector(
                                        onTap: () async {
                                          // showFinDateDialog(context);

                                          var res = await selectFinDate(
                                              context, dateFinVirement);
                                          setState(() {
                                            dateFinVirement = res;
                                          });

                                          // dialogShowInput1(
                                          //     "Exécuter aujourd’hui", dateVirement);
                                        },
                                        child: Padding(
                                          padding: EdgeInsets.only(bottom: 15),
                                          child: Container(
                                            height: 50,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 8),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.black26),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: TextFormField(
                                              controller: mycontrollerFinDate,
                                              // onSaved: (v) {
                                              //   setState(() => {});
                                              // },
                                              keyboardType:
                                                  TextInputType.datetime,
                                              enabled: false,
                                              decoration: InputDecoration(
                                                // labelText: 'Date fin : ',
                                                labelStyle: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Colors.black45,
                                                        fontSize: 16)),
                                                hintText: AppLocalizations.of(context).date_fin,
                                                hintStyle: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Colors.black45,
                                                        fontSize: 16)),
                                                suffixIcon: Icon(
                                                    Icons.calendar_today,
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .iconsColor),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(
                                    // height:
                                    //     MediaQuery.of(context).size.height *
                                    //         0.02,
                                    ),
                            permanent
                                ? GestureDetector(
                                    onTap: () {
                                      // showFinDateDialog(context);
                                      // dialogShowInput1(
                                      //     "Exécuter aujourd’hui", dateVirement);
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.only(bottom: 15),
                                      child: AnimatedContainer(
                                        duration: Duration(seconds: 1),
                                        curve: Curves.fastOutSlowIn,
                                        padding: EdgeInsets.only(
                                            right: 10, top: 8, left: 8),
                                        height: 50,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black26),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: BlocBuilder<VirementBloc,
                                            VirementStateBloc>(
                                          builder: (context, state) {
                                            if (state.requestState ==
                                                    StateStatus.NONE ||
                                                state.requestState == null) {
                                              WidgetsBinding.instance
                                                  .addPostFrameCallback((_) {
                                                if (!isPeriodLoaded)
                                                  context
                                                      .read<VirementBloc>()
                                                      .add(GetPeriodicite());
                                              });
                                              return Container();
                                            } else if (state.requestState ==
                                                StateStatus.LOADING) {
                                              return DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  hint: Text(AppLocalizations.of(context).period),
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          color: Colors.black45,
                                                          fontSize: 16)),
                                                  value: dropdownValue,
                                                  items: <DropdownMenuItem>[
                                                    DropdownMenuItem(
                                                      value: 0,
                                                      child: Center(
                                                          child:
                                                              CircularProgressIndicator(
                                                        backgroundColor:
                                                            GlobalParams
                                                                .themes[
                                                                    "$banque_id"]
                                                                .intituleCmpColor,
                                                        valueColor:
                                                            AlwaysStoppedAnimation<
                                                                    Color>(
                                                                GlobalParams
                                                                    .themes[
                                                                        "$banque_id"]
                                                                    .appBarColor),
                                                      )),
                                                    ),
                                                  ],
                                                  onChanged: (value) {},
                                                ),
                                              );
                                            } else if (state.requestState ==
                                                StateStatus.ERROR) {
                                              return DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  hint: Text(AppLocalizations.of(context).period),
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 16)),
                                                  value: dropdownValue,
                                                  items: <DropdownMenuItem>[
                                                    DropdownMenuItem(
                                                      value: 'A',
                                                      child: ErreurTextWidget(
                                                        errorMessage:
                                                            state.errorMessage,
                                                        actionEvent: () {
                                                          context
                                                              .read<
                                                                  VirementBloc>()
                                                              .add(state
                                                                  .currentAction);
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                  onChanged: (value) {},
                                                ),
                                              );
                                            } else if (state.requestState ==
                                                StateStatus.LOADED) {
                                              isPeriodLoaded = true;
                                              return DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  hint: Text(AppLocalizations.of(context).period),
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 16)),
                                                  value: dropdownValue,
                                                  items: state.periodicites.map
                                                      .periodiciteList
                                                      .map((p) =>
                                                          DropdownMenuItem(
                                                            value: p.code,
                                                            child: Text(
                                                                "${p.libelle}"),
                                                          ))
                                                      .toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      dropdownValue = value;
                                                    });
                                                  },
                                                ),
                                              );
                                            } else {
                                              return Container();
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0035,
                                  ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  AppLocalizations.of(context).vir_parmanent,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 16)),
                                ),
                                Transform.scale(
                                  scale: 0.55,
                                  child: CupertinoSwitch(
                                    activeColor: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                    value: permanent,
                                    onChanged: (value) {
                                      setState(() {
                                        permanent = value;
                                      });
                                      if (value) {
                                        _scrollController.animateTo(
                                            _scrollController
                                                .position.maxScrollExtent,
                                            duration: Duration(seconds: 1),
                                            curve: Curves.easeIn);
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height:
                                  MediaQuery.of(context).size.height * 0.035,
                            ),
                            Container(
                              height: 50,
                              width: MediaQuery.of(context).size.width,
                              child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  onLongPress: fillForm,
                                  onPressed: () {
                                    if (validateForm() == -1) {
                                      errorAlert(context,
                                          "Veuillez remplir les champs vides");
                                    } else if (validateForm() == -2) {
                                      errorAlert(context,
                                          "Le motif doit avoir 3 caractères au minimum");
                                    } else
                                      confirmVir();
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  child: Text(AppLocalizations.of(context).valider,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18)))),
                            )
                          ],
                        ),
                      ),
                    ),
                    BlocListener<VirementBloc, VirementStateBloc>(
                      listener: (context, state) {
                        if (state.requestStateSave == StateStatus.LOADING) {
                          showLoading(context);
                        } else if (state.requestStateSave ==
                            StateStatus.ERROR) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          errorAlert(context, state.errorMessage);
                        } else if (state.requestStateSave ==
                            StateStatus.LOADED) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          permanent
                              ? signeVirementPerm(
                                  state.res['id'], state.res['taskId'])
                              : signeVirement(
                                  state.res['id'], state.res['taskId']);
                        }
                        //
                        else if (state.requestStateCmptSave ==
                            StateStatus.LOADING) {
                          showLoading(context);
                        } else if (state.requestStateCmptSave ==
                            StateStatus.ERROR) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          errorAlert(context, state.errorMessage);
                        } else if (state.requestStateCmptSave ==
                            StateStatus.LOADED) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          signeVirement(state.res['id'], state.res['taskId']);
                        }
                        //
                        else if (state.requestStateCmptSigne ==
                            StateStatus.LOADING) {
                          showLoading(context);
                        } else if (state.requestStateCmptSigne ==
                            StateStatus.LOADED) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          okAlert(context, state.res["message"]);

                          passwordController.text = "";
                          compteDebiterController.text = "";
                          beneficaireController.text = "";
                          montantController.text = "";
                          motifController.text = "";
                          context.read<CompteBloc>().add(ReLoadComptesEvent());
                          context
                              .read<HistoriqueVirementCompteBloc>()
                              .add(LoadHistoriqueVirementCompteEvent(page: 1));
                          Future.delayed(Duration(seconds: 1), () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        HistoriqueVirementPage(
                                          isComingFromMenu: true,
                                          index: isBenifSelected ? 0 : 1,
                                        )));
                          });
                        } else if (state.requestStateCmptSigne ==
                            StateStatus.ERROR) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          errorAlert(context, state.errorMessage);
                          passwordController.text = "";
                        }
                        //
                        else if (state.requestStateSigne ==
                            StateStatus.LOADING) {
                          showLoading(context);
                        } else if (state.requestStateSigne ==
                            StateStatus.LOADED) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          okAlert(context, state.res["message"]);

                          passwordController.text = "";
                          compteDebiterController.text = "";
                          beneficaireController.text = "";
                          montantController.text = "";
                          motifController.text = "";
                          context.read<CompteBloc>().add(ReLoadComptesEvent());
                          context
                              .read<HistoriqueVirementBloc>()
                              .add(LoadHistoriqueVirementEvent(page: 1));

                          Future.delayed(Duration(seconds: 1), () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        HistoriqueVirementPage(
                                          isComingFromMenu: true,
                                        )));
                          });
                        } else if (state.requestStateSigne ==
                            StateStatus.ERROR) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          errorAlert(context, state.errorMessage);
                          passwordController.text = "";
                        }
                        //
                        else if (state.requestStateAbon ==
                            StateStatus.LOADING) {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            // Navigator.pop(context);
                            showLoading(context);
                          });
                        } else if (state.requestStateAbon ==
                            StateStatus.LOADED) {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            okAlert(
                                context, "Votre transaction a été abandonée");
                            compteDebiterController.text = "";
                            beneficaireController.text = "";
                            montantController.text = "";
                            motifController.text = "";
                          });
                        } else if (state.requestStateAbon ==
                            StateStatus.ERROR) {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            errorAlert(
                                context,
                                state.errorMessageAbon == null
                                    ? "une erreur est survenue."
                                    : state.errorMessageAbon);
                          });
                        }
                      },
                      child: Container(),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  confirmVir() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                      AppLocalizations.of(context).recap.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
                width: MediaQuery.of(context).size.width * 0.8,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).compte_debiter} :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${compteDebiterController.text}\n${selectedAccount.identifiantInterne}",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                        alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text("${AppLocalizations.of(context).benef} :  ",
                            maxLines: 1,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold)))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${beneficaireController.text}\n${isBenifSelected ? benif.numeroCompte ?? "" : benif.identifiantInterne ?? ""}",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("${AppLocalizations.of(context).montant} :  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(montantController.text,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.w600))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("${AppLocalizations.of(context).motif} :  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(motifController.text,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).date_exec} :  ",
                        overflow: TextOverflow.clip,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${DateFormat.yMMMMd(MyApp.of(context).getLocale().languageCode).format(dateVirement)}",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              permanent
                  ? Row(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: FittedBox(
                            alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                            fit: BoxFit.scaleDown,
                            child: Text("${AppLocalizations.of(context).date_fin}:  ",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 13,
                                        color: Colors.grey[700],
                                        fontWeight: FontWeight.bold))),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: FittedBox(
                            alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                            fit: BoxFit.scaleDown,
                            child: Text(
                                "${DateFormat.yMMMMd(MyApp.of(context).getLocale().languageCode).format(dateFinVirement)}",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold))),
                          ),
                        ),
                      ],
                    )
                  : Container(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 16))),
                        )),
                  ),
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          _formKeyVir.currentState.save();

                          isBenifSelected
                              ? context.read<VirementBloc>().add(
                                  SaveVirementBeneficiaireEvent(
                                      montant: montantController.text,
                                      motif: motifController.text,
                                      dateVir: dateVirement,
                                      dateFin: dateFinVirement,
                                      benifNom: benif.intitule,
                                      intituleCompte: selectedAccount.intitule,
                                      numCompteCrediter: benif.numeroCompte,
                                      nomCre: benif.intitule,
                                      typeCompteCredite:
                                          benif.beneficiaireListType))
                              : context
                                  .read<VirementBloc>()
                                  .add(SaveVirementCompteEvent(
                                    selectedAccountIntitule:
                                        selectedAccount.identifiantInterne,
                                    montant: montantController.text,
                                    motif: motifController.text,
                                    dateVirement: dateVirement,
                                    dateFinVirement: dateFinVirement,
                                    benefIdentifiant: benif.identifiantInterne,
                                    benifIntitule: benif.intitule,
                                    selectedAccount: selectedAccount.intitule,
                                    benif: benif.identifiantInterne,
                                  ));
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).confirmer,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ))),
                        )),
                  ),
                ],
              ),
              Container(height: MediaQuery.of(context).size.height * 0.01)
            ],
          ),
        );
      },
    );
  }

  signeVirementPerm(id, taskId) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
                Center(
                  child: Text(
                      AppLocalizations.of(context).code_secret_sms,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 16))),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 40,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      // enableInteractiveSelection: false,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      obscureText: true,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            abondonVir(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 17)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                            isBenifSelected
                                ? context.read<VirementBloc>().add(
                                    SigneVirementPermBeneficiaireEvent(
                                        password: passwordController.text))
                                : context.read<VirementBloc>().add(
                                    SigneVirementPermCompteEvent(
                                        password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  signeVirement(id, taskId) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    AppLocalizations.of(context).code_secret_sms,
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            // Navigator.pop(context);
                            abondonVir(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                            isBenifSelected
                                ? context.read<VirementBloc>().add(
                                    SigneVirementBeneficiaireEvent(
                                        password: passwordController.text))
                                : context.read<VirementBloc>().add(
                                    SigneVirementCompteEvent(
                                        password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  showCompteDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(20))),
                  padding: EdgeInsets.only(top: 15),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: Text(AppLocalizations.of(context).compte_debiter.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18))),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  // shrinkWrap: true,
                  children: <Widget>[
                    BlocBuilder<CompteBloc, CompteStateBloc>(
                      builder: (context, state) {
                        if (state.requestStateTransaction == StateStatus.NONE) {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            context
                                .read<CompteBloc>()
                                .add(LoadComptesVirEvent());
                          });
                          return Container();
                        } else if (state.requestStateTransaction ==
                            StateStatus.LOADING) {
                          return Center(
                              heightFactor: 10,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestStateTransaction ==
                            StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<CompteBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestStateTransaction ==
                            StateStatus.LOADED) {
                          isLoaded = true;
                          List<Compte> filtredListComptes =
                              state.listeCompteTransaction.comptes;
                          if (benif.runtimeType == Compte) {
                            filtredListComptes = filtredListComptes
                                .where((element) => element != benif)
                                .toList();
                          }
                          if (filtredListComptes.length == 0)
                            return Center(
                              child: Text(
                                AppLocalizations.of(context).aucun_compte_transaction,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes["$banque_id"]
                                            .validationButtonColor)),
                              ),
                            );
                          else
                            return ListView.separated(
                              controller: _scrollController,
                              shrinkWrap: true,
                              itemCount: filtredListComptes.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    compteDebiterController.text =
                                        "${filtredListComptes[index].intitule}";
                                    // compte["Map"]["compteInstanceList"]
                                    //     [index]["intitule"];
                                    setState(() {
                                      selectedAccount =
                                          filtredListComptes[index];
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        child: Text(
                                            filtredListComptes[index].intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            filtredListComptes[index]
                                                .identifiantInterne,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  showBenifDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(20))),
                  padding: EdgeInsets.only(top: 15),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: Text(AppLocalizations.of(context).benefs,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18))),
                ),
                ListView(
                  shrinkWrap: true,
                  children: [
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.75,
                      // width: MediaQuery.of(context).size.width * 0.8,
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.02,
                          ),
                          BlocBuilder<CompteBloc, CompteStateBloc>(
                            builder: (context, state) {
                              if (state.requestStateTransaction ==
                                  StateStatus.NONE) {
                                WidgetsBinding.instance
                                    .addPostFrameCallback((_) {
                                  context
                                      .read<CompteBloc>()
                                      .add(LoadComptesVirEvent());
                                });
                                return Container();
                              } else if (state.requestStateTransaction ==
                                  StateStatus.LOADING) {
                                return Center(
                                    child: CircularProgressIndicator(
                                  backgroundColor: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                          GlobalParams
                                              .themes["$banque_id"].appBarColor),
                                ));
                              } else if (state.requestStateTransaction ==
                                  StateStatus.ERROR) {
                                return ErreurTextWidget(
                                  errorMessage: state.errorMessage,
                                  actionEvent: () {
                                    context
                                        .read<CompteBloc>()
                                        .add(state.currentAction);
                                  },
                                );
                              } else if (state.requestStateTransaction ==
                                  StateStatus.LOADED) {
                                List<Compte> filtredListComptes = state
                                    .listeCompteTransaction.comptes
                                    .where(
                                        (element) => element != selectedAccount)
                                    .toList();
                                return ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: filtredListComptes.length,
                                  separatorBuilder: (context, index) =>
                                      Container(
                                    color: Colors.white,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: Divider(
                                            color: Colors.black26,
                                            height: 15,
                                          ),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: Divider(
                                            color: Colors.black26,
                                            height: 15,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  itemBuilder: (context, index) {
                                    if (selectedAccount ==
                                        filtredListComptes[index]) {
                                      return Container();
                                    }
                                    return GestureDetector(
                                      onTap: () {
                                        // setState(() {
                                        // compte["Map"]["compteInstanceList"]
                                        //     [index]["intitule"];
                                        setState(() {
                                          beneficaireController.text =
                                              "${filtredListComptes[index].intitule}";
                                          isBenifSelected = false;
                                          benif = filtredListComptes[index];
                                        });
                                        // });
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        color: Colors.white,
                                        child: ListTile(
                                          visualDensity: VisualDensity(
                                              horizontal: -4, vertical: -4),
                                          leading: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 0, 5, 0),
                                            child: banque_id == "ADRIA" || banque_id == "NSIA"
                                                ? Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(0, 10, 0, 10),
                                                    child: Image.asset(
                                                      "assets/$banque_id/images/A_LOGO.png",
                                                      color: GlobalParams
                                                          .themes["$banque_id"]
                                                          .intituleCmpColor,
                                                    ),
                                                  )
                                                : SvgPicture.asset(
                                                    "assets/$banque_id/images/logo.svg",
                                                  ),
                                          ),
                                          title: Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(5, 3, 5, 3),
                                            child: Text(
                                                filtredListComptes[index]
                                                    .intitule,
                                                style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: GlobalParams
                                                          .themes["$banque_id"]
                                                          .appBarColor),
                                                )),
                                          ),
                                          subtitle: Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(5, 0, 0, 0),
                                            child: Text(
                                                filtredListComptes[index]
                                                    .identifiantInterne,
                                                style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      fontSize: 15,
                                                      color: GlobalParams
                                                          .themes["$banque_id"]
                                                          .appBarColor),
                                                )),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              } else {
                                return Container();
                              }
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 25, right: 25),
                            child: Divider(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              height: 40,
                              thickness: 3,
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(bottom: 15, top: 15),
                              height: MediaQuery.of(context).size.height * 0.2,
                              // width: MediaQuery.of(context).size.width * 0.9,
                              child: BlocBuilder<BeneficiaireBloc,
                                  BeneficiaireStateBloc>(
                                builder: (context, state) {
                                  if (state.requestState == StateStatus.NONE) {
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) {
                                      context
                                          .read<BeneficiaireBloc>()
                                          .add(LoadBeneficiaire());
                                    });
                                    return Container();
                                  } else if (state.requestState ==
                                      StateStatus.LOADING) {
                                    return Center(
                                        child: CircularProgressIndicator(
                                      backgroundColor: GlobalParams
                                          .themes["$banque_id"]
                                          .intituleCmpColor,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          GlobalParams.themes["$banque_id"]
                                              .appBarColor),
                                    ));
                                  } else if (state.requestState ==
                                      StateStatus.ERROR) {
                                    return ErreurTextWidget(
                                      errorMessage: state.errorMessage,
                                      actionEvent: () {
                                        context
                                            .read<BeneficiaireBloc>()
                                            .add(state.currentAction);
                                      },
                                    );
                                  } else if (state.requestState ==
                                      StateStatus.LOADED) {
                                    if (state.beneficiaire.map.beneficiaireList
                                            .length ==
                                        0)
                                      return Center(
                                        child: Text(
                                          AppLocalizations.of(context).aucun_benef,
                                          style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .validationButtonColor)),
                                        ),
                                      );
                                    else
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10),
                                        child: Container(
                                          child: ListView.separated(
                                            shrinkWrap: true,
                                            itemCount: state.beneficiaire.map
                                                .beneficiaireList.length,
                                            separatorBuilder:
                                                (context, index) => Container(
                                              color: Colors.white,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.25,
                                                    child: Divider(
                                                      color: Colors.black12,
                                                      height: 15,
                                                      thickness: 1,
                                                    ),
                                                  ),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.25,
                                                    child: Divider(
                                                      color: Colors.black12,
                                                      height: 15,
                                                      thickness: 1,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            itemBuilder: (context, index) {
                                              return GestureDetector(
                                                  onTap: () {
                                                    // setState(() {
                                                    beneficaireController.text =
                                                        "${state.beneficiaire.map.beneficiaireList[index].intitule}";
                                                    setState(() {
                                                      isBenifSelected = true;
                                                      benif = state
                                                              .beneficiaire
                                                              .map
                                                              .beneficiaireList[
                                                          index];
                                                    });
                                                    // });
                                                    Navigator.pop(context);
                                                  },
                                                  child: Container(
                                                    color: Colors.white,
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              5, 0, 5, 0),
                                                      child: ListTile(
                                                        visualDensity:
                                                            VisualDensity(
                                                                horizontal: -4,
                                                                vertical: -4),
                                                        title: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 5,
                                                                  right: 5),
                                                          child: Text(
                                                              state
                                                                      .beneficiaire
                                                                      .map
                                                                      .beneficiaireList[
                                                                          index]
                                                                      .intitule ??
                                                                  "",
                                                              style: GoogleFonts
                                                                  .roboto(
                                                                textStyle: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: GlobalParams
                                                                        .themes[
                                                                            "$banque_id"]
                                                                        .appBarColor),
                                                              )),
                                                        ),
                                                        subtitle: Padding(
                                                          padding: EdgeInsets
                                                              .fromLTRB(
                                                                  5, 0, 0, 0),
                                                          child: Text(
                                                              state
                                                                      .beneficiaire
                                                                      .map
                                                                      .beneficiaireList[
                                                                          index]
                                                                      .numeroCompte ??
                                                                  "",
                                                              style: GoogleFonts
                                                                  .roboto(
                                                                textStyle: TextStyle(
                                                                    fontSize:
                                                                        15,
                                                                    color: GlobalParams
                                                                        .themes[
                                                                            "$banque_id"]
                                                                        .appBarColor),
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                  ));
                                            },
                                          ),
                                        ),
                                      );
                                  } else {
                                    return Container();
                                  }
                                },
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  abondonVir(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 20),
                child: Text(
                  AppLocalizations.of(context).annuler_transaction_message,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18)),
                ),
              ),
              ListTile(
                leading: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).non,
                        style: TextStyle(color: Colors.white, fontSize: 16))),
                trailing: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      // Navigator.pop(context);
                      Navigator.pop(context);

                      isBenifSelected
                          ? context
                              .read<VirementBloc>()
                              .add(AbandonnerVirBenefPhaseSignature())
                          : context
                              .read<VirementBloc>()
                              .add(AbandonnerVirComptePhaseSignature());
                    },
                    color: Colors.white,
                    child: Text(AppLocalizations.of(context).oui,
                        style: TextStyle(
                            fontSize: 16,
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor))),
              )
            ],
          ),
        );
      },
    );
  }

  selectDate(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateSelected ? dateVirement : DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: isDateFinSelected ? dateFinVirement : DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      setState(() {
        updatedDate = picked;
        mycontroller.text = _dateFormat.format(updatedDate);
        isDateSelected = true;
      });
    }
    return updatedDate;
  }

  selectFinDate(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateFinSelected ? dateFinVirement : dateVirement,
      firstDate: dateVirement,
      lastDate: DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      setState(() {
        updatedDate = picked;
        mycontrollerFinDate.text = _dateFormat.format(updatedDate);
        isDateFinSelected = true;
      });
    }
    return updatedDate;
  }

  int validateForm() {
    if (compteDebiterController.text.isEmpty ||
        beneficaireController.text.isEmpty ||
        montantController.text.isEmpty ||
        motifController.text.isEmpty)
      return -1;
    else if (motifController.text.length < 3)
      return -2;
    else
      return 0;
  }

  fillForm() {
    montantController.text = '200';
    motifController.text = 'test';
  }
}
