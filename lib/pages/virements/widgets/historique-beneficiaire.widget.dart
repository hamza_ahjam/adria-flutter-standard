
import 'package:LBA/bloc/historiqueVirement/historique-virement.bloc.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/virements/virement-detail.page.dart';
import 'package:LBA/pages/virements/widgets/comptes/compte.historique.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/show-more.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

class HistoriqueBeneciaire extends StatefulWidget {
  // TokenState tokenState;
  // CompteState compteState;
  bool isFromMenu = false;
  HistoriqueBeneciaire({this.isFromMenu}) {
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState = Provider.of<CompteState>(context, listen: false);
  }

  @override
  _HistoriqueBeneciaireState createState() => _HistoriqueBeneciaireState();
}

class _HistoriqueBeneciaireState extends State<HistoriqueBeneciaire> {
  bool isLoaded = false;

  int page = 1;

  var dif;
  DateTime dateDebut = DateTime.now();
  DateTime datefin = DateTime.now();
  bool isDateDebutSelected = false;
  bool isDateFinSelected = false;

  TextEditingController _mycontrollerDebut = TextEditingController();
  TextEditingController _mycontrollerFin = TextEditingController();
  intl.DateFormat _dateFormat = intl.DateFormat.yMd('fr_FR');
  TextEditingController _libelleControllerB = TextEditingController();

  // @override
  // void dispose() {
  //   super.dispose();
  //   _mycontrollerDebut.dispose();
  //   _mycontrollerFin.dispose();
  //   _libelleControllerB.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      children: [
        Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image:
                    AssetImage("assets/$banque_id/images/background_image.png"),
                fit: BoxFit.cover,
                alignment: Alignment(0, 0.9),
              ),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(22.0),
              ),
            ),
            child: CompteHistoriqueWidget()),
        Padding(
          padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.04, 20, MediaQuery.of(context).size.width * 0.04, 5),
          child: Text(AppLocalizations.of(context).mon_histo.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ))),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.04, 0, MediaQuery.of(context).size.width * 0.04, 5),
          child: Text(AppLocalizations.of(context).virements.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ))),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  showDateDialog(context);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 40,
                  child: TextField(
                    textAlignVertical: TextAlignVertical.bottom,
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).trouver_vir,
                      hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(fontSize: 15),
                      ),
                      prefixIcon: Icon(Icons.search,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  child: Icon(
                    Icons.calendar_today,
                    color: Colors.white,
                    size: 20,
                  )
                  //  SvgPicture.asset(
                  //   "assets/$banque_id/images/date.svg",
                  //   // width: 60,
                  //   // height: 60,
                  // ),
                  ),
            ],
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 8),
        //   child: NomCompteWidget(
        //     context: context,
        //     titre: "Historique",
        //   ),
        // ),
        BlocBuilder<HistoriqueVirementBloc, HistoriqueVirementStateBloc>(
            builder: (context, state) {
          if (state.requestState == StateStatus.NONE) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded)
                context
                    .read<HistoriqueVirementBloc>()
                    .add(LoadHistoriqueVirementEvent(page: page));
            });
            return Container();
          } else if (state.requestState == StateStatus.LOADING) {
            return Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: Center(
                  child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              )),
            );
          } else if (state.requestState == StateStatus.ERROR) {
            return Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context
                      .read<HistoriqueVirementBloc>()
                      .add(state.currentAction);
                },
              ),
            );
          } else if (state.requestState == StateStatus.LOADED) {
            isLoaded = true;
            if (state.historiqueVirementBenif.map.liste.length == 0)
              return Center(
                heightFactor: 20,
                child: Text(
                  AppLocalizations.of(context).aucun_vir,
                  style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(fontSize: 16, color: principaleColor5)),
                ),
              );
            else
              return Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                          height: widget.isFromMenu
                              ? MediaQuery.of(context).size.height * 0.42
                              : MediaQuery.of(context).size.height * 0.45 - 100,
                          child: state.historiqueVirementBenif == null
                              ? Center(
                                  child: CircularProgressIndicator(
                                  backgroundColor: principaleColor5,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      principaleColor6),
                                ))
                              : ListView.separated(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  separatorBuilder: (context, index) => Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: Divider(
                                              color: Colors.black26,
                                              height: 15,
                                            ),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: Divider(
                                              color: Colors.black26,
                                              height: 15,
                                            ),
                                          )
                                        ],
                                      ),
                                  itemCount: state
                                      .historiqueVirementBenif.map.liste.length,
                                  itemBuilder: (_, index) {
                                    // print("${state.historiqueVirementBenif.map.liste[0]}");
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    VirementDetail(
                                                      ref: state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index][
                                                          "identifiantDemande"],
                                                      compte: state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index][
                                                          "numeroCompteCrediter"],
                                                      benef: state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index][
                                                          "intituleCompteCrediter"],
                                                      rib: state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index][
                                                          "numeroCompteCrediter"],
                                                      montant:
                                                      // "100",
                                                      state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index]
                                                          ["montantFormatted"],
                                                      motif: state
                                                          .historiqueVirementBenif
                                                          .map
                                                          .liste[index]["motif"],
                                                      date: state
                                                              .historiqueVirementBenif
                                                              .map
                                                              .liste[index]
                                                          ["beanDateExecution"],
                                                      status: state
                                                          .historiqueVirementBenif
                                                          .map
                                                          .liste[index]["statut"],
                                                    )));
                                        // showHistoriqueDetail(
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["intituleCompteCrediter"],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["montantFormatted"],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["deviseOperationLibelle"],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["motif"],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["dateCreation"],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ['identifiantDemande'],
                                        //     state.historiqueVirementBenif.map.liste[index]
                                        //         ["statut"],
                                        //     context);
                                      },
                                      child: Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: ListTile(
                                          visualDensity: VisualDensity(
                                              horizontal: -4, vertical: -4),
                                          leading: Container(
                                            // width: MediaQuery.of(context)
                                            //         .size
                                            //         .width *
                                            //     0.15,
                                            child: SvgPicture.asset(
                                                "assets/$banque_id/images/Picto_virement.svg",
                                                // fit: BoxFit.fill,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.03,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .colorTab),
                                            //  Row(
                                            //   mainAxisAlignment:
                                            //       MainAxisAlignment.spaceAround,
                                            //   children: [
                                            //     Padding(
                                            //       padding: const EdgeInsets.only(
                                            //           right: 5, top: 10),
                                            //       child: Column(
                                            //         children: [
                                            //           Text(
                                            //             " ${DateFormat.Md('fr').format(DateFormat("dd-MM-yyyy").parse(state.historiqueVirementBenif.map.liste[index]["beanDateExecution"]))}",
                                            //             style: GoogleFonts.roboto(
                                            //                 textStyle: TextStyle(
                                            //                     color:
                                            //                         Colors.black,
                                            //                     fontSize: 15)),
                                            //           ),
                                            //         ],
                                            //       ),
                                            //     ),
                                            //     Container(
                                            //         width: 3,
                                            //         height: double.infinity,
                                            //         color: GlobalParams
                                            //             .themes["$banque_id"]
                                            //             .appBarColor)
                                            //   ],
                                            // ),
                                          ),
                                          trailing: Text(
                                              // "100 CFA",
                                              "${state.historiqueVirementBenif.map.liste[index]["montantFormatted"]} ${state.historiqueVirementBenif.map.liste[index]["deviseOperationLibelle"]}",
                                              style: GoogleFonts.roboto(
                                                  textStyle:
                                                      TextStyle(fontSize: 18))),
                                          title: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5.0),
                                            child: Text(
                                                "${state.historiqueVirementBenif.map.liste[index]["intituleCompteCrediter"]}",
                                                style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 14))),
                                          ),
                                          subtitle: Text(
                                              "${state.historiqueVirementBenif.map.liste[index]["motif"]}",
                                              style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      color: Colors.grey[700],
                                                      fontSize: 12))),
                                          isThreeLine: true,
                                        ),
                                      ),
                                    );
                                  })),
                      state.historiqueVirementBenif == null
                          ? Container()
                          : state.historiqueVirementBenif.map.liste.length > 19
                              ? ShowMore(
                                  showMoreAction: () {
                                    context.read<HistoriqueVirementBloc>().add(
                                        LoadHistoriqueVirementEvent(
                                            page: page));
                                    // compteState.reloadHistoriqueVirement(
                                    //     tokenState, page);
                                    ++page;
                                  },
                                )
                              : Container()
                    ],
                  ),
                ),
              );
          } else {
            return Container();
          }
        })
      ],
    );
  }

  showDateDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Text(
                "Selectionner une date".toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
                textAlign: TextAlign.center,
              ),
            ),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    height: 45,
                    child: TextFormField(
                      textAlignVertical: TextAlignVertical.bottom,
                      controller: _libelleControllerB,
                      decoration: InputDecoration(
                        errorStyle: TextStyle(
                          color: Theme.of(context)
                              .errorColor, // or any other color
                        ),
                        hintText: 'Libelle',
                        hintStyle: GoogleFonts.roboto(
                            fontSize: 17,
                            color: Color.fromRGBO(152, 152, 152, 1)),
                        prefixIcon: Icon(Icons.library_books_outlined,
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                                color: GlobalParams
                                    .themes["$banque_id"].appBarColor,
                                width: 1.5)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  dialogShowInput1("date de debut", dateDebut, context),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  dialogShowInput2("date de fin", datefin, context),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width * 0.042,
                    bottom: MediaQuery.of(context).size.height * 0.025),
                child: RaisedButton(
                  padding: EdgeInsets.only(left: 25, right: 25),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7),
                  ),
                  onPressed: () {
                    // setState(() {
                    dif = datefin.difference(dateDebut).inDays;
                    // });
                    // print(dif);

                    if (dif > 90) {
                      //Navigator.pop(context);
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        errorAlert(
                            context, "vous ne pouvez pas dépasser 3 mois ");
                      });
                    } else {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        // context.read<MouvementBloc>().add(
                        //     LoadRecherchMouvement(
                        //         dateDebut: DateFormat('dd-MM-yyyy')
                        //             .format(dateDebut),
                        //         dateFin: DateFormat('dd-MM-yyyy')
                        //             .format(datefin),
                        //         libelle: _libelleControllerB.text));
                        context.read<HistoriqueVirementBloc>().add(
                            SearchVirBenefEvent(
                                dateDebut:
                                    isDateDebutSelected ? intl.DateFormat('dd-MM-yyyy').format(dateDebut) : '',
                                dateFin:
                                    isDateFinSelected ? intl.DateFormat('dd-MM-yyyy').format(datefin) : '',
                                libelle: _libelleControllerB.text));

                        _libelleControllerB.text = "";
                        _mycontrollerDebut.text = "";
                        _mycontrollerFin.text = "";

                        dateDebut = DateTime.now();
                        datefin = DateTime.now();
                        isDateDebutSelected = false;
                        isDateFinSelected = false;
                      });
                    }
                    // print(
                    //     "dateDebut : ${DateFormat('dd-MM-yyyy').format(dateDebut)} --- datefin: ${DateFormat('dd-MM-yyyy').format(datefin)}");

                    Navigator.pop(context);
                  },
                  child: Text(
                    "Recherche",
                    style:
                        GoogleFonts.roboto(fontSize: 17, color: Colors.white),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  // gradient: LinearGradient(
                  //   colors: <Color>[
                  //     GlobalParams.themes["$banque_id"].appBarColor,
                  //     GlobalParams.themes["$banque_id"].appBarColor
                  //   ],
                  // ),
                ),
              ),
            ],
          );
        });
  }

  dialogShowInput1(String hint, dateselected, context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GestureDetector(
        onTap: () async {
          dateDebut = await selectDate(context, dateselected);
          // setState(() {
          // dateDebut = res;
          // });
        },
        child: Container(
          // width: 250,
          height: 45,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black26),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextField(
            textAlignVertical: TextAlignVertical.bottom,

            controller: _mycontrollerDebut,
            // onChanged: (value) {
            //   _mycontrollerDebut.text = value;
            //   print(value);
            // },
            enabled: false,
            decoration: InputDecoration(
              // labelText: hint.toString(),
              hintText: hint.toString(),
              hintStyle: GoogleFonts.roboto(
                  fontSize: 17, color: Color.fromRGBO(152, 152, 152, 1)),
              prefixIcon: Icon(Icons.search,
                  color: GlobalParams.themes["$banque_id"].iconsColor),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
            ),
          ),
        ),
      ),
    );
  }

  dialogShowInput2(String hint, dateselected, context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GestureDetector(
        onTap: () async {
          datefin = await selectDate2(context, dateselected);
          // setState(() {
          //   datefin = res;
          // });
        },
        child: Container(
          // width: 200,
          height: 45,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black26),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextField(
            textAlignVertical: TextAlignVertical.bottom,
            controller: _mycontrollerFin,
            enabled: false,
            decoration: InputDecoration(
              hintText: hint.toString(),
              hintStyle: GoogleFonts.roboto(
                  fontSize: 17, color: Color.fromRGBO(152, 152, 152, 1)),
              prefixIcon: Icon(Icons.search,
                  color: GlobalParams.themes["$banque_id"].iconsColor),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
            ),
          ),
        ),
      ),
    );
  }

  selectDate(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateDebutSelected
          ? dateDebut
          : (datefin.difference(DateTime.now()).inDays > 0
              ? DateTime.now()
              : datefin),
      firstDate: DateTime(1920),
      lastDate: isDateFinSelected ? datefin : DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      // setState(() {
      updatedDate = picked;
      _mycontrollerDebut.text = _dateFormat.format(updatedDate);
      isDateDebutSelected = true;
      // });
    }
    return updatedDate;
  }

  selectDate2(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateFinSelected
          ? datefin
          : (dateDebut.difference(DateTime.now()).inDays < 0
              ? DateTime.now()
              : dateDebut),
      firstDate: isDateDebutSelected ? dateDebut : DateTime(1920),
      lastDate: DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      // setState(() {
      updatedDate = picked;
      _mycontrollerFin.text = _dateFormat.format(updatedDate);
      isDateFinSelected = true;
      // });

    }
    return updatedDate;
  }
}
