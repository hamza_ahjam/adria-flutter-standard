import 'package:LBA/constants.dart';
import 'package:LBA/services/beneficiaire.service.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/default_button.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

final _formAddBenefKey = GlobalKey<FormState>();
var nomBenif = TextEditingController();
var rib = TextEditingController();
bool load = false;
BeneficiaireService get service => GetIt.I<BeneficiaireService>();

var benificiare;
showAddBenifForm(tokenState, context, token, user) {
  // final block = Provider.of<TokenState>(context);
  Future<dynamic> saveVirement() {
    print("***add benificiare ");
    return service
        .saveBenificiaire(tokenState, token, nomBenif.text, rib.text)
        .then((res) => res)
        .catchError((err) => print("save benif err *** $err"));

    // setState(() {
    //   benificiare = res;
    // });
  }

  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Center(
            child: Text(
              "Ajout d’un nouveau bénéficiaire",
              style: TextStyle(
                  color: KPrimaryColor,
                  fontFamily: KprimaryFont,
                  fontSize: 19,
                  fontWeight: FontWeight.bold),
            ),
          ),
          content: Container(
            height: MediaQuery.of(context).size.height * 0.3,
            // width: MediaQuery.of(context).size.width * 0.9,
            child: Form(
              key: _formAddBenefKey,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                      child: TextFormField(
                        controller: nomBenif,
                        onSaved: (v) {
                          // setState(() => {});
                        },
                        validator: (v) {
                          if (v.isEmpty)
                            return "Nom du bénéficiaire ne peut pas etre vide";
                          return null;
                        },
                        decoration: InputDecoration(
                          labelText: 'Nom du bénéficiaire ',
                          // hintText: 'Nom du bénéficiaire',
                          hintStyle:
                              TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                          prefixIcon:
                              Icon(Icons.person_add, color: KPrimaryColor),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                        ),
                      ),
                    ),
                    TextFormField(
                      controller: rib,
                      onSaved: (v) {
                        // setState(() => {});
                      },
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.length == 24)
                          return null;
                        else
                          return "RIB incorrect il doit avoir 24 chiffres";
                      },
                      decoration: InputDecoration(
                        labelText: 'RIB ',
                        // hintText: 'RIB',
                        hintStyle:
                            TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                        prefixIcon: Icon(Icons.account_balance_wallet_rounded,
                            color: KPrimaryColor),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DefaultButton(
                        onPressed: () {
                          if (_formAddBenefKey.currentState.validate()) {
                            load = true;
                            saveVirement().then((res) {
                              if (res["success"] != null) {
                                // print("res *** $res");
                                if (!res["success"]) {
                                  Navigator.pop(context);
                                  errorAlert(context, res["message"]);
                                }
                                if (res["success"]) {
                                  Navigator.pop(context);
                                  okAlert(context,
                                      "Ce bénéficiaire à été ajouté avec succès");
                                }
                              } else {
                                Navigator.pop(context);
                                errorAlert(context, res["message"]);
                              }
                              nomBenif.text = "";
                              rib.text = "";
                            }).catchError((err) =>
                                print("save benif button err *** $err"));
                            // print("valider ${nomBenif.text}  RIB: ${rib.text} *");
                            load = false;
                          }
                          // _formAddBenefKey.currentState.reset();
                        },
                        child: !load
                            ? Text(
                                "Valider",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: KprimaryFont),
                              )
                            : CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                              ),
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color.fromRGBO(110, 153, 254, 100),
                            Color.fromRGBO(90, 204, 173, 100)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      });
}
