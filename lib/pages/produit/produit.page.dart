import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/dot_indicator.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class ProduitPage extends StatefulWidget {
  ProduitPage({Key key}) : super(key: key);

  @override
  _ProduitPageState createState() => _ProduitPageState();
}

class _ProduitPageState extends State<ProduitPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).prods.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardComptePage()));
              }),
        ),
        body: ListView(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                    margin: const EdgeInsets.only(left: 0, right: 3),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
                Text(
                  AppLocalizations.of(context).credit_hab,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 16)),
                ),
                Container(
                    margin: const EdgeInsets.only(left: 3, right: 0),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.2,
                // width: MediaQuery.of(context).size.width * 0.6,
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.08,
                ),
                child: Image.asset(
                  "assets/$banque_id/images/prod1.png",
                  fit: BoxFit.fill,
                )),
            DotIndicatorWidget(
              lenght: 1,
              page: 0,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.015,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.2,
              ),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.045,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  onPressed: () {},
                  child: Text(AppLocalizations.of(context).souscrire,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                    margin: const EdgeInsets.only(left: 0, right: 3),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
                Text(
                  AppLocalizations.of(context).cartes,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 16)),
                ),
                Container(
                    margin: const EdgeInsets.only(left: 3, right: 0),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.2,
                // width: MediaQuery.of(context).size.width * 0.6,
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.08,
                ),
                child: Image.asset(
                  "assets/$banque_id/images/prod2.png",
                  fit: BoxFit.fill,
                )),
            DotIndicatorWidget(
              lenght: 1,
              page: 0,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.015,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.2,
              ),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.045,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  onPressed: () {},
                  child: Text(AppLocalizations.of(context).souscrire,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                    margin: const EdgeInsets.only(left: 0, right: 3),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
                Text(
                  AppLocalizations.of(context).mes_credits,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 16)),
                ),
                Container(
                    margin: const EdgeInsets.only(left: 3, right: 0),
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Divider(
                      color: Colors.black45,
                      height: 15,
                    )),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.2,
                // width: MediaQuery.of(context).size.width * 0.6,
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.105,
                ),
                child: Image.asset(
                  "assets/$banque_id/images/prod3.png",
                  fit: BoxFit.fill,
                )),
            DotIndicatorWidget(
              lenght: 1,
              page: 0,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.015,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.2,
              ),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.045,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  onPressed: () {},
                  child: Text(AppLocalizations.of(context).souscrire,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
          ],
        ),
      ),
    );
  }
}
