import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/transferts/transfer_nvTransfert_page.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../main.dart';

class TransfertMenuPage extends StatelessWidget {
  const TransfertMenuPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(appBarTitle: AppLocalizations.of(context).transferts,context: context,),
      body: Center(
        child: ListView(
          itemExtent: MediaQuery.of(context).size.height * 0.1,
          padding: EdgeInsets.only(right: 45, left: 45, top: MediaQuery.of(context).size.height * 0.25),
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => NvTrasfertPage()));
              },
              child: ListTile(
                leading: SvgPicture.asset('assets/$banque_id/images/transfert_menu1.svg'),
                title: Text(AppLocalizations.of(context).transferts,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        fontSize: 14,
                        color: GlobalParams.themes[banque_id].appBarColor
                      ),
                    )),
                trailing: SvgPicture.asset(
                  MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                  height: 20,
                  width: 20,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {

              },
              child: ListTile(
                leading: SvgPicture.asset('assets/$banque_id/images/transfert_menu2.svg'),
                title: Text(AppLocalizations.of(context).benefs,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        fontSize: 14,
                        color: GlobalParams.themes[banque_id].appBarColor
                      ),
                    )),
                trailing: SvgPicture.asset(
                  MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                  height: 20,
                  width: 20,
                ),

              ),
            ),
            GestureDetector(
              onTap: () {

              },
              child: ListTile(
                leading: SvgPicture.asset('assets/$banque_id/images/transfert_menu3.svg'),
                title: Text(AppLocalizations.of(context).ajout_benef,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        fontSize: 14,
                        color: GlobalParams.themes[banque_id].appBarColor
                      ),
                    )),
                trailing: SvgPicture.asset(
                  MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                  height: 20,
                  width: 20,
                ),

              ),
            ),
            GestureDetector(
              onTap: () {

              },
              child: ListTile(
                leading: SvgPicture.asset('assets/$banque_id/images/transfert_menu4.svg'),
                title: Text(AppLocalizations.of(context).histo_transfert,
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        fontSize: 14,
                        color: GlobalParams.themes[banque_id].appBarColor
                      ),
                    )),
                trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                ),

              ),
            ),
          ],
        ),
      ),
    );
  }
}
