import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/transfert/transfert_bloc.dart';
import 'package:LBA/bloc/transfert/transfert_event.dart';
import 'package:LBA/bloc/transfert/transfert_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/beneficiaire.model.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';

class NvTrasfertPage extends StatefulWidget {
  NvTrasfertPage({Key key}) : super(key: key);

  @override
  State<NvTrasfertPage> createState() => _NvTrasfertPageState();
}

class _NvTrasfertPageState extends State<NvTrasfertPage> {
  bool isLoaded = false;
  bool isBenefLoaded = false;

  final _formPsdKey = GlobalKey<FormState>();

  var selectedAccount;
  var selectedBenef;

  String dropdownType;

  DateTime dateTransfert = DateTime.now();

  var compteDebiterController = TextEditingController();
  var beneficaireController = TextEditingController();

  TextEditingController dateTextController = TextEditingController();
  TextEditingController montantController = TextEditingController();
  TextEditingController motifController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  DateFormat _dateFormat = DateFormat.yMd('fr_FR');

  List<BeneficiaireList> filtredBenefList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: AppLocalizations.of(context).nv_transfert,
        context: context,
        isPop: true,
      ),
      body: Form(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      if (!isLoaded)
                        context.read<CompteBloc>().add(
                            LoadComptesEvent());
                    });
                    showCompteDialog(context);
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    margin: EdgeInsets.only(top: 60),
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.grey[200]),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextFormField(
                      controller: compteDebiterController,
                      enabled: false,
                      textAlignVertical: TextAlignVertical.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: GlobalParams.themes[banque_id].appBarColor,
                      )),
                      onChanged: (v) {},
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.keyboard_arrow_down,
                          color: GlobalParams.themes[banque_id].appBarColor,
                        ),
                        contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                        hintText: AppLocalizations.of(context).compte_debiter,
                        border: InputBorder.none,
                        hintStyle:
                            TextStyle(color: Colors.grey[600], fontSize: 14),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        color: GlobalParams.themes[banque_id].appBarColor,
                      ),
                      isExpanded: true,
                      hint: Text(
                          AppLocalizations.of(context).canal,
                          style: TextStyle(
                            fontSize: 14
                          ),
                      ),
                      style: GoogleFonts.roboto(
                          textStyle:
                          TextStyle(color: Colors.black45, fontSize: 16)),
                      value: dropdownType,
                      items: <DropdownMenuItem>[
                        DropdownMenuItem(
                          value: 'GAB',
                          child: Text(
                              AppLocalizations.of(context).gab,
                              style: TextStyle(
                                color: GlobalParams.themes[banque_id].appBarColor,
                              ),
                          ),
                        ),
                        DropdownMenuItem(
                          value: 'Agence',
                          child: Text(
                            AppLocalizations.of(context).agence,
                            style: TextStyle(
                              color: GlobalParams.themes[banque_id].appBarColor,
                            ),
                          ),
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          dropdownType = value;
                        });
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                GestureDetector(
                  onTap: (){
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      if (!isBenefLoaded)
                        context.read<BeneficiaireBloc>().add(
                            LoadBeneficiaireMAD());
                    });
                    showBenfDialog(context);
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.grey[200]),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextFormField(
                      controller: beneficaireController,
                      textAlignVertical: TextAlignVertical.center,
                      enabled: false,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: GlobalParams.themes[banque_id].appBarColor,
                      )),
                      enableInteractiveSelection: false,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.person,
                          color: GlobalParams.themes[banque_id].appBarColor,
                        ),
                        contentPadding:
                            EdgeInsets.only(left: 10, bottom: 15, right: 10),
                        border: InputBorder.none,
                        hintText: AppLocalizations.of(context).benef,
                        hintStyle: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: montantController,
                    keyboardType: TextInputType.number,
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      color: GlobalParams.themes[banque_id].appBarColor,
                    )),
                    onChanged: (v) {},
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                      hintText: AppLocalizations.of(context).montant,
                      border: InputBorder.none,
                      hintStyle:
                          TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: motifController,
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      color: GlobalParams.themes[banque_id].appBarColor,
                    )),
                    onChanged: (v) {},
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                      hintText: AppLocalizations.of(context).motif,
                      border: InputBorder.none,
                      hintStyle:
                          TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: GestureDetector(
                    onTap: () async {
                      var res =
                          await selectDate(context, dateTransfert);
                      setState(() {
                        dateTransfert = res;
                      });
                    },
                    child: TextFormField(
                      controller: dateTextController,
                      textAlignVertical: TextAlignVertical.center,
                      enabled: false,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: GlobalParams.themes[banque_id].appBarColor,
                      )),
                      onChanged: (v) {},
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.calendar_today,
                          color: GlobalParams.themes[banque_id].appBarColor,
                        ),
                        contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                        hintText: AppLocalizations.of(context).exec_aujour,
                        border: InputBorder.none,
                        hintStyle:
                            TextStyle(color: Colors.grey[600], fontSize: 14),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                MaterialButton(
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onPressed: () {
                      if (validateForm() == -1) {
                        errorAlert(context,
                          AppLocalizations.of(context).error_remplir_champs,);
                      } else if (validateForm() == -2) {
                        errorAlert(context,
                            "Le motif doit avoir 3 caractères au minimum");
                      } else recapTransfert();
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).suivant,
                        style: GoogleFonts.roboto(
                            textStyle:
                                TextStyle(color: Colors.white, fontSize: 18)))),
                BlocListener<TransfertBloc, TransfertState>(
                  listener: (context, state) {
                    if (state.requestStateSave == StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateSave ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStateSave ==
                        StateStatus.LOADED) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      print('${state.res}');
                      signeTransfert(
                          state.res['id'], state.res['taskId']);
                    } else if (state.requestStateSigne ==
                        StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateSigne ==
                        StateStatus.LOADED) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      okAlert(context, state.res["message"]);

                      passwordController.text = "";
                      compteDebiterController.text = "";
                      beneficaireController.text = "";
                      montantController.text = "";
                      motifController.text = "";
                    } else if (state.requestStateSigne ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                      passwordController.text = "";
                    }
                  },
                  child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showCompteDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius:
                        BorderRadius.only(topLeft: Radius.circular(20))),
                padding: EdgeInsets.only(top: 15),
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.06,
                child: Text(AppLocalizations.of(context).compte_debiter.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18))),
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    BlocBuilder<CompteBloc, CompteStateBloc>(
                      builder: (context, state) {
                        if (state.requestState ==
                            StateStatus.LOADING) {
                          return Center(
                              heightFactor: 10,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestState ==
                            StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<CompteBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestState ==
                            StateStatus.LOADED) {
                          isLoaded = true;
                          // if (benif.runtimeType == CompteInstanceList) {
                          //   filtredListComptes = filtredListComptes
                          //       .where((element) => element != benif)
                          //       .toList();
                          // }
                          if (state.clients.comptes.length ==
                              0)
                            return Center(
                              child: Text(
                                AppLocalizations.of(context).aucun_compte_transaction,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes["$banque_id"]
                                            .validationButtonColor)),
                              ),
                            );
                          else
                            return ListView.separated(
                              shrinkWrap: true,
                              itemCount: state.clients.comptes.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    // compte["Map"]["compteInstanceList"]
                                    //     [index]["intitule"];
                                    setState(() {
                                      selectedAccount =
                                      state.clients.comptes[index];
                                      compteDebiterController.text =
                                      "${selectedAccount.intitule}";
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .identifiantInterne,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      trailing: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .soldeTempsReel,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  showBenfDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius:
                    BorderRadius.only(topLeft: Radius.circular(20))),
                padding: EdgeInsets.only(top: 15),
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.06,
                child: Text(AppLocalizations.of(context).benefs,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18))),
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    BlocBuilder<BeneficiaireBloc, BeneficiaireStateBloc>(
                      builder: (context, state) {
                        if (state.requestStateMAD ==
                            StateStatus.LOADING) {
                          return Center(
                              heightFactor: 10,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestStateMAD ==
                            StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<BeneficiaireBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestStateMAD ==
                            StateStatus.LOADED) {
                          isBenefLoaded = true;
                          filtredBenefList = state.beneficiaireMAD.map.beneficiaireList
                                .where((element) => element.statut == "Signe")
                                .toList();

                          if (filtredBenefList.length ==
                              0)
                            return Center(
                              child: Text(
                                AppLocalizations.of(context).aucun_benef,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes["$banque_id"]
                                            .validationButtonColor)),
                              ),
                            );
                          else
                            return ListView.separated(
                              shrinkWrap: true,
                              itemCount: filtredBenefList.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedBenef = filtredBenefList[index];
                                      beneficaireController.text = "${selectedBenef.intitule}";
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        child: Text(
                                            filtredBenefList[index]
                                                .intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            filtredBenefList[index]
                                                .intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  selectDate(BuildContext context, updatedDate) async {
    var languageCode = MyApp.of(context).getLocale().languageCode;
    final DateTime picked = await showDatePicker(
      context: context,
      locale: Locale(languageCode, codePays),
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      setState(() {
        updatedDate = picked;
        dateTextController.text = _dateFormat.format(updatedDate);
      });
    }
    return updatedDate;
  }

  recapTransfert()   {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    AppLocalizations.of(context).recap.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
                width: MediaQuery.of(context).size.width * 0.8,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).compte_debiter} :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${selectedAccount.intitule}\n ${selectedAccount.identifiantInterne}",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).canal} :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Text(
                        "$dropdownType",
                        maxLines: 1,
                        textAlign: TextAlign.end,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.bold))),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                        alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text("${AppLocalizations.of(context).benef}:  ",
                            maxLines: 1,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold)))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${selectedBenef.intitule}",
                          maxLines: 1,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).montant} :  ",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Text(
                        montantController.text,
                        textAlign: TextAlign.end,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.w600))),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("${AppLocalizations.of(context).motif} :  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(motifController.text,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).date_exec}:  ",
                        overflow: TextOverflow.clip,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${DateFormat.yMMMMd(MyApp.of(context).getLocale().languageCode).format(dateTransfert)}",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),

              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 16))),
                        )),
                  ),
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: MaterialButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          context.read<TransfertBloc>().add(
                              SaveTransfertEvent(
                                  montant: montantController.text,
                                  motif: motifController.text,
                                  gsm: selectedBenef.gsm,
                                  dateTransfert: dateTransfert,
                                  numeroPieceIdentite: selectedBenef.numeroPieceIdentite,
                                  referenceClient: selectedBenef.id,
                                  typePieceIdentite: selectedBenef.typePieceIdentite,
                                  typeTransfer: dropdownType == 'Agence' ? dropdownType.toLowerCase() : dropdownType,
                                  intituleCompte: selectedAccount.intitule,
                                  numCompteCrediter: selectedBenef.numeroCompte,
                                  nomCre: selectedBenef.intitule,
                                  typeCompteCredite:
                                  selectedBenef.beneficiaireListType));
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).confirmer,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                  ))),
                        )),
                  ),
                ],
              ),
              Container(height: MediaQuery.of(context).size.height * 0.01)
            ],
          ),
        );
      },
    );
  }

  showSucess(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          backgroundColor: GlobalParams.themes[banque_id].intituleCmpColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
                width: MediaQuery.of(context).size.width * 0.9,
              ),
              Text(
                  AppLocalizations.of(context).transfert_sucess_msg,
                  textAlign: TextAlign.center,
                  style:
                  GoogleFonts.roboto(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                  )),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.045),
                child: Text(
                    AppLocalizations.of(context).code_transfert,
                    textAlign: TextAlign.center,
                    style:
                    GoogleFonts.roboto(
                        fontSize: 18,
                        color: Colors.white
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.025),
                child: Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: 70),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: GlobalParams.themes[banque_id].alertCodeBgColor,
                    borderRadius: BorderRadius.circular(15)
                  ),
                  child: Text(
                    '93580165',
                    style: TextStyle(
                        color: GlobalParams.themes[banque_id].appBarColor,
                        fontWeight: FontWeight.w900,
                        fontSize: 18
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 15),
                      child: SvgPicture.asset('assets/$banque_id/images/exclamation_icon.svg'),
                    ),
                    Text(
                      AppLocalizations.of(context).code_duree,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: GlobalParams.themes[banque_id].appBarColor,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 33,
                width: MediaQuery.of(context).size.width * 0.3,
                alignment: Alignment.center,
                child: MaterialButton(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color:
                  GlobalParams.themes["$banque_id"].appBarColor,
                  onPressed: () {

                  },
                  child: Text(AppLocalizations.of(context).confirmer,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          ))),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  signeTransfert(id, taskId) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                    AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    AppLocalizations.of(context).code_secret_sms,
                    textAlign: TextAlign.center,
                    style:
                    GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            // Navigator.pop(context);

                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                                context.read<TransfertBloc>().add(
                                SigneTransfertEvent(
                                    password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  int validateForm() {
    if (montantController.text.isEmpty ||
        motifController.text.isEmpty ||
        dropdownType == null)
      return -1;
    else if (motifController.text.length < 3)
      return -2;
    else
      return 0;
  }
}
