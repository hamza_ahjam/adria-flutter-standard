import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/messaging/messageEnvoye.widget.dart';
import 'package:LBA/pages/messaging/messageForm.widget.dart';
import 'package:LBA/pages/messaging/messageRecue.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class MessagingPage extends StatefulWidget {
  int index ;

  MessagingPage({Key key, this.index = 0}) : super(key: key);

  @override
  State<MessagingPage> createState() => _MessagingPageState();
}

class _MessagingPageState extends State<MessagingPage> with SingleTickerProviderStateMixin{
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);

    _tabController.index = widget.index;
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).messages,
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          actions: [
            TextButton.icon(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          fullscreenDialog: true,
                          builder: (context) => MessageFormWidget()));
                },
                icon: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                label: Text(""))
          ],
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()))),
          bottom: TabBar(
              indicatorColor: GlobalParams.themes["$banque_id"].colorTab,
              controller: _tabController,
              unselectedLabelColor: Colors.white,
              labelColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
              tabs: [
                Tab(
                  text: AppLocalizations.of(context).msg_env,
                ),
                Tab(
                  text: AppLocalizations.of(context).msg_rec,
                ),
              ]),
        ),
        body: Stack(
          children: <Widget>[
            Container(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    MessageEnvoyeWidget(),
                    MessageRecueWidget(),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
