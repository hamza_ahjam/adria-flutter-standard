

import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/subjectsMessagerie.model.dart';
import 'package:LBA/pages/messaging/messaging.page.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MessageFormWidget extends StatefulWidget {
  MessageFormWidget({Key key}) : super(key: key);

  @override
  _MessageFormWidgetState createState() => _MessageFormWidgetState();
}

class _MessageFormWidgetState extends State<MessageFormWidget> {
  Subject dropdownValueSub;
  String dropdownValueDes;
  TextEditingController message = TextEditingController();
  bool isSubLoaded = false;
  bool isDesLoaded = false;
  bool _loadingPath = false;
  List<PlatformFile> _paths;
  List<PlatformFile> _piecesJointes = [];

  void _openFileExplorer() async {
    setState(() => _loadingPath = true);
    try {
      _paths = (await FilePicker.platform.pickFiles(
              allowMultiple: true,
              type: FileType.custom,
              withData: true,
              onFileLoading: (FilePickerStatus status) => print(status),
              allowedExtensions: ['jpg', 'pdf', 'doc', 'png', 'jpeg']))
          ?.files;
      if (_piecesJointes == null)
        _piecesJointes = _paths;
      else {
        _piecesJointes.forEach((pj) {
          _paths.removeWhere((element) => element.name.compareTo(pj.name) == 0);
        });
        _piecesJointes.addAll(_paths);
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close, color: Colors.white),
            onPressed: () => Navigator.pop(context)),
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            AppLocalizations.of(context).nv_message,
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
      ),
      body: Container(
        color: Colors.grey[100],
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.8,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                      color: GlobalParams.themes["$banque_id"].bgKeyboard)),
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      AppLocalizations.of(context).motif,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 18)),
                    ),
                  ),
                  BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
                    builder: (context, state) {
                      if (state.requestStateSub == StateStatus.NONE ||
                          state.requestStateSub == null) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          if (!isSubLoaded)
                            context
                                .read<MessagerieBloc>()
                                .add(LoadSubjectsMessagerieEvent());
                        });
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).choix_motif),
                          value: dropdownValueSub,
                          items: <DropdownMenuItem>[],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateSub == StateStatus.LOADING) {
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).choix_motif),
                          value: dropdownValueSub,
                          items: <DropdownMenuItem>[
                            DropdownMenuItem(
                              value: 0,
                              child: Center(
                                  child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              )),
                            ),
                          ],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateSub == StateStatus.ERROR) {
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).choix_motif),
                          value: 0,
                          items: <DropdownMenuItem>[
                            DropdownMenuItem(
                              value: 0,
                              child: ErreurTextWidget(
                                errorMessage: state.errorMessage,
                                actionEvent: () {
                                  context
                                      .read<MessagerieBloc>()
                                      .add(state.currentAction);
                                },
                              ),
                            ),
                          ],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateSub == StateStatus.LOADED) {
                        isSubLoaded = true;
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).choix_motif),
                          value: dropdownValueSub,
                          items: state.subject.map.subjects
                              .map((p) => DropdownMenuItem(
                                    value: p,
                                    child: Text("${p.libelle}"),
                                  ))
                              .toList(),
                          onChanged: (value) {
                            setState(() {
                              dropdownValueSub = value;
                            });
                          },
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 6),
                    child: Text(
                      AppLocalizations.of(context).charge_client,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 18)),
                    ),
                  ),
                  BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
                    builder: (context, state) {
                      if (state.requestStateDes == StateStatus.NONE ||
                          state.requestStateDes == null) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          if (!isDesLoaded)
                            context
                                .read<MessagerieBloc>()
                                .add(LoadDestinataireMessagerieEvent());
                        });
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).charge_client,),
                          value: dropdownValueDes,
                          items: <DropdownMenuItem>[],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateDes == StateStatus.LOADING) {
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).charge_client,),
                          value: dropdownValueDes,
                          items: <DropdownMenuItem>[
                            DropdownMenuItem(
                              value: 0,
                              child: Center(
                                  child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              )),
                            ),
                          ],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateDes == StateStatus.ERROR) {
                        return DropdownButton(
                          isExpanded: true,
                          hint: Text(AppLocalizations.of(context).charge_client),
                          value: 0,
                          items: <DropdownMenuItem>[
                            DropdownMenuItem(
                              value: 0,
                              child: ErreurTextWidget(
                                errorMessage: state.errorMessage,
                                actionEvent: () {
                                  context
                                      .read<MessagerieBloc>()
                                      .add(state.currentAction);
                                },
                              ),
                            ),
                          ],
                          onChanged: (value) {},
                        );
                      } else if (state.requestStateDes == StateStatus.LOADED) {
                        isDesLoaded = true;
                        if (state.destinataire.map.destinataireList.length == 0)
                          return Text(AppLocalizations.of(context).aucun_charge_client,);
                        else
                          return DropdownButton(
                            isExpanded: true,
                            hint: Text(AppLocalizations.of(context).charge_client),
                            value: dropdownValueDes,
                            items: state.destinataire.map.destinataireList
                                .map((p) => DropdownMenuItem(
                                      value: p.id,
                                      child: Text("${p.libelle}"),
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                dropdownValueDes = value;
                              });
                            },
                          );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 6),
                    child: Text(
                      AppLocalizations.of(context).message,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 18)),
                    ),
                  ),
                  TextFormField(
                    controller: message,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintText: AppLocalizations.of(context).ajout_msg,
                      hintStyle:
                          TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                      suffixIcon: Icon(Icons.message_outlined,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _openFileExplorer();
                    },
                    child: Container(
                        padding: EdgeInsets.all(11),
                        alignment: Alignment.bottomRight,
                        child: Icon(Icons.ios_share)),
                  ),
                  _piecesJointes.length != 0
                      ? ListTile(
                          leading: Text(
                            AppLocalizations.of(context).pieces_jointes,
                            style: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                            ),
                          ),
                          trailing: Icon(Icons.attach_file),
                        )
                      : ListTile(),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    child: ListView.builder(
                      itemCount: _piecesJointes.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 3),
                          child: ListTile(
                            leading: _piecesJointes[index].extension == 'pdf'
                                ? Icon(Icons.insert_drive_file)
                                : Image.memory(_piecesJointes[index].bytes),
                            title: Text('${_piecesJointes[index].name}'),
                            trailing: GestureDetector(
                              child: Icon(Icons.highlight_remove),
                              onTap: () {
                                setState(() {
                                  _piecesJointes.removeAt(index);
                                });
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: RaisedButton(
                        onPressed: () {
                          if (dropdownValueSub == null ||
                              dropdownValueDes == null ||
                              message.text.isEmpty) {
                            errorAlert(context,
                                AppLocalizations.of(context).champs_vides);
                          } else{
                            context.read<MessagerieBloc>().add(SaveMessageEvent(
                                subject: dropdownValueSub.libelle,
                                destinataire: dropdownValueDes,
                                corps: message.text,
                                piecesJointes: _piecesJointes));
                            showMessageEnvoye();
                          }
                        },
                        color: GlobalParams.themes["$banque_id"].appBarColor,
                        child: Text(AppLocalizations.of(context).envoyer,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.white, fontSize: 16)))),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  showMessageEnvoye() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
            builder: (context, state) {
              if (state.requestStateSave == StateStatus.LOADING) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor:
                          GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ),
                  ),
                );
              } else if (state.requestStateSave == StateStatus.LOADED) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        "assets/$banque_id/images/imo.png",
                        width: 50,
                        height: 50,
                        color: GlobalParams.themes["$banque_id"].appBarColor,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          AppLocalizations.of(context).msg_envoye,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle:
                                  TextStyle(color: Colors.black, fontSize: 20)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          onPressed: () {
                            context
                                .read<MessagerieBloc>()
                                .add(LoadMessageEnvEvent(page: 1));
                            Navigator.pop(context);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MessagingPage(index: 0,)),
                                (route) => false);
                          },
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          child: Text(AppLocalizations.of(context).ok,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17, color: Colors.white))),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (state.requestStateSave == StateStatus.ERROR) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  content: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/$banque_id/images/error.png",
                          width: 50,
                          height: 50,
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            state.errorMessage.toString(),
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Text(AppLocalizations.of(context).ok,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 17, color: Colors.white))),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else
                return Container();
            },
          );
        });
  }
}
