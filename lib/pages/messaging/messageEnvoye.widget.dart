import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/messaging/message_detail.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:html_character_entities/html_character_entities.dart';

class MessageEnvoyeWidget extends StatefulWidget {
  int totalSize = 0;
  int loadedOrder = 0;

  MessageEnvoyeWidget({Key key}) : super(key: key);

  @override
  State<MessageEnvoyeWidget> createState() => _MessageEnvoyeWidgetState();
}

class _MessageEnvoyeWidgetState extends State<MessageEnvoyeWidget> {
  ScrollController scrollController = ScrollController();

  bool isLoaded = false;
  bool isLoading = false;
  int page = 1;


  int totalPages;
  int currentTotalMessages;

  @override
  void initState() {
    context.read<MessagerieBloc>().add(LoadMessageEnvEvent(page: 1));
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if(widget.totalSize % 10 == 0)
          totalPages = widget.totalSize ~/ 10;
        else totalPages = (widget.totalSize / 10).floor() + 1;

        if (page < totalPages && !isLoading && currentTotalMessages != widget.totalSize) {
          page ++;
          print('page $page, current total $currentTotalMessages');
          context
              .read<MessagerieBloc>()
              .add(LoadMoreMessageEnvEvent(page: page ));
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
      builder: (context, state) {
        print('old state ${state.requestStateEn} -- new state ${state.requestStateLoadMore}');
        if (state.requestStateEn == StateStatus.LOADING) {
          return Center(
              child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          ));
        } else if (state.requestStateEn == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<MessagerieBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestStateEn == StateStatus.LOADED) {
          widget.loadedOrder ++;

          //print('(loaded) messages list: ${widget.listMessages}');
          isLoaded = true;
          if (widget.loadedOrder == 1)
            widget.totalSize = state.messagerieEn.map.total;
          print('total size: ${widget.totalSize}, loadedOrder ${widget.loadedOrder}');

          currentTotalMessages = state.messagerieEn.map.massages.length;
            //print('(if null) messages list: ${widget.totalSize}');


          //print('list of messages: $widget.listMessages');

          if (state.messagerieEn.map.massages.length == 0)
            return Center(
              child: Text(
                AppLocalizations.of(context).aucun_message,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 16,
                        color: GlobalParams.themes["$banque_id"].appBarColor)),
              ),
            );
          else
            return Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Container(
                    //height: MediaQuery.of(context).size.height * 0.75,
                    child: ListView.builder(
                      controller: scrollController,
                      itemCount: state.messagerieEn.map.massages.length,
                      itemBuilder: (BuildContext context, int index) {

                        // print(
                        //     "${widget.listMessages[index].identifiant}");
                        return GestureDetector(
                          onTap: () {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              context.read<MessagerieBloc>().add(
                                  LoadDetailMessageEvent(
                                      id: state.messagerieEn.map.massages[index]
                                          .identifiant));
                            });
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MessageDetailWidget()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              padding: EdgeInsets.only(top: 5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                      color: GlobalParams
                                          .themes["$banque_id"].bgKeyboard)),
                              child: ListTile(
                                visualDensity: VisualDensity(horizontal: -4),
                                leading: Padding(
                                  padding:
                                  const EdgeInsets.only(top: 10.0, left: 4),
                                  child: Icon(
                                    Icons.mark_chat_read_outlined,
                                    color: GlobalParams
                                        .themes["$banque_id"].appBarColor,
                                  ),
                                ),
                                title: Text(
                                    "${HtmlCharacterEntities.decode(state.messagerieEn.map.massages[index].sujet)}"),
                                subtitle: Text(
                                    "${state.messagerieEn.map.massages[index].corps} \n${state.messagerieEn.map.massages[index].dateFomated}"),
                                isThreeLine: true,
                                // trailing: Icon(Icons.arrow_forward_ios_sharp),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Container(
                  child: BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
                    builder: (context, state) {
                      if (state.requestStateLoadMore == StateStatus.LOADING){
                        isLoading = true;
                        return Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: CircularProgressIndicator(
                            backgroundColor:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                GlobalParams.themes["$banque_id"].appBarColor),
                          ),
                        );
                      }
                      else if (state.requestStateLoadMore == StateStatus.ERROR){
                        return ErreurTextWidget(
                          errorMessage: state.errorMessage,
                          actionEvent: () {
                            context.read<MessagerieBloc>().add(state.currentAction);
                          },
                        );
                      }
                      else if(state.requestStateLoadMore == StateStatus.LOADED){
                        isLoading = false;
                        //widget.listMessages.addAll(state.messagerieEn.map.massages);
                        //print('list messages length: ${widget.listMessages.length}');
                        return Container();
                      }
                      else return Container();
                    },
                  ),
                )
              ],
            );
        }
        else return Container();
      },
    );
  }
}
