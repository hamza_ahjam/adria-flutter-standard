import 'dart:convert';
import 'dart:io';

import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:html_character_entities/html_character_entities.dart';
import 'package:open_file/open_file.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';


class MessageDetailWidget extends StatelessWidget {
  MessageDetailWidget({Key key}) : super(key: key);

  showSnackBar(file, BuildContext context) {
    final snackBar = SnackBar(
      content: Text("Pj téléchargée avec succes",
          style: GoogleFonts.roboto(color: Colors.white)),
      duration: Duration(seconds: 3),
      backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
      action: SnackBarAction(
          label: "open",
          onPressed: () {
            OpenFile.open(file);
          }),
    );
    return ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var pdfBytes;
  File file;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "Détail du message",
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
            onPressed: () => Navigator.pop(context)),
      ),
      body: BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
        builder: (context, state) {
          if (state.requestStateDetail == StateStatus.LOADING) {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
          } else if (state.requestStateDetail == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<MessagerieBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestStateDetail == StateStatus.LOADED) {
            return ListView.builder(
              itemCount: state.detailMessage.map.history.length,
              itemBuilder: (context, index) {
                return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.03,
                            bottom: MediaQuery.of(context).size.height * 0.1),
                        child: Text(
                          HtmlCharacterEntities.decode(
                              state.detailMessage.map.history[index].sujet),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.05),
                        child: Text(
                          state.detailMessage.map.history[index].corps ?? "",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                            fontSize: 15,
                          )),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.3,
                      ),
                      state.detailMessage.map.history[index].piecesJointes
                                  .length ==
                              0
                          ? Container()
                          : Container(
                              height: MediaQuery.of(context).size.height * 0.4,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: state.detailMessage.map
                                    .history[index].piecesJointes.length,
                                itemBuilder: (context, indexPj) => Padding(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.05),
                                  child: Row(
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            state
                                                    .detailMessage
                                                    .map
                                                    .history[index]
                                                    .piecesJointes[indexPj]
                                                    .nameDocument ??
                                                "",
                                            style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                              fontSize: 10,
                                            )),
                                          ),
                                          Stack(
                                            children: [
                                              Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.15,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.2,
                                                child: (state
                                                        .detailMessage
                                                        .map
                                                        .history[index]
                                                        .piecesJointes[indexPj]
                                                        .nameDocument
                                                        .contains("pdf")
                                                    ? SfPdfViewer.file(
                                                        pdfBytes,
                                                        canShowPaginationDialog:
                                                            true,
                                                        // enableTextSelection: true,
                                                        enableDoubleTapZooming:
                                                            true,
                                                      )
                                                    : Image.memory(
                                                        base64Decode(
                                                          state
                                                              .detailMessage
                                                              .map
                                                              .history[index]
                                                              .piecesJointes[
                                                                  indexPj]
                                                              .contenu,
                                                        ),
                                                      )),
                                              ),
                                              GestureDetector(
                                                onTap: () async {
                                                  try {
                                                    if (Platform.isAndroid) {
                                                      //   await Permission.storage
                                                      //     .request();
                                                      await Permission.storage
                                                          .request();
                                                      String path = await ExternalPath
                                                          .getExternalStoragePublicDirectory(
                                                              ExternalPath
                                                                  .DIRECTORY_DOWNLOADS);

                                                      String fullPath =
                                                          "$path/${state.detailMessage.map.history[index].piecesJointes[indexPj].nameDocument}";

                                                      // File file = File(fullPath);

                                                      // var raf = file.openSync(
                                                      //     mode: FileMode.write);

                                                      file = File(fullPath);
                                                      if (state
                                                          .detailMessage
                                                          .map
                                                          .history[index]
                                                          .piecesJointes[
                                                              indexPj]
                                                          .nameDocument
                                                          .contains("pdf")) {
                                                        var raf = file.openSync(
                                                            mode:
                                                                FileMode.write);

                                                        var pdfDocument = PdfDocument
                                                            .fromBase64String(state
                                                                .detailMessage
                                                                .map
                                                                .history[index]
                                                                .piecesJointes[
                                                                    indexPj]
                                                                .contenu);
                                                        pdfBytes =
                                                            pdfDocument.save();

                                                        raf.writeFromSync(
                                                            pdfBytes);

                                                        await raf.close();
                                                      } else {
                                                        file = await file.writeAsBytes(
                                                            base64Decode(state
                                                                .detailMessage
                                                                .map
                                                                .history[index]
                                                                .piecesJointes[
                                                                    indexPj]
                                                                .contenu));
                                                      }

                                                      // print(file.absolute.path);
                                                      showSnackBar(
                                                          file.absolute.path,
                                                          context);
                                                    } else {
                                                      Share.shareFiles(
                                                          ["${file.path}"],
                                                          text: state
                                                              .detailMessage
                                                              .map
                                                              .history[index]
                                                              .piecesJointes[
                                                                  indexPj]
                                                              .nameDocument);
                                                    }
                                                  } catch (e) {
                                                    print(
                                                        "Pj download err widget err *** $e");
                                                    errorAlert(
                                                        context, e.toString());
                                                  }
                                                },
                                                child: Icon(
                                                  Icons.download,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                    ]);
              },
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
