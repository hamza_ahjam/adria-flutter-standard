import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/messaging/message_detail.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/show-more.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:html_character_entities/html_character_entities.dart';

class MessageRecueWidget extends StatelessWidget {
  MessageRecueWidget({Key key}) : super(key: key);
  bool isLoaded = false;
  int page = 1;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessagerieBloc, MessagerieStateBloc>(
      builder: (context, state) {
        if (state.requestStateRc == StateStatus.NONE) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (!isLoaded)
              context.read<MessagerieBloc>().add(LoadMessageRcEvent(page: 1));
          });
          return Container();
        } else if (state.requestStateRc == StateStatus.LOADING) {
          return Center(
              child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          ));
        } else if (state.requestStateRc == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<MessagerieBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestStateRc == StateStatus.LOADED) {
          isLoaded = true;
          if (state.messagerieRc.map.massages.length == 0)
            return Center(
              child: Text(
                AppLocalizations.of(context).aucun_message,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 16,
                        color: GlobalParams.themes["$banque_id"].appBarColor)),
              ),
            );
          else
            return ListView(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: ListView.builder(
                    itemCount: state.messagerieRc.map.massages.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            context.read<MessagerieBloc>().add(
                                LoadDetailMessageEvent(
                                    id: state.messagerieEn.map.massages[index]
                                        .identifiant));
                          });
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MessageDetailWidget()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            padding: EdgeInsets.only(top: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                    color: GlobalParams
                                        .themes["$banque_id"].bgKeyboard)),
                            child: ListTile(
                              visualDensity: VisualDensity(horizontal: -4),
                              leading: Padding(
                                padding:
                                    const EdgeInsets.only(top: 10.0, left: 4),
                                child: Icon(
                                  Icons.mark_chat_read_outlined,
                                  color: GlobalParams
                                      .themes["$banque_id"].appBarColor,
                                ),
                              ),
                              title: Text(
                                  "${HtmlCharacterEntities.decode(state.messagerieRc.map.massages[index].sujet)}"),
                              subtitle: Text(
                                  "${state.messagerieRc.map.massages[index].corps} \n${state.messagerieRc.map.massages[index].dateFomated}"),
                              isThreeLine: true,
                              // trailing: Icon(Icons.arrow_forward_ios_sharp),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                state.messagerieRc == null
                    ? Container()
                    : state.messagerieRc.map.massages.length > 9
                        ? ShowMore(
                            showMoreAction: () {
                              context
                                  .read<MessagerieBloc>()
                                  .add(LoadMessageRcEvent(page: page));
                              // compteState.reloadHistoriqueVirement(
                              //     tokenState, page);
                              ++page;
                            },
                          )
                        : Container()
              ],
            );
        } else {
          return Container();
        }
      },
    );
  }
}
