import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:barcode_scan2/model/scan_result.dart';
import 'package:barcode_scan2/platform_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';


import '../../../constants.dart';
import '../../../main.dart';


class PaiementQRPage extends StatefulWidget {
  PaiementQRPage({Key key}) : super(key: key);

  @override
  State<PaiementQRPage> createState() => _PaiementQRPageState();
}

class _PaiementQRPageState extends State<PaiementQRPage> {
  final _formPsdKey = GlobalKey<FormState>();

  String barcode = "";

  var passwordController = TextEditingController();
  var montantController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: AppLocalizations.of(context).paiement_comm,
        context: context,
        isPop: true,
      ),
      body: Form(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 25, horizontal: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(
                  child: Icon(Icons.qr_code_scanner, size: MediaQuery.of(context).size.width * 0.7,)
                ),
                MaterialButton(
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onPressed: () {
                      scan();
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).scan_qr,
                        style: GoogleFonts.roboto(
                            textStyle:
                            TextStyle(color: Colors.white, fontSize: 18)))),
                Text(
                  barcode,
                  style: TextStyle(
                    fontSize: 16
                ),),
                BlocListener<WalletBloc, WalletState>(
                  listener: (context, state) {
                    if (state.requestStateScanQR == StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateScanQR ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStateScanQR ==
                        StateStatus.LOADED) {
                      Navigator.pop(context);
                      setState(() {
                        barcode = 'isValid: ${state.qrRes['error']}';
                      });

                      print('isValid: ${state.qrRes['error']} ************************');
                    }
                    if (state.requestStateDecryptPhone == StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStateDecryptPhone ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStateDecryptPhone ==
                        StateStatus.LOADED) {
                      Navigator.pop(context);
                      setState(() {
                        // barcode = 'phone: ${state.decryptedRes['decryptedString']["data"]}';
                        print('isValid: ${state.qrRes['error']} ************************');
                        recapTransfert();
                      });
                    }
                    if (state.requestStatePaiementAchatSave == StateStatus.LOADING) {
                      showLoading(context);
                    } else if (state.requestStatePaiementAchatSave ==
                        StateStatus.ERROR) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      errorAlert(context, state.errorMessage);
                    } else if (state.requestStatePaiementAchatSave ==
                        StateStatus.LOADED) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      signePaiement();
                    }
                  },
                  child: Container(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future scan() async {
    try {
      ScanResult barcode = await BarcodeScanner.scan(options: ScanOptions(android: AndroidOptions(useAutoFocus: true, aspectTolerance: 0.15)));
      setState(() {
        // this.barcode = barcode.rawContent;
        if (barcode.type.name == 'Barcode')
          context.read<WalletBloc>().add(
              ScanQREvent(qr: barcode.rawContent, scanType: 'PAYMENT'));
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  recapTransfert()   {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    AppLocalizations.of(context).recap.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: BlocBuilder<WalletBloc, WalletState>(
            builder: (context, state) {
              montantController.text = state.qrRes['amount'].toString();
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                            "${AppLocalizations.of(context).marchand} :",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: FittedBox(
                          alignment:
                              MyApp.of(context).getLocale().languageCode == 'ar'
                                  ? Alignment.centerLeft
                                  : Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Text("${state.qrRes['merchantName']}",
                              maxLines: 2,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text("${AppLocalizations.of(context).montant} :",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: state.qrRes['dynamicQR']
                            ? Text("${state.qrRes['amount']}",
                                maxLines: 1,
                                textAlign: TextAlign.end,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold)))
                            : TextFormField(
                                controller: montantController,
                                keyboardType: TextInputType.number,
                                textAlignVertical: TextAlignVertical.center,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                  color: Colors.grey[700],
                                )),
                                onChanged: (v) {},
                                decoration: InputDecoration(
                                  suffixIcon: Icon(Icons.edit, color: Colors.black,),
                                  contentPadding: EdgeInsets.only(
                                      left: 10, bottom: 5, right: 0),
                                  hintText:
                                      AppLocalizations.of(context).montant,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[600], fontSize: 14),
                                ),
                              ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                            "${AppLocalizations.of(context).categ_comm} :  ",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Text("${state.qrRes['mcc']}",
                            textAlign: TextAlign.end,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w600))),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: FittedBox(
                          alignment:
                              MyApp.of(context).getLocale().languageCode == 'ar'
                                  ? Alignment.centerRight
                                  : Alignment.centerLeft,
                          fit: BoxFit.scaleDown,
                          child: Text(
                              "${AppLocalizations.of(context).ville} :  ",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: FittedBox(
                          alignment:
                              MyApp.of(context).getLocale().languageCode == 'ar'
                                  ? Alignment.centerLeft
                                  : Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Text("${state.qrRes['merchantCity']}",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.28,
                        child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Colors.white,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(AppLocalizations.of(context).annuler,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          color: GlobalParams
                                              .themes["$banque_id"]
                                              .intituleCmpColor,
                                          fontSize: 16))),
                            )),
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.28,
                        child: MaterialButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            onPressed: () {
                              context
                                  .read<WalletBloc>()
                                  .add(EntrerEnRelationOfflineEvent());
                              context
                                  .read<WalletBloc>()
                                  .add(SaveWalletPaiementAchatEvent(
                                      montant: state.qrRes['dynamicQR'] ? state.qrRes['amount'] : montantController.text,
                                      telephone: state.decryptedRes['decryptedString']["data"]
                                      ));
                            },
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child:
                                  Text(AppLocalizations.of(context).confirmer,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                        fontSize: 14,
                                        color: Colors.white,
                                      ))),
                            )),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }

  signePaiement() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
                Center(
                  child: Text(
                      AppLocalizations.of(context).code_secret_sms,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 16))),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 40,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      // enableInteractiveSelection: false,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      obscureText: true,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 17)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                            context.read<WalletBloc>().add(
                                SignWalletPaiementAchatEvent(
                                    password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}