import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';
import '../../../main.dart';

class PaiementTelephonePage extends StatefulWidget {
  PaiementTelephonePage({Key key}) : super(key: key);

  @override
  State<PaiementTelephonePage> createState() => _PaiementTelephonePageState();
}

class _PaiementTelephonePageState extends State<PaiementTelephonePage> {

  final _formPsdKey = GlobalKey<FormState>();

  TextEditingController montantController = TextEditingController();

  TextEditingController motifController = TextEditingController();

  TextEditingController telephoneController = TextEditingController();

  TextEditingController nomController = TextEditingController();

  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: AppLocalizations.of(context).paiement_comm,
        context: context,
        isPop: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 50),
        child: Form(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 60),
                  alignment: Alignment.center,
                  child: Text(
                    AppLocalizations.of(context).infos_comm,
                    style: TextStyle(
                        fontSize: 16,
                        color: GlobalParams.themes[banque_id].intituleCmpColor
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                BlocBuilder<WalletBloc, WalletState>(
                  builder: (context, state) {
                    if(state.requestStateGetBeneficiary == StateStatus.NONE || state.requestStateGetBeneficiary == null){
                      return Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: telephoneController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (v) {
                                String phone = v;
                                String match = phoneNumberRegex.stringMatch(telephoneController.text);
                                if(match != null && match.compareTo(telephoneController.text) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phone));
                                  });
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.phone),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                hintText: AppLocalizations.of(context).num_tel,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.grey[600], fontSize: 14),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.04,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: nomController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.person),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).nom_prenom,
                                hintStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else if(state.requestStateGetBeneficiary == StateStatus.LOADING){
                      return Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: telephoneController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (v) {
                                String phone = v;
                                String match = phoneNumberRegex.stringMatch(telephoneController.text);
                                if(match != null && match.compareTo(telephoneController.text) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phone));
                                  });
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.refresh),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                hintText: AppLocalizations.of(context).num_tel,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.grey[600], fontSize: 14),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.04,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: nomController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.person),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).nom_prenom,
                                hintStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else if(state.requestStateGetBeneficiary == StateStatus.LOADED){
                      if(state.beneficiaryRes != null){
                        nomController.text = state.beneficiaryRes['name'];

                        state.beneficiaryRes = null;
                      }
                      return Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: telephoneController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (v) {
                                String phone = v;
                                String match = phoneNumberRegex.stringMatch(telephoneController.text);
                                if(match != null && match.compareTo(telephoneController.text) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phone));
                                  });
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.phone),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                hintText: AppLocalizations.of(context).num_tel,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.grey[600], fontSize: 14),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.04,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: nomController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.person),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).nom_prenom,
                                hintStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else if(state.requestStateGetBeneficiary == StateStatus.ERROR){
                      return Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: telephoneController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (v) {
                                String phone;
                                String match = phoneNumberRegex.stringMatch(telephoneController.text);
                                if(match != null && match.compareTo(telephoneController.text) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phone));
                                  });
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.phone),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                hintText: AppLocalizations.of(context).num_tel,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.grey[600], fontSize: 14),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.04,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border:
                                Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            child: TextFormField(
                              controller: nomController,
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.person),
                                contentPadding: EdgeInsets.only(
                                    left: 10, bottom: 15, right: 10),
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).nom_prenom,
                                hintStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }
                    else return Container();
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    AppLocalizations.of(context).infos_paiement,
                    style: TextStyle(
                        fontSize: 16,
                        color: GlobalParams.themes[banque_id].intituleCmpColor
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: montantController,
                    keyboardType: TextInputType.number,
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          color: GlobalParams.themes[banque_id].appBarColor,
                        )),
                    onChanged: (v) {},
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.money),
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                      hintText: AppLocalizations.of(context).montant,
                      border: InputBorder.none,
                      hintStyle:
                      TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: motifController,
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          color: GlobalParams.themes[banque_id].appBarColor,
                        )),
                    onChanged: (v) {},
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.description),
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15, right: 10),
                      hintText: AppLocalizations.of(context).motif,
                      border: InputBorder.none,
                      hintStyle:
                      TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                MaterialButton(
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onLongPress: fillForm,
                    onPressed: () {
                      if (validateForm() == -1) {
                        errorAlert(context,
                          AppLocalizations.of(context).error_remplir_champs,);
                      } else if (validateForm() == -2) {
                        errorAlert(context,
                            "Le motif doit avoir 3 caractères au minimum");
                      } else if (validateForm() == -3) {
                        errorAlert(context,
                            "Vérifier votre numero de telephone");
                      } else recapTransfert();
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).suivant,
                        style: GoogleFonts.roboto(
                            textStyle:
                            TextStyle(color: Colors.white, fontSize: 18)))),
                BlocListener<WalletBloc, WalletState>(
                    listener: (context, state) {
                      if (state.requestStateEnterEnRelation == StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStateEnterEnRelation ==
                          StateStatus.ERROR) {
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                      } else if (state.requestStateEnterEnRelation ==
                          StateStatus.LOADED) {
                        Navigator.pop(context);
                      } else if (state.requestStatePaiementAchatSave == StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStatePaiementAchatSave ==
                          StateStatus.ERROR) {
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                      } else if (state.requestStatePaiementAchatSave ==
                          StateStatus.LOADED) {
                        Navigator.pop(context);
                      } else if (state.requestStatePaiementAchatOtp ==
                          StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStatePaiementAchatOtp ==
                          StateStatus.LOADED) {
                        Navigator.pop(context);
                        signePaiement();
                      } else if (state.requestStatePaiementAchatOtp ==
                          StateStatus.ERROR) {
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                        passwordController.text = "";
                      } else if (state.requestStatePaiementAchatSign ==
                          StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStatePaiementAchatSign ==
                          StateStatus.LOADED) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        okAlert(context, state.res["message"]);

                        passwordController.text = "";
                        telephoneController.text = "";
                        nomController.text = "";
                        montantController.text = "";
                        motifController.text = "";
                      } else if (state.requestStatePaiementAchatSign ==
                          StateStatus.ERROR) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                        passwordController.text = "";
                      } else if (state.requestStatePaiementAchatAbandonner ==
                          StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStatePaiementAchatAbandonner ==
                          StateStatus.LOADED) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        okAlert(context, state.res["message"] != null ? state.res["message"] : "Transaction abandonnée");

                        passwordController.text = "";
                        telephoneController.text = "";
                        nomController.text = "";
                        montantController.text = "";
                        motifController.text = "";
                      } else if (state.requestStatePaiementAchatAbandonner ==
                          StateStatus.ERROR) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                        passwordController.text = "";
                      }
                    },
                  child: Container(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  recapTransfert()   {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    AppLocalizations.of(context).recap.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
                width: MediaQuery.of(context).size.width * 0.8,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).num_tel} :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${telephoneController.text}",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).nom_prenom} :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Text(
                        nomController.text,
                        maxLines: 1,
                        textAlign: TextAlign.end,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.bold))),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("${AppLocalizations.of(context).montant} :  ",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: Text(
                        montantController.text,
                        textAlign: TextAlign.end,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 13, fontWeight: FontWeight.w600))),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("${AppLocalizations.of(context).motif} :  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(motifController.text,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 16))),
                        )),
                  ),
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: MaterialButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          context.read<WalletBloc>().add(
                              EntrerEnRelationOfflineEvent()
                          );
                          context.read<WalletBloc>().add(
                              SaveWalletPaiementAchatEvent(
                                montant: montantController.text,
                                telephone: telephoneController.text,
                                transactionIndicator: 'H'
                              )
                          );
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(AppLocalizations.of(context).confirmer,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                  ))),
                        )),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  int validateForm() {
    if (telephoneController.text.isEmpty ||
        nomController.text.isEmpty ||
        montantController.text.isEmpty ||
        motifController.text.isEmpty )
      return -1;
    else if (motifController.text.length < 3)
      return -2;
    else if (telephoneController.text.length < 10)
      return -3;
    else
      return 0;
  }

  signePaiement() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
                Center(
                  child: Text(
                      AppLocalizations.of(context).code_secret_sms,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 16))),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 40,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      // enableInteractiveSelection: false,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      obscureText: true,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            // Navigator.pop(context);
                            abondonVir(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 17)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                            context.read<WalletBloc>().add(
                                SignWalletPaiementAchatEvent(
                                    password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  abondonVir(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 20),
                child: Text(
                  AppLocalizations.of(context).annuler_transaction_message,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18)),
                ),
              ),
              ListTile(
                leading: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).non,
                        style: TextStyle(color: Colors.white, fontSize: 16))),
                trailing: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      // Navigator.pop(context);
                      Navigator.pop(context);

                      context.read<WalletBloc>().add(
                          AbandonnerWalletPaiementAchatEvent());
                    },
                    color: Colors.white,
                    child: Text(AppLocalizations.of(context).oui,
                        style: TextStyle(
                            fontSize: 16,
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor))),
              )
            ],
          ),
        );
      },
    );
  }

  fillForm (){
    telephoneController.text = '0678954120';
    nomController.text = 'Test Test';
    montantController.text = '1200';
    motifController.text = 'paiement achat';
  }
}
