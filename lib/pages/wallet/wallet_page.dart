import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:LBA/pages/wallet/money_transfer_page.dart';
import 'package:LBA/pages/wallet/paiement_achat/pay_orders_page.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';
import '../../main.dart';

class WalletPage extends StatefulWidget{
  @override
  State<WalletPage> createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('back');
        showDeconnectionDialog(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).wallet, context: context, isWallet: true,
          walletDeconnexion: () => showDeconnectionDialog(context)),
        body: Center(
          child: ListView(
            itemExtent: MediaQuery.of(context).size.height * 0.1,
            padding: EdgeInsets.only(
                right: 45,
                left: 45,
                top: MediaQuery.of(context).size.height * 0.25),
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MoneyTransferPage()));
                },
                child: ListTile(
                  leading: SvgPicture.asset(
                    'assets/$banque_id/images/Factures.svg',
                    height: 25,
                    width: 25,
                  ),
                  title: Text(AppLocalizations.of(context).money_transfer,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: GlobalParams.themes[banque_id].appBarColor),
                      )),
                  trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PayOrdersPage()));
                },
                child: ListTile(
                  leading:
                  SvgPicture.asset('assets/$banque_id/images/fav_icon.svg',
                    height: 30,
                    width: 30,
                  ),
                  title: Text(AppLocalizations.of(context).pay_my_orders,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: GlobalParams.themes[banque_id].appBarColor),
                      )),
                  trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showDeconnectionDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 8, 5),
                  child: Icon(
                    Icons.logout,
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    size: 33,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).deconnexion.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color:
                          GlobalParams.themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02,
                    bottom: MediaQuery.of(context).size.height * 0.03),
                child: Text(
                    AppLocalizations.of(context).deconnexion_msg,
                    textAlign: TextAlign.center,
                    style:
                    GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18))),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  MaterialButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () => Navigator.pop(context),
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).non,
                        style: GoogleFonts.roboto(
                            textStyle:
                            TextStyle(fontSize: 17, color: Colors.white))),
                  ),
                  MaterialButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)),
                    color: Colors.white,
                    child: Text(AppLocalizations.of(context).oui,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 17,
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor))),
                    onPressed: () {
                      context.read<WalletBloc>().add(ResetWalletEvent());

                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => LoginPage()),
                              (Route<dynamic> route) => false);
                    },
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}