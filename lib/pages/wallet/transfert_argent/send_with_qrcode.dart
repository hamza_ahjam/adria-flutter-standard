import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';
import '../../../main.dart';
import '../money_transfer_page.dart';

class SendWithQRCode extends StatefulWidget{
  @override
  State<SendWithQRCode> createState() => _SendWithQRCodeState();

}

class _SendWithQRCodeState extends State<SendWithQRCode> {
  String barcode = "";
  TextEditingController passwordController = TextEditingController();
  final _formPsdKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            AppLocalizations.of(context).transfer_by_qr_code.toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => MoneyTransferPage()));
            }),
      ),
      body: Center(
        child: Column(
          children: [
            Icon(Icons.qr_code_2_rounded,
                size: 200,
                color: GlobalParams.themes["$banque_id"].intituleCmpColor),
            ElevatedButton(onPressed: (){
            scan();
            }, child: Text("Lancer le scan", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),)),
            BlocListener<WalletBloc, WalletState>(
              listener: (context, state) {
                if (state.requestStateScanQR == StateStatus.LOADING) {
                  showLoading(context);
                } else if (state.requestStateScanQR ==
                    StateStatus.ERROR) {
                  Navigator.pop(context);
                  errorAlert(context, state.errorMessage);
                } else if (state.requestStateScanQR ==
                    StateStatus.LOADED) {
                  Navigator.pop(context);
                  setState(() {
                    barcode = 'isValid: ${state.qrRes['error']}';
                  });

                  print('isValid: ${state.qrRes['error']} ************************');
                }
                if (state.requestStateDecryptPhone == StateStatus.LOADING) {
                  showLoading(context);
                } else if (state.requestStateDecryptPhone ==
                    StateStatus.ERROR) {
                  Navigator.pop(context);
                  errorAlert(context, state.errorMessage);
                } else if (state.requestStateDecryptPhone ==
                    StateStatus.LOADED) {
                  Navigator.pop(context);
                  setState(() {
                    barcode = 'phone: ${state.decryptedRes['decryptedString']["data"]}';
                    print('isValid: ${state.qrRes['error']} ************************');
                    recapTransfert();
                  });
                }
                if (state.requestStatePaiementAchatSave == StateStatus.LOADING) {
                  showLoading(context);
                } else if (state.requestStatePaiementAchatSave ==
                    StateStatus.ERROR) {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  errorAlert(context, state.errorMessage);
                } else if (state.requestStatePaiementAchatSave ==
                    StateStatus.LOADED) {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  signePaiement();
                }
              },
              child: Container(),
            )

          ],
        ),
      )
    );
  }

  Future scan() async {
    try {
      ScanResult barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode.rawContent);
      context.read<WalletBloc>().add(ScanQREvent(qr: barcode.rawContent, scanType: 'Transfer'.toUpperCase()));
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }

    print("QRcode result: $barcode");

  }

  recapTransfert()   {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    AppLocalizations.of(context).recap.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: BlocBuilder<WalletBloc, WalletState>(
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                            "${AppLocalizations.of(context).benef} :",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: FittedBox(
                          alignment:
                          MyApp.of(context).getLocale().languageCode == 'ar'
                              ? Alignment.centerLeft
                              : Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Text("${state.decryptedRes['decryptedString']['data']}",
                              maxLines: 2,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text("${AppLocalizations.of(context).montant} :",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.grey[700],
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Text("${state.qrRes['amount']}",
                            maxLines: 1,
                            textAlign: TextAlign.end,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                            "${AppLocalizations.of(context).motif} :  ",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Text("${state.qrRes['purpose']}",
                            textAlign: TextAlign.end,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w600))),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.28,
                        child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Colors.white,
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(AppLocalizations.of(context).annuler,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          color: GlobalParams
                                              .themes["$banque_id"]
                                              .intituleCmpColor,
                                          fontSize: 16))),
                            )),
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.28,
                        child: MaterialButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            onPressed: () {
                              context
                                  .read<WalletBloc>()
                                  .add(EntrerEnRelationOfflineEvent());
                              context
                                  .read<WalletBloc>()
                                  .add(SaveWalletPaiementAchatEvent(
                                  montant: state.qrRes['amount'],
                                  telephone: state.decryptedRes['decryptedString']["data"],
                                  transactionIndicator: 'W',
                                  intituleBeneficiaire: state.decryptedRes['decryptedString']["data"]
                              ));
                            },
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child:
                              Text(AppLocalizations.of(context).confirmer,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                        fontSize: 14,
                                        color: Colors.white,
                                      ))),
                            )),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }

  signePaiement() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).code_validation.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
                Center(
                  child: Text(
                      AppLocalizations.of(context).code_secret_sms,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 16))),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 40,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      // enableInteractiveSelection: false,
                      validator: (v) {
                        if (v.isEmpty)
                          return AppLocalizations.of(context).error_mdp;
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      obscureText: true,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 17)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (_formPsdKey.currentState.validate()) {
                            context.read<WalletBloc>().add(
                                SignWalletPaiementAchatEvent(
                                    password: passwordController.text));
                          }
                        },
                        child: Text(AppLocalizations.of(context).valider,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}