import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/wallet.service.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../constants.dart';
import '../money_transfer_page.dart';

class ReceiveWithQRCode extends StatefulWidget {
  @override
  State<ReceiveWithQRCode> createState() => _ReceiveWithQRCodeState();
}

class _ReceiveWithQRCodeState extends State<ReceiveWithQRCode> {

  WalletService get walletService => GetIt.I<WalletService>();

  String montant;
  String motif;
  bool remplirMontant = true;
  bool showQRCode = false;
  TextEditingController passwordController = TextEditingController();
  TextEditingController motifController = TextEditingController();
  String qrCodeData = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    walletService.entreEnRelationOffline();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            AppLocalizations.of(context).create_qr_code.toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => MoneyTransferPage()));
            }),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: showQRCode == false ? fieldsToFill() : getQRCode(qrCodeData),
          ),

          BlocListener<WalletBloc, WalletState>(
            listener: (context, state) {
              if (state.requestStateGetQRcode == StateStatus.LOADING) {
                showLoading(context);
              } else if (state.requestStateGetQRcode ==
                  StateStatus.ERROR) {
                Navigator.pop(context);
                errorAlert(context, state.errorMessage);
              } else if (state.requestStateGetQRcode ==
                  StateStatus.LOADED) {
                Navigator.pop(context);
                setState(() {
                  qrCodeData = state.qrCodeData;
                  showQRCode = true;
                });
              }
            },
            child: Container(),
          )
        ],
      ),

    );
  }


  Widget getQRCode(String textBase){

    print("qrcodeData $textBase");

    return Column(
      children: [
        GestureDetector(
          onTap:  (){
            setState(() {
              showQRCode = false;
            });
          },
          child: Icon(Icons.refresh,
              size: 50,
              color: GlobalParams.themes[banque_id].intituleCmpColor
          ),
        ),
        Container(
          alignment: Alignment.topCenter,
          child: QrImage(
            data: textBase,
            version: QrVersions.auto,
            size: 200.0,
          ),
        ),
      ],
    );
  }

  Widget fieldsToFill() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Center(
              child: Text("Votre solde est de : 10 000 DH",
                  style: TextStyle(
                      fontSize: 24,
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.bold))),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Row(
            children: [
              Text("Préciser le montant", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
              Spacer(),
              Switch(
                  value: remplirMontant,
                  onChanged: (value) {
                    setState(() {
                      remplirMontant = value;
                    });
                  }),
            ],
          ),
        ),
        Container(
          child: remplirMontant == true
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.grey[200]),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                        hintText: "Montant",
                        border: InputBorder.none,
                        hintStyle:
                            TextStyle(color: Colors.grey[600], fontSize: 14),
                      ),
                      textAlignVertical: TextAlignVertical.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: GlobalParams.themes[banque_id].appBarColor,
                      )),
                      onChanged: (text) {
                        montant = text;
                      },
                      keyboardType: TextInputType.number,
                    ),
                  ),
                )
              : Container(),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.05,
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.grey[200]),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: TextField(
              controller: motifController,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                hintText: "Motif",
                border: InputBorder.none,
                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
              ),
              textAlignVertical: TextAlignVertical.center,
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: GlobalParams.themes[banque_id].appBarColor,
              )),
              onChanged: (text) {
                motif = text;
              },
              keyboardType: TextInputType.text,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 45,
            child: GestureDetector(
              child: ElevatedButton(
                  onPressed: () async {
                    String message = fieldsValidator();
                    if (message.isNotEmpty) {
                      errorAlert(context, message);
                    } else {
                      if(remplirMontant == false) montant = "0.0";
                      context.read<WalletBloc>().add(GetQRcodeDataEvent(amount: montant, motif: motifController.text));
                      setState(() {
                        showQRCode = !showQRCode;
                      });
                    }
                  },
                  child: Text("Réception par QR Code",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18)))),
            ),
          ),
        )
      ],
    );
  }

  String fieldsValidator() {
    if (remplirMontant && (montant == null || montant.isEmpty)) {
      return "Veuillez remplir le champ montant ";
    }

    return "";
  }
}
