import 'package:LBA/bloc/wallet/wallet_bloc.dart';
import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/wallet.service.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';
import '../money_transfer_page.dart';

class TransferByPhoneNumberPage extends StatefulWidget {
  @override
  State<TransferByPhoneNumberPage> createState() =>
      _TransferByPhoneNumberPageState();
}

class _TransferByPhoneNumberPageState extends State<TransferByPhoneNumberPage> {
  String phoneNumber;
  String fullName;
  String montant;
  String motif;

  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController telephoneController = TextEditingController();
  TextEditingController montantController = TextEditingController();
  TextEditingController motifController = TextEditingController();

  final _formPsdKey = GlobalKey<FormState>();
  GlobalKey<FormState> _formChangePsdKey = GlobalKey<FormState>();

  WalletService get walletService => GetIt.I<WalletService>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    walletService.entreEnRelationOffline();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            AppLocalizations.of(context).transefr_by_phone.toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => MoneyTransferPage()));
            }),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Center(
                    child: Text("Votre solde est de : 10 000 DH",
                        style: TextStyle(
                            fontSize: 24,
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.bold))),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Text(
                  "Informations du bénéficiaire",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              BlocBuilder<WalletBloc, WalletState>(
                builder: (context, state){
                  print("${state.requestStateGetBeneficiary}");
                  if(state.requestStateGetBeneficiary == StateStatus.NONE || state.requestStateGetBeneficiary == null){
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Téléphone",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                phoneNumber = text;
                                String match = phoneNumberRegex.stringMatch(phoneNumber);
                                if(match != null && match.compareTo(phoneNumber) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phoneNumber));
                                  });
                                }
                              },
                              controller: telephoneController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              controller: nameController,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Nom",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                fullName = text;
                              },
                              keyboardType: TextInputType.name,
                            ),
                          ),
                        ),
                      ],
                    );
                  }

                  if(state.requestStateGetBeneficiary == StateStatus.LOADING){
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.refresh),
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Téléphone",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                phoneNumber = text;
                                String match = phoneNumberRegex.stringMatch(phoneNumber);
                                if(match != null && match.compareTo(phoneNumber) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phoneNumber));
                                  });
                                }
                              },
                              controller: telephoneController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              controller: nameController,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Nom",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                fullName = text;
                              },
                              keyboardType: TextInputType.name,
                            ),
                          ),
                        ),
                      ],
                    );
                  }

                  if(state.requestStateGetBeneficiary == StateStatus.LOADED){
                    if(state.beneficiaryRes != null){
                      nameController.text = state.beneficiaryRes['name'];
                      fullName = state.beneficiaryRes['name'];
                      state.beneficiaryRes = null;
                    }

                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Téléphone",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                phoneNumber = text;
                                String match = phoneNumberRegex.stringMatch(phoneNumber);
                                if(match != null && match.compareTo(phoneNumber) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phoneNumber));
                                  });
                                }
                              },
                              controller: telephoneController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              controller: nameController,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Nom",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                fullName = text;
                              },
                              keyboardType: TextInputType.name,
                            ),
                          ),
                        ),
                      ],
                    );
                  }

                  if(state.requestStateGetBeneficiary == StateStatus.ERROR){
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Téléphone",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                phoneNumber = text;
                                String match = phoneNumberRegex.stringMatch(phoneNumber);
                                if(match != null && match.compareTo(phoneNumber) == 0){
                                  WidgetsBinding.instance
                                      .addPostFrameCallback((_) {
                                    context.read<WalletBloc>().add(GetBeneficiaryEvent(telephone: phoneNumber));
                                  });
                                }
                              },
                              controller: telephoneController,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.grey[200]),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            child: TextField(
                              controller: nameController,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                hintText: "Nom",
                                border: InputBorder.none,
                                hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams.themes[banque_id].appBarColor,
                                  )),
                              onChanged: (text) {
                                fullName = text;
                              },
                              keyboardType: TextInputType.name,
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                  return Container();
                },
              ),

              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Informations du transfert",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                      hintText: "Montant",
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      color: GlobalParams.themes[banque_id].appBarColor,
                    )),
                    onChanged: (text) {
                      montant = text;
                    },
                    controller: montantController,
                    keyboardType: TextInputType.number,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.grey[200]),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                      hintText: "Motif",
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
                    ),
                    textAlignVertical: TextAlignVertical.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      color: GlobalParams.themes[banque_id].appBarColor,
                    )),
                    onChanged: (text) {
                      motif = text;
                    },
                    controller: motifController,
                    keyboardType: TextInputType.text,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 45,
                  child: ElevatedButton(
                      onPressed: () async {
                        String message = fieldsValidator();
                        if (message.isNotEmpty) {
                          errorAlert(context, message);
                        } else {

                          print("before event call fullname == $fullName");

                          context.read<WalletBloc>().add(
                              SaveWalletPaiementAchatEvent(
                                  montant: montant,
                                  telephone: phoneNumber,
                                  transactionIndicator: 'W',
                                intituleBeneficiaire: fullName
                              )
                          );
                        }
                      },
                      child: Text("Transférer",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18)))),
                ),
              ),

              BlocListener<WalletBloc, WalletState>(
                listener: (context, state) {
                  if (state.requestStatePaiementAchatSave == StateStatus.LOADING) {
                    showLoading(context);
                  } else if (state.requestStatePaiementAchatSave ==
                      StateStatus.ERROR) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    errorAlert(context, state.errorMessage);
                  } else if (state.requestStatePaiementAchatSave ==
                      StateStatus.LOADED) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  } else if (state.requestStatePaiementAchatOtp ==
                      StateStatus.LOADING) {
                    showLoading(context);
                  } else if (state.requestStatePaiementAchatOtp ==
                      StateStatus.LOADED) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    signeTransfer();
                  } else if (state.requestStatePaiementAchatOtp ==
                      StateStatus.ERROR) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    errorAlert(context, state.errorMessage);
                    passwordController.text = "";
                  } else if (state.requestStatePaiementAchatSign ==
                      StateStatus.LOADING) {
                    showLoading(context);
                  } else if (state.requestStatePaiementAchatSign ==
                      StateStatus.LOADED) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    okAlert(context, state.res["message"]);

                    passwordController.text = "";
                    telephoneController.text = "";
                    nameController.text = "";
                    montantController.text = "";
                    motifController.text = "";
                  } else if (state.requestStatePaiementAchatSign ==
                      StateStatus.ERROR) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    errorAlert(context, state.errorMessage);
                    passwordController.text = "";
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  signeTransfer() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "Transfert par N° Tel".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Text("Emetteur", style: TextStyle(fontSize: 18)),
                    Spacer(),
                    Text("0671677078", style: TextStyle(fontSize: 18)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Icon(
                          Icons.arrow_downward_rounded,
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                        ),
                      ),
                      Spacer(),
                      Text("$montant DH",
                          style: TextStyle(
                              fontSize: 22, color: Colors.blueAccent)),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Icon(
                          Icons.arrow_downward_rounded,
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    Text(
                      "Bénéficiaire",
                      style: TextStyle(fontSize: 18),
                    ),
                    Spacer(),
                    Text(phoneNumber,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return "Mot de passe ne peut pas etre vide";
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: Text(
                        "Demander le code à nouveau",
                        style: TextStyle(decoration: TextDecoration.underline),
                      ),
                    ),
                  ),
                  onTap: (){
                    showLoading(context);
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                          child: Text("Annuler",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if(passwordController.text.isNotEmpty){
                            Navigator.pop(context);
                            context.read<WalletBloc>().add(SignWalletPaiementAchatEvent(password: passwordController.text));
                          } else{
                            errorAlert(context, "Veuillez entrer le code que vous avez reçu par SMS");
                          }


                        },
                        child: Text("Valider",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  okAlert(context, txt) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/$banque_id/images/imo.png",
                  width: 50,
                  height: 50,
                  color: GlobalParams.themes["$banque_id"].appBarColor,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "$txt",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(color: Colors.black, fontSize: 20)),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(AppLocalizations.of(context).fermer,
                            style:
                                TextStyle(color: Colors.white, fontSize: 16)),
                      )),
                ),
              ],
            ),
          );
        });
  }

  String fieldsValidator() {

    String match;

    if(phoneNumber != null){
      match = phoneNumberRegex.stringMatch(phoneNumber);
    } else{
      return "Veuillez saisir un numéro de téléphone";
    }

    if (match == null || match.compareTo(phoneNumber) != 0) {
      return "Le numéro de téléphone que vous avez saisi est incorrecte";
    }

    if (nameController.text == null || nameController.text.isEmpty) {
      return "Veuillez remplir le champ nom ";
    }

    if (montant == null || montant.isEmpty) {
      return "Veuillez remplir le champ montant ";
    }

    return "";
  }

}
