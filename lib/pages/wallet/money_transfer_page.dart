import 'package:LBA/config/global.params.dart';
import 'package:LBA/pages/wallet/transfert_argent/receive_with_qrcode.dart';
import 'package:LBA/pages/wallet/transfert_argent/send_with_qrcode.dart';
import 'package:LBA/pages/wallet/transfert_argent/transfer_by_phone_number.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';
import '../../main.dart';

class MoneyTransferPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
            appBarTitle: AppLocalizations.of(context).money_transfer, context: context, isPop: true,),
        body: Center(
          child: ListView(
            itemExtent: MediaQuery.of(context).size.height * 0.1,
            padding: EdgeInsets.only(
                right: 45,
                left: 45,
                top: MediaQuery.of(context).size.height * 0.25),
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => TransferByPhoneNumberPage()));
                },
                child: ListTile(
                  leading: SvgPicture.asset(
                    'assets/$banque_id/images/Factures.svg',
                    height: 25,
                    width: 25,
                  ),
                  title: Text(AppLocalizations.of(context).transefr_by_phone,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: GlobalParams.themes[banque_id].appBarColor),
                      )),
                  trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SendWithQRCode()));
                },
                child: ListTile(
                  leading:
                  SvgPicture.asset('assets/$banque_id/images/fav_icon.svg',
                    height: 30,
                    width: 30,
                  ),
                  title: Text(AppLocalizations.of(context).transfer_by_qr_code,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: GlobalParams.themes[banque_id].appBarColor),
                      )),
                  trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ReceiveWithQRCode()));
                },
                child: ListTile(
                  leading:
                  SvgPicture.asset('assets/$banque_id/images/fav_icon.svg',
                    height: 30,
                    width: 30,
                  ),
                  title: Text(AppLocalizations.of(context).create_qr_code,
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: GlobalParams.themes[banque_id].appBarColor),
                      )),
                  trailing: SvgPicture.asset(
                    MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 'assets/$banque_id/images/arrow_back.svg'
                        : 'assets/$banque_id/images/arrow_forward.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}