import 'package:LBA/bloc/devise/devise.bloc.dart';
import 'package:LBA/bloc/devise/devise.event.dart';
import 'package:LBA/bloc/devise/devise.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class DevisesPage extends StatelessWidget {
  DevisesPage({Key key}) : super(key: key);

  bool isLoaded = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "Cours Devises",
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_rounded),
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DashboardComptePage()))),
      ),
      body: BlocBuilder<DeviseBloc, DeviseStateBloc>(
        builder: (context, state) {
          if (state.requestState == StateStatus.NONE) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded) context.read<DeviseBloc>().add(LoadDeviseEvent());
            });
            return Container();
          } else if (state.requestState == StateStatus.LOADING) {
            return Center(
                heightFactor: 15,
                child: CircularProgressIndicator(
                  backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      GlobalParams.themes["$banque_id"].appBarColor),
                ));
          } else if (state.requestState == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<DeviseBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestState == StateStatus.LOADED) {
            isLoaded = true;
            if (state.devise.map.matriceChangeList.length == 0)
              return Center(
                heightFactor: 20,
                child: Text(
                  "aucune devise trouver",
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          fontSize: 16,
                          color:
                              GlobalParams.themes["$banque_id"].appBarColor)),
                ),
              );
            else
              return Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  itemCount: state.devise.map.matriceChangeList.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.18,
                      color:
                          GlobalParams.themes["$banque_id"].mouvementBodyColor,
                      child: ListView(
                        physics: NeverScrollableScrollPhysics(),
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height * 0.04,
                            color: GlobalParams
                                .themes["$banque_id"].bgTitleContainer,
                            child: Center(
                              child: Text(
                                state.devise.map.matriceChangeList[index]
                                    .libelle,
                                style: GoogleFonts.roboto(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          ListTile(
                            title: Text(
                              "Achat",
                              style: GoogleFonts.roboto(),
                            ),
                            trailing: Text(
                              "${state.devise.map.matriceChangeList[index].achat} ${state.devise.map.matriceChangeList[index].codeAlphaSource}",
                              style: GoogleFonts.roboto(),
                            ),
                          ),
                          ListTile(
                            title: Text(
                              "Vente",
                              style: GoogleFonts.roboto(),
                            ),
                            trailing: Text(
                              "${state.devise.map.matriceChangeList[index].vente} ${state.devise.map.matriceChangeList[index].codeAlphaSource}",
                              style: GoogleFonts.roboto(),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
