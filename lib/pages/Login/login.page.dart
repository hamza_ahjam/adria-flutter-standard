
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/main.dart';
import 'package:LBA/pages/Login/activated.widget.dart';
import 'package:LBA/services/forced_upgrade_service.dart';
import 'package:LBA/widgets/header.dart';
import 'package:LBA/widgets/login/login-form.dart';
import 'package:LBA/widgets/menu/offlineMenu.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_secure_keyboard/flutter_secure_keyboard.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  ForcedUpgradeService get forcedUpgrade => GetIt.I<ForcedUpgradeService>();
  final storage = new FlutterSecureStorage();
  String isAccountActive;
  @override
  void initState() {
    // arreter le timer lancé pour éviter l'affichage l'alert d'inactivité
    MyApp.cancelTimer();

    super.initState();
    storage.read(key: "isActive").then((res) {
      setState(() {
        isAccountActive = res;
      });
    }).catchError((err) => print("storage err $err"));
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIOverlays([]);

    checkConnection() async {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        return showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset(
                        "assets/$banque_id/images/error.png",
                        width: 50,
                        height: 50,
                        color: GlobalParams.themes["$banque_id"].appBarColor,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Verifier votre connexion internet et réessayer",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle:
                                  TextStyle(color: Colors.black, fontSize: 20)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text("Fermer",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                            )),
                      ),
                    ],
                  ),
                ),
              );
            });
      }
    }

    checkConnection();
    //checkAppVersion();

    return WillPopScope(
      onWillPop: () {
        SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
        return Future.value(true);
      },
      child: Scaffold(
        body: login()
        // FutureBuilder(
        //   future: forcedUpgrade.getAppVersion(),
        //   builder: (BuildContext context, AsyncSnapshot<ForcedUpgradeResponse> snapshot) {
        //     if (snapshot.hasData) {
        //       ForcedUpgradeResponse response = snapshot.data;
        //       var version = Platform.isAndroid ? double.parse(response.androidVersion)
        //           : double.parse(response.iOSVersion);
        //       if(version > appVersion){
        //         return Center(child: ForcedUpgradePage());
        //       } else{
        //         return login();
        //       }
        //     } else {
        //       return Center(child: CircularProgressIndicator());
        //     }
        //   },
        // ),
      ),
    );
  }


  Widget login(){
    return Scaffold(
      // backgroundColor: Colors.grey[100],
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: AppBar(
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          systemOverlayStyle: SystemUiOverlayStyle.light,
          elevation: 0,
        ),
      ),
      bottomNavigationBar: OffLineMenu(3),
      // resizeToAvoidBottomPadding: true,
      // resizeToAvoidBottomInset: true,
      body: Container(
        decoration: BoxDecoration(
          color: GlobalParams.themes["$banque_id"].appBarColor,
          image: DecorationImage(
            image: AssetImage("assets/$banque_id/images/login_bkg.png"),
            fit: BoxFit.fill,
          ),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(25.0),
            bottomLeft: banque_id == 'NSIA'
                ? Radius.circular(25.0)
                : Radius.circular(0),
          ),
        ),
        child: ListView(
          children: <Widget>[
            Header(),
            isAccountActive != "true" ? LoginForm() : ActivatedPage(),
            // Footer(),
          ],
        ),
      ),
    );
  }

}
