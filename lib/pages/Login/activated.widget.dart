import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/bloc/token/token.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/default_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class ActivatedPage extends StatefulWidget {
  const ActivatedPage({Key key}) : super(key: key);

  @override
  _ActivatedPageState createState() => _ActivatedPageState();
}

class _ActivatedPageState extends State<ActivatedPage> {
  final storage = new FlutterSecureStorage();
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _formChangePsdKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    userNameController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.all(30),
      child: ListView(
        children: <Widget>[
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
            child: Container(
              decoration: BoxDecoration(
                  color: Color.fromRGBO(139, 106, 142, 1),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: userNameController,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(color: Colors.white)),
                // textInputAction: TextInputAction.next,

                validator: (v) {
                  if (v.isEmpty) return "Identifiant ne peut pas etre vide";
                  return null;
                },
                // keyboardAppearance: Brightness.dark,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  hintText: 'Identifiant',
                  hintStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: KprimaryFont),
                  prefixIcon: Icon(Icons.person, color: Colors.white),
                  enabledBorder: OutlineInputBorder(
                      // borderSide:
                      //     BorderSide(color: Color.fromRGBO(89, 43, 95, 0.35)),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  focusedBorder: OutlineInputBorder(
                      // borderSide:
                      //     BorderSide(color: Color.fromRGBO(89, 43, 95, 1)),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: DefaultButton(
                child: Text('Suivant',
                    style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    )),
                gradient: LinearGradient(
                  colors: <Color>[
                    GlobalParams.themes["$banque_id"].otherButtonColor,
                    GlobalParams.themes["$banque_id"].otherButtonColor,
                  ],
                ),
                onPressed: () {
                  if (userNameController.text.isNotEmpty) {
                    context.read<TokenBloc>().add(ActivateAccountEvent(
                          rad: userNameController.text,
                        ));
                  } else {
                    errorAlert(context, "Identifiant ne peut pas etre vide");
                  }
                }),
          ),
          BlocListener<TokenBloc, TokenStateBloc>(
            listener: (context, state) {
              if (state.requestState == StateStatus.LOADING) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  showLoading(context);
                });
              } else if (state.requestState == StateStatus.LOADED) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Navigator.pop(context);
                  if (state.resActivated["Map"]["success"])
                    saveActivatedOtp();
                  else
                    return Text(
                        "success false modif psd "); // go to change pasd
                  userNameController.text = "";
                });
              } else if (state.requestState == StateStatus.ERROR) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Navigator.pop(context);
                  errorAlert(
                      context, state.errorMessage ?? "Invalid user credential");
                });
              }
              //
              else if (state.requestStatePsd == StateStatus.LOADING) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  showLoading(context);
                });
              } else if (state.requestStatePsd == StateStatus.LOADED) {
                WidgetsBinding.instance.addPostFrameCallback((_) async {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  if (state.resOtp["Map"]["success"]) {
                    okAlert(context,
                        "Votre téléphone est vérifié, vous pouvez acceder à vos comptes");
                    // SharedPreferences prefs =
                    //     await SharedPreferences.getInstance();
                    // prefs.setBool("_isActivated", true);

                    // isActive = true;
                    storage.write(key: "isActive", value: "true");
                    // storage.deleteAll();
                    Future.delayed(Duration(seconds: 1), () {
                      Navigator.pop(context);
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    });
                  } else {
                    errorAlert(context, state.resOtp["Map"]["msg"]);
                  }
                  passwordController.text = "";
                });
              } else if (state.requestStatePsd == StateStatus.ERROR) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Navigator.pop(context);
                  errorAlert(
                      context, state.errorMessage ?? "Une erreur est survenue");
                });
              } else {
                return Container();
              }
            },
            child: Container(),
          )
        ],
      ),
    );
  }

  saveActivatedOtp() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.33,
            child: SingleChildScrollView(
              child: Form(
                key: _formChangePsdKey,
                child: Column(
                  children: [
                    Image.asset("assets/$banque_id/images/lock.png"),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 25, bottom: 20, left: 13),
                      child: Center(
                        child: Text(
                            "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(fontSize: 16))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15, bottom: 25),
                      child: TextFormField(
                        controller: passwordController,
                        keyboardType: TextInputType.number,
                        // enableInteractiveSelection: false,
                        validator: (v) {
                          if (v.isEmpty)
                            return "Mot de passe ne peut pas etre vide";
                          return null;
                        },
                        style: TextStyle(color: Colors.black),
                        obscureText: true,
                        decoration: InputDecoration(
                          // errorStyle: TextStyle( ),
                          hintText: 'Mot de passe',
                          hintStyle:
                              TextStyle(fontSize: 16, color: Colors.black),
                          prefixIcon: Icon(
                            Icons.lock_open,
                            color: GlobalParams.themes["$banque_id"].iconsColor,
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 13),
                          height: 40,
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: Colors.white,
                              child: Text("Annuler",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          color: GlobalParams
                                              .themes["$banque_id"]
                                              .validationButtonColor,
                                          fontSize: 16)))),
                        ),
                        Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: RaisedButton(
                            color: GlobalParams
                                .themes["$banque_id"].validationButtonColor,
                            onPressed: () {
                              if (_formChangePsdKey.currentState.validate()) {
                                context
                                    .read<TokenBloc>()
                                    .add(ActivateAccountSaveOtpEvent(
                                      otp: passwordController.text,
                                    ));
                              }
                            },
                            child: Text("Valider",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                ))),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
