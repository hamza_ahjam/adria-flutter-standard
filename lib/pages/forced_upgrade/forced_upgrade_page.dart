import 'package:LBA/config/global.params.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:launch_review/launch_review.dart';

import '../../constants.dart';

class ForcedUpgradePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: AppBar(
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          systemOverlayStyle: SystemUiOverlayStyle.light,
          elevation: 0,
        ),
      ),
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.15,
          child: Column(
            children: [
              Text(
                  "Une nouvelle version de l'application est disponible"),
              ElevatedButton(
                  onPressed: () {
                    LaunchReview.launch(androidAppId: playStoreID, iOSAppId: appStoreID);
                  }, child: Text("Installer la nouvelle version"))
            ],
          ),
        ),
      ),
    );
  }
}
