import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/creanceDetails.model.dart';
import 'package:LBA/pages/factures/factureRecap.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class FactureChampWidget extends StatefulWidget {
  ListCreance creance;
  String title;
  var logo;
  List<String> dynamicFormValues;
  FactureChampWidget({this.creance, this.title, this.logo, this.dynamicFormValues});

  @override
  _FactureChampWidgetState createState() => _FactureChampWidgetState();
}

class _FactureChampWidgetState extends State<FactureChampWidget> {
  bool fav = false;
  Map<String, TextEditingController> controllers = {};
  Map<String, bool> controllerFactureImpayes = {};
  bool formLoaded = false;
  bool formFacLoaded = false;
  TextEditingController libelleFav = TextEditingController();
  ScrollController _scrollController = ScrollController();
  var globalParams;
  @override
  void dispose() {
    formLoaded = false;
    controllers.clear();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "${widget.creance.nom}".toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
              onPressed: () => Navigator.pop(context)),
        ),
        body: ListView(
          controller: _scrollController,
          shrinkWrap: true,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: CachedNetworkImage(
                      imageUrl: widget.logo,
                      placeholder: (context, url) => CircularProgressIndicator(
                        backgroundColor:
                        Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            GlobalParams.themes["$banque_id"].appBarColor),
                      ),
                      errorWidget: (context, url, error) => Image.asset(
                          "assets/$banque_id/images/invoice.png"),
                      width: 100,
                      height: 70,
                    ),
                  ),
                  Text("${widget.title}",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 18))),
                ],
              ),
            ),
            Text(
              "${widget.creance.nom}",
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(
                  textStyle:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(91, 102, 112, 0.1),
                        offset: const Offset(
                          0.0,
                          0.0,
                        ),
                        blurRadius: 8,
                        spreadRadius: 2.0,
                      ), //BoxShadow
                      BoxShadow(
                        color: Color.fromRGBO(91, 102, 112, 0.1),
                        offset: const Offset(0.0, 0.0),
                        blurRadius: 0.0,
                        spreadRadius: 0.0,
                      ),
                    ]),
                // height: MediaQuery.of(context).size.height * 0.65,
                child: BlocBuilder<FactureBloc, FactureStateBloc>(
                  builder: (context, state) {
                    if (state.requestStateForm == StateStatus.LOADING) {
                      return Center(
                          heightFactor: 5,
                          child: CircularProgressIndicator(
                            backgroundColor: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                GlobalParams.themes["$banque_id"].appBarColor),
                          ));
                    } else if (state.requestStateForm == StateStatus.ERROR) {
                      return ErreurTextWidget(
                        errorMessage: state.errorMessage,
                        actionEvent: () {
                          context.read<FactureBloc>().add(state.currentAction);
                        },
                      );
                    } else if (state.requestStateForm == StateStatus.LOADED) {
                      if (state.creancierForm.map.champs.length == 0)
                        return Center(
                          heightFactor: 10,
                          child: Text(
                            AppLocalizations.of(context).aucun_res,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 16, color: principaleColor5)),
                          ),
                        );
                      else
                        return ListView(
                          shrinkWrap: true,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                itemCount:
                                    state.creancierForm.map.champs.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  // print(state.creancierForm.map.champs[index]
                                  //     .typeChamps);
                                  if (!formLoaded)
                                    state.creancierForm.map.champs
                                        .forEach((champ) {
                                      controllers["${champ.nomChamps}"] =
                                          TextEditingController();
                                    });

                                  if (widget.dynamicFormValues != null)
                                    controllers["${state.creancierForm.map.champs[index].nomChamps}"].text = widget.dynamicFormValues[index].split('=').last;

                                  formLoaded = true;
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(12, 8, 12, 8),
                                    child: state.creancierForm.map.champs[index]
                                                .typeChamps ==
                                            "text"
                                        ? Container(
                                            height: 70,
                                            child: TextFormField(
                                              controller: controllers[
                                                  "${state.creancierForm.map.champs[index].nomChamps}"],
                                              textInputAction:
                                                  TextInputAction.unspecified,
                                              keyboardType:
                                                  TextInputType.visiblePassword,
                                              maxLength: state.creancierForm.map
                                                  .champs[index].tailleMax,
                                              decoration: InputDecoration(
                                                errorStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .errorColor, // or any other color
                                                ),
                                                hintText:
                                                    "${state.creancierForm.map.champs[index].libelle}",
                                                hintStyle: TextStyle(
                                                    fontSize: 15,
                                                    fontFamily: KprimaryFont),
                                                suffixIcon: Icon(
                                                    Icons
                                                        .library_books_outlined,
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .iconsColor),
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            height: 70,
                                            child: TextFormField(
                                              controller: controllers[
                                                  "${state.creancierForm.map.champs[index].nomChamps}"],
                                              textInputAction:
                                                  TextInputAction.unspecified,
                                              keyboardType:
                                                  TextInputType.visiblePassword,
                                              maxLength: state.creancierForm.map
                                                  .champs[index].tailleMax,
                                              decoration: InputDecoration(
                                                errorStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .errorColor, // or any other color
                                                ),
                                                hintText:
                                                    "${state.creancierForm.map.champs[index].libelle}",
                                                hintStyle: TextStyle(
                                                    fontSize: 15,
                                                    fontFamily: KprimaryFont),
                                                suffixIcon: Icon(
                                                    Icons
                                                        .library_books_outlined,
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .iconsColor),
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                              enableInteractiveSelection: false,
                                              onTap: () {
                                                FocusScope.of(context)
                                                    .requestFocus(FocusNode());
                                                // showMontant(
                                                //     context,
                                                //     state.creancierForm.map
                                                //         .champs[index].listVals,
                                                //     controllers[
                                                //             "${state.creancierForm.map.champs[index].nomChamps}"]
                                                //         .text);

                                                showModalBottomSheet(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              30.0),
                                                    ),
                                                    isDismissible: true,
                                                    context: context,
                                                    builder: (context) {
                                                      return ListView(
                                                        children:
                                                            state
                                                                .creancierForm
                                                                .map
                                                                .champs[index]
                                                                .listVals
                                                                .map((p) =>
                                                                    ListTile(
                                                                      leading:
                                                                          Icon(
                                                                        Icons
                                                                            .add_circle_outlined,
                                                                        color: GlobalParams
                                                                            .themes["$banque_id"]
                                                                            .iconsColor,
                                                                      ),
                                                                      title: Text(
                                                                          "$p"),
                                                                      onTap:
                                                                          () {
                                                                        Navigator.pop(
                                                                            context);
                                                                        controllers["${state.creancierForm.map.champs[index].nomChamps}"]
                                                                            .text = p;
                                                                      },
                                                                    ))
                                                                .toList(),
                                                      );
                                                    });
                                              },
                                            ),
                                          ),
                                    // DropdownButton(
                                    //     isExpanded: true,
                                    //     hint: Text(
                                    //         "${state.creancierForm.map.champs[index].libelle}"),
                                    //     value: controllers[
                                    //                 "${state.creancierForm.map.champs[index].nomChamps}"]
                                    //             .text ??
                                    //         index.toString(),
                                    //     items: state.creancierForm.map
                                    //         .champs[index].listVals
                                    //         .map((p) {
                                    //       // print(p);
                                    //       DropdownMenuItem(
                                    //         value: p ?? index,
                                    //         child: Text(p),
                                    //       );
                                    //     }).toList(),
                                    //     onChanged: (value) {
                                    //       setState(() {
                                    //         controllers[
                                    //                 "${state.creancierForm.map.champs[index].nomChamps}"]
                                    //             .text = value;
                                    //       });
                                    //     },
                                    //   ),
                                  );
                                },
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Ajouter aux Favoris ",
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(fontSize: 16))),
                                Checkbox(
                                  value: fav,
                                  onChanged: (v) {
                                    setState(() {
                                      fav = v;
                                    });
                                  },
                                ),
                              ],
                            ),
                            fav
                                ? Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(12, 8, 12, 8),
                                    child: Container(
                                      height: 54,
                                      child: TextFormField(
                                        controller: libelleFav,
                                        decoration: InputDecoration(
                                          errorStyle: TextStyle(
                                            color: Theme.of(context)
                                                .errorColor, // or any other color
                                          ),
                                          hintText: "Libellé du Favoris",
                                          hintStyle: TextStyle(
                                              fontSize: 15,
                                              fontFamily: KprimaryFont),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                        ),
                                      ),
                                    ),
                                  )
                                : Container(),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(10, 8, 10, 16),
                              child: Container(
                                height: 40,
                                child: RaisedButton(
                                  onPressed: () {
                                    var formValues = [];
                                    String saisiForm = "";
                                    controllers.forEach((key, value) {
                                      formValues.add(key + "=" + value.text);
                                      saisiForm = formValues.join("-");
                                    });

                                    var bodyObj = {
                                      "checkedFavoris": fav,
                                      "creanceId":
                                          "${state.creancierForm.map.creance.code}",
                                      "creancierId":
                                          "${widget.creance.creancierId}",
                                      "dynamicFormValues": formValues,
                                      "libelleCreance": widget.creance.nom,
                                      "libelleCreancier": widget.title,
                                      if (fav)
                                        "libelleFavoris":
                                            libelleFav.text.toString(),
                                      "logoPath": "${widget.logo}",
                                      "saisiForm": saisiForm,
                                      "type": "${state.creancierForm.map.type}"
                                    };
                                    // print(bodyObj);
                                    // print(controllers["ND"].text);
                                    context.read<FactureBloc>().add(
                                        CreanceFormCreateEvent(
                                            bodyObj: bodyObj));
                                    globalParams = [
                                      {
                                        "libelle": "NUMERO DE TELEPHONE",
                                        "nomChamp": "ND",
                                        "valeurChamp":
                                            "${controllers["ND"].text}",
                                      }
                                    ];
                                  },
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  child: Text(
                                    "Valider",
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.white, fontSize: 20)),
                                  ),
                                ),
                              ),
                            ),
                            BlocListener<FactureBloc, FactureStateBloc>(
                              listener: (context, state) {
                                if (state.requestStateFormCreate ==
                                    StateStatus.LOADING) {
                                  showLoading(context);
                                } else if (state.requestStateFormCreate ==
                                    StateStatus.LOADED) {
                                  Navigator.pop(context);
                                  showFacture(context, state.res);
                                  // okAlert(
                                  //     context,
                                  //     state.res["Map"][
                                  //                 "demandePaiementFactureInstance"]
                                  //             ["montantTotalTTC"] ??
                                  //         "Ppération effectué avec succès");
                                  // Future.delayed(Duration(seconds: 1), () {
                                  //   Navigator.pop(context);
                                  //   Navigator.push(
                                  //       context,
                                  //       MaterialPageRoute(
                                  //           builder: (context) => FacturePage()));
                                  // });
                                } else if (state.requestStateFormCreate ==
                                    StateStatus.ERROR) {
                                  Navigator.pop(context);
                                  errorAlert(context, state.errorMessage);
                                }
                              },
                              child: Container(),
                            )
                          ],
                        );
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  showFacture(context, res) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        title: Center(
          child: Text(
            "Récapitulatif",
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: GlobalParams
                        .themes["$banque_id"].validationButtonColor)),
          ),
        ),
        content: FactureRecapWidget(
            res: res,
            logo: widget.logo,
            paramsGlobal: globalParams,
            title: widget.title),
      ),
    );
  }

  // showMontant(context, montants, controller) {
  //   return showDialog(
  //     context: context,
  //     builder: (context) => AlertDialog(
  //       title: Center(
  //         child: Text(
  //           "Montant",
  //           style: GoogleFonts.roboto(
  //               textStyle: TextStyle(
  //                   color: GlobalParams
  //                       .themes["$banque_id"].validationButtonColor)),
  //         ),
  //       ),
  //       content: Container(
  //         height: MediaQuery.of(context).size.height * 0.4,
  //         width: MediaQuery.of(context).size.width * 0.9,
  //         child: ListView(
  //             children: montants
  //                 .map<Widget>((p) => ListTile(
  //                       leading: Icon(
  //                         Icons.add_circle_outlined,
  //                         color: GlobalParams.themes["$banque_id"].iconsColor,
  //                       ),
  //                       title: Text("$p"),
  //                       onTap: () {
  //                         controller = p;
  //                         Navigator.pop(context);
  //                       },
  //                     ))
  //                 .toList()),
  //       ),
  //     ),
  //   );
  // }
}
