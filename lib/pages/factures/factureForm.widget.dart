import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/creancier.model.dart';
import 'package:LBA/pages/factures/factureChamp.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class FactureFormWidget extends StatelessWidget {
  ListElement facture;
  FactureFormWidget({this.facture});

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "${facture.nom}".toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
              onPressed: () => Navigator.pop(context)),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: CachedNetworkImage(
                      imageUrl: facture.logoPath,
                      placeholder: (context, url) => CircularProgressIndicator(
                        backgroundColor:
                        Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            GlobalParams.themes["$banque_id"].appBarColor),
                      ),
                      errorWidget: (context, url, error) => Image.asset(
                          "assets/$banque_id/images/invoice.png"),
                      width: 100,
                      height: 70,
                    ),
                  ),
                  Text("${facture.nom}",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontSize: 18))),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(91, 102, 112, 0.1),
                        offset: const Offset(
                          0.0,
                          0.0,
                        ),
                        blurRadius: 8,
                        spreadRadius: 2.0,
                      ), //BoxShadow
                      BoxShadow(
                        color: Color.fromRGBO(91, 102, 112, 0.1),
                        offset: const Offset(0.0, 0.0),
                        blurRadius: 0.0,
                        spreadRadius: 0.0,
                      ),
                    ]),
                height: MediaQuery.of(context).size.height * 0.5,
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        "Services:",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                        )),
                      ),
                    ),
                    BlocBuilder<FactureBloc, FactureStateBloc>(
                      builder: (context, state) {
                        if (state.requestStateDt == StateStatus.LOADING) {
                          return Center(
                              heightFactor: 5,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestStateDt == StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<FactureBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestStateDt == StateStatus.LOADED) {
                          if (state.creance.map.listCreances.length == 0)
                            return Center(
                              heightFactor: 10,
                              child: Text(
                                AppLocalizations.of(context).aucun_res,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams
                                            .themes["$banque_id"].appBarColor)),
                              ),
                            );
                          else
                            return ListView.builder(
                              controller: _scrollController,
                              itemCount: state.creance.map.listCreances.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                    height: 50,
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      onPressed: () {
                                        context.read<FactureBloc>().add(
                                            LoadCreanceFormEvent(
                                                codeCreance: state.creance.map
                                                    .listCreances[index].code,
                                                codeCreancier: state
                                                    .creance
                                                    .map
                                                    .listCreances[index]
                                                    .creancierId));
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (contex) =>
                                                    FactureChampWidget(
                                                      title: facture.nom,
                                                      creance: state.creance.map
                                                          .listCreances[index],
                                                      logo: facture.logoPath,
                                                    )));
                                      },
                                      child: Text(
                                        "${state.creance.map.listCreances[index].nom}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17)),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
