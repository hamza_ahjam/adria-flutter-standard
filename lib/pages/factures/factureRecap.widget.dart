import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FactureRecapWidget extends StatefulWidget {
  var res;
  var logo;
  var paramsGlobal;
  String title;
  FactureRecapWidget({this.res, this.logo, this.paramsGlobal, this.title})
      : super();

  @override
  _FactureRecapWidgetState createState() => _FactureRecapWidgetState();
}

class _FactureRecapWidgetState extends State<FactureRecapWidget> {
  bool formFacLoaded = false;
  Map<String, bool> controllerFactureImpayes = {};
  double total = 0;
  var compteDebiterController = TextEditingController();
  var selectedAccount;
  bool isLoaded = false;
  var facDet;
  var impayes = [];

  int index;
  @override
  Widget build(BuildContext context) {
    var res = widget.res;
    return Container(
      height: MediaQuery.of(context).size.height * 0.8,
      width: MediaQuery.of(context).size.width * 0.9,
      child: ListView.builder(
          itemCount: res["Map"]["demandePaiementFactureInstance"]
                  ["paramsGlobal"]
              .length,
          itemBuilder: (context, index) {
            if (!formFacLoaded)
              res["Map"]["demandePaiementFactureInstance"]["impayesForSave"]
                  .forEach((fac) {
                controllerFactureImpayes["${fac["idArticle"]}"] = false;
              });
            formFacLoaded = true;
            return Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: ListView(
                children: <Widget>[
                  Center(
                    child: CachedNetworkImage(
                      imageUrl: widget.logo,
                      placeholder: (context, url) => CircularProgressIndicator(
                        backgroundColor:
                        Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            GlobalParams.themes["$banque_id"].appBarColor),
                      ),
                      errorWidget: (context, url, error) => Image.asset(
                          "assets/$banque_id/images/invoice.png"),
                      width: 100,
                      height: 70,
                    ),
                  ),
                  ListTile(
                    title: Text(
                      "${res["Map"]["demandePaiementFactureInstance"]["paramsGlobal"][index]["libelle"]}",
                      style: GoogleFonts.roboto(),
                    ),
                    trailing: Text(
                      "${res["Map"]["demandePaiementFactureInstance"]["paramsGlobal"][index]["valeurChamp"]}",
                      style: GoogleFonts.roboto(),
                    ),
                  ),
                  Divider(
                    height: 8,
                    color:
                        GlobalParams.themes["$banque_id"].validationButtonColor,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text("A payer",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          color: Colors.green,
                          fontWeight: FontWeight.w600,
                          fontSize: 18)),
                  Container(
                    height: MediaQuery.of(context).size.height *
                        (res["Map"]["detailsCreance"]["nbrCreances"] * 0.11),
                    child: ListView.builder(
                      itemCount: res["Map"]["demandePaiementFactureInstance"]
                              ["impayesForSave"]
                          .length,
                      itemBuilder: (BuildContext context, int itemIndex) {
                        return ListTile(
                          visualDensity:
                              VisualDensity(horizontal: -4, vertical: 2),
                          leading: Checkbox(
                            activeColor:
                                GlobalParams.themes["$banque_id"].appBarColor,
                            visualDensity:
                                VisualDensity(horizontal: -4, vertical: 0),
                            value: controllerFactureImpayes[
                                "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}"],
                            onChanged: (v) {
                              setState(() {
                                controllerFactureImpayes[
                                    "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}"] = v;
                              });
                              if (v) {
                                total += res["Map"]
                                        ["demandePaiementFactureInstance"]
                                    ["impayesForSave"][itemIndex]["prixTTC"];
                                facDet = {
                                  "dateFacture":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["dateFactureFormatted"]}",
                                  "dateFactureFormatted":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["dateFactureFormatted"]}",
                                  "description":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}",
                                  "idArticle":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}",
                                  "prixTTC":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["prixTTC"]}",
                                  "prixTTCToString":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["prixTTC"]}",
                                  "typeArticle":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["typeArticle"]}"
                                };
                                impayes.add(facDet);
                                index = impayes.indexOf(facDet);
                              } else {
                                total -= res["Map"]
                                        ["demandePaiementFactureInstance"]
                                    ["impayesForSave"][itemIndex]["prixTTC"];
                                facDet = {
                                  "dateFacture":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["dateFactureFormatted"]}",
                                  "dateFactureFormatted":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["dateFactureFormatted"]}",
                                  "description":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}",
                                  "idArticle":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}",
                                  "prixTTC":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["prixTTC"]}",
                                  "prixTTCToString":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["prixTTC"]}",
                                  "typeArticle":
                                      "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["typeArticle"]}"
                                };
                                impayes.removeAt(index);
                              }
                            },
                          ),
                          // isThreeLine: true,
                          title: Text(
                            "Référence :",
                            style: GoogleFonts.roboto(),
                          ),
                          subtitle: Column(
                            children: [
                              Text(
                                "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["idArticle"]}",
                                style: GoogleFonts.roboto(),
                              ),
                              Text(
                                "Echéance  ${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["dateFactureFormatted"]}",
                                style: GoogleFonts.roboto(),
                              )
                            ],
                          ),
                          trailing: Text(
                            "${res["Map"]["demandePaiementFactureInstance"]["impayesForSave"][itemIndex]["prixTTC"]} MAD",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(color: Colors.green)),
                          ),
                        );
                      },
                    ),
                  ),
                  Divider(
                    height: 8,
                    color:
                        GlobalParams.themes["$banque_id"].validationButtonColor,
                  ),
                  ListTile(
                    title: Text(
                      "Total à payer",
                      style: GoogleFonts.roboto(textStyle: TextStyle()),
                    ),
                    trailing: Text(
                      "$total MAD",
                      style: GoogleFonts.roboto(textStyle: TextStyle()),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      "Frais",
                      style: GoogleFonts.roboto(textStyle: TextStyle()),
                    ),
                    trailing: Text(
                      "${res["Map"]["demandePaiementFactureInstance"]["typeFrais"] ?? 0} MAD",
                      style: GoogleFonts.roboto(textStyle: TextStyle()),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Total  ",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(fontSize: 17)),
                      ),
                      Text(
                        "${res["Map"]["demandePaiementFactureInstance"]["typeFrais"] == null ? total : total + res["Map"]["demandePaiementFactureInstance"]["typeFrais"]} MAD",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.green,
                                fontSize: 17,
                                fontWeight: FontWeight.w600)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        if (!isLoaded)
                          context.read<CompteBloc>().add(LoadComptesVirEvent());
                      });
                      showCompteDialog(context);
                    },
                    child: Container(
                      height: 60,
                      child: TextFormField(
                        controller: compteDebiterController,
                        validator: (v) {
                          if (v.isEmpty)
                            return "Compte à débiter ne peut pas etre vide";
                          return null;
                        },
                        enabled: false,
                        // initialValue:
                        //     compteDebiterController.text,
                        decoration: InputDecoration(
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          // errorText:
                          //     "Compte à débiter ne peut pas etre vide",
                          hintText: 'Compte à débiter',
                          hintStyle:
                              TextStyle(fontSize: 15, fontFamily: KprimaryFont),
                          suffixIcon: Icon(
                              Icons.arrow_drop_down_circle_outlined,
                              color:
                                  GlobalParams.themes["$banque_id"].iconsColor),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (selectedAccount != null)
                        context.read<FactureBloc>().add(FormCreateSaveEvent(
                            paiementTotal: res["Map"]["demandePaiementFactureInstance"]["impayesForSave"].length ==
                                impayes.length,
                            agenceCmp: res["Map"]["demandePaiementFactureInstance"]
                                    ["agenceCompte"] ??
                                100,
                            codeCreance: res["Map"]
                                ["demandePaiementFactureInstance"]["creanceId"],
                            codeCreancier: res["Map"]["demandePaiementFactureInstance"]
                                ["creancierId"],
                            codeProduitCompte: res["Map"]
                                        ["demandePaiementFactureInstance"]
                                    ["codeProduitCompte"] ??
                                2222,
                            impayesJson: impayes,
                            intituleCompte: selectedAccount.intitule,
                            libelleCreance: res["Map"]
                                ["demandePaiementFactureInstance"]["libelleCreance"],
                            libelleCreancier: widget.title,
                            montantTotalTTC: total,
                            paramsGlobal: widget.paramsGlobal,
                            logo: widget.logo,
                            referencePartenaire: res["Map"]["demandePaiementFactureInstance"]["referencePartenaire"],
                            numeroCompte: selectedAccount.identifiantInterne,
                            type: res["Map"]["type"]));
                      else
                        errorAlert(
                            context, "Compte à débiter ne peut pas etre vide");
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    color: GlobalParams.themes["$banque_id"].appBarColor,
                    child: Text(
                      "Valide",
                      style: GoogleFonts.roboto(
                          textStyle:
                              TextStyle(color: Colors.white, fontSize: 16)),
                    ),
                  ),
                  BlocListener<FactureBloc, FactureStateBloc>(
                    listener: (context, state) {
                      if (state.requestStateSave == StateStatus.LOADING) {
                        showLoading(context);
                      } else if (state.requestStateSave == StateStatus.LOADED) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        okAlert(context, "Opération effectué avec succès");
                      } else if (state.requestStateSave == StateStatus.ERROR) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        errorAlert(context, state.errorMessage);
                      }
                    },
                    child: Container(),
                  )
                ],
              ),
            );
          }),
    );
  }

  showCompteDialog(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            title: Center(
              child: Text(
                "Compte à débiter",
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color: GlobalParams
                            .themes["$banque_id"].validationButtonColor)),
              ),
            ),
            content: Container(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: BlocBuilder<CompteBloc, CompteStateBloc>(
                          builder: (context, state) {
                            if (state.requestStateTransaction ==
                                StateStatus.NONE) {
                              WidgetsBinding.instance.addPostFrameCallback((_) {
                                context
                                    .read<CompteBloc>()
                                    .add(LoadComptesVirEvent());
                              });
                              return Container();
                            } else if (state.requestStateTransaction ==
                                StateStatus.LOADING) {
                              return Center(
                                  child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                            } else if (state.requestStateTransaction ==
                                StateStatus.ERROR) {
                              return ErreurTextWidget(
                                errorMessage: state.errorMessage,
                                actionEvent: () {
                                  context
                                      .read<CompteBloc>()
                                      .add(state.currentAction);
                                },
                              );
                            } else if (state.requestStateTransaction ==
                                StateStatus.LOADED) {
                              isLoaded = true;
                              if (state.listeCompteTransaction.comptes.length ==
                                  0)
                                return Center(
                                  child: Text(
                                    AppLocalizations.of(context).aucun_compte_transaction,
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            fontSize: 16,
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .validationButtonColor)),
                                  ),
                                );
                              else
                                return ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: state
                                      .listeCompteTransaction.comptes.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      onTap: () {
                                        // setState(() {
                                        compteDebiterController.text =
                                            "${state.listeCompteTransaction.comptes[index].intitule}";
                                        // compte["Map"]["compteInstanceList"]
                                        //     [index]["intitule"];
                                        setState(() {
                                          selectedAccount = state
                                              .listeCompteTransaction
                                              .comptes[index];
                                        });
                                        // });
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        color: (index % 2) != 0
                                            ? Colors.grey[100]
                                            : Colors.white,
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(5.0),
                                                  child: Container(
                                                    height: 40,
                                                    width: 5,
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .iconsColor,
                                                  ),
                                                ),
                                                Column(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              5, 3, 5, 3),
                                                      child: Text(state
                                                          .listeCompteTransaction
                                                          .comptes[index]
                                                          .intitule),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              5, 0, 0, 0),
                                                      child: Text(state
                                                          .listeCompteTransaction
                                                          .comptes[index]
                                                          .identifiantInterne),
                                                    ),
                                                  ],
                                                ),
                                                // Padding(
                                                //   padding: EdgeInsets.fromLTRB(
                                                //       5, 0, 0, 0),
                                                //   child: Text(
                                                //       "${state.listeCompteTransaction.comptes[index].soldeTempsReel} ${state.listeCompteTransaction.comptes[index].devise}"),
                                                // ),
                                                Icon(
                                                  Icons.arrow_right,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .iconsColor,
                                                  size: 35,
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                );
                            } else {
                              return Container();
                            }
                          },
                        )),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
