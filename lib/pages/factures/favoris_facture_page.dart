import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/creanceDetails.model.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';
import 'factureChamp.widget.dart';

class FavorisFacturePage extends StatelessWidget {
  FavorisFacturePage({Key key}) : super(key: key);

  bool isLoaded = false;
  ListCreance creance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: AppLocalizations.of(context).favoris,
        context: context,
        isPop: true,
      ),
      body: BlocBuilder<FactureBloc, FactureStateBloc>(
        builder: (context, state) {
          print(state.requestStateFav);
          if (state.requestStateFav == StateStatus.NONE) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded) {
                context.read<FactureBloc>().add(LoadFavorisEvent());
              }
            });
            return Container();
          } else if (state.requestStateFav == StateStatus.LOADING) {
            return Center(
                heightFactor: 15,
                child: CircularProgressIndicator(
                  backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      GlobalParams.themes["$banque_id"].appBarColor),
                ));
          } else if (state.requestStateFav == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<FactureBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestStateFav == StateStatus.LOADED) {
            isLoaded = true;
            if (state.favoris.map.list.length == 0)
              return Center(
                heightFactor: 20,
                child: Text(
                  AppLocalizations.of(context).aucun_fav,
                  style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(fontSize: 16, color: principaleColor5)),
                ),
              );
            else
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: ListView.separated(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: state.favoris.map.list.length,
                        itemBuilder: (_, index) {
                          // print("${state.historiqueVirementBenif.map.liste[0]}");
                          return GestureDetector(
                            onTap: () {
                              creance = ListCreance(
                                  nom: state.favoris.map.list[index].libelleCreance,
                                  creancierId:
                                      state.favoris.map.list[index].creancierId,
                                  code: state.favoris.map.list[index].creanceId);
                              context.read<FactureBloc>().add(
                                  LoadCreanceFormEvent(
                                      codeCreance: state.favoris.map.list[index].creanceId,
                                      codeCreancier: state.favoris.map.list[index].creancierId));
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (contex) =>
                                          FactureChampWidget(
                                            title: state.favoris.map.list[index].libelleCreancier,
                                            creance: creance,
                                            logo: state.favoris.map.list[index].logoPath,
                                            dynamicFormValues: state.favoris.map.list[index].dynamicFormValues,
                                          )));
                            },
                            child: ListTile(
                              leading: state.favoris.map.list[index].logoPath != null ? CachedNetworkImage(
                                imageUrl: state.favoris.map.list[index].logoPath,
                                placeholder: (context, url) => CircularProgressIndicator(
                                  backgroundColor:
                                      Colors.white,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      GlobalParams.themes["$banque_id"].appBarColor),
                                ),
                                errorWidget: (context, url, error) => Image.asset(
                                    "assets/$banque_id/images/invoice.png"),
                                width: 40,
                                height: 40,
                              ) : Image.asset(
                                  "assets/$banque_id/images/invoice.png"),
                              trailing: IconButton(
                                onPressed: (){
                                  showDeleteDialog(context, state.favoris.map.list[index].id);
                                },
                                icon: Icon(Icons.cancel),
                                color: Colors.redAccent,
                              ),
                              title: Padding(
                                padding: const EdgeInsets.only(bottom: 5.0),
                                child: Text(
                                    "${state.favoris.map.list[index].libelleFavoris}",
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.black, fontSize: 16))),
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) => ListTile(
                          visualDensity:
                          VisualDensity(horizontal: -4, vertical: -4),
                          leading: Container(
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: Divider(
                              color: Colors.black45,
                              height: 15,
                            ),
                          ),
                          trailing: Container(
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: Divider(
                              color: Colors.black45,
                              height: 15,
                            ),
                          ),
                        ),
                    ),
                  ),
                  BlocListener<FactureBloc, FactureStateBloc>(
                    listener: (context, state) {
                      if (state.requestStateDelete == StateStatus.LOADING) {
                        return showLoading(context);
                      } else if (state.requestStateDelete == StateStatus.LOADED) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        context.read<FactureBloc>().add(LoadFavorisEvent());
                          return okAlert(context, 'Favori supprimé avec succès');
                      } else if (state.requestStateDelete == StateStatus.ERROR) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        return errorAlert(
                            context, state.errorMessage ?? "Erreur!");
                      } else {
                        return Container();
                      }
                    },
                    child: Container(),
                  )
                ],
              );
          } else {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded) {
                context.read<FactureBloc>().add(LoadFavorisEvent());
              }
            });
            return Container();
          }
        },
      ),
    );
  }

  void showDeleteDialog(BuildContext context, idFav) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Confirmation".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color:
                          GlobalParams.themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02,
                    bottom: MediaQuery.of(context).size.height * 0.03),
                child: Text(
                    "Êtes-vous sûr de vouloir supprimer ce favori ?",
                    textAlign: TextAlign.center,
                    style:
                    GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18))),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  MaterialButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)
                    ),
                    onPressed: () => Navigator.pop(context),
                    color: Colors.white,
                    child: Text("Non",
                        style: GoogleFonts.roboto(
                            textStyle:
                            TextStyle(fontSize: 17, color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor))),
                  ),
                  MaterialButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    color: GlobalParams
                        .themes["$banque_id"].intituleCmpColor,
                    child: Text("Oui",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 17,
                                color: Colors.white))),
                    onPressed: () {
                      context.read<FactureBloc>().add(DeleteFavoriEvent(idFav: idFav));
                    },
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
