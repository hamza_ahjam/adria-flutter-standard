import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/factures/factureForm.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class FacturePage extends StatelessWidget {
  FacturePage({Key key}) : super(key: key);

  bool isLoaded = false;
  ScrollController _scrollController = ScrollController();
  ScrollController _nestedScrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
          appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            shape: ContinuousRectangleBorder(
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(60))),
            toolbarHeight: MediaQuery.of(context).size.height * 0.085,
            title: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                AppLocalizations.of(context).facts.toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
            ),
            centerTitle: true,
            backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
          ),
          body: BlocBuilder<FactureBloc, FactureStateBloc>(
              builder: (context, state) {
            if (state.requestState == StateStatus.NONE) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<FactureBloc>().add(LoadFactureEvent());
              });
              return Container();
            } else if (state.requestState == StateStatus.LOADING) {
              return Center(
                  heightFactor: 15,
                  child: CircularProgressIndicator(
                    backgroundColor:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        GlobalParams.themes["$banque_id"].appBarColor),
                  ));
            } else if (state.requestState == StateStatus.ERROR) {
              return ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<FactureBloc>().add(state.currentAction);
                },
              );
            } else if (state.requestState == StateStatus.LOADED) {
              isLoaded = true;
              if (state.facture.keys.length == 0)
                return Center(
                  child: Text(
                    "Aucune facture trouvée",
                    style: GoogleFonts.roboto(
                        textStyle:
                        TextStyle(fontSize: 16, color: GlobalParams.themes[banque_id].appBarColor)),
                  ),
                );
              else return Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.facture.keys.length,
                      itemBuilder: (context, index) {
                        return ListView(
                            controller: _scrollController,
                            shrinkWrap: true,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                padding: EdgeInsets.fromLTRB(20, 12, 0, 8),
                                color: Colors.grey[300],
                                child: Text(
                                  "${state.facture.keys.toList()[index]}",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18)),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 12),
                                child: Wrap(
                                  children: [
                                    ListView.builder(
                                      controller: _nestedScrollController,
                                      shrinkWrap: true,
                                      itemCount: state
                                          .facture[state.facture.keys
                                              .toList()[index]]
                                          .length,
                                      itemBuilder: (context, itemIndex) {
                                        return GestureDetector(
                                          onTap: () {
                                            context.read<FactureBloc>().add(
                                                LoadCreanceEvent(
                                                    code: state
                                                        .facture[state
                                                                .facture.keys
                                                                .toList()[
                                                            index]][itemIndex]
                                                        .code));
                                            // Navigator.pop(context);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        FactureFormWidget(
                                                          facture: state
                                                              .facture[state
                                                                  .facture.keys
                                                                  .toList()[
                                                              index]][itemIndex],
                                                        )));
                                          },
                                          child: Container(
                                            color: (itemIndex % 2) != 0
                                                ? Colors.grey[100]
                                                : Colors.white,
                                            child: ListTile(
                                              leading: state.facture[state.facture.keys.toList()[index]][itemIndex].logoPath != null ? CachedNetworkImage(
                                                imageUrl: state
                                                          .facture[state
                                                                  .facture.keys
                                                                  .toList()[
                                                              index]][itemIndex]
                                                          .logoPath,
                                                      progressIndicatorBuilder: (context, url, downloadProgress) => CircularProgressIndicator(
                                                  backgroundColor:
                                                  GlobalParams.themes["$banque_id"].intituleCmpColor,
                                                  value: downloadProgress.progress,
                                                  valueColor: AlwaysStoppedAnimation<Color>(
                                                      GlobalParams.themes["$banque_id"].appBarColor
                                                  ),
                                                ),
                                                errorWidget: (context, url, error) => Image.asset(
                                                  "assets/$banque_id/images/invoice.png"),
                                                width: 40,
                                                height: 40,
                                              ) : Image.asset(
                                                  "assets/$banque_id/images/invoice.png"),
                                              //  Image.network(
                                              //   "${state.facture[state.facture.keys.toList()[index]][itemIndex].logoPath}",
                                              //   cacheHeight: 30,
                                              //   cacheWidth: 60,
                                              //   width: 60,
                                              //   height: 30,
                                              // ),
                                              title: Text(
                                                  "${state.facture[state.facture.keys.toList()[index]][itemIndex].nom}",
                                                  style: GoogleFonts.roboto()),
                                              trailing: Icon(
                                                Icons.arrow_forward_ios,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .iconsColor,
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              )
                            ]);
                      }),
                ),
              );
            } else {
              return Container();
            }
          })),
    );
  }
}
