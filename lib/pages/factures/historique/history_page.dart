import 'package:LBA/bloc/facturehistory/facture_history_bloc.dart';
import 'package:LBA/bloc/facturehistory/facture_history_event.dart';
import 'package:LBA/bloc/facturehistory/facture_history_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';

class HistoriqueFactures extends StatefulWidget {
  @override
  State<HistoriqueFactures> createState() => _HistoriqueFacturesState();
}

class _HistoriqueFacturesState extends State<HistoriqueFactures> {
  bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: OnlineAppBarWidget(
            appBarTitle: 'HISTORIQUE DES PAIEMENT',
            context: context,
            isPop: true),
        body: BlocBuilder<FactureHistoryBloc, FactureHistoryStateBloc>(
            builder: (context, state) {
          if (state.requestState == StateStatus.NONE) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded)
                context
                    .read<FactureHistoryBloc>()
                    .add(LoadFactureHistoryEvent());
            });
            return Container();
          } else if (state.requestState == StateStatus.LOADING) {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
          } else if (state.requestState == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<FactureHistoryBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestState == StateStatus.LOADED) {
            isLoaded = true;
            if (state.listFactures == null || state.listFactures.map.factures.length == 0)
              return Center(
                child: Text(
                  AppLocalizations.of(context).aucun_pai,
                  style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(fontSize: 16, color: GlobalParams.themes[banque_id].appBarColor)),
                ),
              );
            else
              return Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: ListView.builder(
                      itemCount: state.listFactures.map.factures.length,
                      itemBuilder: (context, index) => Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, right: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Column(
                                        children: [
                                          SvgPicture.asset(
                                            "assets/$banque_id/images/blue_check.svg",
                                          ),
                                          Text(
                                            state.listFactures.map.factures
                                                .elementAt(index)
                                                .statutCode,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Color.fromRGBO(
                                                    29, 44, 98, 1)),
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 10, top: 10),
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                state.listFactures.map.factures
                                                    .elementAt(index)
                                                    .libelleCreance,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Color.fromRGBO(
                                                        29, 44, 98, 1)),
                                              ),
                                              Text(
                                                  state.listFactures.map
                                                          .factures
                                                          .elementAt(index)
                                                          .libelleCreancier +
                                                      "\n" +
                                                      state.listFactures.map
                                                          .factures
                                                          .elementAt(index)
                                                          .dateEnvoiDemande,
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Color.fromRGBO(
                                                          29, 44, 98, 1))),
                                            ]),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 40),
                                        child: Text(
                                            "-" +
                                                state.listFactures.map.factures
                                                    .elementAt(index)
                                                    .montantTotalTTCToString +
                                                " " +
                                                state.listFactures.map.factures
                                                    .elementAt(index)
                                                    .deviseOperationLibelle,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                color: Color.fromRGBO(
                                                    29, 44, 98, 1))),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                    padding:
                                        EdgeInsets.only(left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.20,
                                          child: Divider(
                                            color: Color.fromRGBO(
                                                248, 248, 248, 1),
                                            thickness: 2.5,
                                          ),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.20,
                                          child: Divider(
                                            color: Color.fromRGBO(
                                                248, 248, 248, 1),
                                            thickness: 2.5,
                                          ),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          )),
                ),
              );
          } else {
            return Container();
          }
        }));
  }
}
