import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/dat/dat.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/show-more.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;

import '../../../main.dart';

class ListDatWidget extends StatelessWidget {
  ListDatWidget({Key key}) : super(key: key);

  bool isLoaded = false;
  int page = 1;
  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp.of(context).getLocale().languageCode;

    return BlocBuilder<DatBloc, DatStateBloc>(
      builder: (context, state) {
        // if (state.requestState == StateStatus.NONE) {
        //   WidgetsBinding.instance.addPostFrameCallback((_) {
        //     if (!isLoaded) context.read<DatBloc>().add(LoadDatEvent());
        //   });
        //   return Container();
        // } else
        if (state.requestState == StateStatus.LOADING) {
          return Center(
              heightFactor: 15,
              child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
        } else if (state.requestState == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<DatBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestState == StateStatus.LOADED) {
          isLoaded = true;
          if (state.listDat.map.liste.length == 0)
            return Center(
              heightFactor: 20,
              child: Text(
                AppLocalizations.of(context).aucun_dat,
                style: GoogleFonts.roboto(
                    textStyle:
                        TextStyle(fontSize: 16, color: principaleColor5)),
              ),
            );
          else
            return SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: state.listDat.map.liste.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.03,
                          10,
                          MediaQuery.of(context).size.width * 0.03,
                          MediaQuery.of(context).size.height * 0.02,
                        ),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30))),
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        color: GlobalParams
                                            .themes["$banque_id"].appBarColor,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(30),
                                            topRight: Radius.circular(30))),
                                    child: ListTile(
                                      title: Text(
                                        "${AppLocalizations.of(context).num_dat}: ${state.listDat.map.liste[index].compteDepotId}",
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .intituleCmpColor)),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            MediaQuery.of(context).size.width *
                                                0.05),
                                    child: ListTile(
                                      title: Text(
                                        AppLocalizations.of(context).type,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                      trailing: Text(
                                        "${state.listDat.map.liste[index].type}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: EdgeInsets.symmetric(
                                  //       horizontal:
                                  //           MediaQuery.of(context).size.width *
                                  //               0.05),
                                  //   child: ListTile(
                                  //     title: Text(
                                  //       "Montant Dat",
                                  //       style: GoogleFonts.roboto(
                                  //           textStyle: TextStyle(
                                  //               fontWeight: FontWeight.w600,
                                  //               fontSize: 16)),
                                  //     ),
                                  //     trailing: Text(
                                  //       "${state.listDat.map.liste[index].montantDatFormatted}",
                                  //       style: GoogleFonts.roboto(
                                  //           textStyle: TextStyle(
                                  //               fontWeight: FontWeight.w600,
                                  //               fontSize: 16)),
                                  //     ),
                                  //   ),
                                  // ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            MediaQuery.of(context).size.width *
                                                0.05),
                                    child: ListTile(
                                      title: Text(
                                        AppLocalizations.of(context).taux,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                      trailing: Text(
                                        "${state.listDat.map.liste[index].taux} %",
                                        textDirection: TextDirection.ltr,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            MediaQuery.of(context).size.width *
                                                0.05),
                                    child: ListTile(
                                      title: Text(
                                        AppLocalizations.of(context).nanti,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                      trailing: Text(
                                        "${state.listDat.map.liste[index].nanti}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            MediaQuery.of(context).size.width *
                                                0.05),
                                    child: ListTile(
                                      title: Text(
                                        AppLocalizations.of(context).date_ech,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                      trailing: Text(
                                        "${intl.DateFormat.yMMMMd(languageCode).format(intl.DateFormat("dd-MM-yyyy").parse(state.listDat.map.liste[index].dateEcheance))}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16)),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal:
                                          MediaQuery.of(context).size.width *
                                              0.095,
                                    ),
                                    child: Divider(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      thickness: 2.5,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.width * 0.05,
                                      MediaQuery.of(context).size.height * 0.01,
                                      MediaQuery.of(context).size.width * 0.05,
                                      MediaQuery.of(context).size.height * 0.02,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "3000 CFA",
                                        textDirection: TextDirection.ltr,
                                        // "${state.listDat.map.liste[index].montantDatFormatted} CFA",
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .intituleCmpColor)),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  state.listDat.map.liste.length > 19
                      ? ShowMore(
                          showMoreAction: () {
                            context
                                .read<DatBloc>()
                                .add(LoadDatEvent(page: page));
                            // _compteState.loadNextHistoriquePage(tokenState, page);
                            ++page;
                          },
                        )
                      : Container()
                ],
              ),
            );
        } else {
          return Container();
        }
      },
    );
  }
}
