import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.bloc.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class CompteDatWidget extends StatelessWidget {
  CompteDatWidget({Key key}) : super(key: key);
  ScrollController _scrollController = ScrollController();
  bool isLoaded = false;
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.22,
          child:
              BlocBuilder<CompteBloc, CompteStateBloc>(builder: (context, state) {
            // print("${state.requestStateVir}");
            if (state.requestStateVir == StateStatus.NONE) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<CompteBloc>().add(LoadComptesDatEvent());
              });
              return Container();
            } else if (state.requestStateVir == StateStatus.LOADING) {
              return Center(
                  child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
            } else if (state.requestStateVir == StateStatus.ERROR) {
              return ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<CompteBloc>().add(state.currentAction);
                },
              );
            } else if (state.requestStateVir == StateStatus.LOADED) {
              isLoaded = true;
              if (state.listeCompteVir.comptes.length == 0) {
                return Center(
                  heightFactor: 20,
                  child: Text(
                    "Vous ne pouvez pas consulter vos DAT avec un compte en devise",
                    style: GoogleFonts.roboto(),
                  ),
                );
              } else
                return Container(
                  padding: EdgeInsets.only(top: 8),
                  child: PageView.builder(
                      physics: BouncingScrollPhysics(),
                      onPageChanged: (int page) {
                        context.read<DatBloc>().add(ChangeAccountLoadDatEvent(
                            compte: state.listeCompteVir.comptes[page]));

                        context.read<NomCompteBloc>().add(
                            ChangeCurrentAccountEvent(
                                compte: state.listeCompteVir.comptes[page]));
                      },
                      scrollDirection: Axis.horizontal,
                      itemCount: state.listeCompteVir.comptes.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          width: MediaQuery.of(context).size.width * 95,
                          child: ListView(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    left:
                                        MediaQuery.of(context).size.width * 0.05),
                                child: Text(
                                    "${state.listeCompteVir.comptes[index].intitule}",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          color: GlobalParams.themes["$banque_id"]
                                              .intituleCmpColor,
                                          fontSize: 21,
                                          fontWeight: FontWeight.bold),
                                    )),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left:
                                        MediaQuery.of(context).size.width * 0.05),
                                child: Text(
                                    "${state.listeCompteVir.comptes[index].identifiantInterne != null ? state.listeCompteVir.comptes[index].identifiantInterne : ""}",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                        color: GlobalParams
                                            .themes["$banque_id"].numCmpColor,
                                        fontSize: 18,
                                      ),
                                    )),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    right:
                                        MediaQuery.of(context).size.width * 0.05,
                                    top: 20),
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  child: RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text: "-1 996",
                                          // "${state.listeCompteVir.comptes[index].soldeTempsReel != null ? state.listeCompteVir.comptes[index].soldeTempsReel : ""} ",
                                          style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .soldeCmpColor,
                                                fontSize: 23,
                                                fontWeight: FontWeight.bold),
                                          )),
                                      WidgetSpan(
                                        child: Transform.translate(
                                          offset: const Offset(2, -4),
                                          child: Text("CFA",
                                              // "${state.listeCompteVir.comptes[index].devise != null ? state.listeCompteVir.comptes[index].devise : ""}",
                                              //superscript is usually smaller in size
                                              textScaleFactor: 0.7,
                                              style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .soldeCmpColor,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.bold),
                                              )),
                                        ),
                                      )
                                    ]),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8),
                                child: DotsIndicator(
                                  dotsCount: state.listeCompteVir.comptes.length,
                                  position: index.toDouble(),
                                  decorator: DotsDecorator(
                                    activeColor: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                    size: const Size.square(9.0),
                                    activeSize: const Size(18.0, 9.0),
                                    activeShape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5.0)),
                                  ),
                                ),
                              ),
                              // Divider(
                              //   height: 20,
                              //   color: Colors.grey,
                              // ),
                            ],
                          ),
                        );
                      }),
                );
            } else {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<CompteBloc>().add(LoadComptesDatEvent());
              });
              return Container();
            }
          })),
    );
  }
}
