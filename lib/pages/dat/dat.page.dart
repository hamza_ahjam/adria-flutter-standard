import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/dat/souscription_dat.page.dart';
import 'package:LBA/pages/dat/widgets/compte.dat.dart';
import 'package:LBA/pages/dat/widgets/listDat.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DatPage extends StatelessWidget {
  const DatPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: AppLocalizations.of(context).dat,
        context: context,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => SouscriptionDatPage())),
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        elevation: 4,
        backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
      ),
      body: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      "assets/$banque_id/images/background_image.png"),
                  fit: BoxFit.cover,
                  alignment: Alignment(0, 0.9)),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(22.0),
              ),
            ),
            child: CompteDatWidget(),
          ),
          // BlocBuilder<CompteBloc, CompteStateBloc>(builder: (context, state) {
          //   if (state.requestStateVir == StateStatus.LOADED) {
          //     if (state.listeCompteVir.comptes.length == 0) {
          //       return Container();
          //     } else
          //       return NomCompteWidget(
          //         titre: "DAT",
          //       );
          //   } else {
          //     return Container();
          //   }
          // }),
          Expanded(child: ListDatWidget())
        ],
      ),
    );
  }
}
