import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.event.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/cartes/widgets/carteDetail.widget.dart';
import 'package:LBA/pages/cartes/widgets/commandeCarte/choixTypeCarte.widget.dart';
import 'package:LBA/pages/cartes/widgets/historiqueDemandeCarte.page.dart';
import 'package:LBA/pages/cartes/widgets/listCarte.widget.dart';
import 'package:LBA/pages/cartes/widgets/opperationCarte.widget.dart';
import 'package:LBA/pages/cartes/widgets/reglagesCarte.widget.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CartePage extends StatefulWidget {
  CartePage();

  @override
  State<CartePage> createState() => _CartePageState();
}

class _CartePageState extends State<CartePage> {
  bool showReglage = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).cartes.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardComptePage()));
              }),
          actions: [
            if (banque_id == "NSIA")
              TextButton.icon(
                  onPressed: () {
                    setState(() {
                      showReglage = !showReglage;
                    });
                  },
                  icon: SvgPicture.asset(
                    "assets/$banque_id/images/param.svg",
                    color: Colors.white,
                  ),
                  label: Text(""))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white,),
          onPressed: () {
            context.read<CarteBloc>().add(LoadTypeCarteEvent());
            commanderCarteDialog();
          },
        ),
        // bottomNavigationBar: Container(
        //     // color: principaleColor5,
        //     height: MediaQuery.of(context).size.height * 0.08,
        //     child: BlocBuilder<CarteBloc, CarteStateBloc>(
        //       builder: (context, state) {
        //         if (state.requestState == StateStatus.LOADED) {
        //           return RaisedButton.icon(
        //               onPressed: state.currentCarte.prepayee
        //                   ? () {
        //                       print("${state.currentCarte.prepayee}");
        //                     }
        //                   : null,
        //               color: GlobalParams.themes["$banque_id"].appBarColor,
        //               icon: Icon(
        //                 Icons.collections_bookmark_outlined,
        //                 color: Colors.white,
        //               ),
        //               label: Text("Recharger",
        //                   style: GoogleFonts.roboto(
        //                       textStyle:
        //                           TextStyle(color: Colors.white, fontSize: 16))));
        //         } else {
        //           return Container();
        //         }
        //       },
        //     )),
        extendBodyBehindAppBar: true,
        body: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  color: Colors.white,
                ),
              )),
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.height * 0.49,
                child: Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.37,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "assets/$banque_id/images/login_bg.png"),
                            fit: BoxFit.cover,
                            alignment: Alignment(0, 0.3)),
                      ),
                      // borderRadius: BorderRadius.only(
                      //   bottomLeft: const Radius.circular(40.0),
                      //   bottomRight: const Radius.circular(40.0),
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: CarteDetailWidget(),
                    ),
                    Positioned(
                        top: MediaQuery.of(context).size.height * 0.2,
                        child: ListCarteWidget()),
                  ],
                ),
              ),
              !showReglage
                  ? Container(
                color: Colors.white,
                child: BlocBuilder<MouvementCarteBloc, MouvementCarteStateBloc>(
                    builder: (context, state) {
                  if (state.requestState == StateStatus.LOADED)
                    return Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 8),
                      child: Text(
                        AppLocalizations.of(context).mvt,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 17,
                        )),
                      ),
                    );
                  else
                    return Container();
                }),
              )
                  : Container(),
              Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.height * 0.35,
                child: showReglage ? ReglageCarteWidget() : OpperationCarteWidget(),
              )
            ],
          ),
        ),
      ),
    );
  }

  showSetting(context) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.47,
          width: MediaQuery.of(context).size.width * 0.9,
          child: ListView(
            children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Icon(
                    Icons.add_to_photos_rounded,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Commander une carte",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HistoriqueDemandeCarte()));
                },
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Icon(
                    Icons.list,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Voir toutes les demandes",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Image.asset(
                    "assets/$banque_id/images/pin.png",
                    fit: BoxFit.fill,
                    // color: GlobalParams.themes["$banque_id"].iconsColor,
                    width: 18,
                    height: 18,
                  ),
                  title: Text(
                    "Code Carte",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Image.asset(
                    "assets/$banque_id/images/cvv.png",
                    fit: BoxFit.fill,
                    // color: GlobalParams.themes["$banque_id"].iconsColor,
                    width: 18,
                    height: 18,
                  ),
                  title: Text(
                    "Code Internet",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Image.asset(
                    "assets/$banque_id/images/opposition.png",
                    fit: BoxFit.fill,
                    // color: GlobalParams.themes["$banque_id"].iconsColor,
                    width: 18,
                    height: 18,
                  ),
                  title: Text(
                    "Opposition",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Image.asset(
                    "assets/$banque_id/images/bloquer.png",
                    fit: BoxFit.fill,
                    // color: GlobalParams.themes["$banque_id"].iconsColor,
                    width: 18,
                    height: 18,
                  ),
                  title: Text(
                    "Bloquer",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  context.read<CarteBloc>().add(LoadCardPlafond());
                },
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4),
                  leading: Image.asset(
                    "assets/$banque_id/images/plafond.png",
                    fit: BoxFit.fill,
                    // color: GlobalParams.themes["$banque_id"].iconsColor,
                    width: 18,
                    height: 18,
                  ),
                  title: Text(
                    "Plafonds",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor)),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: GlobalParams.themes["$banque_id"].iconsColor,
                  ),
                ),
              ),
              BlocListener<CarteBloc, CarteStateBloc>(
                listener: (context, state) {
                  if (state.requestStatePl == StateStatus.LOADING) {
                    return showLoading(context);
                  } else if (state.requestStatePl == StateStatus.LOADED) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    return showPlafonds(context, state.plafond["plafonds"]);
                  } else if (state.requestStatePl == StateStatus.ERROR) {
                    // Navigator.pop(context);
                    Navigator.pop(context);
                    return errorAlert(
                        context, state.errorMessage ?? "Carte introuvable");
                  } else {
                    return Container();
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }

  showPlafonds(context, plafond) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              title: Text("Nouveaux Plafonds"),
              content: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                width: MediaQuery.of(context).size.width * 0.9,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: plafond.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: ListView(
                      children: [
                        ListTile(
                          title: Text("Retrait au Maroc"),
                          trailing: Text(
                              "${plafond[index]["plafondPaiementsFotmatted"]} MAD"),
                        ),
                        ListTile(
                          title: Text("Paiement au Maroc"),
                          trailing: Text(
                              "${plafond[index]["plafondRetraitFotmatted"]} MAD"),
                        ),
                        RaisedButton(
                          onPressed: () {
                            context.read<CarteBloc>().add(LoadUpdatePlafond(
                                plafond[index]["codePlafond"]));
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                          child: Text(
                            "Valider formule ?",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(color: Colors.white)),
                          ),
                        ),
                        BlocListener<CarteBloc, CarteStateBloc>(
                          listener: (context, state) {
                            if (state.requestStateUp == StateStatus.LOADING) {
                              return showLoading(context);
                            } else if (state.requestStateUp ==
                                StateStatus.LOADED) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              okAlert(
                                  context,
                                  state.resUpdatePlafond["message"] ??
                                      "Plafonds modifié avec succès");
                              context
                                  .read<MouvementCarteBloc>()
                                  .add(LoadCartePlafondEvent());
                            } else if (state.requestStateUp ==
                                StateStatus.ERROR) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              errorAlert(context,
                                  state.errorMessage ?? "Erreur technique");
                            } else {
                              return Container();
                            }
                          },
                          child: Container(),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }

  commanderCarteDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return ChoixTypeCarteWidget();
      },
    );
  }
}
