import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/cartes/widgets/opposition_carte.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';
import 'bloquerCarte.widget.dart';

class ReglageCarteWidget extends StatefulWidget {
  ReglageCarteWidget({Key key}) : super(key: key);

  @override
  State<ReglageCarteWidget> createState() => _ReglageCarteWidgetState();
}

class _ReglageCarteWidgetState extends State<ReglageCarteWidget> {
  var compteDebiterController = TextEditingController();

  TextEditingController montantController = TextEditingController();

  var selectedAccount;

  ScrollController _scrollController = ScrollController();
  bool isLoaded = false;
  var passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                IconButton(
                  onPressed: () {
                    bloquerCarteDialog();
                  },
                  icon: SvgPicture.asset(
                    "assets/$banque_id/images/bloquer_carte_icon.svg",
                    height: 30,
                    width: 30,
                    fit: BoxFit.cover,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).bloquer_carte,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: GlobalParams.themes["$banque_id"].appBarColor,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
            Column(
              children: [
                IconButton(
                  onPressed: () {
                    context.read<CarteBloc>().add(LoadMotifOpposition());
                    oppasitionCarte(context);
                  },
                  icon: SvgPicture.asset(
                      "assets/$banque_id/images/opposition_carte_icon.svg",
                      height: 27,
                      width: 27,
                      fit: BoxFit.cover),
                ),
                Text(AppLocalizations.of(context).oposition_carte,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: GlobalParams.themes["$banque_id"].appBarColor,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            ),
            Column(
              children: [
                IconButton(
                  onPressed: () {
                    context.read<CarteBloc>().add(LoadCardPlafond());
                  },
                  icon: SvgPicture.asset(
                      "assets/$banque_id/images/plafond_icon.svg",
                      height: 40,
                      width: 40,
                      fit: BoxFit.cover),
                ),
                Text('${AppLocalizations.of(context).plafonds}\n',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: GlobalParams.themes["$banque_id"].appBarColor,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            )
          ],
        ),
        SizedBox(
          height: 25,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            BlocBuilder<CarteBloc, CarteStateBloc>(
              builder: (context, state) {
                if (state.currentCarte != null &&
                    state.currentCarte.prepayee &&
                    state.requestState == StateStatus.LOADED)
                  return Column(
                    children: [
                      IconButton(
                        onPressed: () => rechargeCarte(context),
                        icon: SvgPicture.asset(
                            "assets/$banque_id/images/recharge_carte_icon.svg",
                            height: 40,
                            width: 40,
                            fit: BoxFit.fill),
                      ),
                      Text(
                        AppLocalizations.of(context).charger_carte,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  );
                else
                  return Column(
                    children: [
                      IconButton(
                        onPressed: null,
                        icon: SvgPicture.asset(
                            "assets/$banque_id/images/recharge_carte_icon.svg",
                            color: Colors.grey,
                            height: 40,
                            width: 40,
                            fit: BoxFit.fill),
                      ),
                      Text(
                        AppLocalizations.of(context).charger_carte,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  );
              },
            ),
            Column(
              children: [
                IconButton(
                  onPressed: () => recalculCode(context),
                  icon: SvgPicture.asset(
                      "assets/$banque_id/images/recalcul_pin_icon.svg",
                      height: 27,
                      width: 27,
                      fit: BoxFit.fill),
                ),
                Text(AppLocalizations.of(context).recalcule_pin,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: GlobalParams.themes["$banque_id"].appBarColor,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            ),
            Column(
              children: [
                IconButton(
                  onPressed: () => genererCvv(context),
                  icon: SvgPicture.asset(
                      "assets/$banque_id/images/cvv_icon.svg",
                      height: 25,
                      width: 25,
                      fit: BoxFit.fill),
                ),
                Text(AppLocalizations.of(context).generer_cvv,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: GlobalParams.themes["$banque_id"].appBarColor,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            )
          ],
        ),
        BlocListener<CarteBloc, CarteStateBloc>(
          listener: (context, state) {
            if (state.requestStatePl == StateStatus.LOADING) {
              return showLoading(context);
            } else if (state.requestStatePl == StateStatus.LOADED) {
              // Navigator.pop(context);
              Navigator.pop(context);
              return showPlafonds(context, state.plafond["plafonds"]);
            } else if (state.requestStatePl == StateStatus.ERROR) {
              // Navigator.pop(context);
              Navigator.pop(context);
              return errorAlert(
                  context, state.errorMessage ?? "Carte introuvable");
            } else {
              return Container();
            }
          },
          child: Container(),
        )
      ],
    );
  }

  showPlafonds(context, plafond) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25))),

              titlePadding: EdgeInsets.zero,
              // clipBehavior: Clip.antiAliasWithSaveLayer,
              insetPadding: EdgeInsets.zero,
              title: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "Nouveaux Plafonds".toUpperCase(),
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
              content: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                width: MediaQuery.of(context).size.width * 0.7,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: plafond.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: ListView(
                      children: [
                        ListTile(
                          title: Text("Retrait au Maroc"),
                          trailing: Text(
                              "${plafond[index]["plafondPaiementsFotmatted"]} MAD"),
                        ),
                        ListTile(
                          title: Text("Paiement au Maroc"),
                          trailing: Text(
                              "${plafond[index]["plafondRetraitFotmatted"]} MAD"),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                          width: MediaQuery.of(context).size.width * 0.8,
                        ),
                        Container(
                          height: 40,
                          child: RaisedButton(
                            elevation: 0,
                            onPressed: () {
                              context.read<CarteBloc>().add(LoadUpdatePlafond(
                                  plafond[index]["codePlafond"]));
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Text(
                              "Valider formule ?",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: Colors.white, fontSize: 18)),
                            ),
                          ),
                        ),
                        BlocListener<CarteBloc, CarteStateBloc>(
                          listener: (context, state) {
                            if (state.requestStateUp == StateStatus.LOADING) {
                              return showLoading(context);
                            } else if (state.requestStateUp ==
                                StateStatus.LOADED) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              okAlert(
                                  context,
                                  state.resUpdatePlafond["message"] ??
                                      "Plafonds modifié avec succès");
                              context
                                  .read<MouvementCarteBloc>()
                                  .add(LoadCartePlafondEvent());
                            } else if (state.requestStateUp ==
                                StateStatus.ERROR) {
                              Navigator.pop(context);
                              Navigator.pop(context);
                              errorAlert(context,
                                  state.errorMessage ?? "Erreur technique");
                            } else {
                              return Container();
                            }
                          },
                          child: Container(),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }

  oppasitionCarte(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Center(
                child: Text(
                  "opposition sur carte".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            content: OppositionCarteWidget());
      },
    );
  }

  rechargeCarte(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Center(
                child: Text(
                  "recharge carte".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                GestureDetector(
                  onTap: () {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      if (!isLoaded)
                        context.read<CompteBloc>().add(LoadComptesVirEvent());
                    });
                    showCompteDialog(context);
                  },
                  child: Container(
                    height: 50,
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black26),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextFormField(
                      controller: compteDebiterController,
                      enabled: false,
                      decoration: InputDecoration(
                        errorStyle: TextStyle(
                          color: Theme.of(context)
                              .errorColor, // or any other color
                        ),
                        hintText: 'Compte à débiter',
                        hintStyle: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                          color: Colors.black45,
                        )),
                        suffixIcon: Icon(Icons.arrow_drop_down_circle_outlined,
                            color:
                                GlobalParams.themes["$banque_id"].iconsColor),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.035,
                  width: MediaQuery.of(context).size.width * 0.7,
                ),
                Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: montantController,
                    cursorColor: secondaryColor7,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      fillColor: secondaryColor7,
                      focusColor: secondaryColor7,
                      hoverColor: secondaryColor7,
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintText: 'Montant',
                      hintStyle: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 16,
                        color: Colors.black45,
                      )),
                      suffixIcon: Icon(Icons.money,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.035,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1),
                  child: Container(
                    height: 40,
                    child: RaisedButton(
                      elevation: 0,
                      onPressed: () {
                        if (selectedAccount != null ||
                            montantController.text.length > 1) {
                          context.read<CarteBloc>().add(LoadRechargeCarte(
                              montantController.text,
                              selectedAccount.identifiantInterne));
                        } else {
                          errorAlert(context, "Veuillez remplir le champ");
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      child: Text(
                        "Valider",
                        style: GoogleFonts.roboto(
                            textStyle:
                                TextStyle(color: Colors.white, fontSize: 18)),
                      ),
                    ),
                  ),
                ),
                BlocListener<CarteBloc, CarteStateBloc>(
                  listener: (context, state) {
                    if (state.requestStateOpp == StateStatus.LOADING) {
                      return showLoading(context);
                    } else if (state.requestStateOpp == StateStatus.LOADED) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      return signeOperation(context, () {
                        context.read<CarteBloc>().add(
                            LoadSigneRechargeCarte(passwordController.text));
                      });
                    } else if (state.requestStateOpp == StateStatus.ERROR) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      return errorAlert(context,
                          state.errorMessage ?? "Une erreur est survenue");
                    } else {
                      return Container();
                    }
                  },
                  child: Container(),
                )
              ],
            ));
      },
    );
  }

  signeOperation(context, Function f) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "CODE DE VALIDATION".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return "Mot de passe ne peut pas etre vide";
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                          child: Text("Annuler",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (passwordController.text.isNotEmpty) {
                            f();
                            // context.read<CarteBloc>().add(
                            //     LoadSigneRechargeCarte(
                            //         passwordController.text));
                            passwordController.text = "";
                          } else {
                            errorAlert(context, "Veuillez remplir le champ");
                          }
                        },
                        child: Text("Valider",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                ),
                BlocListener<CarteBloc, CarteStateBloc>(
                  listener: (context, state) {
                    if (state.requestStateOppSigne == StateStatus.LOADING) {
                      return showLoading(context);
                    } else if (state.requestStateOppSigne ==
                        StateStatus.LOADED) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      return okAlert(
                          context,
                          state.signeRes["message"] ??
                              "Carte opposée avec succes");
                    } else if (state.requestStateOppSigne ==
                        StateStatus.ERROR) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      return errorAlert(context,
                          state.errorMessage ?? "Une erreur est survenue");
                    } else {
                      return Container();
                    }
                  },
                  child: Container(),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  showCompteDialog(context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Center(
                child: Text(
                  "COMPTE À DÉBITER".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            content: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                // shrinkWrap: true,
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                  ),
                  BlocBuilder<CompteBloc, CompteStateBloc>(
                    builder: (context, state) {
                      if (state.requestStateTransaction == StateStatus.NONE) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          context
                              .read<CompteBloc>()
                              .add(LoadComptesChequeEvent());
                        });
                        return Container();
                      } else if (state.requestStateTransaction ==
                          StateStatus.LOADING) {
                        return Center(
                            heightFactor: 10,
                            child: CircularProgressIndicator(
                              backgroundColor: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  GlobalParams
                                      .themes["$banque_id"].appBarColor),
                            ));
                      } else if (state.requestStateTransaction ==
                          StateStatus.ERROR) {
                        return ErreurTextWidget(
                          errorMessage: state.errorMessage,
                          actionEvent: () {
                            context.read<CompteBloc>().add(state.currentAction);
                          },
                        );
                      } else if (state.requestStateTransaction ==
                          StateStatus.LOADED) {
                        isLoaded = true;
                        if (state.listeCompteTransaction.comptes.length == 0)
                          return Center(
                            child: Text(
                              AppLocalizations.of(context).aucun_compte_transaction,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      color: GlobalParams.themes["$banque_id"]
                                          .validationButtonColor)),
                            ),
                          );
                        else
                          return Container(
                            height: MediaQuery.of(context).size.height * 0.5,
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: ListView.separated(
                              controller: _scrollController,
                              shrinkWrap: true,
                              itemCount:
                                  state.listeCompteTransaction.comptes.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    compteDebiterController.text =
                                        "${state.listeCompteTransaction.comptes[index].intitule}";
                                    // compte["Map"]["compteInstanceList"]
                                    //     [index]["intitule"];
                                    setState(() {
                                      selectedAccount = state
                                          .listeCompteTransaction
                                          .comptes[index];
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 3, 5, 3),
                                        child: Text(
                                            state.listeCompteTransaction
                                                .comptes[index].intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Text(
                                          state
                                              .listeCompteTransaction
                                              .comptes[index]
                                              .identifiantInterne,
                                          style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontSize: 15,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .appBarColor),
                                          )),
                                      trailing: RichText(
                                        text: TextSpan(children: [
                                          TextSpan(
                                              text: "1 960",
                                              // "${state.clients.comptes[index].soldeTempsReel ?? ""} ",
                                              style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                                    color: GlobalParams
                                                        .themes["$banque_id"]
                                                        .intituleCmpColor,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                          WidgetSpan(
                                            child: Transform.translate(
                                              offset: const Offset(1, -6),
                                              child: Text("CFA",
                                                  // "${state.clients.comptes[index].devise ?? ""}",
                                                  //superscript is usually smaller in size
                                                  textScaleFactor: 0.7,
                                                  style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: GlobalParams
                                                            .themes[
                                                                "$banque_id"]
                                                            .intituleCmpColor,
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  )),
                                            ),
                                          )
                                        ]),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                      } else {
                        return Container();
                      }
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  recalculCode(context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: Center(
                child: Text(
                  AppLocalizations.of(context).recalcule_pin_titre.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                AppLocalizations.of(context).recalcule_pin_message,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17)),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1),
                  child: Container(
                    height: 40,
                    child: RaisedButton(
                      elevation: 0,
                      onPressed: () {
                        context.read<CarteBloc>().add(LoadRecalculePin());
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      child: Text(
                        AppLocalizations.of(context).recalculer,
                        style: GoogleFonts.roboto(
                            textStyle:
                                TextStyle(color: Colors.white, fontSize: 18)),
                      ),
                    ),
                  ),
                ),
                BlocListener<CarteBloc, CarteStateBloc>(
                  listener: (context, state) {
                    if (state.requestStateOpp == StateStatus.LOADING) {
                      return showLoading(context);
                    } else if (state.requestStateOpp == StateStatus.LOADED) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      return signeOperation(context, () {
                        context.read<CarteBloc>().add(
                            LoadSigneRecalculePin(passwordController.text));
                      });
                    } else if (state.requestStateOpp == StateStatus.ERROR) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      return errorAlert(context,
                          state.errorMessage ?? "Une erreur est survenue");
                    } else {
                      return Container();
                    }
                  },
                  child: Container(),
                )
              ],
            ));
      },
    );
  }

  genererCvv(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              titlePadding: EdgeInsets.zero,
              title: Container(
                height: MediaQuery.of(context).size.height * 0.05,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context).generer_cvv_titre.toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ),
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  DottedBorder(
                    dashPattern: [4, 9, 4, 9],
                    borderType: BorderType.RRect,
                    radius: Radius.circular(22),
                    padding: EdgeInsets.all(5),
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).code_gen,
                        style: GoogleFonts.roboto(fontSize: 17),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1,
                    ),
                    child: Container(
                      height: 40,
                      child: RaisedButton(
                        elevation: 0,
                        onPressed: () {
                          context.read<CarteBloc>().add(LoadGenereCvv());
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        child: Text(
                            AppLocalizations.of(context).generer_cvv_titre,
                          style: GoogleFonts.roboto(
                              textStyle:
                                  TextStyle(color: Colors.white, fontSize: 18)),
                        ),
                      ),
                    ),
                  ),
                  BlocListener<CarteBloc, CarteStateBloc>(
                    listener: (context, state) {
                      if (state.requestStateOpp == StateStatus.LOADING) {
                        return showLoading(context);
                      } else if (state.requestStateOpp == StateStatus.LOADED) {
                        // Navigator.pop(context);
                        Navigator.pop(context);
                        return signeOperation(context, () {
                          context
                              .read<CarteBloc>()
                              .add(LoadSigneGenereCvv(passwordController.text));
                        });
                      } else if (state.requestStateOpp == StateStatus.ERROR) {
                        passwordController.text = "";
                        // Navigator.pop(context);
                        Navigator.pop(context);
                        return errorAlert(context,
                            state.errorMessage ?? "Une erreur est survenue");
                      } else {
                        return Container();
                      }
                    },
                    child: Container(),
                  )
                ],
              ));
        });
  }

  bloquerCarteDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        context.read<CarteBloc>().add(CardStatusEvent());
        return BloquerCarteWidget();
      },
    );
  }
}
