import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/cartes/carte.page.dart';
import 'package:LBA/pages/cartes/widgets/historiqueDemande/historique_demande_carte.widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HistoriqueDemandeCarte extends StatelessWidget {
  const HistoriqueDemandeCarte({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "Historique",
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_rounded),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CartePage()))),
          bottom: TabBar(
              indicatorColor: GlobalParams.themes["$banque_id"].colorTab,
              tabs: [
                Tab(
                  text: "demandes de carte",
                ),
                Tab(
                  text: "demandes d'opposition",
                ),
              ]),
        ),
        body: Stack(
          children: <Widget>[
            Container(
                child: TabBarView(
              children: <Widget>[
                Container(child: HistoriqueDemandeCarteWidget()),
                Container(child: HistoriqueDemandeCarteWidget())
              ],
            ))
          ],
        ),
      ),
    );
  }
}
