import 'dart:ui';

import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ListCarteWidget extends StatelessWidget {
  ScrollController _scrollController = ScrollController();

  // TokenState tokenState;
  // CarteState carteState;
  bool isLoaded = false;
  ListCarteWidget() {
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // carteState = Provider.of<CarteState>(context, listen: false);
    // carteState.loadCartes(tokenState);
  }
  // TokenStateBloc tokenStateBloc;
  // Token tn;
  @override
  Widget build(BuildContext context) {
    // tokenStateBloc = BlocProvider.of<TokenStateBloc>(context);
    // tn = tokenStateBloc.getToken;
    // print("${tn.refreshExpiresIn}");
    return BlocBuilder<CarteBloc, CarteStateBloc>(builder: (context, state) {
      print('requestState listCarte: ${state.requestState}');
      if (state.requestState == StateStatus.NONE) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (!isLoaded) context.read<CarteBloc>().add(LoadCarteEvent());
        });
        return Container();
      } else if (state.requestState == StateStatus.LOADING) {
        return Center(
            widthFactor: 12,
            // heightFactor: 5,
            child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
      } else if (state.requestState == StateStatus.ERROR) {
        return
         ErreurTextWidget(
          errorMessage: state.errorMessage,
          actionEvent: () {
            context.read<CarteBloc>().add(state.currentAction);
          },
        );
      } else if (state.requestState == StateStatus.LOADED) {
        isLoaded = true;

        if (state.carte.map.listCarteInstance.length == 0) {
          return Center(
            child: Text(""),
          );
        } else
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            width: MediaQuery.of(context).size.width,
            child: CarouselSlider(
              // controller: _scrollController,
              // shrinkWrap: true,
              // scrollDirection: Axis.horizontal,
              options: CarouselOptions(
                height: MediaQuery.of(context).size.height * 0.25,
                viewportFraction: 0.75,
                scrollDirection: Axis.horizontal,
                autoPlayCurve: Curves.fastOutSlowIn,
                enableInfiniteScroll: false,
                enlargeCenterPage: true,
                pageSnapping: true,
                onPageChanged: (index, reason) {
                  context.read<CarteBloc>().add(ChangeCurrentCartEvent(
                      state.carte.map.listCarteInstance[index]));
                },
              ),
              // itemCount: state.carte.map.listCarteInstance.length,
              // itemBuilder: (context, index) {

              items: state.carte.map.listCarteInstance.map((index) {
                return Builder(builder: (BuildContext context) {
                  return GestureDetector(
                    onTap: () {
                      // _scrollController.animateTo(
                      //     (index *
                      //             (MediaQuery.of(context).size.width *
                      //                 0.72))
                      //         .toDouble(),
                      //     duration: Duration(milliseconds: 500),
                      //     curve: Curves.easeIn);
                      // context.read<CarteBloc>().add(ChangeCurrentCartEvent(
                      //     state.carte.map.listCarteInstance[index]));
                      // cs.loadCarteOpperation(
                      //     tokenState,
                      //     cs.carte.map.listCarteInstance[index].id,
                      //     cs.carte.map.listCarteInstance[index].numero,
                      //     cs.carte.map.listCarteInstance[index].numeroCompte,
                      //     cs.carte.map.listCarteInstance[index]);
                    },
                    child: Container(
                      // padding: EdgeInsets.fromLTRB(5, 15, 15, 5), //20
                      child: Stack(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: Transform.scale(
                              scale: 1.115,
                              child: Image.asset(
                                  "assets/$banque_id/images/Card1.png",
                                  fit: BoxFit.cover),
                            ),
                          ),
                          Positioned(
                              top: MediaQuery.of(context).size.height * 0.09,
                              left: MediaQuery.of(context).size.width * 0.23,
                              right: MediaQuery.of(context).size.width * 0.1,
                              child: Text(
                                // "CARTE",
                                index.typeCarte,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        color: Colors.white, fontSize: 12)),
                              )),
                          Positioned(
                              top: MediaQuery.of(context).size.height * 0.15,
                              left: MediaQuery.of(context).size.width * 0.12,
                              child: Text(
                                index.numero,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        color: Colors.white, fontSize: 12)),
                              )),
                          Positioned(
                              top: MediaQuery.of(context).size.height * 0.12,
                              left: MediaQuery.of(context).size.width * 0.45,
                              right: MediaQuery.of(context).size.width * 0.1,
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  index.dateExpiration,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 13),
                                ),
                              )),
                          // Positioned(
                          //   top: MediaQuery.of(context).size.height * 0.2,
                          //   left: MediaQuery.of(context).size.width * 0.3,
                          //   // right: MediaQuery.of(context).size.width * 0.4,
                          //   child: DotsIndicator(
                          //     dotsCount:
                          //         state.carte.map.listCarteInstance.length,
                          //     // cs.carte.map.listCarteInstance.length,
                          //     position: index.toDouble(),
                          //     decorator: DotsDecorator(
                          //       color: Color.fromRGBO(89, 43, 95, 0.5),
                          //       activeColor: GlobalParams
                          //           .themes["$banque_id"].appBarColor,
                          //       size: const Size.square(9.0),
                          //       activeSize: const Size(18.0, 9.0),
                          //       activeShape: RoundedRectangleBorder(
                          //           borderRadius:
                          //               BorderRadius.circular(5.0)),
                          //     ),
                          //   ),
                          // ),
                          // Positioned(
                          //   top: MediaQuery.of(context).size.height * 0.22,
                          //   child: Padding(
                          //     padding: const EdgeInsets.only(right: 18),
                          //     child: Column(
                          //       children: [
                          //         Container(
                          //           width: MediaQuery.of(context).size.width,
                          //           child: ListTile(
                          //             title: Text(
                          //               "Numéro de compte",
                          //               maxLines: 1,
                          //             ),
                          //             trailing: Text(
                          //               "${cs.carte.map.listCarteInstance[index].numeroCompte}",
                          //               maxLines: 1,
                          //             ),
                          //           ),
                          //         ),
                          //         Container(
                          //           child: ListTile(
                          //             title: Text("Stattut de carte"),
                          //             trailing: Text(
                          //                 "${cs.carte.map.listCarteInstance[index].statut}"),
                          //           ),
                          //         ),
                          //         Container(
                          //           child: ListTile(
                          //             title: Text("Date Expiration"),
                          //             trailing: Text(
                          //                 "${cs.carte.map.listCarteInstance[index].dateExpiration}"),
                          //           ),
                          //         )
                          //       ],
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  );
                });
              }).toList(),
              // : CarouselSlider(
              //     options: CarouselOptions(
              //       height: 400.0,
              //       enableInfiniteScroll: false,
              //       viewportFraction: 0.75,
              //     ),
              //     items: (cs.carte.map.listCarteInstance).map((carte) {
              //       return Builder(
              //         builder: (BuildContext context) {
              //           return Padding(
              //             padding: const EdgeInsets.only(top: 15),
              //             child: Container(
              //                 width: MediaQuery.of(context).size.width,
              //                 // margin: EdgeInsets.symmetric(horizontal: 2.0),
              //                 // decoration: BoxDecoration(color: Colors.amber),
              //                 child: Stack(
              //                   children: [
              //                     Image.asset(
              //                       "assets/images/1CAM.png",
              //                       fit: BoxFit.contain,
              //                       // height: 200,
              //                       // width: 300,
              //                     ),
              //                     Positioned(
              //                         top: MediaQuery.of(context).size.height *
              //                             0.04,
              //                         left: MediaQuery.of(context).size.width *
              //                             0.15,
              //                         right: MediaQuery.of(context).size.width *
              //                             0.1,
              //                         child: Text(
              //                           carte.typeCarte,
              //                           style: TextStyle(color: Colors.white),
              //                         )),
              //                     Positioned(
              //                         top: MediaQuery.of(context).size.height *
              //                             0.1,
              //                         left: MediaQuery.of(context).size.width *
              //                             0.08,
              //                         child: Text(
              //                           carte.numero,
              //                           style: TextStyle(color: Colors.white),
              //                         )),
              //                     Positioned(
              //                         top: MediaQuery.of(context).size.height *
              //                             0.1,
              //                         left: MediaQuery.of(context).size.width *
              //                             0.5,
              //                         right: MediaQuery.of(context).size.width *
              //                             0.1,
              //                         child: Text(
              //                           carte.dateExpiration,
              //                           style: TextStyle(color: Colors.white),
              //                         )),
              //                   ],
              //                 )),
              //           );
              //         },
              //       );
              //     }).toList(),
              //   )
            ),
          );
      } else {
        return Container();
      }
    });
  }
}
