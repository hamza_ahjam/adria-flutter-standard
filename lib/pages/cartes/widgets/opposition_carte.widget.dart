import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/cheques/widgets/demandeChequier.form.radio.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OppositionCarteWidget extends StatefulWidget {
  OppositionCarteWidget({Key key}) : super(key: key);

  @override
  _OppositionCarteWidgetState createState() => _OppositionCarteWidgetState();
}

class _OppositionCarteWidgetState extends State<OppositionCarteWidget> {
  var radio1 = '';
  var passwordController = TextEditingController();
  // final _formPsdOpp = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CarteBloc, CarteStateBloc>(
      builder: (context, state) {
        if (state.requestStateMotif == StateStatus.LOADING) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.15,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Center(
                widthFactor: 12,
                // heightFactor: 5,
                child: CircularProgressIndicator(
                  backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      GlobalParams.themes["$banque_id"].appBarColor),
                )),
          );
        } else if (state.requestStateMotif == StateStatus.ERROR) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.15,
            width: MediaQuery.of(context).size.width * 0.8,
            child: ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<CarteBloc>().add(state.currentAction);
              },
            ),
          );
        } else if (state.requestStateMotif == StateStatus.LOADED) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
                width: MediaQuery.of(context).size.width * 0.8,
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.04),
                child: Text("Motif",
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold))),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.15,
                width: MediaQuery.of(context).size.width * 0.8,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: state
                        .motifOpposition["Map"]["motifOppositionList"].length,
                    itemBuilder: (context, index) => LabeledRadio(
                          value: state.motifOpposition["Map"]
                              ["motifOppositionList"][index]["libelle"],
                          groupValue: radio1,
                          onChanged: (v) {
                            setState(() {
                              radio1 = state.motifOpposition["Map"]
                                  ["motifOppositionList"][index]["libelle"];
                            });
                          },
                          label: Text(
                            state.motifOpposition["Map"]["motifOppositionList"]
                                [index]["libelle"],
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(fontSize: 17)),
                          ),
                          padding: EdgeInsets.zero,
                        )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1),
                child: Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width * 0.1,
                  child: RaisedButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    onPressed: () {
                      if (radio1.isNotEmpty) {
                        confirmOpp(context);
                      } else {
                        errorAlert(context, "Veuillez choisir le motif");
                      }
                    },
                    child: Text("Opposer",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                        ))),
                  ),
                ),
              ),
              BlocListener<CarteBloc, CarteStateBloc>(
                listener: (context, state) {
                  if (state.requestStateOpp == StateStatus.LOADING) {
                    return showLoading(context);
                  } else if (state.requestStateOpp == StateStatus.LOADED) {
                    // Navigator.pop(context);
                    Navigator.pop(context);
                    return signeOpp(context);
                  } else if (state.requestStateOpp == StateStatus.ERROR) {
                    // Navigator.pop(context);
                    Navigator.pop(context);
                    return errorAlert(context,
                        state.errorMessage ?? "Une erreur est survenue");
                  } else {
                    return Container();
                  }
                },
                child: Container(),
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  confirmOpp(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 20),
                child: Text(
                  "Etes-vous sur de vouloir opposer cette carte ?",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18)),
                ),
              ),
              ListTile(
                leading: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text("Non",
                        style: TextStyle(color: Colors.white, fontSize: 16))),
                trailing: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      // Navigator.pop(context);
                      Navigator.pop(context);

                      context.read<CarteBloc>().add(LoadOpposition(radio1));
                    },
                    color: Colors.white,
                    child: Text("Oui",
                        style: TextStyle(
                            fontSize: 16,
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor))),
              )
            ],
          ),
        );
      },
    );
  }

  signeOpp(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "CODE DE VALIDATION".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return "Mot de passe ne peut pas etre vide";
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                          child: Text("Annuler",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          if (passwordController.text.isNotEmpty) {
                            context.read<CarteBloc>().add(
                                LoadSigneOpposition(passwordController.text));
                          } else {
                            errorAlert(context, "Veuillez remplir le champ");
                          }
                        },
                        child: Text("Valider",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                ),
                BlocListener<CarteBloc, CarteStateBloc>(
                  listener: (context, state) {
                    if (state.requestStateOppSigne == StateStatus.LOADING) {
                      return showLoading(context);
                    } else if (state.requestStateOppSigne ==
                        StateStatus.LOADED) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      return okAlert(context,
                          "Merci de nous faire parvenir la déclaration de perte ou de vol dans 24h"
                          // state.signeRes["message"] ??
                          //     "Carte opposée avec succes"
                          );
                    } else if (state.requestStateOppSigne ==
                        StateStatus.ERROR) {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      return errorAlert(context,
                          state.errorMessage ?? "Une erreur est survenue");
                    } else {
                      return Container();
                    }
                  },
                  child: Container(),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
