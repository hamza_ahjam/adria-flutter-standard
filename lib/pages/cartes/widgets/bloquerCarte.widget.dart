import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';

class BloquerCarteWidget extends StatefulWidget {

  BloquerCarteWidget({Key key}) : super(key: key);

  @override
  State<BloquerCarteWidget> createState() => _BloquerCarteWidgetState();
}

class _BloquerCarteWidgetState extends State<BloquerCarteWidget> {
  PageController _pageController = PageController();

  int page = 0;
  String textValue;
  String typeBloque;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CarteBloc, CarteStateBloc>(
        builder: (context, state) {
          if (state.requestCardStatus == StateStatus.LOADING) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              content: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      GlobalParams.themes["$banque_id"].appBarColor),
                ),
              ),
            );
          } else if (state.requestCardStatus == StateStatus.LOADED) {
            print('isContactLess Blocked ? ${state.cardStatus['cardStatus']['isContactLess']}, page $page');
            textValue = (page == 0 && state.cardStatus['cardStatus']['isContactLess'] == 0)
                  ? "Bloquer" : (page == 1 && state.cardStatus['cardStatus']['isBloquerInternational'] == 0)
                  ? "Bloquer" : (page == 2 && state.cardStatus['cardStatus']['isBloquerAll'] == 0)
                  ? "Bloquer" : "Débloquer";
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              titlePadding: EdgeInsets.zero,
              // clipBehavior: Clip.antiAliasWithSaveLayer,
              insetPadding: EdgeInsets.zero,
              title: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "Blocage carte".toUpperCase(),
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
              content: Container(
                width: 300,
                height: 150,
                child: PageView(
                  onPageChanged: (page){
                    setState(() {
                      this.page = page;
                    });
                  },
                  controller: _pageController,
                  scrollDirection: Axis.horizontal,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(width: 24,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/$banque_id/images/bloquer_carte_lock_icon.svg",
                              color: Colors.white,
                            ),
                            Text(
                              'Contactless',
                              style: GoogleFonts.roboto(
                                  color: GlobalParams.themes["$banque_id"].appBarColor,
                                  fontSize: 20),),
                          ],
                        ),
                        IconButton(
                            onPressed: (){
                              _pageController.animateToPage(
                                  1,
                                  duration: Duration(seconds: 1),
                                  curve: Curves.ease);
                            },
                            icon: Icon(
                                Icons.arrow_forward_ios,
                                color: GlobalParams.themes["$banque_id"].intituleCmpColor)),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          onPressed: (){
                            _pageController.animateToPage(
                                0,
                                duration: Duration(seconds: 1),
                                curve: Curves.ease);
                          },
                          icon: Icon(
                              Icons.arrow_back_ios),
                          color: GlobalParams.themes["$banque_id"].intituleCmpColor,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/$banque_id/images/bloquer_carte_lock_icon.svg",
                              color: Colors.white,
                            ),
                            Text(
                              'International',
                              style: GoogleFonts.roboto(
                                  color: GlobalParams.themes["$banque_id"].appBarColor,
                                  fontSize: 20),),
                          ],
                        ),
                        IconButton(
                            onPressed: (){
                              _pageController.animateToPage(
                                  2,
                                  duration: Duration(seconds: 1),
                                  curve: Curves.ease);
                            },
                            icon: Icon(
                                Icons.arrow_forward_ios,
                                color: GlobalParams.themes["$banque_id"].intituleCmpColor)),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          onPressed: (){
                            _pageController.animateToPage(
                                1,
                                duration: Duration(seconds: 1),
                                curve: Curves.ease);
                          },
                          icon: Icon(
                              Icons.arrow_back_ios),
                          color: GlobalParams.themes["$banque_id"].intituleCmpColor,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8),
                              child: SvgPicture.asset(
                                "assets/$banque_id/images/bloquer_carte_lock_icon.svg",
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              'La carte   ',
                              style: GoogleFonts.roboto(
                                  color: GlobalParams.themes["$banque_id"].appBarColor,
                                  fontSize: 20),),
                          ],
                        ),
                        Container(width: 24,)
                      ],
                    ),
                  ],
                ),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: MaterialButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      color:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                      onPressed: () {
                        if (textValue == 'Bloquer'){
                          typeBloque = _pageController.page.toInt() == 0 ? 'disableContactLess' : _pageController.page.toInt() == 1
                                       ? 'disableInternational'
                                       : 'bloquerCarte';
                        }else{
                          typeBloque = _pageController.page.toInt() == 0 ? 'enableContactLess' : _pageController.page.toInt() == 1
                                       ? 'enableInternational'
                                       : 'debloquerCarte';
                        }
                        context.read<CarteBloc>().add(BloquerCarteEvent(
                            typeBloque: typeBloque
                        ));
                        showBlocCarteDialog(context);
                        //print('page ${_pageController.page}');
                      },
                      child: Text(
                          textValue,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                fontSize: 17,
                                color: Colors.white,
                              )
                          )
                      )
                  ),
                ),
              ],
              actionsAlignment: MainAxisAlignment.center,
            );
          } else if (state.requestCardStatus == StateStatus.ERROR) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              content: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      "assets/$banque_id/images/error.png",
                      width: 50,
                      height: 50,
                      color: GlobalParams.themes["$banque_id"].appBarColor,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        state.errorMessage != null
                            ? state.errorMessage.toString()
                            : state.resBloque['message'],
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.black, fontSize: 20)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        onPressed: () {
                          context.read<CarteBloc>().add(state.currentAction);
                          //Navigator.pop(context);
                        },
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor,
                        child: Text("Ok",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 17, color: Colors.white))),
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else return Container();
  });
  }

  showBlocCarteDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return BlocBuilder<CarteBloc, CarteStateBloc>(
            builder: (context, state) {
              if (state.requestStateBloque == StateStatus.LOADING) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ),
                  ),
                );
              } else if (state.requestStateBloque == StateStatus.LOADED) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        "assets/$banque_id/images/imo.png",
                        width: 50,
                        height: 50,
                        color: GlobalParams.themes["$banque_id"].appBarColor,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          '${state.resBloque['message']}',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle:
                              TextStyle(color: Colors.black, fontSize: 20)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          child: Text("Ok",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17, color: Colors.white))),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (state.requestStateBloque == StateStatus.ERROR) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  content: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/$banque_id/images/error.png",
                          width: 50,
                          height: 50,
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            state.errorMessage != null
                                ? state.errorMessage.toString()
                                : state.resBloque['message'],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              //Navigator.pop(context);
                              context.read<CarteBloc>().add(state.currentAction);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Text("Ok",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 17, color: Colors.white))),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else
                return Container();
            },
          );
        });
  }
}
