import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OpperationCarteWidget extends StatelessWidget {
  ScrollController _scrollController = ScrollController();
  // TokenState tokenState;
  // CarteState carteState;

  OpperationCarteWidget();

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: BlocBuilder<MouvementCarteBloc, MouvementCarteStateBloc>(
          builder: (context, state) {
        if (state.requestState == StateStatus.LOADING) {
          return Center(
              heightFactor: 5,
              child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
        } else if (state.requestState == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<MouvementCarteBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestState == StateStatus.LOADED) {
          if (state.mouvementCarte.map.mouvementCarteList.length == 0)
            return Center(
              heightFactor: 10,
              child: Text(
                AppLocalizations.of(context).aucun_mvt,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(fontSize: 16, color: GlobalParams.themes[banque_id].appBarColor)),
              ),
            );
          else
            return ListView.separated(
                controller: _scrollController,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: state.mouvementCarte.map.mouvementCarteList.length,
                separatorBuilder: (context, index) => Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: MediaQuery.of(context)
                            .size
                            .width *
                            0.25,
                        child: Divider(
                          color: Colors.black26,
                          height: 10,
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context)
                            .size
                            .width *
                            0.25,
                        child: Divider(
                          color: Colors.black26,
                          height: 10,
                        ),
                      )
                    ],
                  ),
                ),
                itemBuilder: (context, index) {
                  return ListTile(
                    visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                    // leading: Image.asset(
                    //   "assets/$banque_id/images/Picto_virement.png",
                    //   fit: BoxFit.fill,
                    //   width: 33,
                    //   height: 33,
                    // ),
                    title: Text(
                      "${intl.DateFormat.yMMMMd('fr').format(intl.DateFormat("dd/MM/yyyy").parse(state.mouvementCarte.map.mouvementCarteList[index]["dateOperation"]))}",
                      style: TextStyle(
                          color: mouvementsTextColor,
                          fontFamily: KprimaryFont,
                          fontSize: 14,
                          fontWeight: KprimaryWeight),
                    ),
                    subtitle: Text(
                        "${state.mouvementCarte.map.mouvementCarteList[index]["libelleOperation"]}",
                        style: TextStyle(
                            color: mouvementsTextColor,
                            fontFamily: KprimaryFont,
                            fontSize: 13)),
                    trailing: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text: state.mouvementCarte.map
                                        .mouvementCarteList[index]["sens"] !=
                                    "C"
                                ? " ${state.mouvementCarte.map.mouvementCarteList[index]["montantOperationLoc_formatted"]}"
                                : " + ${state.mouvementCarte.map.mouvementCarteList[index]["montantOperationLoc_formatted"]}",
                            style: TextStyle(
                                color: mouvementsTextColor,
                                fontWeight: FontWeight.bold,
                                fontFamily: KprimaryFont,
                                fontSize: 15)),
                        WidgetSpan(
                          child: Transform.translate(
                            offset: const Offset(2, -4),
                            child: Text(
                              " ${state.mouvementCarte.map.mouvementCarteList[index]["devise"]}",
                              //superscript is usually smaller in size
                              textScaleFactor: 0.7,
                              style: TextStyle(
                                  color: mouvementsTextColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                            ),
                          ),
                        )
                      ]),
                    ),
                  );
                });
        } else {
          return Container();
        }
      }),
    );
  }
}
