import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/compte.model.dart';
import 'package:LBA/models/typeCarte.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../constants.dart';

class RecapitulatifCarteWidget extends StatefulWidget {
  ListTypeCarte carte;
  Compte compte;
  RecapitulatifCarteWidget({Key key, this.carte, this.compte}) : super(key: key);

  @override
  State<RecapitulatifCarteWidget> createState() => _RecapitulatifCarteWidgetState();
}

class _RecapitulatifCarteWidgetState extends State<RecapitulatifCarteWidget> {
  final _formPsdKey = GlobalKey<FormState>();

  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25))),
      titlePadding: EdgeInsets.zero,
      // clipBehavior: Clip.antiAliasWithSaveLayer,
      insetPadding: EdgeInsets.zero,
      title: Container(
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                AppLocalizations.of(context).recap,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ],
        ),
      ),
      content: SingleChildScrollView(
        child: Container(
          width: 300,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  ListTile(
                    title: Text(
                      "${AppLocalizations.of(context).type_carte} :",
                      style: TextStyle(
                          color: mouvementsTextColor,
                          fontFamily: KprimaryFont,
                          fontSize: 14,),
                    ),
                    trailing: Text(
                      "${widget.carte.typeCarteLibelle}",
                      style: TextStyle(
                          color: mouvementsTextColor,
                          fontFamily: KprimaryFont,
                          fontSize: 14,
                          fontWeight: KprimaryWeight),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      "${AppLocalizations.of(context).compte_choisis} :",
                      style: TextStyle(
                          color: mouvementsTextColor,
                          fontFamily: KprimaryFont,
                          fontSize: 14,),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: ListTile(
                      tileColor: Colors.grey[200],
                      visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                      dense: true,
                      title: Text(
                        "${widget.compte.identifiantInterne}",
                        style: TextStyle(
                            color: mouvementsTextColor,
                            fontFamily: KprimaryFont,
                            fontSize: 14,),
                      ),
                      subtitle: Text(
                        "${widget.compte.intitule}",
                        style: TextStyle(
                          color: mouvementsTextColor,
                          fontFamily: KprimaryFont,
                          fontSize: 14,
                          fontWeight: KprimaryWeight),
                      ),
                      trailing: RichText(
                        text: TextSpan(
                            children:[
                              TextSpan(
                                text:double.parse(widget.compte.soldeComptable.replaceAll(' ', '').replaceAll(',', '.')) > 0
                                    ?"+ ${double.parse(widget.compte.soldeComptable.replaceAll(' ', '').replaceAll(',', '.')).toInt()}"
                                    :double.parse(widget.compte.soldeComptable.replaceAll(' ', '').replaceAll(',', '.')) > 0,
                                style: TextStyle(
                                    color: double.parse(widget.compte.soldeComptable.replaceAll(' ', '').replaceAll(',', '.')) > 0
                                        ? Colors.green
                                        : Colors.red,
                                    fontFamily: KprimaryFont,
                                    fontSize: 14,
                                    fontWeight: KprimaryWeight),
                              ),
                              WidgetSpan(
                                child: Transform.translate(
                                  offset: const Offset(1, -6),
                                  child: Text("CFA",
                                      // "${state.clients.comptes[index].devise ?? ""}",
                                      //superscript is usually smaller in size
                                      textScaleFactor: 0.7,
                                      style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: double.parse(widget.compte.soldeComptable.replaceAll(' ', '').replaceAll(',', '.')) > 0
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: 21,
                                            fontWeight: FontWeight.w600),
                                      )),
                                ),
                              )
                            ]
                        ),
                      )
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: MaterialButton(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            color:
            GlobalParams.themes["$banque_id"].intituleCmpColor,
            onPressed: () {
                context.read<CarteBloc>().add(SaveCommandeCarteEvent(
                    identifiantInterne: widget.compte.identifiantInterne,
                    intituleCompte: widget.compte.intitule,
                    paysBeneficiaire: 'MA',
                    typeCatreLibelle: widget.carte.typeCarteLibelle,
                    typeCarteCode: widget.carte.typeCarte));

                showSaveCammand(context);
            },
            child: Text(AppLocalizations.of(context).commander,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                      fontSize: 17,
                      color: Colors.white,
                    ))),
          ),
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }

  showSaveCammand(context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return BlocBuilder<CarteBloc, CarteStateBloc>(
            builder: (context, state) {
              if (state.requestStateSave == StateStatus.LOADING) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ),
                  ),
                );
              } else if (state.requestStateSave == StateStatus.LOADED) {
                return signeCommnadeCarteDialog(state.res['id'], state.res['taskId'], context);
              } else if (state.requestStateSave == StateStatus.ERROR) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  content: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/$banque_id/images/error.png",
                          width: 50,
                          height: 50,
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            state.errorMessage.toString(),
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Text("Ok",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 17, color: Colors.white))),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else
                return Container();
            },
          );
        });
  }

  signeCommnadeCarteDialog(id, taskId, BuildContext context) {
    return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              titlePadding: EdgeInsets.zero,
              title: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: SvgPicture.asset(
                        "assets/$banque_id/images/otp.svg",
                        color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                      ),
                    ),
                    Text(
                      AppLocalizations.of(context).code_validation.toUpperCase(),
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ),
              content: Form(
                key: _formPsdKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                      width: MediaQuery.of(context).size.width * 0.9,
                    ),
                    Text(
                        AppLocalizations.of(context).saisir_mdp,
                        textAlign: TextAlign.center,
                        style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height * 0.045),
                      child: Container(
                        height: 50,
                        child: TextFormField(
                          controller: passwordController,
                          keyboardType: TextInputType.number,
                          validator: (v) {
                            if (v.isEmpty)
                              return AppLocalizations.of(context).error_mdp;
                            return null;
                          },
                          style: TextStyle(color: Colors.black),
                          textAlign: TextAlign.center,
                          obscureText: true,
                          decoration: InputDecoration(
                            hintText: '___   ___   ___   ___   ___   ___',
                            hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: RaisedButton(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(
                                      color: GlobalParams
                                          .themes["$banque_id"].intituleCmpColor)),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: Colors.white,
                              child: Text(AppLocalizations.of(context).annuler,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          fontSize: 17,
                                          color: GlobalParams.themes["$banque_id"]
                                              .intituleCmpColor)))),
                        ),
                        Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                            onPressed: () {
                              if (_formPsdKey.currentState.validate()) {
                                context.read<CarteBloc>().add(SigneCommandeCarteEvent(
                                    password: passwordController.text));
                                confirmSignCommande(context);
                              }
                            },
                            child: Text(AppLocalizations.of(context).valider,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                      fontSize: 17,
                                      color: Colors.white,
                                    ))),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );

  }

  confirmSignCommande(context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return BlocBuilder<CarteBloc, CarteStateBloc>(
            builder: (context, state) {
              if (state.requestStateSigne == StateStatus.LOADING) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ),
                  ),
                );
              } else if (state.requestStateSigne == StateStatus.LOADED) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25))),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        "assets/$banque_id/images/imo.png",
                        width: 50,
                        height: 50,
                        color: GlobalParams.themes["$banque_id"].appBarColor,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          state.res['message'],
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle:
                              TextStyle(color: Colors.black, fontSize: 20)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          child: Text(AppLocalizations.of(context).ok,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17, color: Colors.white))),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (state.requestStateSigne == StateStatus.ERROR) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  content: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/$banque_id/images/error.png",
                          width: 50,
                          height: 50,
                          color: GlobalParams.themes["$banque_id"].appBarColor,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            state.errorMessage != null
                                ? state.errorMessage.toString()
                                : state.res['message'],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Colors.black, fontSize: 20)),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              passwordController.text = '';
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Text(AppLocalizations.of(context).ok,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 17, color: Colors.white))),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else
                return Container();
            },
          );
        });
  }
}
