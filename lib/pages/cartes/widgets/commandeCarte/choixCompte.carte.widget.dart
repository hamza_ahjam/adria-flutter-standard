import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/typeCarte.model.dart';
import 'package:LBA/pages/cartes/widgets/commandeCarte/recapitulatif.carte.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../constants.dart';

class ChoixCompteCarte extends StatelessWidget {
  ListTypeCarte carte;
  ChoixCompteCarte({Key key, this.carte}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25))),
      titlePadding: EdgeInsets.zero,
      // clipBehavior: Clip.antiAliasWithSaveLayer,
      insetPadding: EdgeInsets.zero,
      title: Container(
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                AppLocalizations.of(context).choix_compte,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ],
        ),
      ),
      content:
      Directionality(
        textDirection: TextDirection.ltr,
            child: BlocBuilder<CompteBloc, CompteStateBloc>(builder: (context, state) {
        if (state.requestStateCheque == StateStatus.LOADING) {
            return Container(
              height: 300,
              width: 300,
              child: Center(
                  heightFactor: 5,
                  child: CircularProgressIndicator(
                    backgroundColor:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        GlobalParams.themes["$banque_id"].appBarColor),
                  )),
            );
        } else if (state.requestStateCheque == StateStatus.ERROR) {
            return Container(
              height: 300,
              width: 300,
              child: ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<CompteBloc>().add(state.currentAction);
                },
              ),
            );
        } else if (state.requestStateCheque == StateStatus.LOADED) {
            if (state.listeCompteCheque.comptes.length == 0)
              return Container(
                height: 300,
                width: 300,
                child: Center(
                  heightFactor: 10,
                  child: Text(
                    AppLocalizations.of(context).aucune_carte,
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(fontSize: 16, color: principaleColor5)),
                  ),
                ),
              );
            else
              return SingleChildScrollView(
                child: Container(
                  width: 300,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ConstrainedBox(
                        constraints: new BoxConstraints(maxHeight: 300),
                        child: ListView.separated(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: state.listeCompteCheque.comptes.length,
                            separatorBuilder: (context, index) => Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: MediaQuery.of(context).size.width *
                                            0.25,
                                        child: Divider(
                                          color: Colors.black26,
                                          height: 10,
                                        ),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width *
                                            0.25,
                                        child: Divider(
                                          color: Colors.black26,
                                          height: 10,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                  choixRecapDialog(context, carte,
                                      state.listeCompteCheque.comptes[index]);
                                },
                                child: ListTile(
                                    visualDensity: VisualDensity(
                                        horizontal: 0, vertical: -4),
                                    title: Text(
                                      "${state.listeCompteCheque.comptes[index].identifiantInterne}",
                                      style: TextStyle(
                                          color: mouvementsTextColor,
                                          fontFamily: KprimaryFont,
                                          fontSize: 14,
                                          fontWeight: KprimaryWeight),
                                    ),
                                    subtitle: Text(
                                        "${state.listeCompteCheque.comptes[index].intitule}",
                                        style: TextStyle(
                                            color: mouvementsTextColor,
                                            fontFamily: KprimaryFont,
                                            fontSize: 13)),
                                    trailing: RichText(
                                      text: TextSpan(children: [
                                        TextSpan(
                                          text: double.parse(state
                                                      .listeCompteCheque
                                                      .comptes[index]
                                                      .soldeComptable
                                                      .replaceAll(' ', '')
                                                      .replaceAll(',', '.')) >
                                                  0
                                              ? "+ ${double.parse(state.listeCompteCheque.comptes[index].soldeComptable.replaceAll(' ', '').replaceAll(',', '.')).toInt()}"
                                              : double.parse(state
                                                      .listeCompteCheque
                                                      .comptes[index]
                                                      .soldeComptable
                                                      .replaceAll(' ', '')
                                                      .replaceAll(',', '.'))
                                                  .toInt()
                                                  .toString(),
                                          style: TextStyle(
                                              color: double.parse(state
                                                          .listeCompteCheque
                                                          .comptes[index]
                                                          .soldeComptable
                                                          .replaceAll(' ', '')
                                                          .replaceAll(',', '.')) >
                                                      0
                                                  ? Colors.green
                                                  : Colors.red,
                                              fontFamily: KprimaryFont,
                                              fontSize: 14,
                                              fontWeight: KprimaryWeight),
                                        ),
                                        WidgetSpan(
                                          child: Transform.translate(
                                            offset: const Offset(1, -6),
                                            child: Text("CFA",
                                                textScaleFactor: 0.7,
                                                style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      color: double.parse(state
                                                                  .listeCompteCheque
                                                                  .comptes[index]
                                                                  .soldeComptable
                                                                  .replaceAll(
                                                                      ' ', '')
                                                                  .replaceAll(
                                                                      ',', '.')) >
                                                              0
                                                          ? Colors.green
                                                          : Colors.red,
                                                      fontSize: 21,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                )),
                                          ),
                                        )
                                      ]),
                                    )),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              );
        } else {
            return Container();
        }
      }),
          ),
    );
  }

  choixRecapDialog(context, carte, compte) {
    return showDialog(
      context: context,
      builder: (context) {
        return RecapitulatifCarteWidget(
          carte: carte,
          compte: compte,
        );
      },
    );
  }
}
