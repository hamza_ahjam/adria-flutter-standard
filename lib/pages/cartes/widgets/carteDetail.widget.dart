import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CarteDetailWidget extends StatelessWidget {
  // CarteState carteState;
  CarteDetailWidget();
  bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CarteBloc, CarteStateBloc>(builder: (context, state) {
      print('requestState carteDetails: ${state.requestState}');
      if (state.requestState == StateStatus.NONE) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (!isLoaded) context.read<CarteBloc>().add(LoadCarteEvent());
        });
        return Container();
      }
      // else if (state.requestState == StateStatus.LOADING) {
      //   return Center(
      //       heightFactor: 5,
      //       child: CircularProgressIndicator(
      //         backgroundColor:
      //             GlobalParams.themes["$banque_id"].intituleCmpColor,
      //         valueColor: AlwaysStoppedAnimation<Color>(
      //             GlobalParams.themes["$banque_id"].appBarColor),
      //       ));
      // }
      else if (state.requestState == StateStatus.ERROR) {
        return ErreurTextWidget(
          errorMessage: state.errorMessage,
          actionEvent: () {
            context.read<CarteBloc>().add(state.currentAction);
          },
        );
      } else if (state.requestState == StateStatus.LOADED) {
        isLoaded = true;
        if (state.carte.map.listCarteInstance.length == 0) {
          return Center(
            child: Text(
              AppLocalizations.of(context).aucune_carte,
              style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18)),
            ),
          );
        } else
          return Container(
            //height: MediaQuery.of(context).size.height * 0.27,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 10, top: 18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(context).num_compte,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold))),
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: Text("${state.currentCarte.numeroCompte}",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: GlobalParams
                                        .themes["$banque_id"].bgMensualiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                    ],
                  ),
                ),
                // ListTile(
                //   visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                //   title: Text("Numéro de compte :",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: Colors.white,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                //   trailing: Text("${state.currentCarte.numeroCompte}",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: GlobalParams
                //                   .themes["$banque_id"].bgMensualiteColor,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(AppLocalizations.of(context).statut_carte,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                    Text("${state.currentCarte.statut}",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: GlobalParams
                                    .themes["$banque_id"].bgMensualiteColor,
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ],
                ),
                // ListTile(
                //   visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                //   title: Text("Statut de carte :",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: Colors.white,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                //   trailing: Text("${state.currentCarte.statut}",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: GlobalParams
                //                   .themes["$banque_id"].bgMensualiteColor,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                // ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(context).date_exp,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold))),
                      Text("${state.currentCarte.dateExpiration}",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: GlobalParams
                                      .themes["$banque_id"].bgMensualiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold))),
                    ],
                  ),
                ),
                // ListTile(
                //   visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                //   title: Text("Date Expiration :",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: Colors.white,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                //   trailing: Text("${state.currentCarte.dateExpiration}",
                //       style: GoogleFonts.roboto(
                //           textStyle: TextStyle(
                //               color: GlobalParams
                //                   .themes["$banque_id"].bgMensualiteColor,
                //               fontSize: 13,
                //               fontWeight: FontWeight.bold))),
                // ),
                BlocBuilder<MouvementCarteBloc, MouvementCarteStateBloc>(
                  builder: (context, state) {
                    if (state.requestStatePl == StateStatus.LOADING) {
                      return Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: ListView(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(AppLocalizations.of(context).retrait,
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold))),
                                Text("...",
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .bgMensualiteColor,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold))),
                              ],
                            ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text("Retrait au Maroc :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text("...",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // ),

                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 12),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppLocalizations.of(context).paiement,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold))),
                                  Text("...",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .bgMensualiteColor,
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold))),
                                ],
                              ),
                            ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text("Paiement au Maroc :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text("...",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // ),
                            // Row(
                            //   mainAxisAlignment:
                            //       MainAxisAlignment.spaceBetween,
                            //   children: [
                            //     Text("Paiement et Retrait à l'étranger :",
                            //         style: GoogleFonts.roboto(
                            //             textStyle: TextStyle(
                            //                 color: Colors.white,
                            //                 fontSize: 13,
                            //                 fontWeight: FontWeight.bold))),
                            //     Text("...",
                            //         style: GoogleFonts.roboto(
                            //             textStyle: TextStyle(
                            //                 color: GlobalParams
                            //                     .themes["$banque_id"]
                            //                     .bgMensualiteColor,
                            //                 fontSize: 13,
                            //                 fontWeight: FontWeight.bold))),
                            //   ],
                            // ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text(
                            //       "Paiement et Retrait à l'étranger :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text("...",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // )
                          ],
                        ),
                      );
                    } else if (state.requestStatePl == StateStatus.ERROR) {
                      return ErreurTextWidget(
                        errorMessage: state.errorMessagePl,
                        actionEvent: () {
                          context
                              .read<MouvementCarteBloc>()
                              .add(state.currentAction);
                        },
                      );
                    } else if (state.requestStatePl == StateStatus.LOADED) {
                      return Container(
                        //height: MediaQuery.of(context).size.height * 0.15,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(AppLocalizations.of(context).retrait,
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold))),
                                Directionality(
                                  textDirection: TextDirection.ltr,
                                  child: Text("10 000 CFA",
                                      // "${state.res["Map"]["plafonds"]["plafondRetraitFotmatted"]}  CFA",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .bgMensualiteColor,
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold))),
                                ),
                              ],
                            ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text("Retrait au Maroc :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text(
                            //       "${state.res["Map"]["plafonds"]["plafondRetraitFotmatted"]}  MAD",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 12),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppLocalizations.of(context).paiement,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold))),
                                  Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: Text("10 000 CFA",
                                        // "${state.res["Map"]["plafonds"]["plafondPaiementsFotmatted"]}  CFA",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .bgMensualiteColor,
                                                fontSize: 13,
                                                fontWeight: FontWeight.bold))),
                                  ),
                                ],
                              ),
                            ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text("Paiement au Maroc :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text(
                            //       "${state.res["Map"]["plafonds"]["plafondPaiementsFotmatted"]}  MAD",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // // ),
                            // Row(
                            //   mainAxisAlignment:
                            //       MainAxisAlignment.spaceBetween,
                            //   children: [
                            //     Text("Paiement et Retrait à l'étranger :",
                            //         style: GoogleFonts.roboto(
                            //             textStyle: TextStyle(
                            //                 color: Colors.white,
                            //                 fontSize: 13,
                            //                 fontWeight: FontWeight.bold))),
                            //     Text(
                            //         "${state.res["Map"]["plafonds"]["plafondPaiementInternationnalFotmatted"]}  CFA",
                            //         style: GoogleFonts.roboto(
                            //             textStyle: TextStyle(
                            //                 color: GlobalParams
                            //                     .themes["$banque_id"]
                            //                     .bgMensualiteColor,
                            //                 fontSize: 13,
                            //                 fontWeight: FontWeight.bold))),
                            //   ],
                            // ),
                            // ListTile(
                            //   visualDensity:
                            //       VisualDensity(horizontal: 0, vertical: -4),
                            //   title: Text(
                            //       "Paiement et Retrait à l'étranger :",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: Colors.white,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            //   trailing: Text(
                            //       "${state.res["Map"]["plafonds"]["plafondPaiementInternationnalFotmatted"]}  MAD",
                            //       style: GoogleFonts.roboto(
                            //           textStyle: TextStyle(
                            //               color: GlobalParams
                            //                   .themes["$banque_id"]
                            //                   .bgMensualiteColor,
                            //               fontSize: 13,
                            //               fontWeight: FontWeight.bold))),
                            // )
                          ],
                        ),
                      );
                    } else {
                      return Container();
                    }
                  },
                )
                // ListTile(
                //   visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                //   title: Text("Plafond Retrait Banque :"),
                //   trailing: Text("${state.currentCarte.plafondRetraitBanque}"),
                // ),
                // ListTile(
                //   visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                //   title: Text("Solde :"),
                //   trailing: Text("${state.currentCarte.solde}"),
                // )
              ],
            ),
          );
      } else {
        return Container();
      }
    });
  }
}
