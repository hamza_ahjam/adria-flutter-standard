import 'dart:io';

import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:open_file/open_file.dart';
//import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class RelevePage extends StatelessWidget {
  // CompteState compteState;
  // TokenState tokenState;
  RelevePage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // getPermission();
  }

  // getPermission() async {
  //   print("get permission");
  //   await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  // }

  showSnackBar(file) {
    final snackBar = SnackBar(
      content: Text("Rib est télécharger avec succes",
          style: GoogleFonts.roboto(color: Colors.white)),
      duration: Duration(seconds: 3),
      backgroundColor: principaleColor5,
      action: SnackBarAction(
          label: "open",
          onPressed: () {
            OpenFile.open(file);
          }),
    );
    return _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  final snackBar = SnackBar(
    content: Text("Relevés bancaires est télécharger avec succes",
        style: GoogleFonts.roboto(color: Colors.white)),
    duration: Duration(seconds: 3),
    backgroundColor: principaleColor5,
  );

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // return Consumer<CompteState>(builder: (_, cs, __) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(80))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            onPressed: () => Navigator.pop(context),
          ),
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).rel_bancaire.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        ),
        body: BlocBuilder<MouvementBloc, MouvementStateBloc>(
            builder: (context, state) {
          if (state.requestState == StateStatus.LOADING) {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
          } else if (state.requestState == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<MouvementBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestState == StateStatus.LOADED) {
            return ListView(children: [
              Container(
                  padding: EdgeInsets.fromLTRB(17, 8, 17, 5),
                  // width: MediaQuery.of(context).size.width * 0.9,
                  height: MediaQuery.of(context).size.height * 0.7,
                  child: SfPdfViewer.file(
                    state.file,
                    canShowPaginationDialog: true,
                    // enableTextSelection: true,
                    enableDoubleTapZooming: true,
                  )),
              Platform.isAndroid
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: RaisedButton(
                            shape: CircleBorder(),
                            onPressed: () {
                              Share.shareFiles(["${state.file.path}"],
                                  text: state.relevePdf.map.filename);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Icon(
                              Icons.share_outlined,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: RaisedButton(
                            elevation: 4,
                            shape: CircleBorder(),
                            // ignore: missing_return
                            onPressed: () async {
                              try {
                                // await Permission.storage.request();
                                String path = await ExternalPath
                                    .getExternalStoragePublicDirectory(
                                        ExternalPath.DIRECTORY_DOWNLOADS);
                                String fullPath =
                                    "$path/${state.relevePdf.map.filename}";

                                print(fullPath);
                                File file = File(fullPath);
                                var raf = file.openSync(mode: FileMode.write);

                                var pdfDocument = PdfDocument.fromBase64String(
                                    state.relevePdf.map.content);
                                var pdfBytes = pdfDocument.save();

                                raf.writeFromSync(pdfBytes);

                                await raf.close();

                                showSnackBar(fullPath);
                              } catch (e) {
                                print("releve download err widget err *** $e");
                                errorAlert(context, e.toString());
                              }
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Icon(
                              Icons.download_outlined,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: RaisedButton(
                            shape: CircleBorder(),
                            onPressed: () {
                              Share.shareFiles(["${state.file.path}"],
                                  text: state.relevePdf.map.filename);
                            },
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            child: Icon(
                              Icons.share_outlined,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    )
            ]);
          } else {
            return Container();
          }
        }));
    // });
  }
}
