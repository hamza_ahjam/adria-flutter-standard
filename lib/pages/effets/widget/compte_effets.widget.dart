import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/effets/effets.bloc.dart';
import 'package:LBA/bloc/effets/effets.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/dot_indicator.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CompteEffets extends StatelessWidget {
  const CompteEffets({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.22,
          child:
              BlocBuilder<CompteBloc, CompteStateBloc>(builder: (context, state) {
            // print("${state.requestStateCheque}");
            // if (state.requestStateCheque == StateStatus.NONE) {
            //   WidgetsBinding.instance.addPostFrameCallback((_) {
            //     context.read<CompteBloc>().add(LoadComptesChequeEvent());
            //   });
            //   return Container();
            // } else
            if (state.requestState == StateStatus.LOADING) {
              return Center(
                  child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
            } else if (state.requestState == StateStatus.ERROR) {
              return ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<CompteBloc>().add(state.currentAction);
                },
              );
            } else if (state.requestState == StateStatus.LOADED) {
              return Container(
                padding: EdgeInsets.only(top: 8),
                child: PageView.builder(
                    // controller: _scrollController,
                    // shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    onPageChanged: (int page) {
                      context.read<EffetsBloc>().add(LoadEffetsEvent());
                    },
                    scrollDirection: Axis.horizontal,
                    itemCount: state.clients.comptes.length,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        width: MediaQuery.of(context).size.width * 0.95,
                        child: ListView(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(
                                  MediaQuery.of(context).size.width * 0.05,
                                  10,
                                  0,
                                  0),
                              child:
                                  Text("${state.clients.comptes[index].intitule}",
                                      style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: GlobalParams
                                                .themes["$banque_id"]
                                                .intituleCmpColor,
                                            fontSize: 21,
                                            fontWeight: FontWeight.bold),
                                      )),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width * 0.05),
                              child: Text(
                                  "${state.clients.comptes[index].identifiantInterne ?? ""}",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  )),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * 0.05),
                              child: ListTile(
                                contentPadding: EdgeInsets.only(right: 0),
                                trailing: RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                        text: "1 962",
                                        // "${state.clients.comptes[index].soldeTempsReel ?? ""} ",
                                        style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .soldeCmpColor,
                                              fontSize: 23,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    WidgetSpan(
                                      child: Transform.translate(
                                        offset: const Offset(1, -6),
                                        child: Text("CFA",
                                            // "${state.clients.comptes[index].devise ?? ""}",
                                            //superscript is usually smaller in size
                                            textScaleFactor: 0.7,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .soldeCmpColor,
                                                  fontSize: 21,
                                                  fontWeight: FontWeight.w600),
                                            )),
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                            ),
                            DotIndicatorWidget(
                              lenght: state.clients.comptes.length,
                              page: index,
                            ),

                            // Divider(
                            //   height: 20,
                            //   color: Colors.grey,
                            // ),
                          ],
                        ),
                      );
                    }),
              );
            } else {
              return Container();
            }
          })),
    );
  }
}
