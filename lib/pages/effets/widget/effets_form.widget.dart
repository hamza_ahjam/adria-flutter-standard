import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/compte.model.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EffetsFormWidget extends StatefulWidget {
  EffetsFormWidget({Key key}) : super(key: key);

  @override
  _EffetsFormWidgetState createState() => _EffetsFormWidgetState();
}

class _EffetsFormWidgetState extends State<EffetsFormWidget> {
  TextEditingController _compteDebiterController = TextEditingController();
  TextEditingController _beneficaireController = TextEditingController();

  TextEditingController _typeEffet = TextEditingController();
  ScrollController _scrollController = ScrollController();
  TextEditingController _montant = TextEditingController();
  TextEditingController _motif = TextEditingController();
  TextEditingController _dateExec = TextEditingController();
  bool isLoaded = false;
  var selectedAccount;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _compteDebiterController.dispose();
    _scrollController.dispose();
    _typeEffet.dispose();
    _montant.dispose();
    _motif.dispose();
    _dateExec.dispose();
    _beneficaireController.dispose();
  }

  @override
  void initState() {
    _dateExec.text = DateFormat('dd/MM/yyyy').format(DateTime.now());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
                AppLocalizations.of(context).nv_demande.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pop(context);
              }),
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.01,
          ),
          child: ListView(
            children: [
              GestureDetector(
                onTap: () {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (!isLoaded)
                      context.read<CompteBloc>().add(LoadComptesVirEvent());
                  });
                  showCompteDialog(context);
                },
                child: Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: _compteDebiterController,
                    enabled: false,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintText: AppLocalizations.of(context).compte_debiter,
                      hintStyle: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 16,
                        color: Colors.black45,
                      )),
                      suffixIcon: Icon(Icons.arrow_drop_down_circle_outlined,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              GestureDetector(
                onTap: () {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (!isLoaded)
                      context.read<CompteBloc>().add(LoadComptesVirEvent());
                  });
                  showEffetsType(context);
                },
                child: Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: _typeEffet,
                    enabled: false,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintText: AppLocalizations.of(context).type,
                      hintStyle: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 16,
                        color: Colors.black45,
                      )),
                      suffixIcon: Icon(Icons.arrow_drop_down_circle_outlined,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              GestureDetector(
                onTap: () {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (!isLoaded)
                      context.read<CompteBloc>().add(LoadComptesVirEvent());
                  });
                  showVendeurDialog(context);
                },
                child: Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextFormField(
                    controller: _beneficaireController,
                    enabled: false,
                    decoration: InputDecoration(
                      errorStyle: TextStyle(
                        color:
                            Theme.of(context).errorColor, // or any other color
                      ),
                      hintText: AppLocalizations.of(context).compte_vendeur,
                      hintStyle: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 16,
                        color: Colors.black45,
                      )),
                      suffixIcon: Icon(Icons.person_outline_rounded,
                          color: GlobalParams.themes["$banque_id"].iconsColor),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _montant,
                  cursorColor: secondaryColor7,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    fillColor: secondaryColor7,
                    focusColor: secondaryColor7,
                    hoverColor: secondaryColor7,
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: AppLocalizations.of(context).montant,
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _motif,
                  cursorColor: secondaryColor7,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    fillColor: secondaryColor7,
                    focusColor: secondaryColor7,
                    hoverColor: secondaryColor7,
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: AppLocalizations.of(context).motif,
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _dateExec,
                  keyboardType: TextInputType.datetime,
                  enabled: false,
                  decoration: InputDecoration(
                    labelStyle: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(color: Colors.black45, fontSize: 16)),
                    hintText: AppLocalizations.of(context).date_exec,
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    suffixIcon: Icon(Icons.calendar_today,
                        color: GlobalParams.themes["$banque_id"].iconsColor),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              Container(
                height: 45,
                child: RaisedButton(
                  onPressed: () {
                    if (_compteDebiterController.text.isNotEmpty &&
                        _typeEffet.text.isNotEmpty &&
                        _montant.text.isNotEmpty &&
                        _motif.text.isNotEmpty) {
                      okAlert(context, AppLocalizations.of(context).demande_sucess_msg);
                    } else {
                      errorAlert(context, AppLocalizations.of(context).error_remplir_champs);
                    }
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  child: Text(AppLocalizations.of(context).suivant,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ))),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showCompteDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(20))),
                  padding: EdgeInsets.only(top: 15),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: Text(AppLocalizations.of(context).compte_debiter.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18))),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  // shrinkWrap: true,
                  children: <Widget>[
                    BlocBuilder<CompteBloc, CompteStateBloc>(
                      builder: (context, state) {
                        if (state.requestStateTransaction == StateStatus.NONE) {
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            context
                                .read<CompteBloc>()
                                .add(LoadComptesVirEvent());
                          });
                          return Container();
                        } else if (state.requestStateTransaction ==
                            StateStatus.LOADING) {
                          return Center(
                              heightFactor: 10,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestStateTransaction ==
                            StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<CompteBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestStateTransaction ==
                            StateStatus.LOADED) {
                          isLoaded = true;
                          List<Compte> filtredListComptes =
                              state.listeCompteTransaction.comptes;

                          if (filtredListComptes.length == 0)
                            return Center(
                              child: Text(
                                AppLocalizations.of(context).aucun_compte_transaction,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes["$banque_id"]
                                            .validationButtonColor)),
                              ),
                            );
                          else
                            return ListView.separated(
                              controller: _scrollController,
                              shrinkWrap: true,
                              itemCount: filtredListComptes.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    _compteDebiterController.text =
                                        "${filtredListComptes[index].intitule}";
                                    // compte["Map"]["compteInstanceList"]
                                    //     [index]["intitule"];
                                    setState(() {
                                      selectedAccount =
                                          filtredListComptes[index];
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        child: Text(
                                            filtredListComptes[index].intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            filtredListComptes[index]
                                                .identifiantInterne,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  showEffetsType(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(20))),
                  padding: EdgeInsets.only(top: 15),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: Text("Type d'effet",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18))),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  // shrinkWrap: true,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _typeEffet.text = "Effet de commerce";
                        });
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.all(8),
                        // height:
                        //     MediaQuery.of(context).size.height * 0.1,
                        child: ListTile(
                          visualDensity:
                              VisualDensity(horizontal: -4, vertical: -4),
                          title: Padding(
                            padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                            child: Text("Effet de commerce",
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: GlobalParams
                                          .themes["$banque_id"].appBarColor),
                                )),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  showVendeurDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(20))),
                  padding: EdgeInsets.only(top: 15),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: Text(AppLocalizations.of(context).benefs,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18))),
                ),
                ListView(
                  shrinkWrap: true,
                  children: [
                    Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          BlocBuilder<BeneficiaireBloc, BeneficiaireStateBloc>(
                            builder: (context, state) {
                              if (state.requestState == StateStatus.NONE) {
                                WidgetsBinding.instance
                                    .addPostFrameCallback((_) {
                                  context
                                      .read<BeneficiaireBloc>()
                                      .add(LoadBeneficiaire());
                                });
                                return Container();
                              } else if (state.requestState ==
                                  StateStatus.LOADING) {
                                return Center(
                                    heightFactor: 10,
                                    child: CircularProgressIndicator(
                                  backgroundColor: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      GlobalParams
                                          .themes["$banque_id"].appBarColor),
                                ));
                              } else if (state.requestState ==
                                  StateStatus.ERROR) {
                                return ErreurTextWidget(
                                  errorMessage: state.errorMessage,
                                  actionEvent: () {
                                    context
                                        .read<BeneficiaireBloc>()
                                        .add(state.currentAction);
                                  },
                                );
                              } else if (state.requestState ==
                                  StateStatus.LOADED) {
                                if (state.beneficiaire.map.beneficiaireList
                                        .length ==
                                    0)
                                  return Center(
                                    child: Text(
                                      AppLocalizations.of(context).aucun_benef,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontSize: 16,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .validationButtonColor)),
                                    ),
                                  );
                                else
                                  return Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Container(
                                      child: ListView.separated(
                                        shrinkWrap: true,
                                        itemCount: state.beneficiaire.map
                                            .beneficiaireList.length,
                                        separatorBuilder: (context, index) =>
                                            Container(
                                          color: Colors.white,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                child: Divider(
                                                  color: Colors.black12,
                                                  height: 15,
                                                  thickness: 1,
                                                ),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                child: Divider(
                                                  color: Colors.black12,
                                                  height: 15,
                                                  thickness: 1,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  _beneficaireController.text =
                                                      "${state.beneficiaire.map.beneficiaireList[index].intitule}";
                                                });
                                                Navigator.pop(context);
                                              },
                                              child: Container(
                                                color: Colors.white,
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      5, 0, 5, 0),
                                                  child: ListTile(
                                                    visualDensity:
                                                        VisualDensity(
                                                            horizontal: -4,
                                                            vertical: -4),
                                                    title: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 5,
                                                              right: 5),
                                                      child: Text(
                                                          state
                                                                  .beneficiaire
                                                                  .map
                                                                  .beneficiaireList[
                                                                      index]
                                                                  .intitule ??
                                                              "",
                                                          style: GoogleFonts
                                                              .roboto(
                                                            textStyle: TextStyle(
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: GlobalParams
                                                                    .themes[
                                                                        "$banque_id"]
                                                                    .appBarColor),
                                                          )),
                                                    ),
                                                    subtitle: Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              5, 0, 0, 0),
                                                      child: Text(
                                                          state
                                                              .beneficiaire
                                                              .map
                                                              .beneficiaireList[
                                                                  index]
                                                              .numeroCompte,
                                                          style: GoogleFonts
                                                              .roboto(
                                                            textStyle: TextStyle(
                                                                fontSize: 15,
                                                                color: GlobalParams
                                                                    .themes[
                                                                        "$banque_id"]
                                                                    .appBarColor),
                                                          )),
                                                    ),
                                                  ),
                                                ),
                                              ));
                                        },
                                      ),
                                    ),
                                  );
                              } else {
                                return Container();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
