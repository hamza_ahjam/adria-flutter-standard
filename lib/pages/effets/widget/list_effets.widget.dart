import 'package:LBA/bloc/effets/effets.bloc.dart';
import 'package:LBA/bloc/effets/effets.event.dart';
import 'package:LBA/bloc/effets/effets.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../main.dart';

class ListEffetWidget extends StatelessWidget {
  const ListEffetWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp.of(context).getLocale().languageCode;

    return BlocBuilder<EffetsBloc, EffetsStateBloc>(
      builder: (context, state) {
        if (state.requestState == StateStatus.NONE) {
          context.read<EffetsBloc>().add(LoadEffetsEvent());

          return Container();
        } else if (state.requestState == StateStatus.LOADING) {
          return Center(
              child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          ));
        } else if (state.requestState == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<EffetsBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestState == StateStatus.LOADED) {
          return ListView(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.05,
                    10,
                    MediaQuery.of(context).size.width * 0.05,
                    10),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30))),
                          child: ListTile(
                            title: Text(
                              AppLocalizations.of(context).num_ref,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                      color: Colors.white)),
                            ),
                            trailing: Text(
                              "00000094949848C ",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text("${AppLocalizations.of(context).date_demande} :",
                              style: GoogleFonts.roboto()),
                          trailing: Text(
                            "${DateFormat('dd MMMM yyyy', languageCode).format(DateTime.now())}",
                            style: GoogleFonts.roboto(
                              textStyle: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text("${AppLocalizations.of(context).num_compte_vendeur} :",
                              style: GoogleFonts.roboto()),
                          trailing: Text("9563218546",
                              style: GoogleFonts.roboto(
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                              )),
                        ),
                        ListTile(
                          title: Text("${AppLocalizations.of(context).type} :", style: GoogleFonts.roboto()),
                          trailing: Text("Lettre de change",
                              style: GoogleFonts.roboto(
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                              )),
                        ),
                        ListTile(
                          title: Text("${AppLocalizations.of(context).statut} :", style: GoogleFonts.roboto()),
                          trailing: Text("En cours",
                              style: GoogleFonts.roboto(
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                              )),
                        ),
                        ListTile(
                          title: Text("${AppLocalizations.of(context).montant} :", style: GoogleFonts.roboto()),
                          trailing: Text("2300 CFA",
                              style: GoogleFonts.roboto(
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                              )),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
