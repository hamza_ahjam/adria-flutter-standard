import 'package:LBA/bloc/effets/effets.bloc.dart';
import 'package:LBA/bloc/effets/effets.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/effets/widget/compte_effets.widget.dart';
import 'package:LBA/pages/effets/widget/effets_form.widget.dart';
import 'package:LBA/pages/effets/widget/list_effets.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class EffetsPage extends StatefulWidget {
  int index;
  EffetsPage({Key key, this.index = 0}) : super(key: key);

  @override
  _EffetsPageState createState() => _EffetsPageState();
}

class _EffetsPageState extends State<EffetsPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).effets,
          context: context,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => EffetsFormWidget())),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          elevation: 4,
          backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          "assets/$banque_id/images/background_image.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.9),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                    ),
                  ),
                  child: CompteEffets()),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.05,
                5,
                MediaQuery.of(context).size.width * 0.05,
                MediaQuery.of(context).size.width * 0.02,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.index = 0;
                      });
                      context.read<EffetsBloc>().add(LoadEffetsEvent());
                    },
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).status_paye,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 0
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 0
                              ? GlobalParams
                                  .themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.index = 1;
                      });
                      context.read<EffetsBloc>().add(LoadEffetsEvent());
                    },
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).status_encaisse,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 1
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 1
                              ? GlobalParams
                                  .themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.index = 2;
                      });
                      context.read<EffetsBloc>().add(LoadEffetsEvent());
                    },
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).status_impaye,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 2
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 2
                              ? GlobalParams
                                  .themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: ListEffetWidget()),
          ],
        ),
      ),
    );
  }
}
