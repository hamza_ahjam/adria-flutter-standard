import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/app_bar.dart';
import 'package:LBA/widgets/header.dart';
import 'package:LBA/widgets/menu/offlineMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class NumeroAgence extends StatelessWidget {
  const NumeroAgence({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        moveToLogin(context);
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: OffLineMenu(0),
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(MediaQuery.of(context).size.height * 0.05),
            child: AppBarWidget(
              context: context,
            )),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/$banque_id/images/login_bg.png"),
                fit: BoxFit.cover),
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(22.0),
            ),
          ),
          child: ListView(
            children: <Widget>[
              Header(),
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Center(
                        child: Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Text(
                          AppLocalizations.of(context).num_urg,
                          style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 20,
                                color:
                                    GlobalParams.themes["$banque_id"].infoColor,
                                fontWeight: FontWeight.bold),
                          )),
                    )),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.35,
                      width: double.infinity,
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            ...(agnecInfos(context) as List).map((nav) {
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(2, 8, 2, 8),
                                    child: GestureDetector(
                                      onTap: () {
                                        // print(nav['numero']);
                                        launch("tel://${nav['numero']}");
                                      },
                                      child: ListTile(
                                          leading: Transform.translate(
                                            offset: Offset(35, 0),
                                            child: Container(
                                              height: double.infinity,
                                              width: 6,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .intituleCmpColor,
                                            ),
                                          ),
                                          title: Text(nav['titre'],
                                              style: GoogleFonts.roboto(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white)),
                                          subtitle: Text(
                                            nav['numero'],
                                            style: GoogleFonts.roboto(
                                                color: Colors.white),
                                          ),
                                          trailing: nav['icon']
                                              ? Container(
                                                  width: 70,
                                                  height: 140,
                                                  decoration: BoxDecoration(
                                                      color: GlobalParams
                                                          .themes["$banque_id"]
                                                          .intituleCmpColor,
                                                      border: Border.all(
                                                        color: GlobalParams
                                                            .themes["$banque_id"]
                                                            .intituleCmpColor,
                                                      ),
                                                      shape: BoxShape.circle
                                                      // borderRadius:
                                                      //     BorderRadius.all(
                                                      //         Radius.circular(50)),
                                                      ),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.center,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets.only(
                                                                bottom: 5),
                                                        child: Text(
                                                          "7j/7",
                                                          style: TextStyle(
                                                              color: Colors.white,
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      Text("24h/24",
                                                          style: TextStyle(
                                                              color: Colors.white,
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))
                                                    ],
                                                  ),
                                                )
                                              : Padding(
                                                  padding: EdgeInsets.all(0),
                                                )),
                                    ),
                                  )
                                ],
                              );
                            })
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // SizedBox(
              //   height: MediaQuery.of(context).size.height * 0.001,
              // ),
              // Footer()
            ],
          ),
        ),
      ),
    );
  }
}
