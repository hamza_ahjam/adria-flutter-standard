import 'package:LBA/models/historique.model.dart';

/*
Cette classe est accecible à partir des diffrentes widgets du projet.
Il permet de sauvgarder le mode de filtrage par date pour l'utiliser quand l'utilisateur navigue vers un autre compte.
 */
class AppData {
  static final AppData _appData = new AppData._internal();

  Mode critere = Mode.MONTH;

  factory AppData() {
    return _appData;
  }
  AppData._internal();
}
final appData = AppData();