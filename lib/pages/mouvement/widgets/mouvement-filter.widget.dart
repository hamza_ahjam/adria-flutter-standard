import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/models/historique.model.dart';
import 'package:LBA/pages/mouvement/sharedData/mouvement.sharedData.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class MouvementFilterWidget extends StatefulWidget {
  MouvementFilterWidget();

  @override
  _MouvementFilterWidgetState createState() => _MouvementFilterWidgetState();
}

class _MouvementFilterWidgetState extends State<MouvementFilterWidget> {
  bool y;
  bool m;
  bool w;
  bool d;
  @override
  void initState() {
    switch (appData.critere) {
      case Mode.YEAR:
        y = true;
        m = false;
        w = false;
        d = false;
        break;
      case Mode.MONTH:
        y = false;
        m = true;
        w = false;
        d = false;
        break;
      case Mode.DAY:
        y = false;
        m = false;
        w = false;
        d = true;
        break;
      case Mode.WEEK:
        y = false;
        m = false;
        w = true;
        d = false;
        break;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Container(
        padding: EdgeInsets.only(
            left: 5, right: 5, top: MediaQuery.of(context).size.height * 0.018),
        height: MediaQuery.of(context).size.height * 0.15,
        alignment: Alignment.topCenter,
        decoration: BoxDecoration(
            color: Colors.grey[50],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              // topRight: Radius.circular(20)
            ),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(91, 102, 112, 0.1),
                offset: const Offset(
                  0.0,
                  0.0,
                ),
                blurRadius: 10,
                spreadRadius: 3.0,
              ), //BoxShadow
              BoxShadow(
                color: Color.fromRGBO(91, 102, 112, 0.1),
                offset: const Offset(0.0, 0.0),
                blurRadius: 10,
                spreadRadius: 3.0,
              ),
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.042,
              width: MediaQuery.of(context).size.width * 0.2,
              child: MaterialButton(
                  elevation: 0,
                  color: y
                      ? GlobalParams.themes["$banque_id"].intituleCmpColor
                      : Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor)),
                  onPressed: () {
                    appData.critere = Mode.YEAR;
                    setState(() {
                      y = true;
                      m = false;
                      w = false;
                      d = false;
                    });
                    // widget.compteState.transformData(Mode.YEAR);
                    context
                        .read<MouvementBloc>()
                        .add(LoadMouvementFiltre(mode: Mode.YEAR));
                  },
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      AppLocalizations.of(context).annee,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: y
                                  ? Colors.white
                                  : GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                    ),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.042,
                width: MediaQuery.of(context).size.width * 0.18,
                child: MaterialButton(
                  elevation: 0,
                  color: m
                      ? GlobalParams.themes["$banque_id"].intituleCmpColor
                      : Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor)),
                  onPressed: () {
                    appData.critere = Mode.MONTH;
                    setState(() {
                      y = false;
                      m = true;
                      w = false;
                      d = false;
                    });
                    // widget.compteState.transformData(Mode.MONTH);
                    context
                        .read<MouvementBloc>()
                        .add(LoadMouvementFiltre(mode: Mode.MONTH));
                  },
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                        AppLocalizations.of(context).mois,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: m
                                  ? Colors.white
                                  : GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.042,
                width: MediaQuery.of(context).size.width * 0.25,
                child: MaterialButton(
                    elevation: 0,
                    color: w
                        ? GlobalParams.themes["$banque_id"].intituleCmpColor
                        : Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)),
                    onPressed: () {
                      appData.critere = Mode.WEEK;
                      setState(() {
                        y = false;
                        m = false;
                        w = true;
                        d = false;
                      });
                      // widget.compteState.transformData(Mode.YEAR);
                      context
                          .read<MouvementBloc>()
                          .add(LoadMouvementFiltre(mode: Mode.WEEK));
                    },
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        AppLocalizations.of(context).semaine,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: w
                                    ? Colors.white
                                    : GlobalParams
                                        .themes["$banque_id"].intituleCmpColor)),
                      ),
                    )),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.042,
              width: MediaQuery.of(context).size.width * 0.18,
              child: MaterialButton(
                  elevation: 0,
                  color: d
                      ? GlobalParams.themes["$banque_id"].intituleCmpColor
                      : Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor)),
                  onPressed: () {
                    appData.critere = Mode.DAY;
                    setState(() {
                      y = false;
                      m = false;
                      w = false;
                      d = true;
                    });
                    // widget.compteState.transformData(Mode.YEAR);
                    context
                        .read<MouvementBloc>()
                        .add(LoadMouvementFiltre(mode: Mode.DAY));
                  },
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      AppLocalizations.of(context).jour,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: d
                                  ? Colors.white
                                  : GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
