import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/bloc/mouvement/mouvement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/show-more.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

class AllMouvementWidget extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();
  final ScrollController _nestedScrollController = ScrollController();

  AllMouvementWidget() {
    // _compteState.loadMouvementData(tokenState);

    // this._scrollController.addListener(() {
    //   if (_scrollController.position.pixels ==
    //       _scrollController.position.maxScrollExtent) {
    //     _compteState.loadNextHistoriquePage(tokenState);
    //   }
    // });

    // _nestedScrollController.addListener(() {
    //   if (_nestedScrollController.position.pixels ==
    //       _nestedScrollController.position.maxScrollExtent) {
    //     print("nested");
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    int page = 1;
    // print(cs.historiqueMouvements.listMouvements.length);
    return Directionality(
      textDirection: TextDirection.ltr,
      child: BlocBuilder<MouvementBloc, MouvementStateBloc>(
          builder: (context, state) {
        if (state.requestState == StateStatus.LOADING) {
          return Center(
              child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          ));
        } else if (state.requestState == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<MouvementBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestState == StateStatus.LOADED) {
          if (state.mouvement.map.orderedlistMouvements.keys.length == 0)
            return Center(
              child: Text(
                AppLocalizations.of(context).aucun_mvt,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 16,
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor)),
              ),
            );
          else {
            /*context
                .read<MouvementBloc>()
                .add(LoadMouvementFiltre(mode: appData.critere));*/
            return ListView(
              children: [
                ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount:
                      state.mouvement.map.orderedlistMouvements.keys.length,
                  itemBuilder: (_, index) {
                    return Container(
                      padding: EdgeInsets.fromLTRB(5, 10, 15, 10),
                      child: ListView(
                        controller: _scrollController,
                        shrinkWrap: true,
                        // physics: ClampingScrollPhysics(),
                        children: [
                          Container(
                            height: 35,
                            // color: GlobalParams
                            //     .themes["$banque_id"].mouvementtitleColor,
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.04,
                                7,
                                0,
                                8),
                            child: Row(
                              children: [
                                Text(
                                  "${state.mouvement.map.orderedlistMouvements.keys.toList()[index]}",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 17,
                                          color: GlobalParams.themes["$banque_id"]
                                              .intituleCmpColor)),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 8, 8),
                                  child: Icon(
                                    Icons.arrow_drop_down_circle,
                                    color: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Wrap(children: <Widget>[
                            ListView.separated(
                              controller: _nestedScrollController,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: state
                                  .mouvement
                                  .map
                                  .orderedlistMouvements[state
                                      .mouvement.map.orderedlistMouvements.keys
                                      .toList()[index]]
                                  .length,
                              separatorBuilder: (context, index) => ListTile(
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -4),
                                leading: Container(
                                  width: MediaQuery.of(context).size.width * 0.25,
                                  child: Divider(
                                    color: Colors.black26,
                                    height: 15,
                                  ),
                                ),
                                trailing: Container(
                                  width: MediaQuery.of(context).size.width * 0.25,
                                  child: Divider(
                                    color: Colors.black26,
                                    height: 15,
                                  ),
                                ),
                              ),
                              itemBuilder: (_, itemIndex) {
                                return ListTile(
                                  visualDensity:
                                      VisualDensity(horizontal: -4, vertical: -4),
                                  leading: SvgPicture.asset(
                                      "assets/$banque_id/images/Picto_virement.svg",
                                      fit: BoxFit.fill,
                                      width: MediaQuery.of(context).size.width *
                                          0.033,
                                      height: MediaQuery.of(context).size.height *
                                          0.033,
                                      color: GlobalParams
                                          .themes["$banque_id"].colorTab),
                                  title: Padding(
                                    padding:
                                        const EdgeInsets.only(top: 5, bottom: 4),
                                    child: Text(
                                      "${intl.DateFormat.yMMMMd('fr').format(intl.DateFormat("dd-MM-yyyy").parse(state.mouvement.map.orderedlistMouvements[state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex].resultDateOperation))}",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: principaleColor5,
                                              fontSize: 14)),
                                    ),
                                  ),
                                  subtitle: Text(
                                    "${state.mouvement.map.orderedlistMouvements[state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex].libelle}",
                                    maxLines: 2,
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: principaleColor5,
                                            fontSize: 12)),
                                  ),
                                  trailing: RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text: state
                                                      .mouvement
                                                      .map
                                                      .orderedlistMouvements[state
                                                              .mouvement
                                                              .map
                                                              .orderedlistMouvements
                                                              .keys
                                                              .toList()[index]]
                                                          [itemIndex]
                                                      .sens ==
                                                  "C"
                                              ? " + ${state.mouvement.map.orderedlistMouvements[state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex].resultCredit}"
                                              : " - ${state.mouvement.map.orderedlistMouvements[state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex].resultDebit}",
                                          style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  color: state
                                                              .mouvement
                                                              .map
                                                              .orderedlistMouvements[
                                                                  state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex]
                                                              .sens !=
                                                          "C"
                                                      ? Colors.red
                                                      : Colors.green[700],
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: KprimaryFont,
                                                  fontSize: 16))),
                                      WidgetSpan(
                                        child: Transform.translate(
                                          offset: const Offset(2, -4),
                                          child: Text(
                                              " ${state.mouvement.map.orderedlistMouvements[state.mouvement.map.orderedlistMouvements.keys.toList()[index]][itemIndex].devise}",
                                              //superscript is usually smaller in size
                                              textScaleFactor: 0.7,
                                              style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                                    color: state
                                                                .mouvement
                                                                .map
                                                                .orderedlistMouvements[
                                                                    state
                                                                        .mouvement
                                                                        .map
                                                                        .orderedlistMouvements
                                                                        .keys
                                                                        .toList()[index]]
                                                                    [itemIndex]
                                                                .sens !=
                                                            "C"
                                                        ? Colors.red
                                                        : Colors.green[700],
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16),
                                              )),
                                        ),
                                      )
                                    ]),
                                  ),
                                );
                              },
                            )
                          ]
/*                          height: MediaQuery.of(context).size.height *
                                (0.12 *
                                    state
                                        .mouvement
                                        .map
                                        .orderedlistMouvements[state.mouvement.map
                                            .orderedlistMouvements.keys
                                            .toList()[index]]
                                        .length),*/
                              // color: GlobalParams
                              //     .themes["$banque_id"].mouvementBodyColor,,
                              )
                        ],
                      ),
                    );
                  },
                ),
                ShowMore(
                  showMoreAction: () {
                    context
                        .read<MouvementBloc>()
                        .add(LoadNextMouvementPage(page: page));
                    // _compteState.loadNextHistoriquePage(tokenState, page);
                    ++page;
                  },
                ),
              ],
            );
          }
        } else {
          return Container();
        }
      }),
    );
  }
}
