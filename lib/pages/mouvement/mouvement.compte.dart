
import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/mouvement/widgets/allMouvement.widget.dart';
import 'package:LBA/pages/mouvement/widgets/compteMouvement.widget.dart';
import 'package:LBA/pages/mouvement/widgets/mouvement-filter.widget.dart';
import 'package:LBA/pages/releve_bancaire/releve.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:expandable_bottom_bar/expandable_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MouvementComptePage extends StatelessWidget {
  MouvementComptePage();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  DateTime dateDebut = DateTime.now();
  DateTime datefin = DateTime.now();
  bool isDateDebutSelected = false;
  bool isDateFinSelected = false;

  DateFormat _dateFormat = DateFormat.yMd('fr_FR');

  // ScrollController _scrollController = ScrollController();
  TextEditingController _mycontrollerDebut = TextEditingController();
  TextEditingController _mycontrollerFin = TextEditingController();
  TextEditingController _libelleController = TextEditingController();
  var dif;
  bool selected = false;

  // @override
  // void dispose() {
  //   _mycontrollerDebut.dispose();
  //   _mycontrollerFin.dispose();
  //   _libelleController.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultBottomBarController(
      child: Scaffold(
        key: _scaffoldKey,
        extendBody: true,
        resizeToAvoidBottomInset: false,
        // bottomNavigationBar: OnlineMenu(),
        // drawer: OnlineDrawer(context: context),
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).mes_mouvements,
          context: context,
        ),
        extendBodyBehindAppBar: true,
        body: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(top: 0),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              "assets/$banque_id/images/background_image.png"),
                          fit: BoxFit.cover,
                          alignment: Alignment(0, 0.9)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(22.0),
                      ),
                    ),
                    child: CompteMouvementWidget(),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(top: 8, bottom: 8),
                  //   child: NomCompteWidget(),
                  // ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //   children: [
                  //     GestureDetector(
                  //       onTap: () {
                  //         compteState.transformData(Mode.YEAR);
                  //       },
                  //       child: InputChip(
                  //         // avatar: Icon(Icons.remove),
                  //         label: Text('Year'),
                  //         onSelected: (bool value) {},
                  //         // onPressed: () {
                  //         //   compteState.transformData(Mode.YEAR);
                  //         // },
                  //       ),
                  //     ),
                  //     GestureDetector(
                  //       onTap: () {
                  //         compteState.transformData(Mode.MONTH);
                  //       },
                  //       child: InputChip(
                  //         // avatar: Icon(Icons.remove),
                  //         label: Text('Month'),
                  //         onSelected: (bool value) {},
                  //         // onPressed: () {
                  //         //   compteState.transformData(Mode.MONTH);
                  //         // },
                  //       ),
                  //     ),
                  //     InputChip(
                  //       // avatar: Icon(Icons.remove),
                  //       label: Text('Day'),
                  //       onSelected: (bool value) {},
                  //     ),
                  //   ],
                  // ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.55,
                    child: AllMouvementWidget(),
                  ),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            MouvementFilterWidget(),
            Container(
              height: MediaQuery.of(context).size.height * 0.07,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    // topRight: Radius.circular(20)MediaQuery.of(context).size.height * 0.01,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(91, 102, 112, 0.1),
                      offset: const Offset(0, 0),
                      blurRadius: 5,
                      spreadRadius: 3,
                    ),
                    BoxShadow(
                      color: Color.fromRGBO(91, 102, 112, 0.1),
                      offset: const Offset(0, 0),
                      blurRadius: 5,
                      spreadRadius: 3,
                    ), //BoxShadow
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  // GestureDetector(
                  //   onTap: () {
                  //     Navigator.pop(context);

                  //     Navigator.push(context,
                  //         MaterialPageRoute(builder: (context) => FacturePage()));
                  //   },
                  //   child: Image.asset(
                  //     "assets/$banque_id/images/bt_Factures.png",
                  //     width: 50,
                  //     height: 50,
                  //   ),
                  // ),
                  GestureDetector(
                    onTap: () {
                      showDateDialog(context);
                    },
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/recherche_icon.svg",
                      width: MediaQuery.of(context).size.width * 0.03,
                      height: MediaQuery.of(context).size.height * 0.03,
                      color: GlobalParams
                          .themes["$banque_id"].cancelButtonTextColor,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      context.read<MouvementBloc>().add(LoadMouvementPdfEvent(
                            dateDebut:
                                DateFormat('dd-MM-yyyy').format(dateDebut),
                            dateFin: DateFormat('dd-MM-yyyy').format(datefin),
                          ));

                      // Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RelevePage()));
                    },
                    child: SvgPicture.asset(
                      "assets/$banque_id/images/pdf_icon.svg",
                      width: MediaQuery.of(context).size.width * 0.03,
                      height: MediaQuery.of(context).size.height * 0.03,
                      color: GlobalParams
                          .themes["$banque_id"].cancelButtonTextColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  showDateDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            titlePadding: EdgeInsets.zero,
            title: Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Text(
                AppLocalizations.of(context).trouver_mouvement.toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
                textAlign: TextAlign.center,
              ),
            ),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    height: 45,
                    child: TextFormField(
                      textAlignVertical: TextAlignVertical.bottom,
                      controller: _libelleController,
                      decoration: InputDecoration(
                        errorStyle: TextStyle(
                          color: Theme.of(context)
                              .errorColor, // or any other color
                        ),
                        hintText: 'Libelle',
                        hintStyle: GoogleFonts.roboto(
                            fontSize: 17,
                            color: Color.fromRGBO(152, 152, 152, 1)),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  dialogShowInput1(AppLocalizations.of(context).date_deb, dateDebut, context),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  dialogShowInput2(AppLocalizations.of(context).date_fin, datefin, context),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width * 0.042,
                    left: MediaQuery.of(context).size.width * 0.042,
                    bottom: MediaQuery.of(context).size.height * 0.025),
                child: RaisedButton(
                  padding: EdgeInsets.only(left: 25, right: 25),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7),
                  ),
                  onPressed: () {
                    // setState(() {
                    dif = datefin.difference(dateDebut).inDays;
                    // });
                    // print(dif);

                    if (dif > 90) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        errorAlert(
                            context, "vous ne pouvez pas dépasser 3 mois ");
                      });
                    } else {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        context.read<MouvementBloc>().add(LoadRecherchMouvement(
                            dateDebut:
                                DateFormat('dd-MM-yyyy').format(dateDebut),
                            dateFin: DateFormat('dd-MM-yyyy').format(datefin),
                            libelle: _libelleController.text));
                      });
                    }
                    Navigator.pop(context);
                  },
                  child: Text(
                    AppLocalizations.of(context).trouver,
                    style:
                        GoogleFonts.roboto(fontSize: 17, color: Colors.white),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  // gradient: LinearGradient(
                  //   colors: <Color>[
                  //     GlobalParams.themes["$banque_id"].appBarColor,
                  //     GlobalParams.themes["$banque_id"].appBarColor
                  //   ],
                  // ),
                ),
              ),
            ],
          );
        });
  }

  dialogShowInput1(String hint, dateselected, context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GestureDetector(
        onTap: () async {
          dateDebut = await selectDate(context, dateselected);
          // setState(() {
          // dateDebut = res;
          // });
        },
        child: Container(
          // width: 250,
          height: 45,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black26),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextFormField(
            textAlignVertical: TextAlignVertical.bottom,
            controller: _mycontrollerDebut,
            // onChanged: (value) {
            //   _mycontrollerDebut.text = value;
            //   print(value);
            // },
            enabled: false,
            decoration: InputDecoration(
              // labelText: hint.toString(),
              hintText: hint.toString(),
              hintStyle: GoogleFonts.roboto(
                  fontSize: 17, color: Color.fromRGBO(152, 152, 152, 1)),
              suffixIcon: Transform.scale(
                scale: 0.7,
                child: SvgPicture.asset(
                  "assets/$banque_id/images/picto_calendrier.svg",
                  width: MediaQuery.of(context).size.width * 0.01,
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
              ),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(7))),
            ),
          ),
        ),
      ),
    );
  }

  dialogShowInput2(String hint, dateselected, context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: GestureDetector(
        onTap: () async {
          datefin = await selectDate2(context, dateselected);
          // setState(() {
          //   datefin = res;
          // });
        },
        child: Container(
          // width: 200,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black26),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          height: 45,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.bottom,
            controller: _mycontrollerFin,
            enabled: false,
            decoration: InputDecoration(
              hintText: hint.toString(),
              // labelText: hint.toString(),
              hintStyle: GoogleFonts.roboto(
                  fontSize: 17, color: Color.fromRGBO(152, 152, 152, 1)),
              suffixIcon: Transform.scale(
                scale: 0.7,
                child: SvgPicture.asset(
                  "assets/$banque_id/images/picto_calendrier.svg",
                  width: MediaQuery.of(context).size.width * 0.01,
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
              ),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(7))),
            ),
          ),
        ),
      ),
    );
  }

  selectDate(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateDebutSelected
          ? dateDebut
          : (datefin.difference(DateTime.now()).inDays > 0
              ? DateTime.now()
              : datefin),
      firstDate: DateTime(1920),
      lastDate: isDateFinSelected ? datefin : DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      // setState(() {
      updatedDate = picked;
      _mycontrollerDebut.text = _dateFormat.format(updatedDate);
      // });
      isDateDebutSelected = true;
    }
    return updatedDate;
  }

  selectDate2(BuildContext context, updatedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: isDateFinSelected
          ? datefin
          : (dateDebut.difference(DateTime.now()).inDays < 0
              ? DateTime.now()
              : dateDebut),
      firstDate: isDateDebutSelected ? dateDebut : DateTime(1920),
      lastDate: DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      // setState(() {
      updatedDate = picked;
      _mycontrollerFin.text = _dateFormat.format(updatedDate);
      isDateFinSelected = true;
      // });

    }
    return updatedDate;
  }
}
