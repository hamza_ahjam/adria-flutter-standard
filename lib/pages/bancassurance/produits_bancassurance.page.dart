import 'package:LBA/bloc/bancassurance/banceassurance.bloc.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.event.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/bancassurance/bankassurance_list.page.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class ProduitsBanceassurancePage extends StatelessWidget {
  ProduitsBanceassurancePage({Key key}) : super(key: key);

  bool isLoaded = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "Bancassurance".toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()));
            }),
      ),
      body: BlocBuilder<BanceassuranceBloc, BanceassuranceStateBloc>(
        builder: (context, state) {
          if (state.requestState == StateStatus.NONE) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              if (!isLoaded)
                context
                    .read<BanceassuranceBloc>()
                    .add(LoadBanceassuranceEvent());
            });
            return Container();
          } else if (state.requestState == StateStatus.LOADING) {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
          } else if (state.requestState == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<BanceassuranceBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestState == StateStatus.LOADED) {
            isLoaded = true;
            if (state.bancassuranceList.map.bankAssuranceList.length == 0)
              return Center(
                child: Text(
                  AppLocalizations.of(context).aucun_prod_ba,
                  style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(fontSize: 16, color: principaleColor5)),
                ),
              );
            else
              return Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.1),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount:
                      state.bancassuranceList.map.bankAssuranceList.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          context.read<BanceassuranceBloc>().add(
                              LoadListAssuranceEvent(
                                  code: state.bancassuranceList.map
                                      .bankAssuranceList[index].code));
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BankassuranceListPage(
                                        name: state.bancassuranceList.map
                                            .bankAssuranceList[index].libelle,
                                        type: state.bancassuranceList.map
                                            .bankAssuranceList[index].code,
                                      )));
                        },
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          shadowColor:
                              GlobalParams.themes["$banque_id"].appBarColor,
                          child: ListTile(
                            title: Text(
                                state.bancassuranceList.map
                                    .bankAssuranceList[index].libelle,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: GlobalParams.themes["$banque_id"]
                                            .appBarColor))),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              size: 30,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
