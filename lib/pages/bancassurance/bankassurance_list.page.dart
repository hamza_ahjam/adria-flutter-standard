import 'package:LBA/bloc/bancassurance/banceassurance.bloc.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/bancassurance/produit_bankassurance.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class BankassuranceListPage extends StatelessWidget {
  BankassuranceListPage({this.name, this.type}) : super();
  String name;
  String type;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "$name".toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: BlocBuilder<BanceassuranceBloc, BanceassuranceStateBloc>(
        builder: (context, state) {
          if (state.requestStateListAssurance == StateStatus.LOADING) {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor:
                  GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
          } else if (state.requestStateListAssurance == StateStatus.ERROR) {
            return ErreurTextWidget(
              errorMessage: state.errorMessage,
              actionEvent: () {
                context.read<BanceassuranceBloc>().add(state.currentAction);
              },
            );
          } else if (state.requestStateListAssurance == StateStatus.LOADED) {
            if (state.listAssurance.map.listAssurances.length == 0)
              return Center(
                child: Text(
                    AppLocalizations.of(context).aucun_ba,
                  style: GoogleFonts.roboto(
                      textStyle:
                          TextStyle(fontSize: 16, color: principaleColor5)),
                ),
              );
            else
              return Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.1),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: state.listAssurance.map.listAssurances.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          print('Montant cot ${state.listAssurance.map
                              .listAssurances[index].periodiciteCotisation}');
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ProduitBankassuranceWidget(
                                        name: name,
                                        type: type,
                                        assurance: state.listAssurance.map
                                            .listAssurances[index],
                                        isSituation: state.listAssurance.map
                                            .listAssurances[index].dateSituation != null,
                                        policeNumber: state.listAssurance.map
                                            .listAssurances[index].numeroPolice,
                                        isCotisation: state.listAssurance.map
                                            .listAssurances[index].periodiciteCotisation != null,
                                      )));
                        },
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          shadowColor:
                              GlobalParams.themes["$banque_id"].appBarColor,
                          child: ListTile(
                            title: Text(
                                state.listAssurance.map.listAssurances[index]
                                            .numeroPolice ==
                                        null
                                    ? state.listAssurance.map
                                        .listAssurances[index].nomProduit
                                    : "Dossier N° ${state.listAssurance.map.listAssurances[index].numeroPolice}",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: GlobalParams.themes["$banque_id"]
                                            .appBarColor))),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              size: 30,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
