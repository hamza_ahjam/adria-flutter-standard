import 'package:LBA/bloc/bancassurance/banceassurance.bloc.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.event.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/list_assurance.model.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ProduitBankassuranceWidget extends StatefulWidget {
  ProduitBankassuranceWidget(
      {this.name,
      this.assurance,
      this.isSituation = false,
      this.policeNumber,
      this.isCotisation = false,
      this.type
      })
      : super();
  String name;
  String policeNumber;
  String type;
  bool isSituation;
  bool isCotisation;
  ListAssuranceElement assurance;

  @override
  State<ProduitBankassuranceWidget> createState() =>
      _ProduitBankassuranceWidgetState();
}

class _ProduitBankassuranceWidgetState
    extends State<ProduitBankassuranceWidget> {
  PageController _pageController = PageController();

  int page = 0;
  bool isLoadedSit = false;
  bool isLoadedCot = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "${widget.name}".toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: PageView(
          onPageChanged: (page) {
            setState(() {
              this.page = page;
            });
          },
          controller: _pageController,
          scrollDirection: Axis.horizontal,
          children: [
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              shadowColor: GlobalParams.themes["$banque_id"].appBarColor,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    child: ListTile(
                      leading: Container(
                        height: 20,
                        width: 20,
                      ),
                      title: Text(
                        "Contrat".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.white)),
                      ),
                      trailing: IconButton(
                        onPressed: () {
                          if (widget.isSituation && !isLoadedSit)
                            context.read<BanceassuranceBloc>().add(LoadSituationAssuranceEvent(policeNumber: widget.policeNumber, productNumber: widget.type == "cam_retraite" ? "CAMRET" :"CAMEDU"));
                          _pageController.animateToPage(1,
                              duration: Duration(seconds: 1),
                              curve: Curves.ease);
                        },
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          size: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("Date Souscription",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                        ))),
                    trailing: Text(widget.assurance.dateSouscription ?? "-",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ))),
                  ),
                  ListTile(
                    title: Text("Date d'effet",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                        ))),
                    trailing: Text(widget.assurance.dateEffet ?? "-",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ))),
                  ),
                  ListTile(
                    title: Text("Date d'échéance",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                        ))),
                    trailing: Text(widget.assurance.dateEcheance ?? "-",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ))),
                  ),
                  ListTile(
                    title: Text("Cotisation Annuelle",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                        ))),
                    trailing:
                        Text(widget.assurance.cotisationTotalAnnuelle ?? "-",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ))),
                  ),
                  ListTile(
                    title: Text("Valeur du batiment",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 16,
                        ))),
                    trailing: Text(widget.assurance.valeurBatiment ?? "-",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ))),
                  ),
                ],
              ),
            ),
            if (widget.isSituation)
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                shadowColor: GlobalParams.themes["$banque_id"].appBarColor,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                      child: ListTile(
                        leading: IconButton(
                          onPressed: () {
                            _pageController.animateToPage(0,
                                duration: Duration(seconds: 1),
                                curve: Curves.ease);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                        title: Text(
                          "Situation".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white)),
                        ),
                        trailing: IconButton(
                          onPressed: () {
                            _pageController.animateToPage(2,
                                duration: Duration(seconds: 1),
                                curve: Curves.ease);
                          },
                          icon: Icon(
                            Icons.arrow_forward_ios,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    BlocBuilder<BanceassuranceBloc, BanceassuranceStateBloc>(
                        builder: (context, state) {
                      if (state.requestStateSituation == StateStatus.LOADING) {
                        return Center(
                            heightFactor: 10,
                            child: CircularProgressIndicator(
                              backgroundColor: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  GlobalParams
                                      .themes["$banque_id"].appBarColor),
                            ));
                      } else if (state.requestStateSituation ==
                          StateStatus.ERROR) {
                        return ErreurTextWidget(
                          errorMessage: state.errorMessage,
                          actionEvent: () {
                            context
                                .read<BanceassuranceBloc>()
                                .add(state.currentAction);
                          },
                        );
                      } else if (state.requestStateSituation ==
                          StateStatus.LOADED) {
                        isLoadedSit = true;
                        return Column(
                          children: [
                            ListTile(
                              title: Text("Situation épargne N°: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .numeroPolice ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Cumul de vos cotisations: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .cumulCotisations ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Valorisation de votre épargne: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .epargneRevalorise ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text(
                                  "Plafond des prestations possibles sur votre épargne: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Rachat total: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .valeurRachatBrute ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Rachat partiel: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .plafondRachatPartiel ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Avance: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .plafondAvanceAccorder ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Montant dû: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                            ),
                            ListTile(
                              title: Text("Avance dû: ",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontSize: 16,
                                  ))),
                              trailing: Text(
                                  state.assuranceSituation.map.situation
                                          .avanceAccordeeMajoreeInterets ??
                                      "-",
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ))),
                            ),
                          ],
                        );
                      } else {
                        return Container();
                      }
                    }),
                  ],
                ),
              ),
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              shadowColor: GlobalParams.themes["$banque_id"].appBarColor,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    child: ListTile(
                      title: Text(
                        "bénéficiaire".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.white)),
                      ),
                      leading: IconButton(
                        onPressed: () {
                          _pageController.animateToPage(widget.isSituation ? 1 : 0,
                              duration: Duration(seconds: 1),
                              curve: Curves.ease);
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          size: 30,
                          color: Colors.white,
                        ),
                      ),
                      trailing: IconButton(
                        onPressed: () {
                          print('${widget.assurance.identifiant} ${widget.type}');
                          if (widget.isCotisation && !isLoadedCot)
                            context.read<BanceassuranceBloc>().add(LoadCotisationsAssuranceEvent(idContrat: widget.assurance.identifiant, typeAssurance: widget.type));
                          _pageController.animateToPage(3,
                              duration: Duration(seconds: 1),
                              curve: Curves.ease);
                        },
                        icon: widget.isCotisation
                            ? Icon(
                                Icons.arrow_forward_ios,
                                size: 30,
                                color: Colors.white,
                              )
                            : Container(
                                height: 20,
                                width: 20,
                              ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.02,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Text("Assuré",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 18,
                        ))),
                  ),
                  Text(widget.assurance.assure ?? "-",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ))),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Text("${widget.assurance.benificiaire1 != null ? "Les bénéficiaires en cas de décès" : ''}",
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                          fontSize: 18,
                        ))),
                  ),
                  Text(
                      "${widget.assurance.benificiaire1 ?? ""} \n ${widget.assurance.benificiaire2 ?? ""} \n ${widget.assurance.benificiaire3 ?? ""} \n ${widget.assurance.benificiaire4 ?? ""}",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ))),
                ],
              ),
            ),
            if (widget.isCotisation)
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                shadowColor: GlobalParams.themes["$banque_id"].appBarColor,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                      child: ListTile(
                        leading: IconButton(
                          onPressed: () {
                            _pageController.animateToPage(2,
                                duration: Duration(seconds: 1),
                                curve: Curves.ease);
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                        title: Text(
                          "Cotisations".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white)),
                        ),
                        trailing: Container(
                          height: 20,
                          width: 20,
                        ),
                      ),
                    ),
                    Expanded(
                      child: BlocBuilder<BanceassuranceBloc, BanceassuranceStateBloc>(
                          builder: (context, state) {
                            if (state.requestStateCotisations == StateStatus.LOADING) {
                              return Center(
                                  heightFactor: 10,
                                  child: CircularProgressIndicator(
                                    backgroundColor: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        GlobalParams
                                            .themes["$banque_id"].appBarColor),
                                  ));
                            } else if (state.requestStateCotisations ==
                                StateStatus.ERROR) {
                              return ErreurTextWidget(
                                errorMessage: state.errorMessage,
                                actionEvent: () {
                                  context
                                      .read<BanceassuranceBloc>()
                                      .add(state.currentAction);
                                },
                              );
                            } else if (state.requestStateCotisations ==
                                StateStatus.LOADED) {
                              isLoadedCot = true;
                              return ListView.separated(
                                shrinkWrap: true,
                                physics: AlwaysScrollableScrollPhysics(),
                                itemCount: state
                                    .cotisations
                                    .map
                                    .listCotisations
                                    .length,
                                separatorBuilder: (context, index) => ListTile(
                                  visualDensity:
                                  VisualDensity(horizontal: -4, vertical: -4),
                                  leading: Container(
                                    width: MediaQuery.of(context).size.width * 0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  trailing: Container(
                                    width: MediaQuery.of(context).size.width * 0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                ),
                                itemBuilder: (_, index) {
                                  return ListTile(
                                    visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -4),
                                    leading: SvgPicture.asset(
                                        "assets/$banque_id/images/Picto_virement.svg",
                                        fit: BoxFit.fill,
                                        width: MediaQuery.of(context).size.width *
                                            0.033,
                                        height: MediaQuery.of(context).size.height *
                                            0.033,
                                        color: GlobalParams
                                            .themes["$banque_id"].colorTab),
                                    title: Padding(
                                      padding:
                                      const EdgeInsets.only(top: 5, bottom: 4),
                                      child: Text(
                                        "${DateFormat.yMMMMd('fr').format(DateFormat("dd-MM-yyyy").parse(state.cotisations.map.listCotisations[index].dateOperation))}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: GlobalParams.themes["$banque_id"].appBarColor,
                                                fontSize: 14)),
                                      ),
                                    ),
                                    subtitle: Text(
                                      "Prélèvement- Education",
                                      maxLines: 2,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: GlobalParams.themes["$banque_id"].appBarColor,
                                              fontSize: 12)),
                                    ),
                                    trailing: RichText(
                                      text: TextSpan(children: [
                                        TextSpan(
                                            text: state.cotisations.map.listCotisations[index].montant,
                                            style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                                    color: GlobalParams.themes["$banque_id"].appBarColor,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: KprimaryFont,
                                                    fontSize: 16))),
                                        WidgetSpan(
                                          child: Transform.translate(
                                            offset: const Offset(2, -4),
                                            child: Text(
                                                "XOF",
                                                textScaleFactor: 0.7,
                                                style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16),
                                                )),
                                          ),
                                        )
                                      ]),
                                    ),
                                  );
                                },
                              );
                            } else {
                              return Container();
                            }
                          }),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
