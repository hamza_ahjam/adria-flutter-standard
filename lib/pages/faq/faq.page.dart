import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class FAQPage extends StatefulWidget {
  const FAQPage({Key key}) : super(key: key);

  @override
  _FAQPageState createState() => _FAQPageState();
}

class _FAQPageState extends State<FAQPage> {
  bool qst1 = false;
  bool qst2 = false;
  bool qst3 = false;
  bool qst4 = false;
  @override
  Widget build(BuildContext context) {
    // print(qst1);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(80))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            AppLocalizations.of(context).infos.toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => DashboardComptePage()))),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            // height: MediaQuery.of(context).size.height *0.9,
            color: Colors.white,
            child: ListView(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      qst1 = !qst1;
                    });
                  },
                  child: ListTile(
                    title: Text(AppLocalizations.of(context).aide_comm,
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                            fontSize: 14,
                            color: !qst1
                                ? Colors.black
                                : GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                          ),
                        )),
                    trailing: Icon(
                        !qst1 ? Icons.arrow_drop_down : Icons.arrow_drop_up),
                  ),
                ),
                qst1
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(17, 10, 17, 10),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                  "- Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 8),
                                child: Text(
                                    "- Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat ",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                  "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 20),
                                child: Text(
                                    "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                "Pour plus d’information, rendez-vous dans nos agences $banque_id ou contactez-nous 24h/24, 7j/7.",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20, right: 50, left: 50),
                                child: RaisedButton(
                                  onPressed: () {
                                    launch("tel://+221338496060");
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].otherButtonColor,
                                  child: Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: ListTile(
                                      title: Text(
                                        "+221 33 849 60 60",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                      leading: Image.asset(
                                          "assets/$banque_id/images/phone.png"),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                Divider(
                  color: Colors.grey[850],
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      // print(qst1);
                      qst2 = !qst2;
                    });
                  },
                  child: ListTile(
                    title: Text(AppLocalizations.of(context).securite_mp,
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                            fontSize: 14,
                            color: !qst2
                                ? Colors.black
                                : GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                          ),
                        )),
                    trailing: Icon(
                        !qst2 ? Icons.arrow_drop_down : Icons.arrow_drop_up),
                  ),
                ),
                qst2
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(17, 10, 17, 10),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                  "- Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 8),
                                child: Text(
                                    "- Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat ",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                  "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 20),
                                child: Text(
                                    "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                "Pour plus d’information, rendez-vous dans nos agences $banque_id ou contactez-nous 24h/24, 7j/7.",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20, right: 50, left: 50),
                                child: RaisedButton(
                                  onPressed: () {
                                    launch("tel://+221338496060");
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].otherButtonColor,
                                  child: Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: ListTile(
                                      title: Text(
                                        "+221 33 849 60 60",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                      leading: Image.asset(
                                          "assets/$banque_id/images/phone.png"),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                Divider(
                  color: Colors.grey[850],
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      // print(qst1);
                      qst3 = !qst3;
                    });
                  },
                  child: ListTile(
                    title: Text(AppLocalizations.of(context).utilisation,
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              fontSize: 14,
                              color: !qst3
                                  ? Colors.black
                                  : GlobalParams
                                      .themes["$banque_id"].intituleCmpColor),
                        )),
                    trailing: Icon(
                        !qst3 ? Icons.arrow_drop_down : Icons.arrow_drop_up),
                  ),
                ),
                qst3
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(17, 10, 17, 10),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                  "- Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 8),
                                child: Text(
                                    "- Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat ",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                  "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 20),
                                child: Text(
                                    "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                "Pour plus d’information, rendez-vous dans nos agences $banque_id ou contactez-nous 24h/24, 7j/7.",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20, right: 50, left: 50),
                                child: RaisedButton(
                                  onPressed: () {
                                    launch("tel://+221338496060");
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].otherButtonColor,
                                  child: Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: ListTile(
                                      title: Text(
                                        "+221 33 849 60 60",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                      leading: Image.asset(
                                          "assets/$banque_id/images/phone.png"),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                Divider(
                  color: Colors.grey[850],
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      // print(qst1);
                      qst4 = !qst4;
                    });
                  },
                  child: ListTile(
                    title: Text(AppLocalizations.of(context).reclamation,
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              fontSize: 14,
                              color: !qst4
                                  ? Colors.black
                                  : GlobalParams
                                      .themes["$banque_id"].intituleCmpColor),
                        )),
                    trailing: Icon(
                        !qst4 ? Icons.arrow_drop_down : Icons.arrow_drop_up),
                  ),
                ),
                qst4
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(17, 10, 17, 10),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                  "- Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 8),
                                child: Text(
                                    "- Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat ",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                  "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                  style: GoogleFonts.roboto(
                                    textStyle: TextStyle(fontSize: 14),
                                  )),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 20),
                                child: Text(
                                    "- cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                    style: GoogleFonts.roboto(
                                      textStyle: TextStyle(fontSize: 14),
                                    )),
                              ),
                              Text(
                                "Pour plus d’information, rendez-vous dans nos agences $banque_id ou contactez-nous 24h/24, 7j/7.",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20, right: 50, left: 50),
                                child: RaisedButton(
                                  onPressed: () {
                                    launch("tel://+221338496060");
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].otherButtonColor,
                                  child: Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: ListTile(
                                      title: Text(
                                        "+221 33 849 60 60",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                      leading: Image.asset(
                                          "assets/$banque_id/images/phone.png"),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                Divider(
                  color: Colors.grey[850],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
