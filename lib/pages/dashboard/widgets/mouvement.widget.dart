import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MouvementWidget extends StatelessWidget {
  // ScrollController _scrollController = new ScrollController();

  MouvementWidget();

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: BlocBuilder<MouvementBloc, MouvementStateBloc>(
          builder: (context, state) {
        if (state.requestState == StateStatus.LOADING) {
          return Center(
              heightFactor: 5,
              child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
        } else if (state.requestState == StateStatus.ERROR) {
          return ErreurTextWidget(
            errorMessage: state.errorMessage,
            actionEvent: () {
              context.read<MouvementBloc>().add(state.currentAction);
            },
          );
        } else if (state.requestState == StateStatus.LOADED) {
          return SizedBox(
              child: state.mouvement.map.results.length == 0
                  ? Center(
                      child: Text(
                        AppLocalizations.of(context).aucun_mvt,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 16,
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor)),
                      ),
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      // controller: _scrollController,
                      itemCount: 3,
                      itemBuilder: (_, index) {
                        // if (cs.historiqueMouvements.listMouvements.length == 0)
                        //   return Text("no mouvement");

                        return ListTile(
                          visualDensity:
                              VisualDensity(horizontal: 0, vertical: -1),
                          leading: Transform.translate(
                            offset: Offset(
                                MediaQuery.of(context).size.width * 0.03, 0),
                            child: SvgPicture.asset(
                                "assets/$banque_id/images/Picto_virement.svg",
                                fit: BoxFit.fill,
                                width:
                                    MediaQuery.of(context).size.width * 0.028,
                                height:
                                    MediaQuery.of(context).size.height * 0.028,
                                color: GlobalParams
                                    .themes["$banque_id"].pictoColor),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.only(bottom: 4),
                            child: Text(
                                "${intl.DateFormat.yMMMMd('fr').format(intl.DateFormat("dd-MM-yyyy").parse(state.mouvement.map.results[index].resultDateOperation))}",
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                    color: GlobalParams
                                        .themes["$banque_id"].numCmpColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          ),
                          subtitle: Text(
                              "${state.mouvement.map.results[index].libelle}",
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                  color: GlobalParams
                                      .themes["$banque_id"].numCmpColor,
                                  fontSize: 13,
                                ),
                              )),
                          trailing: Padding(
                            padding: EdgeInsets.only(right: 3),
                            child: RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                    text: state.mouvement.map.results[index]
                                                .sens !=
                                            "C"
                                        ? " - ${state.mouvement.map.results[index].resultDebit}"
                                        : " + ${state.mouvement.map.results[index].resultCredit}",
                                    style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            color: Colors.white,
                                            // state.mouvement.map
                                            //             .results[index].sens !=
                                            //         "C"
                                            //     ? Colors.red
                                            //     : Colors.green[700],
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15))),
                                WidgetSpan(
                                  child: Transform.translate(
                                    offset: const Offset(2, -6),
                                    child: Text(
                                      " ${state.mouvement.map.results[index].devise}",
                                      //superscript is usually smaller in size
                                      textScaleFactor: 0.7,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: Colors.white,
                                              // state.mouvement.map
                                              //             .results[index].sens !=
                                              //         "C"
                                              //     ? Colors.red
                                              //     : Colors.green[700],
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)),
                                    ),
                                  ),
                                )
                              ]),
                            ),
                          ),
                        );
                      },
                    ));
        } else {
          return Container();
        }
      }),
    );
  }
}
