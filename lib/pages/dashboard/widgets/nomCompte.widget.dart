import 'package:LBA/bloc/nom-compte/nom-compte.bloc.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

class NomCompteWidget extends StatelessWidget {
  String titre;
  NomCompteWidget({BuildContext context, this.titre = "Mouvements"});
  CompteService get compteService => GetIt.I<CompteService>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NomCompteBloc, NomCompteStateBloc>(
        builder: (context, state) {
      if (state.requestState == StateStatus.LOADING) {
        return Text("...");
      } else if (state.requestState == StateStatus.ERROR) {
        return Text("");
      } else if (state.requestState == StateStatus.LOADED) {
        return Center(
          child: RichText(
            maxLines: 1,
            text: TextSpan(
                // style: TextStyle(
                //   color: KPrimaryColor[700],
                //   fontFamily: KprimaryFont,
                //   fontSize: 20,

                // ),
                children: <TextSpan>[
                  TextSpan(
                      text: " $titre du compte : ",
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      )),
                  TextSpan(
                      text:
                          "${state.currentCompte.intitule == null ? "" : state.currentCompte.intitule}  ",
                      // "${mouvement['Map']['list'].length > 0 ? mouvement['Map']['list'][0]['dateValeur'] : "00-00-00"}",
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          color: GlobalParams.themes["$banque_id"].numCmpColor,
                          fontSize: 17,
                        ),
                      )),
                ]),
          ),
        );
      } else {
        return Container();
      }
    });
  }
}
