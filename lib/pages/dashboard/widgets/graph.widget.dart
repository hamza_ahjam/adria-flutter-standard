
import 'package:LBA/bloc/graph/graph.bloc.dart';
import 'package:LBA/bloc/graph/graph.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class GraphWidget extends StatelessWidget {
  GraphWidget();

  double idx = 0;
  List<Color> gradientColors = [
    GlobalParams.themes["$banque_id"].graphbodyColor,
    GlobalParams.themes["$banque_id"].graphbodyColor
  ];
  @override
  Widget build(BuildContext context) {
    final f = new DateFormat('dd/MM');
    return BlocBuilder<GraphBloc, GraphStateBloc>(builder: (context, state) {
      if (state.requestState == StateStatus.LOADING) {
        idx = 0;
        gradientColors = [
          GlobalParams.themes["$banque_id"].graphbodyColor,
          GlobalParams.themes["$banque_id"].graphbodyColor
        ];
        return Container(
          height: MediaQuery.of(context).size.height * 0.25,
          child: Center(
              child: CircularProgressIndicator(
            backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
            valueColor: AlwaysStoppedAnimation<Color>(
                GlobalParams.themes["$banque_id"].appBarColor),
          )),
        );
      } else if (state.requestState == StateStatus.ERROR) {
        return ErreurTextWidget(
          errorMessage: state.errorMessage,
          actionEvent: () {
            context.read<GraphBloc>().add(state.currentAction);
          },
        );
      } else if (state.requestState == StateStatus.LOADED) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(18, 0, 18, 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(
                    0,
                    MediaQuery.of(context).size.height * 0.018,
                    0,
                    MediaQuery.of(context).size.height * 0.013),
                child: Text(
                  AppLocalizations.of(context).evolution_solde.toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  )),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: double.infinity,
                  child: state.graph.map.soldes.length == 0
                      ? Center(
                          child: Text(
                            "aucun graph pour le moment",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 16,
                                    color: GlobalParams
                                        .themes["$banque_id"].appBarColor)),
                          ),
                        )
                      : LineChart(
                          // swapAnimationDuration: Duration(milliseconds: 150),

                          LineChartData(
                            // showingTooltipIndicators: ,
                            lineTouchData: LineTouchData(
                              //fullHeightTouchLine: true,
                              touchTooltipData: LineTouchTooltipData(
                                tooltipBgColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                // showOnTopOfTheChartBoxArea: true,
                                tooltipRoundedRadius: 8,
                                getTooltipItems:
                                    (List<LineBarSpot> lineBarsSpot) {
                                  return lineBarsSpot.map((lineBarSpot) {
                                    return LineTooltipItem(
                                      lineBarSpot.y.toString(),
                                      TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    );
                                  }).toList();
                                },
                              ),
                              getTouchedSpotIndicator:
                                  (LineChartBarData barData,
                                      List<int> spotIndexes) {
                                return spotIndexes.map((index) {
                                  return TouchedSpotIndicatorData(
                                    FlLine(
                                      color: GlobalParams
                                          .themes["$banque_id"].infoBgColor,
                                      strokeWidth: 2,
                                    ),
                                    FlDotData(
                                      show: true,
                                      getDotPainter:
                                          (spot, percent, barData, index) =>
                                              FlDotCirclePainter(
                                        radius: 9,
                                        color: GlobalParams
                                            .themes["$banque_id"].infoBgColor,
                                        strokeWidth: 4,
                                        strokeColor: Colors.white,
                                      ),
                                    ),
                                  );
                                }).toList();
                              },
                            ),
                            titlesData: FlTitlesData(
                              bottomTitles: SideTitles(
                                  showTitles: true,
                                  getTitles: (value) =>
                                      '          ${f.format(new DateFormat("dd-MM").parse(state.graph.map.dates[value.toInt()]))}',
                                  getTextStyles: (context, value) => TextStyle(
                                        color: principaleColor8,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                      ),
                                  interval: 14,
                                  margin: 13,
                                  reservedSize: 15),
                              show: true,
                              leftTitles: SideTitles(
                                showTitles: false,
                                getTextStyles: (context, value) => TextStyle(
                                  color: principaleColor8,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10,
                                ),
                                interval: 200,
                                margin: 3,
                              ),
                              rightTitles: SideTitles(showTitles: false),
                              topTitles: SideTitles(showTitles: false),
                            ),
                            gridData: FlGridData(
                                drawVerticalLine: false,
                                getDrawingHorizontalLine: (value) {
                                  return FlLine(
                                      color: Colors.grey.withOpacity(0.06));
                                },
                                show: true,
                                horizontalInterval: 250),
                            // maxX: 30,
                            // minX: 1,
                            borderData: FlBorderData(
                              show: false,

                              // border:
                              //     Border.all(color: const Color(0xff37434d), width: 1),
                            ),
                            lineBarsData: [
                              // FlDotCirclePainter(color: Colors.black),
                              // FlLine(color: Colors.black),

                              LineChartBarData(
                                  spots: state.graph.map.soldes
                                      .map<FlSpot>((spot) =>
                                          FlSpot(idx++, spot.toDouble()))
                                      .toList(),
                                  isCurved: true,
                                  colors: gradientColors,
                                  // dotData: FlDotSquarePainter(size: 2),

                                  isStrokeCapRound: false,
                                  dotData: FlDotData(
                                    show: false,
                                  ),
                                  barWidth: 2,
                                  belowBarData: BarAreaData(
                                    show: false,
                                    colors: gradientColors = [
                                      GlobalParams.themes["$banque_id"]
                                          .graphGradienBodyColor,
                                      GlobalParams.themes["$banque_id"]
                                          .graphGradienBodyColor,
                                      // Color.fromRGBO(213, 7, 88, 0.1),
                                      // Color.fromRGBO(213, 7, 88, 0.1),
                                    ],
                                    // gradientFrom: Offset(1, 1),
                                    // gradientTo: Offset(1, 1),
                                  )),
                            ],
                            // FlDotCirclePainter(color: Colors.black),
                          ),
                          swapAnimationDuration:
                              Duration(milliseconds: 150), // Optional
                          // swapAnimationCurve: Curves.linear,
                        )),
            ],
          ),
        );
      } else {
        return Container();
      }
    });
  }
}
