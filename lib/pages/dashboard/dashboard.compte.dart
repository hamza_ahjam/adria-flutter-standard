import 'dart:io';

import 'package:LBA/alerts/Alerts_Settings.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/main.dart';
import 'package:LBA/pages/dashboard/widgets/compte.widget.dart';
import 'package:LBA/pages/dashboard/widgets/graph.widget.dart';
import 'package:LBA/pages/dashboard/widgets/mouvement.widget.dart';
import 'package:LBA/pages/mouvement/mouvement.compte.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/widgets/menu/onlineDrawer.dart';
import 'package:LBA/widgets/menu/onlineMenu.dart';
import 'package:expandable_bottom_bar/expandable_bottom_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class DashboardComptePage extends StatefulWidget {
  // CompteState compteState;
  DashboardComptePage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: true);
  }

  @override
  State<DashboardComptePage> createState() => _DashboardComptePageState();
}

class _DashboardComptePageState extends State<DashboardComptePage>
    with WidgetsBindingObserver {
  CompteService get compteService => GetIt.I<CompteService>();

  final storage = new FlutterSecureStorage();
  int notifCount = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> initNotif() async {
    await FirebaseMessaging.instance.getToken();
    FirebaseMessaging.onMessage.listen(receivedMessage);

    FirebaseMessaging.onMessageOpenedApp.listen(openedMessage);
  }

  void receivedMessage(RemoteMessage event) {
    print("message recieved");
    print(event.notification.body);

    if (mounted)
      setState(() {
        notifCount++;
        storage.write(key: 'notifCount', value: notifCount.toString());
      });
  }

  void openedMessage(message) {
    print('Message opened!');
    // setState(() {
    //   notifCount ++;
    //   print('notifCount $notifCount');
    //   storage.write(key: 'notifCount', value: notifCount.toString());
    // });
  }

  @override
  void initState() {
    super.initState();
    storage.read(key: 'notifCount').then((value) {
      if (mounted)
        setState(() {
          notifCount = value != null && value.isNotEmpty ? int.parse(value) : 0;
          print('notifCount $notifCount');
        });
    });
    initNotif();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      storage.read(key: 'notifCount').then((value) {
        if (mounted)
          setState(() {
            notifCount = value != null && value.isNotEmpty ? int.parse(value) : 0;
            print('notifCount $notifCount');
          });
      });
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // démarrage du timer au lancement de la partie online de l'application
    MyApp.initializeTimer();

    // SystemChrome.setSystemUIOverlayStyle(
    //     SystemUiOverlayStyle(statusBarColor: principaleColor5));
    // SystemChrome.setEnabledSystemUIOverlays([]);

    // if (this.tokenState.token == null) {
    //   // Navigator.of(context).popAndPushNamed("/login");
    //   Navigator.pop(context);
    //   Navigator.push(
    //       context, MaterialPageRoute(builder: (context) => LoginPage()));
    //   this.tokenState.logout();
    // }
    // this.tokenState.addListener(() {
    //   if (this.tokenState.timerExpired == true) {
    //     // print("${tokenState.currentTimeout}");
    //     this.tokenState.stopTimer();
    //     // Navigator.of(context).popAndPushNamed("/login");
    //     Navigator.pop(context);
    //     Navigator.push(
    //         context, MaterialPageRoute(builder: (context) => LoginPage()));
    //     this.tokenState.logout();
    //   }
    // });

    return WillPopScope(
      onWillPop: () {
        showDeconnectionDialog(context);
        return Future.value(true);
      },
      child: GestureDetector(
          // behavior: HitTestBehavior.translucent,
          // onHorizontalDragDown: (tapDown) {
          //   tokenState.initTimer();
          // },
          child: DefaultBottomBarController(
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: Container(
              color: GlobalParams.themes["$banque_id"].appBarColor,
            ),
          ),
          backgroundColor: Colors.white,
          key: _scaffoldKey,
          bottomNavigationBar: OnlineMenu(),
          drawer: SafeArea(
              child: OnlineDrawer(tokenState: compteService.tokenState)),
          body: ListView(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Colors.white,
                ),
                child: ListView(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "assets/$banque_id/images/login_bg.png"),
                            fit: BoxFit.cover,
                            alignment: Alignment(0, 0.85)),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(22.0),
                        ),
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            AppBar(
                              title: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: banque_id == "ADRIA" || banque_id == "NSIA"
                                    ? Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 10, 0, 10),
                                        child: Image.asset(
                                            "assets/$banque_id/images/A_LOGO.png"),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 15, 0, 10),
                                        child: SvgPicture.asset(
                                            "assets/$banque_id/images/logo.svg"),
                                      ),
                              ),
                              centerTitle: true,
                              elevation: 0,
                              backgroundColor: Colors.transparent,
                              leading: IconButton(
                                icon: SvgPicture.asset(
                                    MyApp.of(context).getLocale().languageCode == 'ar'
                                        ? "assets/$banque_id/images/drawerIc_ar.svg"
                                        : "assets/$banque_id/images/drawerIc.svg"),
                                onPressed: () =>
                                    _scaffoldKey.currentState.openDrawer(),
                              ),
                              actions: [
                                if (banque_id == "NSIA")
                                  Stack(
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          storage.write(
                                              key: 'notifCount', value: '');
                                          context.read<MessagerieBloc>().add(
                                              LoadMessageEnvEvent(page: 1));
                                          Navigator.pop(context);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AlertsSettings()));
                                        },
                                        icon: SvgPicture.asset(
                                            "assets/$banque_id/images/alert.svg"),
                                      ),
                                      notifCount != 0
                                          ? Positioned(
                                              bottom: 35,
                                              left: 25,
                                              child: Container(
                                                constraints: BoxConstraints(
                                                  minWidth: 20,
                                                  minHeight: 20,
                                                ),
                                                padding: EdgeInsets.all(2),
                                                child: Text(
                                                  "$notifCount",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                                decoration: new BoxDecoration(
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor,
                                                  borderRadius:
                                                      BorderRadius.circular(12),
                                                ),
                                              ))
                                          : Container()
                                    ],
                                  )
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 5, top: 10),
                              child: Container(
                                  width: double.infinity,
                                  height: Platform.isAndroid
                                      ? MediaQuery.of(context).size.height *
                                          0.21
                                      : MediaQuery.of(context).size.height *
                                          0.23,
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: CompteWidget(),
                                      ),
                                      // NomCompteWidget(context: context),
                                      BlocBuilder<CompteBloc, CompteStateBloc>(
                                        builder: (context, state) {
                                          if (state.requestState ==
                                              StateStatus.LOADED) {
                                            return Padding(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.03,
                                                  right: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.03),
                                              child: Row(
                                                children: [
                                                  Text(AppLocalizations.of(context).mvt,
                                                      style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: GlobalParams
                                                                .themes[
                                                                    "$banque_id"]
                                                                .intituleCmpColor,
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )),
                                                  Spacer(),
                                                  Container(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.035,
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.24,
                                                    child: MaterialButton(
                                                      minWidth: 0,
                                                      height: 0,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(50.0),
                                                      ),
                                                      color: GlobalParams
                                                          .themes["$banque_id"]
                                                          .intituleCmpColor,
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context,
                                                            PageTransition(
                                                                type: PageTransitionType
                                                                    .topToBottom,
                                                                child:
                                                                    MouvementComptePage()));
                                                        // MyApp.of(context).setLocale(Locale.fromSubtags(languageCode: 'en'));
                                                      },
                                                      child: Text(
                                                          AppLocalizations.of(context).afficher_plus,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: GoogleFonts
                                                              .roboto(
                                                            textStyle: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          )),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            );
                                          } else {
                                            return Container();
                                          }
                                        },
                                      )
                                    ],
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: GestureDetector(
                                onTap: () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             MouvementComptePage()));
                                },
                                child: Container(
                                  width: double.infinity,
                                  height: Platform.isAndroid
                                      ? MediaQuery.of(context).size.height *
                                          0.25
                                      : MediaQuery.of(context).size.height *
                                          0.21,
                                  child: MouvementWidget(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GraphWidget(),
                  ],
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
