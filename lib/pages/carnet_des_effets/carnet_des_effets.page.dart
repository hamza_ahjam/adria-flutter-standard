import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/carnet_des_effets/widget/carnet_effets_list.widget.dart';
import 'package:LBA/pages/carnet_des_effets/widget/compte_carnet_effets.widget.dart';
import 'package:LBA/pages/carnet_des_effets/widget/form.carnet_effets.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CarnetDesEffetsPage extends StatelessWidget {
  const CarnetDesEffetsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).carnets_effets,
          context: context,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => FormCarnetEffets())),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          elevation: 4,
          backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          "assets/$banque_id/images/background_image.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.9),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                    ),
                  ),
                  child: CompteCarnetEffetsWidget()),
            ),
            Expanded(child: ListCarnetEffetWidget()),
          ],
        ),
      ),
    );
  }
}
