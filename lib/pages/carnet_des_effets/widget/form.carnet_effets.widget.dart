import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/cheques/widgets/demandeChequier.form.radio.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../main.dart';

class FormCarnetEffets extends StatefulWidget {
  FormCarnetEffets({Key key}) : super(key: key);

  @override
  _FormCarnetEffetsState createState() => _FormCarnetEffetsState();
}

class _FormCarnetEffetsState extends State<FormCarnetEffets> {
  var radio1 = '1';
  var radio2 = "false";
  var radio3 = "false";
  var passwordController = TextEditingController();
  GlobalKey<FormState> _formchequierKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).commander_carnet.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pop(context);
              }),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.12,
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.17,
                  right: MediaQuery.of(context).size.width * 0.17,
                  top: 20),
              child: Text('${AppLocalizations.of(context).nbre_feuille} :  ',
                  style:
                      GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  width: 150,
                  padding: EdgeInsets.only(
                    left: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 0
                        : MediaQuery.of(context).size.width * 0.17 - 13,
                    right: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? MediaQuery.of(context).size.width * 0.17 - 13
                        : 0,
                  ),
                  child: LabeledRadio(
                    value: '2',
                    groupValue: radio1,
                    onChanged: (v) {
                      setState(() {
                        radio1 = "2";
                      });
                    },
                    label: Text(
                      '50 ',
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  padding: EdgeInsets.only(
                    left: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 0
                        : MediaQuery.of(context).size.width * 0.2,
                    right: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? MediaQuery.of(context).size.width * 0.2
                        : 0,
                  ),
                  child: LabeledRadio(
                    value: '1',
                    groupValue: radio1,
                    onChanged: (v) {
                      setState(() {
                        radio1 = "1";
                      });
                    },
                    label: Text(
                      '25',
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
              ],
            ),
            Padding(
              padding:
              EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.17,
                right: MediaQuery.of(context).size.width * 0.17,
              ),
              child: Text('${AppLocalizations.of(context).barre} :',
                  style:
                      GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 150,
                  height: MediaQuery.of(context).size.height * 0.08,
                  padding: EdgeInsets.only(
                      left: MyApp.of(context).getLocale().languageCode == 'ar'
                          ? 0
                          : MediaQuery.of(context).size.width * 0.17 - 13,
                      right: MyApp.of(context).getLocale().languageCode == 'ar'
                          ? MediaQuery.of(context).size.width * 0.17 - 13
                          : 0,
                  ),
                  child: LabeledRadio(
                    value: 'true',
                    groupValue: radio2,
                    onChanged: (v) {
                      setState(() {
                        radio2 = v;
                      });
                    },
                    label: Text(
                      AppLocalizations.of(context).oui,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  padding: EdgeInsets.only(
                      left: MyApp.of(context).getLocale().languageCode == 'ar'
                          ? 0
                          : MediaQuery.of(context).size.width * 0.2,
                      right: MyApp.of(context).getLocale().languageCode == 'ar'
                          ? MediaQuery.of(context).size.width * 0.2
                          : 0,
                  ),
                  child: LabeledRadio(
                    value: "false",
                    groupValue: radio2,
                    onChanged: (v) {
                      setState(() {
                        radio2 = v;
                      });
                    },
                    label: Text(
                      AppLocalizations.of(context).non,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
              ],
            ),
            Padding(
              padding:
              EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.17,
                right: MediaQuery.of(context).size.width * 0.17,
              ),
              child: Text('${AppLocalizations.of(context).endossable} :',
                  style:
                      GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 150,
                  height: MediaQuery.of(context).size.height * 0.08,
                  padding: EdgeInsets.only(
                    left: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 0
                        : MediaQuery.of(context).size.width * 0.17 - 13,
                    right: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? MediaQuery.of(context).size.width * 0.17 - 13
                        : 0,
                  ),
                  child: LabeledRadio(
                    value: "true",
                    groupValue: radio3,
                    onChanged: (v) {
                      setState(() {
                        radio3 = v;
                      });
                    },
                    label: Text(
                      AppLocalizations.of(context).oui,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  padding: EdgeInsets.only(
                    left: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? 0
                        : MediaQuery.of(context).size.width * 0.2,
                    right: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? MediaQuery.of(context).size.width * 0.2
                        : 0,
                  ),
                  child: LabeledRadio(
                    value: "false",
                    groupValue: radio3,
                    onChanged: (v) {
                      setState(() {
                        radio3 = v;
                      });
                    },
                    label: Text(
                      AppLocalizations.of(context).non,
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                    padding: EdgeInsets.zero,
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.2,
                right: MediaQuery.of(context).size.width * 0.15,
                left: MediaQuery.of(context).size.width * 0.15,
              ),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.06,
                // width: MediaQuery.of(context).size.width * 0.7,
                child: RaisedButton(
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Text(
                    AppLocalizations.of(context).commander,
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
