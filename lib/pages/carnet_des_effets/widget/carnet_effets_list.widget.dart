import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../main.dart';

class ListCarnetEffetWidget extends StatelessWidget {
  const ListCarnetEffetWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp.of(context).getLocale().languageCode;

    return ListView(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05,
              10, MediaQuery.of(context).size.width * 0.05, 10),
          child: Card(
            elevation: 5,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: ListTile(
                      title: Text(
                        AppLocalizations.of(context).num_ref,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.white)),
                      ),
                      trailing: Text(
                        "CHQ00000094949848C ",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.white)),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).date_demande} :",
                        style: GoogleFonts.roboto()),
                    trailing: Text(
                      "${DateFormat('dd MMMM yyyy', languageCode).format(DateTime.now())}",
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).num_compte} :",
                        style: GoogleFonts.roboto()),
                    trailing: Text("99858457765757",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).type} :",
                        style: GoogleFonts.roboto()),
                    trailing: Text("25 unités",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).statut} :", style: GoogleFonts.roboto()),
                    trailing: Text("Rejeté",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).nombre} :", style: GoogleFonts.roboto()),
                    trailing: Text("1",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
