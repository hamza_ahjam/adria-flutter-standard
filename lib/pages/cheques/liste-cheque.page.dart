import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/cheques/widgets/compteChequie.widget.dart';
import 'package:LBA/pages/cheques/widgets/demande-chequier.widget.dart';
import 'package:LBA/pages/cheques/widgets/demandeChequier.form.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'historiqueCheques_page.dart';

class ListChequePage extends StatelessWidget {
  // CompteState compteState;
  // TokenState tokenState;
  ListChequePage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState.loadDemendeChequier(tokenState);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).cheques,
          context: context,
          isHisto: true,
          destination: MaterialPageRoute(
              builder: (context) => HistoriqueChequePage()),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => showDemandeCheque(context),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          elevation: 4,
          backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
        ),
        // bottomNavigationBar: Container(
        //     // color: Color(0xFFE6543F),
        //     height: 70,
        //     child: RaisedButton.icon(
        //         onPressed: () {
        //           // Navigator.push(context, MaterialPageRoute(builder: (context) => DemandeChequierForm(context)));
        //           showDemandeCheque(context);
        //         },
        //         color: GlobalParams.themes["$banque_id"].intituleCmpColor,
        //         icon: Icon(
        //           Icons.payments,
        //           color: Colors.white,
        //         ),
        //         label: Text("Demander chèquier",
        //             style: GoogleFonts.roboto(
        //                 textStyle:
        //                     TextStyle(color: Colors.white, fontSize: 16))))),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          "assets/$banque_id/images/background_image.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.9),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                    ),
                  ),
                  child: CompteChequeWidget()),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                AppLocalizations.of(context).histo_dem_chq,
                style: TextStyle(
                    color: GlobalParams.themes["$banque_id"].appBarColor,
                    fontSize: 16),
              ),
            ),
            Expanded(child: DemandeChequierWidget()),
          ],
        ),
      ),
    );
  }
}

showDemandeCheque(context) {
  return showModalBottomSheet(
      isScrollControlled: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
        ),
      ),
      isDismissible: true,
      context: context,
      builder: (context) {
        return Container(
          //height: MediaQuery.of(context).size.height * 0.4,
          child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
              ),
              child: DemandeChequierForm()),
        );
      });
}
