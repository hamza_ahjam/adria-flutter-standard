import 'package:LBA/config/global.params.dart';
import 'package:LBA/pages/cheques/widgets/compteChequie.widget.dart';
import 'package:LBA/pages/cheques/widgets/listeCheques_widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';

class HistoriqueChequePage extends StatefulWidget {
  int index;

  HistoriqueChequePage({Key key, this.index = 0}) : super(key: key);

  @override
  State<HistoriqueChequePage> createState() => _HistoriqueChequePageState();
}

class _HistoriqueChequePageState extends State<HistoriqueChequePage> {
  String status;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        context: context,
        appBarTitle: AppLocalizations.of(context).cheques,
        isChq: true,
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    "assets/$banque_id/images/background_image.png"),
                fit: BoxFit.cover,
                alignment: Alignment(0, 0.9),
              ),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(22.0),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CompteChequeWidget(isHistoCheque: true,),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Text(
              AppLocalizations.of(context).histo_chq,
              style: TextStyle(
                  color: GlobalParams.themes["$banque_id"].appBarColor,
                  fontSize: 16),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.05,
              5,
              MediaQuery.of(context).size.width * 0.05,
              MediaQuery.of(context).size.width * 0.02,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      status = "Payé";
                      widget.index = 0;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Text(AppLocalizations.of(context).status_paye,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 14,
                                color: widget.index == 0
                                    ? Colors.white
                                    : GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor))),
                    decoration: BoxDecoration(
                        color: widget.index == 0
                            ? GlobalParams.themes["$banque_id"].appBarColor
                            : Colors.white,
                        border: Border.all(
                            color: widget.index == 0
                                ? Colors.white
                                : GlobalParams
                                    .themes["$banque_id"].intituleCmpColor),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      status = "Encaissé";
                      widget.index = 1;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Text(AppLocalizations.of(context).status_encaisse,
                        textAlign: TextAlign.center,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 14,
                                color: widget.index == 1
                                    ? Colors.white
                                    : GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor))),
                    decoration: BoxDecoration(
                        color: widget.index == 1
                            ? GlobalParams.themes["$banque_id"].appBarColor
                            : Colors.white,
                        border: Border.all(
                            color: widget.index == 1
                                ? Colors.white
                                : GlobalParams
                                    .themes["$banque_id"].intituleCmpColor),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      status = "Impayé";
                      widget.index = 2;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Text(AppLocalizations.of(context).status_impaye,
                        textAlign: TextAlign.center,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 14,
                                color: widget.index == 2
                                    ? Colors.white
                                    : GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor))),
                    decoration: BoxDecoration(
                        color: widget.index == 2
                            ? GlobalParams.themes["$banque_id"].appBarColor
                            : Colors.white,
                        border: Border.all(
                            color: widget.index == 2
                                ? Colors.white
                                : GlobalParams
                                    .themes["$banque_id"].intituleCmpColor),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: ListChequesWidget(statut: status,)),
        ],
      ),
    );
  }
}
