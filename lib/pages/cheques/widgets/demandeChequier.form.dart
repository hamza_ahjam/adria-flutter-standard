import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/cheque/cheque.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'demandeChequier.form.radio.widget.dart';

class DemandeChequierForm extends StatefulWidget {
  // CompteState compteState;
  // TokenState tokenState;
  DemandeChequierForm() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
  }

  @override
  _DemandeChequierFormState createState() => _DemandeChequierFormState();
}

class _DemandeChequierFormState extends State<DemandeChequierForm> {
  var radio1 = '1';
  var radio2 = "false";
  var radio3 = "false";
  var passwordController = TextEditingController();
  GlobalKey<FormState> _formchequierKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // print("radio1 : $radio1 -- radio2: $radio2 -- radio3: $radio3");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[200],
        title: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(AppLocalizations.of(context).commander_chq.toUpperCase(),
              style: GoogleFonts.roboto(
                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              )),
        ),
        elevation: 0,
        centerTitle: true,
        leading: Container(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.17,
                right: MediaQuery.of(context).size.width * 0.17,
                top: 20),
            child: Text('${AppLocalizations.of(context).nbre_feuille} :  ',
                style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.08,
                width: 150,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.17 - 13),
                child: LabeledRadio(
                  value: '2',
                  groupValue: radio1,
                  onChanged: (v) {
                    setState(() {
                      radio1 = "2";
                    });
                  },
                  label: Text(
                    '50 ',
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.08,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.2),
                child: LabeledRadio(
                  value: '1',
                  groupValue: radio1,
                  onChanged: (v) {
                    setState(() {
                      radio1 = "1";
                    });
                  },
                  label: Text(
                    '25',
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
            ],
          ),
          Padding(
            padding:
                EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.17,
                    right: MediaQuery.of(context).size.width * 0.17,
                ),
            child: Text('${AppLocalizations.of(context).barre} :',
                style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
          ),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Container(
                width: 150,
                height: MediaQuery.of(context).size.height * 0.08,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.17 - 13),
                child: LabeledRadio(
                  value: 'true',
                  groupValue: radio2,
                  onChanged: (v) {
                    setState(() {
                      radio2 = v;
                    });
                  },
                  label: Text(
                    AppLocalizations.of(context).oui,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.08,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.2),
                child: LabeledRadio(
                  value: "false",
                  groupValue: radio2,
                  onChanged: (v) {
                    setState(() {
                      radio2 = v;
                    });
                  },
                  label: Text(
                    AppLocalizations.of(context).non,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
            ],
          ),
          Padding(
            padding:
                EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.17,
                    right: MediaQuery.of(context).size.width * 0.17,
                ),
            child: Text('${AppLocalizations.of(context).endossable} :',
                style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20))),
          ),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Container(
                width: 150,
                height: MediaQuery.of(context).size.height * 0.08,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.17 - 13),
                child: LabeledRadio(
                  value: "true",
                  groupValue: radio3,
                  onChanged: (v) {
                    setState(() {
                      radio3 = v;
                    });
                  },
                  label: Text(
                    AppLocalizations.of(context).oui,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.08,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.2),
                child: LabeledRadio(
                  value: "false",
                  groupValue: radio3,
                  onChanged: (v) {
                    setState(() {
                      radio3 = v;
                    });
                  },
                  label: Text(
                    AppLocalizations.of(context).non,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 20)),
                  ),
                  padding: EdgeInsets.zero,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 10,
              right: MediaQuery.of(context).size.width * 0.15,
              left: MediaQuery.of(context).size.width * 0.15,
            ),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.06,
              // width: MediaQuery.of(context).size.width * 0.7,
              child: RaisedButton(
                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: Text(
                  AppLocalizations.of(context).commander,
                  style: TextStyle(color: Colors.white, fontSize: 17),
                ),
                onPressed: () {
                  // showLoading(context);

                  context.read<ChequeBloc>().add(DemandeSaveChequieEvent(
                      context: context,
                      crossed: radio2,
                      endorsable: radio3,
                      numChequiers: 1,
                      typeChequier: radio1));
                },
              ),
            ),
          ),
          BlocListener<ChequeBloc, ChequeStateBloc>(
            listener: (context, state) {
              if (state.requestStateSave == StateStatus.LOADING) {
                showLoading(context);
              } else if (state.requestStateSave == StateStatus.LOADED) {
                Navigator.pop(context);
                Navigator.pop(context);
                signeDemandeChequier(
                    state.res["id"], state.res["taskId"], context);
              } else if (state.requestStateSave == StateStatus.ERROR) {
                Navigator.pop(context);
                Navigator.pop(context);
                errorAlert(
                    context,
                    state.errorMessage == null
                        ? "error occured! try later"
                        : state.errorMessage);
              }
              //
            },
            child: Container(),
          )
        ],
      ),
    );
  }

  signeDemandeChequier(id, taskId, context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "CODE DE VALIDATION".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formchequierKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Center(
                    child: Text(
                        "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(fontSize: 16))),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height * 0.045),
                    child: Container(
                      height: 50,
                      child: TextFormField(
                        controller: passwordController,
                        keyboardType: TextInputType.number,
                        validator: (v) {
                          if (v.isEmpty)
                            return "Mot de passe ne peut pas etre vide";
                          return null;
                        },

                        // keyboardAppearance: Brightness.dark,
                        style: TextStyle(color: Colors.black),
                        obscureText: true,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          hintText: '___   ___   ___   ___   ___   ___',
                          hintStyle:
                              TextStyle(fontSize: 16, color: Colors.black),
                          enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Colors.white,
                            child: Text("Annuler",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        color: GlobalParams.themes["$banque_id"]
                                            .intituleCmpColor,
                                        fontSize: 17)))),
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          onPressed: () {
                            if (_formchequierKey.currentState.validate()) {
                              context.read<ChequeBloc>().add(
                                  DemandeSigneChequieEvent(
                                      psd: passwordController.text));
                            }
                          },
                          child: Text("Valider",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                fontSize: 17,
                                color: Colors.white,
                              ))),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          actions: [
            BlocListener<ChequeBloc, ChequeStateBloc>(
              listener: (context, state) {
                if (state.requestStateSigne == StateStatus.LOADING) {
                  showLoading(context);
                } else if (state.requestStateSigne == StateStatus.LOADED) {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  okAlert(
                      context,
                      state.res["message"] == null
                          ? "demande envoyer"
                          : state.res["message"]);
                  context.read<ChequeBloc>().add(LoadChequeEvent());
                } else if (state.requestStateSigne == StateStatus.ERROR) {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  errorAlert(
                      context,
                      state.errorMessage == null
                          ? "Erreur technique"
                          : state.errorMessage);
                }
              },
              child: Container(),
            )
          ],
        );
      },
    );
  }
}
