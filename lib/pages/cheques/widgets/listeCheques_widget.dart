import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/cheque/cheque.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../main.dart';

class ListChequesWidget extends StatelessWidget {
  ListChequesWidget({Key key, this.statut}) : super(key: key);

  bool isLoaded = false;
  String statut;
  var filtredList;

  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp
        .of(context)
        .getLocale()
        .languageCode;

    return BlocBuilder<ChequeBloc, ChequeStateBloc>(builder: (context, state) {
      if (state.requestStateHisto == StateStatus.NONE) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (!isLoaded) context.read<ChequeBloc>().add(LoadHistoriqueChequeEvent());
        });
        return Container();
      } else if (state.requestStateHisto == StateStatus.LOADING) {
        return Center(
            heightFactor: 15,
            child: CircularProgressIndicator(
              backgroundColor:
              GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
      } else if (state.requestStateHisto == StateStatus.ERROR) {
        return ErreurTextWidget(
          errorMessage: state.errorMessage,
          actionEvent: () {
            context.read<ChequeBloc>().add(state.currentAction);
          },
        );
      } else if (state.requestStateHisto == StateStatus.LOADED) {
        isLoaded = true;
        if (state.listCheques.map.cheques.length == 0)
          return Center(
            heightFactor: 15,
            child: Text(
              AppLocalizations.of(context).aucun_chq,
              // "Vous ne pouvez pas demander vos chèque avec un compte en devise",
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(fontSize: 16, color: principaleColor5)),
            ),
          );
        else {
          if (statut == null)
            filtredList = state.listCheques.map.cheques.where((element) => element.statut == "Payé").toList();
          else
            filtredList = state.listCheques.map.cheques.where((element) => element.statut == statut).toList();
          return ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: filtredList.length,
              itemBuilder: (_, index) {
                print(statut);
                return Padding(
                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05,
                      10, MediaQuery.of(context).size.width * 0.05, 10),
                  child: Card(
                    elevation: 5,
                    shape:
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color:
                                GlobalParams.themes["$banque_id"].intituleCmpColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    topRight: Radius.circular(30))),
                            child: ListTile(
                              title: Text(
                                AppLocalizations.of(context).num_chq,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        color: Colors.white)),
                              ),
                              trailing: Text(
                                "${filtredList[index].id}",
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                        color: Colors.white)),
                              ),
                            ),
                          ),
                          ListTile(
                            title:
                            Text(
                                AppLocalizations.of(context).date_paiement,
                                style: GoogleFonts.roboto()),
                            trailing: Text(
                              "${DateFormat('dd MMMM yyyy', languageCode).format(DateFormat("dd-MM-yyyy").parse(filtredList[index].datePaiement))}",
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          ListTile(
                            title: Text(AppLocalizations.of(context).num_compte_em,
                                style: GoogleFonts.roboto()),
                            trailing: Text(filtredList[index].numeroCompte,
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ),
                          ListTile(
                            title: Text(AppLocalizations.of(context).statut,
                                style: GoogleFonts.roboto()),
                            trailing: Text(filtredList[index].statut,
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ),
                          ListTile(
                            title: Text(AppLocalizations.of(context).montant,
                                style: GoogleFonts.roboto()),
                            trailing: Text("${filtredList[index].montant}",
                                style: GoogleFonts.roboto(
                                  textStyle: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.02,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              });
        }
      } else {
        if (!isLoaded) context.read<ChequeBloc>().add(LoadHistoriqueChequeEvent());
        return Container();
      }
    });
  }
}
