import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/cheque/cheque.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/show-more.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';

class DemandeChequierWidget extends StatelessWidget {
  DemandeChequierWidget({Key key}) : super(key: key);

  bool isLoaded = false;
  int page = 1;
  @override
  Widget build(BuildContext context) {
    var languageCode = MyApp
        .of(context)
        .getLocale()
        .languageCode;

    return BlocBuilder<ChequeBloc, ChequeStateBloc>(builder: (context, state) {
      if (state.requestState == StateStatus.NONE) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (!isLoaded) context.read<ChequeBloc>().add(LoadChequeEvent());
        });
        return Container();
      } else if (state.requestState == StateStatus.LOADING) {
        return Center(
            heightFactor: 15,
            child: CircularProgressIndicator(
              backgroundColor:
              GlobalParams.themes["$banque_id"].intituleCmpColor,
              valueColor: AlwaysStoppedAnimation<Color>(
                  GlobalParams.themes["$banque_id"].appBarColor),
            ));
      } else if (state.requestState == StateStatus.ERROR) {
        return ErreurTextWidget(
          errorMessage: state.errorMessage,
          actionEvent: () {
            context.read<ChequeBloc>().add(state.currentAction);
          },
        );
      } else if (state.requestState == StateStatus.LOADED) {
        isLoaded = true;
        if (state.listChequier.map.liste.length == 0)
          return Center(
            heightFactor: 15,
            child: Text(
              AppLocalizations.of(context).aucune_dem_chq,
              // "Vous ne pouvez pas demander vos chèque avec un compte en devise",
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(fontSize: 16, color: principaleColor5)),
            ),
          );
        else
          return SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: state.listChequier.map.liste.length,
                    itemBuilder: (_, index) {
                      return Padding(
                        padding:  EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.05,
                            10,
                            MediaQuery.of(context).size.width * 0.05,
                            10),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(30),
                                          topRight: Radius.circular(30))),
                                  child: ListTile(
                                    title: Text(
                                      AppLocalizations.of(context).num_ref,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .appBarColor)),
                                    ),
                                    trailing: Text(
                                      "${state.listChequier.map.liste[index].identifiantDemande}",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16,
                                              color: GlobalParams
                                                  .themes["$banque_id"]
                                                  .appBarColor)),
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text(AppLocalizations.of(context).num_compte,
                                      style: GoogleFonts.roboto()),
                                  trailing: Text(
                                    "${state.listChequier.map.liste[index].numeroCompte}",
                                    style: GoogleFonts.roboto(),
                                  ),
                                ),
                                ListTile(
                                  title: Text(AppLocalizations.of(context).type,
                                      style: GoogleFonts.roboto()),
                                  trailing: Text(
                                      "${state.listChequier.map.liste[index].typeChequierLibelle}",
                                      style: GoogleFonts.roboto()),
                                ),
                                ListTile(
                                  title: Text(AppLocalizations.of(context).statut,
                                      style: GoogleFonts.roboto()),
                                  trailing: Text(
                                      "${state.listChequier.map.liste[index].statut}",
                                      style: GoogleFonts.roboto()),
                                ),
                                ListTile(
                                  title: Text(AppLocalizations.of(context).nombre,
                                      style: GoogleFonts.roboto()),
                                  trailing: Text(
                                      "${state.listChequier.map.liste[index].nombreChequiers}",
                                      style: GoogleFonts.roboto()),
                                ),
                                ListTile(
                                  title: Text(AppLocalizations.of(context).date_demande,
                                      style: GoogleFonts.roboto()),
                                  trailing: Text(
                                      "${DateFormat.yMMMMd(languageCode).format(DateFormat("dd-MM-yyyy").parse(state.listChequier.map.liste[index].dateEnvoiDemande))}",
                                      style: GoogleFonts.roboto()),
                                ),
                                // Divider(
                                //   color: Colors.black,
                                // )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                state.listChequier.map.liste.length > 19
                    ? ShowMore(
                  showMoreAction: () {
                    context
                        .read<ChequeBloc>()
                        .add(LoadChequeEvent(page: page));
                    // _compteState.loadNextHistoriquePage(tokenState, page);
                    ++page;
                  },
                )
                    : Container()
              ],
            ),
          );
      } else {
        return Container();
      }
    });
  }
}
