import 'dart:io';

import 'package:LBA/bloc/rib/rib.bloc.dart';
import 'package:LBA/bloc/rib/rib.event.dart';
import 'package:LBA/bloc/rib/rib.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/rib/widgets/compteRib.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
// import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:open_file/open_file.dart';
// import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class RibPage extends StatelessWidget {
  // CompteState compteState;
  // TokenState tokenState;
  RibPage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState.loadRib(tokenState);
    // print(tokenState.getUserDetails()["identifiantContrat"]);
    // flutterDownload();
    // getPermission();
  }
  bool isLoaded = false;

  // getPermission() async {
  //   print("get permission");
  //   await Permission.storage.request();
  //   // await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  // }

  showSnackBar(file) {
    final snackBar = SnackBar(
      content: Text("Rib est télécharger avec succes",
          style: GoogleFonts.roboto(color: Colors.white)),
      duration: Duration(seconds: 3),
      backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
      action: SnackBarAction(
          label: "open",
          onPressed: () {
            OpenFile.open(file);
          }),
    );
    return _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).rib_titre,
          context: context,
        ),
        body: ListView(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      "assets/$banque_id/images/background_image.png"),
                  fit: BoxFit.cover,
                  alignment: Alignment(0, 0.9),
                ),
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(22.0),
                ),
              ),
              // padding: EdgeInsets.only(top: 10),
              child: CompteRibWidget(
                context: context,
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.only(bottom: 10),
            //   child: Center(
            //     child: NomCompteWidget(
            //       context: context,
            //       titre: "RIB",
            //     ),
            //   ),
            // ),
            BlocBuilder<RibBloc, RibStateBloc>(builder: (context, state) {
              if (state.requestState == StateStatus.NONE) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  if (!isLoaded) context.read<RibBloc>().add(LoadRibPdfEvent());
                });
                return Container();
              } else if (state.requestState == StateStatus.LOADING) {
                return Center(
                    heightFactor: 10,
                    child: CircularProgressIndicator(
                      backgroundColor:
                          GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ));
              } else if (state.requestState == StateStatus.ERROR) {
                return ErreurTextWidget(
                  errorMessage: state.errorMessage,
                  actionEvent: () {
                    context.read<RibBloc>().add(state.currentAction);
                  },
                );
              } else if (state.requestState == StateStatus.LOADED) {
                isLoaded = true;
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                          padding: EdgeInsets.fromLTRB(17, 20, 17, 5),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                              color: Colors.white),
                          // width: MediaQuery.of(context).size.width * 0.9,
                          height: MediaQuery.of(context).size.height * 0.5,
                          child: SfPdfViewer.file(
                            state.file,
                            canShowPaginationDialog: true,
                            // enableTextSelection: true,
                            enableDoubleTapZooming: true,
                          )),
                    ),
                    Platform.isAndroid
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                child: RaisedButton(
                                    elevation: 4,
                                    shape: CircleBorder(),
                                    onPressed: () {
                                      Share.shareFiles(["${state.file.path}"],
                                          text: state.relevePdf.map.filename);
                                    },
                                    color: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                    child: Icon(
                                      Icons.share_outlined,
                                      color: Colors.white,
                                      size: 30,
                                    )),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                child: RaisedButton(
                                    elevation: 4,
                                    shape: CircleBorder(),
                                    onPressed: () async {
                                      try {
                                        await Permission.storage.request();

                                        String path = await ExternalPath
                                            .getExternalStoragePublicDirectory(
                                                ExternalPath
                                                    .DIRECTORY_DOWNLOADS);

                                        String fullPath =
                                            "$path/${state.relevePdf.map.filename}";

                                        File file = File(fullPath);
                                        var raf =
                                            file.openSync(mode: FileMode.write);

                                        var pdfDocument =
                                            PdfDocument.fromBase64String(
                                                state.relevePdf.map.content);
                                        var pdfBytes = pdfDocument.save();

                                        raf.writeFromSync(pdfBytes);

                                        await raf.close();
                                        showSnackBar(fullPath);
                                      } catch (e) {
                                        print(
                                            "rib download err widget err *** $e");
                                        errorAlert(context, e.toString());
                                      }
                                    },
                                    color: GlobalParams
                                        .themes["$banque_id"].intituleCmpColor,
                                    child: Icon(
                                      Icons.download_outlined,
                                      color: Colors.white,
                                      size: 30,
                                    )),
                              ),
                            ],
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.06,
                                child: RaisedButton(
                                  shape: CircleBorder(),
                                  onPressed: () {
                                    Share.shareFiles(["${state.file.path}"],
                                        text: state.relevePdf.map.filename);
                                  },
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor,
                                  child: Icon(
                                    Icons.share_outlined,
                                    color: Colors.white,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ],
                          )
                  ],
                );
              } else {
                return Container();
              }
            })
          ],
        ),
      ),
    );
  }
}
