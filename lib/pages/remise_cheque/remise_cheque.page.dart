import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/remise_cheque/widget/compte_remise_cheque.widget.dart';
import 'package:LBA/pages/remise_cheque/widget/nouvelle_remise.widget.dart';
import 'package:LBA/pages/remise_cheque/widget/remise_cheque_list.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class RemiseChequePage extends StatefulWidget {
  int index = 0;
  RemiseChequePage({Key key, this.index = 0}) : super(key: key);

  @override
  _RemiseChequePageState createState() => _RemiseChequePageState();
}

class _RemiseChequePageState extends State<RemiseChequePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).remise_cheques,
          context: context,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => NouvelleRemiseWidget())),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          elevation: 4,
          backgroundColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          "assets/$banque_id/images/background_image.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.9),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                    ),
                  ),
                  child: CompteRemiseChequeWidget()),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.05,
                5,
                MediaQuery.of(context).size.width * 0.05,
                MediaQuery.of(context).size.width * 0.02,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.index = 0;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).a_encaisser,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 0
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 0
                              ? GlobalParams
                                  .themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.index = 1;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text(AppLocalizations.of(context).a_payer,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 1
                                      ? Colors.white
                                      : GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor))),
                      decoration: BoxDecoration(
                          color: widget.index == 1
                              ? GlobalParams
                                  .themes["$banque_id"].intituleCmpColor
                              : Colors.white,
                          border: Border.all(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(child: ListRemiseChequeWidget()),
          ],
        ),
      ),
    );
  }
}
