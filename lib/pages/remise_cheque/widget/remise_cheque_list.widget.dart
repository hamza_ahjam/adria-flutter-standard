import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class ListRemiseChequeWidget extends StatelessWidget {
  const ListRemiseChequeWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05,
              10, MediaQuery.of(context).size.width * 0.05, 10),
          child: Card(
            elevation: 5,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: ListTile(
                      title: Text(
                        AppLocalizations.of(context).num_dossier,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.white)),
                      ),
                      trailing: Text(
                        "00000094949848C ",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.white)),
                      ),
                    ),
                  ),
                  ListTile(
                    title:
                        Text("${AppLocalizations.of(context).ref_remise} :", style: GoogleFonts.roboto()),
                    trailing: Text(
                      "544543544",
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  ListTile(
                    title:
                        Text("${AppLocalizations.of(context).type_val} :", style: GoogleFonts.roboto()),
                    trailing: Text("Internet",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).compte_crediter} :",
                        style: GoogleFonts.roboto()),
                    trailing: Text("461865615618168",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).rib_tire} :", style: GoogleFonts.roboto()),
                    trailing: Text("2344 5876 0987 6451 8715 9871",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).date_sort} :", style: GoogleFonts.roboto()),
                    trailing: Text("13/12/21",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).statut} :", style: GoogleFonts.roboto()),
                    trailing: Text("En cours",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  // Divider(
                  //   color: Colors.black,
                  // )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
