import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class NouvelleRemiseFormWidget extends StatefulWidget {
  var compteCrediter;
  NouvelleRemiseFormWidget({Key key, this.compteCrediter}) : super(key: key);

  @override
  _NouvelleRemiseFormWidgetState createState() =>
      _NouvelleRemiseFormWidgetState();
}

class _NouvelleRemiseFormWidgetState extends State<NouvelleRemiseFormWidget> {
  TextEditingController _numCheque = TextEditingController();
  TextEditingController _cmpCredit = TextEditingController();
  TextEditingController _typeRemise = TextEditingController();
  TextEditingController _montant = TextEditingController();
  final _formPsdKey = GlobalKey<FormState>();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _cmpCredit.text = widget.compteCrediter.intitule +
        " " +
        widget.compteCrediter.identifiantInterne;
  }

  @override
  void dispose() {
    super.dispose();
    _numCheque.dispose();
    _typeRemise.dispose();
    _montant.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "Nouvelle remise".toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pop(context);
              }),
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.05,
            MediaQuery.of(context).size.height * 0.01,
          ),
          child: ListView(
            children: [
              Text(
                "En confirmant votre remise de chèque, vous déclarez sur l’honneur avoir fourni des informations correctes et conformes",
                style: GoogleFonts.roboto(),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.035,
              ),
              Text(
                "Numéro de chèque :",
                style: GoogleFonts.roboto(),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _numCheque,
                  cursorColor: secondaryColor7,
                  decoration: InputDecoration(
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: '85546789965020097',
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.035,
              ),
              Text(
                "Compte à créditer :",
                style: GoogleFonts.roboto(),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _cmpCredit,
                  cursorColor: secondaryColor7,
                  decoration: InputDecoration(
                    fillColor: secondaryColor7,
                    focusColor: secondaryColor7,
                    hoverColor: secondaryColor7,
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: 'KAMAL 877464675892981',
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.035,
              ),
              Text(
                "Type de la remise :",
                style: GoogleFonts.roboto(),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _typeRemise,
                  cursorColor: secondaryColor7,
                  decoration: InputDecoration(
                    fillColor: secondaryColor7,
                    focusColor: secondaryColor7,
                    hoverColor: secondaryColor7,
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: 'Remise à l’encaissement',
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.035,
              ),
              Text(
                "Montant :",
                style: GoogleFonts.roboto(),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextFormField(
                  controller: _montant,
                  cursorColor: secondaryColor7,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    fillColor: secondaryColor7,
                    focusColor: secondaryColor7,
                    hoverColor: secondaryColor7,
                    errorStyle: TextStyle(
                      color: Theme.of(context).errorColor, // or any other color
                    ),
                    hintText: '0 CFA',
                    hintStyle: GoogleFonts.roboto(
                        textStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
                    border: InputBorder.none,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.035,
              ),
              Container(
                height: 40,
                child: RaisedButton(
                  onPressed: () {
                    if (_numCheque.text.isNotEmpty &&
                        _typeRemise.text.isNotEmpty &&
                        _montant.text.isNotEmpty) {
                      confirmRemise();
                    } else {
                      errorAlert(context, "Merci de remplir tous les champs");
                    }
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  child: Text("Suivant",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ))),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  confirmRemise() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          // clipBehavior: Clip.antiAliasWithSaveLayer,
          insetPadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "récapitulatif".toUpperCase(),
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
                // width: MediaQuery.of(context).size.width * 0.55,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: Text("N° de chèque :",
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 13,
                                fontWeight: FontWeight.bold))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(_numCheque.text ?? "",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                        alignment: Alignment.centerLeft,
                        fit: BoxFit.scaleDown,
                        child: Text("Compte à créditer: ",
                            maxLines: 1,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold)))),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(_cmpCredit.text ?? "",
                          maxLines: 2,
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("Date d’execution:  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(
                          "${DateFormat('yyyy-MM-dd').format(DateTime.now())}",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.w600))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: FittedBox(
                      alignment: Alignment.centerLeft,
                      fit: BoxFit.scaleDown,
                      child: Text("Montant:  ",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold))),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: FittedBox(
                      alignment: Alignment.centerRight,
                      fit: BoxFit.scaleDown,
                      child: Text(_montant.text ?? "",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.bold))),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text("Annuler",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 16))),
                        )),
                  ),
                  Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.28,
                    child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          Navigator.pop(context);
                          signeRemise();
                        },
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text("Confirmer",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ))),
                        )),
                  ),
                ],
              ),
              Container(height: MediaQuery.of(context).size.height * 0.01)
            ],
          ),
        );
      },
    );
  }

  signeRemise() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "CODE DE VALIDATION".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formPsdKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.9,
                ),
                Text(
                    "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return "Mot de passe ne peut pas etre vide";
                        return null;
                      },
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {},
                          color: Colors.white,
                          child: Text("Annuler",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 17,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        onPressed: () {
                          Navigator.pop(context);
                          okAlert(context,
                              "La remise de chèque a bien été prise en charge");
                        },
                        child: Text("Valider",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                              fontSize: 17,
                              color: Colors.white,
                            ))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
