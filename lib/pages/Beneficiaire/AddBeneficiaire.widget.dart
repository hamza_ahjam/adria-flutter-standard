import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/pages/virements/liste-virement.page.dart';
import 'package:LBA/pages/virements/virement_navigation.widget.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddBeneficiaireWidget extends StatelessWidget {
  // BeneficiaireState beneficiaireState;
  // TokenState tokenState;
  bool isComingFromMenu = false;
  AddBeneficiaireWidget({this.isComingFromMenu}) {
    // beneficiaireState = Provider.of<BeneficiaireState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
  }

//   @override
//   _AddBeneficiaireWidgetState createState() => _AddBeneficiaireWidgetState();
// }

// class _AddBeneficiaireWidgetState extends State<AddBeneficiaireWidget> {
  GlobalKey<FormState> _formKeybenef = GlobalKey<FormState>();
  GlobalKey<FormState> _formKeyPass = GlobalKey<FormState>();
  String dropdownValue;
  var nomBenif = TextEditingController();
  var rib = TextEditingController();
  var passwordController = TextEditingController();
  bool load = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, isComingFromMenu);
        return Future.value(true);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 8, 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width * 0.38,
                child: RaisedButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor)),
                    onPressed: () {
                      // Navigator.pop(context);
                      isComingFromMenu
                          ? Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DashboardComptePage()))
                          : Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  type: PageTransitionType.topToBottom,
                                  child: VirementNavigationPage()));
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => DashboardComptePage()));
                    },
                    color: Colors.white,
                    child: Text(AppLocalizations.of(context).annuler,
                        style: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 16))),
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width * 0.38,
                child: RaisedButton(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      if (_formKeybenef.currentState.validate()) {
                        confirmBenef(nomBenif.text, rib.text, context);
                      }
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text(AppLocalizations.of(context).valider,
                        style: TextStyle(color: Colors.white, fontSize: 16))),
              )
            ],
          ),
        ),
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).ajout_benef.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () => isComingFromMenu
                  ? Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardComptePage()))
                  : Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VirementNavigationPage()))),
        ),
        backgroundColor: Colors.white,
        body: Center(
          child: Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width * 0.85,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 25, bottom: 30),
                    child: Text(
                        AppLocalizations.of(context).saisir_infos_benef,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: GlobalParams
                                    .themes["$banque_id"].appBarColor))),
                  ),
                  Form(
                    key: _formKeybenef,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                          child: TextFormField(
                            key: Key("nomBenef"),
                            controller: nomBenif,
                            onSaved: (v) {
                              // setState(() => {});
                            },
                            validator: (v) {
                              if (v.isEmpty)
                                return AppLocalizations.of(context).error_nom_benef;
                              return null;
                            },
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context).nom_prenom,
                              // hintText: 'Nom du bénéficiaire',
                              hintStyle: TextStyle(
                                  fontSize: 15, fontFamily: KprimaryFont),
                              prefixIcon: Icon(Icons.person_add,
                                  color: GlobalParams
                                      .themes["$banque_id"].iconsColor),
                            ),
                          ),
                        ),
                        TextFormField(
                          controller: rib,
                          onSaved: (v) {
                            // setState(() => {});
                          },
                          keyboardType: TextInputType.number,
                          validator: (v) {
                            if (v.length == 24)
                              return null;
                            else
                              return AppLocalizations.of(context).error_rib_benef;
                          },
                          decoration: InputDecoration(
                            labelText: AppLocalizations.of(context).rib,
                            // hintText: 'RIB',
                            hintStyle: TextStyle(
                                fontSize: 15, fontFamily: KprimaryFont),
                            prefixIcon: Icon(
                                Icons.account_balance_wallet_rounded,
                                color: GlobalParams
                                    .themes["$banque_id"].iconsColor),
                          ),
                        ),
                      ],
                    ),
                  ),
                  BlocListener<BeneficiaireBloc, BeneficiaireStateBloc>(
                    child: Container(),
                    listener: (context, state) {
                      if (state.requestStateAdd == StateStatus.LOADING) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          showLoading(context);
                        });
                      } else if (state.requestStateAdd == StateStatus.LOADED) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          saveBenef(nomBenif.text, rib.text,
                              state.res["taskId"], state.res["id"], context);
                        });
                      } else if (state.requestStateAdd == StateStatus.ERROR) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          errorAlert(context, state.errorMessage);
                        });
                      } else if (state.requestStateAdd == StateStatus.NONE) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          return Container();
                        });
                      }
                      //
                      else if (state.requestStateSigne == StateStatus.NONE) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          return Container();
                        });
                      } else if (state.requestStateSigne ==
                          StateStatus.LOADING) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          showLoading(context);
                        });
                      } else if (state.requestStateSigne ==
                          StateStatus.LOADED) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          // signeBenefSuccess(nomBenif.text, rib.text, context);
                          okAlert(context,
                              "Votre opération à été effectuée avec succès");
                          nomBenif.text = "";
                          rib.text = "";
                          passwordController.text = "";
                          // "votre virement à été envoyé avec succès"
                          //
                          context
                              .read<BeneficiaireBloc>()
                              .add(LoadBeneficiaire());
                        });
                      } else if (state.requestStateSigne == StateStatus.ERROR) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          errorAlert(
                              context,
                              state.errorMessage == null
                                  ? "une erreur est survenue."
                                  : state.errorMessage);
                          passwordController.text = "";
                        });
                      }
                      //
                      else if (state.requestStateAbon == StateStatus.LOADING) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          // Navigator.pop(context);
                          showLoading(context);
                        });
                      } else if (state.requestStateAbon == StateStatus.LOADED) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          okAlert(context, AppLocalizations.of(context).transaction_abandonne);
                        });
                      } else if (state.requestStateAbon == StateStatus.ERROR) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          errorAlert(
                              context,
                              state.errorMessage == null
                                  ? "une erreur est survenue."
                                  : state.errorMessage);
                        });
                      } else {
                        return Container();
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  confirmBenef(nom, rib, context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    AppLocalizations.of(context).recap,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
          content: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            //height: MediaQuery.of(context).size.height * 0.4,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Padding(
                  //   padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                  //   child: ListTile(
                  //     visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                  //     title: Text("Vous devez confirmer votre  ",
                  //         textAlign: TextAlign.center,
                  //         style: GoogleFonts.roboto(
                  //             textStyle: TextStyle(
                  //                 fontSize: 15, fontWeight: FontWeight.bold))),
                  //     subtitle: Text("opération",
                  //         textAlign: TextAlign.center,
                  //         style: GoogleFonts.roboto(
                  //             textStyle: TextStyle(
                  //                 color: Colors.black,
                  //                 fontSize: 15,
                  //                 fontWeight: FontWeight.bold))),
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: MediaQuery.of(context).size.height * 0.03,
                  // ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Text("${AppLocalizations.of(context).nom_benef} :",
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: FittedBox(
                          alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Text("$nom",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.1,
                        child: Text("${AppLocalizations.of(context).rib_benef} :",
                            maxLines: 1,
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold))),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: FittedBox(
                          alignment: MyApp.of(context).getLocale().languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Text("$rib",
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: GlobalParams.themes["$banque_id"]
                                        .intituleCmpColor)),
                            onPressed: () {
                              Navigator.pop(context);
                              // abondonBenef(context);
                            },
                            color: Colors.white,
                            child: FittedBox(
                              alignment: Alignment.center,
                              fit: BoxFit.scaleDown,
                              child: Text(AppLocalizations.of(context).annuler,
                                  style: TextStyle(
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor,
                                      fontSize: 17)),
                            )),
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: RaisedButton(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          onPressed: () {
                            // Navigator.pop(context);

                            context.read<BeneficiaireBloc>().add(
                                AddSaveBeneficiaireEvent(nom: nom, rib: rib));
                          },
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.scaleDown,
                            child: Text(AppLocalizations.of(context).confirmer,
                                style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  saveBenef(nom, rib, taskId, id, context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          titlePadding: EdgeInsets.zero,
          title: Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SvgPicture.asset(
                    "assets/$banque_id/images/otp.svg",
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                  ),
                ),
                Text(
                  "CODE DE VALIDATION".toUpperCase(),
                  style: GoogleFonts.roboto(
                      textStyle: TextStyle(
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          content: Form(
            key: _formKeyPass,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
                Text(
                    "Veuillez saisir le code secret reçu par sms pour valider cette opération",
                    textAlign: TextAlign.center,
                    style:
                        GoogleFonts.roboto(textStyle: TextStyle(fontSize: 17))),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.045),
                  child: Container(
                    height: 50,
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.number,
                      validator: (v) {
                        if (v.isEmpty)
                          return "Mot de passe ne peut pas etre vide";
                        return null;
                      },

                      // keyboardAppearance: Brightness.dark,
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '___   ___   ___   ___   ___   ___',
                        hintStyle: TextStyle(fontSize: 16, color: Colors.black),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor)),
                          onPressed: () {
                            // Navigator.pop(context);
                            abondonBenef(context);
                          },
                          color: Colors.white,
                          child: Text(AppLocalizations.of(context).annuler,
                              style: TextStyle(
                                  fontSize: 17,
                                  color: GlobalParams
                                      .themes["$banque_id"].intituleCmpColor))),
                    ),
                    Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: GlobalParams
                              .themes["$banque_id"].intituleCmpColor,
                          onPressed: () {
                            // Navigator.pop(context);
                            if (_formKeyPass.currentState.validate()) {
                              context.read<BeneficiaireBloc>().add(
                                  AddSigneBeneficiaireEvent(
                                      password: passwordController.text));
                            }
                          },
                          child: Text(AppLocalizations.of(context).valider,
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.white,
                              ))),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  signeBenefSuccess(nom, rib, context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.4,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset("assets/$banque_id/images/ok.png"),
                  Padding(
                    padding: const EdgeInsets.only(top: 25, bottom: 25),
                    child: Text("Votre opération à été effectuée avec succès",
                        style: TextStyle(fontSize: 16)),
                  ),
                  ListTile(
                    title: Text("Nom béneficiaire:",
                        style: TextStyle(fontSize: 14)),
                    trailing: Text("$nom", style: TextStyle(fontSize: 14)),
                  ),

                  ListTile(
                    title: Text("RIB:", style: TextStyle(fontSize: 14)),
                    trailing: Text("$rib", style: TextStyle(fontSize: 14)),
                  ),
                  // Text("Nom béneficiaire:  $nom",
                  //     style: GoogleFonts.roboto(
                  //         textStyle: TextStyle(fontSize: 14))),
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 30, top: 15),
                  //   child: Text("RIB:  $rib",
                  //       style: GoogleFonts.roboto(
                  //           textStyle: TextStyle(fontSize: 14))),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Center(
                      child: Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: Colors.white,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ListeVirementPage()));
                              },
                              child: Text("Fermer",
                                  style: TextStyle(
                                      color: GlobalParams
                                          .themes["$banque_id"].appBarColor,
                                      fontSize: 16)),
                            )),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  abondonBenef(context) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 20),
                child: Text(
                  "Voulez-vous annuler la transaction en cours",
                  style: GoogleFonts.roboto(textStyle: TextStyle(fontSize: 18)),
                ),
              ),
              ListTile(
                leading: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    child: Text("Non",
                        style: TextStyle(color: Colors.white, fontSize: 16))),
                trailing: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    onPressed: () {
                      // Navigator.pop(context);
                      Navigator.pop(context);
                      nomBenif.text = "";
                      rib.text = "";

                      context.read<BeneficiaireBloc>().add(AbandonerBenef());
                    },
                    color: Colors.white,
                    child: Text("Oui",
                        style: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontSize: 16))),
              )
            ],
          ),
        );
      },
    );
  }
}
