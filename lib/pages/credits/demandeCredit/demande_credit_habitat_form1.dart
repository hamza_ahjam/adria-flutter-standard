import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/credits/credit.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/CreditHabitatForm.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';
import 'credits_step_two_page.dart';

class DemandeCreditHabitatForm1Page extends StatefulWidget {
  DemandeCreditHabitatForm1Page({Key key}) : super(key: key);

  @override
  State<DemandeCreditHabitatForm1Page> createState() => _DemandeCreditHabitatForm1PageState();
}

class _DemandeCreditHabitatForm1PageState extends State<DemandeCreditHabitatForm1Page> {
  bool isLoaded = false;
  var selectedAccount;

  DateTime dateDemande = DateTime.now();

  final _formPsdKey = GlobalKey<FormState>();

  List<String> items = [];

  List<ListItems> childItems = [];
  List<ListItems> filtredChildItems = [];
  List<ListItems> catItems = [];

  Map<ListItems,List<ListItems>> mapItemsList = {};

  Map<String, String> dropValue = {};
  Map<String, TextEditingController> controllers = {};
  Map<String, DateTime> datesDemande = {};

  DateFormat _dateFormat = DateFormat.yMd('fr_FR');

  var compteDebiterController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: 'Demander un crédit à l\'habitat',
        context: context,
        isPop: true,
      ),
      body: ListView(
        children: [
          BlocBuilder<CreditBloc, CreditStateBloc>(
            builder: (context, state) {
              if (state.requestState == StateStatus.NONE) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<CreditBloc>().add(LoadCreditHabitatFormEvent(
                      product: "177219876", step: "1"));
              });
                return Container();
              } else if (state.requestStateForm == StateStatus.LOADING) {
                return Center(
                    heightFactor: 15,
                    child: CircularProgressIndicator(
                      backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          GlobalParams.themes["$banque_id"].appBarColor),
                    ));
              } else if (state.requestStateForm == StateStatus.ERROR) {
                return ErreurTextWidget(
                  errorMessage: state.errorMessage,
                  actionEvent: () {
                    context.read<CreditBloc>().add(state.currentAction);
                  },
                );
              } else if (state.requestStateForm == StateStatus.LOADED) {
                isLoaded = true;
                if (state.formItems.map.list.length == 0)
                  return Center(
                    heightFactor: 20,
                    child: Text(
                      AppLocalizations.of(context).aucun_element,
                      style: GoogleFonts.roboto(
                          textStyle:
                          TextStyle(fontSize: 16, color: principaleColor5)),
                    ),
                  );
                else {
                  mapItemsList = {};
                  childItems = [];
                  catItems = [];
                  state.formItems.map.list.forEach((item) {
                  if (item.type == "category") {
                    catItems.add(item);
                    // items.addAll(item.child.split(', '));
                  } else {
                    childItems.add(item);
                  }
                  });
                  catItems.forEach((catItem) {
                    print('cat --------------------------------------');
                    filtredChildItems = [];
                    childItems.forEach((childItem) {
                      if (catItem.child.contains(childItem.code))
                        filtredChildItems.add(childItem);
                      print(filtredChildItems.length);
                    });
                    mapItemsList.addAll({catItem: filtredChildItems});
                  });
                  print(mapItemsList.length);

                return Form(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: mapItemsList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15),
                                    child: Text(
                                      '${mapItemsList.keys.toList()[index].labelle}',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes[banque_id].intituleCmpColor
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.01,
                                  ),
                                  ListView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      itemCount: mapItemsList
                                          .values
                                          .toList()[index]
                                          .length,
                                      itemBuilder: (BuildContext context,
                                          int childIndex) {
                                        // dropValue["${mapItemsList[index].values.toList()[0][childIndex].code}"] = '';
                                        return Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(top: 5),
                                              child: Text(
                                                '${mapItemsList.values.toList()[index][childIndex].labelle}',
                                                style: TextStyle(
                                                  color: GlobalParams.themes[banque_id].appBarColor,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height * 0.01,
                                            ),
                                            if ((mapItemsList.values.toList()[index][childIndex].type == 'text' && mapItemsList.values.toList()[index][childIndex].code != 'employeur') || mapItemsList.values.toList()[index][childIndex].type == 'number')
                                              Container(
                                                height: MediaQuery.of(context).size.height * 0.05,
                                                decoration: BoxDecoration(
                                                    border: Border.all(width: 2, color: Colors.grey[200]),
                                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                                child: TextFormField(
                                                  controller: controllers['${mapItemsList.values.toList()[index][childIndex].code}'],
                                                  keyboardType: mapItemsList.values.toList()[index][childIndex].type == 'text' ? TextInputType.text : TextInputType.number,
                                                  textAlignVertical: TextAlignVertical.center,
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                        color: Colors.black,
                                                      )),
                                                  onChanged: (v) {},
                                                  decoration: InputDecoration(
                                                    contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                                    hintText: '${mapItemsList.values.toList()[index][childIndex].labelle}',
                                                    border: InputBorder.none,
                                                    hintStyle:
                                                    TextStyle(color: Colors.grey[600], fontSize: 14),
                                                  ),
                                                ),
                                              ),
                                            if (mapItemsList.values.toList()[index][childIndex].type == 'text' && mapItemsList.values.toList()[index][childIndex].code == 'employeur')
                                              Container(
                                                height: MediaQuery.of(context).size.height * 0.05,
                                                decoration: BoxDecoration(
                                                    border: Border.all(width: 2, color: Colors.grey[200]),
                                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                                child: Autocomplete<Data>(
                                                  fieldViewBuilder: (
                                                      BuildContext context,
                                                      TextEditingController fieldTextEditingController,
                                                      FocusNode fieldFocusNode,
                                                      VoidCallback onFieldSubmitted
                                                      ) {
                                                    controllers['${mapItemsList.values.toList()[index][childIndex].code}'] = fieldTextEditingController;
                                                    return TextFormField(
                                                      controller: fieldTextEditingController,
                                                      focusNode: fieldFocusNode,
                                                      style: GoogleFonts.roboto(
                                                          textStyle: TextStyle(
                                                            color: Colors.black,
                                                          )),
                                                      decoration: InputDecoration(
                                                        contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                                        hintText: '${mapItemsList.values.toList()[index][childIndex].labelle}',
                                                        border: InputBorder.none,
                                                        hintStyle:
                                                        TextStyle(color: Colors.grey[600], fontSize: 14),
                                                      ),
                                                    );
                                                  },
                                                    onSelected: (Data option){
                                                      print(option.libelle);
                                                    },
                                                    displayStringForOption: (Data option) => option.libelle,
                                                    optionsBuilder: (TextEditingValue textEditingValue){
                                                      if (textEditingValue.text == '') {
                                                        return const Iterable<Data>.empty();
                                                      }
                                                      return mapItemsList.values.toList()[index][1].data.where((Data option)
                                                         => option.libelle.toLowerCase().contains(textEditingValue.text.toLowerCase())
                                                      ).toList();
                                                    },

                                                ),
                                              ),
                                            if (mapItemsList.values.toList()[index][childIndex].type == 'date')
                                                Container(
                                                  height: MediaQuery.of(context).size.height * 0.05,
                                                  decoration: BoxDecoration(
                                                      border: Border.all(width: 2, color: Colors.grey[200]),
                                                      borderRadius: BorderRadius.all(Radius.circular(10))),
                                                  child: GestureDetector(
                                                    onTap: () async {
                                                      var res =
                                                      await selectDate(context, datesDemande['${mapItemsList.values.toList()[index][childIndex].code}'], '${mapItemsList.values.toList()[index][childIndex].code}');
                                                      setState(() {
                                                        datesDemande['${mapItemsList.values.toList()[index][childIndex].code}'] = res;
                                                      });
                                                    },
                                                    child: TextFormField(
                                                      controller: controllers['${mapItemsList.values.toList()[index][childIndex].code}'],
                                                      textAlignVertical: TextAlignVertical.center,
                                                      enabled: false,
                                                      style: GoogleFonts.roboto(
                                                          textStyle: TextStyle(
                                                            color: Colors.black,
                                                          )),
                                                      onChanged: (v) {},
                                                      decoration: InputDecoration(
                                                        suffixIcon: Icon(
                                                          Icons.calendar_today,
                                                          color: GlobalParams.themes[banque_id].appBarColor,
                                                        ),
                                                        contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                                        hintText: '${mapItemsList.values.toList()[index][childIndex].labelle}',
                                                        border: InputBorder.none,
                                                        hintStyle:
                                                        TextStyle(color: Colors.grey[600], fontSize: 14),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                            if (mapItemsList.values.toList()[index][childIndex].type == 'select')
                                              Container(
                                                height: MediaQuery.of(context).size.height * 0.05,
                                                padding: EdgeInsets.only(left: 10, right: 10),
                                                decoration: BoxDecoration(
                                                    border: Border.all(width: 2, color: Colors.grey[200]),
                                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                                child: DropdownButtonHideUnderline(
                                                  child: DropdownButton(
                                                    icon: Icon(
                                                      Icons.keyboard_arrow_down,
                                                      color: GlobalParams.themes[banque_id].appBarColor,
                                                    ),
                                                    isExpanded: true,
                                                    hint: Text(
                                                      "${mapItemsList.values.toList()[index][childIndex].labelle}",
                                                      style: TextStyle(
                                                          fontSize: 14
                                                      ),
                                                    ),
                                                    style: GoogleFonts.roboto(
                                                        textStyle:
                                                        TextStyle(color: Colors.black, fontSize: 16)),
                                                    value: dropValue["${mapItemsList.values.toList()[index][childIndex].code}"],
                                                    items: mapItemsList.values.toList()[index][childIndex].data
                                                        .map((val) {
                                                      return DropdownMenuItem(
                                                        value: val.identifiant,
                                                        child: Text(val.libelle),
                                                      );
                                                    }).toList(),
                                                    onChanged: (value) {
                                                       setState(() {
                                                          dropValue["${mapItemsList.values.toList()[index][childIndex].code}"] = value;
                                                        });
                                                      },
                                                  ),
                                                ),
                                              ),
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height * 0.01,
                                            ),
                                          ],
                                        );
                                      })
                                ],
                              );
                            },
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 5, bottom:  MediaQuery.of(context).size.height * 0.01,),
                              child: Text(
                                'Compte à débiter',
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                WidgetsBinding.instance.addPostFrameCallback((_) {
                                  if (!isLoaded)
                                    context.read<CompteBloc>().add(
                                        LoadComptesEvent());
                                });
                                showCompteDialog(context);
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.05,
                                decoration: BoxDecoration(
                                    border: Border.all(width: 2, color: Colors.grey[200]),
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: TextFormField(
                                  controller: compteDebiterController,
                                  enabled: false,
                                  textAlignVertical: TextAlignVertical.center,
                                  style: GoogleFonts.roboto(
                                      textStyle: TextStyle(
                                        color: Colors.black,
                                      )),
                                  onChanged: (v) {},
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.keyboard_arrow_down,
                                      color: GlobalParams.themes[banque_id].appBarColor,
                                    ),
                                    contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                    hintText: 'Compte à débiter',
                                    border: InputBorder.none,
                                    hintStyle:
                                    TextStyle(color: Colors.grey[600], fontSize: 14),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            MaterialButton(
                                minWidth: double.infinity,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CreditsStepTwoPage()));
                                },
                                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                                child: Text("Suivant",
                                    style: GoogleFonts.roboto(
                                        textStyle:
                                        TextStyle(color: Colors.white, fontSize: 18)))),
                          ],
                        ),
                      ),
                    ),
                  );
                }
              } else {
                return Container();
              }
            },
          )
        ]
      ),
    );
  }

  selectDate(BuildContext context, updatedDate, code) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("fr", "FR"),
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2220),
    );
    if (picked != null && picked != updatedDate) {
      setState(() {
        updatedDate = picked;
        controllers['$code'] = TextEditingController();
        controllers['$code'].text = _dateFormat.format(updatedDate);
      });
    }
    return updatedDate;
  }

  showCompteDialog(context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius:
                    BorderRadius.only(topLeft: Radius.circular(20))),
                padding: EdgeInsets.only(top: 15),
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.06,
                child: Text("COMPTE À DÉBITER",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                            color: GlobalParams
                                .themes["$banque_id"].intituleCmpColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18))),
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    BlocBuilder<CompteBloc, CompteStateBloc>(
                      builder: (context, state) {
                        if (state.requestState ==
                            StateStatus.LOADING) {
                          return Center(
                              heightFactor: 10,
                              child: CircularProgressIndicator(
                                backgroundColor: GlobalParams
                                    .themes["$banque_id"].intituleCmpColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    GlobalParams
                                        .themes["$banque_id"].appBarColor),
                              ));
                        } else if (state.requestState ==
                            StateStatus.ERROR) {
                          return ErreurTextWidget(
                            errorMessage: state.errorMessage,
                            actionEvent: () {
                              context
                                  .read<CompteBloc>()
                                  .add(state.currentAction);
                            },
                          );
                        } else if (state.requestState ==
                            StateStatus.LOADED) {
                          isLoaded = true;
                          // if (benif.runtimeType == CompteInstanceList) {
                          //   filtredListComptes = filtredListComptes
                          //       .where((element) => element != benif)
                          //       .toList();
                          // }
                          if (state.clients.comptes.length ==
                              0)
                            return Center(
                              child: Text(
                                AppLocalizations.of(context).aucun_compte_transaction,
                                style: GoogleFonts.roboto(
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        color: GlobalParams.themes["$banque_id"]
                                            .validationButtonColor)),
                              ),
                            );
                          else
                            return ListView.separated(
                              shrinkWrap: true,
                              itemCount: state.clients.comptes.length,
                              separatorBuilder: (context, index) => Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Divider(
                                      color: Colors.black26,
                                      height: 15,
                                    ),
                                  )
                                ],
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    // compte["Map"]["compteInstanceList"]
                                    //     [index]["intitule"];
                                    setState(() {
                                      selectedAccount =
                                      state.clients.comptes[index];
                                      compteDebiterController.text = "${selectedAccount.intitule} :  ${selectedAccount.soldeTempsReel}";
                                    });
                                    // });
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    // color: (index % 2) != 0
                                    //     ? Colors.grey[100]
                                    //     : Colors.white,
                                    padding: EdgeInsets.all(8),
                                    // height:
                                    //     MediaQuery.of(context).size.height * 0.1,
                                    child: ListTile(
                                      visualDensity: VisualDensity(
                                          horizontal: -4, vertical: -4),
                                      title: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .intitule,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      subtitle: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .identifiantInterne,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                      trailing: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 0, 0, 0),
                                        child: Text(
                                            state
                                                .clients
                                                .comptes[index]
                                                .soldeTempsReel,
                                            style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                  fontSize: 15,
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .appBarColor),
                                            )),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }
}
