import 'package:LBA/config/global.params.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';

class CreditRequestRecap extends StatefulWidget {
  @override
  State<CreditRequestRecap> createState() => _CreditRequestRecapState();
}

class _CreditRequestRecapState extends State<CreditRequestRecap> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "Demander un crédit à l\'habitat".toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()));
            }),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                "Récapitulatif de la simulation",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              color: Colors.black45,
            ),
          ),

          //first row
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              color: Colors.grey,
              child: Row(
                children: [
                  Expanded(
                      child: Text("Durée du crédit",
                          style: TextStyle(fontSize: 17))),
                  Expanded(
                      child: Text("24",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 17)))
                ],
              ),
            ),
          ),

          //second row
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              color: Colors.grey,
              child: Row(
                children: [
                  Expanded(
                      child: Text("Montant du crédit",
                          style: TextStyle(fontSize: 17))),
                  Expanded(
                      child: Text("0",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 17)))
                ],
              ),
            ),
          ),

          //third row
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              color: Colors.grey,
              child: Row(
                children: [
                  Expanded(child: Text("Taux", style: TextStyle(fontSize: 17))),
                  Expanded(
                      child: Text("6.5",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 17)))
                ],
              ),
            ),
          ),

          //fourth row
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              color: Colors.grey,
              child: Row(
                children: [
                  Expanded(
                      child:
                          Text("Mensualité", style: TextStyle(fontSize: 17))),
                  Expanded(
                      child: Text("0",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 17)))
                ],
              ),
            ),
          ),

          //fifth row
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.05,
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              color: Colors.grey,
              child: Row(
                children: [
                  Expanded(
                      child: Text("Type de prèlèvement",
                          style: TextStyle(fontSize: 17))),
                  Expanded(
                      child: Text("Prèlèvement bancaire",
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 17)))
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(left:20, right:20, top: 10),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                            if (states.contains(MaterialState.pressed))
                              return Colors.red;
                            return Colors.red; // Use the component's default.
                          },
                        ),
                      ),
                      onPressed: () {  },
                      child: Text("Annuler la demande",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 16
                                  )))),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left:10),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed))
                                return Colors.green;
                              return Colors.green; // Use the component's default.
                            },
                          ),
                        ),
                        onPressed: () {  },
                        child: Text("Créer la demande",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 16)))),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
