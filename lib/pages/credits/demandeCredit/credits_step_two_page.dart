import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/credits/credit.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/credits_step_two.dart';
import 'package:LBA/pages/credits/demandeCredit/credit_request_recap.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';

class CreditsStepTwoPage extends StatefulWidget {
  @override
  State<CreditsStepTwoPage> createState() => _CreditsStepTwoPageState();
}

class _CreditsStepTwoPageState extends State<CreditsStepTwoPage> {
  bool isLoaded = false;
  Map<String, int> select = new Map();
  Map<String, double> sliders = new Map();
  Map<String, String> textFields = new Map();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              "Demander un crédit à l\'habitat".toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () {
                // Navigator.pop(context);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardComptePage()));
              }),
        ),
        body: ListView(
          children: [
            BlocBuilder<CreditBloc, CreditStateBloc>(builder: (context, state) {
              print(state.requestStepTwoForm);
              print(state.requestStepTwoForm);
              if (state.requestStepTwoForm == StateStatus.NONE) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  if (!isLoaded)
                    context.read<CreditBloc>().add(LoadCreditStepTwo());
                });
                return Container();
              } else if (state.requestStepTwoForm == StateStatus.LOADING) {
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor:
                      GlobalParams.themes["$banque_id"].intituleCmpColor,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      GlobalParams.themes["$banque_id"].appBarColor),
                ));
              } else if (state.requestStepTwoForm == StateStatus.ERROR) {
                return ErreurTextWidget(
                  errorMessage: state.errorMessage,
                  actionEvent: () {
                    context.read<CreditBloc>().add(state.currentAction);
                  },
                );
              } else if (state.requestStepTwoForm == StateStatus.LOADED) {
                isLoaded = true;
                if (state.creditsStepTwo.map.list.length == 0)
                  return Center(
                    child: Text(
                      AppLocalizations.of(context).aucun_form,
                      style: GoogleFonts.roboto(
                          textStyle:
                              TextStyle(fontSize: 16, color: principaleColor5)),
                    ),
                  );
                else {
                  Map<CreditItem, dynamic> formItems =
                      new Map<CreditItem, dynamic>();

                  for (int i = 0;
                      i < state.creditsStepTwo.map.list.length;
                      i++) {
                    if (state.creditsStepTwo.map.list[i].type
                        .contains("category")) {
                      var childs =
                          state.creditsStepTwo.map.list[i].child.split(",");
                      var childsItems = <CreditItem>[];
                      for (int j = 0;
                          j < state.creditsStepTwo.map.list.length;
                          j++) {
                        if (state.creditsStepTwo.map.list[i].child
                            .contains(state.creditsStepTwo.map.list[j].code)) {
                          childsItems.add(state.creditsStepTwo.map.list[j]);
                          print(
                              "child: ${state.creditsStepTwo.map.list[j].code}");
                        }
                      }
                      print(
                          "category: " + state.creditsStepTwo.map.list[i].code);
                      formItems.putIfAbsent(
                          state.creditsStepTwo.map.list[i], () => childsItems);
                    }
                  }
                  return SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        children: [
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: state.creditsStepTwo.map.list.length,
                              itemBuilder: (_, index) {
                                if (state.creditsStepTwo.map.list[index].type
                                    .contains("select")) {
                                  //return Text(state.creditsStepTwo.map.list[index].labelle);
                                  return getDropDown2(
                                      state.creditsStepTwo.map.list[index]
                                          .labelle,
                                      state
                                          .creditsStepTwo.map.list[index].data);
                                }

                                if (state.creditsStepTwo.map.list[index].type
                                    .contains("text")) {
                                  textFields.putIfAbsent(
                                      state.creditsStepTwo.map.list[index]
                                          .labelle,
                                      () => "");
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20, top:10),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.05,
                                      decoration: BoxDecoration(
                                          border: Border.all(width: 2, color: Colors.grey[200]),
                                          borderRadius: BorderRadius.all(Radius.circular(10))),
                                      child: TextField(
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                            hintText: state.creditsStepTwo.map
                                                .list[index].labelle,
                                            border: InputBorder.none,
                                            hintStyle:
                                            TextStyle(color: Colors.grey[600], fontSize: 14),
                                          ),
                                          textAlignVertical: TextAlignVertical.center,
                                          style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                color: GlobalParams.themes[banque_id].appBarColor,
                                              )),
                                          onChanged: (text) {
                                            textFields[state.creditsStepTwo.map
                                                .list[index].labelle] = text;
                                          }),
                                    ),
                                  );
                                }

                                if (state.creditsStepTwo.map.list[index].type
                                    .contains("textarea")) {
                                  textFields.putIfAbsent(
                                      state.creditsStepTwo.map.list[index]
                                          .labelle,
                                      () => "");
                                  return Padding(
                                    padding:
                                        EdgeInsets.only(left: 20, right: 20),
                                    child: TextField(
                                        decoration: InputDecoration(
                                            border: UnderlineInputBorder(),
                                            labelText: state.creditsStepTwo.map
                                                .list[index].labelle),
                                        onChanged: (text) {
                                          textFields[state.creditsStepTwo.map
                                              .list[index].labelle] = text;
                                        }),
                                  );
                                }

                                if (state.creditsStepTwo.map.list[index].type
                                    .contains("number")) {
                                  textFields.putIfAbsent(
                                      state.creditsStepTwo.map.list[index]
                                          .labelle,
                                      () => "");
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20, top:10),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.05,
                                      decoration: BoxDecoration(
                                          border: Border.all(width: 2, color: Colors.grey[200]),
                                          borderRadius: BorderRadius.all(Radius.circular(10))),
                                      child: TextField(
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(left: 10, bottom: 15),
                                            hintText: state.creditsStepTwo.map
                                                .list[index].labelle,
                                            border: InputBorder.none,
                                            hintStyle:
                                            TextStyle(color: Colors.grey[600], fontSize: 14),
                                          ),
                                          textAlignVertical: TextAlignVertical.center,
                                          style: GoogleFonts.roboto(
                                              textStyle: TextStyle(
                                                color: GlobalParams.themes[banque_id].appBarColor,
                                              )),
                                        onChanged: (text) {
                                          textFields[state.creditsStepTwo.map
                                              .list[index].labelle] = text;
                                        },
                                        keyboardType: TextInputType.number,
                                      ),
                                    ),
                                  );
                                }

                                if (state.creditsStepTwo.map.list[index].type
                                    .contains("slider")) {
                                  sliders.putIfAbsent(
                                      state.creditsStepTwo.map.list[index]
                                          .labelle,
                                      () => 0);
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15, left: 20, right: 20),
                                        child: Text(state.creditsStepTwo.map
                                            .list[index].labelle),
                                      ),
                                      Container(
                                        height: 30,
                                        child: Slider(
                                          min: 0.0,
                                          max: 100.0,
                                          value: sliders[state.creditsStepTwo
                                              .map.list[index].labelle],
                                          onChanged: (value) {
                                            setState(() {
                                              sliders[state.creditsStepTwo.map
                                                  .list[index].labelle] = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  );
                                } else
                                  return Container();
                              }),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.7,
                              child: ElevatedButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CreditRequestRecap()));
                                  },
                                  child: Text("Suivant",
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18)))),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }
              } else {
                return Container();
              }
            }),
          ],
        ));
  }

  Widget getDropDown2(String label, List<Data> options) {
    select.putIfAbsent(label, () => 0);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, top: 10),
          child: Text(
            label,
            style: TextStyle(
              color: GlobalParams.themes[banque_id].appBarColor,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.05,
            padding: EdgeInsets.only(left: 20, right: 20, top: 10),
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.grey[200]),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: GlobalParams.themes[banque_id].appBarColor,
                ),
                hint: Text(
                  label,
                  style: TextStyle(
                      fontSize: 14
                  ),
                ),
                style: GoogleFonts.roboto(
                    textStyle:
                    TextStyle(color: Colors.black, fontSize: 16)),
                isExpanded: true,
                value: select[label],
                items: options
                    .map((e) => DropdownMenuItem(
                          child: Text("${e.libelle}"),
                          value: options.indexOf(e),
                        ))
                    .toList(),
                onChanged: (int value) {
                  setState(() {
                    select[label] = value;
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
