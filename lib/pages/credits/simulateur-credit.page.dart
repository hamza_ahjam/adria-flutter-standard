

import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/pages/credits/widget/credit-consommation.widget.dart';
import 'package:LBA/pages/credits/widget/credit-habitat.widget.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';

class SimulateurCreditPage extends StatefulWidget {
  SimulateurCreditPage({Key key}) : super(key: key);

  @override
  _SimulateurCreditPageState createState() => _SimulateurCreditPageState();
}

class _SimulateurCreditPageState extends State<SimulateurCreditPage> {
  int _index;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          shape: ContinuousRectangleBorder(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(60))),
          toolbarHeight: MediaQuery.of(context).size.height * 0.085,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Text(
              AppLocalizations.of(context).sim_credit.toUpperCase(),
              style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
            ),
          ),
          centerTitle: true,
          backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()))),
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.08,
          decoration: BoxDecoration(
              color: Colors.grey[50],
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                // topRight: Radius.circular(20)
              ),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(91, 102, 112, 0.1),
                  offset: const Offset(
                    0.0,
                    0.0,
                  ),
                  blurRadius: 10,
                  spreadRadius: 3.0,
                ), //BoxShadow
                BoxShadow(
                  color: Color.fromRGBO(91, 102, 112, 0.1),
                  offset: const Offset(0.0, 0.0),
                  blurRadius: 10,
                  spreadRadius: 3.0,
                ),
              ]),
          child: TabBar(
              indicatorColor: Colors.transparent,
              labelColor: GlobalParams.themes["$banque_id"].intituleCmpColor,
              unselectedLabelColor:
                  GlobalParams.themes["$banque_id"].appBarColor,
              onTap: (v) {
                setState(() {
                  _index = v;
                });
              },
              tabs: [
                Tab(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Container(
                      child: Icon(Icons.home_filled),
                      alignment: MyApp.of(context).getLocale().languageCode == 'ar'
                          ? Alignment.centerRight
                          : Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 18, right: 18),
                    ),
                  ),
                ),
                Tab(
                    child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: Container(
                    child: SvgPicture.asset(
                        "assets/$banque_id/images/consommation.svg",
                        color: _index == 1
                            ? GlobalParams.themes["$banque_id"].intituleCmpColor
                            : GlobalParams.themes["$banque_id"].appBarColor),
                    alignment: MyApp.of(context).getLocale().languageCode == 'ar'
                        ? Alignment.centerLeft
                        : Alignment.centerRight,
                    padding: EdgeInsets.only(right: 18, left: 18),
                  ),
                )),
              ]),
        ),
        body: Stack(
          children: <Widget>[
            Container(
                child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Container(
                  child: CreditHabitatWidget(),
                ),
                Container(child: CreditConsommationWidget()),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
