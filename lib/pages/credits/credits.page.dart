import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/credits/credit.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

import '../../main.dart';

class CreditsPage extends StatelessWidget {
  // CompteState compteState;
  // TokenState tokenState;
  CreditsPage() {
    // compteState = Provider.of<CompteState>(context, listen: false);
    // tokenState = Provider.of<TokenState>(context, listen: false);
    // compteState.loadCredits(tokenState);
  }
  bool isLoaded = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
          appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            shape: ContinuousRectangleBorder(
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(60))),
            toolbarHeight: MediaQuery.of(context).size.height * 0.085,
            title: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                AppLocalizations.of(context).mes_credits.toUpperCase(),
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
            ),
            centerTitle: true,
            backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  // Navigator.pop(context);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardComptePage()));
                }),
          ),
          body: BlocBuilder<CreditBloc, CreditStateBloc>(
              builder: (context, state) {
            if (state.requestState == StateStatus.NONE) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (!isLoaded)
                  context.read<CreditBloc>().add(LoadCreditEvent());
              });
              return Container();
            } else if (state.requestState == StateStatus.LOADING) {
              return Center(
                  child: CircularProgressIndicator(
                backgroundColor:
                    GlobalParams.themes["$banque_id"].intituleCmpColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    GlobalParams.themes["$banque_id"].appBarColor),
              ));
            } else if (state.requestState == StateStatus.ERROR) {
              return ErreurTextWidget(
                errorMessage: state.errorMessage,
                actionEvent: () {
                  context.read<CreditBloc>().add(state.currentAction);
                },
              );
            } else if (state.requestState == StateStatus.LOADED) {
              isLoaded = true;
              if (state.listCredit.map.credits.length == 0)
                return Center(
                  child: Text(
                    AppLocalizations.of(context).aucun_credit,
                    style: GoogleFonts.roboto(
                        textStyle:
                            TextStyle(fontSize: 16, color: principaleColor5)),
                  ),
                );
              else
                return ListView.builder(
                    shrinkWrap: true,
                    itemCount: state.listCredit.map.credits.length,
                    itemBuilder: (_, index) {
                      // if (cs.historiqueChequier.map.liste.length == 0)
                      //   return Text("pas de mouvement");
                      var languageCode = MyApp.of(context).getLocale().languageCode;
                      return Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(30),
                                            topRight: Radius.circular(30))),
                                    child: ListTile(
                                      title: Text(
                                        AppLocalizations.of(context).num_compte.toUpperCase(),
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .appBarColor)),
                                      ),
                                      trailing: Text(
                                        "${state.listCredit.map.credits[index].numeroCompte}",
                                        style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16,
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .appBarColor)),
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).capital_emp.toUpperCase(),
                                        style: GoogleFonts.roboto()),
                                    trailing: Text("4 000 000",
                                        textDirection: TextDirection.ltr,
                                        // "${state.listCredit.map.credits[index].capitalEmprunte}",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).tx_credit.toUpperCase()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].tauxGlobalDuCredit} %",
                                        textDirection: TextDirection.ltr,
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).montant_interet.toUpperCase()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].echeanceAvecAssuranceFormatted ?? 1000} ",
                                        textDirection: TextDirection.ltr,
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(AppLocalizations.of(context).date_deb.toUpperCase()),
                                    trailing: Text(
                                        "${intl.DateFormat.yMMMMd(languageCode).format(intl.DateFormat("dd-MM-yyyy")
                                            .parse(state.listCredit.map.credits[index].dateFirstEcheanceFormatted))} ",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(AppLocalizations.of(context).date_fin.toUpperCase()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].dateFineFormatted != null
                                            ? intl.DateFormat.yMMMMd(languageCode).format(intl.DateFormat("dd-MM-yyyy")
                                            .parse(state.listCredit.map.credits[index].dateFineFormatted))
                                            : intl.DateFormat.yMMMMd(languageCode).format(intl.DateFormat("dd-MM-yyyy")
                                                .parse("09-07-2021"))} ",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).montant_rembourser.toUpperCase()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].montantDuPret ?? 10000} ",
                                        textDirection: TextDirection.ltr,
                                        style: GoogleFonts.roboto()),
                                  ),
                                  // ListTile(
                                  //   title: Text("Date prochaine échéance"),
                                  //   trailing: Text(
                                  //       "${DateFormat.yMMMMd('fr').format(DateFormat("dd-MM-yyyy").parse(state.listCredit.map.credits[index].dateProchaineEcheance))} ",
                                  //       style: GoogleFonts.roboto()),
                                  // ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).num_dossier.toUpperCase(),
                                        style: GoogleFonts.roboto()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].numeroDossier}",
                                        textDirection: TextDirection.ltr,
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(AppLocalizations.of(context).type_pret.toUpperCase(),
                                        style: GoogleFonts.roboto()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].typeDePret}",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(
                                        AppLocalizations.of(context).period_interert.toUpperCase(),
                                        style: GoogleFonts.roboto()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].periodiciteInteret}",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  ListTile(
                                    title: Text(AppLocalizations.of(context).duree_pret.toUpperCase()),
                                    trailing: Text(
                                        "${state.listCredit.map.credits[index].dureeDePret} ${AppLocalizations.of(context).mois}",
                                        style: GoogleFonts.roboto()),
                                  ),
                                  // Divider(
                                  //   color: Colors.black,
                                  // )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    });
            } else {
              return Container();
            }
          })),
    );
  }
}
