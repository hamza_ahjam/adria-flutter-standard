import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;
class CreditConsommationWidget extends StatefulWidget {
  const CreditConsommationWidget({Key key}) : super(key: key);

  @override
  _CreditConsommationWidgetState createState() =>
      _CreditConsommationWidgetState();
}

class _CreditConsommationWidgetState extends State<CreditConsommationWidget> {
  var tTaux = TextEditingController();
  var tDuree = TextEditingController();
  var tMontant = TextEditingController();
  double taux = 2;
  double duree = 0;
  double montant = 0;
  var formatter = intl.NumberFormat('#,###,000');

  bool eTaux = false;
  bool eDuree = false;
  bool eMontant = false;
  @override
  Widget build(BuildContext context) {
    double mensualite = (montant + (montant * (taux * 0.01))) / duree;

    if(mensualite.roundToDouble().isNaN || mensualite.isInfinite){
      mensualite = 0;
    }

    return Center(
      child: Container(
        padding: EdgeInsets.fromLTRB(5, 50, 5, 25),
        width: MediaQuery.of(context).size.width * 0.85,
        child: ListView(
          children: <Widget>[
            Text(AppLocalizations.of(context).credit_conso.toUpperCase(),
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: GlobalParams.themes["$banque_id"].appBarColor),
                )),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 20),
              child: Text(
                AppLocalizations.of(context).taux,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 20,
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor)),
              ),
            ),
            Slider(
                activeColor: GlobalParams.themes["$banque_id"].iconsColor,
                inactiveColor: Colors.grey[200],
                min: 2,
                max: 15,
                value: taux,
                onChanged: (v) {
                  setState(() {
                    taux = v;
                    tTaux.text = (v.toInt()).toString();
                  });
                }),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "2%",
                    style: GoogleFonts.roboto(),
                  ),
                  Text(
                    "15%",
                    style: GoogleFonts.roboto(),
                  ),
                ],
              ),
            ),
            !eTaux
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        eTaux = !eTaux;
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "${taux.toInt()}%",
                        style: GoogleFonts.roboto(),
                      ),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.3,
                        0,
                        MediaQuery.of(context).size.width * 0.3,
                        0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        suffix: Text("%"),
                      ),
                      controller: tTaux,
                      keyboardType: TextInputType.number,
                      cursorColor: secondaryColor7,
                      onChanged: (v) {
                        if (double.parse(v) <= 2)
                          setState(() {
                            taux = 2;
                            tTaux.text = "2";
                          });
                        else if (double.parse(v) > 15)
                          setState(() {
                            taux = 15;
                            tTaux.text = "15";
                          });
                        else
                          setState(() {
                            taux = double.parse(v);
                          });
                      },
                      onFieldSubmitted: (v) {
                        if (double.parse(v) <= 2)
                          setState(() {
                            taux = 2;
                            tTaux.text = "2";
                            eTaux = !eTaux;
                          });
                        else if (double.parse(v) > 15)
                          setState(() {
                            taux = 15;
                            tTaux.text = "15";
                            eTaux = !eTaux;
                          });
                        else
                          setState(() {
                            taux = double.parse(v);
                            eTaux = !eTaux;
                          });
                      },
                      style: GoogleFonts.roboto(),
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 20),
              child: Text(
                AppLocalizations.of(context).duree,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 20,
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor)),
              ),
            ),
            Slider(
                activeColor: GlobalParams.themes["$banque_id"].iconsColor,
                inactiveColor: Colors.grey[200],
                min: 0,
                max: 84,
                value: duree,
                onChanged: (v) {
                  setState(() {
                    duree = v;
                    tDuree.text = (v.toInt()).toString();
                  });
                }),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "0 ${AppLocalizations.of(context).mois}",
                    style: GoogleFonts.roboto(),
                  ),
                  Text(
                    "84 ${AppLocalizations.of(context).mois}",
                    style: GoogleFonts.roboto(),
                  ),
                ],
              ),
            ),
            !eDuree
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        eDuree = !eDuree;
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "${duree.toInt()} ${AppLocalizations.of(context).mois}",
                        style: GoogleFonts.roboto(),
                      ),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.3,
                        0,
                        MediaQuery.of(context).size.width * 0.3,
                        0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        suffix: Text(AppLocalizations.of(context).mois),
                      ),
                      controller: tDuree,
                      keyboardType: TextInputType.number,
                      cursorColor: secondaryColor7,
                      onChanged: (v) {
                        if (double.parse(v) <= 0)
                          setState(() {
                            duree = 0;
                            tDuree.text = "0";
                          });
                        else if (double.parse(v) > 84)
                          setState(() {
                            duree = 84;
                            tDuree.text = "84";
                          });
                        else
                          setState(() {
                            duree = double.parse(v);
                          });
                      },
                      onFieldSubmitted: (v) {
                        if (double.parse(v) <= 0)
                          setState(() {
                            duree = 0;
                            tDuree.text = "0";
                            eDuree = !eDuree;
                          });
                        else if (double.parse(v) > 84)
                          setState(() {
                            duree = 84;
                            tDuree.text = "84";
                            eDuree = !eDuree;
                          });
                        else
                          setState(() {
                            duree = double.parse(v);
                            eDuree = !eDuree;
                          });
                      },
                      style: GoogleFonts.roboto(),
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 20),
              child: Text(
                  AppLocalizations.of(context).montant,
                style: GoogleFonts.roboto(
                    textStyle: TextStyle(
                        fontSize: 20,
                        color: GlobalParams
                            .themes["$banque_id"].intituleCmpColor)),
              ),
            ),
            Slider(
                activeColor: GlobalParams.themes["$banque_id"].iconsColor,
                inactiveColor: Colors.grey[200],
                min: 0,
                max: 500000,
                value: montant,
                onChanged: (v) {
                  setState(() {
                    montant = v;
                    tMontant.text = (v.toInt()).toString();
                  });
                }),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "0 ",
                    style: GoogleFonts.roboto(),
                  ),
                  Text(
                    "5 000 000",
                    textDirection: TextDirection.ltr,
                    style: GoogleFonts.roboto(),
                  ),
                ],
              ),
            ),
            !eMontant
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        eMontant = !eMontant;
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "${montant.toStringAsFixed(montant.truncateToDouble() == montant ? 0 : 2)} CFA",
                        textDirection: TextDirection.ltr,
                        style: GoogleFonts.roboto(),
                      ),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width * 0.3,
                        0,
                        MediaQuery.of(context).size.width * 0.3,
                        0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        suffix: Text(""),
                      ),
                      controller: tMontant,
                      keyboardType: TextInputType.number,
                      cursorColor: secondaryColor7,
                      onChanged: (v) {
                        if (double.parse(v) <= 0)
                          setState(() {
                            montant = 0;
                            tMontant.text = "0";
                          });
                        else if (double.parse(v) > 500000)
                          setState(() {
                            montant = 500000;
                            tMontant.text = "500000";
                          });
                        else
                          setState(() {
                            montant = double.parse(v);
                          });
                      },
                      onFieldSubmitted: (v) {
                        if (double.parse(v) <= 0)
                          setState(() {
                            montant = 0;
                            tMontant.text = "0";
                            eMontant = !eMontant;
                          });
                        else if (double.parse(v) > 500000)
                          setState(() {
                            montant = 500000;
                            tMontant.text = "500000";
                            eMontant = !eMontant;
                          });
                        else
                          setState(() {
                            montant = double.parse(v);
                            eMontant = !eMontant;
                          });
                      },
                      style: GoogleFonts.roboto(),
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(
                top: 40,
              ),
              child: DottedBorder(
                dashPattern: [4, 9, 4, 9],
                borderType: BorderType.RRect,
                radius: Radius.circular(22),
                padding: EdgeInsets.all(20),
                color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                strokeWidth: 3,
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Container(
                            padding: EdgeInsets.only(top: 10, bottom: 5),
                            alignment: Alignment.center,
                            child: Text(
                              "${AppLocalizations.of(context).votre_mensualite} :",
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                  textStyle: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: GlobalParams.themes["$banque_id"]
                                          .intituleCmpColor)),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Container(
                            padding: EdgeInsets.only(bottom: 10),
                            alignment: Alignment.center,
                              child: RichText(
                                textDirection: TextDirection.ltr,
                                text: TextSpan(children: [
                                  TextSpan(
                                      text: "${mensualite.toStringAsFixed(mensualite.truncateToDouble() == mensualite ? 0 : 2)}",
                                      // "${state.clients.comptes[index].soldeTempsReel ?? ""} ",
                                      style: GoogleFonts.roboto(
                                        textStyle: TextStyle(
                                            fontSize: 25,
                                            color: GlobalParams.themes["$banque_id"]
                                                .intituleCmpColor),
                                      )),
                                  WidgetSpan(
                                    child: Transform.translate(
                                      offset: const Offset(1, -6),
                                      child: Text("CFA",
                                          textScaleFactor: 0.7,
                                          style: GoogleFonts.roboto(
                                            textStyle: TextStyle(
                                                color: GlobalParams
                                                    .themes["$banque_id"]
                                                    .intituleCmpColor,
                                                fontSize: 21,
                                                fontWeight: FontWeight.w600),
                                          )),
                                    ),
                                  )
                                ]),
                              ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
