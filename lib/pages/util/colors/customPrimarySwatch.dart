//palette.dart
import 'package:flutter/material.dart';
class Palette {
  static const MaterialColor kToDark = const MaterialColor(
    0xFFd29e0e, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xffedc34c ),//10%
      100: const Color(0xffedc142),//20%
      200: const Color(0xffedbd32),//30%
      300: const Color(0xfff0bd29),//40%
      400: const Color(0xfff2ba16),//50%
      500: const Color(0xffdea809),//60%
      600: const Color(0xffba8c06),//70%
      700: const Color(0xff9e7706),//80%
      800: const Color(0xff7a5c05),//90%
      900: const Color(0xff332704),//100%
    },
  );
}