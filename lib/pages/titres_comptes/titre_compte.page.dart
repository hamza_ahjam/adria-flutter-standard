import 'package:LBA/bloc/titres/titre_bloc.dart';
import 'package:LBA/bloc/titres/titre_event.dart';
import 'package:LBA/bloc/titres/titre_state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/titres_comptes/widgets/compte_titre.widget.dart';
import 'package:LBA/widgets/erreurText.widget.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class TitreComptePage extends StatefulWidget {
  int index;
  TitreComptePage({Key key, this.index = 0}) : super(key: key);

  @override
  State<TitreComptePage> createState() => _TitreComptePageState();
}

class _TitreComptePageState extends State<TitreComptePage> {
  bool isLoaded = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToDashboard(context, true);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).titres_comptes,
          context: context,
        ),
        body: Column(
          children: [
            Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          "assets/$banque_id/images/background_image.png"),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.9)),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(22.0),
                  ),
                ),
                child: CompteTitreWidget()),
            Expanded(
              child: BlocBuilder<TitreBloc, TitreStateBloc>(
                  builder: (context, state) {
                if (state.requestState == StateStatus.NONE) {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (!isLoaded)
                      context.read<TitreBloc>().add(LoadTitreEvent());
                  });
                  return Container();
                } else if (state.requestState == StateStatus.LOADING) {
                  return Center(
                      child: CircularProgressIndicator(
                    backgroundColor:
                        GlobalParams.themes["$banque_id"].intituleCmpColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        GlobalParams.themes["$banque_id"].appBarColor),
                  ));
                } else if (state.requestState == StateStatus.ERROR) {
                  return ErreurTextWidget(
                    errorMessage: state.errorMessage,
                    actionEvent: () {
                      context.read<TitreBloc>().add(state.currentAction);
                    },
                  );
                } else if (state.requestState == StateStatus.LOADED) {
                  isLoaded = true;
                  if (state.listTitres.map.titres.length == 0)
                    return Center(
                      child: Text(
                        AppLocalizations.of(context).aucun_titre,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontSize: 16,
                                color: GlobalParams
                                    .themes["$banque_id"].appBarColor)),
                      ),
                    );
                  else
                    return Column(
                      children: [
                        /*                     Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                          "assets/$banque_id/images/background_image.png"),
                                      fit: BoxFit.cover,
                                      alignment: Alignment(0, 0.9),
                                    ),
                                    borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(22.0),
                                    ),
                                  ),
                                  child: CompteTitreWidget()),
                            ),*/
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.05,
                            5,
                            MediaQuery.of(context).size.width * 0.05,
                            MediaQuery.of(context).size.width * 0.02,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    widget.index = 0;
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Text(
                                      AppLocalizations.of(context).portfeuille,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontSize: 12,
                                              color: widget.index == 0
                                                  ? Colors.white
                                                  : GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor))),
                                  decoration: BoxDecoration(
                                      color: widget.index == 0
                                          ? GlobalParams.themes["$banque_id"]
                                              .intituleCmpColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: GlobalParams.themes["$banque_id"]
                                            .intituleCmpColor,
                                      ),
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    widget.index = 1;
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Text(
                                      AppLocalizations.of(context).transactions,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: GoogleFonts.roboto(
                                          textStyle: TextStyle(
                                              fontSize: 12,
                                              color: widget.index == 1
                                                  ? Colors.white
                                                  : GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor))),
                                  decoration: BoxDecoration(
                                      color: widget.index == 1
                                          ? GlobalParams.themes["$banque_id"]
                                              .intituleCmpColor
                                          : Colors.white,
                                      border: Border.all(
                                        color: GlobalParams.themes["$banque_id"]
                                            .intituleCmpColor,
                                      ),
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: state.listTitres.map.titres.length,
                                itemBuilder: (_, index) {
                                  return Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        MediaQuery.of(context).size.width *
                                            0.05,
                                        10,
                                        MediaQuery.of(context).size.width *
                                            0.05,
                                        10),
                                    child: Card(
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(30))),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.8,
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: GlobalParams
                                                      .themes["$banque_id"]
                                                      .intituleCmpColor,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  30),
                                                          topRight:
                                                              Radius.circular(
                                                                  30))),
                                              child: ListTile(
                                                leading: Container(
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  20))),
                                                  child: Icon(
                                                    Icons.arrow_upward,
                                                    color: Colors.blue,
                                                    size: 40,
                                                  ),
                                                ),
                                                title: Text(
                                                  AppLocalizations.of(context)
                                                      .val_action,
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 15,
                                                          color: Colors.white)),
                                                ),
                                                subtitle: Text(
                                                  "${state.listTitres.map.titres[index].nomValeurDetenue}",
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 13,
                                                          color: Colors.white)),
                                                ),
                                                trailing: Text(
                                                  "${state.listTitres.map.titres[index].dateCours}",
                                                  style: GoogleFonts.roboto(
                                                      textStyle: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 15,
                                                          color: Colors.white)),
                                                ),
                                              ),
                                            ),
                                            ListTile(
                                              title: Text(
                                                  "${AppLocalizations.of(context).montant_brute} :",
                                                  style: GoogleFonts.roboto()),
                                              trailing: Text(
                                                "${state.listTitres.map.titres[index].montantBrute}",
                                                style: GoogleFonts.roboto(
                                                  textStyle: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ),
                                            ListTile(
                                              title: Text(
                                                  "${AppLocalizations.of(context).valorisation} :",
                                                  style: GoogleFonts.roboto()),
                                              trailing: Text(
                                                  "${state.listTitres.map.titres[index].montantValorise}",
                                                  style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )),
                                            ),
                                            ListTile(
                                              title: Text(
                                                  "${AppLocalizations.of(context).total_pmv} :",
                                                  style: GoogleFonts.roboto()),
                                              trailing: Text(
                                                  "${state.listTitres.map.titres[index].totalPMV}",
                                                  style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )),
                                            ),
                                            SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.03,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                })),
                      ],
                    );
                } else {
                  return Container();
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}
