import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart' as intl;

class ListTitreWidget extends StatelessWidget {
  const ListTitreWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05,
              10, MediaQuery.of(context).size.width * 0.05, 10),
          child: Card(
            elevation: 5,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color:
                            GlobalParams.themes["$banque_id"].intituleCmpColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: ListTile(
                      leading: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Icon(
                          Icons.arrow_upward,
                          color: Colors.blue,
                          size: 40,
                        ),
                      ),
                      title: Text(
                        AppLocalizations.of(context).val_action,
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                                color: Colors.white)),
                      ),
                      subtitle: Text(
                        "097526678",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 13,
                                color: Colors.white)),
                      ),
                      trailing: Text(
                        "${intl.DateFormat('dd/MM/yyyy').format(DateTime.now())}",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                                color: Colors.white)),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).montant_brute} :", style: GoogleFonts.roboto()),
                    trailing: Text(
                      "56932.0",
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).valorisation} :", style: GoogleFonts.roboto()),
                    trailing: Text("4322.0",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  ListTile(
                    title: Text("${AppLocalizations.of(context).total_pmv} :", style: GoogleFonts.roboto()),
                    trailing: Text("-10.994,00",
                        style: GoogleFonts.roboto(
                          textStyle: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
