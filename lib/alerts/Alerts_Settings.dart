import 'package:LBA/config/global.params.dart';
import 'package:LBA/pages/dashboard/dashboard.compte.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants.dart';

class AlertsSettings extends StatefulWidget {
  @override
  State<AlertsSettings> createState() => _AlertsSettingsState();
}

class _AlertsSettingsState extends State<AlertsSettings> {
  bool maintenance = false;
  bool marketingCampain = false;
  bool expiredCard = false;
  bool availableCard = false;
  bool availableCheck = false;
  bool unpaidCheck = false;
  bool unpaidCreditTerm = false;
  bool unpaidDirectDebit = false;
  bool receivedTransfer = false;
  bool newBillAvailable = false;
  bool billPaymentFailed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))),
        toolbarHeight: MediaQuery.of(context).size.height * 0.085,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text(
            "paramètres alertes".toUpperCase(),
            style: GoogleFonts.roboto(
                textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                )),
          ),
        ),
        centerTitle: true,
        backgroundColor: GlobalParams.themes["$banque_id"].appBarColor,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardComptePage()));
            }),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 45, right: 45, top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notifications Occasionnelles :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: maintenance,
                      onChanged: (value) {
                        setState(() {
                          maintenance = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Maintenance",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: marketingCampain,
                      onChanged: (value) {
                        setState(() {
                          marketingCampain = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Campagne Marketing",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notification carte bancaire :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: expiredCard,
                      onChanged: (value) {
                        setState(() {
                          expiredCard = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Carte arrivée à expiration",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: availableCard,
                      onChanged: (value) {
                        setState(() {
                          availableCard = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Carte disponible en agence",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notification chéquier :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: availableCheck,
                      onChanged: (value) {
                        setState(() {
                          availableCheck = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Chéquier disponible en agence",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notification impayés :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: unpaidCheck,
                      onChanged: (value) {
                        setState(() {
                          unpaidCheck = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Chèque émis impayée",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: unpaidCreditTerm,
                      onChanged: (value) {
                        setState(() {
                          unpaidCreditTerm = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Echéance de crédit impayée",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: unpaidDirectDebit,
                      onChanged: (value) {
                        setState(() {
                          unpaidDirectDebit = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Prélèvement automatique impayé",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notification Virement :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: receivedTransfer,
                      onChanged: (value) {
                        setState(() {
                          receivedTransfer = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Virement reçu",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  "Notification Paiement facture :",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      //TODO: get the color from global params
                      color: Color.fromRGBO(29, 44, 98, 1)),
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: newBillAvailable,
                      onChanged: (value) {
                        setState(() {
                          newBillAvailable = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Disponibilité d’une nouvelle facture",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              ),
              Container(
                height: 38,
                child: Row(
                  children: [
                    Switch(
                      value: billPaymentFailed,
                      onChanged: (value) {
                        setState(() {
                          billPaymentFailed = value;
                        });
                      },
                      activeColor: Color.fromRGBO(29, 44, 98, 1),
                      activeTrackColor: Color.fromRGBO(112, 112, 112, 1),
                    ),
                    Text(
                      "Paiement de facture échoué",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(67, 67, 67, 1)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
