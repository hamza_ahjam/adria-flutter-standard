import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/app_bar.dart';
import 'package:LBA/widgets/header.dart';
import 'package:LBA/widgets/menu/offlineMenu.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class InformationPage extends StatefulWidget {
  const InformationPage({Key key}) : super(key: key);

  @override
  _InformationPageState createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        moveToLogin(context);
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: OffLineMenu(2),
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(MediaQuery.of(context).size.height * 0.05),
            child: AppBarWidget(
              context: context,
            )),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/$banque_id/images/login_bg.png"),
                fit: BoxFit.cover),
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(22.0),
            ),
          ),
          child: ListView(
            children: <Widget>[
              Header(
                isFromAgences: false,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015),
                          child: Text(
                              AppLocalizations.of(context).infos,
                              style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    fontSize: 20,
                                    color: GlobalParams
                                        .themes["$banque_id"].infoColor,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.08,
                      ),
                      GestureDetector(
                        onTap: () {
                          print("tap 1");
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.065,
                            width: MediaQuery.of(context).size.width * 0.85,
                            decoration: BoxDecoration(
                                color: GlobalParams
                                    .themes["$banque_id"].graphbodyColor,
                                borderRadius: BorderRadius.circular(50.0)),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {},
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              child: Transform.translate(
                                offset: Offset(
                                    MediaQuery.of(context).size.height * 0.025,
                                    0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        AppLocalizations.of(context).gest_comptes,
                                        style: GoogleFonts.roboto(
                                            color: Colors.white, fontSize: 18),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_right_rounded,
                                      size: 45,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.04,
                      ),
                      GestureDetector(
                        onTap: () {
                          print("tap 2");
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.065,
                            width: MediaQuery.of(context).size.width * 0.85,
                            decoration: BoxDecoration(
                                color: GlobalParams
                                    .themes["$banque_id"].graphbodyColor,
                                borderRadius: BorderRadius.circular(50.0)),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {},
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              child: Transform.translate(
                                offset: Offset(
                                    MediaQuery.of(context).size.height * 0.025,
                                    0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        AppLocalizations.of(context).identification,
                                        style: GoogleFonts.roboto(
                                            color: Colors.white, fontSize: 18),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_right_rounded,
                                      size: 45,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.04,
                      ),
                      GestureDetector(
                        onTap: () {
                          print("tap 3");
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.065,
                            width: MediaQuery.of(context).size.width * 0.85,
                            decoration: BoxDecoration(
                                color: GlobalParams
                                    .themes["$banque_id"].graphbodyColor,
                                borderRadius: BorderRadius.circular(50.0)),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {},
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              child: Transform.translate(
                                offset: Offset(
                                    MediaQuery.of(context).size.height * 0.025,
                                    0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        AppLocalizations.of(context).paiements,
                                        style: GoogleFonts.roboto(
                                            color: Colors.white, fontSize: 18),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_right_rounded,
                                      size: 45,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.04,
                      ),
                      GestureDetector(
                        onTap: () {
                          print("tap 4");
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.065,
                            width: MediaQuery.of(context).size.width * 0.85,
                            decoration: BoxDecoration(
                                color: GlobalParams
                                    .themes["$banque_id"].graphbodyColor,
                                borderRadius: BorderRadius.circular(50.0)),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {},
                              color: GlobalParams
                                  .themes["$banque_id"].intituleCmpColor,
                              child: Transform.translate(
                                offset: Offset(
                                    MediaQuery.of(context).size.height * 0.025,
                                    0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        AppLocalizations.of(context).securite,
                                        style: GoogleFonts.roboto(
                                            color: Colors.white, fontSize: 18),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_right_rounded,
                                      size: 45,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                      ),
                      // SizedBox(
                      //   height: MediaQuery.of(context).size.height * 0.03,
                      // )
                      // DefaultButton(
                      //     child: Padding(
                      //       padding: EdgeInsets.only(
                      //           left: 20, right: 10, top: 5, bottom: 5),
                      //       child: Row(
                      //         children: <Widget>[
                      //           Text(
                      //             'Gestionnaire des comptes',
                      //             style: TextStyle(
                      //                 color: Colors.white,
                      //                 fontSize: 17,
                      //                 fontFamily: 'TitilliumWeb'),
                      //           ),
                      //           Icon(
                      //             Icons.keyboard_arrow_right,
                      //             color: Colors.white,
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //     gradient: LinearGradient(
                      //       colors: <Color>[
                      //         Color.fromRGBO(110, 153, 254, 100),
                      //         Color.fromRGBO(90, 204, 173, 100)
                      //       ],
                      //     ),
                      //     onPressed: () {
                      //       print("Gestionnaire des comptes");
                      //     }),
                    ],
                  ),
                ),
              ),
              // Footer()
            ],
          ),
        ),
      ),
    );
  }
}
