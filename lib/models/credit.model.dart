// To parse this JSON data, do
//
//     final credits = creditsFromJson(jsonString);

import 'dart:convert';

Credits creditsFromJson(String str) => Credits.fromJson(json.decode(str));

String creditsToJson(Credits data) => json.encode(data.toJson());

class Credits {
  Credits({
    this.map,
  });

  MapClass map;

  factory Credits.fromJson(Map<String, dynamic> json) => Credits(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.credits,
    this.success,
  });

  List<Credit> credits;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        credits:
            List<Credit>.from(json["credits"].map((x) => Credit.fromJson(x))),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "credits": List<dynamic>.from(credits.map((x) => x.toJson())),
        "success": success,
      };
}

class Credit {
  Credit({
    this.tauxGlobalDuCreditFormatted,
    this.nombreEcheances,
    this.installmentPeriods,
    this.loanAmount,
    this.dateFine,
    this.mensualite,
    this.capitalRestantDu,
    this.montantPinalite,
    this.nombreEcheancesPayes,
    this.nouveauTauxEndettement,
    this.echeanceAvecAssurance,
    this.tauxAssurance,
    this.montantChargesEtCommissions,
    this.montantImpaye,
    this.devise,
    this.encours,
    this.errCode,
    this.disbursedLoan,
    this.primeFlatAssurance,
    this.nombreEcheancesRestantes,
    this.tauxAutofinancementDuProjet,
    this.coutTotalDuCredit,
    this.coutDuProjet,
    this.tauxGlobalDuCredit,
    this.capitalRestantFormatted,
    this.tauxPinalite,
    this.dateDeblocage,
    this.periodiciteInteret,
    this.dateDeblocageFormatted,
    this.dateFirstEcheance,
    this.dateStatut,
    this.tauxNotaire,
    this.montantProchaineEcheance,
    this.capitalEmprunte,
    this.firstInstallmentDate,
    this.nombreImpaye,
    this.echeanceAvecAssuranceFormatted,
    this.dateFineFormatted,
    this.typeDePret,
    this.dateLastEcheance,
    this.idAbonne,
    this.periodiciteCapital,
    this.outstandingLoanAmount,
    this.impayeCreditList,
    this.dureeDePret,
    this.nombreEcheancesComing,
    this.dateDernierDeblocage,
    this.installmentArrears,
    this.apportPersonnel,
    this.dateFirstEcheanceFormatted,
    this.lastDisbursementDate,
    this.montantAvance,
    this.numeroDossier,
    this.dateProchaineEcheance,
    this.startDate,
    this.coutFraisEtNotaire,
    this.capitalRestant,
    this.fraisDeDossierDeCredit,
    this.dateLastEcheanceeFormatted,
    this.identifiant,
    this.statut,
    this.apportConjodynamic,
    this.typeEcheance,
    this.installmentDate,
    this.typeAssurance,
    this.montantDuPret,
    this.dynamicitule,
    this.errorMessage,
    this.idCreditClient,
    this.mensualiteFomated,
    this.numeroCompte,
    this.capitalEmprunteFormatted,
    this.echeanceHorsAssurance,
  });

  dynamic tauxGlobalDuCreditFormatted;
  dynamic nombreEcheances;
  dynamic installmentPeriods;
  dynamic loanAmount;
  dynamic dateFine;
  dynamic mensualite;
  dynamic capitalRestantDu;
  dynamic montantPinalite;
  dynamic nombreEcheancesPayes;
  dynamic nouveauTauxEndettement;
  dynamic echeanceAvecAssurance;
  dynamic tauxAssurance;
  dynamic montantChargesEtCommissions;
  String montantImpaye;
  dynamic devise;
  dynamic encours;
  dynamic errCode;
  dynamic disbursedLoan;
  dynamic primeFlatAssurance;
  dynamic nombreEcheancesRestantes;
  dynamic tauxAutofinancementDuProjet;
  dynamic coutTotalDuCredit;
  dynamic coutDuProjet;
  dynamic tauxGlobalDuCredit;
  dynamic capitalRestantFormatted;
  dynamic tauxPinalite;
  dynamic dateDeblocage;
  String periodiciteInteret;
  dynamic dateDeblocageFormatted;
  String dateFirstEcheance;
  dynamic dateStatut;
  dynamic tauxNotaire;
  dynamic montantProchaineEcheance;
  dynamic capitalEmprunte;
  dynamic firstInstallmentDate;
  String nombreImpaye;
  dynamic echeanceAvecAssuranceFormatted;
  dynamic dateFineFormatted;
  String typeDePret;
  String dateLastEcheance;
  dynamic idAbonne;
  dynamic periodiciteCapital;
  dynamic outstandingLoanAmount;
  dynamic impayeCreditList;
  dynamic dureeDePret;
  dynamic nombreEcheancesComing;
  dynamic dateDernierDeblocage;
  dynamic installmentArrears;
  dynamic apportPersonnel;
  String dateFirstEcheanceFormatted;
  dynamic lastDisbursementDate;
  dynamic montantAvance;
  String numeroDossier;
  String dateProchaineEcheance;
  dynamic startDate;
  dynamic coutFraisEtNotaire;
  dynamic capitalRestant;
  dynamic fraisDeDossierDeCredit;
  String dateLastEcheanceeFormatted;
  dynamic identifiant;
  dynamic statut;
  dynamic apportConjodynamic;
  dynamic typeEcheance;
  dynamic installmentDate;
  dynamic typeAssurance;
  dynamic montantDuPret;
  dynamic dynamicitule;
  dynamic errorMessage;
  dynamic idCreditClient;
  dynamic mensualiteFomated;
  String numeroCompte;
  dynamic capitalEmprunteFormatted;
  dynamic echeanceHorsAssurance;

  factory Credit.fromJson(Map<String, dynamic> json) => Credit(
        tauxGlobalDuCreditFormatted: json["tauxGlobalDuCreditFormatted"],
        nombreEcheances: json["nombreEcheances"],
        installmentPeriods: json["installmentPeriods"],
        loanAmount: json["loanAmount"],
        dateFine: json["dateFine"],
        mensualite: json["mensualite"],
        capitalRestantDu: json["capitalRestantDu"],
        montantPinalite: json["montantPinalite"],
        nombreEcheancesPayes: json["nombreEcheancesPayes"],
        nouveauTauxEndettement: json["nouveauTauxEndettement"],
        echeanceAvecAssurance: json["echeanceAvecAssurance"],
        tauxAssurance: json["tauxAssurance"],
        montantChargesEtCommissions: json["montantChargesEtCommissions"],
        montantImpaye: json["montantImpaye"],
        devise: json["devise"],
        encours: json["encours"],
        errCode: json["errCode"],
        disbursedLoan: json["disbursedLoan"],
        primeFlatAssurance: json["primeFlatAssurance"],
        nombreEcheancesRestantes: json["nombreEcheancesRestantes"],
        tauxAutofinancementDuProjet: json["tauxAutofinancementDuProjet"],
        coutTotalDuCredit: json["coutTotalDuCredit"],
        coutDuProjet: json["coutDuProjet"],
        tauxGlobalDuCredit: json["tauxGlobalDuCredit"].toDouble(),
        capitalRestantFormatted: json["capitalRestantFormatted"],
        tauxPinalite: json["tauxPinalite"],
        dateDeblocage: json["dateDeblocage"],
        periodiciteInteret: json["periodiciteInteret"],
        dateDeblocageFormatted: json["dateDeblocageFormatted"],
        dateFirstEcheance: json["dateFirstEcheance"],
        dateStatut: json["dateStatut"],
        tauxNotaire: json["tauxNotaire"],
        montantProchaineEcheance: json["montantProchaineEcheance"],
        capitalEmprunte: json["capitalEmprunte"].toDouble(),
        firstInstallmentDate: json["firstInstallmentDate"],
        nombreImpaye: json["nombreImpaye"],
        echeanceAvecAssuranceFormatted: json["echeanceAvecAssuranceFormatted"],
        dateFineFormatted: json["dateFineFormatted"],
        typeDePret: json["typeDePret"],
        dateLastEcheance: json["dateLastEcheance"],
        idAbonne: json["idAbonne"],
        periodiciteCapital: json["periodiciteCapital"],
        outstandingLoanAmount: json["outstandingLoanAmount"],
        impayeCreditList: json["impayeCreditList"],
        dureeDePret: json["dureeDePret"],
        nombreEcheancesComing: json["nombreEcheancesComing"],
        dateDernierDeblocage: json["dateDernierDeblocage"],
        installmentArrears: json["installmentArrears"],
        apportPersonnel: json["apportPersonnel"],
        dateFirstEcheanceFormatted: json["dateFirstEcheanceFormatted"],
        lastDisbursementDate: json["lastDisbursementDate"],
        montantAvance: json["montantAvance"],
        numeroDossier: json["numeroDossier"],
        dateProchaineEcheance: json["dateProchaineEcheance"],
        startDate: json["startDate"],
        coutFraisEtNotaire: json["coutFraisEtNotaire"],
        capitalRestant: json["capitalRestant"],
        fraisDeDossierDeCredit: json["fraisDeDossierDeCredit"],
        dateLastEcheanceeFormatted: json["dateLastEcheanceeFormatted"],
        identifiant: json["identifiant"],
        statut: json["statut"],
        apportConjodynamic: json["apportConjodynamic"],
        typeEcheance: json["typeEcheance"],
        installmentDate: json["installmentDate"],
        typeAssurance: json["typeAssurance"],
        montantDuPret: json["montantDuPret"],
        dynamicitule: json["dynamicitule"],
        errorMessage: json["errorMessage"],
        idCreditClient: json["idCreditClient"],
        mensualiteFomated: json["mensualiteFomated"],
        numeroCompte: json["numeroCompte"],
        capitalEmprunteFormatted: json["capitalEmprunteFormatted"],
        echeanceHorsAssurance: json["echeanceHorsAssurance"],
      );

  Map<String, dynamic> toJson() => {
        "tauxGlobalDuCreditFormatted": tauxGlobalDuCreditFormatted,
        "nombreEcheances": nombreEcheances,
        "installmentPeriods": installmentPeriods,
        "loanAmount": loanAmount,
        "dateFine": dateFine,
        "mensualite": mensualite,
        "capitalRestantDu": capitalRestantDu,
        "montantPinalite": montantPinalite,
        "nombreEcheancesPayes": nombreEcheancesPayes,
        "nouveauTauxEndettement": nouveauTauxEndettement,
        "echeanceAvecAssurance": echeanceAvecAssurance,
        "tauxAssurance": tauxAssurance,
        "montantChargesEtCommissions": montantChargesEtCommissions,
        "montantImpaye": montantImpaye,
        "devise": devise,
        "encours": encours,
        "errCode": errCode,
        "disbursedLoan": disbursedLoan,
        "primeFlatAssurance": primeFlatAssurance,
        "nombreEcheancesRestantes": nombreEcheancesRestantes,
        "tauxAutofinancementDuProjet": tauxAutofinancementDuProjet,
        "coutTotalDuCredit": coutTotalDuCredit,
        "coutDuProjet": coutDuProjet,
        "tauxGlobalDuCredit": tauxGlobalDuCredit,
        "capitalRestantFormatted": capitalRestantFormatted,
        "tauxPinalite": tauxPinalite,
        "dateDeblocage": dateDeblocage,
        "periodiciteInteret": periodiciteInteret,
        "dateDeblocageFormatted": dateDeblocageFormatted,
        "dateFirstEcheance": dateFirstEcheance,
        "dateStatut": dateStatut,
        "tauxNotaire": tauxNotaire,
        "montantProchaineEcheance": montantProchaineEcheance,
        "capitalEmprunte": capitalEmprunte,
        "firstInstallmentDate": firstInstallmentDate,
        "nombreImpaye": nombreImpaye,
        "echeanceAvecAssuranceFormatted": echeanceAvecAssuranceFormatted,
        "dateFineFormatted": dateFineFormatted,
        "typeDePret": typeDePret,
        "dateLastEcheance": dateLastEcheance,
        "idAbonne": idAbonne,
        "periodiciteCapital": periodiciteCapital,
        "outstandingLoanAmount": outstandingLoanAmount,
        "impayeCreditList": impayeCreditList,
        "dureeDePret": dureeDePret,
        "nombreEcheancesComing": nombreEcheancesComing,
        "dateDernierDeblocage": dateDernierDeblocage,
        "installmentArrears": installmentArrears,
        "apportPersonnel": apportPersonnel,
        "dateFirstEcheanceFormatted": dateFirstEcheanceFormatted,
        "lastDisbursementDate": lastDisbursementDate,
        "montantAvance": montantAvance,
        "numeroDossier": numeroDossier,
        "dateProchaineEcheance": dateProchaineEcheance,
        "startDate": startDate,
        "coutFraisEtNotaire": coutFraisEtNotaire,
        "capitalRestant": capitalRestant,
        "fraisDeDossierDeCredit": fraisDeDossierDeCredit,
        "dateLastEcheanceeFormatted": dateLastEcheanceeFormatted,
        "identifiant": identifiant,
        "statut": statut,
        "apportConjodynamic": apportConjodynamic,
        "typeEcheance": typeEcheance,
        "installmentDate": installmentDate,
        "typeAssurance": typeAssurance,
        "montantDuPret": montantDuPret,
        "dynamicitule": dynamicitule,
        "errorMessage": errorMessage,
        "idCreditClient": idCreditClient,
        "mensualiteFomated": mensualiteFomated,
        "numeroCompte": numeroCompte,
        "capitalEmprunteFormatted": capitalEmprunteFormatted,
        "echeanceHorsAssurance": echeanceHorsAssurance,
      };
}
