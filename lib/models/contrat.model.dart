// To parse this JSON data, do
//
//     final contrat = contratFromJson(jsonString);

import 'dart:convert';

Contrat contratFromJson(String str) => Contrat.fromJson(json.decode(str));

String contratToJson(Contrat data) => json.encode(data.toJson());

class Contrat {
  Contrat({
    this.map,
  });

  MapClass map;

  factory Contrat.fromJson(Map<String, dynamic> json) => Contrat(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.success,
    this.contratAbonnementRoot,
  });

  int total;
  bool success;
  List<ContratAbonnementRoot> contratAbonnementRoot;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        success: json["success"],
        contratAbonnementRoot: List<ContratAbonnementRoot>.from(
            json["contratAbonnementRoot"]
                .map((x) => ContratAbonnementRoot.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "success": success,
        "contratAbonnementRoot":
            List<dynamic>.from(contratAbonnementRoot.map((x) => x.toJson())),
      };
}

class ContratAbonnementRoot {
  ContratAbonnementRoot({
    this.id,
    this.lang,
    this.datePattern,
    this.max,
    this.page,
    this.sort,
    this.order,
    this.codeBanque,
    this.userId,
    this.version,
    this.codeBanqueAssocie,
    this.codePaysAssocie,
    this.identifiantContrat,
    this.produitSouscription,
    this.nombreAbonnes,
    this.typeClient,
    this.contratAbonnementRootTypeClientLib,
    this.intituleRc,
    this.radical,
    this.agence,
    this.dateSouscription,
    this.etat,
    this.chargeCLientele,
    this.etatFormatted,
    this.relationCommercialeDto,
    this.qualiteParRapportContrat,
    this.abonnementMobile,
    this.abonnementWeb,
    this.montantMinVirUnitWeb,
    this.montantMaxVirUnitWeb,
    this.montantVirQuotWeb,
    this.nbrMaxVirQuotWeb,
    this.montantMinVirUnitMobile,
    this.montantMaxVirUnitMobile,
    this.montantVirQuotMobile,
    this.nbrMaxVirQuotMobile,
    this.montantVirMassQuotMobile,
    this.montantVirQuotTous,
    this.montantMaxVirUnitTous,
    this.nbrMaxVirQuotTous,
    this.montantMinVirDevUnitWeb,
    this.montantMaxVirDevUnitWeb,
    this.montantVirDevQuotWeb,
    this.nbrMaxVirDevQuotWeb,
    this.montantMinVirDevUnitMobile,
    this.montantMaxVirDevUnitMobile,
    this.montantVirDevQuotMobile,
    this.nbrMaxVirDevQuotMobile,
    this.montantVirDevQuotTous,
    this.montantMaxVirDevUnitTous,
    this.nbrMaxVirDevQuotTous,
    this.montantMinVirMassUnitWeb,
    this.montantMaxVirMassUnitWeb,
    this.montantVirMassQuotWeb,
    this.nbrMaxVirMassQuotWeb,
    this.montantMinVirMassUnitMobile,
    this.montantMaxVirMassUnitMobile,
    this.nbrMaxVirMassQuotMobile,
    this.montantVirMassQuotTous,
    this.montantMaxVirMassUnitTous,
    this.nbrMaxVirMassQuotTous,
    this.montantTousVirQuotTous,
    this.nbrMaxCheqTous,
    this.nbrMaxLcnTous,
    this.nbrMaxBenMadTous,
    this.nbrMaxBenDomTous,
    this.nbrMaxBenRechTous,
    this.nbrMaxBenDevTous,
    this.montantMinVcnssUnitWeb,
    this.montantMaxVcnssUnitWeb,
    this.montantMinVvmUnitWeb,
    this.montantMaxVvmUnitWeb,
    this.montantMinVppUnitWeb,
    this.montantMaxVppUnitWeb,
    this.montantMinVvbUnitWeb,
    this.montantMaxVvbUnitWeb,
    this.montantMinVccUnitWeb,
    this.montantMaxVccUnitWeb,
    this.montantVccQuotWeb,
    this.nbrMaxVccQuotWeb,
    this.montantVvbQuotWeb,
    this.nbrMaxVvbQuotWeb,
    this.montantVppQuotWeb,
    this.nbrMaxVppQuotWeb,
    this.montantVvmQuotWeb,
    this.nbrMaxVvmQuotWeb,
    this.montantVcnssQuotWeb,
    this.nbrMaxVcnssQuotWeb,
    this.montantMinVcnssUnitMobile,
    this.montantMaxVcnssUnitMobile,
    this.montantVcnssQuotMobile,
    this.nbrMaxVcnssQuotMobile,
    this.montantMinVvmUnitMobile,
    this.montantMaxVvmUnitMobile,
    this.montantVvmQuotMobile,
    this.nbrMaxVvmQuotMobile,
    this.montantMinVppUnitMobile,
    this.montantMaxVppUnitMobile,
    this.montantVppQuotMobile,
    this.nbrMaxVppQuotMobile,
    this.montantMinVvbUnitMobile,
    this.montantMaxVvbUnitMobile,
    this.montantVvbQuotMobile,
    this.nbrMaxVvbQuotMobile,
    this.montantMinVccUnitMobile,
    this.montantMaxVccUnitMobile,
    this.montantVccQuotMobile,
    this.nbrMaxVccQuotMobile,
    this.montantVcnssQuotTous,
    this.montantMaxVcnssUnitTous,
    this.nbrMaxVcnssQuotTous,
    this.montantVvmQuotTous,
    this.montantMaxVvmUnitTous,
    this.nbrMaxVvmQuotTous,
    this.montantVppQuotTous,
    this.montantMaxVppUnitTous,
    this.nbrMaxVppQuotTous,
    this.montantVvbQuotTous,
    this.montantMaxVvbUnitTous,
    this.nbrMaxVvbQuotTous,
    this.montantVccQuotTous,
    this.montantMaxVccUnitTous,
    this.nbrMaxVccQuotTous,
    this.montantMinDpfUnitWeb,
    this.montantMaxDpfUnitWeb,
    this.montantDpfQuotWeb,
    this.nbrMaxDpfQuotWeb,
    this.montantMinDpfUnitMobile,
    this.montantMaxDpfUnitMobile,
    this.montantDpfQuotMobile,
    this.nbrMaxDpfQuotMobile,
    this.montantDpfQuotTous,
    this.montantMaxDpfUnitTous,
    this.nbrMaxDpfQuotTous,
    this.montantMinDrgUnitWeb,
    this.montantMaxDrgUnitWeb,
    this.montantDrgQuotWeb,
    this.nbrMaxDrgQuotWeb,
    this.montantMinDrgUnitMobile,
    this.montantMaxDrgUnitMobile,
    this.montantDrgQuotMobile,
    this.nbrMaxDrgQuotMobile,
    this.montantDrgQuotTous,
    this.montantMaxDrgUnitTous,
    this.nbrMaxDrgQuotTous,
    this.dateCreation,
    this.typeClientLib,
  });

  int id;
  dynamic lang;
  dynamic datePattern;
  dynamic max;
  dynamic page;
  dynamic sort;
  dynamic order;
  dynamic codeBanque;
  dynamic userId;
  int version;
  String codeBanqueAssocie;
  String codePaysAssocie;
  String identifiantContrat;
  dynamic produitSouscription;
  int nombreAbonnes;
  dynamic typeClient;
  String contratAbonnementRootTypeClientLib;
  String intituleRc;
  String radical;
  String agence;
  String dateSouscription;
  String etat;
  dynamic chargeCLientele;
  dynamic etatFormatted;
  RelationCommercialeDto relationCommercialeDto;
  dynamic qualiteParRapportContrat;
  bool abonnementMobile;
  bool abonnementWeb;
  dynamic montantMinVirUnitWeb;
  dynamic montantMaxVirUnitWeb;
  dynamic montantVirQuotWeb;
  dynamic nbrMaxVirQuotWeb;
  dynamic montantMinVirUnitMobile;
  dynamic montantMaxVirUnitMobile;
  dynamic montantVirQuotMobile;
  dynamic nbrMaxVirQuotMobile;
  dynamic montantVirMassQuotMobile;
  dynamic montantVirQuotTous;
  dynamic montantMaxVirUnitTous;
  dynamic nbrMaxVirQuotTous;
  dynamic montantMinVirDevUnitWeb;
  dynamic montantMaxVirDevUnitWeb;
  dynamic montantVirDevQuotWeb;
  dynamic nbrMaxVirDevQuotWeb;
  dynamic montantMinVirDevUnitMobile;
  dynamic montantMaxVirDevUnitMobile;
  dynamic montantVirDevQuotMobile;
  dynamic nbrMaxVirDevQuotMobile;
  dynamic montantVirDevQuotTous;
  dynamic montantMaxVirDevUnitTous;
  dynamic nbrMaxVirDevQuotTous;
  dynamic montantMinVirMassUnitWeb;
  dynamic montantMaxVirMassUnitWeb;
  dynamic montantVirMassQuotWeb;
  dynamic nbrMaxVirMassQuotWeb;
  dynamic montantMinVirMassUnitMobile;
  dynamic montantMaxVirMassUnitMobile;
  dynamic nbrMaxVirMassQuotMobile;
  dynamic montantVirMassQuotTous;
  dynamic montantMaxVirMassUnitTous;
  dynamic nbrMaxVirMassQuotTous;
  dynamic montantTousVirQuotTous;
  dynamic nbrMaxCheqTous;
  dynamic nbrMaxLcnTous;
  dynamic nbrMaxBenMadTous;
  dynamic nbrMaxBenDomTous;
  dynamic nbrMaxBenRechTous;
  dynamic nbrMaxBenDevTous;
  dynamic montantMinVcnssUnitWeb;
  dynamic montantMaxVcnssUnitWeb;
  dynamic montantMinVvmUnitWeb;
  dynamic montantMaxVvmUnitWeb;
  dynamic montantMinVppUnitWeb;
  dynamic montantMaxVppUnitWeb;
  dynamic montantMinVvbUnitWeb;
  dynamic montantMaxVvbUnitWeb;
  dynamic montantMinVccUnitWeb;
  dynamic montantMaxVccUnitWeb;
  dynamic montantVccQuotWeb;
  dynamic nbrMaxVccQuotWeb;
  dynamic montantVvbQuotWeb;
  dynamic nbrMaxVvbQuotWeb;
  dynamic montantVppQuotWeb;
  dynamic nbrMaxVppQuotWeb;
  dynamic montantVvmQuotWeb;
  dynamic nbrMaxVvmQuotWeb;
  dynamic montantVcnssQuotWeb;
  dynamic nbrMaxVcnssQuotWeb;
  dynamic montantMinVcnssUnitMobile;
  dynamic montantMaxVcnssUnitMobile;
  dynamic montantVcnssQuotMobile;
  dynamic nbrMaxVcnssQuotMobile;
  dynamic montantMinVvmUnitMobile;
  dynamic montantMaxVvmUnitMobile;
  dynamic montantVvmQuotMobile;
  dynamic nbrMaxVvmQuotMobile;
  dynamic montantMinVppUnitMobile;
  dynamic montantMaxVppUnitMobile;
  dynamic montantVppQuotMobile;
  dynamic nbrMaxVppQuotMobile;
  dynamic montantMinVvbUnitMobile;
  dynamic montantMaxVvbUnitMobile;
  dynamic montantVvbQuotMobile;
  dynamic nbrMaxVvbQuotMobile;
  dynamic montantMinVccUnitMobile;
  dynamic montantMaxVccUnitMobile;
  dynamic montantVccQuotMobile;
  dynamic nbrMaxVccQuotMobile;
  dynamic montantVcnssQuotTous;
  dynamic montantMaxVcnssUnitTous;
  dynamic nbrMaxVcnssQuotTous;
  dynamic montantVvmQuotTous;
  dynamic montantMaxVvmUnitTous;
  dynamic nbrMaxVvmQuotTous;
  dynamic montantVppQuotTous;
  dynamic montantMaxVppUnitTous;
  dynamic nbrMaxVppQuotTous;
  dynamic montantVvbQuotTous;
  dynamic montantMaxVvbUnitTous;
  dynamic nbrMaxVvbQuotTous;
  dynamic montantVccQuotTous;
  dynamic montantMaxVccUnitTous;
  dynamic nbrMaxVccQuotTous;
  dynamic montantMinDpfUnitWeb;
  dynamic montantMaxDpfUnitWeb;
  dynamic montantDpfQuotWeb;
  dynamic nbrMaxDpfQuotWeb;
  dynamic montantMinDpfUnitMobile;
  dynamic montantMaxDpfUnitMobile;
  dynamic montantDpfQuotMobile;
  dynamic nbrMaxDpfQuotMobile;
  dynamic montantDpfQuotTous;
  dynamic montantMaxDpfUnitTous;
  dynamic nbrMaxDpfQuotTous;
  dynamic montantMinDrgUnitWeb;
  dynamic montantMaxDrgUnitWeb;
  dynamic montantDrgQuotWeb;
  dynamic nbrMaxDrgQuotWeb;
  dynamic montantMinDrgUnitMobile;
  dynamic montantMaxDrgUnitMobile;
  dynamic montantDrgQuotMobile;
  dynamic nbrMaxDrgQuotMobile;
  dynamic montantDrgQuotTous;
  dynamic montantMaxDrgUnitTous;
  dynamic nbrMaxDrgQuotTous;
  dynamic dateCreation;
  String typeClientLib;

  factory ContratAbonnementRoot.fromJson(Map<String, dynamic> json) =>
      ContratAbonnementRoot(
        id: json["id"],
        lang: json["lang"],
        datePattern: json["datePattern"],
        max: json["max"],
        page: json["page"],
        sort: json["sort"],
        order: json["order"],
        codeBanque: json["codeBanque"],
        userId: json["userId"],
        version: json["version"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        codePaysAssocie: json["codePaysAssocie"],
        identifiantContrat: json["identifiantContrat"],
        produitSouscription: json["produitSouscription"],
        nombreAbonnes: json["nombreAbonnes"],
        typeClient: json["typeClient"],
        contratAbonnementRootTypeClientLib: json["typeClient_lib"],
        intituleRc: json["intituleRC"],
        radical: json["radical"],
        agence: json["agence"],
        dateSouscription: json["dateSouscription"],
        etat: json["etat"],
        chargeCLientele: json["chargeCLientele"],
        etatFormatted: json["etatFormatted"],
        relationCommercialeDto:
            RelationCommercialeDto.fromJson(json["relationCommercialeDTO"]),
        qualiteParRapportContrat: json["qualiteParRapportContrat"],
        abonnementMobile: json["abonnementMobile"],
        abonnementWeb: json["abonnementWeb"],
        montantMinVirUnitWeb: json["montantMinVirUnitWeb"],
        montantMaxVirUnitWeb: json["montantMaxVirUnitWeb"],
        montantVirQuotWeb: json["montantVirQuotWeb"],
        nbrMaxVirQuotWeb: json["nbrMaxVirQuotWeb"],
        montantMinVirUnitMobile: json["montantMinVirUnitMobile"],
        montantMaxVirUnitMobile: json["montantMaxVirUnitMobile"],
        montantVirQuotMobile: json["montantVirQuotMobile"],
        nbrMaxVirQuotMobile: json["nbrMaxVirQuotMobile"],
        montantVirMassQuotMobile: json["montantVirMassQuotMobile"],
        montantVirQuotTous: json["montantVirQuotTous"],
        montantMaxVirUnitTous: json["montantMaxVirUnitTous"],
        nbrMaxVirQuotTous: json["nbrMaxVirQuotTous"],
        montantMinVirDevUnitWeb: json["montantMinVirDevUnitWeb"],
        montantMaxVirDevUnitWeb: json["montantMaxVirDevUnitWeb"],
        montantVirDevQuotWeb: json["montantVirDevQuotWeb"],
        nbrMaxVirDevQuotWeb: json["nbrMaxVirDevQuotWeb"],
        montantMinVirDevUnitMobile: json["montantMinVirDevUnitMobile"],
        montantMaxVirDevUnitMobile: json["montantMaxVirDevUnitMobile"],
        montantVirDevQuotMobile: json["montantVirDevQuotMobile"],
        nbrMaxVirDevQuotMobile: json["nbrMaxVirDevQuotMobile"],
        montantVirDevQuotTous: json["montantVirDevQuotTous"],
        montantMaxVirDevUnitTous: json["montantMaxVirDevUnitTous"],
        nbrMaxVirDevQuotTous: json["nbrMaxVirDevQuotTous"],
        montantMinVirMassUnitWeb: json["montantMinVirMassUnitWeb"],
        montantMaxVirMassUnitWeb: json["montantMaxVirMassUnitWeb"],
        montantVirMassQuotWeb: json["montantVirMassQuotWeb"],
        nbrMaxVirMassQuotWeb: json["nbrMaxVirMassQuotWeb"],
        montantMinVirMassUnitMobile: json["montantMinVirMassUnitMobile"],
        montantMaxVirMassUnitMobile: json["montantMaxVirMassUnitMobile"],
        nbrMaxVirMassQuotMobile: json["nbrMaxVirMassQuotMobile"],
        montantVirMassQuotTous: json["montantVirMassQuotTous"],
        montantMaxVirMassUnitTous: json["montantMaxVirMassUnitTous"],
        nbrMaxVirMassQuotTous: json["nbrMaxVirMassQuotTous"],
        montantTousVirQuotTous: json["montantTousVirQuotTous"],
        nbrMaxCheqTous: json["nbrMaxCheqTous"],
        nbrMaxLcnTous: json["nbrMaxLCNTous"],
        nbrMaxBenMadTous: json["nbrMaxBenMADTous"],
        nbrMaxBenDomTous: json["nbrMaxBenDomTous"],
        nbrMaxBenRechTous: json["nbrMaxBenRechTous"],
        nbrMaxBenDevTous: json["nbrMaxBenDevTous"],
        montantMinVcnssUnitWeb: json["montantMinVCNSSUnitWeb"],
        montantMaxVcnssUnitWeb: json["montantMaxVCNSSUnitWeb"],
        montantMinVvmUnitWeb: json["montantMinVVMUnitWeb"],
        montantMaxVvmUnitWeb: json["montantMaxVVMUnitWeb"],
        montantMinVppUnitWeb: json["montantMinVPPUnitWeb"],
        montantMaxVppUnitWeb: json["montantMaxVPPUnitWeb"],
        montantMinVvbUnitWeb: json["montantMinVVBUnitWeb"],
        montantMaxVvbUnitWeb: json["montantMaxVVBUnitWeb"],
        montantMinVccUnitWeb: json["montantMinVCCUnitWeb"],
        montantMaxVccUnitWeb: json["montantMaxVCCUnitWeb"],
        montantVccQuotWeb: json["montantVCCQuotWeb"],
        nbrMaxVccQuotWeb: json["nbrMaxVCCQuotWeb"],
        montantVvbQuotWeb: json["montantVVBQuotWeb"],
        nbrMaxVvbQuotWeb: json["nbrMaxVVBQuotWeb"],
        montantVppQuotWeb: json["montantVPPQuotWeb"],
        nbrMaxVppQuotWeb: json["nbrMaxVPPQuotWeb"],
        montantVvmQuotWeb: json["montantVVMQuotWeb"],
        nbrMaxVvmQuotWeb: json["nbrMaxVVMQuotWeb"],
        montantVcnssQuotWeb: json["montantVCNSSQuotWeb"],
        nbrMaxVcnssQuotWeb: json["nbrMaxVCNSSQuotWeb"],
        montantMinVcnssUnitMobile: json["montantMinVCNSSUnitMobile"],
        montantMaxVcnssUnitMobile: json["montantMaxVCNSSUnitMobile"],
        montantVcnssQuotMobile: json["montantVCNSSQuotMobile"],
        nbrMaxVcnssQuotMobile: json["nbrMaxVCNSSQuotMobile"],
        montantMinVvmUnitMobile: json["montantMinVVMUnitMobile"],
        montantMaxVvmUnitMobile: json["montantMaxVVMUnitMobile"],
        montantVvmQuotMobile: json["montantVVMQuotMobile"],
        nbrMaxVvmQuotMobile: json["nbrMaxVVMQuotMobile"],
        montantMinVppUnitMobile: json["montantMinVPPUnitMobile"],
        montantMaxVppUnitMobile: json["montantMaxVPPUnitMobile"],
        montantVppQuotMobile: json["montantVPPQuotMobile"],
        nbrMaxVppQuotMobile: json["nbrMaxVPPQuotMobile"],
        montantMinVvbUnitMobile: json["montantMinVVBUnitMobile"],
        montantMaxVvbUnitMobile: json["montantMaxVVBUnitMobile"],
        montantVvbQuotMobile: json["montantVVBQuotMobile"],
        nbrMaxVvbQuotMobile: json["nbrMaxVVBQuotMobile"],
        montantMinVccUnitMobile: json["montantMinVCCUnitMobile"],
        montantMaxVccUnitMobile: json["montantMaxVCCUnitMobile"],
        montantVccQuotMobile: json["montantVCCQuotMobile"],
        nbrMaxVccQuotMobile: json["nbrMaxVCCQuotMobile"],
        montantVcnssQuotTous: json["montantVCNSSQuotTous"],
        montantMaxVcnssUnitTous: json["montantMaxVCNSSUnitTous"],
        nbrMaxVcnssQuotTous: json["nbrMaxVCNSSQuotTous"],
        montantVvmQuotTous: json["montantVVMQuotTous"],
        montantMaxVvmUnitTous: json["montantMaxVVMUnitTous"],
        nbrMaxVvmQuotTous: json["nbrMaxVVMQuotTous"],
        montantVppQuotTous: json["montantVPPQuotTous"],
        montantMaxVppUnitTous: json["montantMaxVPPUnitTous"],
        nbrMaxVppQuotTous: json["nbrMaxVPPQuotTous"],
        montantVvbQuotTous: json["montantVVBQuotTous"],
        montantMaxVvbUnitTous: json["montantMaxVVBUnitTous"],
        nbrMaxVvbQuotTous: json["nbrMaxVVBQuotTous"],
        montantVccQuotTous: json["montantVCCQuotTous"],
        montantMaxVccUnitTous: json["montantMaxVCCUnitTous"],
        nbrMaxVccQuotTous: json["nbrMaxVCCQuotTous"],
        montantMinDpfUnitWeb: json["montantMinDPFUnitWeb"],
        montantMaxDpfUnitWeb: json["montantMaxDPFUnitWeb"],
        montantDpfQuotWeb: json["montantDPFQuotWeb"],
        nbrMaxDpfQuotWeb: json["nbrMaxDPFQuotWeb"],
        montantMinDpfUnitMobile: json["montantMinDPFUnitMobile"],
        montantMaxDpfUnitMobile: json["montantMaxDPFUnitMobile"],
        montantDpfQuotMobile: json["montantDPFQuotMobile"],
        nbrMaxDpfQuotMobile: json["nbrMaxDPFQuotMobile"],
        montantDpfQuotTous: json["montantDPFQuotTous"],
        montantMaxDpfUnitTous: json["montantMaxDPFUnitTous"],
        nbrMaxDpfQuotTous: json["nbrMaxDPFQuotTous"],
        montantMinDrgUnitWeb: json["montantMinDRGUnitWeb"],
        montantMaxDrgUnitWeb: json["montantMaxDRGUnitWeb"],
        montantDrgQuotWeb: json["montantDRGQuotWeb"],
        nbrMaxDrgQuotWeb: json["nbrMaxDRGQuotWeb"],
        montantMinDrgUnitMobile: json["montantMinDRGUnitMobile"],
        montantMaxDrgUnitMobile: json["montantMaxDRGUnitMobile"],
        montantDrgQuotMobile: json["montantDRGQuotMobile"],
        nbrMaxDrgQuotMobile: json["nbrMaxDRGQuotMobile"],
        montantDrgQuotTous: json["montantDRGQuotTous"],
        montantMaxDrgUnitTous: json["montantMaxDRGUnitTous"],
        nbrMaxDrgQuotTous: json["nbrMaxDRGQuotTous"],
        dateCreation: json["dateCreation"],
        typeClientLib: json["typeClientLib"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lang": lang,
        "datePattern": datePattern,
        "max": max,
        "page": page,
        "sort": sort,
        "order": order,
        "codeBanque": codeBanque,
        "userId": userId,
        "version": version,
        "codeBanqueAssocie": codeBanqueAssocie,
        "codePaysAssocie": codePaysAssocie,
        "identifiantContrat": identifiantContrat,
        "produitSouscription": produitSouscription,
        "nombreAbonnes": nombreAbonnes,
        "typeClient": typeClient,
        "typeClient_lib": contratAbonnementRootTypeClientLib,
        "intituleRC": intituleRc,
        "radical": radical,
        "agence": agence,
        "dateSouscription": dateSouscription,
        "etat": etat,
        "chargeCLientele": chargeCLientele,
        "etatFormatted": etatFormatted,
        "relationCommercialeDTO": relationCommercialeDto.toJson(),
        "qualiteParRapportContrat": qualiteParRapportContrat,
        "abonnementMobile": abonnementMobile,
        "abonnementWeb": abonnementWeb,
        "montantMinVirUnitWeb": montantMinVirUnitWeb,
        "montantMaxVirUnitWeb": montantMaxVirUnitWeb,
        "montantVirQuotWeb": montantVirQuotWeb,
        "nbrMaxVirQuotWeb": nbrMaxVirQuotWeb,
        "montantMinVirUnitMobile": montantMinVirUnitMobile,
        "montantMaxVirUnitMobile": montantMaxVirUnitMobile,
        "montantVirQuotMobile": montantVirQuotMobile,
        "nbrMaxVirQuotMobile": nbrMaxVirQuotMobile,
        "montantVirMassQuotMobile": montantVirMassQuotMobile,
        "montantVirQuotTous": montantVirQuotTous,
        "montantMaxVirUnitTous": montantMaxVirUnitTous,
        "nbrMaxVirQuotTous": nbrMaxVirQuotTous,
        "montantMinVirDevUnitWeb": montantMinVirDevUnitWeb,
        "montantMaxVirDevUnitWeb": montantMaxVirDevUnitWeb,
        "montantVirDevQuotWeb": montantVirDevQuotWeb,
        "nbrMaxVirDevQuotWeb": nbrMaxVirDevQuotWeb,
        "montantMinVirDevUnitMobile": montantMinVirDevUnitMobile,
        "montantMaxVirDevUnitMobile": montantMaxVirDevUnitMobile,
        "montantVirDevQuotMobile": montantVirDevQuotMobile,
        "nbrMaxVirDevQuotMobile": nbrMaxVirDevQuotMobile,
        "montantVirDevQuotTous": montantVirDevQuotTous,
        "montantMaxVirDevUnitTous": montantMaxVirDevUnitTous,
        "nbrMaxVirDevQuotTous": nbrMaxVirDevQuotTous,
        "montantMinVirMassUnitWeb": montantMinVirMassUnitWeb,
        "montantMaxVirMassUnitWeb": montantMaxVirMassUnitWeb,
        "montantVirMassQuotWeb": montantVirMassQuotWeb,
        "nbrMaxVirMassQuotWeb": nbrMaxVirMassQuotWeb,
        "montantMinVirMassUnitMobile": montantMinVirMassUnitMobile,
        "montantMaxVirMassUnitMobile": montantMaxVirMassUnitMobile,
        "nbrMaxVirMassQuotMobile": nbrMaxVirMassQuotMobile,
        "montantVirMassQuotTous": montantVirMassQuotTous,
        "montantMaxVirMassUnitTous": montantMaxVirMassUnitTous,
        "nbrMaxVirMassQuotTous": nbrMaxVirMassQuotTous,
        "montantTousVirQuotTous": montantTousVirQuotTous,
        "nbrMaxCheqTous": nbrMaxCheqTous,
        "nbrMaxLCNTous": nbrMaxLcnTous,
        "nbrMaxBenMADTous": nbrMaxBenMadTous,
        "nbrMaxBenDomTous": nbrMaxBenDomTous,
        "nbrMaxBenRechTous": nbrMaxBenRechTous,
        "nbrMaxBenDevTous": nbrMaxBenDevTous,
        "montantMinVCNSSUnitWeb": montantMinVcnssUnitWeb,
        "montantMaxVCNSSUnitWeb": montantMaxVcnssUnitWeb,
        "montantMinVVMUnitWeb": montantMinVvmUnitWeb,
        "montantMaxVVMUnitWeb": montantMaxVvmUnitWeb,
        "montantMinVPPUnitWeb": montantMinVppUnitWeb,
        "montantMaxVPPUnitWeb": montantMaxVppUnitWeb,
        "montantMinVVBUnitWeb": montantMinVvbUnitWeb,
        "montantMaxVVBUnitWeb": montantMaxVvbUnitWeb,
        "montantMinVCCUnitWeb": montantMinVccUnitWeb,
        "montantMaxVCCUnitWeb": montantMaxVccUnitWeb,
        "montantVCCQuotWeb": montantVccQuotWeb,
        "nbrMaxVCCQuotWeb": nbrMaxVccQuotWeb,
        "montantVVBQuotWeb": montantVvbQuotWeb,
        "nbrMaxVVBQuotWeb": nbrMaxVvbQuotWeb,
        "montantVPPQuotWeb": montantVppQuotWeb,
        "nbrMaxVPPQuotWeb": nbrMaxVppQuotWeb,
        "montantVVMQuotWeb": montantVvmQuotWeb,
        "nbrMaxVVMQuotWeb": nbrMaxVvmQuotWeb,
        "montantVCNSSQuotWeb": montantVcnssQuotWeb,
        "nbrMaxVCNSSQuotWeb": nbrMaxVcnssQuotWeb,
        "montantMinVCNSSUnitMobile": montantMinVcnssUnitMobile,
        "montantMaxVCNSSUnitMobile": montantMaxVcnssUnitMobile,
        "montantVCNSSQuotMobile": montantVcnssQuotMobile,
        "nbrMaxVCNSSQuotMobile": nbrMaxVcnssQuotMobile,
        "montantMinVVMUnitMobile": montantMinVvmUnitMobile,
        "montantMaxVVMUnitMobile": montantMaxVvmUnitMobile,
        "montantVVMQuotMobile": montantVvmQuotMobile,
        "nbrMaxVVMQuotMobile": nbrMaxVvmQuotMobile,
        "montantMinVPPUnitMobile": montantMinVppUnitMobile,
        "montantMaxVPPUnitMobile": montantMaxVppUnitMobile,
        "montantVPPQuotMobile": montantVppQuotMobile,
        "nbrMaxVPPQuotMobile": nbrMaxVppQuotMobile,
        "montantMinVVBUnitMobile": montantMinVvbUnitMobile,
        "montantMaxVVBUnitMobile": montantMaxVvbUnitMobile,
        "montantVVBQuotMobile": montantVvbQuotMobile,
        "nbrMaxVVBQuotMobile": nbrMaxVvbQuotMobile,
        "montantMinVCCUnitMobile": montantMinVccUnitMobile,
        "montantMaxVCCUnitMobile": montantMaxVccUnitMobile,
        "montantVCCQuotMobile": montantVccQuotMobile,
        "nbrMaxVCCQuotMobile": nbrMaxVccQuotMobile,
        "montantVCNSSQuotTous": montantVcnssQuotTous,
        "montantMaxVCNSSUnitTous": montantMaxVcnssUnitTous,
        "nbrMaxVCNSSQuotTous": nbrMaxVcnssQuotTous,
        "montantVVMQuotTous": montantVvmQuotTous,
        "montantMaxVVMUnitTous": montantMaxVvmUnitTous,
        "nbrMaxVVMQuotTous": nbrMaxVvmQuotTous,
        "montantVPPQuotTous": montantVppQuotTous,
        "montantMaxVPPUnitTous": montantMaxVppUnitTous,
        "nbrMaxVPPQuotTous": nbrMaxVppQuotTous,
        "montantVVBQuotTous": montantVvbQuotTous,
        "montantMaxVVBUnitTous": montantMaxVvbUnitTous,
        "nbrMaxVVBQuotTous": nbrMaxVvbQuotTous,
        "montantVCCQuotTous": montantVccQuotTous,
        "montantMaxVCCUnitTous": montantMaxVccUnitTous,
        "nbrMaxVCCQuotTous": nbrMaxVccQuotTous,
        "montantMinDPFUnitWeb": montantMinDpfUnitWeb,
        "montantMaxDPFUnitWeb": montantMaxDpfUnitWeb,
        "montantDPFQuotWeb": montantDpfQuotWeb,
        "nbrMaxDPFQuotWeb": nbrMaxDpfQuotWeb,
        "montantMinDPFUnitMobile": montantMinDpfUnitMobile,
        "montantMaxDPFUnitMobile": montantMaxDpfUnitMobile,
        "montantDPFQuotMobile": montantDpfQuotMobile,
        "nbrMaxDPFQuotMobile": nbrMaxDpfQuotMobile,
        "montantDPFQuotTous": montantDpfQuotTous,
        "montantMaxDPFUnitTous": montantMaxDpfUnitTous,
        "nbrMaxDPFQuotTous": nbrMaxDpfQuotTous,
        "montantMinDRGUnitWeb": montantMinDrgUnitWeb,
        "montantMaxDRGUnitWeb": montantMaxDrgUnitWeb,
        "montantDRGQuotWeb": montantDrgQuotWeb,
        "nbrMaxDRGQuotWeb": nbrMaxDrgQuotWeb,
        "montantMinDRGUnitMobile": montantMinDrgUnitMobile,
        "montantMaxDRGUnitMobile": montantMaxDrgUnitMobile,
        "montantDRGQuotMobile": montantDrgQuotMobile,
        "nbrMaxDRGQuotMobile": nbrMaxDrgQuotMobile,
        "montantDRGQuotTous": montantDrgQuotTous,
        "montantMaxDRGUnitTous": montantMaxDrgUnitTous,
        "nbrMaxDRGQuotTous": nbrMaxDrgQuotTous,
        "dateCreation": dateCreation,
        "typeClientLib": typeClientLib,
      };
}

class RelationCommercialeDto {
  RelationCommercialeDto({
    this.id,
    this.lang,
    this.datePattern,
    this.max,
    this.page,
    this.sort,
    this.order,
    this.codeBanque,
    this.userId,
    this.version,
    this.codeBanqueAssocie,
    this.codePaysAssocie,
    this.map,
    this.radical,
    this.intitule,
    this.dateEntreeRelation,
    this.dateEntreeRelationLibelle,
    this.beanDateEntreeRelation,
    this.photo,
    this.agenceAffectationCommerciale,
    this.agenceAffectationCommercialeLibelle,
    this.portefeuilleSuiviCommercial,
    this.portefeuilleSuiviCommercialLibelle,
    this.segment,
    this.segmentLibelle,
    this.compteId,
    this.comptes,
    this.listLength,
    this.agenceComptabilisation,
    this.agenceSuiviQuotidien,
    this.portefeuilleRecouvrement,
    this.portefeuilleSuiviQuotidien,
    this.agenceComptabilisationLibelle,
    this.agenceSuiviQuotidienLibelle,
    this.portefeuilleRecouvrementLibelle,
    this.portefeuilleSuiviQuotidienLibelle,
    this.langue,
    this.langueLibelle,
    this.conventionGroup,
    this.groupe,
    this.instanceFermeture,
    this.relationCloturee,
    this.motifCloture,
    this.utilisateurPlacantFermeture,
    this.cloturant,
    this.interdictionCarteBancaire,
    this.interdictionChequier,
    this.adresse1,
    this.adresse2,
    this.adresse3,
    this.codePostale,
    this.ville,
    this.pays,
    this.villeLibelle,
    this.paysLibelle,
    this.telephone,
    this.tiersId,
    this.intituleTiers,
    this.photoClient,
    this.photoExist,
    this.fichierjointes,
    this.descriptionpj,
    this.numeroRegistreCommerce,
    this.numeroIfNational,
    this.tradeFinanceBanqueId,
  });

  int id;
  dynamic lang;
  dynamic datePattern;
  dynamic max;
  dynamic page;
  dynamic sort;
  dynamic order;
  dynamic codeBanque;
  dynamic userId;
  int version;
  String codeBanqueAssocie;
  String codePaysAssocie;
  RelationCommercialeDtoMap map;
  String radical;
  String intitule;
  dynamic dateEntreeRelation;
  dynamic dateEntreeRelationLibelle;
  dynamic beanDateEntreeRelation;
  dynamic photo;
  dynamic agenceAffectationCommerciale;
  dynamic agenceAffectationCommercialeLibelle;
  dynamic portefeuilleSuiviCommercial;
  dynamic portefeuilleSuiviCommercialLibelle;
  dynamic segment;
  dynamic segmentLibelle;
  dynamic compteId;
  dynamic comptes;
  int listLength;
  dynamic agenceComptabilisation;
  dynamic agenceSuiviQuotidien;
  dynamic portefeuilleRecouvrement;
  dynamic portefeuilleSuiviQuotidien;
  dynamic agenceComptabilisationLibelle;
  dynamic agenceSuiviQuotidienLibelle;
  dynamic portefeuilleRecouvrementLibelle;
  dynamic portefeuilleSuiviQuotidienLibelle;
  dynamic langue;
  dynamic langueLibelle;
  dynamic conventionGroup;
  dynamic groupe;
  dynamic instanceFermeture;
  dynamic relationCloturee;
  dynamic motifCloture;
  dynamic utilisateurPlacantFermeture;
  dynamic cloturant;
  dynamic interdictionCarteBancaire;
  dynamic interdictionChequier;
  dynamic adresse1;
  dynamic adresse2;
  dynamic adresse3;
  dynamic codePostale;
  dynamic ville;
  dynamic pays;
  dynamic villeLibelle;
  dynamic paysLibelle;
  dynamic telephone;
  dynamic tiersId;
  dynamic intituleTiers;
  dynamic photoClient;
  bool photoExist;
  List<dynamic> fichierjointes;
  List<dynamic> descriptionpj;
  dynamic numeroRegistreCommerce;
  dynamic numeroIfNational;
  dynamic tradeFinanceBanqueId;

  factory RelationCommercialeDto.fromJson(Map<String, dynamic> json) =>
      RelationCommercialeDto(
        id: json["id"],
        lang: json["lang"],
        datePattern: json["datePattern"],
        max: json["max"],
        page: json["page"],
        sort: json["sort"],
        order: json["order"],
        codeBanque: json["codeBanque"],
        userId: json["userId"],
        version: json["version"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        codePaysAssocie: json["codePaysAssocie"],
        map: RelationCommercialeDtoMap.fromJson(json["map"]),
        radical: json["radical"],
        intitule: json["intitule"],
        dateEntreeRelation: json["dateEntreeRelation"],
        dateEntreeRelationLibelle: json["dateEntreeRelationLibelle"],
        beanDateEntreeRelation: json["beanDateEntreeRelation"],
        photo: json["photo"],
        agenceAffectationCommerciale: json["agenceAffectationCommerciale"],
        agenceAffectationCommercialeLibelle:
            json["agenceAffectationCommercialeLibelle"],
        portefeuilleSuiviCommercial: json["portefeuilleSuiviCommercial"],
        portefeuilleSuiviCommercialLibelle:
            json["portefeuilleSuiviCommercialLibelle"],
        segment: json["segment"],
        segmentLibelle: json["segmentLibelle"],
        compteId: json["compteId"],
        comptes: json["comptes"],
        listLength: json["listLength"],
        agenceComptabilisation: json["agenceComptabilisation"],
        agenceSuiviQuotidien: json["agenceSuiviQuotidien"],
        portefeuilleRecouvrement: json["portefeuilleRecouvrement"],
        portefeuilleSuiviQuotidien: json["portefeuilleSuiviQuotidien"],
        agenceComptabilisationLibelle: json["agenceComptabilisationLibelle"],
        agenceSuiviQuotidienLibelle: json["agenceSuiviQuotidienLibelle"],
        portefeuilleRecouvrementLibelle:
            json["portefeuilleRecouvrementLibelle"],
        portefeuilleSuiviQuotidienLibelle:
            json["portefeuilleSuiviQuotidienLibelle"],
        langue: json["langue"],
        langueLibelle: json["langueLibelle"],
        conventionGroup: json["conventionGroup"],
        groupe: json["groupe"],
        instanceFermeture: json["instanceFermeture"],
        relationCloturee: json["relationCloturee"],
        motifCloture: json["motifCloture"],
        utilisateurPlacantFermeture: json["utilisateurPlacantFermeture"],
        cloturant: json["cloturant"],
        interdictionCarteBancaire: json["interdictionCarteBancaire"],
        interdictionChequier: json["interdictionChequier"],
        adresse1: json["adresse1"],
        adresse2: json["adresse2"],
        adresse3: json["adresse3"],
        codePostale: json["codePostale"],
        ville: json["ville"],
        pays: json["pays"],
        villeLibelle: json["villeLibelle"],
        paysLibelle: json["paysLibelle"],
        telephone: json["telephone"],
        tiersId: json["tiersId"],
        intituleTiers: json["intituleTiers"],
        photoClient: json["photoClient"],
        photoExist: json["photoExist"],
        fichierjointes:
            List<dynamic>.from(json["fichierjointes"].map((x) => x)),
        descriptionpj: List<dynamic>.from(json["descriptionpj"].map((x) => x)),
        numeroRegistreCommerce: json["numeroRegistreCommerce"],
        numeroIfNational: json["numeroIFNational"],
        tradeFinanceBanqueId: json["tradeFinanceBanqueId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lang": lang,
        "datePattern": datePattern,
        "max": max,
        "page": page,
        "sort": sort,
        "order": order,
        "codeBanque": codeBanque,
        "userId": userId,
        "version": version,
        "codeBanqueAssocie": codeBanqueAssocie,
        "codePaysAssocie": codePaysAssocie,
        "map": map.toJson(),
        "radical": radical,
        "intitule": intitule,
        "dateEntreeRelation": dateEntreeRelation,
        "dateEntreeRelationLibelle": dateEntreeRelationLibelle,
        "beanDateEntreeRelation": beanDateEntreeRelation,
        "photo": photo,
        "agenceAffectationCommerciale": agenceAffectationCommerciale,
        "agenceAffectationCommercialeLibelle":
            agenceAffectationCommercialeLibelle,
        "portefeuilleSuiviCommercial": portefeuilleSuiviCommercial,
        "portefeuilleSuiviCommercialLibelle":
            portefeuilleSuiviCommercialLibelle,
        "segment": segment,
        "segmentLibelle": segmentLibelle,
        "compteId": compteId,
        "comptes": comptes,
        "listLength": listLength,
        "agenceComptabilisation": agenceComptabilisation,
        "agenceSuiviQuotidien": agenceSuiviQuotidien,
        "portefeuilleRecouvrement": portefeuilleRecouvrement,
        "portefeuilleSuiviQuotidien": portefeuilleSuiviQuotidien,
        "agenceComptabilisationLibelle": agenceComptabilisationLibelle,
        "agenceSuiviQuotidienLibelle": agenceSuiviQuotidienLibelle,
        "portefeuilleRecouvrementLibelle": portefeuilleRecouvrementLibelle,
        "portefeuilleSuiviQuotidienLibelle": portefeuilleSuiviQuotidienLibelle,
        "langue": langue,
        "langueLibelle": langueLibelle,
        "conventionGroup": conventionGroup,
        "groupe": groupe,
        "instanceFermeture": instanceFermeture,
        "relationCloturee": relationCloturee,
        "motifCloture": motifCloture,
        "utilisateurPlacantFermeture": utilisateurPlacantFermeture,
        "cloturant": cloturant,
        "interdictionCarteBancaire": interdictionCarteBancaire,
        "interdictionChequier": interdictionChequier,
        "adresse1": adresse1,
        "adresse2": adresse2,
        "adresse3": adresse3,
        "codePostale": codePostale,
        "ville": ville,
        "pays": pays,
        "villeLibelle": villeLibelle,
        "paysLibelle": paysLibelle,
        "telephone": telephone,
        "tiersId": tiersId,
        "intituleTiers": intituleTiers,
        "photoClient": photoClient,
        "photoExist": photoExist,
        "fichierjointes": List<dynamic>.from(fichierjointes.map((x) => x)),
        "descriptionpj": List<dynamic>.from(descriptionpj.map((x) => x)),
        "numeroRegistreCommerce": numeroRegistreCommerce,
        "numeroIFNational": numeroIfNational,
        "tradeFinanceBanqueId": tradeFinanceBanqueId,
      };
}

class RelationCommercialeDtoMap {
  RelationCommercialeDtoMap();

  factory RelationCommercialeDtoMap.fromJson(Map<String, dynamic> json) =>
      RelationCommercialeDtoMap();

  Map<String, dynamic> toJson() => {};
}
