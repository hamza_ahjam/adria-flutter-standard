


class CreditsStepTwo {
  MapClass map;

  CreditsStepTwo({this.map});

  CreditsStepTwo.fromJson(Map<String, dynamic> json) {
    map = json['Map'] != null ? new MapClass.fromJson(json['Map']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.map != null) {
      data['Map'] = this.map.toJson();
    }
    return data;
  }
}

class MapClass {
  List<CreditItem> list;

  MapClass({this.list});

  MapClass.fromJson(Map<String, dynamic> json) {
    if (json['list'] != null) {
      list = <CreditItem>[];
      json['list'].forEach((v) {
        list.add(new CreditItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class CreditItem {
  String code;
  int step;
  bool visible;
  bool mondatory;
  bool editable;
  String labelle;
  int length;
  String type;
  int min;
  int max;
  List<Data> data;
  String child;
  String information;
  String defaultValue;

  CreditItem(
      {this.code,
      this.step,
      this.visible,
      this.mondatory,
      this.editable,
      this.labelle,
      this.length,
      this.type,
      this.min,
      this.max,
      this.data,
      this.child,
      this.information,
      this.defaultValue});

  CreditItem.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    step = json['step'];
    visible = json['visible'];
    mondatory = json['mondatory'];
    editable = json['editable'];
    labelle = json['labelle'];
    length = json['length'];
    type = json['type'];
    min = json['min'];
    max = json['max'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    child = json['child'];
    information = json['information'];
    defaultValue = json['defaultValue'];
  }

  Map<String, dynamic> toJson() => {
        'code': code,
        'step': step,
        'visible': visible,
        'mondatory': mondatory,
        'editable': editable,
        'labelle': labelle,
        'length': length,
        'type': type,
        'min': min,
        'max': max,
        'data': data != null ? data.map((v) => v.toJson()).toList() : null,
        'child': child,
        'information': information,
        'defaultValue': defaultValue
      };
}

class Data {
  String identifiant;
  String libelle;
  String code;

  Data({this.identifiant, this.libelle, this.code});

  Data.fromJson(Map<String, dynamic> json) {
    identifiant = json['identifiant'];
    libelle = json['libelle'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() =>
      {"identifiant": identifiant, "libelle": libelle, "code": code};
}
