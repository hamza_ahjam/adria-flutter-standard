class ForcedUpgradeResponse{
  int code;
  String androidVersion;
  bool success;
  String iOSVersion;

  ForcedUpgradeResponse({this.code, this.androidVersion, this.success, this.iOSVersion});

  ForcedUpgradeResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    androidVersion = json['androidVersion'];
    success = json['success'];
    iOSVersion = json['iOSVersion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['androidVersion'] = this.androidVersion;
    data['success'] = this.success;
    data['iOSVersion'] = this.iOSVersion;
    return data;
  }
}