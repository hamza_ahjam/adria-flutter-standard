import 'package:LBA/models/mouvement.model.dart';
import 'package:intl/intl.dart';

class HistoriqueMouvements {
  int totalPages = 10;
  int totalElements;
  bool last;
  int size = 10;
  int number = 0;
  List<Mouvement> listMouvements = [];
  Map<String, List<Mouvement>> orderedlistMouvements = new Map();
  HistoriqueMouvements.fromJson(Map<String, dynamic> json)
      : listMouvements = (json['Map']['list'] as List)
            .map((m) => Mouvement.toJson(m))
            .toList();

  transform(Mode mode) {
    listMouvements.forEach((item) {
      var d = DateFormat("dd-MM-yyyy").parse(item.dateValeur);
      // print(d);
      String goupKey;
      switch (mode) {
        case Mode.MONTH:
          goupKey = "${d.year} - ${d.month}";
          break;
        case Mode.YEAR:
          goupKey = "${d.year}";
          break;
        case Mode.DAY:
          goupKey = "${d.year} - ${d.month} - ${d.day}";
          break;
        case Mode.WEEK:
          goupKey = "${d.year} - ${d.month} - ${d.weekday}";
          break;
      }

      if (orderedlistMouvements[goupKey] == null) {
        orderedlistMouvements[goupKey] = [];
      }
      orderedlistMouvements[goupKey].add(item);
    });
  }
}

enum Mode { YEAR, MONTH, DAY, WEEK }
