import 'dart:convert';

Titres titresFromJson(String str) => Titres.fromJson(json.decode(str));

String titresToJson(Titres data) => json.encode(data.toJson());

class Titres {
  MapClass map;

  Titres({
    this.map,
  });

  factory Titres.fromJson(Map<String, dynamic> json) => Titres(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  List<Titre> titres;
  bool success;
  String message;

  MapClass({this.titres, this.success, this.message});

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        titres: List<Titre>.from(json["portefeuilleTitreInstanceList"]
            .map((x) => Titre.fromJson(x))),
        success: json["success"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "portefeuilleTitreInstanceList":
            List<dynamic>.from(titres.map((x) => x.toJson())),
        "success": success,
        "message": message,
      };
}

class Titre {
  String valeurBoursiereFormatted;
  String totalCours;
  dynamic montantPRMP;
  dynamic valeurBoursiere;
  String totalFormatted;
  dynamic dernierCours;
  String prixMoyenAcquisitionFormatted;
  String pmvFormatted;
  String totalCount;
  dynamic montantValorise;
  dynamic total;
  dynamic pmv;
  String totalBrut;
  String montantPRMPFormatted;
  String montantBruteFormatted;
  dynamic quantite;
  String dateCours;
  String totalPMV;
  String devise;
  String valeurderniercoursFormatted;
  dynamic montantBrute;
  String quantiteFormatted;
  dynamic valeurderniercours;
  String nomValeurDetenue;
  String dernierCoursFormatted;
  String montantValoriseFormatted;
  String ponderation;
  dynamic prixMoyenAcquisition;

  Titre(
      {this.valeurBoursiereFormatted,
      this.totalCours,
      this.montantPRMP,
      this.valeurBoursiere,
      this.totalFormatted,
      this.dernierCours,
      this.prixMoyenAcquisitionFormatted,
      this.pmvFormatted,
      this.totalCount,
      this.montantValorise,
      this.total,
      this.pmv,
      this.totalBrut,
      this.montantPRMPFormatted,
      this.montantBruteFormatted,
      this.quantite,
      this.dateCours,
      this.totalPMV,
      this.devise,
      this.valeurderniercoursFormatted,
      this.montantBrute,
      this.quantiteFormatted,
      this.valeurderniercours,
      this.nomValeurDetenue,
      this.dernierCoursFormatted,
      this.montantValoriseFormatted,
      this.ponderation,
      this.prixMoyenAcquisition});

  Titre.fromJson(Map<String, dynamic> json) {
    valeurBoursiereFormatted = json['valeurBoursiere_formatted'];
    totalCours = json['totalCours'];
    montantPRMP = json['montantPRMP'];
    valeurBoursiere = json['valeurBoursiere'];
    totalFormatted = json['totalFormatted'];
    dernierCours = json['dernierCours'];
    prixMoyenAcquisitionFormatted = json['prixMoyenAcquisition_formatted'];
    pmvFormatted = json['pmv_formatted'];
    totalCount = json['totalCount'];
    montantValorise = json['montantValorise'];
    total = json['total'];
    pmv = json['pmv'];
    totalBrut = json['totalBrut'];
    montantPRMPFormatted = json['montantPRMP_formatted'];
    montantBruteFormatted = json['montantBrute_formatted'];
    quantite = json['quantite'];
    dateCours = json['dateCours'];
    totalPMV = json['totalPMV'];
    devise = json['devise'];
    valeurderniercoursFormatted = json['valeurderniercours_formatted'];
    montantBrute = json['montantBrute'];
    quantiteFormatted = json['quantite_formatted'];
    valeurderniercours = json['valeurderniercours'];
    nomValeurDetenue = json['nomValeurDetenue'];
    dernierCoursFormatted = json['dernierCours_formatted'];
    montantValoriseFormatted = json['montantValoriseFormatted'];
    ponderation = json['ponderation'];
    prixMoyenAcquisition = json['prixMoyenAcquisition'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['valeurBoursiere_formatted'] = this.valeurBoursiereFormatted;
    data['totalCours'] = this.totalCours;
    data['montantPRMP'] = this.montantPRMP;
    data['valeurBoursiere'] = this.valeurBoursiere;
    data['totalFormatted'] = this.totalFormatted;
    data['dernierCours'] = this.dernierCours;
    data['prixMoyenAcquisition_formatted'] = this.prixMoyenAcquisitionFormatted;
    data['pmv_formatted'] = this.pmvFormatted;
    data['totalCount'] = this.totalCount;
    data['montantValorise'] = this.montantValorise;
    data['total'] = this.total;
    data['pmv'] = this.pmv;
    data['totalBrut'] = this.totalBrut;
    data['montantPRMP_formatted'] = this.montantPRMPFormatted;
    data['montantBrute_formatted'] = this.montantBruteFormatted;
    data['quantite'] = this.quantite;
    data['dateCours'] = this.dateCours;
    data['totalPMV'] = this.totalPMV;
    data['devise'] = this.devise;
    data['valeurderniercours_formatted'] = this.valeurderniercoursFormatted;
    data['montantBrute'] = this.montantBrute;
    data['quantite_formatted'] = this.quantiteFormatted;
    data['valeurderniercours'] = this.valeurderniercours;
    data['nomValeurDetenue'] = this.nomValeurDetenue;
    data['dernierCours_formatted'] = this.dernierCoursFormatted;
    data['montantValoriseFormatted'] = this.montantValoriseFormatted;
    data['ponderation'] = this.ponderation;
    data['prixMoyenAcquisition'] = this.prixMoyenAcquisition;
    return data;
  }
}
