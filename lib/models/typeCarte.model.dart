class TypeCarte {
  MapClass map;

  TypeCarte({this.map});

  factory TypeCarte.fromJson(Map<String, dynamic> json) => TypeCarte(
    map: MapClass.fromJson(json["Map"]),
  );

  Map<String, dynamic> toJson() => {
    "Map": map.toJson(),
  };
}

class MapClass {
  List<ListTypeCarte> listTypeCarte;
  bool success;
  String message;

  MapClass({this.listTypeCarte, this.success, this.message});

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
    success: json["success"],
    message: json["message"],
    listTypeCarte: List<ListTypeCarte>.from(
        json["plist"]
            .map((x) => ListTypeCarte.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "message": message,
    "plist":
    List<dynamic>.from(listTypeCarte.map((x) => x.toJson())),
  };
}

class ListTypeCarte {
  String typeCarte;
  String typeCarteLibelle;
  bool showVisual;
  bool flagdevise;
  bool prepaye;
  bool internationnal;

  ListTypeCarte(
      {this.typeCarte,
        this.typeCarteLibelle,
        this.showVisual,
        this.flagdevise,
        this.prepaye,
        this.internationnal});

  ListTypeCarte.fromJson(Map<String, dynamic> json) {
    typeCarte = json['typeCarte'];
    typeCarteLibelle = json['typeCarte_libelle'];
    showVisual = json['showVisual'];
    flagdevise = json['flagdevise'];
    prepaye = json['prepaye'];
    internationnal = json['internationnal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['typeCarte'] = this.typeCarte;
    data['typeCarte_libelle'] = this.typeCarteLibelle;
    data['showVisual'] = this.showVisual;
    data['flagdevise'] = this.flagdevise;
    data['prepaye'] = this.prepaye;
    data['internationnal'] = this.internationnal;
    return data;
  }
}