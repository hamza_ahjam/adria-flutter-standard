// To parse this JSON data, do
//
//     final currentCarte = currentCarteFromJson(jsonString);

import 'dart:convert';

CurrentCarte currentCarteFromJson(String str) =>
    CurrentCarte.fromJson(json.decode(str));

String currentCarteToJson(CurrentCarte data) => json.encode(data.toJson());

class CurrentCarte {
  CurrentCarte({
    this.id,
    this.typeCarte,
    this.numero,
    this.numeroCompte,
    this.nomDetenteur,
    this.prenomDetenteur,
    this.statut,
    this.deviseCompte,
    this.montantCarte,
    this.retraitBanque,
    this.retraitConfrere,
    this.retraitInternationale,
    this.achatInternational,
    this.achatLocal,
    this.dateValidite,
    this.dateCreation,
    this.dateExpiration,
    this.debutValidite,
    this.codeAgencePorteur,
    this.paiementFactures,
    this.rechargeTelephonnique,
    this.modifStatut,
    this.cashAdvancedQuotidienLocal,
    this.retraitGabQuotidienLocal,
    this.achatParCarteQuotidienLocal,
    this.rechargeGsmQuotidienLocal,
    this.depotArgentQuotidienLocal,
    this.paiementFactureQuotidienLocal,
    this.virementQuotidienLocal,
    this.typeProduitCarte,
    this.cartePlafondNote,
    this.cashAdvancedMensuelLocal,
    this.retraitGabMensuelLocal,
    this.achatParCarteMensuelLocal,
    this.rechargeGsmMensuelLocal,
    this.depotArgentMensuelLocal,
    this.paiementFactureMensuelLocal,
    this.virementMensuelLocal,
    this.mouvementCartes,
    this.cashAdvancedQuotidienInternationnal,
    this.retraitGabQuotidienInternationnal,
    this.achatParCarteQuotidienInternationnal,
    this.rechargeGsmQuotidienInternationnal,
    this.depotArgentQuotidienInternationnal,
    this.paiementFactureQuotidienInternationnal,
    this.virementQuotidienInternationnal,
    this.cashAdvancedMensuelInternationnal,
    this.retraitGabMensuelInternationnal,
    this.achatParCarteMensuelInternationnal,
    this.rechargeGsmMensuelInternationnal,
    this.depotArgentMensuelInternationnal,
    this.paiementFactureMensuelInternationnal,
    this.virementMensuelInternationnal,
    this.carteImage,
    this.enCours,
    this.codePlastic,
    this.plafondRetraitBanque,
    this.plafondRetraitConfrere,
    this.plafondPaiements,
    this.plafondECommerce,
    this.plafondRetraitInternationnal,
    this.plafondPaiementInternationnal,
    this.plafondEcommerceInternationnal,
    this.plafondRetraitBanqueSg,
    this.periode,
    this.reliquatDotation,
    this.solde,
    this.porteur,
    this.dateActivation,
    this.montantDotation,
    this.plafondRetraitInternationalQuotidien,
    this.isContactLess,
    this.isInternationalBlocked,
    this.isStopList,
    this.isBlockedAll,
    this.isCanceled,
    this.isValid,
    this.rib,
    this.internationnal,
    this.prepayee,
  });

  String id;
  String typeCarte;
  String numero;
  String numeroCompte;
  dynamic nomDetenteur;
  dynamic prenomDetenteur;
  String statut;
  dynamic deviseCompte;
  dynamic montantCarte;
  dynamic retraitBanque;
  dynamic retraitConfrere;
  dynamic retraitInternationale;
  dynamic achatInternational;
  dynamic achatLocal;
  dynamic dateValidite;
  String dateCreation;
  String dateExpiration;
  dynamic debutValidite;
  dynamic codeAgencePorteur;
  dynamic paiementFactures;
  dynamic rechargeTelephonnique;
  dynamic modifStatut;
  dynamic cashAdvancedQuotidienLocal;
  dynamic retraitGabQuotidienLocal;
  dynamic achatParCarteQuotidienLocal;
  dynamic rechargeGsmQuotidienLocal;
  dynamic depotArgentQuotidienLocal;
  dynamic paiementFactureQuotidienLocal;
  dynamic virementQuotidienLocal;
  dynamic typeProduitCarte;
  dynamic cartePlafondNote;
  dynamic cashAdvancedMensuelLocal;
  dynamic retraitGabMensuelLocal;
  dynamic achatParCarteMensuelLocal;
  dynamic rechargeGsmMensuelLocal;
  dynamic depotArgentMensuelLocal;
  dynamic paiementFactureMensuelLocal;
  dynamic virementMensuelLocal;
  dynamic mouvementCartes;
  dynamic cashAdvancedQuotidienInternationnal;
  dynamic retraitGabQuotidienInternationnal;
  dynamic achatParCarteQuotidienInternationnal;
  dynamic rechargeGsmQuotidienInternationnal;
  dynamic depotArgentQuotidienInternationnal;
  dynamic paiementFactureQuotidienInternationnal;
  dynamic virementQuotidienInternationnal;
  dynamic cashAdvancedMensuelInternationnal;
  dynamic retraitGabMensuelInternationnal;
  dynamic achatParCarteMensuelInternationnal;
  dynamic rechargeGsmMensuelInternationnal;
  dynamic depotArgentMensuelInternationnal;
  dynamic paiementFactureMensuelInternationnal;
  dynamic virementMensuelInternationnal;
  String carteImage;
  dynamic enCours;
  dynamic codePlastic;
  dynamic plafondRetraitBanque;
  dynamic plafondRetraitConfrere;
  dynamic plafondPaiements;
  dynamic plafondECommerce;
  dynamic plafondRetraitInternationnal;
  dynamic plafondPaiementInternationnal;
  dynamic plafondEcommerceInternationnal;
  dynamic plafondRetraitBanqueSg;
  dynamic periode;
  String reliquatDotation;
  String solde;
  dynamic porteur;
  String dateActivation;
  dynamic montantDotation;
  dynamic plafondRetraitInternationalQuotidien;
  int isContactLess;
  int isInternationalBlocked;
  int isStopList;
  int isBlockedAll;
  int isCanceled;
  int isValid;
  String rib;
  bool internationnal;
  bool prepayee;

  factory CurrentCarte.fromJson(Map<String, dynamic> json) => CurrentCarte(
        id: json["id"],
        typeCarte: json["typeCarte"],
        numero: json["numero"],
        numeroCompte: json["numeroCompte"],
        nomDetenteur: json["nomDetenteur"],
        prenomDetenteur: json["prenomDetenteur"],
        statut: json["statut"],
        deviseCompte: json["deviseCompte"],
        montantCarte: json["montantCarte"],
        retraitBanque: json["retraitBanque"],
        retraitConfrere: json["retraitConfrere"],
        retraitInternationale: json["retraitInternationale"],
        achatInternational: json["achatInternational"],
        achatLocal: json["achatLocal"],
        dateValidite: json["dateValidite"],
        dateCreation: json["dateCreation"],
        dateExpiration: json["dateExpiration"],
        debutValidite: json["debutValidite"],
        codeAgencePorteur: json["codeAgencePorteur"],
        paiementFactures: json["paiementFactures"],
        rechargeTelephonnique: json["rechargeTelephonnique"],
        modifStatut: json["modifStatut"],
        cashAdvancedQuotidienLocal: json["cashAdvancedQuotidienLocal"],
        retraitGabQuotidienLocal: json["retraitGabQuotidienLocal"],
        achatParCarteQuotidienLocal: json["achatParCarteQuotidienLocal"],
        rechargeGsmQuotidienLocal: json["rechargeGsmQuotidienLocal"],
        depotArgentQuotidienLocal: json["depotArgentQuotidienLocal"],
        paiementFactureQuotidienLocal: json["paiementFactureQuotidienLocal"],
        virementQuotidienLocal: json["virementQuotidienLocal"],
        typeProduitCarte: json["typeProduitCarte"],
        cartePlafondNote: json["cartePlafondNote"],
        cashAdvancedMensuelLocal: json["cashAdvancedMensuelLocal"],
        retraitGabMensuelLocal: json["retraitGabMensuelLocal"],
        achatParCarteMensuelLocal: json["achatParCarteMensuelLocal"],
        rechargeGsmMensuelLocal: json["rechargeGsmMensuelLocal"],
        depotArgentMensuelLocal: json["depotArgentMensuelLocal"],
        paiementFactureMensuelLocal: json["paiementFactureMensuelLocal"],
        virementMensuelLocal: json["virementMensuelLocal"],
        mouvementCartes: json["mouvementCartes"],
        cashAdvancedQuotidienInternationnal:
            json["cashAdvancedQuotidienInternationnal"],
        retraitGabQuotidienInternationnal:
            json["retraitGabQuotidienInternationnal"],
        achatParCarteQuotidienInternationnal:
            json["achatParCarteQuotidienInternationnal"],
        rechargeGsmQuotidienInternationnal:
            json["rechargeGsmQuotidienInternationnal"],
        depotArgentQuotidienInternationnal:
            json["depotArgentQuotidienInternationnal"],
        paiementFactureQuotidienInternationnal:
            json["paiementFactureQuotidienInternationnal"],
        virementQuotidienInternationnal:
            json["virementQuotidienInternationnal"],
        cashAdvancedMensuelInternationnal:
            json["cashAdvancedMensuelInternationnal"],
        retraitGabMensuelInternationnal:
            json["retraitGabMensuelInternationnal"],
        achatParCarteMensuelInternationnal:
            json["achatParCarteMensuelInternationnal"],
        rechargeGsmMensuelInternationnal:
            json["rechargeGsmMensuelInternationnal"],
        depotArgentMensuelInternationnal:
            json["depotArgentMensuelInternationnal"],
        paiementFactureMensuelInternationnal:
            json["paiementFactureMensuelInternationnal"],
        virementMensuelInternationnal: json["virementMensuelInternationnal"],
        carteImage: json["carteImage"],
        enCours: json["enCours"],
        codePlastic: json["codePlastic"],
        plafondRetraitBanque: json["plafondRetraitBanque"],
        plafondRetraitConfrere: json["plafondRetraitConfrere"],
        plafondPaiements: json["plafondPaiements"],
        plafondECommerce: json["plafondECommerce"],
        plafondRetraitInternationnal: json["plafondRetraitInternationnal"],
        plafondPaiementInternationnal: json["plafondPaiementInternationnal"],
        plafondEcommerceInternationnal: json["plafondEcommerceInternationnal"],
        plafondRetraitBanqueSg: json["plafondRetraitBanqueSg"],
        periode: json["periode"],
        reliquatDotation: json["reliquatDotation"],
        solde: json["solde"],
        porteur: json["porteur"],
        dateActivation: json["dateActivation"],
        montantDotation: json["montantDotation"],
        plafondRetraitInternationalQuotidien:
            json["plafondRetraitInternationalQuotidien"],
        isContactLess: json["isContactLess"],
        isInternationalBlocked: json["isInternationalBlocked"],
        isStopList: json["isStopList"],
        isBlockedAll: json["isBlockedAll"],
        isCanceled: json["isCanceled"],
        isValid: json["isValid"],
        rib: json["rib"],
        internationnal: json["internationnal"],
        prepayee: json["prepayee"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "typeCarte": typeCarte,
        "numero": numero,
        "numeroCompte": numeroCompte,
        "nomDetenteur": nomDetenteur,
        "prenomDetenteur": prenomDetenteur,
        "statut": statut,
        "deviseCompte": deviseCompte,
        "montantCarte": montantCarte,
        "retraitBanque": retraitBanque,
        "retraitConfrere": retraitConfrere,
        "retraitInternationale": retraitInternationale,
        "achatInternational": achatInternational,
        "achatLocal": achatLocal,
        "dateValidite": dateValidite,
        "dateCreation": dateCreation,
        "dateExpiration": dateExpiration,
        "debutValidite": debutValidite,
        "codeAgencePorteur": codeAgencePorteur,
        "paiementFactures": paiementFactures,
        "rechargeTelephonnique": rechargeTelephonnique,
        "modifStatut": modifStatut,
        "cashAdvancedQuotidienLocal": cashAdvancedQuotidienLocal,
        "retraitGabQuotidienLocal": retraitGabQuotidienLocal,
        "achatParCarteQuotidienLocal": achatParCarteQuotidienLocal,
        "rechargeGsmQuotidienLocal": rechargeGsmQuotidienLocal,
        "depotArgentQuotidienLocal": depotArgentQuotidienLocal,
        "paiementFactureQuotidienLocal": paiementFactureQuotidienLocal,
        "virementQuotidienLocal": virementQuotidienLocal,
        "typeProduitCarte": typeProduitCarte,
        "cartePlafondNote": cartePlafondNote,
        "cashAdvancedMensuelLocal": cashAdvancedMensuelLocal,
        "retraitGabMensuelLocal": retraitGabMensuelLocal,
        "achatParCarteMensuelLocal": achatParCarteMensuelLocal,
        "rechargeGsmMensuelLocal": rechargeGsmMensuelLocal,
        "depotArgentMensuelLocal": depotArgentMensuelLocal,
        "paiementFactureMensuelLocal": paiementFactureMensuelLocal,
        "virementMensuelLocal": virementMensuelLocal,
        "mouvementCartes": mouvementCartes,
        "cashAdvancedQuotidienInternationnal":
            cashAdvancedQuotidienInternationnal,
        "retraitGabQuotidienInternationnal": retraitGabQuotidienInternationnal,
        "achatParCarteQuotidienInternationnal":
            achatParCarteQuotidienInternationnal,
        "rechargeGsmQuotidienInternationnal":
            rechargeGsmQuotidienInternationnal,
        "depotArgentQuotidienInternationnal":
            depotArgentQuotidienInternationnal,
        "paiementFactureQuotidienInternationnal":
            paiementFactureQuotidienInternationnal,
        "virementQuotidienInternationnal": virementQuotidienInternationnal,
        "cashAdvancedMensuelInternationnal": cashAdvancedMensuelInternationnal,
        "retraitGabMensuelInternationnal": retraitGabMensuelInternationnal,
        "achatParCarteMensuelInternationnal":
            achatParCarteMensuelInternationnal,
        "rechargeGsmMensuelInternationnal": rechargeGsmMensuelInternationnal,
        "depotArgentMensuelInternationnal": depotArgentMensuelInternationnal,
        "paiementFactureMensuelInternationnal":
            paiementFactureMensuelInternationnal,
        "virementMensuelInternationnal": virementMensuelInternationnal,
        "carteImage": carteImage,
        "enCours": enCours,
        "codePlastic": codePlastic,
        "plafondRetraitBanque": plafondRetraitBanque,
        "plafondRetraitConfrere": plafondRetraitConfrere,
        "plafondPaiements": plafondPaiements,
        "plafondECommerce": plafondECommerce,
        "plafondRetraitInternationnal": plafondRetraitInternationnal,
        "plafondPaiementInternationnal": plafondPaiementInternationnal,
        "plafondEcommerceInternationnal": plafondEcommerceInternationnal,
        "plafondRetraitBanqueSg": plafondRetraitBanqueSg,
        "periode": periode,
        "reliquatDotation": reliquatDotation,
        "solde": solde,
        "porteur": porteur,
        "dateActivation": dateActivation,
        "montantDotation": montantDotation,
        "plafondRetraitInternationalQuotidien":
            plafondRetraitInternationalQuotidien,
        "isContactLess": isContactLess,
        "isInternationalBlocked": isInternationalBlocked,
        "isStopList": isStopList,
        "isBlockedAll": isBlockedAll,
        "isCanceled": isCanceled,
        "isValid": isValid,
        "rib": rib,
        "internationnal": internationnal,
        "prepayee": prepayee,
      };
}
