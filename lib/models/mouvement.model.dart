class Mouvement {
  String sens;
  String codeOperation;
  String libelle;
  String dateValeur;
  String credit_;
  String debit_;
  int id;
  String dateOperation;
  int debit;
  int credit;
  String devise;

  Mouvement.toJson(Map<String, dynamic> json)
      : id = json['id'],
        sens = json['sens'],
        codeOperation = json['codeOperation'],
        libelle = json['libelle'],
        dateValeur = json['dateValeur'],
        credit_ = (json['sens'] == "C") ? json['credit_'] : '',
        debit_ = (json['sens'] == "D") ? json['debit_'] : '',
        dateOperation = json['dateOperation'],
        debit = (json['sens'] == "C") ? json['credit'] : 0,
        credit = (json['sens'] == "D") ? json['debit'] : 0,
        devise = json['devise'];
}
