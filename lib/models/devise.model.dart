// To parse this JSON data, do
//
//     final devise = deviseFromJson(jsonString);

import 'dart:convert';

Devise deviseFromJson(String str) => Devise.fromJson(json.decode(str));

String deviseToJson(Devise data) => json.encode(data.toJson());

class Devise {
  Devise({
    this.map,
  });

  MapClass map;

  factory Devise.fromJson(Map<String, dynamic> json) => Devise(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.matriceChangeList,
    this.success,
    this.matriceChangeTotal,
  });

  List<MatriceChangeList> matriceChangeList;
  bool success;
  int matriceChangeTotal;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        matriceChangeList: List<MatriceChangeList>.from(
            json["MatriceChangeList"]
                .map((x) => MatriceChangeList.fromJson(x))),
        success: json["success"],
        matriceChangeTotal: json["MatriceChangeTotal"],
      );

  Map<String, dynamic> toJson() => {
        "MatriceChangeList":
            List<dynamic>.from(matriceChangeList.map((x) => x.toJson())),
        "success": success,
        "MatriceChangeTotal": matriceChangeTotal,
      };
}

class MatriceChangeList {
  MatriceChangeList({
    this.id,
    this.code,
    this.libelle,
    this.codeAlphaCible,
    this.codeAlphaSource,
    this.dateChange,
    this.deviseCible,
    this.deviseSource,
    this.achat,
    this.vente,
    this.unite,
    this.dateChangeFormatted,
    this.originType,
    this.searchNatur,
    this.tauxChange,
  });

  dynamic id;
  dynamic code;
  String libelle;
  String codeAlphaCible;
  String codeAlphaSource;
  String dateChange;
  String deviseCible;
  String deviseSource;
  double achat;
  double vente;
  dynamic unite;
  String dateChangeFormatted;
  dynamic originType;
  dynamic searchNatur;
  dynamic tauxChange;

  factory MatriceChangeList.fromJson(Map<String, dynamic> json) =>
      MatriceChangeList(
        id: json["id"],
        code: json["code"],
        libelle: json["libelle"],
        codeAlphaCible: json["codeAlphaCible"],
        codeAlphaSource: json["codeAlphaSource"],
        dateChange: json["dateChange"],
        deviseCible: json["deviseCible"],
        deviseSource: json["deviseSource"],
        achat: json["achat"].toDouble(),
        vente: json["vente"].toDouble(),
        unite: json["unite"],
        dateChangeFormatted: json["dateChangeFormatted"],
        originType: json["originType"],
        searchNatur: json["searchNatur"],
        tauxChange: json["tauxChange"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "libelle": libelle,
        "codeAlphaCible": codeAlphaCible,
        "codeAlphaSource": codeAlphaSource,
        "dateChange": dateChange,
        "deviseCible": deviseCible,
        "deviseSource": deviseSource,
        "achat": achat,
        "vente": vente,
        "unite": unite,
        "dateChangeFormatted": dateChangeFormatted,
        "originType": originType,
        "searchNatur": searchNatur,
        "tauxChange": tauxChange,
      };
}
