// To parse this JSON data, do
//
//     final beneficiaire = beneficiaireFromJson(jsonString);

import 'dart:convert';

Beneficiaire beneficiaireFromJson(String str) =>
    Beneficiaire.fromJson(json.decode(str));

String beneficiaireToJson(Beneficiaire data) => json.encode(data.toJson());

class Beneficiaire {
  Beneficiaire({
    this.map,
  });

  MapClass map;

  factory Beneficiaire.fromJson(Map<String, dynamic> json) => Beneficiaire(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.beneficiaireList,
    this.success,
  });

  List<BeneficiaireList> beneficiaireList;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        beneficiaireList: List<BeneficiaireList>.from(
            json["beneficiaireList"].map((x) => BeneficiaireList.fromJson(x))),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "beneficiaireList":
            List<dynamic>.from(beneficiaireList.map((x) => x.toJson())),
        "success": success,
      };
}

class BeneficiaireList {
  BeneficiaireList({
    this.id,
    this.identifiant,
    this.codeProduit,
    this.nom,
    this.prenom,
    this.identifiantInterneCompte,
    this.numeroCompte,
    this.email,
    this.compteConvertible,
    this.deviseCompte,
    this.deviseCompteLibelle,
    this.type,
    this.nature,
    this.statutCode,
    this.tpiLibelle,
    this.codeSecretPlain,
    this.intitule,
    this.codeBanque,
    this.codeAgence,
    this.clientInterne,
    this.cle,
    this.version,
    this.notificationSms,
    this.notificationEmail,
    this.gsm,
    this.statut,
    this.codeBic,
    this.codeSwift,
    this.iban,
    this.bicIban,
    this.numeroCarte,
    this.typePieceIdentite,
    this.typePieceIdentiteLibelle,
    this.numeroPieceIdentite,
    this.mail,
    this.numeroGsm,
    this.address,
    this.adresse1,
    this.adresse2,
    this.adresse3,
    this.canalWeb,
    this.canalMobile,
    this.genre,
    this.genreLibelle,
    this.compte,
    this.beneficiaireListType,
    this.toStringe,
    this.convertibleLibelle,
    this.statutLibelle,
    this.natureCompte,
    this.typeCompte,
    this.deviseFormatted,
    this.carteRechargeable,
    this.codeUtilisateurAyantCree,
  });

  int id;
  String identifiant;
  dynamic codeProduit;
  dynamic nom;
  dynamic prenom;
  dynamic identifiantInterneCompte;
  String numeroCompte;
  dynamic email;
  String compteConvertible;
  String deviseCompte;
  String deviseCompteLibelle;
  String type;
  String nature;
  String statutCode;
  dynamic tpiLibelle;
  dynamic codeSecretPlain;
  String intitule;
  String codeBanque;
  dynamic codeAgence;
  dynamic clientInterne;
  dynamic cle;
  dynamic version;
  dynamic notificationSms;
  dynamic notificationEmail;
  dynamic gsm;
  String statut;
  dynamic codeBic;
  dynamic codeSwift;
  dynamic iban;
  dynamic bicIban;
  dynamic numeroCarte;
  dynamic typePieceIdentite;
  dynamic typePieceIdentiteLibelle;
  dynamic numeroPieceIdentite;
  dynamic mail;
  dynamic numeroGsm;
  dynamic address;
  dynamic adresse1;
  dynamic adresse2;
  dynamic adresse3;
  dynamic canalWeb;
  dynamic canalMobile;
  dynamic genre;
  String genreLibelle;
  String compte;
  String beneficiaireListType;
  String toStringe;
  String convertibleLibelle;
  dynamic statutLibelle;
  dynamic natureCompte;
  dynamic typeCompte;
  String deviseFormatted;
  dynamic carteRechargeable;
  dynamic codeUtilisateurAyantCree;

  factory BeneficiaireList.fromJson(Map<String, dynamic> json) =>
      BeneficiaireList(
        id: json["id"],
        identifiant: json["identifiant"],
        codeProduit: json["codeProduit"],
        nom: json["nom"],
        prenom: json["prenom"],
        identifiantInterneCompte: json["identifiantInterneCompte"],
        numeroCompte: json["numeroCompte"],
        email: json["email"],
        compteConvertible: json["compteConvertible"],
        deviseCompte: json["deviseCompte"],
        deviseCompteLibelle: json["deviseCompteLibelle"],
        type: json["type"],
        nature: json["nature"],
        statutCode: json["statutCode"],
        tpiLibelle: json["tpiLibelle"],
        codeSecretPlain: json["codeSecretPlain"],
        intitule: json["intitule"],
        codeBanque: json["codeBanque"] == null ? null : json["codeBanque"],
        codeAgence: json["codeAgence"],
        clientInterne: json["clientInterne"],
        cle: json["cle"],
        version: json["version"],
        notificationSms: json["notificationSMS"],
        notificationEmail: json["notificationEmail"],
        gsm: json["gsm"],
        statut: json["statut"],
        codeBic: json["codeBic"],
        codeSwift: json["codeSwift"],
        iban: json["iban"],
        bicIban: json["bicIban"],
        numeroCarte: json["numeroCarte"],
        typePieceIdentite: json["typePieceIdentite"],
        typePieceIdentiteLibelle: json["typePieceIdentiteLibelle"],
        numeroPieceIdentite: json["numeroPieceIdentite"],
        mail: json["mail"],
        numeroGsm: json["numeroGsm"],
        address: json["address"],
        adresse1: json["adresse1"],
        adresse2: json["adresse2"],
        adresse3: json["adresse3"],
        canalWeb: json["canalWeb"],
        canalMobile: json["canalMobile"],
        genre: json["genre"],
        genreLibelle: json["genreLibelle"],
        compte: json["compte"],
        beneficiaireListType: json["type_"],
        toStringe: json["toStringe_"],
        convertibleLibelle: json["convertible_libelle"],
        statutLibelle: json["statutLibelle"],
        natureCompte: json["natureCompte"],
        typeCompte: json["typeCompte"],
        deviseFormatted: json["deviseFormatted"],
        carteRechargeable: json["carteRechargeable"],
        codeUtilisateurAyantCree: json["codeUtilisateurAyantCree"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "identifiant": identifiant,
        "codeProduit": codeProduit,
        "nom": nom,
        "prenom": prenom,
        "identifiantInterneCompte": identifiantInterneCompte,
        "numeroCompte": numeroCompte,
        "email": email,
        "compteConvertible": compteConvertible,
        "deviseCompte": deviseCompte,
        "deviseCompteLibelle": deviseCompteLibelle,
        "type": type,
        "nature": nature,
        "statutCode": statutCode,
        "tpiLibelle": tpiLibelle,
        "codeSecretPlain": codeSecretPlain,
        "intitule": intitule,
        "codeBanque": codeBanque == null ? null : codeBanque,
        "codeAgence": codeAgence,
        "clientInterne": clientInterne,
        "cle": cle,
        "version": version,
        "notificationSMS": notificationSms,
        "notificationEmail": notificationEmail,
        "gsm": gsm,
        "statut": statut,
        "codeBic": codeBic,
        "codeSwift": codeSwift,
        "iban": iban,
        "bicIban": bicIban,
        "numeroCarte": numeroCarte,
        "typePieceIdentite": typePieceIdentite,
        "typePieceIdentiteLibelle": typePieceIdentiteLibelle,
        "numeroPieceIdentite": numeroPieceIdentite,
        "mail": mail,
        "numeroGsm": numeroGsm,
        "address": address,
        "adresse1": adresse1,
        "adresse2": adresse2,
        "adresse3": adresse3,
        "canalWeb": canalWeb,
        "canalMobile": canalMobile,
        "genre": genre,
        "genreLibelle": genreLibelle,
        "compte": compte,
        "type_": beneficiaireListType,
        "toStringe_": toStringe,
        "convertible_libelle": convertibleLibelle,
        "statutLibelle": statutLibelle,
        "natureCompte": natureCompte,
        "typeCompte": typeCompte,
        "deviseFormatted": deviseFormatted,
        "carteRechargeable": carteRechargeable,
        "codeUtilisateurAyantCree": codeUtilisateurAyantCree,
      };
}
