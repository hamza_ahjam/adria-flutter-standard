class CreditHabitatForm {
  MapClass map;

  CreditHabitatForm({this.map});

  CreditHabitatForm.fromJson(Map<String, dynamic> json) {
    map = json['Map'] != null ? new MapClass.fromJson(json['Map']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.map != null) {
      data['Map'] = this.map.toJson();
    }
    return data;
  }
}

class MapClass {
  List<ListItems> list;

  MapClass({this.list});

  MapClass.fromJson(Map<String, dynamic> json) {
    if (json['list'] != null) {
      list = <ListItems>[];
      json['list'].forEach((v) {
        list.add(new ListItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListItems {
  String code;
  int step;
  bool visible;
  bool mondatory;
  bool editable;
  String labelle;
  String information;
  int length;
  String type;
  int min;
  int max;
  String child;
  List<Data> data;
  String defaultValue;

  ListItems(
      {this.code,
        this.step,
        this.visible,
        this.mondatory,
        this.editable,
        this.labelle,
        this.information,
        this.length,
        this.type,
        this.min,
        this.max,
        this.child,
        this.data,
        this.defaultValue});

  ListItems.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    step = json['step'];
    visible = json['visible'];
    mondatory = json['mondatory'];
    editable = json['editable'];
    labelle = json['labelle'];
    information = json['information'];
    length = json['length'];
    type = json['type'];
    min = json['min'];
    max = json['max'];
    child = json['child'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    defaultValue = json['defaultValue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['step'] = this.step;
    data['visible'] = this.visible;
    data['mondatory'] = this.mondatory;
    data['editable'] = this.editable;
    data['labelle'] = this.labelle;
    data['information'] = this.information;
    data['length'] = this.length;
    data['type'] = this.type;
    data['min'] = this.min;
    data['max'] = this.max;
    data['child'] = this.child;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['defaultValue'] = this.defaultValue;
    return data;
  }
}

class Data {
  String identifiant;
  String libelle;
  String code;
  String parent;
  String organisme;
  String segment;

  Data(
      {this.identifiant,
        this.libelle,
        this.code,
        this.parent,
        this.organisme,
        this.segment});

  Data.fromJson(Map<String, dynamic> json) {
    identifiant = json['identifiant'];
    libelle = json['libelle'];
    code = json['code'];
    parent = json['parent'];
    organisme = json['organisme'];
    segment = json['segment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identifiant'] = this.identifiant;
    data['libelle'] = this.libelle;
    data['code'] = this.code;
    data['parent'] = this.parent;
    data['organisme'] = this.organisme;
    data['segment'] = this.segment;
    return data;
  }
}