// To parse this JSON data, do
//
//     final creanceDetails = creanceDetailsFromJson(jsonString);

import 'dart:convert';

CreanceDetails creanceDetailsFromJson(String str) =>
    CreanceDetails.fromJson(json.decode(str));

String creanceDetailsToJson(CreanceDetails data) => json.encode(data.toJson());

class CreanceDetails {
  CreanceDetails({
    this.map,
  });

  MapClass map;

  factory CreanceDetails.fromJson(Map<String, dynamic> json) => CreanceDetails(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.listCreances,
    this.success,
  });

  List<ListCreance> listCreances;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        listCreances: List<ListCreance>.from(
            json["listCreances"].map((x) => ListCreance.fromJson(x))),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "listCreances": List<dynamic>.from(listCreances.map((x) => x.toJson())),
        "success": success,
      };
}

class ListCreance {
  ListCreance({
    this.nom,
    this.code,
    this.categorie,
    this.creancierId,
    this.logoPath,
    this.libelleCreancier,
    this.id,
  });

  String nom;
  String code;
  dynamic categorie;
  String creancierId;
  dynamic logoPath;
  dynamic libelleCreancier;
  dynamic id;

  factory ListCreance.fromJson(Map<String, dynamic> json) => ListCreance(
        nom: json["nom"],
        code: json["code"],
        categorie: json["categorie"],
        creancierId: json["creancierId"],
        logoPath: json["logoPath"],
        libelleCreancier: json["libelleCreancier"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "nom": nom,
        "code": code,
        "categorie": categorie,
        "creancierId": creancierId,
        "logoPath": logoPath,
        "libelleCreancier": libelleCreancier,
        "id": id,
      };
}
