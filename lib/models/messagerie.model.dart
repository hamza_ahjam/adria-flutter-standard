// To parse this JSON data, do
//
//     final messagerie = messagerieFromJson(jsonString);

import 'dart:convert';

Messagerie messagerieFromJson(String str) =>
    Messagerie.fromJson(json.decode(str));

String messagerieToJson(Messagerie data) => json.encode(data.toJson());

class Messagerie {
  Messagerie({
    this.map,
  });

  MapClass map;

  factory Messagerie.fromJson(Map<String, dynamic> json) => Messagerie(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.massages,
  });

  int total;
  List<Massage> massages;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        massages: List<Massage>.from(
            json["massages"].map((x) => Massage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "massages": List<dynamic>.from(massages.map((x) => x.toJson())),
      };
}

class Massage {
  Massage({
    this.sujet,
    this.corps,
    this.identifiant,
    this.statut,
    this.date,
    this.dateFomated,
    this.id,
    this.destinataires,
    this.sender,
    this.senderId,
    this.destinatairesNames,
    this.piecesJointsId,
    this.piecesJointsNames,
    this.piecesJointes,
    this.urlPiecesJoints,
    this.type,
    this.customdestinatairesNames,
  });

  String sujet;
  String corps;
  String identifiant;
  String statut;
  String date;
  String dateFomated;
  int id;
  String destinataires;
  String sender;
  String senderId;
  dynamic destinatairesNames;
  dynamic piecesJointsId;
  PiecesJointsNames piecesJointsNames;
  dynamic piecesJointes;
  dynamic urlPiecesJoints;
  dynamic type;
  dynamic customdestinatairesNames;

  factory Massage.fromJson(Map<String, dynamic> json) => Massage(
        sujet: json["sujet"],
        corps: json["corps"],
        identifiant: json["identifiant"],
        statut: json["statut"],
        date: json["date"],
        dateFomated: json["dateFomated"],
        id: json["id"],
        destinataires: json["destinataires"],
        sender: json["sender"],
        senderId: json["senderId"],
        destinatairesNames: json["destinatairesNames"],
        piecesJointsId: json["piecesJointsId"],
        piecesJointsNames:
            PiecesJointsNames.fromJson(json["piecesJointsNames"]),
        piecesJointes: json["piecesJointes"],
        urlPiecesJoints: json["urlPiecesJoints"],
        type: json["type"],
        customdestinatairesNames: json["customdestinatairesNames"],
      );

  Map<String, dynamic> toJson() => {
        "sujet": sujet,
        "corps": corps,
        "identifiant": identifiant,
        "statut": statut,
        "date": date,
        "dateFomated": dateFomated,
        "id": id,
        "destinataires": destinataires,
        "sender": sender,
        "senderId": senderId,
        "destinatairesNames": destinatairesNames,
        "piecesJointsId": piecesJointsId,
        "piecesJointsNames": piecesJointsNames.toJson(),
        "piecesJointes": piecesJointes,
        "urlPiecesJoints": urlPiecesJoints,
        "type": type,
        "customdestinatairesNames": customdestinatairesNames,
      };
}

class PiecesJointsNames {
  PiecesJointsNames();

  factory PiecesJointsNames.fromJson(Map<String, dynamic> json) =>
      PiecesJointsNames();

  Map<String, dynamic> toJson() => {};
}
