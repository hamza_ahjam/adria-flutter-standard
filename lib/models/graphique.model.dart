// To parse this JSON data, do
//
//     final graphique = graphiqueFromJson(jsonString);

import 'dart:convert';

Graphique graphiqueFromJson(String str) => Graphique.fromJson(json.decode(str));

String graphiqueToJson(Graphique data) => json.encode(data.toJson());

class Graphique {
  Graphique({
    this.map,
  });

  MapClass map;

  factory Graphique.fromJson(Map<String, dynamic> json) => Graphique(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.credits,
    this.debits,
    this.dates,
    this.devise,
    this.soldes,
  });

  List<dynamic> credits;
  List<dynamic> debits;
  List<dynamic> dates;
  String devise;
  List<dynamic> soldes;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        credits: List<dynamic>.from(json["credits"].map((x) => x)),
        debits: List<dynamic>.from(json["debits"].map((x) => x)),
        dates: List<dynamic>.from(json["dates"].map((x) => x)),
        devise: json["devise"],
        soldes: List<dynamic>.from(json["soldes"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "credits": List<dynamic>.from(credits.map((x) => x)),
        "debits": List<dynamic>.from(debits.map((x) => x)),
        "dates": List<dynamic>.from(dates.map((x) => x)),
        "devise": devise,
        "soldes": List<dynamic>.from(soldes.map((x) => x)),
      };
}
