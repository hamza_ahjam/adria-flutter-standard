// To parse this JSON data, do
//
//     final rib = ribFromJson(jsonString);

import 'dart:convert';

Rib ribFromJson(String str) => Rib.fromJson(json.decode(str));

String ribToJson(Rib data) => json.encode(data.toJson());

class Rib {
  Rib({
    this.map,
  });

  MapClass map;

  factory Rib.fromJson(Map<String, dynamic> json) => Rib(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.filename,
    this.contentType,
    this.content,
  });

  String filename;
  ContentType contentType;
  String content;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        filename: json["filename"],
        contentType: ContentType.fromJson(json["contentType"]),
        content: json["content"],
      );

  Map<String, dynamic> toJson() => {
        "filename": filename,
        "contentType": contentType.toJson(),
        "content": content,
      };
}

class ContentType {
  ContentType({
    this.type,
    this.subtype,
    this.parameters,
    this.qualityValue,
    this.charset,
    this.wildcardType,
    this.wildcardSubtype,
    this.concrete,
  });

  String type;
  String subtype;
  Parameters parameters;
  double qualityValue;
  dynamic charset;
  bool wildcardType;
  bool wildcardSubtype;
  bool concrete;

  factory ContentType.fromJson(Map<String, dynamic> json) => ContentType(
        type: json["type"],
        subtype: json["subtype"],
        parameters: Parameters.fromJson(json["parameters"]),
        qualityValue: json["qualityValue"],
        charset: json["charset"],
        wildcardType: json["wildcardType"],
        wildcardSubtype: json["wildcardSubtype"],
        concrete: json["concrete"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "subtype": subtype,
        "parameters": parameters.toJson(),
        "qualityValue": qualityValue,
        "charset": charset,
        "wildcardType": wildcardType,
        "wildcardSubtype": wildcardSubtype,
        "concrete": concrete,
      };
}

class Parameters {
  Parameters();

  factory Parameters.fromJson(Map<String, dynamic> json) => Parameters();

  Map<String, dynamic> toJson() => {};
}
