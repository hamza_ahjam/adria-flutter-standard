import 'dart:convert';

HistoriqueCheque creditsFromJson(String str) => HistoriqueCheque.fromJson(json.decode(str));

String creditsToJson(HistoriqueCheque data) => json.encode(data.toJson());

class HistoriqueCheque {
  HistoriqueCheque({
    this.map,
  });

  MapClass map;

  factory HistoriqueCheque.fromJson(Map<String, dynamic> json) => HistoriqueCheque(
    map: MapClass.fromJson(json["Map"]),
  );

  Map<String, dynamic> toJson() => {
    "Map": map.toJson(),
  };
}

class MapClass {
  MapClass({
    this.cheques,
    this.success,
  });

  List<Liste> cheques;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
    cheques:
    List<Liste>.from(json["liste"].map((x) => Liste.fromJson(x))),
    success: json["success"],
  );

  Map<String, dynamic> toJson() => {
    "liste": List<dynamic>.from(cheques.map((x) => x.toJson())),
    "success": success,
  };
}

class Liste {
  Liste({
    this.id,
    this.devise,
    this.datePaiement,
    this.statut,
    this.agence,
    this.montant,
    this.numeroCompte
  });

  dynamic id;
  dynamic devise;
  dynamic datePaiement;
  dynamic statut;
  dynamic agence;
  dynamic montant;
  dynamic numeroCompte;

  factory Liste.fromJson(Map<String, dynamic> json) => Liste(
    id: json["id"],
    devise: json["devise"],
    datePaiement: json["datePaiement"],
    statut: json["statut"],
    agence: json["agence"],
    montant: json["montant"],
    numeroCompte: json["numeroCompte"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "devise": devise,
    "datePaiement": datePaiement,
    "statut": statut,
    "agence": agence,
    "montant": montant,
    "numeroCompte": numeroCompte,
  };
}
