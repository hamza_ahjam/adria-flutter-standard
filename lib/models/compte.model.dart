// To parse this JSON data, do
//
//     final compte = compteFromJson(jsonString);

import 'dart:convert';

Compte compteFromJson(String str) => Compte.fromJson(json.decode(str));

String compteToJson(Compte data) => json.encode(data.toJson());

class Compte {
  Compte({
    this.toString_,
    this.compteSoldeIndisponible,
    this.convertibleLibelle,
    this.soldeIndisponible,
    this.compteSoldeComptable,
    this.deviseCode,
    this.elligibiliteLcn,
    this.natureCompte,
    this.autorisation,
    this.agence,
    this.compteSoldeTempsReel,
    this.elligibiliteDemande,
    this.soldeTempsReel,
    this.cleControle,
    this.elligibliteVirementCr,
    this.id,
    this.codeProduit,
    this.soldeComptableDevise,
    this.cleRib,
    this.identifiantInterne,
    this.compteDeviseCode,
    this.radical,
    this.elligibilitePrelivement,
    this.convertible,
    this.overdraftFlag,
    this.elligibliteVirementDb,
    this.compteSoldeComptableDevise,
    this.devise,
    this.intitule,
    this.identifiantInterneNotFormatted,
    this.soldeComptable,
    this.compteAutorisation,
    this.elligibilitePf,
  });

  String toString_;
  int compteSoldeIndisponible;
  String convertibleLibelle;
  String soldeIndisponible;
  dynamic compteSoldeComptable;
  String deviseCode;
  bool elligibiliteLcn;
  dynamic natureCompte;
  int autorisation;
  String agence;
  dynamic compteSoldeTempsReel;
  bool elligibiliteDemande;
  String soldeTempsReel;
  dynamic cleControle;
  bool elligibliteVirementCr;
  int id;
  String codeProduit;
  String soldeComptableDevise;
  dynamic cleRib;
  String identifiantInterne;
  String compteDeviseCode;
  String radical;
  bool elligibilitePrelivement;
  String convertible;
  dynamic overdraftFlag;
  bool elligibliteVirementDb;
  dynamic compteSoldeComptableDevise;
  String devise;
  String intitule;
  String identifiantInterneNotFormatted;
  String soldeComptable;
  String compteAutorisation;
  bool elligibilitePf;

  factory Compte.fromJson(Map<String, dynamic> json) => Compte(
        toString_: json["toString_"],
        compteSoldeIndisponible: json["soldeIndisponible_"],
        convertibleLibelle: json["convertible_libelle"],
        soldeIndisponible: json["soldeIndisponible"],
        compteSoldeComptable: json["soldeComptable_"],
        deviseCode: json["deviseCode"],
        elligibiliteLcn: json["elligibiliteLCN"],
        natureCompte: json["natureCompte"],
        autorisation: json["autorisation"],
        agence: json["agence"],
        compteSoldeTempsReel: json["soldeTempsReel_"],
        elligibiliteDemande: json["elligibiliteDemande"],
        soldeTempsReel: json["soldeTempsReel"],
        cleControle: json["cleControle"],
        elligibliteVirementCr: json["elligibliteVirementCr"],
        id: json["id"],
        codeProduit: json["codeProduit"],
        soldeComptableDevise: json["soldeComptableDevise"],
        cleRib: json["cleRIB"],
        identifiantInterne: json["identifiantInterne"],
        compteDeviseCode: json["devise_code"],
        radical: json["radical"],
        elligibilitePrelivement: json["elligibilitePrelivement"],
        convertible: json["convertible"],
        overdraftFlag: json["overdraftFlag"],
        elligibliteVirementDb: json["elligibliteVirementDb"],
        compteSoldeComptableDevise: json["soldeComptableDevise_"],
        devise: json["devise"],
        intitule: json["intitule"],
        identifiantInterneNotFormatted:
            json["identifiantInterne_NOT_FORMATTED"],
        soldeComptable: json["soldeComptable"],
        compteAutorisation: json["autorisation_"],
        elligibilitePf: json["elligibilitePF"],
      );

  Map<String, dynamic> toJson() => {
        "toString_": toString_,
        "soldeIndisponible_": compteSoldeIndisponible,
        "convertible_libelle": convertibleLibelle,
        "soldeIndisponible": soldeIndisponible,
        "soldeComptable_": compteSoldeComptable,
        "deviseCode": deviseCode,
        "elligibiliteLCN": elligibiliteLcn,
        "natureCompte": natureCompte,
        "autorisation": autorisation,
        "agence": agence,
        "soldeTempsReel_": compteSoldeTempsReel,
        "elligibiliteDemande": elligibiliteDemande,
        "soldeTempsReel": soldeTempsReel,
        "cleControle": cleControle,
        "elligibliteVirementCr": elligibliteVirementCr,
        "id": id,
        "codeProduit": codeProduit,
        "soldeComptableDevise": soldeComptableDevise,
        "cleRIB": cleRib,
        "identifiantInterne": identifiantInterne,
        "devise_code": compteDeviseCode,
        "radical": radical,
        "elligibilitePrelivement": elligibilitePrelivement,
        "convertible": convertible,
        "overdraftFlag": overdraftFlag,
        "elligibliteVirementDb": elligibliteVirementDb,
        "soldeComptableDevise_": compteSoldeComptableDevise,
        "devise": devise,
        "intitule": intitule,
        "identifiantInterne_NOT_FORMATTED": identifiantInterneNotFormatted,
        "soldeComptable": soldeComptable,
        "autorisation_": compteAutorisation,
        "elligibilitePF": elligibilitePf,
      };
}
