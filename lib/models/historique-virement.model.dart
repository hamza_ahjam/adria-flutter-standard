// To parse this JSON data, do
//
//     final historiqueVirement = historiqueVirementFromJson(jsonString);

import 'dart:convert';

HistoriqueVirement historiqueVirementFromJson(String str) =>
    HistoriqueVirement.fromJson(json.decode(str));

String historiqueVirementToJson(HistoriqueVirement data) =>
    json.encode(data.toJson());

class HistoriqueVirement {
  HistoriqueVirement({
    this.map,
  });

  MapClass map;

  factory HistoriqueVirement.fromJson(Map<String, dynamic> json) =>
      HistoriqueVirement(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.liste,
    this.success,
  });

  int total;
  List<dynamic> liste;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        liste: List<dynamic>.from(json["liste"].map((x) => x)),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "liste": List<dynamic>.from(liste.map((x) => x)),
        "success": success,
      };
}
