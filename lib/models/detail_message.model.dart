// To parse this JSON data, do
//
//     final detailMessage = detailMessageFromJson(jsonString);

import 'dart:convert';

DetailMessage detailMessageFromJson(String str) =>
    DetailMessage.fromJson(json.decode(str));

String detailMessageToJson(DetailMessage data) => json.encode(data.toJson());

class DetailMessage {
  DetailMessage({
    this.map,
  });

  MapClass map;

  factory DetailMessage.fromJson(Map<String, dynamic> json) => DetailMessage(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.success,
    this.history,
  });

  bool success;
  List<History> history;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        success: json["success"],
        history:
            List<History>.from(json["history"].map((x) => History.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "history": List<dynamic>.from(history.map((x) => x.toJson())),
      };
}

class History {
  History({
    this.sujet,
    this.corps,
    this.identifiant,
    this.statut,
    this.date,
    this.dateFomated,
    this.id,
    this.destinataires,
    this.sender,
    this.senderId,
    this.destinatairesNames,
    this.piecesJointsId,
    this.piecesJointsNames,
    this.piecesJointes,
    this.urlPiecesJoints,
    this.type,
    this.customdestinatairesNames,
  });

  String sujet;
  String corps;
  String identifiant;
  String statut;
  String date;
  String dateFomated;
  int id;
  String destinataires;
  String sender;
  String senderId;
  dynamic destinatairesNames;
  dynamic piecesJointsId;
  dynamic piecesJointsNames;
  List<PiecesJointe> piecesJointes;
  dynamic urlPiecesJoints;
  String type;
  dynamic customdestinatairesNames;

  factory History.fromJson(Map<String, dynamic> json) => History(
        sujet: json["sujet"],
        corps: json["corps"],
        identifiant: json["identifiant"],
        statut: json["statut"],
        date: json["date"],
        dateFomated: json["dateFomated"],
        id: json["id"],
        destinataires: json["destinataires"],
        sender: json["sender"],
        senderId: json["senderId"],
        destinatairesNames: json["destinatairesNames"],
        piecesJointsId: json["piecesJointsId"],
        piecesJointsNames: json["piecesJointsNames"],
        piecesJointes: List<PiecesJointe>.from(
            json["piecesJointes"].map((x) => PiecesJointe.fromJson(x))),
        urlPiecesJoints: json["urlPiecesJoints"],
        type: json["type"],
        customdestinatairesNames: json["customdestinatairesNames"],
      );

  Map<String, dynamic> toJson() => {
        "sujet": sujet,
        "corps": corps,
        "identifiant": identifiant,
        "statut": statut,
        "date": date,
        "dateFomated": dateFomated,
        "id": id,
        "destinataires": destinataires,
        "sender": sender,
        "senderId": senderId,
        "destinatairesNames": destinatairesNames,
        "piecesJointsId": piecesJointsId,
        "piecesJointsNames": piecesJointsNames,
        "piecesJointes":
            List<dynamic>.from(piecesJointes.map((x) => x.toJson())),
        "urlPiecesJoints": urlPiecesJoints,
        "type": type,
        "customdestinatairesNames": customdestinatairesNames,
      };
}

class PiecesJointe {
  PiecesJointe({
    this.id,
    this.lang,
    this.datePattern,
    this.max,
    this.page,
    this.sort,
    this.order,
    this.codeBanque,
    this.userId,
    this.version,
    this.codeBanqueAssocie,
    this.codePaysAssocie,
    this.descriptionpj,
    this.filename,
    this.nameDocument,
    this.contenu,
    this.picejointe,
    this.taille,
    this.tailleFormatted,
  });

  int id;
  dynamic lang;
  dynamic datePattern;
  dynamic max;
  dynamic page;
  dynamic sort;
  dynamic order;
  dynamic codeBanque;
  dynamic userId;
  int version;
  String codeBanqueAssocie;
  String codePaysAssocie;
  dynamic descriptionpj;
  dynamic filename;
  String nameDocument;
  String contenu;
  dynamic picejointe;
  dynamic taille;
  dynamic tailleFormatted;

  factory PiecesJointe.fromJson(Map<String, dynamic> json) => PiecesJointe(
        id: json["id"],
        lang: json["lang"],
        datePattern: json["datePattern"],
        max: json["max"],
        page: json["page"],
        sort: json["sort"],
        order: json["order"],
        codeBanque: json["codeBanque"],
        userId: json["userId"],
        version: json["version"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        codePaysAssocie: json["codePaysAssocie"],
        descriptionpj: json["descriptionpj"],
        filename: json["filename"],
        nameDocument: json["nameDocument"],
        contenu: json["contenu"],
        picejointe: json["picejointe"],
        taille: json["taille"],
        tailleFormatted: json["tailleFormatted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lang": lang,
        "datePattern": datePattern,
        "max": max,
        "page": page,
        "sort": sort,
        "order": order,
        "codeBanque": codeBanque,
        "userId": userId,
        "version": version,
        "codeBanqueAssocie": codeBanqueAssocie,
        "codePaysAssocie": codePaysAssocie,
        "descriptionpj": descriptionpj,
        "filename": filename,
        "nameDocument": nameDocument,
        "contenu": contenu,
        "picejointe": picejointe,
        "taille": taille,
        "tailleFormatted": tailleFormatted,
      };
}
