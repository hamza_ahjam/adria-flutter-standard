import 'dart:core';

class Favoris {
  MapClass map;

  Favoris({this.map});

  Favoris.fromJson(Map<String, dynamic> json) {
    map = json['Map'] != null ? new MapClass.fromJson(json['Map']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.map != null) {
      data['Map'] = this.map.toJson();
    }
    return data;
  }
}

class MapClass {
  bool success;
  String message;
  List<ListItems> list;

  MapClass({this.success, this.list, this.message});

  MapClass.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['list'] != null) {
      list = <ListItems>[];
      json['list'].forEach((v) {
        list.add(new ListItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListItems {
  int id;
  String creancierId;
  String creanceId;
  String saisiForm;
  String libelleFavoris;
  String libelleCreancier;
  String libelleCreance;
  String logoPath;
  int nbrFacture;
  String type;
  List<String> dynamicFormValues;

  ListItems(
      {this.id,
        this.creancierId,
        this.creanceId,
        this.saisiForm,
        this.libelleFavoris,
        this.libelleCreancier,
        this.libelleCreance,
        this.logoPath,
        this.nbrFacture,
        this.type,
        this.dynamicFormValues});

  ListItems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    creancierId = json['creancierId'];
    creanceId = json['creanceId'];
    saisiForm = json['saisiForm'];
    libelleFavoris = json['libelleFavoris'];
    libelleCreancier = json['libelleCreancier'];
    libelleCreance = json['libelleCreance'];
    logoPath = json['logoPath'];
    nbrFacture = json['nbrFacture'];
    type = json['type'];
    dynamicFormValues = json['dynamicFormValues'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['creancierId'] = this.creancierId;
    data['creanceId'] = this.creanceId;
    data['saisiForm'] = this.saisiForm;
    data['libelleFavoris'] = this.libelleFavoris;
    data['libelleCreancier'] = this.libelleCreancier;
    data['libelleCreance'] = this.libelleCreance;
    data['logoPath'] = this.logoPath;
    data['nbrFacture'] = this.nbrFacture;
    data['type'] = this.type;
    data['dynamicFormValues'] = this.dynamicFormValues;
    return data;
  }
}