// To parse this JSON data, do
//
//     final creancierForm = creancierFormFromJson(jsonString);

import 'dart:convert';

CreancierForm creancierFormFromJson(String str) =>
    CreancierForm.fromJson(json.decode(str));

String creancierFormToJson(CreancierForm data) => json.encode(data.toJson());

class CreancierForm {
  CreancierForm({
    this.map,
  });

  MapClass map;

  factory CreancierForm.fromJson(Map<String, dynamic> json) => CreancierForm(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.champs,
    this.creance,
    this.type,
  });

  List<Champ> champs;
  Creance creance;
  String type;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        champs: List<Champ>.from(json["champs"].map((x) => Champ.fromJson(x))),
        creance: Creance.fromJson(json["creance"]),
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "champs": List<dynamic>.from(champs.map((x) => x.toJson())),
        "creance": creance.toJson(),
        "type": type,
      };
}

class Champ {
  Champ({
    this.libelle,
    this.nomChamps,
    this.options,
    this.formatChamps,
    this.tailleMin,
    this.tailleMax,
    this.contrainte,
    this.typeChamps,
    this.listVals,
  });

  String libelle;
  String nomChamps;
  dynamic options;
  String formatChamps;
  int tailleMin;
  int tailleMax;
  String contrainte;
  String typeChamps;
  List<dynamic> listVals;

  factory Champ.fromJson(Map<String, dynamic> json) => Champ(
        libelle: json["libelle"],
        nomChamps: json["nomChamps"],
        options: json["options"],
        formatChamps: json["formatChamps"],
        tailleMin: json["tailleMin"],
        tailleMax: json["tailleMax"],
        contrainte: json["contrainte"],
        typeChamps: json["typeChamps"],
        listVals: List<dynamic>.from(json["listVals"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "libelle": libelle,
        "nomChamps": nomChamps,
        "options": options,
        "formatChamps": formatChamps,
        "tailleMin": tailleMin,
        "tailleMax": tailleMax,
        "contrainte": contrainte,
        "typeChamps": typeChamps,
        "listVals": List<dynamic>.from(listVals.map((x) => x)),
      };
}

class Creance {
  Creance({
    this.nom,
    this.code,
    this.categorie,
    this.creancierId,
    this.logoPath,
    this.libelleCreancier,
    this.id,
  });

  String nom;
  String code;
  dynamic categorie;
  String creancierId;
  dynamic logoPath;
  dynamic libelleCreancier;
  dynamic id;

  factory Creance.fromJson(Map<String, dynamic> json) => Creance(
        nom: json["nom"],
        code: json["code"],
        categorie: json["categorie"],
        creancierId: json["creancierId"],
        logoPath: json["logoPath"],
        libelleCreancier: json["libelleCreancier"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "nom": nom,
        "code": code,
        "categorie": categorie,
        "creancierId": creancierId,
        "logoPath": logoPath,
        "libelleCreancier": libelleCreancier,
        "id": id,
      };
}
