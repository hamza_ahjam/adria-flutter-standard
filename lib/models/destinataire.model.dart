// To parse this JSON data, do
//
//     final destinataire = destinataireFromJson(jsonString);

import 'dart:convert';

Destinataire destinataireFromJson(String str) =>
    Destinataire.fromJson(json.decode(str));

String destinataireToJson(Destinataire data) => json.encode(data.toJson());

class Destinataire {
  Destinataire({
    this.map,
  });

  MapClass map;

  factory Destinataire.fromJson(Map<String, dynamic> json) => Destinataire(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.destinataireList,
  });

  List<DestinataireList> destinataireList;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        destinataireList: List<DestinataireList>.from(
            json["destinataireList"].map((x) => DestinataireList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "destinataireList":
            List<dynamic>.from(destinataireList.map((x) => x.toJson())),
      };
}

class DestinataireList {
  DestinataireList({
    this.id,
    this.libelle,
  });

  String id;
  String libelle;

  factory DestinataireList.fromJson(Map<String, dynamic> json) =>
      DestinataireList(
        id: json["id"],
        libelle: json["libelle"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
      };
}
