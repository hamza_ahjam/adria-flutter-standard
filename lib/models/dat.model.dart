// To parse this JSON data, do
//
//     final dat = datFromJson(jsonString);

import 'dart:convert';

Dat datFromJson(String str) => Dat.fromJson(json.decode(str));

String datToJson(Dat data) => json.encode(data.toJson());

class Dat {
  Dat({
    this.map,
  });

  MapClass map;

  factory Dat.fromJson(Map<String, dynamic> json) => Dat(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.liste,
    this.success,
  });

  int total;
  List<Liste> liste;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        liste: List<Liste>.from(json["liste"].map((x) => Liste.fromJson(x))),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "liste": List<dynamic>.from(liste.map((x) => x.toJson())),
        "success": success,
      };
}

class Liste {
  Liste({
    this.id,
    this.compteSouscription,
    this.codeBanqueAssocie,
    this.compteDepot,
    this.devise,
    this.type,
    this.indexe,
    this.nanti,
    this.statutCode,
    this.deviseLibelle,
    this.periodiciteLibelle,
    this.modeRenouvlementLibelle,
    this.compteSouscriptionId,
    this.compteVersementInteretId,
    this.compteDepotId,
    this.version,
    this.montantDat,
    this.montantDatFormatted,
    this.taux,
    this.origineFonds,
    this.dateExcecution,
    this.dateEffet,
    this.dateEcheance,
    this.periodicite,
    this.interetPrecompte,
    this.capitalisation,
    this.prelevementsLiberatoires,
    this.nombreJours,
    this.modeRenouvlement,
    this.tauxRendementAnnuelBrut,
    this.compteVersementInteret,
    this.statut,
    this.numero,
    this.typeLibelle,
    this.agence,
    this.typeInteret,
    this.dureeEnMois,
    this.radical,
    this.montantMax,
    this.montantMin,
    this.dateDebut,
    this.dateFin,
  });

  dynamic id;
  dynamic compteSouscription;
  dynamic codeBanqueAssocie;
  String compteDepot;
  dynamic devise;
  String type;
  dynamic indexe;
  dynamic nanti;
  dynamic statutCode;
  dynamic deviseLibelle;
  dynamic periodiciteLibelle;
  dynamic modeRenouvlementLibelle;
  dynamic compteSouscriptionId;
  dynamic compteVersementInteretId;
  dynamic compteDepotId;
  dynamic version;
  dynamic montantDat;
  String montantDatFormatted;
  dynamic taux;
  dynamic origineFonds;
  dynamic dateExcecution;
  String dateEffet;
  String dateEcheance;
  dynamic periodicite;
  dynamic interetPrecompte;
  dynamic capitalisation;
  dynamic prelevementsLiberatoires;
  dynamic nombreJours;
  dynamic modeRenouvlement;
  dynamic tauxRendementAnnuelBrut;
  String compteVersementInteret;
  dynamic statut;
  String numero;
  dynamic typeLibelle;
  String agence;
  String typeInteret;
  String dureeEnMois;
  dynamic radical;
  dynamic montantMax;
  dynamic montantMin;
  dynamic dateDebut;
  dynamic dateFin;

  factory Liste.fromJson(Map<String, dynamic> json) => Liste(
        id: json["id"],
        compteSouscription: json["compteSouscription"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        compteDepot: json["compteDepot"],
        devise: json["devise"],
        type: json["type"],
        indexe: json["indexe"],
        nanti: json["nanti"],
        statutCode: json["statutCode"],
        deviseLibelle: json["deviseLibelle"],
        periodiciteLibelle: json["periodiciteLibelle"],
        modeRenouvlementLibelle: json["modeRenouvlementLibelle"],
        compteSouscriptionId: json["compteSouscriptionId"],
        compteVersementInteretId: json["compteVersementInteretId"],
        compteDepotId: json["compteDepotId"],
        version: json["version"],
        montantDat: json["montantDAT"],
        montantDatFormatted: json["montantDATFormatted"],
        taux: json["taux"],
        origineFonds: json["origineFonds"],
        dateExcecution: json["dateExcecution"],
        dateEffet: json["dateEffet"],
        dateEcheance: json["dateEcheance"],
        periodicite: json["periodicite"],
        interetPrecompte: json["interetPrecompte"],
        capitalisation: json["capitalisation"],
        prelevementsLiberatoires: json["prelevementsLiberatoires"],
        nombreJours: json["nombreJours"],
        modeRenouvlement: json["modeRenouvlement"],
        tauxRendementAnnuelBrut: json["tauxRendementAnnuelBrut"],
        compteVersementInteret: json["compteVersementInteret"],
        statut: json["statut"],
        numero: json["numero"],
        typeLibelle: json["typeLibelle"],
        agence: json["agence"],
        typeInteret: json["typeInteret"],
        dureeEnMois: json["dureeEnMois"],
        radical: json["radical"],
        montantMax: json["montantMax"],
        montantMin: json["montantMin"],
        dateDebut: json["dateDebut"],
        dateFin: json["dateFin"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "compteSouscription": compteSouscription,
        "codeBanqueAssocie": codeBanqueAssocie,
        "compteDepot": compteDepot,
        "devise": devise,
        "type": type,
        "indexe": indexe,
        "nanti": nanti,
        "statutCode": statutCode,
        "deviseLibelle": deviseLibelle,
        "periodiciteLibelle": periodiciteLibelle,
        "modeRenouvlementLibelle": modeRenouvlementLibelle,
        "compteSouscriptionId": compteSouscriptionId,
        "compteVersementInteretId": compteVersementInteretId,
        "compteDepotId": compteDepotId,
        "version": version,
        "montantDAT": montantDat,
        "montantDATFormatted": montantDatFormatted,
        "taux": taux,
        "origineFonds": origineFonds,
        "dateExcecution": dateExcecution,
        "dateEffet": dateEffet,
        "dateEcheance": dateEcheance,
        "periodicite": periodicite,
        "interetPrecompte": interetPrecompte,
        "capitalisation": capitalisation,
        "prelevementsLiberatoires": prelevementsLiberatoires,
        "nombreJours": nombreJours,
        "modeRenouvlement": modeRenouvlement,
        "tauxRendementAnnuelBrut": tauxRendementAnnuelBrut,
        "compteVersementInteret": compteVersementInteret,
        "statut": statut,
        "numero": numero,
        "typeLibelle": typeLibelle,
        "agence": agence,
        "typeInteret": typeInteret,
        "dureeEnMois": dureeEnMois,
        "radical": radical,
        "montantMax": montantMax,
        "montantMin": montantMin,
        "dateDebut": dateDebut,
        "dateFin": dateFin,
      };
}
