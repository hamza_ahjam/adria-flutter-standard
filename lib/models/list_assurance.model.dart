// To parse this JSON data, do
//
//     final listAssurance = listAssuranceFromJson(jsonString);

import 'dart:convert';

ListAssurance listAssuranceFromJson(String str) =>
    ListAssurance.fromJson(json.decode(str));

String listAssuranceToJson(ListAssurance data) => json.encode(data.toJson());

class ListAssurance {
  ListAssurance({
    this.map,
  });

  MapClass map;

  factory ListAssurance.fromJson(Map<String, dynamic> json) => ListAssurance(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.listAssurances,
  });

  int total;
  List<ListAssuranceElement> listAssurances;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        listAssurances: List<ListAssuranceElement>.from(json["listAssurances"]
            .map((x) => ListAssuranceElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "listAssurances":
            List<dynamic>.from(listAssurances.map((x) => x.toJson())),
      };
}

class ListAssuranceElement {
  ListAssuranceElement({
    this.identifiant,
    this.numeroCompte,
    this.radical,
    this.numeroPolice,
    this.dateSouscription,
    this.dateEffet,
    this.dateEcheance,
    this.periodiciteCotisation,
    this.souscripteur,
    this.enfantBeneficiaire,
    this.statutContrat,
    this.dateSituation,
    this.montantEpargne,
    this.valeurRachatBrute,
    this.valeurRachatNette,
    this.montant,
    this.montantPlafondAvance,
    this.montantPrimeProvisionnelle,
    this.montantAvance,
    this.montantRachatPartiel,
    this.benificiaire1,
    this.benificiaire2,
    this.benificiaire3,
    this.benificiaire4,
    this.cumulCotisation,
    this.intituleAssure,
    this.valeurBatiment,
    this.cotisationTotalAnnuelle,
    this.nomProduit,
    this.montantCotisation,
    this.assure,
    this.idContrat,
    this.type,
  });

  String identifiant;
  String numeroCompte;
  String radical;
  String numeroPolice;
  String dateSouscription;
  String dateEffet;
  String dateEcheance;
  dynamic periodiciteCotisation;
  dynamic souscripteur;
  dynamic enfantBeneficiaire;
  dynamic statutContrat;
  dynamic dateSituation;
  dynamic montantEpargne;
  dynamic valeurRachatBrute;
  dynamic valeurRachatNette;
  dynamic montant;
  dynamic montantPlafondAvance;
  dynamic montantPrimeProvisionnelle;
  dynamic montantAvance;
  dynamic montantRachatPartiel;
  dynamic benificiaire1;
  dynamic benificiaire2;
  dynamic benificiaire3;
  dynamic benificiaire4;
  dynamic cumulCotisation;
  String intituleAssure;
  String valeurBatiment;
  String cotisationTotalAnnuelle;
  dynamic nomProduit;
  dynamic montantCotisation;
  String assure;
  dynamic idContrat;
  dynamic type;

  factory ListAssuranceElement.fromJson(Map<String, dynamic> json) =>
      ListAssuranceElement(
        identifiant: json["identifiant"],
        numeroCompte: json["numeroCompte"],
        radical: json["radical"],
        numeroPolice: json["numeroPolice"],
        dateSouscription: json["dateSouscription"],
        dateEffet: json["dateEffet"],
        dateEcheance: json["dateEcheance"],
        periodiciteCotisation: json["periodiciteCotisation"],
        souscripteur: json["souscripteur"],
        enfantBeneficiaire: json["enfantBeneficiaire"],
        statutContrat: json["statutContrat"],
        dateSituation: json["dateSituation"],
        montantEpargne: json["montantEpargne"],
        valeurRachatBrute: json["valeurRachatBrute"],
        valeurRachatNette: json["valeurRachatNette"],
        montant: json["montant"],
        montantPlafondAvance: json["montantPlafondAvance"],
        montantPrimeProvisionnelle: json["montantPrimeProvisionnelle"],
        montantAvance: json["montantAvance"],
        montantRachatPartiel: json["montantRachatPartiel"],
        benificiaire1: json["benificiaire1"],
        benificiaire2: json["benificiaire2"],
        benificiaire3: json["benificiaire3"],
        benificiaire4: json["benificiaire4"],
        cumulCotisation: json["cumulCotisation"],
        intituleAssure: json["intituleAssure"],
        valeurBatiment: json["valeurBatiment"],
        cotisationTotalAnnuelle: json["cotisationTotalAnnuelle"],
        nomProduit: json["nomProduit"],
        montantCotisation: json["montantCotisation"],
        assure: json["assure"],
        idContrat: json["idContrat"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "identifiant": identifiant,
        "numeroCompte": numeroCompte,
        "radical": radical,
        "numeroPolice": numeroPolice,
        "dateSouscription": dateSouscription,
        "dateEffet": dateEffet,
        "dateEcheance": dateEcheance,
        "periodiciteCotisation": periodiciteCotisation,
        "souscripteur": souscripteur,
        "enfantBeneficiaire": enfantBeneficiaire,
        "statutContrat": statutContrat,
        "dateSituation": dateSituation,
        "montantEpargne": montantEpargne,
        "valeurRachatBrute": valeurRachatBrute,
        "valeurRachatNette": valeurRachatNette,
        "montant": montant,
        "montantPlafondAvance": montantPlafondAvance,
        "montantPrimeProvisionnelle": montantPrimeProvisionnelle,
        "montantAvance": montantAvance,
        "montantRachatPartiel": montantRachatPartiel,
        "benificiaire1": benificiaire1,
        "benificiaire2": benificiaire2,
        "benificiaire3": benificiaire3,
        "benificiaire4": benificiaire4,
        "cumulCotisation": cumulCotisation,
        "intituleAssure": intituleAssure,
        "valeurBatiment": valeurBatiment,
        "cotisationTotalAnnuelle": cotisationTotalAnnuelle,
        "nomProduit": nomProduit,
        "montantCotisation": montantCotisation,
        "assure": assure,
        "idContrat": idContrat,
        "type": type,
      };
}
