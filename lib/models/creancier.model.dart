// To parse this JSON data, do
//
//     final creanciers = creanciersFromJson(jsonString);

import 'dart:convert';

Creanciers creanciersFromJson(String str) =>
    Creanciers.fromJson(json.decode(str));

String creanciersToJson(Creanciers data) => json.encode(data.toJson());

class Creanciers {
  Creanciers({
    this.map,
  });

  MapClass map;

  factory Creanciers.fromJson(Map<String, dynamic> json) => Creanciers(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.success,
    this.list,
  });

  bool success;
  List<ListElement> list;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        success: json["success"],
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
      };
}

class ListElement {
  ListElement({
    this.nom,
    this.code,
    this.description,
    this.logoPath,
    this.siteUrl,
    this.categorie,
    this.typeCreancier,
    this.creances,
  });

  String nom;
  String code;
  String description;
  String logoPath;
  String siteUrl;
  String categorie;
  TypeCreancier typeCreancier;
  List<dynamic> creances;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        nom: json["nom"],
        code: json["code"],
        description: json["description"],
        logoPath: json["logoPath"],
        siteUrl: json["siteUrl"],
        categorie: json["categorie"],
        typeCreancier: typeCreancierValues.map[json["typeCreancier"]],
        creances: List<dynamic>.from(json["creances"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "nom": nom,
        "code": code,
        "description": description,
        "logoPath": logoPath,
        "siteUrl": siteUrl,
        "categorie": categorie,
        "typeCreancier": typeCreancierValues.reverse[typeCreancier],
        "creances": List<dynamic>.from(creances.map((x) => x)),
      };
}

enum TypeCreancier { F, R }

final typeCreancierValues =
    EnumValues({"F": TypeCreancier.F, "R": TypeCreancier.R});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
