import 'package:LBA/models/compte.model.dart';

class ListCompte {
  List<Compte> comptes = [];

  ListCompte.fromJson(Map<String, dynamic> json)
      : comptes = (json["Map"]["compteInstanceList"] as List)
            .map<Compte>((cp) => Compte.fromJson(cp))
            .toList();
}
