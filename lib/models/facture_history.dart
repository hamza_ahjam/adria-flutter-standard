

import 'dart:convert';

FacturesHistory titresFromJson(String str) => FacturesHistory.fromJson(json.decode(str));

String titresToJson(FacturesHistory data) => json.encode(data.toJson());


class FacturesHistory{
  MapClass map;

  FacturesHistory({
    this.map,
  });

  factory FacturesHistory.fromJson(Map<String, dynamic> json) => FacturesHistory(
    map: MapClass.fromJson(json["Map"]),
  );

  Map<String, dynamic> toJson() => {
    "Map": map.toJson(),
  };
}


class MapClass {

  List<FactureHistory> factures;
  bool success;
  int total;
  String message;
  String codeErreur;

  MapClass({
    this.factures,
    this.success,
    this.total,
    this.message,
    this.codeErreur
  });

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
    factures:
    List<FactureHistory>.from(json["liste"].map((x) => FactureHistory.fromJson(x))),
    success: json["success"],
    total: json["total"],
    message: json["message"],
    codeErreur: json["codeErreur"]
  );

  Map<String, dynamic> toJson() => {
    "liste": List<dynamic>.from(factures.map((x) => x.toJson())),
    "success": success,
    "total": total,
    "message": message,
    "codeErreur": codeErreur
  };
}


class FactureHistory {
  int id;
  String radical;
  String intituleClient;
  String numeroCompte;
  String deviseCompte;
  String agenceCompte;
  String intituleCompte;
  String codeBanqueTransaction;
  String idInitiateur;
  String deviseOperation;
  int version;
  String statutCode;
  String codeProduitCompte;
  String deviseOperationLibelle;
  String nomCompteDetaille;
  String identifiantExterne;
  String dateEnvoiDemande;
  int nombreChequiers;
  int nombreLCN;
  bool endorsable;
  bool crossed;
  String creancierId;
  String creanceId;
  double montantTotalTTC;
  String montantTotalTTCToString;
  String libelleCreance;
  String libelleCreancier;
  String referencePartenaire;

  FactureHistory(
      {this.id,
        this.radical,
        this.intituleClient,
        this.numeroCompte,
        this.deviseCompte,
        this.agenceCompte,
        this.intituleCompte,
        this.codeBanqueTransaction,
        this.idInitiateur,
        this.deviseOperation,
        this.version,
        this.statutCode,
        this.codeProduitCompte,
        this.deviseOperationLibelle,
        this.nomCompteDetaille,
        this.identifiantExterne,
        this.dateEnvoiDemande,
        this.nombreChequiers,
        this.nombreLCN,
        this.endorsable,
        this.crossed,
        this.creancierId,
        this.creanceId,
        this.montantTotalTTC,
        this.montantTotalTTCToString,
        this.libelleCreance,
        this.libelleCreancier,
        this.referencePartenaire});

  FactureHistory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    radical = json['radical'];
    intituleClient = json['intituleClient'];
    numeroCompte = json['numeroCompte'];
    deviseCompte = json['deviseCompte'];
    agenceCompte = json['agenceCompte'];
    intituleCompte = json['intituleCompte'];
    codeBanqueTransaction = json['codeBanqueTransaction'];
    idInitiateur = json['idInitiateur'];
    deviseOperation = json['deviseOperation'];
    version = json['version'];
    statutCode = json['statutCode'];
    codeProduitCompte = json['codeProduitCompte'];
    deviseOperationLibelle = json['deviseOperationLibelle'];
    nomCompteDetaille = json['nomCompteDetaille'];
    identifiantExterne = json['identifiantExterne'];
    dateEnvoiDemande = json['dateEnvoiDemande'];
    nombreChequiers = json['nombreChequiers'];
    nombreLCN = json['nombreLCN'];
    endorsable = json['endorsable'];
    crossed = json['crossed'];
    creancierId = json['creancierId'];
    creanceId = json['creanceId'];
    montantTotalTTC = json['montantTotalTTC'];
    montantTotalTTCToString = json['montantTotalTTCToString'];
    libelleCreance = json['libelleCreance'];
    libelleCreancier = json['libelleCreancier'];
    referencePartenaire = json['referencePartenaire'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['radical'] = this.radical;
    data['intituleClient'] = this.intituleClient;
    data['numeroCompte'] = this.numeroCompte;
    data['deviseCompte'] = this.deviseCompte;
    data['agenceCompte'] = this.agenceCompte;
    data['intituleCompte'] = this.intituleCompte;
    data['codeBanqueTransaction'] = this.codeBanqueTransaction;
    data['idInitiateur'] = this.idInitiateur;
    data['deviseOperation'] = this.deviseOperation;
    data['version'] = this.version;
    data['statutCode'] = this.statutCode;
    data['codeProduitCompte'] = this.codeProduitCompte;
    data['deviseOperationLibelle'] = this.deviseOperationLibelle;
    data['nomCompteDetaille'] = this.nomCompteDetaille;
    data['identifiantExterne'] = this.identifiantExterne;
    data['dateEnvoiDemande'] = this.dateEnvoiDemande;
    data['nombreChequiers'] = this.nombreChequiers;
    data['nombreLCN'] = this.nombreLCN;
    data['endorsable'] = this.endorsable;
    data['crossed'] = this.crossed;
    data['creancierId'] = this.creancierId;
    data['creanceId'] = this.creanceId;
    data['montantTotalTTC'] = this.montantTotalTTC;
    data['montantTotalTTCToString'] = this.montantTotalTTCToString;
    data['libelleCreance'] = this.libelleCreance;
    data['libelleCreancier'] = this.libelleCreancier;
    data['referencePartenaire'] = this.referencePartenaire;
    return data;
  }
}