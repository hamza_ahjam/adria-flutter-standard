// To parse this JSON data, do
//
//     final historiqueChequier = historiqueChequierFromJson(jsonString);

import 'dart:convert';

HistoriqueChequier historiqueChequierFromJson(String str) =>
    HistoriqueChequier.fromJson(json.decode(str));

String historiqueChequierToJson(HistoriqueChequier data) =>
    json.encode(data.toJson());

class HistoriqueChequier {
  HistoriqueChequier({
    this.map,
  });

  MapClass map;

  factory HistoriqueChequier.fromJson(Map<String, dynamic> json) =>
      HistoriqueChequier(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.total,
    this.liste,
    this.success,
  });

  int total;
  List<Liste> liste;
  bool success;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        total: json["total"],
        liste: List<Liste>.from(json["liste"].map((x) => Liste.fromJson(x))),
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "liste": List<dynamic>.from(liste.map((x) => x.toJson())),
        "success": success,
      };
}

class Liste {
  Liste({
    this.id,
    this.radical,
    this.intituleClient,
    this.numeroCompte,
    this.deviseCompte,
    this.agenceCompte,
    this.intituleCompte,
    this.codeConvertibiliteCompte,
    this.identifiantDemande,
    this.codeBanqueTransaction,
    this.intituleInitiateur,
    this.codeBanqueAssocie,
    this.idInitiateur,
    this.deviseOperation,
    this.version,
    this.statut,
    this.statutCode,
    this.codeProduitCompte,
    this.choixOperateur,
    this.deviseOperationLibelle,
    this.deviseCompteLibelle,
    this.nomCompteDetaille,
    this.numeroCompteCrediter,
    this.identifiantExterne,
    this.dateEnvoiDemande,
    this.typeChequier,
    this.typeChequierLibelle,
    this.nombreChequiers,
    this.identifierExterne,
    this.typeLcn,
    this.typeLcnLibelle,
    this.nombreLcn,
    this.type,
    this.dateMin,
    this.dateMax,
    this.endorsable,
    this.crossed,
  });

  int id;
  String radical;
  String intituleClient;
  String numeroCompte;
  String deviseCompte;
  String agenceCompte;
  dynamic intituleCompte;
  dynamic codeConvertibiliteCompte;
  String identifiantDemande;
  String codeBanqueTransaction;
  dynamic intituleInitiateur;
  dynamic codeBanqueAssocie;
  String idInitiateur;
  dynamic deviseOperation;
  int version;
  String statut;
  String statutCode;
  String codeProduitCompte;
  dynamic choixOperateur;
  dynamic deviseOperationLibelle;
  dynamic deviseCompteLibelle;
  String nomCompteDetaille;
  dynamic numeroCompteCrediter;
  dynamic identifiantExterne;
  String dateEnvoiDemande;
  String typeChequier;
  String typeChequierLibelle;
  int nombreChequiers;
  dynamic identifierExterne;
  dynamic typeLcn;
  dynamic typeLcnLibelle;
  int nombreLcn;
  dynamic type;
  dynamic dateMin;
  dynamic dateMax;
  bool endorsable;
  bool crossed;

  factory Liste.fromJson(Map<String, dynamic> json) => Liste(
        id: json["id"],
        radical: json["radical"],
        intituleClient: json["intituleClient"],
        numeroCompte: json["numeroCompte"],
        deviseCompte: json["deviseCompte"],
        agenceCompte: json["agenceCompte"],
        intituleCompte: json["intituleCompte"],
        codeConvertibiliteCompte: json["codeConvertibiliteCompte"],
        identifiantDemande: json["identifiantDemande"],
        codeBanqueTransaction: json["codeBanqueTransaction"],
        intituleInitiateur: json["intituleInitiateur"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        idInitiateur: json["idInitiateur"],
        deviseOperation: json["deviseOperation"],
        version: json["version"],
        statut: json["statut"],
        statutCode: json["statutCode"],
        codeProduitCompte: json["codeProduitCompte"],
        choixOperateur: json["choixOperateur"],
        deviseOperationLibelle: json["deviseOperationLibelle"],
        deviseCompteLibelle: json["deviseCompteLibelle"],
        nomCompteDetaille: json["nomCompteDetaille"],
        numeroCompteCrediter: json["numeroCompteCrediter"],
        identifiantExterne: json["identifiantExterne"],
        dateEnvoiDemande: json["dateEnvoiDemande"],
        typeChequier: json["typeChequier"],
        typeChequierLibelle: json["typeChequierLibelle"],
        nombreChequiers: json["nombreChequiers"],
        identifierExterne: json["identifierExterne"],
        typeLcn: json["typeLCN"],
        typeLcnLibelle: json["typeLCNLibelle"],
        nombreLcn: json["nombreLCN"],
        type: json["type"],
        dateMin: json["dateMin"],
        dateMax: json["dateMax"],
        endorsable: json["endorsable"],
        crossed: json["crossed"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "radical": radical,
        "intituleClient": intituleClient,
        "numeroCompte": numeroCompte,
        "deviseCompte": deviseCompte,
        "agenceCompte": agenceCompte,
        "intituleCompte": intituleCompte,
        "codeConvertibiliteCompte": codeConvertibiliteCompte,
        "identifiantDemande": identifiantDemande,
        "codeBanqueTransaction": codeBanqueTransaction,
        "intituleInitiateur": intituleInitiateur,
        "codeBanqueAssocie": codeBanqueAssocie,
        "idInitiateur": idInitiateur,
        "deviseOperation": deviseOperation,
        "version": version,
        "statut": statut,
        "statutCode": statutCode,
        "codeProduitCompte": codeProduitCompte,
        "choixOperateur": choixOperateur,
        "deviseOperationLibelle": deviseOperationLibelle,
        "deviseCompteLibelle": deviseCompteLibelle,
        "nomCompteDetaille": nomCompteDetaille,
        "numeroCompteCrediter": numeroCompteCrediter,
        "identifiantExterne": identifiantExterne,
        "dateEnvoiDemande": dateEnvoiDemande,
        "typeChequier": typeChequier,
        "typeChequierLibelle": typeChequierLibelle,
        "nombreChequiers": nombreChequiers,
        "identifierExterne": identifierExterne,
        "typeLCN": typeLcn,
        "typeLCNLibelle": typeLcnLibelle,
        "nombreLCN": nombreLcn,
        "type": type,
        "dateMin": dateMin,
        "dateMax": dateMax,
        "endorsable": endorsable,
        "crossed": crossed,
      };
}
