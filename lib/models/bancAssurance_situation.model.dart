class BancAssuranceSituation {
  MapClass map;

  BancAssuranceSituation({this.map});

  BancAssuranceSituation.fromJson(Map<String, dynamic> json) {
    map = json['Map'] != null ? new MapClass.fromJson(json['Map']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.map != null) {
      data['Map'] = this.map.toJson();
    }
    return data;
  }
}

class MapClass {
  Situation situation;
  bool success;

  MapClass({this.situation, this.success});

  MapClass.fromJson(Map<String, dynamic> json) {
    situation = json['SITUATION'] != null
        ? new Situation.fromJson(json['SITUATION'])
        : null;
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.situation != null) {
      data['SITUATION'] = this.situation.toJson();
    }
    data['success'] = this.success;
    return data;
  }
}

class Situation {
  String numeroPolice;
  String epargneRevalorise;
  String cumulCotisations;
  String valeurRachatBrute;
  String plafondAvanceAccorder;
  String avanceAccordeeMajoreeInterets;
  String plafondRachatPartiel;

  Situation(
      {this.numeroPolice,
        this.epargneRevalorise,
        this.cumulCotisations,
        this.valeurRachatBrute,
        this.plafondAvanceAccorder,
        this.avanceAccordeeMajoreeInterets,
        this.plafondRachatPartiel});

  Situation.fromJson(Map<String, dynamic> json) {
    numeroPolice = json['numeroPolice'];
    epargneRevalorise = json['epargneRevalorise'];
    cumulCotisations = json['cumulCotisations'];
    valeurRachatBrute = json['valeurRachatBrute'];
    plafondAvanceAccorder = json['plafondAvanceAccorder'];
    avanceAccordeeMajoreeInterets = json['avanceAccordeeMajoreeInterets'];
    plafondRachatPartiel = json['plafondRachatPartiel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numeroPolice'] = this.numeroPolice;
    data['epargneRevalorise'] = this.epargneRevalorise;
    data['cumulCotisations'] = this.cumulCotisations;
    data['valeurRachatBrute'] = this.valeurRachatBrute;
    data['plafondAvanceAccorder'] = this.plafondAvanceAccorder;
    data['avanceAccordeeMajoreeInterets'] = this.avanceAccordeeMajoreeInterets;
    data['plafondRachatPartiel'] = this.plafondRachatPartiel;
    return data;
  }
}