// To parse this JSON data, do
//
//     final rechercheMouvement = rechercheMouvementFromJson(jsonString);

import 'dart:convert';

import 'package:LBA/models/historique.model.dart';
import 'package:intl/intl.dart';

RechercheMouvement rechercheMouvementFromJson(String str) =>
    RechercheMouvement.fromJson(json.decode(str));

String rechercheMouvementToJson(RechercheMouvement data) =>
    json.encode(data.toJson());

class RechercheMouvement {
  RechercheMouvement({
    this.map,
  });

  MapClass map;

  factory RechercheMouvement.fromJson(Map<String, dynamic> json) =>
      RechercheMouvement(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.resultsTotal,
    this.success,
    this.results,
  });

  int resultsTotal;
  bool success;
  List<Result> results;

  Map<String, List<Result>> orderedlistMouvements = new Map();

  transform(Mode mode) {
    results.forEach((item) {
      var d = DateFormat("dd-MM-yyyy").parse(item.resultDateValeur);
      // print(d.month);
      String goupKey;
      switch (mode) {
        case Mode.MONTH:
          goupKey = "${d.year}  ${DateFormat.MMMM('fr').format(d)}";
          break;
        case Mode.YEAR:
          goupKey = "${d.year}";
          break;
        case Mode.DAY:
          goupKey = "${d.year}  ${DateFormat.MMMM('fr').format(d)}  ${d.day}";
          break;
        case Mode.WEEK:
          goupKey =
              "${d.year}  ${DateFormat.MMMM('fr').format(d)}  ${d.weekday}";
          break;
      }

      if (orderedlistMouvements[goupKey] == null) {
        orderedlistMouvements[goupKey] = [];
      }
      orderedlistMouvements[goupKey].add(item);
    });
  }

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        resultsTotal: json["resultsTotal"],
        success: json["success"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "resultsTotal": resultsTotal,
        "success": success,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.sens,
    this.codeOperation,
    this.libelle,
    this.dateValeur,
    this.resultCredit,
    this.id,
    this.dateOperation,
    this.credit,
    this.devise,
    this.referenceClient,
    this.identifiantMouvement,
    this.libelleComplementaire,
    this.resultDebit,
    this.resultDateValeur,
    this.resultDateOperation,
    this.debit,
  });

  String sens;
  String codeOperation;
  String libelle;
  String dateValeur;
  String resultCredit;
  int id;
  String dateOperation;
  int credit;
  String devise;
  dynamic referenceClient;
  dynamic identifiantMouvement;
  String libelleComplementaire;
  String resultDebit;
  String resultDateValeur;
  String resultDateOperation;
  double debit;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        sens: json["sens"],
        codeOperation: json["codeOperation"],
        libelle: json["libelle"],
        dateValeur: json["dateValeur"],
        resultCredit: json["credit_"] == null ? null : json["credit_"],
        id: json["id"] == null ? null : json["id"],
        dateOperation: json["dateOperation"],
        credit: json["credit"] == null ? null : json["credit"],
        devise: json["devise"],
        referenceClient: json["referenceClient"],
        identifiantMouvement: json["identifiantMouvement"],
        libelleComplementaire: json["libelleComplementaire"] == null
            ? null
            : json["libelleComplementaire"],
        resultDebit: json["debit_"] == null ? null : json["debit_"],
        resultDateValeur:
            json["dateValeur_"] == null ? null : json["dateValeur_"],
        resultDateOperation:
            json["dateOperation_"] == null ? null : json["dateOperation_"],
        debit: json["debit"] == null ? null : json["debit"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "sens": sens,
        "codeOperation": codeOperation,
        "libelle": libelle,
        "dateValeur": dateValeur,
        "credit_": resultCredit == null ? null : resultCredit,
        "id": id == null ? null : id,
        "dateOperation": dateOperation,
        "credit": credit == null ? null : credit,
        "devise": devise,
        "referenceClient": referenceClient,
        "identifiantMouvement": identifiantMouvement,
        "libelleComplementaire":
            libelleComplementaire == null ? null : libelleComplementaire,
        "debit_": resultDebit == null ? null : resultDebit,
        "dateValeur_": resultDateValeur == null ? null : resultDateValeur,
        "dateOperation_":
            resultDateOperation == null ? null : resultDateOperation,
        "debit": debit == null ? null : debit,
      };
}
