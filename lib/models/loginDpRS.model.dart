class LoginDpRS {
  bool success;
  String username;
  String userId;
  String errorCode;
  String errorMsg;
  String csrfToken;
  bool token;
  String radical;
  String intitule;
  String telephone;
  String tokenFirstAttempt;
  String codeLangue;
  int eligibilityContrat;
  List<Authorities> authorities;
  SignatureMethods signatureMethods;
  List<Contracts> contracts;
  EncryptedPhone encryptedPhone;

  LoginDpRS(
      {this.success,
        this.username,
        this.userId,
        this.errorCode,
        this.errorMsg,
        this.csrfToken,
        this.token,
        this.radical,
        this.intitule,
        this.telephone,
        this.tokenFirstAttempt,
        this.codeLangue,
        this.eligibilityContrat,
        this.authorities,
        this.signatureMethods,
        this.contracts,
        this.encryptedPhone});

  LoginDpRS.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    username = json['username'];
    userId = json['userId'];
    errorCode = json['errorCode'];
    errorMsg = json['errorMsg'];
    csrfToken = json['csrfToken'];
    token = json['token'];
    radical = json['radical'];
    intitule = json['intitule'];
    telephone = json['telephone'];
    tokenFirstAttempt = json['tokenFirstAttempt'];
    codeLangue = json['codeLangue'];
    eligibilityContrat = json['eligibilityContrat'];
    if (json['authorities'] != null) {
      authorities = <Authorities>[];
      json['authorities'].forEach((v) {
        authorities.add(new Authorities.fromJson(v));
      });
    }
    signatureMethods = json['signatureMethods'] != null
        ? new SignatureMethods.fromJson(json['signatureMethods'])
        : null;
    if (json['contracts'] != null) {
      contracts = <Contracts>[];
      json['contracts'].forEach((v) {
        contracts.add(new Contracts.fromJson(v));
      });
    }
    encryptedPhone = json['encryptedPhone'] != null
        ? new EncryptedPhone.fromJson(json['encryptedPhone'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['username'] = this.username;
    data['userId'] = this.userId;
    data['errorCode'] = this.errorCode;
    data['errorMsg'] = this.errorMsg;
    data['csrfToken'] = this.csrfToken;
    data['token'] = this.token;
    data['radical'] = this.radical;
    data['intitule'] = this.intitule;
    data['telephone'] = this.telephone;
    data['tokenFirstAttempt'] = this.tokenFirstAttempt;
    data['codeLangue'] = this.codeLangue;
    data['eligibilityContrat'] = this.eligibilityContrat;
    if (this.authorities != null) {
      data['authorities'] = this.authorities.map((v) => v.toJson()).toList();
    }
    if (this.signatureMethods != null) {
      data['signatureMethods'] = this.signatureMethods.toJson();
    }
    if (this.contracts != null) {
      data['contracts'] = this.contracts.map((v) => v.toJson()).toList();
    }
    if (this.encryptedPhone != null) {
      data['encryptedPhone'] = this.encryptedPhone.toJson();
    }
    return data;
  }
}

class Authorities {
  String authority;

  Authorities({this.authority});

  Authorities.fromJson(Map<String, dynamic> json) {
    authority = json['authority'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['authority'] = this.authority;
    return data;
  }
}

class SignatureMethods {
  String rGAB;
  String sD;
  String uBA;
  String aUTH;
  String dRT;
  String oW;
  String dPF;
  String tP2P;

  SignatureMethods(
      {this.rGAB,
        this.sD,
        this.uBA,
        this.aUTH,
        this.dRT,
        this.oW,
        this.dPF,
        this.tP2P});

  SignatureMethods.fromJson(Map<String, dynamic> json) {
    rGAB = json['RGAB'];
    sD = json['SD'];
    uBA = json['UBA'];
    aUTH = json['AUTH'];
    dRT = json['DRT'];
    oW = json['OW'];
    dPF = json['DPF'];
    tP2P = json['TP2P'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['RGAB'] = this.rGAB;
    data['SD'] = this.sD;
    data['UBA'] = this.uBA;
    data['AUTH'] = this.aUTH;
    data['DRT'] = this.dRT;
    data['OW'] = this.oW;
    data['DPF'] = this.dPF;
    data['TP2P'] = this.tP2P;
    return data;
  }
}

class Contracts {
  int id;
  String identifiantContrat;
  int nombreAbonnes;
  String typeClientLib;
  String intituleRC;
  String radical;
  String agence;
  String dateSouscription;
  String packType;
  bool attached;
  bool defaultContract;

  Contracts({
    this.id,
    this.identifiantContrat,
    this.nombreAbonnes,
    this.typeClientLib,
    this.intituleRC,
    this.radical,
    this.agence,
    this.dateSouscription,
    this.packType,
    this.attached,
    this.defaultContract,
  });

  Contracts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    identifiantContrat = json['identifiantContrat'];
    nombreAbonnes = json['nombreAbonnes'];
    typeClientLib = json['typeClient_lib'];
    intituleRC = json['intituleRC'];
    radical = json['radical'];
    agence = json['agence'];
    dateSouscription = json['dateSouscription'];
    packType = json['packType'];
    attached = json['attached'];
    defaultContract = json['defaultContract'];
    typeClientLib = json['typeClientLib'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['identifiantContrat'] = this.identifiantContrat;
    data['nombreAbonnes'] = this.nombreAbonnes;
    data['typeClient_lib'] = this.typeClientLib;
    data['intituleRC'] = this.intituleRC;
    data['radical'] = this.radical;
    data['agence'] = this.agence;
    data['dateSouscription'] = this.dateSouscription;
    data['packType'] = this.packType;
    data['attached'] = this.attached;
    data['defaultContract'] = this.defaultContract;
    data['typeClientLib'] = this.typeClientLib;
    return data;
  }
}

class EncryptedPhone {
  String data;
  String cipher;
  String iv;
  String keyIndex;
  String algorithme;

  EncryptedPhone(
      {this.data, this.cipher, this.iv, this.keyIndex, this.algorithme});

  EncryptedPhone.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    cipher = json['cipher'];
    iv = json['iv'];
    keyIndex = json['keyIndex'];
    algorithme = json['algorithme'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['cipher'] = this.cipher;
    data['iv'] = this.iv;
    data['keyIndex'] = this.keyIndex;
    data['algorithme'] = this.algorithme;
    return data;
  }
}