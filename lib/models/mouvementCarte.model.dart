// To parse this JSON data, do
//
//     final mouvementCarte = mouvementCarteFromJson(jsonString);

import 'dart:convert';

MouvementCarte mouvementCarteFromJson(String str) => MouvementCarte.fromJson(json.decode(str));

String mouvementCarteToJson(MouvementCarte data) => json.encode(data.toJson());

class MouvementCarte {
    MouvementCarte({
        this.map,
    });

    MapClass map;

    factory MouvementCarte.fromJson(Map<String, dynamic> json) => MouvementCarte(
        map: MapClass.fromJson(json["Map"]),
    );

    Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
    };
}

class MapClass {
    MapClass({
        this.mouvementCarteTotal,
        this.mouvementCarteList,
    });

    int mouvementCarteTotal;
    List<dynamic> mouvementCarteList;

    factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        mouvementCarteTotal: json["mouvementCarteTotal"],
        mouvementCarteList: List<dynamic>.from(json["mouvementCarteList"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "mouvementCarteTotal": mouvementCarteTotal,
        "mouvementCarteList": List<dynamic>.from(mouvementCarteList.map((x) => x)),
    };
}
