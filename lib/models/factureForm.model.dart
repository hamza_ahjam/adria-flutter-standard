// To parse this JSON data, do
//
//     final factureForm = factureFormFromJson(jsonString);

import 'dart:convert';

FactureForm factureFormFromJson(String str) =>
    FactureForm.fromJson(json.decode(str));

String factureFormToJson(FactureForm data) => json.encode(data.toJson());

class FactureForm {
  FactureForm({
    this.map,
  });

  MapClass map;

  factory FactureForm.fromJson(Map<String, dynamic> json) => FactureForm(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.listeColonnes,
    this.demandePaiementFactureInstance,
    this.detailsCreance,
    this.impayes,
    this.success,
    this.transactionInstance,
    this.type,
  });

  List<String> listeColonnes;
  DemandePaiementFactureInstance demandePaiementFactureInstance;
  DetailsCreance detailsCreance;
  String impayes;
  bool success;
  TransactionInstance transactionInstance;
  String type;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        listeColonnes: List<String>.from(json["listeColonnes"].map((x) => x)),
        demandePaiementFactureInstance: DemandePaiementFactureInstance.fromJson(
            json["demandePaiementFactureInstance"]),
        detailsCreance: DetailsCreance.fromJson(json["detailsCreance"]),
        impayes: json["impayes"],
        success: json["success"],
        transactionInstance:
            TransactionInstance.fromJson(json["transactionInstance"]),
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "listeColonnes": List<dynamic>.from(listeColonnes.map((x) => x)),
        "demandePaiementFactureInstance":
            demandePaiementFactureInstance.toJson(),
        "detailsCreance": detailsCreance.toJson(),
        "impayes": impayes,
        "success": success,
        "transactionInstance": transactionInstance.toJson(),
        "type": type,
      };
}

class DemandePaiementFactureInstance {
  DemandePaiementFactureInstance({
    this.id,
    this.radical,
    this.intituleClient,
    this.numeroCompte,
    this.deviseCompte,
    this.agenceCompte,
    this.intituleCompte,
    this.codeConvertibiliteCompte,
    this.identifiantDemande,
    this.codeBanqueTransaction,
    this.intituleInitiateur,
    this.codeBanqueAssocie,
    this.idInitiateur,
    this.deviseOperation,
    this.version,
    this.statut,
    this.statutCode,
    this.codeProduitCompte,
    this.choixOperateur,
    this.deviseOperationLibelle,
    this.deviseCompteLibelle,
    this.nomCompteDetaille,
    this.numeroCompteCrediter,
    this.identifiantExterne,
    this.dateEnvoiDemande,
    this.typeChequier,
    this.typeChequierLibelle,
    this.nombreChequiers,
    this.identifierExterne,
    this.typeLcn,
    this.typeLcnLibelle,
    this.nombreLcn,
    this.type,
    this.dateMin,
    this.dateMax,
    this.endorsable,
    this.crossed,
    this.dateSignature,
    this.dateValidation,
    this.creancierId,
    this.creanceId,
    this.codeReconciliation,
    this.montantTotalTtc,
    this.montantTotalTtcToString,
    this.codeAutorisation,
    this.empreintPaiement,
    this.paiementTotal,
    this.impayes,
    this.referenceReglement,
    this.canceled,
    this.seuilMinimal,
    this.libelleCreance,
    this.libelleCreancier,
    this.typeFrais,
    this.impayesJson,
    this.referencePartenaire,
    this.rib,
    this.valeurFrais,
    this.paramsGlobal,
    this.crc,
    this.pathLogo,
    this.reclamation,
    this.nomClient,
    this.emailClient,
    this.numTransactionBanque,
    this.reference,
    this.description,
    this.montant,
    this.frais,
    this.trancationMtc,
    this.impayesForSave,
    this.impayesForUpdate,
  });

  dynamic id;
  String radical;
  dynamic intituleClient;
  dynamic numeroCompte;
  dynamic deviseCompte;
  dynamic agenceCompte;
  dynamic intituleCompte;
  dynamic codeConvertibiliteCompte;
  dynamic identifiantDemande;
  dynamic codeBanqueTransaction;
  dynamic intituleInitiateur;
  dynamic codeBanqueAssocie;
  dynamic idInitiateur;
  dynamic deviseOperation;
  dynamic version;
  dynamic statut;
  dynamic statutCode;
  dynamic codeProduitCompte;
  dynamic choixOperateur;
  dynamic deviseOperationLibelle;
  dynamic deviseCompteLibelle;
  dynamic nomCompteDetaille;
  dynamic numeroCompteCrediter;
  dynamic identifiantExterne;
  dynamic dateEnvoiDemande;
  dynamic typeChequier;
  dynamic typeChequierLibelle;
  int nombreChequiers;
  dynamic identifierExterne;
  dynamic typeLcn;
  dynamic typeLcnLibelle;
  int nombreLcn;
  dynamic type;
  dynamic dateMin;
  dynamic dateMax;
  bool endorsable;
  bool crossed;
  dynamic dateSignature;
  dynamic dateValidation;
  String creancierId;
  String creanceId;
  dynamic codeReconciliation;
  double montantTotalTtc;
  dynamic montantTotalTtcToString;
  dynamic codeAutorisation;
  dynamic empreintPaiement;
  dynamic paiementTotal;
  dynamic impayes;
  dynamic referenceReglement;
  dynamic canceled;
  int seuilMinimal;
  String libelleCreance;
  dynamic libelleCreancier;
  dynamic typeFrais;
  String impayesJson;
  String referencePartenaire;
  dynamic rib;
  dynamic valeurFrais;
  List<ParamsGlobal> paramsGlobal;
  dynamic crc;
  String pathLogo;
  dynamic reclamation;
  dynamic nomClient;
  dynamic emailClient;
  dynamic numTransactionBanque;
  dynamic reference;
  dynamic description;
  dynamic montant;
  dynamic frais;
  dynamic trancationMtc;
  List<Impaye> impayesForSave;
  dynamic impayesForUpdate;

  factory DemandePaiementFactureInstance.fromJson(Map<String, dynamic> json) =>
      DemandePaiementFactureInstance(
        id: json["id"],
        radical: json["radical"],
        intituleClient: json["intituleClient"],
        numeroCompte: json["numeroCompte"],
        deviseCompte: json["deviseCompte"],
        agenceCompte: json["agenceCompte"],
        intituleCompte: json["intituleCompte"],
        codeConvertibiliteCompte: json["codeConvertibiliteCompte"],
        identifiantDemande: json["identifiantDemande"],
        codeBanqueTransaction: json["codeBanqueTransaction"],
        intituleInitiateur: json["intituleInitiateur"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        idInitiateur: json["idInitiateur"],
        deviseOperation: json["deviseOperation"],
        version: json["version"],
        statut: json["statut"],
        statutCode: json["statutCode"],
        codeProduitCompte: json["codeProduitCompte"],
        choixOperateur: json["choixOperateur"],
        deviseOperationLibelle: json["deviseOperationLibelle"],
        deviseCompteLibelle: json["deviseCompteLibelle"],
        nomCompteDetaille: json["nomCompteDetaille"],
        numeroCompteCrediter: json["numeroCompteCrediter"],
        identifiantExterne: json["identifiantExterne"],
        dateEnvoiDemande: json["dateEnvoiDemande"],
        typeChequier: json["typeChequier"],
        typeChequierLibelle: json["typeChequierLibelle"],
        nombreChequiers: json["nombreChequiers"],
        identifierExterne: json["identifierExterne"],
        typeLcn: json["typeLCN"],
        typeLcnLibelle: json["typeLCNLibelle"],
        nombreLcn: json["nombreLCN"],
        type: json["type"],
        dateMin: json["dateMin"],
        dateMax: json["dateMax"],
        endorsable: json["endorsable"],
        crossed: json["crossed"],
        dateSignature: json["dateSignature"],
        dateValidation: json["dateValidation"],
        creancierId: json["creancierId"],
        creanceId: json["creanceId"],
        codeReconciliation: json["codeReconciliation"],
        montantTotalTtc: json["montantTotalTTC"].toDouble(),
        montantTotalTtcToString: json["montantTotalTTCToString"],
        codeAutorisation: json["codeAutorisation"],
        empreintPaiement: json["empreintPaiement"],
        paiementTotal: json["paiementTotal"],
        impayes: json["impayes"],
        referenceReglement: json["referenceReglement"],
        canceled: json["canceled"],
        seuilMinimal: json["seuilMinimal"],
        libelleCreance: json["libelleCreance"],
        libelleCreancier: json["libelleCreancier"],
        typeFrais: json["typeFrais"],
        impayesJson: json["impayesJson"],
        referencePartenaire: json["referencePartenaire"],
        rib: json["rib"],
        valeurFrais: json["valeurFrais"],
        paramsGlobal: List<ParamsGlobal>.from(
            json["paramsGlobal"].map((x) => ParamsGlobal.fromJson(x))),
        crc: json["crc"],
        pathLogo: json["pathLogo"],
        reclamation: json["reclamation"],
        nomClient: json["nomClient"],
        emailClient: json["emailClient"],
        numTransactionBanque: json["numTransactionBanque"],
        reference: json["reference"],
        description: json["description"],
        montant: json["montant"],
        frais: json["frais"],
        trancationMtc: json["trancationMTC"],
        impayesForSave: List<Impaye>.from(
            json["impayesForSave"].map((x) => Impaye.fromJson(x))),
        impayesForUpdate: json["impayesForUpdate"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "radical": radical,
        "intituleClient": intituleClient,
        "numeroCompte": numeroCompte,
        "deviseCompte": deviseCompte,
        "agenceCompte": agenceCompte,
        "intituleCompte": intituleCompte,
        "codeConvertibiliteCompte": codeConvertibiliteCompte,
        "identifiantDemande": identifiantDemande,
        "codeBanqueTransaction": codeBanqueTransaction,
        "intituleInitiateur": intituleInitiateur,
        "codeBanqueAssocie": codeBanqueAssocie,
        "idInitiateur": idInitiateur,
        "deviseOperation": deviseOperation,
        "version": version,
        "statut": statut,
        "statutCode": statutCode,
        "codeProduitCompte": codeProduitCompte,
        "choixOperateur": choixOperateur,
        "deviseOperationLibelle": deviseOperationLibelle,
        "deviseCompteLibelle": deviseCompteLibelle,
        "nomCompteDetaille": nomCompteDetaille,
        "numeroCompteCrediter": numeroCompteCrediter,
        "identifiantExterne": identifiantExterne,
        "dateEnvoiDemande": dateEnvoiDemande,
        "typeChequier": typeChequier,
        "typeChequierLibelle": typeChequierLibelle,
        "nombreChequiers": nombreChequiers,
        "identifierExterne": identifierExterne,
        "typeLCN": typeLcn,
        "typeLCNLibelle": typeLcnLibelle,
        "nombreLCN": nombreLcn,
        "type": type,
        "dateMin": dateMin,
        "dateMax": dateMax,
        "endorsable": endorsable,
        "crossed": crossed,
        "dateSignature": dateSignature,
        "dateValidation": dateValidation,
        "creancierId": creancierId,
        "creanceId": creanceId,
        "codeReconciliation": codeReconciliation,
        "montantTotalTTC": montantTotalTtc,
        "montantTotalTTCToString": montantTotalTtcToString,
        "codeAutorisation": codeAutorisation,
        "empreintPaiement": empreintPaiement,
        "paiementTotal": paiementTotal,
        "impayes": impayes,
        "referenceReglement": referenceReglement,
        "canceled": canceled,
        "seuilMinimal": seuilMinimal,
        "libelleCreance": libelleCreance,
        "libelleCreancier": libelleCreancier,
        "typeFrais": typeFrais,
        "impayesJson": impayesJson,
        "referencePartenaire": referencePartenaire,
        "rib": rib,
        "valeurFrais": valeurFrais,
        "paramsGlobal": List<dynamic>.from(paramsGlobal.map((x) => x.toJson())),
        "crc": crc,
        "pathLogo": pathLogo,
        "reclamation": reclamation,
        "nomClient": nomClient,
        "emailClient": emailClient,
        "numTransactionBanque": numTransactionBanque,
        "reference": reference,
        "description": description,
        "montant": montant,
        "frais": frais,
        "trancationMTC": trancationMtc,
        "impayesForSave":
            List<dynamic>.from(impayesForSave.map((x) => x.toJson())),
        "impayesForUpdate": impayesForUpdate,
      };
}

class Impaye {
  Impaye({
    this.creancierId,
    this.creanceId,
    this.idArticle,
    this.dateFacture,
    this.prixTtc,
    this.prixTtcToString,
    this.typeArticle,
    this.libelleTypeArticle,
    this.referenceTransaction,
    this.properties,
    this.dateFactureFormatted,
    this.description,
  });

  dynamic creancierId;
  dynamic creanceId;
  String idArticle;
  String dateFacture;
  double prixTtc;
  String prixTtcToString;
  String typeArticle;
  dynamic libelleTypeArticle;
  dynamic referenceTransaction;
  dynamic properties;
  String dateFactureFormatted;
  String description;

  factory Impaye.fromJson(Map<String, dynamic> json) => Impaye(
        creancierId: json["creancierId"],
        creanceId: json["creanceId"],
        idArticle: json["idArticle"],
        dateFacture: json["dateFacture"],
        prixTtc: json["prixTTC"].toDouble(),
        prixTtcToString: json["prixTTCToString"],
        typeArticle: json["typeArticle"],
        libelleTypeArticle: json["libelleTypeArticle"],
        referenceTransaction: json["referenceTransaction"],
        properties: json["properties"],
        dateFactureFormatted: json["dateFactureFormatted"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "creancierId": creancierId,
        "creanceId": creanceId,
        "idArticle": idArticle,
        "dateFacture": dateFacture,
        "prixTTC": prixTtc,
        "prixTTCToString": prixTtcToString,
        "typeArticle": typeArticle,
        "libelleTypeArticle": libelleTypeArticle,
        "referenceTransaction": referenceTransaction,
        "properties": properties,
        "dateFactureFormatted": dateFactureFormatted,
        "description": description,
      };
}

class ParamsGlobal {
  ParamsGlobal({
    this.libelle,
    this.nomChamp,
    this.valeurChamp,
  });

  String libelle;
  String nomChamp;
  String valeurChamp;

  factory ParamsGlobal.fromJson(Map<String, dynamic> json) => ParamsGlobal(
        libelle: json["libelle"],
        nomChamp: json["nomChamp"],
        valeurChamp: json["valeurChamp"],
      );

  Map<String, dynamic> toJson() => {
        "libelle": libelle,
        "nomChamp": nomChamp,
        "valeurChamp": valeurChamp,
      };
}

class DetailsCreance {
  DetailsCreance({
    this.libelleCreance,
    this.libelleCreancier,
    this.codeCreance,
    this.codeCreancier,
    this.typeFrais,
    this.valeurFrais,
    this.seuilleMinimal,
    this.seuilleMinimalToString,
    this.codeDevise,
    this.montantTotalTtc,
    this.montantTotalTtcToString,
    this.impayes,
    this.id,
    this.referencePartenaire,
    this.paramsGlobal,
    this.nbrCreances,
    this.statut,
  });

  dynamic libelleCreance;
  dynamic libelleCreancier;
  dynamic codeCreance;
  dynamic codeCreancier;
  dynamic typeFrais;
  dynamic valeurFrais;
  int seuilleMinimal;
  dynamic seuilleMinimalToString;
  dynamic codeDevise;
  double montantTotalTtc;
  String montantTotalTtcToString;
  List<Impaye> impayes;
  int id;
  String referencePartenaire;
  List<ParamsGlobal> paramsGlobal;
  int nbrCreances;
  String statut;

  factory DetailsCreance.fromJson(Map<String, dynamic> json) => DetailsCreance(
        libelleCreance: json["libelleCreance"],
        libelleCreancier: json["libelleCreancier"],
        codeCreance: json["codeCreance"],
        codeCreancier: json["codeCreancier"],
        typeFrais: json["typeFrais"],
        valeurFrais: json["valeurFrais"],
        seuilleMinimal: json["seuilleMinimal"],
        seuilleMinimalToString: json["seuilleMinimalToString"],
        codeDevise: json["codeDevise"],
        montantTotalTtc: json["montantTotalTTC"].toDouble(),
        montantTotalTtcToString: json["montantTotalTTCToString"],
        impayes:
            List<Impaye>.from(json["impayes"].map((x) => Impaye.fromJson(x))),
        id: json["id"],
        referencePartenaire: json["referencePartenaire"],
        paramsGlobal: List<ParamsGlobal>.from(
            json["paramsGlobal"].map((x) => ParamsGlobal.fromJson(x))),
        nbrCreances: json["nbrCreances"],
        statut: json["statut"],
      );

  Map<String, dynamic> toJson() => {
        "libelleCreance": libelleCreance,
        "libelleCreancier": libelleCreancier,
        "codeCreance": codeCreance,
        "codeCreancier": codeCreancier,
        "typeFrais": typeFrais,
        "valeurFrais": valeurFrais,
        "seuilleMinimal": seuilleMinimal,
        "seuilleMinimalToString": seuilleMinimalToString,
        "codeDevise": codeDevise,
        "montantTotalTTC": montantTotalTtc,
        "montantTotalTTCToString": montantTotalTtcToString,
        "impayes": List<dynamic>.from(impayes.map((x) => x.toJson())),
        "id": id,
        "referencePartenaire": referencePartenaire,
        "paramsGlobal": List<dynamic>.from(paramsGlobal.map((x) => x.toJson())),
        "nbrCreances": nbrCreances,
        "statut": statut,
      };
}

class TransactionInstance {
  TransactionInstance({
    this.id,
    this.lang,
    this.datePattern,
    this.max,
    this.page,
    this.sort,
    this.order,
    this.codeBanque,
    this.userId,
    this.version,
    this.codeBanqueAssocie,
    this.codePaysAssocie,
    this.identifiant,
    this.taskId,
    this.jpassword,
    this.radical,
    this.intituleClient,
    this.numeroCompte,
    this.deviseCompte,
    this.agenceCompte,
    this.intituleCompte,
    this.codeConvertibiliteCompte,
    this.identifiantDemande,
    this.codeBanqueTransaction,
    this.intituleInitiateur,
    this.idInitiateur,
    this.deviseOperation,
    this.choixOperateur,
    this.cleRibCompte,
    this.cleControleCompte,
    this.codeProduitCompte,
    this.typeVirement,
    this.fullName,
    this.motifRetourInitiateur,
    this.dateEnvoiDemande,
    this.dateSignature,
    this.dateValidation,
    this.codeReconciliation,
    this.montantTotalTtc,
    this.montantTotalTtcToString,
    this.codeAutorisation,
    this.empreintPaiement,
    this.paiementTotal,
    this.impayes,
    this.referenceReglement,
    this.canceled,
    this.montantTotalTtcapAyer,
    this.codeCreance,
    this.codeCreancier,
    this.libelleCreance,
    this.libelleCreancier,
    this.idArticle,
    this.idCompteDebiter,
    this.seuilMinimal,
    this.typeFrais,
    this.valeurFrais,
    this.impayesJson,
    this.impayesDetails,
    this.referencePartenaire,
    this.rib,
    this.type,
    this.paramsGlobal,
    this.crc,
  });

  dynamic id;
  dynamic lang;
  dynamic datePattern;
  dynamic max;
  dynamic page;
  dynamic sort;
  dynamic order;
  dynamic codeBanque;
  dynamic userId;
  int version;
  String codeBanqueAssocie;
  String codePaysAssocie;
  dynamic identifiant;
  dynamic taskId;
  dynamic jpassword;
  dynamic radical;
  dynamic intituleClient;
  dynamic numeroCompte;
  dynamic deviseCompte;
  dynamic agenceCompte;
  dynamic intituleCompte;
  dynamic codeConvertibiliteCompte;
  dynamic identifiantDemande;
  dynamic codeBanqueTransaction;
  dynamic intituleInitiateur;
  dynamic idInitiateur;
  dynamic deviseOperation;
  dynamic choixOperateur;
  dynamic cleRibCompte;
  dynamic cleControleCompte;
  dynamic codeProduitCompte;
  dynamic typeVirement;
  dynamic fullName;
  dynamic motifRetourInitiateur;
  dynamic dateEnvoiDemande;
  dynamic dateSignature;
  dynamic dateValidation;
  dynamic codeReconciliation;
  dynamic montantTotalTtc;
  dynamic montantTotalTtcToString;
  dynamic codeAutorisation;
  dynamic empreintPaiement;
  dynamic paiementTotal;
  dynamic impayes;
  dynamic referenceReglement;
  dynamic canceled;
  dynamic montantTotalTtcapAyer;
  dynamic codeCreance;
  dynamic codeCreancier;
  dynamic libelleCreance;
  dynamic libelleCreancier;
  dynamic idArticle;
  dynamic idCompteDebiter;
  dynamic seuilMinimal;
  dynamic typeFrais;
  dynamic valeurFrais;
  dynamic impayesJson;
  dynamic impayesDetails;
  dynamic referencePartenaire;
  dynamic rib;
  dynamic type;
  dynamic paramsGlobal;
  dynamic crc;

  factory TransactionInstance.fromJson(Map<String, dynamic> json) =>
      TransactionInstance(
        id: json["id"],
        lang: json["lang"],
        datePattern: json["datePattern"],
        max: json["max"],
        page: json["page"],
        sort: json["sort"],
        order: json["order"],
        codeBanque: json["codeBanque"],
        userId: json["userId"],
        version: json["version"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        codePaysAssocie: json["codePaysAssocie"],
        identifiant: json["identifiant"],
        taskId: json["taskId"],
        jpassword: json["jpassword"],
        radical: json["radical"],
        intituleClient: json["intituleClient"],
        numeroCompte: json["numeroCompte"],
        deviseCompte: json["deviseCompte"],
        agenceCompte: json["agenceCompte"],
        intituleCompte: json["intituleCompte"],
        codeConvertibiliteCompte: json["codeConvertibiliteCompte"],
        identifiantDemande: json["identifiantDemande"],
        codeBanqueTransaction: json["codeBanqueTransaction"],
        intituleInitiateur: json["intituleInitiateur"],
        idInitiateur: json["idInitiateur"],
        deviseOperation: json["deviseOperation"],
        choixOperateur: json["choixOperateur"],
        cleRibCompte: json["cleRibCompte"],
        cleControleCompte: json["cleControleCompte"],
        codeProduitCompte: json["codeProduitCompte"],
        typeVirement: json["typeVirement"],
        fullName: json["fullName"],
        motifRetourInitiateur: json["motifRetourInitiateur"],
        dateEnvoiDemande: json["dateEnvoiDemande"],
        dateSignature: json["dateSignature"],
        dateValidation: json["dateValidation"],
        codeReconciliation: json["codeReconciliation"],
        montantTotalTtc: json["montantTotalTTC"],
        montantTotalTtcToString: json["montantTotalTTCToString"],
        codeAutorisation: json["codeAutorisation"],
        empreintPaiement: json["empreintPaiement"],
        paiementTotal: json["paiementTotal"],
        impayes: json["impayes"],
        referenceReglement: json["referenceReglement"],
        canceled: json["canceled"],
        montantTotalTtcapAyer: json["montantTotalTTCAPAyer"],
        codeCreance: json["codeCreance"],
        codeCreancier: json["codeCreancier"],
        libelleCreance: json["libelleCreance"],
        libelleCreancier: json["libelleCreancier"],
        idArticle: json["idArticle"],
        idCompteDebiter: json["idCompteDebiter"],
        seuilMinimal: json["seuilMinimal"],
        typeFrais: json["typeFrais"],
        valeurFrais: json["valeurFrais"],
        impayesJson: json["impayesJson"],
        impayesDetails: json["impayesDetails"],
        referencePartenaire: json["referencePartenaire"],
        rib: json["rib"],
        type: json["type"],
        paramsGlobal: json["paramsGlobal"],
        crc: json["crc"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lang": lang,
        "datePattern": datePattern,
        "max": max,
        "page": page,
        "sort": sort,
        "order": order,
        "codeBanque": codeBanque,
        "userId": userId,
        "version": version,
        "codeBanqueAssocie": codeBanqueAssocie,
        "codePaysAssocie": codePaysAssocie,
        "identifiant": identifiant,
        "taskId": taskId,
        "jpassword": jpassword,
        "radical": radical,
        "intituleClient": intituleClient,
        "numeroCompte": numeroCompte,
        "deviseCompte": deviseCompte,
        "agenceCompte": agenceCompte,
        "intituleCompte": intituleCompte,
        "codeConvertibiliteCompte": codeConvertibiliteCompte,
        "identifiantDemande": identifiantDemande,
        "codeBanqueTransaction": codeBanqueTransaction,
        "intituleInitiateur": intituleInitiateur,
        "idInitiateur": idInitiateur,
        "deviseOperation": deviseOperation,
        "choixOperateur": choixOperateur,
        "cleRibCompte": cleRibCompte,
        "cleControleCompte": cleControleCompte,
        "codeProduitCompte": codeProduitCompte,
        "typeVirement": typeVirement,
        "fullName": fullName,
        "motifRetourInitiateur": motifRetourInitiateur,
        "dateEnvoiDemande": dateEnvoiDemande,
        "dateSignature": dateSignature,
        "dateValidation": dateValidation,
        "codeReconciliation": codeReconciliation,
        "montantTotalTTC": montantTotalTtc,
        "montantTotalTTCToString": montantTotalTtcToString,
        "codeAutorisation": codeAutorisation,
        "empreintPaiement": empreintPaiement,
        "paiementTotal": paiementTotal,
        "impayes": impayes,
        "referenceReglement": referenceReglement,
        "canceled": canceled,
        "montantTotalTTCAPAyer": montantTotalTtcapAyer,
        "codeCreance": codeCreance,
        "codeCreancier": codeCreancier,
        "libelleCreance": libelleCreance,
        "libelleCreancier": libelleCreancier,
        "idArticle": idArticle,
        "idCompteDebiter": idCompteDebiter,
        "seuilMinimal": seuilMinimal,
        "typeFrais": typeFrais,
        "valeurFrais": valeurFrais,
        "impayesJson": impayesJson,
        "impayesDetails": impayesDetails,
        "referencePartenaire": referencePartenaire,
        "rib": rib,
        "type": type,
        "paramsGlobal": paramsGlobal,
        "crc": crc,
      };
}
