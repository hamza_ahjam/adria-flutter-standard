// To parse this JSON data, do
//
//     final periodicites = periodicitesFromJson(jsonString);

import 'dart:convert';

Periodicites periodicitesFromJson(String str) =>
    Periodicites.fromJson(json.decode(str));

String periodicitesToJson(Periodicites data) => json.encode(data.toJson());

class Periodicites {
  Periodicites({
    this.map,
  });

  MapClass map;

  factory Periodicites.fromJson(Map<String, dynamic> json) => Periodicites(
        map: MapClass.fromJson(json["Map"]),
      );

  Map<String, dynamic> toJson() => {
        "Map": map.toJson(),
      };
}

class MapClass {
  MapClass({
    this.periodiciteList,
  });

  List<PeriodiciteList> periodiciteList;

  factory MapClass.fromJson(Map<String, dynamic> json) => MapClass(
        periodiciteList: List<PeriodiciteList>.from(
            json["periodiciteList"].map((x) => PeriodiciteList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "periodiciteList":
            List<dynamic>.from(periodiciteList.map((x) => x.toJson())),
      };
}

class PeriodiciteList {
  PeriodiciteList({
    this.id,
    this.code,
    this.libelle,
    this.classe,
    this.codeLangue,
    this.codeNatureRemise,
    this.codeBanqueAssocie,
    this.codePaysAssocie,
    this.codeFamille,
    this.nombreDecimal,
    this.codeDevise,
    this.visible,
    this.codeSegmentAssocie,
    this.codeAlpha2,
    this.codeAlpha,
    this.isDefault,
    this.defaultValue,
    this.adresse1,
    this.adresse2,
    this.adresse3,
    this.ville,
    this.numeroTelephone,
    this.numeroFax,
    this.latitude,
    this.longitude,
    this.clazz,
    this.codeIso,
  });

  int id;
  String code;
  String libelle;
  String classe;
  String codeLangue;
  dynamic codeNatureRemise;
  String codeBanqueAssocie;
  dynamic codePaysAssocie;
  dynamic codeFamille;
  dynamic nombreDecimal;
  dynamic codeDevise;
  dynamic visible;
  dynamic codeSegmentAssocie;
  dynamic codeAlpha2;
  dynamic codeAlpha;
  dynamic isDefault;
  dynamic defaultValue;
  dynamic adresse1;
  dynamic adresse2;
  dynamic adresse3;
  dynamic ville;
  dynamic numeroTelephone;
  dynamic numeroFax;
  dynamic latitude;
  dynamic longitude;
  dynamic clazz;
  String codeIso;

  factory PeriodiciteList.fromJson(Map<String, dynamic> json) =>
      PeriodiciteList(
        id: json["id"],
        code: json["code"],
        libelle: json["libelle"],
        classe: json["classe"],
        codeLangue: json["codeLangue"],
        codeNatureRemise: json["codeNatureRemise"],
        codeBanqueAssocie: json["codeBanqueAssocie"],
        codePaysAssocie: json["codePaysAssocie"],
        codeFamille: json["codeFamille"],
        nombreDecimal: json["nombreDecimal"],
        codeDevise: json["codeDevise"],
        visible: json["visible"],
        codeSegmentAssocie: json["codeSegmentAssocie"],
        codeAlpha2: json["codeAlpha2"],
        codeAlpha: json["codeAlpha"],
        isDefault: json["isDefault"],
        defaultValue: json["defaultValue"],
        adresse1: json["adresse1"],
        adresse2: json["adresse2"],
        adresse3: json["adresse3"],
        ville: json["ville"],
        numeroTelephone: json["numeroTelephone"],
        numeroFax: json["numeroFax"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        clazz: json["clazz"],
        codeIso: json["codeIso"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "libelle": libelle,
        "classe": classe,
        "codeLangue": codeLangue,
        "codeNatureRemise": codeNatureRemise,
        "codeBanqueAssocie": codeBanqueAssocie,
        "codePaysAssocie": codePaysAssocie,
        "codeFamille": codeFamille,
        "nombreDecimal": nombreDecimal,
        "codeDevise": codeDevise,
        "visible": visible,
        "codeSegmentAssocie": codeSegmentAssocie,
        "codeAlpha2": codeAlpha2,
        "codeAlpha": codeAlpha,
        "isDefault": isDefault,
        "defaultValue": defaultValue,
        "adresse1": adresse1,
        "adresse2": adresse2,
        "adresse3": adresse3,
        "ville": ville,
        "numeroTelephone": numeroTelephone,
        "numeroFax": numeroFax,
        "latitude": latitude,
        "longitude": longitude,
        "clazz": clazz,
        "codeIso": codeIso,
      };
}
