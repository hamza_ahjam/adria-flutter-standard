class BancAssuranceCotisation {
  MapClass map;

  BancAssuranceCotisation({this.map});

  BancAssuranceCotisation.fromJson(Map<String, dynamic> json) {
    map = json['Map'] != null ? new MapClass.fromJson(json['Map']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.map != null) {
      data['Map'] = this.map.toJson();
    }
    return data;
  }
}

class MapClass {
  int total;
  bool success;
  List<ListCotisations> listCotisations;
  String message;

  MapClass({this.total, this.success, this.listCotisations, this.message});

  MapClass.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    success = json['success'];
    if (json['listCotisations'] != null) {
      listCotisations = <ListCotisations>[];
      json['listCotisations'].forEach((v) {
        listCotisations.add(new ListCotisations.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['success'] = this.success;
    if (this.listCotisations != null) {
      data['listCotisations'] =
          this.listCotisations.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class ListCotisations {
  String idContrat;
  String dateOperation;
  String montant;

  ListCotisations({this.idContrat, this.dateOperation, this.montant});

  ListCotisations.fromJson(Map<String, dynamic> json) {
    idContrat = json['idContrat'];
    dateOperation = json['dateOperation'];
    montant = json['montant'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idContrat'] = this.idContrat;
    data['dateOperation'] = this.dateOperation;
    data['montant'] = this.montant;
    return data;
  }
}