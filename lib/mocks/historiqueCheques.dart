const listHistoriqueCheques = {
  "Map": {
    "total": 2,
    "liste": [
      {
        "id": "CHQ123456781",
        "devise": "XOF",
        "datePaiement": "09-10-2020",
        "statut": "Payé",
        "agence": "00365",
        "montant": 2500,
        "numeroCompte" : "1536987412"
      },
      {
        "id": "CHQ123456782",
        "devise": "XOF",
        "datePaiement": "01-02-2021",
        "statut": "Encaissé",
        "agence": "00365",
        "montant": 5000,
        "numeroCompte" : "1536987412"
      },
      {
        "id": "CHQ123456783",
        "devise": "XOF",
        "datePaiement": "02-05-2020",
        "statut": "Impayé",
        "agence": "00365",
        "montant": 3000,
        "numeroCompte" : "1536987412"
      },
    ],
    "success": true
  }
};
