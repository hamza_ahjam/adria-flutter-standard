const dynamic plafondsMock = {
  "Map": {
    "plafonds": [
      {
        "codePlafond": "1011",
        "plafondPaiementInternationnal": 0.0,
        "plafondPaiementInternationnalFotmatted": "0",
        "plafondPaiements": 4000.0,
        "plafondPaiementsFotmatted": "4 000",
        "plafondRetrait": 2000.0,
        "plafondRetraitFotmatted": "2 000",
        "plafondRetraitInternationnal": 0.0,
        "plafondRetraitInternationnalFotmatted": "0"
      }
    ],
    "success": true,
    "error": "success",
    "message": "PLAFONTS POSSIBLES RETOURNES AVEC SUCCES"
  }
};
