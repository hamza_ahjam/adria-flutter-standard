const dynamic blocageCarteMock = {
  "Map": {
    "success": true,
    "error": "error",
    "message": "Operation reussie",
    "cardStatus": {
      "result": -1,
      "isContactLess": 0,
      "isBloquerInternational": 0,
      "isinStopList": 0,
      "isBloquerAll": 0,
      "isCanceled": 0,
      "isValid": 0,
      "message": "Operation reussie"
    }
  }
};

const dynamic plafondsCardMock = {
  "Map": {
    "success": true,
    "error": "success",
    "message": "Operation reussie",
  }
};
