const listCreditMock = {
  "Map": {
    "credits": [
      {
        "tauxGlobalDuCreditFormatted": null,
        "nombreEcheances": 0,
        "installmentPeriods": null,
        "loanAmount": null,
        "dateFine": null,
        "mensualite": 0,
        "capitalRestantDu": null,
        "montantPinalite": null,
        "nombreEcheancesPayes": 0,
        "nouveauTauxEndettement": null,
        "echeanceAvecAssurance": null,
        "tauxAssurance": null,
        "montantChargesEtCommissions": null,
        "montantImpaye": "0",
        "devise": null,
        "encours": null,
        "errCode": null,
        "disbursedLoan": null,
        "primeFlatAssurance": null,
        "nombreEcheancesRestantes": 0,
        "tauxAutofinancementDuProjet": null,
        "coutTotalDuCredit": null,
        "coutDuProjet": null,
        "tauxGlobalDuCredit": 6.75,
        "capitalRestantFormatted": null,
        "tauxPinalite": null,
        "dateDeblocage": null,
        "periodiciteInteret": "Mensuelle",
        "dateDeblocageFormatted": null,
        "dateFirstEcheance": "2009-01-01T00:00:00.000+0000",
        "dateStatut": null,
        "tauxNotaire": null,
        "montantProchaineEcheance": null,
        "capitalEmprunte": 77271.729999999996,
        "firstInstallmentDate": null,
        "nombreImpaye": "0",
        "echeanceAvecAssuranceFormatted": null,
        "dateFineFormatted": null,
        "typeDePret": "CONSOMMATION",
        "dateLastEcheance": "2014-09-01T01:00:00.000+0000",
        "idAbonne": null,
        "periodiciteCapital": null,
        "outstandingLoanAmount": null,
        "impayeCreditList": null,
        "dureeDePret": 69,
        "nombreEcheancesComing": 0,
        "dateDernierDeblocage": null,
        "installmentArrears": null,
        "apportPersonnel": null,
        "dateFirstEcheanceFormatted": "01-01-2009",
        "lastDisbursementDate": null,
        "montantAvance": null,
        "numeroDossier": "E606766WB21",
        "dateProchaineEcheance": "",
        "startDate": null,
        "coutFraisEtNotaire": null,
        "capitalRestant": 0,
        "fraisDeDossierDeCredit": null,
        "dateLastEcheanceeFormatted": "01-09-2014",
        "identifiant": null,
        "statut": null,
        "apportConjoint": null,
        "typeEcheance": null,
        "installmentDate": null,
        "typeAssurance": null,
        "montantDuPret": null,
        "intitule": null,
        "errorMessage": null,
        "idCreditClient": 0,
        "mensualiteFomated": null,
        "numeroCompte": "0146067666010123",
        "capitalEmprunteFormatted": null,
        "echeanceHorsAssurance": null
      },
      {
        "tauxGlobalDuCreditFormatted": null,
        "nombreEcheances": 0,
        "installmentPeriods": null,
        "loanAmount": null,
        "dateFine": null,
        "mensualite": 0,
        "capitalRestantDu": null,
        "montantPinalite": null,
        "nombreEcheancesPayes": 0,
        "nouveauTauxEndettement": null,
        "echeanceAvecAssurance": null,
        "tauxAssurance": null,
        "montantChargesEtCommissions": null,
        "montantImpaye": "0",
        "devise": null,
        "encours": null,
        "errCode": null,
        "disbursedLoan": null,
        "primeFlatAssurance": null,
        "nombreEcheancesRestantes": 0,
        "tauxAutofinancementDuProjet": null,
        "coutTotalDuCredit": null,
        "coutDuProjet": null,
        "tauxGlobalDuCredit": 6.25,
        "capitalRestantFormatted": null,
        "tauxPinalite": null,
        "dateDeblocage": null,
        "periodiciteInteret": "Mensuelle",
        "dateDeblocageFormatted": null,
        "dateFirstEcheance": "2010-01-30T00:00:00.000+0000",
        "dateStatut": null,
        "tauxNotaire": null,
        "montantProchaineEcheance": null,
        "capitalEmprunte": 895307.32999999996,
        "firstInstallmentDate": null,
        "nombreImpaye": "0",
        "echeanceAvecAssuranceFormatted": null,
        "dateFineFormatted": null,
        "typeDePret": "EQUIPEMENT",
        "dateLastEcheance": "2017-01-30T00:00:00.000+0000",
        "idAbonne": null,
        "periodiciteCapital": null,
        "outstandingLoanAmount": null,
        "impayeCreditList": null,
        "dureeDePret": 96,
        "nombreEcheancesComing": 0,
        "dateDernierDeblocage": null,
        "installmentArrears": null,
        "apportPersonnel": null,
        "dateFirstEcheanceFormatted": "30-01-2010",
        "lastDisbursementDate": null,
        "montantAvance": null,
        "numeroDossier": "09000076901",
        "dateProchaineEcheance": "",
        "startDate": null,
        "coutFraisEtNotaire": null,
        "capitalRestant": 0,
        "fraisDeDossierDeCredit": null,
        "dateLastEcheanceeFormatted": "30-01-2017",
        "identifiant": null,
        "statut": null,
        "apportConjoint": null,
        "typeEcheance": null,
        "installmentDate": null,
        "typeAssurance": null,
        "montantDuPret": null,
        "intitule": null,
        "errorMessage": null,
        "idCreditClient": 0,
        "mensualiteFomated": null,
        "numeroCompte": "0146067666610123",
        "capitalEmprunteFormatted": null,
        "echeanceHorsAssurance": null
      },
      {
        "tauxGlobalDuCreditFormatted": null,
        "nombreEcheances": 0,
        "installmentPeriods": null,
        "loanAmount": null,
        "dateFine": null,
        "mensualite": 0,
        "capitalRestantDu": null,
        "montantPinalite": null,
        "nombreEcheancesPayes": 0,
        "nouveauTauxEndettement": null,
        "echeanceAvecAssurance": null,
        "tauxAssurance": null,
        "montantChargesEtCommissions": null,
        "montantImpaye": "0",
        "devise": null,
        "encours": null,
        "errCode": null,
        "disbursedLoan": null,
        "primeFlatAssurance": null,
        "nombreEcheancesRestantes": 0,
        "tauxAutofinancementDuProjet": null,
        "coutTotalDuCredit": null,
        "coutDuProjet": null,
        "tauxGlobalDuCredit": 7.25,
        "capitalRestantFormatted": null,
        "tauxPinalite": null,
        "dateDeblocage": null,
        "periodiciteInteret": "Mensuelle",
        "dateDeblocageFormatted": null,
        "dateFirstEcheance": "2009-01-01T00:00:00.000+0000",
        "dateStatut": null,
        "tauxNotaire": null,
        "montantProchaineEcheance": null,
        "capitalEmprunte": 133090.39999999999,
        "firstInstallmentDate": null,
        "nombreImpaye": "0",
        "echeanceAvecAssuranceFormatted": null,
        "dateFineFormatted": null,
        "typeDePret": "HABITAT",
        "dateLastEcheance": "2011-02-01T00:00:00.000+0000",
        "idAbonne": null,
        "periodiciteCapital": null,
        "outstandingLoanAmount": null,
        "impayeCreditList": null,
        "dureeDePret": 26,
        "nombreEcheancesComing": 0,
        "dateDernierDeblocage": null,
        "installmentArrears": null,
        "apportPersonnel": null,
        "dateFirstEcheanceFormatted": "01-01-2009",
        "lastDisbursementDate": null,
        "montantAvance": null,
        "numeroDossier": "E606766W381",
        "dateProchaineEcheance": "",
        "startDate": null,
        "coutFraisEtNotaire": null,
        "capitalRestant": 0,
        "fraisDeDossierDeCredit": null,
        "dateLastEcheanceeFormatted": "01-02-2011",
        "identifiant": null,
        "statut": null,
        "apportConjoint": null,
        "typeEcheance": null,
        "installmentDate": null,
        "typeAssurance": null,
        "montantDuPret": null,
        "intitule": null,
        "errorMessage": null,
        "idCreditClient": 0,
        "mensualiteFomated": null,
        "numeroCompte": "0146067666010123",
        "capitalEmprunteFormatted": null,
        "echeanceHorsAssurance": null
      }
    ],
    "success": true
  }
};
