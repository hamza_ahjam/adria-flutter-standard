const dynamic getFormFactureMock = {
  "Map": {
    "champs": [
      {
        "libelle": "Référence de l'avis",
        "nomChamps": "codeBarre",
        "options": null,
        "formatChamps": "2",
        "tailleMin": 12,
        "tailleMax": 12,
        "contrainte": "1",
        "typeChamps": "text",
        "listVals": []
      },
      {
        "libelle": "Montant avant la virgule",
        "nomChamps": "montantsv",
        "options": null,
        "formatChamps": "2",
        "tailleMin": 1,
        "tailleMax": 10,
        "contrainte": "1",
        "typeChamps": "text",
        "listVals": []
      }
    ],
    "libelle": null,
    "creance": {
      "nom": "TGR Taxes, Douane, Amendes",
      "code": "01",
      "categorie": null,
      "creancierId": "1006",
      "logoPath": null,
      "libelleCreancier": null,
      "id": null
    },
    "type": "F"
  }
};
