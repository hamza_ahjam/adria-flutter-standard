const dynamic listCreancierMock = {
  "Map": {
    "success": true,
    "list": [
      {
        "nom": "DGI",
        "code": "1022",
        "description": "(Paiement de la vignette automobile, ...)",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_dgi.png",
        "siteUrl": "http://www.tax.gov.ma",
        "categorie": "1",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Orange Factures",
        "code": "1004",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_orange.bmp",
        "siteUrl": "",
        "categorie": "2",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Orange Recharges",
        "code": "1003",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_orange.bmp",
        "siteUrl": "",
        "categorie": "2",
        "typeCreancier": "R",
        "creances": []
      },
      {
        "nom": "Air Arabia",
        "code": "1023",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_airarabia.png",
        "siteUrl": "",
        "categorie": "6",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Inwi Factures",
        "code": "1018",
        "description": "",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_inwi.png",
        "siteUrl": "",
        "categorie": "2",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Inwi Recharge",
        "code": "1013",
        "description": "",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_inwi.png",
        "siteUrl": "",
        "categorie": "2",
        "typeCreancier": "R",
        "creances": []
      },
      {
        "nom": "RADEEMA",
        "code": "1010",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_RADEEMA.jpeg",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "LYDEC",
        "code": "1005",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_lydec.gif",
        "siteUrl": "LYDEC",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Amendis Tetouan",
        "code": "1009",
        "description": "Amendis Tetouan",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_amendis.png",
        "siteUrl": "Amendis Tetouan",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RAMSA",
        "code": "1015",
        "description": "RAMSA",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_ramsa.png",
        "siteUrl": "RAMSA",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "IAM",
        "code": "1002",
        "description": "logo_IAM.png",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_IAM.png",
        "siteUrl": "IAM",
        "categorie": "2",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "TGR",
        "code": "1006",
        "description":
            "(Paiement de la taxe d'habitation, taxe des services communaux, taxe professionnelle, ...)",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_tgr.gif",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RAK",
        "code": "1016",
        "description": "",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_rak.jpg",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "AL AKHAWAYN UNIVERSITY",
        "code": "1026",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/alakhawayn.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "IAM Recharge",
        "code": "1001",
        "description": "Recharge IAM",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_IAM.png",
        "siteUrl": "www.iam.ma",
        "categorie": "2",
        "typeCreancier": "R",
        "creances": []
      },
      {
        "nom": "DGI",
        "code": "1025",
        "description":
            "(Paiement de la TVA, IR, IS, Droit d’enregistrement...)",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_dgi.png",
        "siteUrl": "http://www.tax.gov.ma",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEM",
        "code": "1019",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_radem2.png ",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ANCFCC",
        "code": "1030",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_ancfcc.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RAM",
        "code": "1028",
        "description": "Royal Air Maroc",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_ram.png",
        "siteUrl": "",
        "categorie": "6",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "JUMIA",
        "code": "1070",
        "description": "",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_jumia.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ONEE-BE",
        "code": "1039",
        "description": "ONEE-BE",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_onee.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEETA",
        "code": "1043",
        "description": "RADEETA",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_radeeta.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEEO",
        "code": "1044",
        "description": "RADEEO",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_radeeo.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEEL",
        "code": "1040",
        "description": "RADEEL",
        "logoPath": "https://www.creditagricole-online.ma/images/RADEEL.jpeg",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "REDAL",
        "code": "1007",
        "description": "Paiement REDAL",
        "logoPath": "https://www.creditagricole-online.ma/images/REDAL.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Autoroutes du Maroc",
        "code": "1024",
        "description": "Recharge JAWAZ",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_ADM.png",
        "siteUrl": "http://www.adm.co.ma",
        "categorie": "7",
        "typeCreancier": "R",
        "creances": []
      },
      {
        "nom": "RADEEF",
        "code": "1014",
        "description": "RADEEF",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_RADEEF.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEES",
        "code": "1045",
        "description":
            "Régie autonome intercommunale de distribution d'eau & d'électricité de la province de Safi",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_RADEES.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Lycee DESCARTES",
        "code": "1033",
        "description": "Lycée DESCARTES",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_descartes.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ANP",
        "code": "1052",
        "description": "Agence Nationale de Ports",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_ANP.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "CTM",
        "code": "1048",
        "description": "CTM",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_CTM.png",
        "siteUrl": "",
        "categorie": "6",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "E-Timbre",
        "code": "1056",
        "description": "E-Timbre",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_dgi.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "TMPA",
        "code": "1055",
        "description": "Tanger Med Port Authority",
        "logoPath": "https://www.creditagricole-online.ma/images/TMPA.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ANCFCC DC",
        "code": "1059",
        "description": "Droits de Conservation",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_ancfcc.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "WIN By INWI",
        "code": "1057",
        "description": "WIN By INWI",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_win.png",
        "siteUrl": "",
        "categorie": "2",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Amendis Tanger",
        "code": "1008",
        "description": "Amendis Tanger",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_amendis.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEEJ",
        "code": "1060",
        "description": "RADEEJ",
        "logoPath": "https://www.creditagricole-online.ma/images/radeej.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "MAGHREBBAIL",
        "code": "1071",
        "description": "MAGHREBBAIL",
        "logoPath":
            "https://www.creditagricole-online.ma/images/MAGHREBBAIL.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "LONDON ACADEMY",
        "code": "1035",
        "description": "LONDON ACADEMY",
        "logoPath":
            "https://www.creditagricole-online.ma/images/london-academy.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "OMPIC",
        "code": "1050",
        "description": "OMPIC",
        "logoPath": "https://www.creditagricole-online.ma/images/OMPIC.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEEC",
        "code": "1020",
        "description": "RADEEC",
        "logoPath": "https://www.creditagricole-online.ma/images/RADEEC.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "American School Of Tangier",
        "code": "1078",
        "description": "American School Of Tangier",
        "logoPath": "https://www.creditagricole-online.ma/images/ASOT.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "TOTAL",
        "code": "1067",
        "description": "TOTAL MAROC",
        "logoPath": "https://www.creditagricole-online.ma/images/total.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Ministère de la justice",
        "code": "1074",
        "description": "Ministère de la justice",
        "logoPath": "https://www.creditagricole-online.ma/images/justice.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Lycée Lyautey",
        "code": "1012",
        "description": "Lycée Lyautey",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_lyautey.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Markoub",
        "code": "1084",
        "description": "Markoub",
        "logoPath": "https://www.creditagricole-online.ma/images/markoub.png",
        "siteUrl": "markoub.ma",
        "categorie": "6",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "CNSS - Employeur Maison",
        "code": "1036",
        "description": "CNSS - Employeur Maison",
        "logoPath": "https://www.creditagricole-online.ma/images/CNSS.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "CNSS - Assuré Volontaire",
        "code": "1037",
        "description": "CNSS - Assuré Volontaire",
        "logoPath": "https://www.creditagricole-online.ma/images/CNSS.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "CNSS - Travailleur Non Salarié (TNS)",
        "code": "1038",
        "description": "CNSS - Travailleur Non Salarié (TNS)",
        "logoPath": "https://www.creditagricole-online.ma/images/CNSS.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Groupe Al Yassamine",
        "code": "1027",
        "description": "Groupe Al Yassamine",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Groupe_Al_Yassamine.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Al Jabr",
        "code": "1053",
        "description": "Al Jabr",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Al_Jabr.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Contrôle des Imports",
        "code": "1082",
        "description": "Contrôle des Imports",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Controle_des_Imports.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ONSSA",
        "code": "1091",
        "description": "ONSSA",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_ONSSA.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Université Euro Med",
        "code": "1128",
        "description": "Université Euro Med",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Universite_Euro_Med.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "FONDATION HASSAN II",
        "code": "1065",
        "description": "FONDATION HASSAN II",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_FONDATION_HASSAN_II.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "PORTNET",
        "code": "1089",
        "description": "PORTNET",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_PORTNET.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "SICAREME",
        "code": "1032",
        "description": "SICAREME",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_SICAREME.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "RADEET",
        "code": "1041",
        "description": "RADEET",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_RADEET.png",
        "siteUrl": "",
        "categorie": "3",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Vivo Energy",
        "code": "1088",
        "description": "Vivo Energy",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Vivo_Energy.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "TMZ - TANGER MED ZONES ET FILIALES",
        "code": "1093",
        "description": "TMZ - TANGER MED ZONES ET FILIALES",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_TMZ.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": " Al Madina",
        "code": "1127",
        "description": " Al Madina",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Al_Madina.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": " Marsa Maroc",
        "code": "1062",
        "description": " Marsa Maroc",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Marsa_Maroc.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": " Atlas Voyage",
        "code": "1054",
        "description": "Atlas Voyage",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Atlas_Voyage.png",
        "siteUrl": "",
        "categorie": "6",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": " PORTNET Laboratoires",
        "code": "1090",
        "description": "PORTNET Laboratoires",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_PORTNET.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "PORTNET – Agent Maritime",
        "code": "1095",
        "description": "PORTNET – Agent Maritime",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_PORTNET_Agent_Maritime.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Université Internationale de Rabat",
        "code": "1101",
        "description": "Université Internationale de Rabat",
        "logoPath": "https://www.creditagricole-online.ma/images/UIR.png",
        "siteUrl": "",
        "categorie": "5",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "ExpressRelais",
        "code": "1066",
        "description": "ExpressRelais",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_CEOS_TECHNOLOGY.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Winxo",
        "code": "1077",
        "description": "Winxo",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Winxo.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Bayt Mal Alqods",
        "code": "1123",
        "description": "Bayt Mal Alqods",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_BaytMalAlqodsy.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Travel Consulting Group",
        "code": "1119",
        "description": "Travel Consulting Group",
        "logoPath": "https://www.creditagricole-online.ma/images/lfen.png",
        "siteUrl": "",
        "categorie": "7",
        "typeCreancier": "F",
        "creances": []
      },
      {
        "nom": "Lycée Paul Valery",
        "code": "1072",
        "description": "Lycée Paul Valery",
        "logoPath":
            "https://www.creditagricole-online.ma/images/logo_Lycée_Paul_Valery.png",
        "siteUrl": "",
        "categorie": "4",
        "typeCreancier": "F",
        "creances": []
      }
    ]
  }
};
