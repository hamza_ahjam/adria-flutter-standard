const dynamic listFavorisFactureMock = {
  "Map": {
    "success": true,
    "list": [
      {
        "id": 177452766,
        "creancierId": "1002",
        "creanceId": "01",
        "saisiForm": "ND=0512345678-Fidelio=1234",
        "libelleFavoris": "TEST_IAM",
        "libelleCreancier": "IAM",
        "libelleCreance": "Produit Fixe Sim",
        "logoPath": "https://www.creditagricole-online.ma/images/logo_IAM.png",
        "nbrFacture": 0,
        "type": "F",
        "dynamicFormValues": ["ND=0512345678", "Fidelio=1234"]
      }
    ]
  }
};
