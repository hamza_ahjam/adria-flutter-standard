const dynamic beneficiaireMock = {
  "Map": {
    "beneficiaireList": [
      {
        "id": 177450906,
        "identifiant": null,
        "codeProduit": null,
        "nom": "TEST",
        "prenom": "ALI",
        "identifiantInterneCompte": null,
        "numeroCompte": null,
        "email": "test@test.com",
        "compteConvertible": "86",
        "deviseCompte": null,
        "deviseCompteLibelle": "MAD",
        "type": "TYPE_ALL",
        "nature": "Mise à disposition",
        "statutCode": "abandonne",
        "tpiLibelle": null,
        "codeSecretPlain": null,
        "intitule": "TEST ALI",
        "codeBanque": null,
        "codeAgence": null,
        "clientInterne": null,
        "cle": null,
        "version": null,
        "notificationSMS": null,
        "notificationEmail": null,
        "gsm": "0652147935",
        "statut": "abandonne",
        "codeBic": null,
        "codeSwift": null,
        "iban": null,
        "bicIban": null,
        "numeroCarte": null,
        "typePieceIdentite": "CI",
        "typePieceIdentiteLibelle": "Carte Nationale d'Identité",
        "numeroPieceIdentite": "JH23908",
        "mail": null,
        "numeroGsm": null,
        "address": null,
        "adresse1": null,
        "adresse2": null,
        "adresse3": null,
        "canalWeb": null,
        "canalMobile": null,
        "genre": null,
        "genreLibelle": "Public",
        "compte": null,
        "type_": "-|-",
        "toString_": "TEST ALI, JH23908",
        "convertible_libelle": "Non",
        "statutLibelle": null,
        "natureCompte": null,
        "typeCompte": null,
        "deviseFormatted": "",
        "carteRechargeable": null,
        "codeUtilisateurAyantCree": null
      },
      {
        "id": 177310268,
        "identifiant": null,
        "codeProduit": null,
        "nom": "mouad",
        "prenom": "mouad",
        "identifiantInterneCompte": null,
        "numeroCompte": null,
        "email": null,
        "compteConvertible": "86",
        "deviseCompte": null,
        "deviseCompteLibelle": "MAD",
        "type": "AUTRES_TypeBeneficiaire",
        "nature": "Mise à disposition",
        "statutCode": "Enregistre",
        "tpiLibelle": null,
        "codeSecretPlain": null,
        "intitule": "mouad mouad",
        "codeBanque": null,
        "codeAgence": null,
        "clientInterne": null,
        "cle": null,
        "version": null,
        "notificationSMS": null,
        "notificationEmail": null,
        "gsm": "0678659740",
        "statut": "Enregistre",
        "codeBic": null,
        "codeSwift": null,
        "iban": null,
        "bicIban": null,
        "numeroCarte": null,
        "typePieceIdentite": "CI",
        "typePieceIdentiteLibelle": "Carte Nationale d'Identité",
        "numeroPieceIdentite": "d71752",
        "mail": null,
        "numeroGsm": null,
        "address": null,
        "adresse1": null,
        "adresse2": null,
        "adresse3": null,
        "canalWeb": null,
        "canalMobile": null,
        "genre": null,
        "genreLibelle": "Public",
        "compte": null,
        "type_": "Autres",
        "toString_": "mouad mouad, d71752",
        "convertible_libelle": "Non",
        "statutLibelle": null,
        "natureCompte": null,
        "typeCompte": null,
        "deviseFormatted": "",
        "carteRechargeable": null,
        "codeUtilisateurAyantCree": null
      },
      {
        "id": 177303689,
        "identifiant": null,
        "codeProduit": null,
        "nom": "RACHIDI",
        "prenom": "ILHAM",
        "identifiantInterneCompte": null,
        "numeroCompte": null,
        "email": null,
        "compteConvertible": "86",
        "deviseCompte": null,
        "deviseCompteLibelle": "MAD",
        "type": "TYPE_ALL",
        "nature": "Mise à disposition",
        "statutCode": "Signe",
        "tpiLibelle": null,
        "codeSecretPlain": null,
        "intitule": "RACHIDI ILHAM",
        "codeBanque": null,
        "codeAgence": null,
        "clientInterne": null,
        "cle": null,
        "version": null,
        "notificationSMS": null,
        "notificationEmail": null,
        "gsm": "0666064678",
        "statut": "Signe",
        "codeBic": null,
        "codeSwift": null,
        "iban": null,
        "bicIban": null,
        "numeroCarte": null,
        "typePieceIdentite": "CI",
        "typePieceIdentiteLibelle": "Carte Nationale d'Identité",
        "numeroPieceIdentite": "AA1234",
        "mail": null,
        "numeroGsm": null,
        "address": null,
        "adresse1": null,
        "adresse2": null,
        "adresse3": null,
        "canalWeb": null,
        "canalMobile": null,
        "genre": null,
        "genreLibelle": "Public",
        "compte": null,
        "type_": "-|-",
        "toString_": "RACHIDI ILHAM, AA1234",
        "convertible_libelle": "Non",
        "statutLibelle": null,
        "natureCompte": null,
        "typeCompte": null,
        "deviseFormatted": "",
        "carteRechargeable": null,
        "codeUtilisateurAyantCree": null
      },
      {
        "id": 177183830,
        "identifiant": null,
        "codeProduit": null,
        "nom": "MOUAD",
        "prenom": "MOUAD",
        "identifiantInterneCompte": null,
        "numeroCompte": null,
        "email": null,
        "compteConvertible": "86",
        "deviseCompte": null,
        "deviseCompteLibelle": "MAD",
        "type": "TYPE_ALL",
        "nature": "Mise à disposition",
        "statutCode": "abandonne",
        "tpiLibelle": null,
        "codeSecretPlain": null,
        "intitule": "MOUAD MOUAD",
        "codeBanque": null,
        "codeAgence": null,
        "clientInterne": null,
        "cle": null,
        "version": null,
        "notificationSMS": null,
        "notificationEmail": null,
        "gsm": "0789877888",
        "statut": "abandonne",
        "codeBic": null,
        "codeSwift": null,
        "iban": null,
        "bicIban": null,
        "numeroCarte": null,
        "typePieceIdentite": "MAR",
        "typePieceIdentiteLibelle": null,
        "numeroPieceIdentite": "D35533",
        "mail": null,
        "numeroGsm": null,
        "address": null,
        "adresse1": null,
        "adresse2": null,
        "adresse3": null,
        "canalWeb": null,
        "canalMobile": null,
        "genre": null,
        "genreLibelle": "Public",
        "compte": null,
        "type_": "-|-",
        "toString_": "MOUAD MOUAD, D35533",
        "convertible_libelle": "Non",
        "statutLibelle": null,
        "natureCompte": null,
        "typeCompte": null,
        "deviseFormatted": "",
        "carteRechargeable": null,
        "codeUtilisateurAyantCree": null
      },
      {
        "id": 177163585,
        "identifiant": null,
        "codeProduit": null,
        "nom": "ASMA",
        "prenom": "ASMA",
        "identifiantInterneCompte": null,
        "numeroCompte": null,
        "email": null,
        "compteConvertible": "86",
        "deviseCompte": null,
        "deviseCompteLibelle": "MAD",
        "type": "TYPE_ALL",
        "nature": "Mise à disposition",
        "statutCode": "Signe",
        "tpiLibelle": null,
        "codeSecretPlain": null,
        "intitule": "ASMA ASMA",
        "codeBanque": null,
        "codeAgence": null,
        "clientInterne": null,
        "cle": null,
        "version": null,
        "notificationSMS": null,
        "notificationEmail": null,
        "gsm": "0666075614",
        "statut": "Signe",
        "codeBic": null,
        "codeSwift": null,
        "iban": null,
        "bicIban": null,
        "numeroCarte": null,
        "typePieceIdentite": "MAR",
        "typePieceIdentiteLibelle": null,
        "numeroPieceIdentite": "12334",
        "mail": null,
        "numeroGsm": null,
        "address": null,
        "adresse1": null,
        "adresse2": null,
        "adresse3": null,
        "canalWeb": null,
        "canalMobile": null,
        "genre": null,
        "genreLibelle": "Public",
        "compte": null,
        "type_": "-|-",
        "toString_": "ASMA ASMA, 12334",
        "convertible_libelle": "Non",
        "statutLibelle": null,
        "natureCompte": null,
        "typeCompte": null,
        "deviseFormatted": "",
        "carteRechargeable": null,
        "codeUtilisateurAyantCree": null
      }
    ],
    "success": true
  }
};

const dynamic deleteBenefMock = {
  "Map": {
    "success": false,
    "message":
        "Impossible de supprimer le bénéficiaire. Ce dernier fait partie des transactions qui sont en cours de traitement",
    "codeErreur": "messages.RG0109"
  }
};
