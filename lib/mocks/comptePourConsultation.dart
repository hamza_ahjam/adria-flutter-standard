const dynamic compteConsultationMock = {
  "Map": {
    "compteInstanceTotal": 4,
    "compteInstanceList": [
      {
        "toString_": "0258131806010113 LEGRARA MBAREK, -3,06 MAD",
        "soldeIndisponible_": 0,
        "convertible_libelle": "86",
        "soldeDisponible_": null,
        "soldeIndisponible": "0,00",
        "soldeComptable_": -3.06,
        "elligibiliteLCN": false,
        "elligibiliteAjoutBeneficiaire": false,
        "natureCompte": null,
        "solde_devise_reel": -3.06,
        "autorisation": 0,
        "agence": "100",
        "soldeTempsReel_": -3.06,
        "intituleClient": "LEGRARA MBAREK",
        "elligibiliteDemande": false,
        "soldeTempsReel": "-3,06",
        "cleControle": null,
        "elligibliteVirementCr": false,
        "id": 177159031,
        "soldeComptableDevise": "-3,06",
        "cleRIB": null,
        "identifiantInterne": "0258131806010113",
        "agence_libelle": "Rabat Ennakhil CA ",
        "devise_code": "504",
        "solde_devise_format": "-3,06",
        "radical": "256303",
        "elligibilitePrelivement": false,
        "convertible": null,
        "overdraftFlag": null,
        "elligibliteVirementDb": false,
        "soldeComptableDevise_": -3.06,
        "solde_devise_valeur": -3.06,
        "solde_devise": -3.06,
        "devise": "MAD",
        "intitule": "LEGRARA MBAREK",
        "REAL_TIME_SUCCESS": true,
        "devise_pivot": "MAD",
        "SOLDE_VALEUR_DEVISE_PIVOT": "-3,06",
        "identifiantInterne_NOT_FORMATTED": "0258131806010113",
        "soldeComptable": "-3,06",
        "soldeValeur": "-3,06",
        "soldeDisponible": "-",
        "SOLDE_REEL_DEVISE_PIVOT": "-3,06",
        "autorisation_": "0,00",
        "elligibilitePF": false
      },
      {
        "toString_": "0141007989050525 LEGRARA MBAREK, 8 757,19 MAD",
        "soldeIndisponible_": 0,
        "convertible_libelle": "86",
        "soldeDisponible_": null,
        "soldeIndisponible": "0,00",
        "soldeComptable_": 8757.19,
        "elligibiliteLCN": false,
        "elligibiliteAjoutBeneficiaire": false,
        "natureCompte": null,
        "solde_devise_reel": 8757.19,
        "autorisation": 0,
        "agence": "100",
        "soldeTempsReel_": 8757.19,
        "intituleClient": "LEGRARA MBAREK",
        "elligibiliteDemande": false,
        "soldeTempsReel": "8 757,19",
        "cleControle": null,
        "elligibliteVirementCr": false,
        "id": 177159030,
        "soldeComptableDevise": "8 757,19",
        "cleRIB": null,
        "identifiantInterne": "0141007989050525",
        "agence_libelle": "Rabat Ennakhil CA ",
        "devise_code": "504",
        "solde_devise_format": "8 757,19",
        "radical": "256303",
        "elligibilitePrelivement": false,
        "convertible": null,
        "overdraftFlag": null,
        "elligibliteVirementDb": false,
        "soldeComptableDevise_": 8757.19,
        "solde_devise_valeur": 8757.19,
        "solde_devise": 8757.19,
        "devise": "MAD",
        "intitule": "LEGRARA MBAREK",
        "REAL_TIME_SUCCESS": true,
        "devise_pivot": "MAD",
        "SOLDE_VALEUR_DEVISE_PIVOT": "8 757,19",
        "identifiantInterne_NOT_FORMATTED": "0141007989050525",
        "soldeComptable": "8 757,19",
        "soldeValeur": "8 757,19",
        "soldeDisponible": "-",
        "SOLDE_REEL_DEVISE_PIVOT": "8 757,19",
        "autorisation_": "0,00",
        "elligibilitePF": false
      },
      {
        "toString_": "0141007986610125 LEGRARA MBAREK, 649 984,11 MAD",
        "soldeIndisponible_": 0,
        "convertible_libelle": "86",
        "soldeDisponible_": null,
        "soldeIndisponible": "0,00",
        "soldeComptable_": 650084.11,
        "elligibiliteLCN": false,
        "elligibiliteAjoutBeneficiaire": false,
        "natureCompte": null,
        "solde_devise_reel": 649984.11,
        "autorisation": 0,
        "agence": "100",
        "soldeTempsReel_": 649984.11,
        "intituleClient": "LEGRARA MBAREK",
        "elligibiliteDemande": false,
        "soldeTempsReel": "649 984,11",
        "cleControle": null,
        "elligibliteVirementCr": false,
        "id": 177159029,
        "soldeComptableDevise": "650 084,11",
        "cleRIB": null,
        "identifiantInterne": "0141007986610125",
        "agence_libelle": "Rabat Ennakhil CA ",
        "devise_code": "504",
        "solde_devise_format": "650 084,11",
        "radical": "256303",
        "elligibilitePrelivement": false,
        "convertible": null,
        "overdraftFlag": null,
        "elligibliteVirementDb": false,
        "soldeComptableDevise_": 650084.11,
        "solde_devise_valeur": 649984.11,
        "solde_devise": 650084.11,
        "devise": "MAD",
        "intitule": "LEGRARA MBAREK",
        "REAL_TIME_SUCCESS": true,
        "devise_pivot": "MAD",
        "SOLDE_VALEUR_DEVISE_PIVOT": "649 984,11",
        "identifiantInterne_NOT_FORMATTED": "0141007986610125",
        "soldeComptable": "650 084,11",
        "soldeValeur": "650 084,11",
        "soldeDisponible": "-",
        "SOLDE_REEL_DEVISE_PIVOT": "649 984,11",
        "autorisation_": "0,00",
        "elligibilitePF": false
      },
      {
        "toString_": "0141007986010125 LEGRARA MBAREK, 693 617,56 MAD",
        "soldeIndisponible_": 0,
        "convertible_libelle": "86",
        "soldeDisponible_": null,
        "soldeIndisponible": "0,00",
        "soldeComptable_": 1006866.31,
        "elligibiliteLCN": false,
        "elligibiliteAjoutBeneficiaire": false,
        "natureCompte": null,
        "solde_devise_reel": 693617.56,
        "autorisation": 0,
        "agence": "100",
        "soldeTempsReel_": 693617.56,
        "intituleClient": "LEGRARA MBAREK",
        "elligibiliteDemande": false,
        "soldeTempsReel": "693 617,56",
        "cleControle": null,
        "elligibliteVirementCr": false,
        "id": 177159028,
        "soldeComptableDevise": "1 006 866,31",
        "cleRIB": null,
        "identifiantInterne": "0141007986010125",
        "agence_libelle": "Rabat Ennakhil CA ",
        "devise_code": "504",
        "solde_devise_format": "1 006 866,31",
        "radical": "256303",
        "elligibilitePrelivement": false,
        "convertible": null,
        "overdraftFlag": null,
        "elligibliteVirementDb": false,
        "soldeComptableDevise_": 1006866.31,
        "solde_devise_valeur": 693617.56,
        "solde_devise": 1006866.31,
        "devise": "MAD",
        "intitule": "LEGRARA MBAREK",
        "REAL_TIME_SUCCESS": true,
        "devise_pivot": "MAD",
        "SOLDE_VALEUR_DEVISE_PIVOT": "693 617,56",
        "identifiantInterne_NOT_FORMATTED": "0141007986010125",
        "soldeComptable": "1 006 866,31",
        "soldeValeur": "1 006 866,31",
        "soldeDisponible": "-",
        "SOLDE_REEL_DEVISE_PIVOT": "693 617,56",
        "autorisation_": "0,00",
        "elligibilitePF": false
      }
    ]
  }
};
