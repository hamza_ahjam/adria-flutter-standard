const dynamic listTitreCompteMock = {
  "Map": {
    "success": true,
    "portefeuilleTitreInstanceList": [
      {
        "valeurBoursiere_formatted": "5.000,00",
        "totalCours": "0,00",
        "dateCours": "01-03-2020",
        "totalPMV": "0,00",
        "valeurBoursiere": 5000,
        "dernierCours": 1000,
        "prixMoyenAcquisition_formatted": "100,00",
        "pmv_formatted": "197.000,00",
        "totalCount": "0",
        "devise": "MAD",
        "valeurderniercours_formatted": "202.000,00",
        "montantBrute": 5000,
        "quantite_formatted": "202",
        "valeurderniercours": 202000,
        "nomValeurDetenue": "snep",
        "dernierCours_formatted": "1.000,00",
        "typeValeur": null,
        "pmv": 197000,
        "id": null,
        "totalBrut": "0,00",
        "montantBrute_formatted": "5.000,00",
        "quantite": 202,
        "prixMoyenAcquisition": 100
      }
    ]
  }
};
