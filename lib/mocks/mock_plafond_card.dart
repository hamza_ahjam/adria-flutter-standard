const dynamic pladondCardMock = {
  "Map": {
    "plafonds": {
      "codePlafond": null,
      "plafondPaiementInternationnal": 0,
      "plafondPaiementInternationnalFotmatted": "10 000",
      "plafondPaiements": 0,
      "plafondPaiementsFotmatted": "5000",
      "plafondEcommerceInternationnal": 0,
      "plafondEcommerceInternationnalFotmatted": "0",
      "plafondECommerce": 0,
      "plafondECommerceFotmatted": "10 000",
      "plafondRetraitInternationalQuotidien": 0,
      "plafondRetraitInternationalQuotidienFotmatted": "0",
      "plafondRetrait": 0,
      "plafondRetraitFotmatted": "10 000",
      "plafondRetraitInternationnal": 0,
      "plafondRetraitInternationnalFotmatted": "10 000",
      "plafondRetraitConfrere": 0,
      "plafondRetraitConfrereFotmatted": "10 000",
      "devise": null,
      "deviseLibelle": null
    },
    "success": false,
    "error": "error",
    "message": "CARTE INTROUVALBE"
  }
};
