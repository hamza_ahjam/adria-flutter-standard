const dynamic mockMouvementPdf = {
  "Map": {
    "filename": "Liste des mouvements.pdf",
    "contentType": {
      "type": "application",
      "subtype": "pdf",
      "parameters": {},
      "qualityValue": 1.0,
      "charset": null,
      "wildcardType": false,
      "wildcardSubtype": false,
      "concrete": true
    },
    "content":
        "JVBERi0xLjUKJeLjz9MKMSAwIG9iago8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDI5Pj5zdHJlYW0KeJwr5HIK4TI2UzA3NVMISeFyDeEK5OICADFiBDEKZW5kc3RyZWFtCmVuZG9iagozIDAgb2JqCjw8L1RhYnMvUy9Hcm91cDw8L1MvVHJhbnNwYXJlbmN5L1R5cGUvR3JvdXAvQ1MvRGV2aWNlUkdCPj4vQ29udGVudHMgMSAwIFIvVHlwZS9QYWdlL1Jlc291cmNlczw8L0NvbG9yU3BhY2U8PC9DUy9EZXZpY2VSR0I+Pi9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXT4+L1BhcmVudCAyIDAgUi9NZWRpYUJveFswIDAgNjEyIDc5Ml0+PgplbmRvYmoKMiAwIG9iago8PC9LaWRzWzMgMCBSXS9UeXBlL1BhZ2VzL0NvdW50IDEvSVRYVCgyLjEuNyk+PgplbmRvYmoKNCAwIG9iago8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvVmlld2VyUHJlZmVyZW5jZXM8PC9QcmludFNjYWxpbmcvQXBwRGVmYXVsdD4+Pj4KZW5kb2JqCjUgMCBvYmoKPDwvTW9kRGF0ZShEOjIwMjIwMTEyMTIwMjQ5WikvQ3JlYXRvcihKYXNwZXJSZXBvcnRzIExpYnJhcnkgdmVyc2lvbiA2LjEwLjAtZGRkZDM4MjE4YTNjNDA0ZDAxZWVjZGI5ZDlhNzYzNmZlMmQwMmQ3YSkvQ3JlYXRpb25EYXRlKEQ6MjAyMjAxMTIxMjAyNDlaKS9Qcm9kdWNlcihpVGV4dCAyLjEuNyBieSAxVDNYVCk+PgplbmRvYmoKeHJlZgowIDYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMDAzMzMgMDAwMDAgbiAKMDAwMDAwMDExMCAwMDAwMCBuIAowMDAwMDAwMzk2IDAwMDAwIG4gCjAwMDAwMDA0ODcgMDAwMDAgbiAKdHJhaWxlcgo8PC9JbmZvIDUgMCBSL0lEIFs8MzBjNmJkOTczZjVmOGU2YmE5Mzg4NmRmNjZlODhkNzU+PGI3ODc5YTk1OTY3MzU2ZDRkN2YwZTI1ZmQzZmIzMTdkPl0vUm9vdCA0IDAgUi9TaXplIDY+PgpzdGFydHhyZWYKNjg0CiUlRU9GCg=="
  }
};
