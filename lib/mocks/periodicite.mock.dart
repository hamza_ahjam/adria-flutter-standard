const dynamic periodiciteMock = {
  "Map": {
    "periodiciteList": [
      {
        "id": 176669454,
        "code": "A",
        "libelle": "Annuelle",
        "classe": "Periodicite",
        "codeLangue": "fr",
        "codeBanqueAssocie": "00225",
        "codeIso": "A"
      },
      {
        "id": 176669017,
        "code": "M",
        "libelle": "Mensuelle",
        "classe": "Periodicite",
        "codeLangue": "fr",
        "codeBanqueAssocie": "00225",
        "codeIso": "M"
      },
      {
        "id": 176669508,
        "code": "S",
        "libelle": "Seméstrielle",
        "classe": "Periodicite",
        "codeLangue": "fr",
        "codeBanqueAssocie": "00225",
        "codeIso": "S"
      },
      {
        "id": 176668994,
        "code": "T",
        "libelle": "Triméstrielle",
        "classe": "Periodicite",
        "codeLangue": "fr",
        "codeBanqueAssocie": "00225",
        "codeIso": "T"
      }
    ]
  }
};
