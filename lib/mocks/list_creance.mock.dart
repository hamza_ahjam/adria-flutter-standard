const dynamic listCreanceMock = {
  "Map": {
    "listCreances": [
      {
        "nom": "Paiement particulier par CIN ou autres SIM",
        "code": "05",
        "categorie": null,
        "creancierId": "1004",
        "logoPath": null,
        "libelleCreancier": null,
        "id": null
      },
      {
        "nom": "Orange Particuliers Sim",
        "code": "01",
        "categorie": null,
        "creancierId": "1004",
        "logoPath": null,
        "libelleCreancier": null,
        "id": null
      },
      {
        "nom": "Orange Entreprises Sim",
        "code": "02",
        "categorie": null,
        "creancierId": "1004",
        "logoPath": null,
        "libelleCreancier": null,
        "id": null
      }
    ],
    "success": true
  }
};
