import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class AssistancePage extends StatelessWidget {
  const AssistancePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OnlineAppBarWidget(
        appBarTitle: 'Assistance',
        context: context,
      ),
      extendBodyBehindAppBar: true,
      body: ListView(
        children: <Widget>[
          Container(
            // padding: EdgeInsets.only(top: 20),
            height: MediaQuery.of(context).size.height * 0.2,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/$banque_id/images/login_bkg.png"),
                  fit: BoxFit.cover),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(22.0),
              ),
            ),
            child: Center(
                child: Text(
              AppLocalizations.of(context).num_urg,
              style: GoogleFonts.roboto(
                textStyle: TextStyle(
                    fontSize: 25,
                    color: GlobalParams.themes["$banque_id"].intituleCmpColor,
                    fontWeight: FontWeight.bold),
              ),
            )),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.03,
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.50,
            width: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ...(agnecInfos(context) as List).map((nav) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(2, 8, 2, 8),
                          child: GestureDetector(
                            onTap: () {
                              // print(nav['numero']);
                              launch("tel://${nav['numero']}");
                            },
                            child: ListTile(
                                leading: Container(
                                  height: double.infinity,
                                  width: 8,
                                  color: GlobalParams
                                      .themes["$banque_id"]
                                      .intituleCmpColor,
                                ),
                                title: Text(nav['titre']),
                                subtitle: Text(nav['numero']),
                                trailing: nav['icon']
                                    ? Container(
                                        width: 80,
                                        height: 80,
                                        decoration: BoxDecoration(
                                            color:
                                            GlobalParams
                                                .themes["$banque_id"]
                                                .intituleCmpColor,
                                            border: Border.all(
                                              color:
                                              GlobalParams
                                                  .themes["$banque_id"]
                                                  .intituleCmpColor,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100))),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 5),
                                              child: Text(
                                                "7j/7",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Text("24h/24",
                                                style: TextStyle(
                                                    color: Colors.white))
                                          ],
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.all(0),
                                      )),
                          ),
                        )
                      ],
                    );
                  })
                ],
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.07,
          ),
        ],
      ),
    );
  }
}
