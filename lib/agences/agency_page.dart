import 'dart:io';

import 'package:LBA/agences/agencies_service.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/app_bar.dart';
import 'package:LBA/widgets/header.dart';
import 'package:LBA/widgets/menu/offlineMenu.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class AgencyPage extends StatefulWidget {
  // var position;
  AgencyPage() {
    // position = getUserLocation();
  }

  @override
  _AgencyPageState createState() => _AgencyPageState();
}

class _AgencyPageState extends State<AgencyPage> {
  String dropdownValue;
  BannerAdsService get service => GetIt.I<BannerAdsService>();
  var lat = 33.5236557, long = -7.6727061, agencies;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getUserLocation();
      await getAgencies();
    });
    String marquerImage = Platform.isAndroid
        ? 'assets/$banque_id/images/marker_nsia_b.png'
        : 'assets/$banque_id/images/markerS.png';

    //String marquerImage = 'assets/$banque_id/images/markerS.png';

    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(48, 48)), marquerImage)
        .then((onValue) {
      myIcon = onValue;
    });
  }

  getUserLocation() async {
    print("get location");
    // var position = await Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
    // await Geolocator.requestPermission();
    var lastPosition = await Geolocator.getLastKnownPosition();
    print("------------ $lastPosition");

    setState(() {
      if (lastPosition != null) {
        lat = lastPosition.latitude;
        long = lastPosition.longitude;
      }
    });
    // print(" $lat --- $long");
    return lastPosition;
  }

  getAgencies() async {
    print("get agenices list");
    try {
      var res = await service.getAgencies();
      setState(() {
        agencies = res;
      });
    } catch (e) {
      print("agencies page getAgencies err *** $e");
    }
  }

  BitmapDescriptor myIcon;
  GoogleMapController _controller;
  Location _location = Location();

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;

    _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, long), zoom: 13),
        ),
      );
    });
  }

  Set<Marker> allMarkers = {};
  int id = 1;
  @override
  Widget build(BuildContext context) {
    if (agencies != null) {
      agencies["Map"]["agenceList"].map<Marker>((m) {
        allMarkers.add(Marker(
            markerId: MarkerId("${m["libelle"]}"),
            position: LatLng(
                m["latitude"].toString().trim() == ""
                    ? 0.0
                    : double.parse(m["latitude"].toString().trim()),
                m["longitude"].toString().trim() == ""
                    ? 0.0
                    : double.parse(m["longitude"].toString().trim())),
            infoWindow: InfoWindow(
              title: "${m["libelle"]}",
              snippet: "${m["adresse1"]}",
            ),
            icon: myIcon));
      }).toList();
    } else {
      allMarkers = {};
    }
    // Completer<GoogleMapController> _controller = Completer();

    return WillPopScope(
      onWillPop: (){
        moveToLogin(context);
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: OffLineMenu(1),
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: false,
        appBar: AppBarWidget(context: context,),
        body: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/$banque_id/images/background_image.png"),
                    fit: BoxFit.cover, alignment: Alignment(0, 0.9)),
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(22.0),
                ),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Header(isFromAgences: true),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.025,
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.03,
                        child: Text(
                          AppLocalizations.of(context).trouver_une_agence,
                          textAlign: TextAlign.center,
                          maxLines: 1,
                          style: GoogleFonts.roboto(
                              color: GlobalParams.themes["$banque_id"].infoColor,
                              fontSize: 17,
                              fontWeight: FontWeight.bold)),
                  ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.03,
                        child: Text(
                          AppLocalizations.of(context).chez_vous,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: GlobalParams.themes["$banque_id"].infoColor,
                              fontSize: 17,
                              fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  )
                ]),
              ),
            ),
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(25)),
                child: GoogleMap(
                  initialCameraPosition:
                      CameraPosition(target: LatLng(lat, long), zoom: 13),
                  onMapCreated: _onMapCreated,
                  markers: Set.from(allMarkers),
                  myLocationButtonEnabled: true,
                  myLocationEnabled: true,
                  scrollGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  trafficEnabled: false,
                  compassEnabled: true,
                  rotateGesturesEnabled: true,
                  zoomControlsEnabled: true,
                  zoomGesturesEnabled: true,
                  gestureRecognizers: Set()
                    ..add(Factory<PanGestureRecognizer>(
                        () => PanGestureRecognizer()))
                    ..add(Factory<ScaleGestureRecognizer>(
                        () => ScaleGestureRecognizer()))
                    ..add(Factory<TapGestureRecognizer>(
                        () => TapGestureRecognizer()))
                    ..add(Factory<VerticalDragGestureRecognizer>(
                        () => VerticalDragGestureRecognizer())),
                  // rotateGesturesEnabled: true,
                  // initialCameraPosition: CameraPosition(
                  //     target: LatLng(lat, long), zoom: 15.0),
                  // mapType: MapType.normal,
                ),
              ),
            ),
            // SizedBox(
            //   height: MediaQuery.of(context).size.height * 0.01,
            // )
          ],
        ),
      ),
    );
  }
}
