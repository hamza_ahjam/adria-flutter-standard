import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_interceptor/extensions/string.extensions.dart';

class BannerAdsService {
  getAds() async {
    const url =
        "http://197.230.233.107:9080/app/administration/adsbanner/listObjectPage";
    var data;
    try {
      var res = await http.post(url.toUri());
      data = json.decode(res.body);
      return data;
    } catch (e) {
      print("service banner ads error : ** $e");
    }
  }

  getAgencies() async {
    const url =
        "http://197.230.233.107:9080/app/administration/agences/list?codeBanque=00225";
    var data;
    try {
      var res = await http.post(url.toUri(), headers: <String, String>{
        'Accept': 'application/json; charset=UTF-8',
      });
      data = json.decode(res.body);
      return data;
    } catch (e) {
      print("service agencies map error : ** $e");
    }
  }
}
