import 'dart:io';

import 'package:LBA/agences/agencies_service.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/widgets/menu/online_app_bar.widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class ListAgenciesPage extends StatefulWidget {
  // Light Theme
  @override
  _ListAgenciesPageState createState() => _ListAgenciesPageState();
}

class _ListAgenciesPageState extends State<ListAgenciesPage> {
  BannerAdsService get service => GetIt.I<BannerAdsService>();
  var lat = 33.5236557, long = -7.6727061, agencies;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await getUserLocation();
      await getAgencies();
    });

    String marquerImage = Platform.isAndroid
        ? 'assets/$banque_id/images/marker.png'
        : 'assets/$banque_id/images/markerS.png';


    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            marquerImage)
        .then((onValue) {
      myIcon = onValue;
    });
  }

  getUserLocation() async {
    print("get location");
    // var position = await Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
    // await Geolocator.requestPermission();
    var lastPosition = await Geolocator.getLastKnownPosition();
    print("------------ $lastPosition");

    setState(() {
      if (lastPosition == null) {
        lat = 335771.0;
        long = -76602.0;
      } else {
        lat = lastPosition.latitude;
        long = lastPosition.longitude;
        // HomePage.kInitialPosition = lastPosition;
      }
    });
    // print(" $lat --- $long");
    return lastPosition;
  }

  getAgencies() async {
    print("get agenices list");
    try {
      var res = await service.getAgencies();
      setState(() {
        agencies = res;
      });
    } catch (e) {
      print("agencies page getAgencies err *** $e");
    }
  }

  // PickResult selectedPlace;

  BitmapDescriptor myIcon;
  GoogleMapController _controller;
  Location _location = Location();

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;

    _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, long), zoom: 13),
        ),
      );
    });
  }

  Set<Marker> allMarkers = {};
  @override
  Widget build(BuildContext context) {
    if (agencies != null) {
      agencies["Map"]["agenceList"].map<Marker>((m) {
        allMarkers.add(Marker(
          markerId: MarkerId("${m["libelle"]}"),
          position: LatLng(
              m["latitude"].toString().trim() == ""
                  ? 0.0
                  : double.parse(m["latitude"].toString().trim()),
              m["longitude"].toString().trim() == ""
                  ? 0.0
                  : double.parse(m["longitude"].toString().trim())),
          infoWindow: InfoWindow(
            title: "${m["libelle"]}",
            snippet: "${m["adresse1"]}",
          ),
          icon: myIcon,
        ));
      }).toList();
    } else {
      allMarkers = {};
    }

    return Scaffold(
        appBar: OnlineAppBarWidget(
          appBarTitle: AppLocalizations.of(context).agences,
          context: context,
        ),
        body: ListView(
          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.9,
              child: GoogleMap(
                initialCameraPosition:
                    CameraPosition(target: LatLng(lat, long), zoom: 13),
                onMapCreated: _onMapCreated,
                markers: Set.from(allMarkers),
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                scrollGesturesEnabled: true,
                tiltGesturesEnabled: true,
                trafficEnabled: false,
                compassEnabled: true,
                rotateGesturesEnabled: true,
                zoomControlsEnabled: true,
                zoomGesturesEnabled: true,
                gestureRecognizers: Set()
                  ..add(Factory<PanGestureRecognizer>(
                      () => PanGestureRecognizer()))
                  ..add(Factory<ScaleGestureRecognizer>(
                      () => ScaleGestureRecognizer()))
                  ..add(Factory<TapGestureRecognizer>(
                      () => TapGestureRecognizer()))
                  ..add(Factory<VerticalDragGestureRecognizer>(
                      () => VerticalDragGestureRecognizer())),
                // rotateGesturesEnabled: true,
                // initialCameraPosition: CameraPosition(
                //     target: LatLng(lat, long), zoom: 15.0),
                // mapType: MapType.normal,
              ),
            ),
            //  PlacePicker(
            //   apiKey: "AIzaSyB6SsR1YTkl6O5nedZfRGjjyx-AgYQKPmU",
            //   initialPosition: LatLng(lat, long),
            //   useCurrentLocation: true,
            //   selectInitialPosition: true,
            //   enableMyLocationButton: true,
            //   forceAndroidLocationManager: true,
            //   // searchForInitialValue: true,
            //   enableMapTypeButton: true,
            //   //usePlaceDetailSearch: true,
            //   onPlacePicked: (result) {
            //     selectedPlace = result;
            //     Navigator.of(context).pop();
            //     setState(() {});
            //   },
            //   forceSearchOnZoomChanged: true,
            //   automaticallyImplyAppBarLeading: false,
            //   //autocompleteLanguage: "ko",
            //   //region: 'au',
            //   //selectInitialPosition: true,
            //   // selectedPlaceWidgetBuilder: (_, selectedPlace, state, isSearchBarFocused) {
            //   //   print("state: $state, isSearchBarFocused: $isSearchBarFocused");
            //   //   return isSearchBarFocused
            //   //       ? Container()
            //   //       : FloatingCard(
            //   //           bottomPosition: 0.0, // MediaQuery.of(context) will cause rebuild. See MediaQuery document for the information.
            //   //           leftPosition: 0.0,
            //   //           rightPosition: 0.0,
            //   //           width: 500,
            //   //           borderRadius: BorderRadius.circular(12.0),
            //   //           child: state == SearchingState.Searching
            //   //               ? Center(child: CircularProgressIndicator())
            //   //               : RaisedButton(
            //   //                   child: Text("Pick Here"),
            //   //                   onPressed: () {
            //   //                     // IMPORTANT: You MUST manage selectedPlace data yourself as using this build will not invoke onPlacePicker as
            //   //                     //            this will override default 'Select here' Button.
            //   //                     print("do something with [selectedPlace] data");
            //   //                     Navigator.of(context).pop();
            //   //                   },
            //   //                 ),
            //   //         );
            //   // },
            //   // pinBuilder: (context, state) {
            //   //   if (state == PinState.Idle) {
            //   //     return Icon(Icons.favorite_border);
            //   //   } else {
            //   //     return Icon(Icons.favorite);
            //   //   }
            //   // },
            // ),
            // ),
            // selectedPlace == null
            //     ? Container()
            //     : Text(selectedPlace.formattedAddress ?? ""),
          ],
        ));
  }

  // showDetail(context, mon, adr, phone) {
  //   return showDialog(
  //       context: context,
  //       builder: (context) {
  //         return AlertDialog(
  //           title: Text(
  //             "$mon",
  //             style: GoogleFonts.roboto(),
  //           ),
  //           content: Container(
  //             height: MediaQuery.of(context).size.height * 0.3,
  //             child: ListView(
  //               children: [
  //                 ListTile(
  //                   leading: Icon(
  //                     Icons.near_me,
  //                     color: principaleColor5,
  //                   ),
  //                   title: Text(
  //                     "$adr",
  //                     style: GoogleFonts.roboto(),
  //                   ),
  //                 ),
  //                 ListTile(
  //                   leading: Icon(
  //                     Icons.phone,
  //                     color: principaleColor5,
  //                   ),
  //                   title: Text(
  //                     "$phone",
  //                     style: GoogleFonts.roboto(),
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
}
