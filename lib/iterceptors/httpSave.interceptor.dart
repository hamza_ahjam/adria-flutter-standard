import 'package:LBA/constants.dart';
import 'package:LBA/main.dart';
import 'package:LBA/models/token.model.dart';
import 'package:alice/alice.dart';
import 'package:http_interceptor/http/interceptor_contract.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';

class HttpSaveInterceptor implements InterceptorContract {
  Token tokenState;
  RequestData currentRequestData;
  HttpSaveInterceptor(this.tokenState);

  Alice _alice = Alice(
    navigatorKey: MyApp.navigatorKey,
    showNotification: true,
    showInspectorOnShake: true,
    darkTheme: false,
    maxCallsCount: 1000,
  );

  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    var user = tokenState.getUserDetails();
    try {
      this.currentRequestData = data;

      data.headers['Accept'] = 'application/json; charset=UTF-8';
      data.headers['Authorization'] = 'Bearer ${tokenState.accessToken}';

      var map = new Map<String, dynamic>();
      map["abonne"] = "${user["abonne"]}";
      map["userAgence"] = "${user["agence"]}";
      map["IntituleClient"] = "${user["intitule"]}";
      map["userAgence"] = "${user["agence"]}";
      map["userCodeBanque"] = codeBanque;
      map["codeBanqueContrat"] = codeBanque;
      map["userCodeLangue"] = langue;
      map["userCodePays"] = codePays;
      map["userContratId"] = "${user["contratId"]}";
      map["defaultLocale"] = "${user["defaultLocale"]}";
      map["dateExpirationProfileMetier"] =
          "${user["dateExpirationProfileMetier"]}";
      map["dateExpirationProfileSegnature"] =
          "${user["dateExpirationProfileMetier"]}";
      map["userDeviseBanque"] = deviseBanque;
      map["userEmail"] = "${user["email"]}";
      map["userFuseauHoraire"] = "${user["fuseauHoraire"]}";
      map["userId"] = "${user["id"]}";
      map["userIdentifiantContrat"] = "${user["identifiantContrat"]}";
      map["userIdentifiantRC"] = "${user["identifiantRC"]}";
      map["userQualite"] = "${user["qualite"]}";
      map["userTypeProfile"] = typeProfil;
      map["userUsername"] = "${user["username"]}";

      data.body = map;
      print("Request save http Interceptor");
    } catch (e) {
      print("error save http *** $e");
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    print("Respanse save http interceptor **");

    _alice.onHttpResponse(data.toHttpResponse());
    return data;
  }
}
