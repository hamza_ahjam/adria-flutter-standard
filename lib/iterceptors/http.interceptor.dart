import 'dart:convert';

import 'package:LBA/constants.dart';
import 'package:LBA/main.dart';
import 'package:LBA/models/token.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:alice/alice.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';

// import 'package:alice/alice.dart';

class HttpInterceptor implements InterceptorContract {
  Token tokenState;

  Alice _alice = Alice(
    navigatorKey: MyApp.navigatorKey,
    showNotification: true,
    showInspectorOnShake: true,
    darkTheme: false,
    maxCallsCount: 1000,
  );

  // RequestData currentRequestData;
  HttpInterceptor(this.tokenState);
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    try {
      // this.currentRequestData = data;
      data.headers['Accept'] = 'application/json; charset=UTF-8';
      data.headers['Content-Type'] = 'application/json; charset=UTF-8';
      data.headers['connection'] = 'keep-alive';
      data.headers['Authorization'] = 'Bearer ${tokenState.accessToken}';

      // data.body = tokenState.getUserDetails();

      print("Request Interceptor");
      // print(data);
    } catch (e) {
      print("error interceptor request *** $e");
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    print("Respanse interceptor ***");
    // tokenState.startTimer();
    _alice.onHttpResponse(data.toHttpResponse());
    return data;
  }
}

class ExpiredTokenRetryPolicy extends RetryPolicy {
  @override
  int maxRetryAttempts = 2;

  Token tokenState;
  ExpiredTokenRetryPolicy(this.tokenState);
  @override
  Future<bool> shouldAttemptRetryOnResponse(ResponseData response) async {
    // dynamic data = json.decode(response.body);
    // && data["error"] != "invalid_grant"
    if (response.statusCode == 401) {
      // Perform your token refresh here.
      print('start refresh token in RetryPolicy');
      tokenState = await refreshToken(tokenState);
      return true;
    }

    return false;
  }
}

CompteService get compteService => GetIt.I<CompteService>();

refreshToken(tokenState) async {
  print("refresh service");
  var params = {
    "client_id": client_id,
    "grant_type": "refresh_token",
    "codeBanque": codeBanque,
    "refresh_token": "${tokenState.refreshToken}",
    "token": "${tokenState.accessToken}",
    "typeProfil": typeProfil,
    "baseUrl": baseUrl,
  };
  try {
    var res = await http.post(URL.toUri(),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          // 'Authorization': 'Bearer $token',
        },
        body: params);
    final data = json.decode(res.body);
    print("status code ${res.statusCode}");
    // print(data);
    Token token = Token.fromJson(data);
    compteService.tokenState = token;
    return token;
  } catch (e) {
    print("error authentication refresh token **** $e");
  }
}
