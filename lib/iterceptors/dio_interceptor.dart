import 'package:dio/dio.dart';

class DioInterceptor extends Interceptor{
  String token;
  bool isPost = false;
  DioInterceptor({this.token, this.isPost});

  var headers = Map<String, String>();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // headers['X-CSRF-TOKEN'] = '$token';
    headers['Accept'] = 'application/json';
    if (isPost)
      headers['content-type'] = 'application/x-www-form-urlencoded';

    options.headers.addAll(headers);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {

  }

}