import 'dart:async';
import 'dart:io';

import 'package:LBA/bloc/bancassurance/banceassurance.bloc.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.state.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.bloc.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/bloc/carte/carte.bloc.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/cheque/cheque.state.dart';
import 'package:LBA/bloc/compte/compte.bloc.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/contrat/contrat.bloc.dart';
import 'package:LBA/bloc/contrat/contrat.state.dart';
import 'package:LBA/bloc/credits/credit.bloc.dart';
import 'package:LBA/bloc/credits/credit.state.dart';
import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.state.dart';
import 'package:LBA/bloc/devise/devise.bloc.dart';
import 'package:LBA/bloc/devise/devise.state.dart';
import 'package:LBA/bloc/effets/effets.bloc.dart';
import 'package:LBA/bloc/effets/effets.state.dart';
import 'package:LBA/bloc/facture/facture.bloc.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/bloc/facturehistory/facture_history_bloc.dart';
import 'package:LBA/bloc/facturehistory/facture_history_state.dart';
import 'package:LBA/bloc/graph/graph.bloc.dart';
import 'package:LBA/bloc/graph/graph.state.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.bloc.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.state.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.bloc.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.state.dart';
import 'package:LBA/bloc/messagerie/messagerie.bloc.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.state.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.state.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.bloc.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.state.dart';
import 'package:LBA/bloc/rib/rib.bloc.dart';
import 'package:LBA/bloc/rib/rib.state.dart';
import 'package:LBA/bloc/token/token.bloc.dart';
import 'package:LBA/bloc/token/token.state.dart';
import 'package:LBA/bloc/transfert/transfert_bloc.dart';
import 'package:LBA/bloc/virement/virement.bloc.dart';
import 'package:LBA/bloc/virement/virement.state.dart';
import 'package:LBA/config/global.params.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/pages/Login/login.page.dart';
import 'package:LBA/pages/util/colors/customPrimarySwatch.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/authentication.service.dart';
import 'package:LBA/services/beneficiaire.service.dart';
import 'package:LBA/services/carte.service.dart';
import 'package:LBA/services/cheque.service.dart';
import 'package:LBA/services/credit_service.dart';
import 'package:LBA/services/facture_service.dart';
import 'package:LBA/services/forced_upgrade_service.dart';
import 'package:LBA/services/messagerie.service.dart';
import 'package:LBA/services/transfert.service.dart';
import 'package:LBA/services/virement.service.dart';
import 'package:LBA/services/wallet.service.dart';
import 'package:LBA/shared_data.dart';
import 'package:LBA/widgets/alertWidget.dart';
import 'package:LBA/widgets/page_wrapper.widget.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import 'agences/agencies_service.dart';
import 'bloc/titres/titre_bloc.dart';
import 'bloc/titres/titre_state.dart';
import 'bloc/transfert/transfert_state.dart';
import 'bloc/wallet/wallet_bloc.dart';
import 'bloc/wallet/wallet_state.dart';

void setUpServices() {
  GetIt.instance.registerLazySingleton(() => AuthenticationService());
  GetIt.instance.registerLazySingleton(() => CompteService());
  GetIt.instance.registerLazySingleton(() => VirementService());
  GetIt.instance.registerLazySingleton(() => BeneficiaireService());
  GetIt.instance.registerLazySingleton(() => CarteService());
  GetIt.instance.registerLazySingleton(() => BannerAdsService());
  GetIt.instance.registerLazySingleton(() => ChequeService());
  GetIt.instance.registerLazySingleton(() => MessagerieService());
  GetIt.instance.registerLazySingleton(() => TransfertService());
  GetIt.instance.registerLazySingleton(() => FactureService());
  GetIt.instance.registerLazySingleton(() => CreditService());
  GetIt.instance.registerLazySingleton(() => ForcedUpgradeService());
  GetIt.instance.registerLazySingleton(() => WalletService());
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  final storage = new FlutterSecureStorage();
  int notifCount = 0;
  storage.read(key: 'notifCount').then((value) {
    notifCount = value.isNotEmpty ? int.parse(value) : 0;
    notifCount++;
    storage.write(key: 'notifCount', value: notifCount.toString());
    print("Handling a background message: notifCount $notifCount");
  });
}

main() async {
  HttpOverrides.global = MyHttpOverrides(); // badCertificateCB

  setUpServices();
  WidgetsFlutterBinding.ensureInitialized();

  // SystemChrome.setEnabledSystemUIOverlays([]);

  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: GlobalParams.themes["$banque_id"].appBarColor,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: Colors.black,
  ));

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  static Timer timer;
  static final navigatorKey = GlobalKey<NavigatorState>();

  static void initializeTimer() {
    if (MyApp.timer != null) {
      MyApp.timer.cancel();
    }
    if (!appData.isLogout) {
      print('timer init');
      MyApp.timer = Timer(const Duration(seconds: 12000), logOutUser);
    }
  }

  static void logOutUser() {
    MyApp.timer.cancel();
    MyApp.timer = null;

    inactivityAlert(navigatorKey.currentState.overlay.context);
  }

  static void cancelTimer() {
    if (MyApp.timer != null) {
      MyApp.timer.cancel();
      MyApp.timer = null;
    }
  }

  static void handleUserInteraction([_]) {
    initializeTimer();
  }

  @override
  _MyAppState createState() => _MyAppState();

  static _MyAppState of(BuildContext context) => context.findAncestorStateOfType<_MyAppState>();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  Locale _locale = Locale.fromSubtags(languageCode: 'fr');

  void setLocale(Locale value) {
    setState(() {
      _locale = value;
    });
  }

  Locale getLocale() {
    return _locale;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: GlobalParams.themes["$banque_id"].appBarColor,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) =>
                TokenBloc(TokenStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                ContratBloc(ContratStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => MouvementBloc(
                MouvementStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                GraphBloc(GraphStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => NomCompteBloc(
                NomCompteStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                DatBloc(DatStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                ChequeBloc(ChequeStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => CompteBloc(
                  CompteStateBloc(
                      requestState: StateStatus.NONE,
                      requestStateVir: StateStatus.NONE,
                      requestStatePsd: StateStatus.NONE,
                      requestStateCheque: StateStatus.NONE),
                  context.read<MouvementBloc>(),
                  context.read<GraphBloc>(),
                  context.read<NomCompteBloc>(),
                  context.read<DatBloc>(),
                  context.read<ChequeBloc>(),
                )),
        BlocProvider(
            create: (context) => BeneficiaireBloc(BeneficiaireStateBloc(
                  requestState: StateStatus.NONE,
                  requestStateAdd: StateStatus.NONE,
                  requestStateDelete: StateStatus.NONE,
                  requestStateAbon: StateStatus.NONE,
                ))),
        BlocProvider(
            create: (context) => HistoriqueVirementBloc(
                HistoriqueVirementStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => HistoriqueVirementCompteBloc(
                HistoriqueVirementCompteStateBloc(
                    requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => MouvementCarteBloc(MouvementCarteStateBloc(
                requestState: StateStatus.NONE,
                requestStatePl: StateStatus.NONE))),
        BlocProvider(
            create: (context) => CarteBloc(
                CarteStateBloc(
                  requestState: StateStatus.NONE,
                ),
                context.read<MouvementCarteBloc>())),
        BlocProvider(
            create: (context) =>
                CreditBloc(CreditStateBloc(requestState: StateStatus.NONE, requestStepTwoForm: StateStatus.NONE))),
        BlocProvider(
            create: (context) => VirementBloc(
                VirementStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                RibBloc(RibStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => MessagerieBloc(MessagerieStateBloc(
                  requestStateEn: StateStatus.NONE,
                  requestStateSub: StateStatus.NONE,
                  requestStateDes: StateStatus.NONE,
                  requestStateRc: StateStatus.NONE,
                  requestStateLoadMore: StateStatus.NONE,
                ))),
        BlocProvider(
            create: (context) =>
                DeviseBloc(DeviseStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                FactureBloc(FactureStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => TransfertBloc(
                TransfertState(requestStateSave: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                TitreBloc(TitreStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => FactureHistoryBloc(
                FactureHistoryStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) =>
                EffetsBloc(EffetsStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => BanceassuranceBloc(
                BanceassuranceStateBloc(requestState: StateStatus.NONE))),
        BlocProvider(
            create: (context) => WalletBloc(
                WalletState(requestStatePaiementAchatSave: StateStatus.NONE, requestStateEnterEnRelation: StateStatus.NONE, requestStateLoginDP: StateStatus.NONE)))
      ],
      child: PageWrapperWidget(
        child: MaterialApp(
          locale: _locale,
          localizationsDelegates: [
            // ... app-specific localization delegate[s] here
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,

            // DefaultMaterialLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('fr'),
            const Locale('en'),
            const Locale('ar'),
          ],
          debugShowCheckedModeBanner: false,
          title: 'LBA',

          navigatorKey: MyApp.navigatorKey,

          // darkTheme: ThemeData(
          //   systemOverlayStyle: SystemUiOverlayStyle.light,
          // ),
          // themeMode: ThemeMode.dark,

          theme: ThemeData(
            // cursorColor: principaleColor5,
            primarySwatch: banque_id == 'NSIA'
                ? banque_id == 'LBA'
                    ? Colors.pink
                    : Palette.kToDark
                : Colors.orange,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          // theme: ThemeData.dark(),
          home: AnimatedSplashScreen(
              duration: 1000,
              splash: Image.asset(
                "assets/$banque_id/images/logo.png",
                // fit: BoxFit.cover,
                height: 200,
                width: 500,
              ),
              nextScreen: LoginPage(),
              splashTransition: SplashTransition.fadeTransition,
              backgroundColor: Colors.white),
        ),
      ),
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
