import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/historique-virement.model.dart';

class HistoriqueVirementCompteStateBloc {
  StateStatus requestState;
  String errorMessage;
  HistoriqueVirementCompteEvent currentAction;
  HistoriqueVirement historiqueVirementCompte;
  var res;

  HistoriqueVirementCompteStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.historiqueVirementCompte,
      this.res});

  HistoriqueVirementCompteStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.historiqueVirementCompte = HistoriqueVirement();
  }
}
