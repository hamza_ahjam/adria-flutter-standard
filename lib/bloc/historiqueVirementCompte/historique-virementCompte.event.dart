import 'package:LBA/models/compte.model.dart';

abstract class HistoriqueVirementCompteEvent<T> {
  T payload;
  HistoriqueVirementCompteEvent({this.payload});
}

class LoadHistoriqueVirementCompteEvent extends HistoriqueVirementCompteEvent {
  int page;
  LoadHistoriqueVirementCompteEvent({this.page}) : super();
}

class ChangeAccountHistoriqueVirementCompteEvent
    extends HistoriqueVirementCompteEvent {
  int page;
  Compte compte;
  ChangeAccountHistoriqueVirementCompteEvent({this.page, this.compte})
      : super();
}

class SearchVirCompteEvent extends HistoriqueVirementCompteEvent {
  String dateDebut, dateFin, libelle;
  SearchVirCompteEvent({this.dateDebut, this.dateFin, this.libelle}) : super();
}

class ResetHistoriqueVirementCompteEvent extends HistoriqueVirementCompteEvent {
  ResetHistoriqueVirementCompteEvent() : super();
}
