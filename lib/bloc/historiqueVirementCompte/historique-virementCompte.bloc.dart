import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.event.dart';
import 'package:LBA/bloc/historiqueVirementCompte/historique-virementCompte.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/historiqueVirement.dart';
import 'package:LBA/models/historique-virement.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/virement.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class HistoriqueVirementCompteBloc extends Bloc<HistoriqueVirementCompteEvent,
    HistoriqueVirementCompteStateBloc> {
  VirementService get virementService => GetIt.I<VirementService>();
  CompteService get compteService => GetIt.I<CompteService>();

  HistoriqueVirementCompteBloc(HistoriqueVirementCompteStateBloc initialState)
      : super(initialState);

  @override
  Stream<HistoriqueVirementCompteStateBloc> mapEventToState(
      HistoriqueVirementCompteEvent event) async* {
    if (event is LoadHistoriqueVirementCompteEvent) {
      yield HistoriqueVirementCompteStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementCompte: state.historiqueVirementCompte);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          final data = await virementService.historiqueVirementMultiDevise(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              compteService.currentCompte.identifiantInterne,
              event.page);

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementCompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountHistoriqueVirementCompteEvent) {
      yield HistoriqueVirementCompteStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementCompte: state.historiqueVirementCompte);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);
          compteService.currentCompte = event.compte;

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          compteService.currentCompte = event.compte;
          final data = await virementService.historiqueVirementMultiDevise(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              event.compte.identifiantInterne,
              event.page);

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementCompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SearchVirCompteEvent) {
      yield HistoriqueVirementCompteStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementCompte: state.historiqueVirementCompte);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          final data =
              await virementService.searchHistoriqueVirementMultiDevise(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            compteService.currentCompte.identifiantInterne,
            1,
            event.dateDebut,
            event.dateFin,
            event.libelle,
          );

          HistoriqueVirementCompteStateBloc historiqueVirementStateBloc =
              HistoriqueVirementCompteStateBloc(
                  historiqueVirementCompte: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementCompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetHistoriqueVirementCompteEvent) {
      yield HistoriqueVirementCompteStateBloc(requestState: StateStatus.NONE);
    }
  }
}
