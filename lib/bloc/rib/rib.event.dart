import 'package:LBA/models/compte.model.dart';

abstract class RibEvent<T> {
  T payload;
  RibEvent({this.payload});
}

class LoadRibPdfEvent extends RibEvent {
  LoadRibPdfEvent() : super();
}

class ChangeAccountEvent extends RibEvent {
  Compte compte;
  ChangeAccountEvent({this.compte}) : super();
}

class InitRibEvent extends RibEvent {
  InitRibEvent() : super();
}
