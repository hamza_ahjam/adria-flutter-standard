import 'dart:io';

import 'package:LBA/bloc/rib/rib.event.dart';
import 'package:LBA/bloc/rib/rib.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/rib.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class RibBloc extends Bloc<RibEvent, RibStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  RibBloc(
    RibStateBloc initialState,
  ) : super(initialState);

  @override
  Stream<RibStateBloc> mapEventToState(
    RibEvent event,
  ) async* {
    if (event is LoadRibPdfEvent) {
      yield RibStateBloc(requestState: StateStatus.LOADING);
      try {
        Rib data = await compteService.getRib(compteService.tokenState,
            compteService.currentCompte.identifiantInterne);

        PdfDocument releve = PdfDocument.fromBase64String(data.map.content);

        var pdfBytes = releve.save();

        final directory = await getApplicationDocumentsDirectory();

        File file = File("${directory.path}/${data.map.filename}");

        file.writeAsBytes(pdfBytes);

        RibStateBloc ribStateBloc = RibStateBloc(
          requestState: StateStatus.LOADED,
          currentAction: event,
          relevePdf: data,
          file: file,
        );
        yield ribStateBloc;
      } catch (e) {
        yield RibStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountEvent) {
      yield RibStateBloc(requestState: StateStatus.LOADING);
      try {
        Rib data = await compteService.getRib(
            compteService.tokenState, event.compte.identifiantInterne);

        PdfDocument releve = PdfDocument.fromBase64String(data.map.content);

        var pdfBytes = releve.save();

        final directory = await getApplicationDocumentsDirectory();

        File file = File("${directory.path}/${data.map.filename}");

        file.writeAsBytes(pdfBytes);

        RibStateBloc ribStateBloc = RibStateBloc(
          requestState: StateStatus.LOADED,
          currentAction: event,
          relevePdf: data,
          file: file,
        );
        yield ribStateBloc;
      } catch (e) {
        yield RibStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitRibEvent) {
      yield RibStateBloc(requestState: StateStatus.NONE);
    }
  }
}
