import 'dart:io';

import 'package:LBA/bloc/rib/rib.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/rib.model.dart';

class RibStateBloc {
  StateStatus requestState;
  String errorMessage;
  RibEvent currentAction;
  Rib relevePdf;
  File file;

  RibStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.file,
    this.relevePdf,
  });

  RibStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
  }
}
