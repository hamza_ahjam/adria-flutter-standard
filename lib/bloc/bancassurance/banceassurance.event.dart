abstract class BanceassuranceEvent<T> {
  T payload;
  BanceassuranceEvent({this.payload});
}

class LoadBanceassuranceEvent extends BanceassuranceEvent {
  LoadBanceassuranceEvent() : super();
}

class LoadListAssuranceEvent extends BanceassuranceEvent {
  String code;
  LoadListAssuranceEvent({this.code}) : super();
}

class LoadSituationAssuranceEvent extends BanceassuranceEvent {
  String productNumber;
  String policeNumber;
  LoadSituationAssuranceEvent({this.productNumber, this.policeNumber}) : super();
}

class LoadCotisationsAssuranceEvent extends BanceassuranceEvent {
  String idContrat;
  String typeAssurance;
  LoadCotisationsAssuranceEvent({this.idContrat, this.typeAssurance}) : super();
}

class ResetBanceassuranceEvent extends BanceassuranceEvent {
  ResetBanceassuranceEvent() : super();
}
