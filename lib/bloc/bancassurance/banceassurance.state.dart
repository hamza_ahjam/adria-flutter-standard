import 'package:LBA/bloc/bancassurance/banceassurance.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/bancAssurance_cotisation.model.dart';
import 'package:LBA/models/bancAssurance_situation.model.dart';
import 'package:LBA/models/banceassurance.model.dart';
import 'package:LBA/models/list_assurance.model.dart';

class BanceassuranceStateBloc {
  StateStatus requestState;
  StateStatus requestStateListAssurance;
  StateStatus requestStateCotisations;
  StateStatus requestStateSituation;
  String errorMessage;
  BanceassuranceEvent currentAction;
  Bancassurance bancassuranceList;
  ListAssurance listAssurance;
  BancAssuranceCotisation cotisations;
  BancAssuranceSituation assuranceSituation;
  var res;

  BanceassuranceStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStateListAssurance,
      this.requestStateCotisations,
      this.requestStateSituation,
      this.bancassuranceList,
      this.listAssurance,
      this.cotisations,
      this.assuranceSituation,
      this.res});

  BanceassuranceStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateListAssurance = StateStatus.NONE;
    this.requestStateCotisations = StateStatus.NONE;
    this.requestStateSituation = StateStatus.NONE;
    this.errorMessage = "";
    this.bancassuranceList = Bancassurance();
    this.listAssurance = ListAssurance();
    this.cotisations = BancAssuranceCotisation();
    this.assuranceSituation = BancAssuranceSituation();
    this.res = null;
  }
}
