import 'package:LBA/bloc/bancassurance/banceassurance.event.dart';
import 'package:LBA/bloc/bancassurance/banceassurance.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class BanceassuranceBloc
    extends Bloc<BanceassuranceEvent, BanceassuranceStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  BanceassuranceBloc(BanceassuranceStateBloc initialState)
      : super(initialState);
  @override
  Stream<BanceassuranceStateBloc> mapEventToState(
      BanceassuranceEvent event) async* {
    if (event is LoadBanceassuranceEvent) {
      yield BanceassuranceStateBloc(requestState: StateStatus.LOADING);
      try {
        final data = await compteService.getBanceassurance();

        BanceassuranceStateBloc contratStateBloc = BanceassuranceStateBloc(
            bancassuranceList: data,
            requestState: StateStatus.LOADED,
            currentAction: event);
        yield contratStateBloc;
      } catch (e) {
        yield BanceassuranceStateBloc(
            errorMessage: e.message.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadListAssuranceEvent) {
      yield BanceassuranceStateBloc(
        requestStateListAssurance: StateStatus.LOADING,
        requestState: state.requestState,
        bancassuranceList: state.bancassuranceList,
      );
      try {
        final data = await compteService.getListAssurance(event.code);

        BanceassuranceStateBloc contratStateBloc = BanceassuranceStateBloc(
          listAssurance: data,
          requestStateListAssurance: StateStatus.LOADED,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
        yield contratStateBloc;
      } catch (e) {
        yield BanceassuranceStateBloc(
          errorMessage: e.message.toString(),
          requestStateListAssurance: StateStatus.ERROR,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
      }
    } else if (event is LoadSituationAssuranceEvent) {
      yield BanceassuranceStateBloc(
        requestStateSituation: StateStatus.LOADING,
        listAssurance: state.listAssurance,
        requestStateListAssurance: state.requestStateListAssurance,
        requestState: state.requestState,
        bancassuranceList: state.bancassuranceList,
      );
      try {
        final data = await compteService.getSituation(event.productNumber, event.policeNumber);

        BanceassuranceStateBloc situationStateBloc = BanceassuranceStateBloc(
          assuranceSituation: data,
          requestStateSituation: StateStatus.LOADED,
          listAssurance: state.listAssurance,
          requestStateListAssurance: state.requestStateListAssurance,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
        yield situationStateBloc;
      } catch (e) {
        yield BanceassuranceStateBloc(
          errorMessage: e.message.toString(),
          requestStateSituation: StateStatus.ERROR,
          requestStateListAssurance: state.requestStateListAssurance,
          listAssurance: state.listAssurance,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
      }
    } else if (event is LoadCotisationsAssuranceEvent) {
      yield BanceassuranceStateBloc(
        requestStateCotisations: StateStatus.LOADING,
        listAssurance: state.listAssurance,
        assuranceSituation: state.assuranceSituation,
        requestStateSituation: state.requestStateSituation,
        requestStateListAssurance: state.requestStateListAssurance,
        requestState: state.requestState,
        bancassuranceList: state.bancassuranceList,
      );
      try {
        final data = await compteService.getCotisations(event.idContrat, event.typeAssurance);

        BanceassuranceStateBloc situationStateBloc = BanceassuranceStateBloc(
          cotisations: data,
          requestStateCotisations: StateStatus.LOADED,
          assuranceSituation: state.assuranceSituation,
          requestStateSituation: state.requestStateSituation,
          listAssurance: state.listAssurance,
          requestStateListAssurance: state.requestStateListAssurance,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
        yield situationStateBloc;
      } catch (e) {
        yield BanceassuranceStateBloc(
          errorMessage: e.message.toString(),
          requestStateCotisations: StateStatus.ERROR,
          assuranceSituation: state.assuranceSituation,
          requestStateSituation: state.requestStateSituation,
          requestStateListAssurance: state.requestStateListAssurance,
          listAssurance: state.listAssurance,
          currentAction: event,
          requestState: state.requestState,
          bancassuranceList: state.bancassuranceList,
        );
      }
    } else if (event is ResetBanceassuranceEvent) {
      yield BanceassuranceStateBloc(requestState: StateStatus.NONE);
    }
  }
}
