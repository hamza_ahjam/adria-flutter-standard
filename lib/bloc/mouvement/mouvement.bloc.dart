import 'dart:io';

import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/bloc/mouvement/mouvement.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/listMouvement.dart';
import 'package:LBA/mocks/mouvementPDF.dart';
import 'package:LBA/models/historique.model.dart';
import 'package:LBA/models/rechercheMouvement.model.dart';
import 'package:LBA/models/rib.model.dart';
import 'package:LBA/pages/mouvement/sharedData/mouvement.sharedData.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class MouvementBloc extends Bloc<MouvementEvent, MouvementStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  MouvementBloc(MouvementStateBloc initialState) : super(initialState);

  @override
  Stream<MouvementStateBloc> mapEventToState(MouvementEvent event) async* {
    if (event is LoadMouvementEvent) {
      yield MouvementStateBloc(
          requestState: StateStatus.LOADING, mouvement: state.mouvement);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          RechercheMouvement historiqueMouvements =
              RechercheMouvement.fromJson(listMouvement);

          historiqueMouvements.map.orderedlistMouvements = new Map();

          historiqueMouvements.map.transform(Mode.MONTH);
          final data = historiqueMouvements;
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        } else {
          final data = await compteService.getMouvements(1);
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        }
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountEvent) {
      yield MouvementStateBloc(
          requestState: StateStatus.LOADING, mouvement: state.mouvement);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = RechercheMouvement.fromJson(listMouvement);

          RechercheMouvement historiqueMouvements = data;
          historiqueMouvements.map.orderedlistMouvements = new Map();
          historiqueMouvements.map.transform(appData.critere);

          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        } else {
          compteService.currentCompte = event.compte;
          final data = await compteService.getMouvements(1);

          RechercheMouvement historiqueMouvements = data;
          historiqueMouvements.map.orderedlistMouvements = new Map();
          historiqueMouvements.map.transform(appData.critere);

          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        }
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadRecherchMouvement) {
      yield MouvementStateBloc(requestState: StateStatus.LOADING);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          RechercheMouvement historiqueMouvements =
              RechercheMouvement.fromJson(listMouvement);

          historiqueMouvements.map.orderedlistMouvements = new Map();

          historiqueMouvements.map.transform(appData.critere);

          final data = historiqueMouvements;
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        } else {
          RechercheMouvement data = await compteService.getRechercheMouvements(
              event.dateFin, event.dateDebut, event.libelle);
          // print(data.map.results.length);
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        }
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadMouvementFiltre) {
      yield MouvementStateBloc(
          requestState: StateStatus.LOADING, mouvement: state.mouvement);
      try {
        RechercheMouvement historiqueMouvements = state.mouvement;
        historiqueMouvements.map.orderedlistMouvements = new Map();

        historiqueMouvements.map.transform(event.mode);

        MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
            mouvement: state.mouvement,
            requestState: StateStatus.LOADED,
            currentAction: event);
        yield mouvementStateBloc;
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadMouvementPdfEvent) {
      yield MouvementStateBloc(
          requestState: StateStatus.LOADING, mouvement: state.mouvement);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          Rib data = Rib.fromJson(mockMouvementPdf);

          PdfDocument releve = PdfDocument.fromBase64String(data.map.content);

          var pdfBytes = releve.save();

          final directory = await getApplicationDocumentsDirectory();

          File file = File("${directory.path}/${data.map.filename}");

          file.writeAsBytes(pdfBytes);

          // print("data : ** $data");
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: state.mouvement,
              relevePdf: data,
              file: file,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        } else {
          Rib data = await compteService.getMouvementPdf(
              event.dateDebut, event.dateFin);

          PdfDocument releve = PdfDocument.fromBase64String(data.map.content);

          var pdfBytes = releve.save();

          final directory = await getApplicationDocumentsDirectory();

          File file = File("${directory.path}/${data.map.filename}");

          file.writeAsBytes(pdfBytes);

          // print("data : ** $data");
          MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
              mouvement: state.mouvement,
              relevePdf: data,
              file: file,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield mouvementStateBloc;
        }
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadNextMouvementPage) {
      yield MouvementStateBloc(requestState: StateStatus.LOADING);
      try {
        RechercheMouvement data = await compteService.getMouvements(event.page);

        MouvementStateBloc mouvementStateBloc = MouvementStateBloc(
            mouvement: data,
            requestState: StateStatus.LOADED,
            currentAction: event);
        yield mouvementStateBloc;
      } catch (e) {
        yield MouvementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetMouvementEvent) {
      yield MouvementStateBloc(requestState: StateStatus.NONE);
    }
  }
}
