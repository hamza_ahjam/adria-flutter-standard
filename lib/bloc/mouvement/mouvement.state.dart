import 'dart:io';

import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/rechercheMouvement.model.dart';
import 'package:LBA/models/rib.model.dart';

class MouvementStateBloc {
  StateStatus requestState;
  String errorMessage;
  MouvementEvent currentAction;
  RechercheMouvement mouvement;
  Rib relevePdf;
  File file;

  MouvementStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.mouvement,
      this.relevePdf,
      this.file});

  MouvementStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.mouvement = RechercheMouvement();
    this.relevePdf = Rib();
  }
}
