import 'package:LBA/models/compte.model.dart';
import 'package:LBA/models/historique.model.dart';

abstract class MouvementEvent<T> {
  T payload;
  MouvementEvent({this.payload});
}

class LoadMouvementEvent extends MouvementEvent {
  LoadMouvementEvent() : super();
}

class ResetMouvementEvent extends MouvementEvent {
  ResetMouvementEvent() : super();
}

class ChangeAccountEvent extends MouvementEvent {
  Compte compte;
  ChangeAccountEvent({this.compte}) : super();
}

class LoadRecherchMouvement extends MouvementEvent {
  String dateDebut, dateFin, libelle;
  LoadRecherchMouvement({this.dateDebut, this.dateFin, this.libelle}) : super();
}

class LoadMouvementFiltre extends MouvementEvent {
  Mode mode;
  LoadMouvementFiltre({this.mode}) : super();
}

class LoadMouvementPdfEvent extends MouvementEvent {
  String dateDebut, dateFin;
  LoadMouvementPdfEvent({this.dateDebut, this.dateFin}) : super();
}

class LoadNextMouvementPage extends MouvementEvent {
  int page;
  LoadNextMouvementPage({this.page}) : super();
}
