import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/CreditHabitatForm.dart';
import 'package:LBA/models/credit.model.dart';
import 'package:LBA/models/credits_step_two.dart';

class CreditStateBloc {
  StateStatus requestState;
  StateStatus requestStateForm;
  StateStatus requestStepTwoForm;
  String errorMessage;
  CreditEvent currentAction;
  Credits listCredit;
  CreditHabitatForm formItems;
  CreditsStepTwo creditsStepTwo;

  CreditStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.requestStateForm,
    this.listCredit,
    this.requestStepTwoForm,
    this.creditsStepTwo,
    this.formItems
  });

  CreditStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateForm = StateStatus.NONE;
    this.errorMessage = "";
    this.listCredit = Credits();
    this.formItems = CreditHabitatForm();
  }
}
