abstract class CreditEvent<T> {
  T payload;
  CreditEvent({this.payload});
}

class LoadCreditEvent extends CreditEvent {
  LoadCreditEvent() : super();
}

class LoadCreditStepTwo extends CreditEvent{
  LoadCreditStepTwo() : super();
}

class LoadCreditHabitatFormEvent extends CreditEvent {
  String product;
  String step;
  LoadCreditHabitatFormEvent({this.product, this.step}) : super();
}

class InitCreditEvent extends CreditEvent {
  InitCreditEvent() : super();
}
