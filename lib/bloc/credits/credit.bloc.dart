import 'package:LBA/bloc/credits/credit.event.dart';
import 'package:LBA/bloc/credits/credit.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/listCredit.dart';
import 'package:LBA/models/CreditHabitatForm.dart';
import 'package:LBA/models/credit.model.dart';
import 'package:LBA/models/credits_step_two.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/credit_service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class CreditBloc extends Bloc<CreditEvent, CreditStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  CreditService get creditService => GetIt.I<CreditService>();

  CreditBloc(CreditStateBloc initialState) : super(initialState);
  @override
  Stream<CreditStateBloc> mapEventToState(CreditEvent event) async* {
    if (event is LoadCreditEvent) {
      print("Credit event : LoadCreditEvent");
      yield CreditStateBloc(
          requestState: StateStatus.LOADING, listCredit: state.listCredit);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          Credits data = Credits.fromJson(listCreditMock);

          CreditStateBloc contratStateBloc = CreditStateBloc(
              listCredit: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        } else {
          Credits data = await compteService.getCredits();

          CreditStateBloc contratStateBloc = CreditStateBloc(
              listCredit: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        }
      } catch (e) {
        yield CreditStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadCreditStepTwo) {
      print("load event credit step two event");
      yield CreditStateBloc(
          requestStepTwoForm: StateStatus.LOADING,
          creditsStepTwo: state.creditsStepTwo);

      try {
        CreditsStepTwo data = await creditService.getForm();

        print("loaded event credit step two");

        CreditStateBloc contratStateBloc = CreditStateBloc(
            creditsStepTwo: data,
            requestStepTwoForm: StateStatus.LOADED,
            currentAction: event);
        yield contratStateBloc;
      } catch (e) {
        print("credit step two event error");

        yield CreditStateBloc(
            errorMessage: e.toString(),
            requestStepTwoForm: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadCreditHabitatFormEvent) {
      print("Credit event : LoadCreditHabitatFormEvent");
      yield CreditStateBloc(
        requestStateForm: StateStatus.LOADING,
      );
      try {
        CreditHabitatForm data =
            await compteService.getCreditHabitatForm(event.product, event.step);

        CreditStateBloc contratStateBloc = CreditStateBloc(
            formItems: data,
            requestStateForm: StateStatus.LOADED,
            requestStepTwoForm: StateStatus.NONE,
            currentAction: event);
        yield contratStateBloc;
      } catch (e) {
        yield CreditStateBloc(
            errorMessage: e.toString(),
            requestStateForm: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitCreditEvent) {
      print("Credit event : InitCreditEvent");
      yield CreditStateBloc(
          requestState: StateStatus.NONE,
          requestStateForm: StateStatus.NONE,
          requestStepTwoForm: StateStatus.NONE);
    }
  }
}
