abstract class TokenEvent<T> {
  T payload;
  TokenEvent({this.payload});
}

class LoginEvent extends TokenEvent {
  String userName, password;
  LoginEvent({this.userName, this.password}) : super();
}

class ForgetPsdEvent extends TokenEvent {
  String identifiant;
  ForgetPsdEvent({this.identifiant}) : super();
}

class LogoutEvent extends TokenEvent {
  LogoutEvent() : super();
}

class ActivateAccountEvent extends TokenEvent {
  String rad;
  ActivateAccountEvent({this.rad}) : super();
}

class ActivateAccountSaveOtpEvent extends TokenEvent {
  String otp;
  ActivateAccountSaveOtpEvent({this.otp}) : super();
}
