import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/token.model.dart';

class TokenStateBloc {
  StateStatus requestState;
  StateStatus requestStatePsd;
  String errorMessage;
  TokenEvent currentAction;
  String userName, password;
  Token token;
  var res;
  var resActivated;
  var resOtp;

  TokenStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStatePsd,
      this.token,
      this.res,
      this.resActivated,
      this.resOtp,
      this.userName,
      this.password});

  TokenStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStatePsd = StateStatus.NONE;
    this.errorMessage = "";
    this.token = Token();
    this.res = null;
    this.resActivated = null;
    this.resOtp = null;
  }

  // Token get getToken => token;

}
