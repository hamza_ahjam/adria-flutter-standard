import 'dart:async';

import 'package:LBA/bloc/token/token.event.dart';
import 'package:LBA/bloc/token/token.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/tokenMock.dart';
import 'package:LBA/models/token.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/authentication.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

// part 'token_event.dart';
// part 'token_state.dart';

class TokenBloc extends Bloc<TokenEvent, TokenStateBloc> {
  AuthenticationService get serviceAuth => GetIt.I<AuthenticationService>();
  CompteService get compteService => GetIt.I<CompteService>();

  TokenBloc(
    TokenStateBloc initialState,
  ) : super(initialState);

  @override
  Stream<TokenStateBloc> mapEventToState(
    TokenEvent event,
  ) async* {
    if (event is LoginEvent) {
      yield TokenStateBloc(requestState: StateStatus.LOADING);
      try {
        if (modeDemo) {
          final data = Token.fromJson(tokenMock);
          compteService.tokenState = data;
          TokenStateBloc tokenState = TokenStateBloc(
              requestState: StateStatus.LOADED,
              token: data,
              currentAction: event);
          yield tokenState;
        } else {
          final data = await serviceAuth.login(event.userName, event.password);
          compteService.tokenState = data;
          TokenStateBloc tokenState = TokenStateBloc(
              requestState: StateStatus.LOADED,
              token: data,
              currentAction: event);
          yield tokenState;
        }
      } catch (e) {
        // print("bloc err $e");
        yield TokenStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ForgetPsdEvent) {
      yield TokenStateBloc(
          requestStatePsd: StateStatus.LOADING,
          requestState: state.requestState);
      try {
        final data = await compteService.forgetPsd(event.identifiant);

        if (data["success"]) {
          TokenStateBloc tokenState = TokenStateBloc(
              requestStatePsd: StateStatus.LOADED,
              requestState: state.requestState,
              res: data,
              currentAction: event);
          yield tokenState;
        } else {
          TokenStateBloc tokenState = TokenStateBloc(
              requestStatePsd: StateStatus.ERROR,
              requestState: state.requestState,
              res: data,
              errorMessage: data["message"],
              currentAction: event);
          yield tokenState;
        }
      } catch (e) {
        yield TokenStateBloc(
            errorMessage: e.toString(),
            requestStatePsd: StateStatus.ERROR,
            requestState: state.requestState,
            currentAction: event);
      }
    } else if (event is ActivateAccountEvent) {
      yield TokenStateBloc(requestState: StateStatus.LOADING);
      try {
        final data = await serviceAuth.activateCompte(event.rad);

        TokenStateBloc tokenState = TokenStateBloc(
            requestState: StateStatus.LOADED,
            resActivated: data,
            currentAction: event);
        yield tokenState;
      } catch (e) {
        // print("bloc err $e");
        yield TokenStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ActivateAccountSaveOtpEvent) {
      yield TokenStateBloc(
          requestStatePsd: StateStatus.LOADING,
          resActivated: state.resActivated);
      try {
        final data = await serviceAuth.activateCompteOtp(
            event.otp, state.resActivated["Map"]["Token"]);

        TokenStateBloc tokenState = TokenStateBloc(
            requestStatePsd: StateStatus.LOADED,
            resOtp: data,
            currentAction: event);
        yield tokenState;
      } catch (e) {
        // print("bloc err $e");
        yield TokenStateBloc(
            errorMessage: e.toString(),
            requestStatePsd: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LogoutEvent) {
      yield TokenStateBloc(requestState: StateStatus.NONE, token: null);
    }
  }
}
