import 'package:LBA/models/compte.model.dart';

abstract class HistoriqueVirementEvent<T> {
  T payload;
  HistoriqueVirementEvent({this.payload});
}

class LoadHistoriqueVirementEvent extends HistoriqueVirementEvent {
  int page;
  LoadHistoriqueVirementEvent({this.page}) : super();
}

class ChangeAccountHistoriqueVirementEvent extends HistoriqueVirementEvent {
  int page;
  Compte compte;
  ChangeAccountHistoriqueVirementEvent({this.page, this.compte}) : super();
}

class ResetHistoriqueVirementEvent extends HistoriqueVirementEvent {
  ResetHistoriqueVirementEvent() : super();
}

class SearchVirBenefEvent extends HistoriqueVirementEvent {
  String dateDebut, dateFin, libelle;
  SearchVirBenefEvent({this.dateDebut, this.dateFin, this.libelle}) : super();
}

// class LoadHistoriqueVirementCompteEvent extends HistoriqueVirementEvent {
//   int page;
//   LoadHistoriqueVirementCompteEvent({this.page}) : super();
// }

// class ChangeAccountHistoriqueVirementCompteEvent
//     extends HistoriqueVirementEvent {
//   int page;
//   Compte compte;
//   ChangeAccountHistoriqueVirementCompteEvent({this.page, this.compte})
//       : super();
// }
