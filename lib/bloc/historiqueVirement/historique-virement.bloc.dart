import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/bloc/historiqueVirement/historique-virement.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/historiqueVirement.dart';
import 'package:LBA/models/historique-virement.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/virement.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class HistoriqueVirementBloc
    extends Bloc<HistoriqueVirementEvent, HistoriqueVirementStateBloc> {
  VirementService get virementService => GetIt.I<VirementService>();
  CompteService get compteService => GetIt.I<CompteService>();

  HistoriqueVirementBloc(HistoriqueVirementStateBloc initialState)
      : super(initialState);

  @override
  Stream<HistoriqueVirementStateBloc> mapEventToState(
      HistoriqueVirementEvent event) async* {
    if (event is LoadHistoriqueVirementEvent) {
      yield HistoriqueVirementStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementBenif: state.historiqueVirementBenif);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);
          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          final data = await virementService.historiqueVirementBenif(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              compteService.currentCompte.identifiantInterne,
              event.page);

          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountHistoriqueVirementEvent) {
      yield HistoriqueVirementStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementBenif: state.historiqueVirementBenif);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);
          compteService.currentCompte = event.compte;

          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          compteService.currentCompte = event.compte;
          // compteStateBloc.currentCompte = compteService.currentCompte;
          final data = await virementService.historiqueVirementBenif(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              event.compte.identifiantInterne,
              event.page);

          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetHistoriqueVirementEvent) {
      yield HistoriqueVirementStateBloc(requestState: StateStatus.NONE);
    } else if (event is SearchVirBenefEvent) {
      yield HistoriqueVirementStateBloc(
          requestState: StateStatus.LOADING,
          historiqueVirementBenif: state.historiqueVirementBenif);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = HistoriqueVirement.fromJson(historiqueMock);

          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        } else {
          final data = await virementService.searchHistoriqueVirementBenif(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            compteService.currentCompte.identifiantInterne,
            1,
            event.dateDebut,
            event.dateFin,
            event.libelle,
          );

          HistoriqueVirementStateBloc historiqueVirementStateBloc =
              HistoriqueVirementStateBloc(
                  historiqueVirementBenif: data,
                  requestState: StateStatus.LOADED,
                  currentAction: event);
          yield historiqueVirementStateBloc;
        }
      } catch (e) {
        yield HistoriqueVirementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    }
    // if (event is LoadHistoriqueVirementCompteEvent) {
    //   yield HistoriqueVirementStateBloc(
    //     requestStateCompte: StateStatus.LOADING,
    //   );

    //   try {
    //     final data = await virementService.historiqueVirementMultiDevise(
    //         compteService.tokenState,
    //         compteService.tokenState.getUserDetails(),
    //         compteService.currentCompte.identifiantInterne,
    //         event.page);

    //     HistoriqueVirementStateBloc historiqueVirementStateBloc =
    //         HistoriqueVirementStateBloc(
    //             historiqueVirementCompte: data,
    //             requestStateCompte: StateStatus.LOADED,
    //             currentAction: event);
    //     yield historiqueVirementStateBloc;
    //   } catch (e) {
    //     yield HistoriqueVirementStateBloc(
    //         errorMessage: e.toString(),
    //         requestStateCompte: StateStatus.ERROR,
    //         currentAction: event);
    //   }
    // }
    // if (event is ChangeAccountHistoriqueVirementCompteEvent) {
    //   yield HistoriqueVirementStateBloc(
    //       requestStateCompte: StateStatus.LOADING,
    //       historiqueVirementBenif: state.historiqueVirementBenif);

    //   try {
    //     compteService.currentCompte = event.compte;
    //     final data = await virementService.historiqueVirementMultiDevise(
    //         compteService.tokenState,
    //         compteService.tokenState.getUserDetails(),
    //         event.compte.identifiantInterne,
    //         event.page);

    //     HistoriqueVirementStateBloc historiqueVirementStateBloc =
    //         HistoriqueVirementStateBloc(
    //             historiqueVirementCompte: data,
    //             requestStateCompte: StateStatus.LOADED,
    //             currentAction: event);
    //     yield historiqueVirementStateBloc;
    //   } catch (e) {
    //     yield HistoriqueVirementStateBloc(
    //         errorMessage: e.toString(),
    //         requestStateCompte: StateStatus.ERROR,
    //         currentAction: event);
    //   }
    // }
  }
}
