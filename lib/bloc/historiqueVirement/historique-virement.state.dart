import 'package:LBA/bloc/historiqueVirement/historique-virement.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/historique-virement.model.dart';

class HistoriqueVirementStateBloc {
  StateStatus requestState;
  // StateStatus requestStateCompte;
  String errorMessage;
  HistoriqueVirementEvent currentAction;
  HistoriqueVirement historiqueVirementBenif;
  var res;

  HistoriqueVirementStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.historiqueVirementBenif,
      this.res});

  HistoriqueVirementStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.historiqueVirementBenif = HistoriqueVirement();
  }
}
