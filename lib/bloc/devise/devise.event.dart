abstract class DeviseEvent<T> {
  T payload;
  DeviseEvent({this.payload});
}

class LoadDeviseEvent extends DeviseEvent {
  LoadDeviseEvent() : super();
}
