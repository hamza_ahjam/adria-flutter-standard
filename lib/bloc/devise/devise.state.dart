import 'package:LBA/bloc/devise/devise.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/devise.model.dart';

class DeviseStateBloc {
  StateStatus requestState;
  String errorMessage;
  DeviseEvent currentAction;
  Devise devise;

  DeviseStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.devise,
  });

  DeviseStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.devise = Devise();
  }
}
