import 'package:LBA/bloc/devise/devise.event.dart';
import 'package:LBA/bloc/devise/devise.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class DeviseBloc extends Bloc<DeviseEvent, DeviseStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  DeviseBloc(DeviseStateBloc initialState) : super(initialState);
  @override
  Stream<DeviseStateBloc> mapEventToState(DeviseEvent event) async* {
    if (event is LoadDeviseEvent) {
      yield DeviseStateBloc(
        requestState: StateStatus.LOADING,
      );
      try {
        final data = await compteService.getDevise();

        DeviseStateBloc deviseStateBloc = DeviseStateBloc(
            devise: data,
            requestState: StateStatus.LOADED,
            currentAction: event);
        yield deviseStateBloc;
      } catch (e) {
        yield DeviseStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    }
  }
}
