import 'package:LBA/bloc/titres/titre_event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/titre_model.dart';

class TitreStateBloc {
  StateStatus requestState;
  String errorMessage;
  TitreEvent currentAction;
  Titres listTitres;

  TitreStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.listTitres,
  });

  TitreStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.listTitres = Titres();
  }
}