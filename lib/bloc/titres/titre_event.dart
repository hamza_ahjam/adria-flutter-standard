abstract class TitreEvent<T> {
  T payload;
  TitreEvent({this.payload});
}

class LoadTitreEvent extends TitreEvent {
  LoadTitreEvent() : super();
}

class InitTitreEvent extends TitreEvent {
  InitTitreEvent() : super();
}
