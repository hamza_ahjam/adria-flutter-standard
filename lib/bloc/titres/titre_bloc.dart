import 'package:LBA/bloc/titres/titre_event.dart';
import 'package:LBA/bloc/titres/titre_state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/litst_titre_compte_mock.dart';
import 'package:LBA/models/titre_model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class TitreBloc extends Bloc<TitreEvent, TitreStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  TitreBloc(TitreStateBloc initialState) : super(initialState);
  @override
  Stream<TitreStateBloc> mapEventToState(TitreEvent event) async* {
    if (event is LoadTitreEvent) {
      yield TitreStateBloc(
          requestState: StateStatus.LOADING, listTitres: state.listTitres);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          Titres data = Titres.fromJson(listTitreCompteMock);

          TitreStateBloc titresStateBloc = TitreStateBloc(
              listTitres: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield titresStateBloc;
        } else {
          Titres data = await compteService.getTitres();

          TitreStateBloc titresStateBloc = TitreStateBloc(
              listTitres: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield titresStateBloc;
        }
      } catch (e) {
        yield TitreStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitTitreEvent) {
      yield TitreStateBloc(requestState: StateStatus.NONE);
    }
  }
}
