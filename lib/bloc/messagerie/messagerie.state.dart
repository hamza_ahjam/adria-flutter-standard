import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/destinataire.model.dart';
import 'package:LBA/models/detail_message.model.dart';
import 'package:LBA/models/messagerie.model.dart';
import 'package:LBA/models/subjectsMessagerie.model.dart';

class MessagerieStateBloc {
  StateStatus requestStateEn;
  StateStatus requestStateRc;
  StateStatus requestStateLoadMore;
  StateStatus requestStateSub;
  StateStatus requestStateDes;
  StateStatus requestStateSave;
  StateStatus requestStateDetail;
  String errorMessage;
  MessagerieEvent currentAction;
  Messagerie messagerieEn;
  Messagerie messagerieRc;
  SubjectsMessagerie subject;
  Destinataire destinataire;
  DetailMessage detailMessage;

  MessagerieStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestStateEn,
    this.requestStateLoadMore,
    this.requestStateRc,
    this.requestStateSub,
    this.messagerieEn,
    this.messagerieRc,
    this.requestStateDes,
    this.destinataire,
    this.subject,
    this.requestStateSave,
    this.detailMessage,
    this.requestStateDetail,
  });

  MessagerieStateBloc.initialState() {
    this.requestStateEn = StateStatus.NONE;
    this.requestStateLoadMore = StateStatus.NONE;
    this.requestStateRc = StateStatus.NONE;
    this.requestStateSub = StateStatus.NONE;
    this.requestStateDes = StateStatus.NONE;
    this.requestStateSave = StateStatus.NONE;
    this.requestStateDetail = StateStatus.NONE;
    this.errorMessage = "";
    this.messagerieEn = Messagerie();
    this.messagerieRc = Messagerie();
    this.subject = SubjectsMessagerie();
    this.destinataire = Destinataire();
    this.detailMessage = DetailMessage();
  }
}
