abstract class MessagerieEvent<T> {
  T payload;
  MessagerieEvent({this.payload});
}

class LoadMessageEnvEvent extends MessagerieEvent {
  int page;
  LoadMessageEnvEvent({this.page}) : super();
}

class LoadMoreMessageEnvEvent extends MessagerieEvent {
  int page;
  LoadMoreMessageEnvEvent({this.page}) : super();
}

class LoadMessageRcEvent extends MessagerieEvent {
  int page;
  LoadMessageRcEvent({this.page}) : super();
}

class LoadSubjectsMessagerieEvent extends MessagerieEvent {
  LoadSubjectsMessagerieEvent() : super();
}

class LoadDestinataireMessagerieEvent extends MessagerieEvent {
  LoadDestinataireMessagerieEvent() : super();
}

class LoadDetailMessageEvent extends MessagerieEvent {
  String id;
  LoadDetailMessageEvent({this.id}) : super();
}

class ResetMessageEvent extends MessagerieEvent {
  ResetMessageEvent() : super();
}

class SaveMessageEvent extends MessagerieEvent{
  var corps,
      destinataire,
      subject,
      piecesJointes;
  SaveMessageEvent({this.corps, this.destinataire, this.subject, this.piecesJointes}) : super();
}