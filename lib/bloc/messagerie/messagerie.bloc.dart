import 'package:LBA/bloc/messagerie/messagerie.event.dart';
import 'package:LBA/bloc/messagerie/messagerie.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/list_destinataire_facture.mock.dart';
import 'package:LBA/mocks/list_subjectMessagerie.mock.dart';
import 'package:LBA/mocks/message_envoyer.mock.dart';
import 'package:LBA/mocks/message_recus.mock.dart';
import 'package:LBA/models/destinataire.model.dart';
import 'package:LBA/models/messagerie.model.dart';
import 'package:LBA/models/subjectsMessagerie.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/messagerie.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class MessagerieBloc extends Bloc<MessagerieEvent, MessagerieStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  MessagerieService get messagerieService => GetIt.I<MessagerieService>();

  MessagerieBloc(MessagerieStateBloc initialState) : super(initialState);
  @override
  Stream<MessagerieStateBloc> mapEventToState(MessagerieEvent event) async* {
    if (event is LoadMessageEnvEvent) {
      yield MessagerieStateBloc(
        requestStateEn: StateStatus.LOADING,
        messagerieRc: state.messagerieRc,
        requestStateRc: state.requestStateRc,
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = Messagerie.fromJson(messageEnvoyerMock);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieEn: data,
              requestStateEn: StateStatus.LOADED,
              messagerieRc: state.messagerieRc,
              requestStateRc: state.requestStateRc,
              currentAction: event);
          yield messagerieStateBloc;
        } else {
          final data = await compteService.getMessagerieEnv(event.page);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieEn: data,
              requestStateEn: StateStatus.LOADED,
              messagerieRc: state.messagerieRc,
              requestStateRc: state.requestStateRc,
              currentAction: event);
          yield messagerieStateBloc;
        }
      } catch (e) {
        yield MessagerieStateBloc(
            errorMessage: e.toString(),
            requestStateEn: StateStatus.ERROR,
            messagerieRc: state.messagerieRc,
            requestStateRc: state.requestStateRc,
            currentAction: event);
      }
    } else if (event is LoadMoreMessageEnvEvent) {
      yield MessagerieStateBloc(
          requestStateLoadMore: StateStatus.LOADING,
          requestStateEn: state.requestStateEn,
          messagerieRc: state.messagerieRc,
          requestStateRc: state.requestStateRc,
          messagerieEn: state.messagerieEn);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = Messagerie.fromJson(messageEnvoyerMock);

          // var allData = state.messagerieEn;
          // allData.map.massages.addAll(data.map.massages);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieEn: data,
              requestStateLoadMore: StateStatus.LOADED,
              requestStateEn: state.requestStateEn,
              messagerieRc: state.messagerieRc,
              requestStateRc: state.requestStateRc,
              currentAction: event);
          yield messagerieStateBloc;
        } else {
          final data = await compteService.getMessagerieEnv(event.page);
          var allData = state.messagerieEn;
          allData.map.massages.addAll(data.map.massages);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieEn: allData,
              requestStateLoadMore: StateStatus.LOADED,
              requestStateEn: state.requestStateEn,
              messagerieRc: state.messagerieRc,
              requestStateRc: state.requestStateRc,
              currentAction: event);
          yield messagerieStateBloc;
        }
      } catch (e) {
        yield MessagerieStateBloc(
            errorMessage: e.toString(),
            requestStateLoadMore: StateStatus.ERROR,
            requestStateEn: state.requestStateEn,
            messagerieRc: state.messagerieRc,
            requestStateRc: state.requestStateRc,
            messagerieEn: state.messagerieEn,
            currentAction: event);
      }
    } else if (event is LoadMessageRcEvent) {
      yield MessagerieStateBloc(
        requestStateRc: StateStatus.LOADING,
        messagerieEn: state.messagerieEn,
        requestStateEn: state.requestStateEn,
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = Messagerie.fromJson(messageRecusMock);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieRc: data,
              requestStateRc: StateStatus.LOADED,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              currentAction: event);
          yield messagerieStateBloc;
        } else {
          final data = await compteService.getMessagerieRc(event.page);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              messagerieRc: data,
              requestStateRc: StateStatus.LOADED,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              currentAction: event);
          yield messagerieStateBloc;
        }
      } catch (e) {
        yield MessagerieStateBloc(
            errorMessage: e.toString(),
            requestStateRc: StateStatus.ERROR,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            currentAction: event);
      }
    } else if (event is LoadSubjectsMessagerieEvent) {
      yield MessagerieStateBloc(
          requestStateSub: StateStatus.LOADING,
          requestStateRc: state.requestStateRc,
          messagerieRc: state.messagerieRc,
          messagerieEn: state.messagerieEn,
          requestStateEn: state.requestStateEn,
          requestStateDes: state.requestStateDes,
          destinataire: state.destinataire);
      try {
        if (modeDemo) {
          final data = SubjectsMessagerie.fromJson(subjectMessagerieMock);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              subject: data,
              requestStateSub: StateStatus.LOADED,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              requestStateDes: state.requestStateDes,
              destinataire: state.destinataire,
              currentAction: event);
          yield messagerieStateBloc;
        } else {
          final data = await compteService.getSubjectsMessagerie();

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              subject: data,
              requestStateSub: StateStatus.LOADED,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              requestStateDes: state.requestStateDes,
              destinataire: state.destinataire,
              currentAction: event);
          yield messagerieStateBloc;
        }
      } catch (e) {
        yield MessagerieStateBloc(
            requestStateSub: StateStatus.ERROR,
            errorMessage: e.toString(),
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            requestStateDes: state.requestStateDes,
            destinataire: state.destinataire,
            currentAction: event);
      }
    } else if (event is LoadDestinataireMessagerieEvent) {
      yield MessagerieStateBloc(
        requestStateDes: StateStatus.LOADING,
        requestStateSub: state.requestStateSub,
        subject: state.subject,
        requestStateRc: state.requestStateRc,
        messagerieRc: state.messagerieRc,
        messagerieEn: state.messagerieEn,
        requestStateEn: state.requestStateEn,
      );
      try {
        if (modeDemo) {
          final data = Destinataire.fromJson(listDestinataireMock);

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              requestStateDes: StateStatus.LOADED,
              destinataire: data,
              requestStateSub: state.requestStateSub,
              subject: state.subject,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              currentAction: event);
          yield messagerieStateBloc;
        } else {
          final data = await compteService.getDestinataire();

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              requestStateDes: StateStatus.LOADED,
              destinataire: data,
              requestStateSub: state.requestStateSub,
              subject: state.subject,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
              currentAction: event);
          yield messagerieStateBloc;
        }
      } catch (e) {
        yield MessagerieStateBloc(
            requestStateDes: StateStatus.ERROR,
            requestStateSub: state.requestStateSub,
            subject: state.subject,
            errorMessage: e.toString(),
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            currentAction: event);
      }
    } else if (event is LoadDetailMessageEvent) {
      yield MessagerieStateBloc(
        requestStateDetail: StateStatus.LOADING,
        requestStateDes: state.requestStateDes,
        requestStateSub: state.requestStateSub,
        subject: state.subject,
        requestStateRc: state.requestStateRc,
        messagerieRc: state.messagerieRc,
        messagerieEn: state.messagerieEn,
        requestStateEn: state.requestStateEn,
      );
      try {
        final data = await compteService.getDetailMessage(event.id);

        MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
            requestStateDetail: StateStatus.LOADED,
            requestStateDes: state.requestStateDes,
            detailMessage: data,
            requestStateSub: state.requestStateSub,
            subject: state.subject,
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            currentAction: event);
        yield messagerieStateBloc;
      } catch (e) {
        yield MessagerieStateBloc(
            requestStateDetail: StateStatus.ERROR,
            requestStateDes: state.requestStateDes,
            requestStateSub: state.requestStateSub,
            subject: state.subject,
            errorMessage: e.toString(),
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            currentAction: event);
      }
    } else if (event is ResetMessageEvent) {
      yield MessagerieStateBloc(
          requestStateEn: StateStatus.NONE,
          requestStateRc: StateStatus.NONE,
          requestStateSub: StateStatus.NONE,
          requestStateDes: StateStatus.NONE,
          requestStateSave: StateStatus.NONE);
    } else if (event is SaveMessageEvent) {
      yield MessagerieStateBloc(
        requestStateDes: state.requestStateDes,
        requestStateSave: StateStatus.LOADING,
        requestStateSub: state.requestStateSub,
        subject: state.subject,
        destinataire: state.destinataire,
        requestStateRc: state.requestStateRc,
        messagerieRc: state.messagerieRc,
        messagerieEn: state.messagerieEn,
        requestStateEn: state.requestStateEn,
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
            requestStateDes: state.requestStateDes,
            requestStateSave: StateStatus.LOADED,
            requestStateSub: state.requestStateSub,
            subject: state.subject,
            destinataire: state.destinataire,
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
          );
          yield messagerieStateBloc;
        } else {
          final data = await messagerieService.saveMessageServiceMP(
              compteService.tokenState,
              compteService.currentCompte.identifiantInterne,
              event.corps,
              event.destinataire,
              event.subject,
              event.piecesJointes);

          print("data $data");

          if (data == 200) {
            MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              requestStateDes: state.requestStateDes,
              requestStateSave: StateStatus.LOADED,
              requestStateSub: state.requestStateSub,
              subject: state.subject,
              destinataire: state.destinataire,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
            );
            yield messagerieStateBloc;
          } else {
            MessagerieStateBloc messagerieStateBloc = MessagerieStateBloc(
              requestStateDes: state.requestStateDes,
              requestStateSave: StateStatus.ERROR,
              subject: state.subject,
              destinataire: state.destinataire,
              requestStateRc: state.requestStateRc,
              messagerieRc: state.messagerieRc,
              messagerieEn: state.messagerieEn,
              requestStateEn: state.requestStateEn,
            );
            yield messagerieStateBloc;
          }
        }
      } catch (e) {
        yield MessagerieStateBloc(
            requestStateDes: state.requestStateDes,
            requestStateSave: StateStatus.ERROR,
            subject: state.subject,
            destinataire: state.destinataire,
            requestStateRc: state.requestStateRc,
            messagerieRc: state.messagerieRc,
            messagerieEn: state.messagerieEn,
            requestStateEn: state.requestStateEn,
            errorMessage: e.toString());
      }
    }
  }
}
