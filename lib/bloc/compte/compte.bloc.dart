import 'package:LBA/bloc/cheque/cheque.bloc.dart';
import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/bloc/compte/compte.state.dart';
import 'package:LBA/bloc/dat/dat.bloc.dart';
import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/graph/graph.bloc.dart';
import 'package:LBA/bloc/graph/graph.event.dart';
import 'package:LBA/bloc/mouvement/mouvement.bloc.dart';
import 'package:LBA/bloc/mouvement/mouvement.event.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.bloc.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.event.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/comptePourConsultation.dart';
import 'package:LBA/mocks/compte_pour_transaction.dart';
import 'package:LBA/models/client.compte.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class CompteBloc extends Bloc<CompteEvent, CompteStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  MouvementBloc mouvementBloc;
  GraphBloc graphBloc;
  NomCompteBloc nomCompteBloc;
  DatBloc datBloc;
  ChequeBloc chequeBloc;

  CompteBloc(CompteStateBloc initialState, this.mouvementBloc, this.graphBloc,
      this.nomCompteBloc, this.datBloc, this.chequeBloc)
      : super(initialState);
  @override
  Stream<CompteStateBloc> mapEventToState(CompteEvent event) async* {
    if (event is LoadComptesEvent) {
      yield CompteStateBloc(
          requestState: StateStatus.LOADING, clients: state.clients);
      try {
        if (modeDemo) {
          ListCompte data = ListCompte.fromJson(compteConsultationMock);

          compteService.listCompte = data;
          compteService.currentCompte = data.comptes[0];

          compteService.listCompteChequier = data;
          compteService.currentCompteChequier = data.comptes[0];

          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: compteService.listCompte,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;
          if (state.requestState == StateStatus.LOADED) {
            nomCompteBloc
                .add(ChangeCurrentAccountEvent(compte: state.currentCompte));
            mouvementBloc.add(LoadMouvementEvent());
            graphBloc.add(LoadGraphEvent());
          }
        } else {
          ListCompte data = await compteService.getCompte("CL_ALL");

          compteService.listCompte = data;
          compteService.currentCompte = data.comptes[0];

          compteService.listCompteChequier = data;
          compteService.currentCompteChequier = data.comptes[0];

          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: compteService.listCompte,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;
          if (state.requestState == StateStatus.LOADED) {
            nomCompteBloc
                .add(ChangeCurrentAccountEvent(compte: state.currentCompte));
            mouvementBloc.add(LoadMouvementEvent());
            graphBloc.add(LoadGraphEvent());
          }
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ReLoadComptesEvent) {
      yield CompteStateBloc(
          requestState: StateStatus.LOADING, clients: state.clients);
      try {
        if (modeDemo) {
          ListCompte data = ListCompte.fromJson(compteConsultationMock);

          compteService.listCompte = data;
          compteService.currentCompte = data.comptes[0];

          compteService.listCompteChequier = data;
          compteService.currentCompteChequier = data.comptes[0];

          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: compteService.listCompte,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;
        } else {
          ListCompte data = await compteService.getCompte("CL_ALL");

          compteService.listCompte = data;
          compteService.currentCompte = data.comptes[0];

          compteService.listCompteChequier = data;
          compteService.currentCompteChequier = data.comptes[0];

          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: compteService.listCompte,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadComptesChequeEvent) {
      print("in");
      yield CompteStateBloc(
          requestState: state.requestState,
          clients: state.clients,
          currentCompte: state.currentCompte,
          requestStateCheque: StateStatus.LOADING);
      try {
        if (modeDemo) {
          ListCompte data = ListCompte.fromJson(comptePourTransactionMock);

          if (data.comptes.length == 0) {
            CompteStateBloc compteStateBloc = CompteStateBloc(
                listeCompteCheque: data,
                requestState: state.requestState,
                requestStateCheque: StateStatus.LOADED,
                clients: state.clients,
                currentCompte: state.currentCompte,
                currentAction: event);

            yield compteStateBloc;
          } else {
            compteService.listCompteChequier = data;
            compteService.currentCompteChequier = data.comptes[0];

            CompteStateBloc compteStateBloc = CompteStateBloc(
                listeCompteCheque: compteService.listCompteChequier,
                requestState: state.requestState,
                requestStateCheque: StateStatus.LOADED,
                clients: state.clients,
                currentCompte: state.currentCompte,
                currentAction: event);

            yield compteStateBloc;
          }
        } else {
          print("in bloc before call");
          ListCompte data = await compteService.getComptesChequie();
          print("in bloc after call");

          if (data.comptes.length == 0) {
            CompteStateBloc compteStateBloc = CompteStateBloc(
                listeCompteCheque: data,
                requestState: state.requestState,
                requestStateCheque: StateStatus.LOADED,
                clients: state.clients,
                currentCompte: state.currentCompte,
                currentAction: event);

            yield compteStateBloc;
          } else {
            compteService.listCompteChequier = data;
            compteService.currentCompteChequier = data.comptes[0];

            CompteStateBloc compteStateBloc = CompteStateBloc(
                listeCompteCheque: compteService.listCompteChequier,
                requestState: state.requestState,
                requestStateCheque: StateStatus.LOADED,
                clients: state.clients,
                currentCompte: state.currentCompte,
                currentAction: event);

            yield compteStateBloc;
          }

          // if (state.listeCompteCheque.comptes.length > 0)
          //   chequeBloc.add(LoadChequeEvent());
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestState: state.requestState,
            requestStateCheque: StateStatus.ERROR,
            clients: state.clients,
            currentCompte: state.currentCompte,
            currentAction: event);
      }
    } else if (event is LoadComptesVirEvent) {
      yield CompteStateBloc(
          requestStateTransaction: StateStatus.LOADING,
          clients: state.clients,
          currentCompte: state.currentCompte,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          ListCompte data = ListCompte.fromJson(compteConsultationMock);
          CompteStateBloc compteStateBloc = CompteStateBloc(
              listeCompteTransaction: data,
              requestState: state.requestState,
              requestStateTransaction: StateStatus.LOADED,
              clients: state.clients,
              currentCompte: state.currentCompte,
              currentAction: event);
          yield compteStateBloc;
        } else {
          ListCompte data = await compteService.getCompte("CL_RB");
          CompteStateBloc compteStateBloc = CompteStateBloc(
              listeCompteTransaction: data,
              requestState: state.requestState,
              requestStateTransaction: StateStatus.LOADED,
              clients: state.clients,
              currentCompte: state.currentCompte,
              currentAction: event);
          yield compteStateBloc;
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestStateTransaction: StateStatus.ERROR,
            requestState: state.requestState,
            clients: state.clients,
            currentCompte: state.currentCompte,
            currentAction: event);
      }
    } else if (event is LoadComptesDatEvent) {
      yield CompteStateBloc(
          requestStateVir: StateStatus.LOADING,
          clients: state.clients,
          currentCompte: state.currentCompte,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          ListCompte data = ListCompte.fromJson(compteConsultationMock);

          CompteStateBloc compteStateBloc = CompteStateBloc(
              listeCompteVir: data,
              requestState: state.requestState,
              requestStateVir: StateStatus.LOADED,
              clients: state.clients,
              currentCompte: state.currentCompte,
              currentAction: event);
          yield compteStateBloc;
          if (state.listeCompteVir.comptes.length > 0)
            datBloc.add(LoadDatEvent());
        } else {
          ListCompte data = await compteService.getComptesChequie();
          CompteStateBloc compteStateBloc = CompteStateBloc(
              listeCompteVir: data,
              requestState: state.requestState,
              requestStateVir: StateStatus.LOADED,
              clients: state.clients,
              currentCompte: state.currentCompte,
              currentAction: event);
          yield compteStateBloc;
          if (state.listeCompteVir.comptes.length > 0)
            datBloc.add(LoadDatEvent());
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestStateVir: StateStatus.ERROR,
            requestState: state.requestState,
            clients: state.clients,
            currentCompte: state.currentCompte,
            currentAction: event);
      }
    } else if (event is ChangeCompteEvent) {
      yield CompteStateBloc(
          requestState: StateStatus.LOADING, clients: state.clients);
      try {
        if (modeDemo) {
          compteService.currentCompte = event.payload;
          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: state.clients,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;
        } else {
          compteService.currentCompte = event.payload;
          CompteStateBloc compteStateBloc = CompteStateBloc(
              clients: state.clients,
              currentCompte: compteService.currentCompte,
              requestState: StateStatus.LOADED,
              currentAction: event);

          yield compteStateBloc;

          mouvementBloc.add(LoadMouvementEvent());
          graphBloc.add(LoadGraphEvent());
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangePasswordEvent) {
      yield CompteStateBloc(
          clients: state.clients,
          requestState: StateStatus.LOADED,
          currentCompte: state.currentCompte,
          requestStatePsd: StateStatus.LOADING);
      try {
        final data = await compteService.changePsd(compteService.tokenState,
            event.oldPsd, event.newPsd, event.confPsd);

        if (data["success"]) {
          CompteStateBloc compteStateBloc = CompteStateBloc(
            requestState: StateStatus.LOADED,
            requestStatePsd: StateStatus.LOADED,
            currentAction: event,
            clients: state.clients,
            currentCompte: state.currentCompte,
            res: data,
          );
          yield compteStateBloc;
        } else {
          CompteStateBloc compteStateBloc = CompteStateBloc(
              requestState: StateStatus.LOADED,
              requestStatePsd: StateStatus.ERROR,
              currentAction: event,
              clients: state.clients,
              currentCompte: state.currentCompte,
              errorMessage: data["message"]);
          yield compteStateBloc;
        }
      } catch (e) {
        yield CompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.LOADED,
            requestStatePsd: StateStatus.ERROR,
            clients: state.clients,
            currentCompte: state.currentCompte,
            currentAction: event);
      }
    } else if (event is ResetAccountEvent) {
      yield CompteStateBloc(
          requestState: StateStatus.NONE,
          clients: null,
          currentCompte: null,
          requestStateCheque: StateStatus.NONE,
          requestStatePsd: StateStatus.NONE,
          requestStateTransaction: StateStatus.NONE,
          requestStateVir: StateStatus.NONE);
      nomCompteBloc.add(ResetCurrentAccountEvent());
      mouvementBloc.add(ResetMouvementEvent());
      graphBloc.add(InitGraphEvent());
      print("logout from bloc");
    }
  }
}
