import 'package:LBA/bloc/compte/compte.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/client.compte.dart';
import 'package:LBA/models/compte.model.dart';

class CompteStateBloc {
  StateStatus requestState;
  StateStatus requestStatePsd;
  StateStatus requestStateVir;
  StateStatus requestStateCheque;
  StateStatus requestStateTransaction;
  String errorMessage;
  CompteEvent currentAction;
  ListCompte clients;
  ListCompte listeCompteVir;
  ListCompte listeCompteCheque;
  ListCompte listeCompteTransaction;
  Compte currentCompte;
  var res;

  Compte get currentCompteGetter => currentCompte;
  set currentCompteSetter(Compte v) {
    currentCompte = v;
  }

  CompteStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStatePsd,
      this.requestStateVir,
      this.currentCompte,
      this.listeCompteCheque,
      this.requestStateCheque,
      this.listeCompteTransaction,
      this.requestStateTransaction,
      this.res,
      this.listeCompteVir,
      this.clients});

  CompteStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStatePsd = StateStatus.NONE;
    this.requestStateVir = StateStatus.NONE;
    this.requestStateCheque = StateStatus.NONE;
    this.requestStateTransaction = StateStatus.NONE;
    this.errorMessage = "";
    this.clients = null;
    this.listeCompteCheque = null;
    this.listeCompteVir = null;
    this.listeCompteTransaction = null;
    this.currentCompte = Compte();
    this.res = null;
  }
}
