import 'package:LBA/models/compte.model.dart';

abstract class CompteEvent<T> {
  T payload;
  CompteEvent({this.payload});
}

class LoadComptesEvent extends CompteEvent {
  LoadComptesEvent() : super();
}

class ReLoadComptesEvent extends CompteEvent {
  ReLoadComptesEvent() : super();
}

class LoadComptesChequeEvent extends CompteEvent {
  LoadComptesChequeEvent() : super();
}

class LoadComptesVirEvent extends CompteEvent {
  LoadComptesVirEvent() : super();
}

class LoadComptesDatEvent extends CompteEvent {
  LoadComptesDatEvent() : super();
}

class ChangeCompteEvent extends CompteEvent<Compte> {
  ChangeCompteEvent(Compte payload) : super(payload: payload);
}

class ChangePasswordEvent extends CompteEvent<Compte> {
  String newPsd, oldPsd, confPsd;
  ChangePasswordEvent(this.oldPsd, this.newPsd, this.confPsd) : super();
}

class ResetAccountEvent extends CompteEvent {
  ResetAccountEvent() : super();
}
