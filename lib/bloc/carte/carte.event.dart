import 'package:LBA/models/carte.model.dart';

abstract class CarteEvent<T> {
  T payload;
  CarteEvent({this.payload});
}

class LoadCarteEvent extends CarteEvent {
  LoadCarteEvent() : super();
}

class LoadTypeCarteEvent extends CarteEvent {
  LoadTypeCarteEvent() : super();
}

class ChangeCurrentCartEvent extends CarteEvent {
  ChangeCurrentCartEvent(ListCarteInstance payload) : super(payload: payload);
}

class LoadCardPlafond extends CarteEvent {
  LoadCardPlafond() : super();
}

class LoadUpdatePlafond extends CarteEvent {
  var plafondCode;
  LoadUpdatePlafond(this.plafondCode) : super();
}

class LoadMotifOpposition extends CarteEvent {
  LoadMotifOpposition() : super();
}

class LoadOpposition extends CarteEvent {
  String motif;
  LoadOpposition(this.motif) : super();
}

class LoadSigneOpposition extends CarteEvent {
  String psd;
  LoadSigneOpposition(this.psd) : super();
}

class LoadRechargeCarte extends CarteEvent {
  String montant, numeroCompte;
  LoadRechargeCarte(this.montant, this.numeroCompte) : super();
}

class LoadSigneRechargeCarte extends CarteEvent {
  String psd;
  LoadSigneRechargeCarte(this.psd) : super();
}

class LoadRecalculePin extends CarteEvent {
  LoadRecalculePin() : super();
}

class LoadSigneRecalculePin extends CarteEvent {
  String psd;
  LoadSigneRecalculePin(this.psd) : super();
}

class LoadGenereCvv extends CarteEvent {
  LoadGenereCvv() : super();
}

class LoadSigneGenereCvv extends CarteEvent {
  String psd;
  LoadSigneGenereCvv(this.psd) : super();
}

class ResetCarteEvent extends CarteEvent {
  ResetCarteEvent() : super();
}

class SaveCommandeCarteEvent extends CarteEvent{
  var identifiantInterne,
      intituleCompte,
      paysBeneficiaire,
      telephoneBeneficiaire,
      typeCatreLibelle,
      typeCarteCode;

  SaveCommandeCarteEvent({
      this.identifiantInterne,
      this.intituleCompte,
      this.paysBeneficiaire,
      this.telephoneBeneficiaire,
      this.typeCatreLibelle,
      this.typeCarteCode});
}

class SigneCommandeCarteEvent extends CarteEvent {
  String password;
  SigneCommandeCarteEvent({this.password}) : super();
}

class BloquerCarteEvent extends CarteEvent{
  String typeBloque;

  BloquerCarteEvent({this.typeBloque});
}

class CardStatusEvent extends CarteEvent{

  CardStatusEvent() : super();
}

