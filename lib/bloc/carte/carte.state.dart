import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/carte.model.dart';
import 'package:LBA/models/mouvementCarte.model.dart';
import 'package:LBA/models/typeCarte.model.dart';

class CarteStateBloc {
  StateStatus requestState;
  StateStatus typeCarteRequestState;
  StateStatus requestStatePl;
  StateStatus requestStateUp;
  StateStatus requestStateSave;
  StateStatus requestStateSigne;
  StateStatus requestStateBloque;
  StateStatus requestStateMotif;
  StateStatus requestStateOpp;
  StateStatus requestStateOppSigne;
  StateStatus requestCardStatus;
  String errorMessage;
  CarteEvent currentAction;
  Carte carte;
  TypeCarte typeCarte;
  ListCarteInstance currentCarte;
  MouvementCarte mouvementCarte;
  var plafond;
  var resUpdatePlafond;
  var res;
  var resBloque;
  var cardStatus;
  var motifOpposition;
  var opposition;
  var signeRes;

  CarteStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.carte,
    this.currentCarte,
    this.plafond,
    this.requestStatePl,
    this.requestStateUp,
    this.resUpdatePlafond,
    this.mouvementCarte,
    this.motifOpposition,
    this.requestStateMotif,
    this.requestCardStatus,
    this.opposition,
    this.requestStateOpp,
    this.signeRes,
    this.requestStateOppSigne,
    this.res,
    this.resBloque,
    this.requestStateSigne,
    this.requestStateBloque,
    this.requestStateSave,
    this.typeCarte,
    this.typeCarteRequestState,
    this.cardStatus
  });

  CarteStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.typeCarteRequestState = StateStatus.NONE;
    this.requestStatePl = StateStatus.NONE;
    this.requestStateUp = StateStatus.NONE;
    this.requestStateSave = StateStatus.NONE;
    this.requestStateSigne = StateStatus.NONE;
    this.requestStateBloque = StateStatus.NONE;
    this.requestStateMotif = StateStatus.NONE;
    this.requestStateOpp = StateStatus.NONE;
    this.requestStateOppSigne = StateStatus.NONE;
    this.requestCardStatus = StateStatus.NONE;
    this.errorMessage = "";
    this.carte = Carte();
    this.typeCarte = TypeCarte();
    this.mouvementCarte = MouvementCarte();
    this.currentCarte = ListCarteInstance();
  }
}
