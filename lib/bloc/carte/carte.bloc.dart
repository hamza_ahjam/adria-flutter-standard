import 'package:LBA/bloc/carte/carte.event.dart';
import 'package:LBA/bloc/carte/carte.state.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.bloc.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.event.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/add_card.dart';
import 'package:LBA/mocks/bloquer_carte.mock.dart';
import 'package:LBA/mocks/list_card.dart';
import 'package:LBA/mocks/opposition_card.dart';
import 'package:LBA/mocks/plafonds_carte.mock.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/models/carte.model.dart';
import 'package:LBA/models/typeCarte.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/carte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class CarteBloc extends Bloc<CarteEvent, CarteStateBloc> {
  CarteService get carteService => GetIt.I<CarteService>();
  CompteService get compteService => GetIt.I<CompteService>();

  MouvementCarteBloc mouvementCarteBloc;

  CarteBloc(CarteStateBloc initialState, this.mouvementCarteBloc)
      : super(initialState);
  @override
  Stream<CarteStateBloc> mapEventToState(CarteEvent event) async* {
    if (event is LoadCarteEvent) {
      yield CarteStateBloc(
          requestState: StateStatus.LOADING, carte: state.carte);
      try {
        // print("object");
        if (modeDemo) {
          Carte data = Carte.fromJson(cardMock);

          CarteStateBloc carteStateBloc = CarteStateBloc(
              carte: data,
              currentCarte: data.map.listCarteInstance[0],
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield carteStateBloc;
          carteService.currentCarte = state.currentCarte;

          mouvementCarteBloc.add(LoadCarteOpperationEvent());
          mouvementCarteBloc.add(LoadCartePlafondEvent());
        } else {
          Carte data = await carteService.getCarte(compteService.tokenState);
          if (data.map.listCarteInstance.length == 0) {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                carte: data,
                requestState: StateStatus.LOADED,
                currentAction: event);
            yield carteStateBloc;
          } else {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                carte: data,
                currentCarte: data.map.listCarteInstance[0],
                requestState: StateStatus.LOADED,
                currentAction: event);
            yield carteStateBloc;
            carteService.currentCarte = state.currentCarte;

            mouvementCarteBloc.add(LoadCartePlafondEvent());
            mouvementCarteBloc.add(LoadCarteOpperationEvent());
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadCardPlafond) {
      yield CarteStateBloc(
          requestState: state.requestState,
          requestStatePl: StateStatus.LOADING,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          var data = plafondsMock;

          CarteStateBloc carteStateBloc = CarteStateBloc(
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStatePl: StateStatus.LOADED,
              plafond: data["Map"],
              currentAction: event);
          yield carteStateBloc;
        } else {
          var data = await carteService.getPlafonds(
            compteService.tokenState,
            carteService.currentCarte.numero,
            carteService.currentCarte.rib,
          );
          if (data["Map"] != null) {
            if (data["Map"]["success"]) {
              CarteStateBloc carteStateBloc = CarteStateBloc(
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStatePl: StateStatus.LOADED,
                  plafond: data["Map"],
                  currentAction: event);
              yield carteStateBloc;
            } else {
              CarteStateBloc carteStateBloc = CarteStateBloc(
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStatePl: StateStatus.ERROR,
                  plafond: data["message"],
                  currentAction: event);
              yield carteStateBloc;
            }
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeCurrentCartEvent) {
      yield CarteStateBloc(
          requestState: StateStatus.LOADING, carte: state.carte);
      try {
        carteService.currentCarte = event.payload;

        CarteStateBloc carteStateBloc = CarteStateBloc(
            carte: state.carte,
            currentCarte: carteService.currentCarte,
            requestState: StateStatus.LOADED,
            currentAction: event);

        yield carteStateBloc;
        mouvementCarteBloc.add(LoadCartePlafondEvent());
        mouvementCarteBloc.add(LoadCarteOpperationEvent());
      } catch (e) {
        yield CarteStateBloc(
            errorMessage: e.message.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadUpdatePlafond) {
      yield CarteStateBloc(
          requestStateUp: StateStatus.LOADING,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          var data = plafondsCardMock;

          CarteStateBloc carteStateBloc = CarteStateBloc(
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: StateStatus.LOADED,
              resUpdatePlafond: data,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
          yield carteStateBloc;
        } else {
          var data = await carteService.getDemandeUpdatePlafonds(
            compteService.tokenState,
            carteService.currentCarte.numero,
            carteService.currentCarte.rib,
            event.plafondCode,
          );
          if (data["Map"] != null) {
            if (data["Map"]["success"]) {
              CarteStateBloc carteStateBloc = CarteStateBloc(
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: StateStatus.LOADED,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
              yield carteStateBloc;
            } else {
              CarteStateBloc carteStateBloc = CarteStateBloc(
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  requestStateUp: StateStatus.ERROR,
                  resUpdatePlafond: data,
                  errorMessage: data["Map"]["message"],
                  currentAction: event);
              yield carteStateBloc;
            }
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadMotifOpposition) {
      yield CarteStateBloc(
          requestStateMotif: StateStatus.LOADING,
          resUpdatePlafond: state.resUpdatePlafond,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          var data = oppositionCardMock;

          CarteStateBloc carteStateBloc = CarteStateBloc(
              requestStateMotif: StateStatus.LOADED,
              motifOpposition: data,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: state.resUpdatePlafond,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
          yield carteStateBloc;
        } else {
          var data =
              await carteService.getMotifOpposition(compteService.tokenState);
          if (data["Map"] != null) {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                requestStateMotif: StateStatus.LOADED,
                motifOpposition: data,
                carte: state.carte,
                requestState: state.requestState,
                currentCarte: state.currentCarte,
                requestStateUp: state.requestStateUp,
                resUpdatePlafond: state.resUpdatePlafond,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentAction: event);
            yield carteStateBloc;
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            requestStateMotif: StateStatus.ERROR,
            resUpdatePlafond: state.resUpdatePlafond,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadOpposition) {
      yield CarteStateBloc(
          requestStateOpp: StateStatus.LOADING,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          var data = saveMock;

          CarteStateBloc carteStateBloc = CarteStateBloc(
              signeRes: data,
              requestStateOpp: StateStatus.LOADED,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: state.resUpdatePlafond,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
          yield carteStateBloc;

          // print(data);
        } else {
          var data = await carteService.oppositionCarte(
              compteService.tokenState,
              "100",
              "1",
              "2222",
              compteService.tokenState.getUserDetails()["fullName"],
              event.motif,
              compteService.tokenState.getUserDetails()["username"]);
          if (data["Map"] != null) {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                signeRes: data,
                requestStateOpp: StateStatus.LOADED,
                motifOpposition: state.motifOpposition,
                requestStateMotif: state.requestStateMotif,
                carte: state.carte,
                requestState: state.requestState,
                currentCarte: state.currentCarte,
                requestStateUp: state.requestStateUp,
                resUpdatePlafond: state.resUpdatePlafond,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentAction: event);
            yield carteStateBloc;
          }
          // print(data);
        }
      } catch (e) {
        yield CarteStateBloc(
            resUpdatePlafond: state.resUpdatePlafond,
            requestStateOpp: StateStatus.ERROR,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadSigneOpposition) {
      yield CarteStateBloc(
          signeRes: state.signeRes,
          requestStateOppSigne: StateStatus.LOADING,
          resUpdatePlafond: state.resUpdatePlafond,
          requestStateOpp: state.requestStateOpp,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      // print(state.signeRes["Map"]["taskId"]);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = signeMock["Map"];

          yield CarteStateBloc(
              requestStateOppSigne: StateStatus.LOADED,
              signeRes: data,
              requestStateOpp: state.requestStateOpp,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: data,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
        } else {
          var data = await carteService.signeOppositionCarte(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.signeRes["Map"]["id"],
              state.signeRes["Map"]["taskId"],
              event.psd);

          // print(data);

          if (data != null) {
            if (data["success"]) {
              yield CarteStateBloc(
                  requestStateOppSigne: StateStatus.LOADED,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            } else {
              yield CarteStateBloc(
                  errorMessage: data["message"],
                  requestStateOppSigne: StateStatus.ERROR,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            }
          }
          // print(data);
          // print(state.requestStateOppSigne.toString());
        }
      } catch (e) {
        yield CarteStateBloc(
            requestStateOppSigne: StateStatus.ERROR,
            requestStateOpp: state.requestStateOpp,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadRechargeCarte) {
      yield CarteStateBloc(
          requestStateOpp: StateStatus.LOADING,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = saveMock["Map"];

          CarteStateBloc carteStateBloc = CarteStateBloc(
              signeRes: data,
              requestStateOpp: StateStatus.LOADED,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: state.resUpdatePlafond,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
          yield carteStateBloc;
        } else {
          var data = await carteService.rechargeCarte(
              compteService.tokenState,
              "100",
              "2222",
              compteService.tokenState.getUserDetails()["fullName"],
              event.montant,
              event.numeroCompte,
              compteService.tokenState.getUserDetails()["username"]);
          if (data != null) {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                signeRes: data,
                requestStateOpp: StateStatus.LOADED,
                motifOpposition: state.motifOpposition,
                requestStateMotif: state.requestStateMotif,
                carte: state.carte,
                requestState: state.requestState,
                currentCarte: state.currentCarte,
                requestStateUp: state.requestStateUp,
                resUpdatePlafond: state.resUpdatePlafond,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentAction: event);
            yield carteStateBloc;
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            resUpdatePlafond: state.resUpdatePlafond,
            requestStateOpp: StateStatus.ERROR,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadSigneRechargeCarte) {
      yield CarteStateBloc(
          signeRes: state.signeRes,
          requestStateOppSigne: StateStatus.LOADING,
          resUpdatePlafond: state.resUpdatePlafond,
          requestStateOpp: state.requestStateOpp,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      // print(state.signeRes["taskId"]);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = signeMock["Map"];

          yield CarteStateBloc(
              requestStateOppSigne: StateStatus.LOADED,
              signeRes: data,
              requestStateOpp: state.requestStateOpp,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: data,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
        } else {
          var data = await carteService.signeRechargeCarte(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.signeRes["id"],
              state.signeRes["taskId"],
              event.psd);

          // print(data);

          if (data != null) {
            if (data["success"]) {
              yield CarteStateBloc(
                  requestStateOppSigne: StateStatus.LOADED,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            } else {
              yield CarteStateBloc(
                  errorMessage: data["message"],
                  requestStateOppSigne: StateStatus.ERROR,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            }
          }
        }
        // print(data);
        // print(state.requestStateOppSigne.toString());
      } catch (e) {
        yield CarteStateBloc(
            requestStateOppSigne: StateStatus.ERROR,
            requestStateOpp: state.requestStateOpp,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadRecalculePin) {
      yield CarteStateBloc(
          requestStateOpp: StateStatus.LOADING,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = saveMock["Map"];

          CarteStateBloc carteStateBloc = CarteStateBloc(
              signeRes: data,
              requestStateOpp: StateStatus.LOADED,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: state.resUpdatePlafond,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
          yield carteStateBloc;
        } else {
          var data = await carteService.recalculePin(
            compteService.tokenState,
            "100",
            compteService.currentCompte.intitule,
            compteService.tokenState.getUserDetails()["fullName"],
            compteService.currentCompte.identifiantInterne,
            compteService.tokenState.getUserDetails()["username"],
          );
          if (data != null) {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                signeRes: data,
                requestStateOpp: StateStatus.LOADED,
                motifOpposition: state.motifOpposition,
                requestStateMotif: state.requestStateMotif,
                carte: state.carte,
                requestState: state.requestState,
                currentCarte: state.currentCarte,
                requestStateUp: state.requestStateUp,
                resUpdatePlafond: state.resUpdatePlafond,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentAction: event);
            yield carteStateBloc;
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            resUpdatePlafond: state.resUpdatePlafond,
            requestStateOpp: StateStatus.ERROR,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadSigneRecalculePin) {
      yield CarteStateBloc(
          signeRes: state.signeRes,
          requestStateOppSigne: StateStatus.LOADING,
          resUpdatePlafond: state.resUpdatePlafond,
          requestStateOpp: state.requestStateOpp,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      // print(state.signeRes["taskId"]);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = signeMock["Map"];

          yield CarteStateBloc(
              requestStateOppSigne: StateStatus.LOADED,
              signeRes: data,
              requestStateOpp: state.requestStateOpp,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: data,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
        } else {
          var data = await carteService.signeRecalculPin(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.signeRes["id"],
              state.signeRes["taskId"],
              event.psd);

          // print(data);

          if (data != null) {
            if (data["success"]) {
              yield CarteStateBloc(
                  requestStateOppSigne: StateStatus.LOADED,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            } else {
              yield CarteStateBloc(
                  errorMessage: data["message"],
                  requestStateOppSigne: StateStatus.ERROR,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            }
          }
          // print(data);
          // print(state.requestStateOppSigne.toString());
        }
      } catch (e) {
        yield CarteStateBloc(
            requestStateOppSigne: StateStatus.ERROR,
            requestStateOpp: state.requestStateOpp,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadGenereCvv) {
      yield CarteStateBloc(
          requestStateOpp: StateStatus.LOADING,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = saveMock["Map"];

          yield CarteStateBloc(
              signeRes: data,
              requestStateOpp: StateStatus.LOADED,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: state.resUpdatePlafond,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
        } else {
          var data = await carteService.generercvv(
            compteService.tokenState,
          );
          if (data != null) {
            if (data["success"])
              yield CarteStateBloc(
                  signeRes: data,
                  requestStateOpp: StateStatus.LOADED,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: state.resUpdatePlafond,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            else
              yield CarteStateBloc(
                  resUpdatePlafond: state.resUpdatePlafond,
                  requestStateOpp: StateStatus.ERROR,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  errorMessage: data["message"],
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  requestStateUp: state.requestStateUp,
                  currentAction: event);
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            resUpdatePlafond: state.resUpdatePlafond,
            requestStateOpp: StateStatus.ERROR,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is LoadSigneGenereCvv) {
      yield CarteStateBloc(
          signeRes: state.signeRes,
          requestStateOppSigne: StateStatus.LOADING,
          resUpdatePlafond: state.resUpdatePlafond,
          requestStateOpp: state.requestStateOpp,
          motifOpposition: state.motifOpposition,
          requestStateMotif: state.requestStateMotif,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      // print(state.signeRes["taskId"]);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = signeMock["Map"];

          yield CarteStateBloc(
              requestStateOppSigne: StateStatus.LOADED,
              signeRes: data,
              requestStateOpp: state.requestStateOpp,
              motifOpposition: state.motifOpposition,
              requestStateMotif: state.requestStateMotif,
              carte: state.carte,
              requestState: state.requestState,
              currentCarte: state.currentCarte,
              requestStateUp: state.requestStateUp,
              resUpdatePlafond: data,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentAction: event);
        } else {
          var data = await carteService.signeRecalculPin(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.signeRes["id"],
              state.signeRes["taskId"],
              event.psd);

          // print(data);

          if (data != null) {
            if (data["success"]) {
              yield CarteStateBloc(
                  requestStateOppSigne: StateStatus.LOADED,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            } else {
              yield CarteStateBloc(
                  errorMessage: data["message"],
                  requestStateOppSigne: StateStatus.ERROR,
                  signeRes: data,
                  requestStateOpp: state.requestStateOpp,
                  motifOpposition: state.motifOpposition,
                  requestStateMotif: state.requestStateMotif,
                  carte: state.carte,
                  requestState: state.requestState,
                  currentCarte: state.currentCarte,
                  requestStateUp: state.requestStateUp,
                  resUpdatePlafond: data,
                  requestStatePl: state.requestStatePl,
                  plafond: state.plafond,
                  currentAction: event);
            }
          }
          // print(data);
          // print(state.requestStateOppSigne.toString());
        }
      } catch (e) {
        yield CarteStateBloc(
            requestStateOppSigne: StateStatus.ERROR,
            requestStateOpp: state.requestStateOpp,
            motifOpposition: state.motifOpposition,
            requestStateMotif: state.requestStateMotif,
            errorMessage: e.toString(),
            carte: state.carte,
            requestState: state.requestState,
            currentCarte: state.currentCarte,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            requestStateUp: state.requestStateUp,
            currentAction: event);
      }
    } else if (event is ResetCarteEvent) {
      yield CarteStateBloc(
        requestState: StateStatus.NONE,
        typeCarteRequestState: StateStatus.NONE,
        requestCardStatus: StateStatus.NONE,
        requestStateBloque: StateStatus.NONE,
        requestStateMotif: StateStatus.NONE,
        requestStateUp: StateStatus.NONE,
        requestStateOpp: StateStatus.NONE,
        requestStatePl: StateStatus.NONE
      );
      mouvementCarteBloc.add(ResetCarteOpperationEvent());
    } else if (event is LoadTypeCarteEvent) {
      yield CarteStateBloc(
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          typeCarteRequestState: StateStatus.LOADING,
          typeCarte: state.typeCarte,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          TypeCarte data = TypeCarte.fromJson(typeCardMock);

          print('server response : ${data.map.success}');
          CarteStateBloc carteStateBloc = CarteStateBloc(
              typeCarte: data,
              requestStateUp: state.requestStateUp,
              requestState: state.requestState,
              requestStatePl: state.requestStatePl,
              typeCarteRequestState: StateStatus.LOADED,
              currentAction: event,
              plafond: state.plafond,
              currentCarte: state.currentCarte,
              carte: state.carte);
          yield carteStateBloc;
        } else {
          TypeCarte data =
              await carteService.getTypeCarte(compteService.tokenState);
          if (data.map.success) {
            print('server response : ${data.map.success}');
            CarteStateBloc carteStateBloc = CarteStateBloc(
                typeCarte: data,
                requestStateUp: state.requestStateUp,
                requestState: state.requestState,
                requestStatePl: state.requestStatePl,
                typeCarteRequestState: StateStatus.LOADED,
                currentAction: event,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          } else {
            print('server response : ${data.map.success}');
            CarteStateBloc(
                typeCarteRequestState: StateStatus.ERROR,
                requestStateUp: state.requestStateUp,
                requestState: state.requestState,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            errorMessage: e.toString(),
            typeCarteRequestState: StateStatus.ERROR,
            requestStateUp: state.requestStateUp,
            requestState: state.requestState,
            requestStatePl: state.requestStatePl,
            currentAction: event,
            plafond: state.plafond,
            currentCarte: state.currentCarte,
            carte: state.carte);
      }
    } else if (event is SaveCommandeCarteEvent) {
      yield CarteStateBloc(
          requestStateSave: StateStatus.LOADING,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          CarteStateBloc carteStateBloc = CarteStateBloc(
              res: data,
              requestState: state.requestState,
              requestStateUp: state.requestStateUp,
              requestStatePl: state.requestStatePl,
              currentAction: event,
              requestStateSave: StateStatus.LOADED,
              plafond: state.plafond,
              currentCarte: state.currentCarte,
              carte: state.carte);
          yield carteStateBloc;
        } else {
          final data = await carteService.saveCommandeCarte(
            compteService.tokenState,
            event.identifiantInterne,
            compteService.tokenState.getUserDetails()["email"],
            event.intituleCompte,
            compteService.tokenState.getUserDetails()["fullName"],
            event.paysBeneficiaire,
            compteService.tokenState.getUserDetails()['gsm'],
            event.typeCatreLibelle,
            event.typeCarteCode,
            compteService.tokenState.getUserDetails()["username"],
          );

          if (data["success"]) {
            //print(data);
            CarteStateBloc carteStateBloc = CarteStateBloc(
                res: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestStateSave: StateStatus.LOADED,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          } else {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                res: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestStateSave: StateStatus.ERROR,
                errorMessage: data["message"],
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          }
        }
        // print("data from bloc *** $data");
      } catch (e) {
        // print("bloc err $e");
        yield CarteStateBloc(
            requestState: state.requestState,
            requestStateUp: state.requestStateUp,
            requestStatePl: state.requestStatePl,
            errorMessage: e.toString(),
            requestStateSave: StateStatus.ERROR,
            currentAction: event,
            plafond: state.plafond,
            currentCarte: state.currentCarte,
            carte: state.carte);
      }
    } else if (event is SigneCommandeCarteEvent) {
      yield CarteStateBloc(
          res: state.res,
          requestStateSigne: StateStatus.LOADING,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = signeMock["Map"];

          CarteStateBloc virementStateBloc = CarteStateBloc(
              res: data,
              requestStateSigne: StateStatus.LOADED,
              requestStateUp: state.requestStateUp,
              requestState: state.requestState,
              requestStatePl: state.requestStatePl,
              plafond: state.plafond,
              currentCarte: state.currentCarte,
              carte: state.carte);
          yield virementStateBloc;
        } else {
          final data = await carteService.signeCommandeCarte(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.password);
          if (data["success"]) {
            CarteStateBloc virementStateBloc = CarteStateBloc(
                res: data,
                requestStateSigne: StateStatus.LOADED,
                requestStateUp: state.requestStateUp,
                requestState: state.requestState,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield virementStateBloc;
          } else {
            CarteStateBloc virementStateBloc = CarteStateBloc(
                res: data,
                requestStateSigne: StateStatus.ERROR,
                requestStateUp: state.requestStateUp,
                requestState: state.requestState,
                requestStatePl: state.requestStatePl,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield CarteStateBloc(
            requestStateSigne: StateStatus.ERROR,
            requestStateUp: state.requestStateUp,
            requestState: state.requestState,
            requestStatePl: state.requestStatePl,
            plafond: state.plafond,
            currentCarte: state.currentCarte,
            carte: state.carte,
            errorMessage: e.toString());
      }
    } else if (event is BloquerCarteEvent) {
      yield CarteStateBloc(
          requestStateBloque: StateStatus.LOADING,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          final data = blocageCarteMock;

          //print(data);
          CarteStateBloc carteStateBloc = CarteStateBloc(
              resBloque: data,
              requestState: state.requestState,
              requestStateUp: state.requestStateUp,
              requestStatePl: state.requestStatePl,
              currentAction: event,
              requestStateBloque: StateStatus.LOADED,
              plafond: state.plafond,
              currentCarte: state.currentCarte,
              carte: state.carte);
          yield carteStateBloc;
        } else {
          final data = await carteService.bloquerCarte(compteService.tokenState,
              compteService.tokenState.getUserDetails(), event.typeBloque);

          if (data["success"]) {
            //print(data);
            CarteStateBloc carteStateBloc = CarteStateBloc(
                resBloque: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestStateBloque: StateStatus.LOADED,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          } else {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                resBloque: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestStateBloque: StateStatus.ERROR,
                errorMessage: data["message"],
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          }
        }
        // print("data from bloc *** $data");
      } catch (e) {
        // print("bloc err $e");
        yield CarteStateBloc(
            requestState: state.requestState,
            requestStateUp: state.requestStateUp,
            requestStatePl: state.requestStatePl,
            errorMessage: e.toString(),
            requestStateBloque: StateStatus.ERROR,
            currentAction: event,
            plafond: state.plafond,
            currentCarte: state.currentCarte,
            carte: state.carte);
      }
    } else if (event is CardStatusEvent) {
      yield CarteStateBloc(
          requestCardStatus: StateStatus.LOADING,
          requestStateUp: state.requestStateUp,
          requestState: state.requestState,
          requestStatePl: state.requestStatePl,
          plafond: state.plafond,
          currentCarte: state.currentCarte,
          carte: state.carte);
      try {
        if (modeDemo) {
          final data = blocageCarteMock["Map"];

          CarteStateBloc carteStateBloc = CarteStateBloc(
              cardStatus: data,
              requestState: state.requestState,
              requestStateUp: state.requestStateUp,
              requestStatePl: state.requestStatePl,
              currentAction: event,
              requestCardStatus: StateStatus.LOADED,
              plafond: state.plafond,
              currentCarte: state.currentCarte,
              carte: state.carte);
          yield carteStateBloc;
        } else {
          final data = await carteService.cardStatus(compteService.tokenState,
              compteService.tokenState.getUserDetails());

          if (data["success"]) {
            //print(data);
            CarteStateBloc carteStateBloc = CarteStateBloc(
                cardStatus: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestCardStatus: StateStatus.LOADED,
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          } else {
            CarteStateBloc carteStateBloc = CarteStateBloc(
                cardStatus: data,
                requestState: state.requestState,
                requestStateUp: state.requestStateUp,
                requestStatePl: state.requestStatePl,
                currentAction: event,
                requestCardStatus: StateStatus.ERROR,
                errorMessage: data["message"],
                plafond: state.plafond,
                currentCarte: state.currentCarte,
                carte: state.carte);
            yield carteStateBloc;
          }
        }
        // print("data from bloc *** $data");
      } catch (e) {
        // print("bloc err $e");
        yield CarteStateBloc(
            requestState: state.requestState,
            requestStateUp: state.requestStateUp,
            requestStatePl: state.requestStatePl,
            errorMessage: e.toString(),
            requestCardStatus: StateStatus.ERROR,
            currentAction: event,
            plafond: state.plafond,
            currentCarte: state.currentCarte,
            carte: state.carte);
      }
    }
  }
}
