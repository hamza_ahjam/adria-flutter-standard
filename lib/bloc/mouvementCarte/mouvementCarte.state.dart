import 'package:LBA/bloc/mouvementCarte/mouvementCarte.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/mouvementCarte.model.dart';

class MouvementCarteStateBloc {
  StateStatus requestState;
  StateStatus requestStatePl;
  String errorMessage;
  String errorMessagePl;
  MouvementCarteEvent currentAction;
  MouvementCarte mouvementCarte;
  var res;

  MouvementCarteStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStatePl,
      this.errorMessagePl,
      this.res,
      this.mouvementCarte});

  MouvementCarteStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStatePl = StateStatus.NONE;
    this.errorMessage = "";
    this.errorMessage = "";
    this.mouvementCarte = MouvementCarte();
    this.res = null;
  }
}
