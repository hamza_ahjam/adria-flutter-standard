abstract class MouvementCarteEvent<T> {
  T payload;
  MouvementCarteEvent({this.payload});
}

class LoadCarteOpperationEvent extends MouvementCarteEvent {
  LoadCarteOpperationEvent() : super();
}

class LoadCartePlafondEvent extends MouvementCarteEvent {
  LoadCartePlafondEvent() : super();
}

class ResetCarteOpperationEvent extends MouvementCarteEvent {
  ResetCarteOpperationEvent() : super();
}
