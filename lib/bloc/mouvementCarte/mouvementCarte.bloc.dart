import 'package:LBA/bloc/mouvementCarte/mouvementCarte.event.dart';
import 'package:LBA/bloc/mouvementCarte/mouvementCarte.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/listMouvementsCard.dart';
import 'package:LBA/mocks/mock_plafond_card.dart';
import 'package:LBA/models/mouvementCarte.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/carte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class MouvementCarteBloc
    extends Bloc<MouvementCarteEvent, MouvementCarteStateBloc> {
  CarteService get carteService => GetIt.I<CarteService>();
  CompteService get compteService => GetIt.I<CompteService>();

  MouvementCarteBloc(MouvementCarteStateBloc initialState)
      : super(initialState);
  @override
  Stream<MouvementCarteStateBloc> mapEventToState(
      MouvementCarteEvent event) async* {
    if (event is LoadCarteOpperationEvent) {
      yield MouvementCarteStateBloc(
        requestState: StateStatus.LOADING,
        requestStatePl: state.requestStatePl,
        res: state.res,
        errorMessagePl: state.errorMessagePl,
      );

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = MouvementCarte.fromJson(mouvementCartMock);
          // print("in mc carte bloc ${data.map.mouvementCarteList.length}");
          MouvementCarteStateBloc mouvementCarteStateBloc =
              MouvementCarteStateBloc(
                  mouvementCarte: data,
                  requestState: StateStatus.LOADED,
                  requestStatePl: state.requestStatePl,
                  res: state.res,
                  errorMessagePl: state.errorMessagePl,
                  currentAction: event);
          yield mouvementCarteStateBloc;
        } else {
          final data = await carteService.getOperationCarte(
              compteService.tokenState,
              carteService.currentCarte.id,
              carteService.currentCarte.numeroCompte,
              carteService.currentCarte.numero);
          // print("in mc carte bloc ${data.map.mouvementCarteList.length}");
          MouvementCarteStateBloc mouvementCarteStateBloc =
              MouvementCarteStateBloc(
                  mouvementCarte: data,
                  requestState: StateStatus.LOADED,
                  requestStatePl: state.requestStatePl,
                  res: state.res,
                  errorMessagePl: state.errorMessagePl,
                  currentAction: event);
          yield mouvementCarteStateBloc;
        }
      } catch (e) {
        yield MouvementCarteStateBloc(
            errorMessage: e.toString(),
            errorMessagePl: state.errorMessagePl,
            requestState: StateStatus.ERROR,
            requestStatePl: state.requestStatePl,
            res: state.res,
            currentAction: event);
      }
    } else if (event is LoadCartePlafondEvent) {
      yield MouvementCarteStateBloc(
          requestState: state.requestState,
          requestStatePl: StateStatus.LOADING,
          mouvementCarte: state.mouvementCarte,
          errorMessage: state.errorMessage);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = pladondCardMock;
          // print("in mc carte bloc ${data.map.mouvementCarteList.length}");
          if (data != null) {
            MouvementCarteStateBloc mouvementCarteStateBloc =
                MouvementCarteStateBloc(
                    res: data,
                    requestState: state.requestState,
                    mouvementCarte: state.mouvementCarte,
                    errorMessage: state.errorMessage,
                    requestStatePl: StateStatus.LOADED,
                    currentAction: event);
            yield mouvementCarteStateBloc;
          }
        } else {
          final data = await carteService.getPlafondCarte(
            compteService.tokenState,
            carteService.currentCarte.rib,
            carteService.currentCarte.numero,
            carteService.currentCarte.numeroCompte,
          );
          // print("in mc carte bloc ${data.map.mouvementCarteList.length}");
          if (data != null) {
            MouvementCarteStateBloc mouvementCarteStateBloc =
                MouvementCarteStateBloc(
                    res: data,
                    requestState: state.requestState,
                    mouvementCarte: state.mouvementCarte,
                    errorMessage: state.errorMessage,
                    requestStatePl: StateStatus.LOADED,
                    currentAction: event);
            yield mouvementCarteStateBloc;
          }
        }
        // print("${state.res["Map"]["plafonds"]}");
      } catch (e) {
        yield MouvementCarteStateBloc(
            errorMessagePl: e.toString(),
            requestStatePl: StateStatus.ERROR,
            errorMessage: state.errorMessage,
            requestState: state.requestState,
            mouvementCarte: state.mouvementCarte,
            currentAction: event);
      }
    } else if (event is ResetCarteOpperationEvent) {
      yield MouvementCarteStateBloc(requestState: StateStatus.NONE);
    }
  }
}
