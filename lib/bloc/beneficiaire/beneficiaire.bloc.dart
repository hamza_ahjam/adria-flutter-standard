import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/bloc/beneficiaire/beneficiaire.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/beneficiaire.mock.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/models/beneficiaire.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/beneficiaire.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class BeneficiaireBloc extends Bloc<BeneficiaireEvent, BeneficiaireStateBloc> {
  BeneficiaireService get beneficiaireService => GetIt.I<BeneficiaireService>();
  CompteService get compteService => GetIt.I<CompteService>();

  BeneficiaireBloc(BeneficiaireStateBloc initialState) : super(initialState);

  @override
  Stream<BeneficiaireStateBloc> mapEventToState(
      BeneficiaireEvent event) async* {
    if (event is LoadBeneficiaire) {
      yield BeneficiaireStateBloc(
          requestState: StateStatus.LOADING, beneficiaire: state.beneficiaire);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = Beneficiaire.fromJson(beneficiaireMock);

          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              beneficiaire: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield beneficiaireStateBloc;
        } else {
          final data = await beneficiaireService.getBeneficaireList(
              compteService.tokenState.getUserDetails(),
              compteService.tokenState);
          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              beneficiaire: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield beneficiaireStateBloc;
        }
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadBeneficiaireMAD) {
      yield BeneficiaireStateBloc(
          requestStateMAD: StateStatus.LOADING,
          beneficiaireMAD: state.beneficiaire);

      try {
        final data = await beneficiaireService.getBeneficaireListMAD(
            compteService.tokenState.getUserDetails(),
            compteService.tokenState);
        BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
            beneficiaireMAD: data,
            requestStateMAD: StateStatus.LOADED,
            currentAction: event);
        yield beneficiaireStateBloc;
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestStateMAD: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is DeleteBeneficiaireEvent) {
      yield BeneficiaireStateBloc(
          requestState: StateStatus.LOADED,
          requestStateDelete: StateStatus.LOADING,
          beneficiaire: state.beneficiaire,
          res: null);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = deleteBenefMock["Map"];

          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              requestState: StateStatus.LOADED,
              requestStateDelete: StateStatus.LOADED,
              currentAction: event,
              res: data,
              beneficiaire: state.beneficiaire);
          yield beneficiaireStateBloc;
        } else {
          final data = await beneficiaireService.daleteBenificiaire(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            event.id,
            event.nature,
          );

          if (data["success"]) {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestState: StateStatus.LOADED,
                requestStateDelete: StateStatus.LOADED,
                currentAction: event,
                res: data,
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          } else {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestState: StateStatus.LOADED,
                requestStateDelete: StateStatus.ERROR,
                currentAction: event,
                errorMessage: data["message"],
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          }
          // BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
          //     res: data,
          //     requestState: StateStatus.LOADED,
          //     currentAction: event,
          //     beneficiaire: state.beneficiaire);
          // yield beneficiaireStateBloc;
        }
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestStateAdd: StateStatus.ERROR,
            requestState: StateStatus.LOADED,
            beneficiaire: state.beneficiaire,
            currentAction: event);
      }
    } else if (event is AddSaveBeneficiaireEvent) {
      yield BeneficiaireStateBloc(
          requestState: StateStatus.LOADED,
          beneficiaire: state.beneficiaire,
          requestStateAdd: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = saveMock["Map"];

          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              requestStateAdd: StateStatus.LOADED,
              requestState: StateStatus.LOADED,
              currentAction: event,
              res: data,
              beneficiaire: state.beneficiaire);
          yield beneficiaireStateBloc;
        } else {
          final data = await beneficiaireService.saveBenificiaire(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              event.nom,
              event.rib);

          if (data["success"]) {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateAdd: StateStatus.LOADED,
                requestState: StateStatus.LOADED,
                currentAction: event,
                res: data,
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;

            if (data["methodSignature"] == "OTPSMS") {
              // final otpData =
              await beneficiaireService.signeBeneficiaireOTP(
                  compteService.tokenState,
                  compteService.tokenState.getUserDetails(),
                  state.res["id"],
                  state.res["taskId"]);
              // print(otpData);
            }
          } else {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateAdd: StateStatus.ERROR,
                requestState: StateStatus.LOADED,
                currentAction: event,
                errorMessage: data["message"],
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          }

          // print(data);
          // BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
          //     beneficiaire: data,
          //     requestState: StateStatus.LOADED,
          //     currentAction: event);
          // yield beneficiaireStateBloc;
        }
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestStateAdd: StateStatus.ERROR,
            requestState: StateStatus.LOADED,
            beneficiaire: state.beneficiaire,
            currentAction: event);
      }
    } else if (event is AddSigneBeneficiaireEvent) {
      yield BeneficiaireStateBloc(
          requestState: StateStatus.LOADED,
          requestStateSigne: StateStatus.LOADING,
          beneficiaire: state.beneficiaire,
          res: state.res);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              requestStateSigne: StateStatus.LOADED,
              requestState: StateStatus.LOADED,
              currentAction: event,
              res: data,
              beneficiaire: state.beneficiaire);
          yield beneficiaireStateBloc;
        } else {
          final data = await beneficiaireService.signeBeneficiaire(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res['id'],
              state.res['taskId'],
              event.password);

          if (data["success"]) {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateSigne: StateStatus.LOADED,
                requestState: StateStatus.LOADED,
                currentAction: event,
                res: data,
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          } else {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateSigne: StateStatus.ERROR,
                requestState: StateStatus.LOADED,
                currentAction: event,
                errorMessage: data["message"],
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          }
          // print(data);
          // BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
          //     beneficiaire: data,
          //     requestState: StateStatus.LOADED,
          //     currentAction: event);
          // yield beneficiaireStateBloc;
        }
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            requestState: StateStatus.LOADED,
            beneficiaire: state.beneficiaire,
            currentAction: event);
      }
    } else if (event is AbandonerBenef) {
      yield BeneficiaireStateBloc(
          requestState: state.requestState,
          requestStateSigne: state.requestStateSigne,
          requestStateAbon: StateStatus.LOADING,
          beneficiaire: state.beneficiaire,
          res: state.res);

      try {
        if (modeDemo) {
          final data = signeMock["Map"];

          BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
              requestStateAbon: StateStatus.LOADED,
              requestState: state.requestState,
              requestStateSigne: state.requestStateSigne,
              currentAction: event,
              res: state.res,
              beneficiaire: state.beneficiaire);
          yield beneficiaireStateBloc;
        } else {
          final data = await beneficiaireService.abondonnerPhaseSignature(
              compteService.tokenState.getUserDetails(),
              compteService.tokenState,
              state.res['id'],
              state.res['taskId']);

          if (data["success"]) {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateAbon: StateStatus.LOADED,
                requestState: state.requestState,
                requestStateSigne: state.requestStateSigne,
                currentAction: event,
                res: state.res,
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          } else {
            BeneficiaireStateBloc beneficiaireStateBloc = BeneficiaireStateBloc(
                requestStateAbon: StateStatus.ERROR,
                requestState: state.requestState,
                requestStateSigne: state.requestStateSigne,
                currentAction: event,
                errorMessage: data["message"],
                beneficiaire: state.beneficiaire);
            yield beneficiaireStateBloc;
          }
          // print(state.res.toString());
        }
      } catch (e) {
        yield BeneficiaireStateBloc(
            errorMessage: e.toString(),
            requestStateAbon: StateStatus.ERROR,
            requestState: state.requestState,
            requestStateSigne: state.requestStateSigne,
            beneficiaire: state.beneficiaire,
            currentAction: event);
      }
    } else if (event is ResetBenefEvent) {
      yield BeneficiaireStateBloc(requestState: StateStatus.NONE);
    }
  }
}
