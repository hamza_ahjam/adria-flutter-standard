abstract class BeneficiaireEvent<T> {
  T payload;
  BeneficiaireEvent({this.payload});
}

class LoadBeneficiaire extends BeneficiaireEvent {
  LoadBeneficiaire() : super();
}

class LoadBeneficiaireMAD extends BeneficiaireEvent {
  LoadBeneficiaireMAD() : super();
}

class DeleteBeneficiaireEvent extends BeneficiaireEvent {
  int id;
  var nature;
  DeleteBeneficiaireEvent({this.id, this.nature}) : super();
}

class AddSaveBeneficiaireEvent extends BeneficiaireEvent {
  String nom, rib;
  AddSaveBeneficiaireEvent({this.nom, this.rib}) : super();
}

class AddSigneBeneficiaireEvent extends BeneficiaireEvent {
  String password;
  AddSigneBeneficiaireEvent({this.password}) : super();
}

class AbandonerBenef extends BeneficiaireEvent {
  AbandonerBenef() : super();
}

class ResetBenefEvent extends BeneficiaireEvent {
  ResetBenefEvent() : super();
}
