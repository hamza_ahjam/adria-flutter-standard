import 'package:LBA/bloc/beneficiaire/beneficiaire.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/beneficiaire.model.dart';

class BeneficiaireStateBloc {
  StateStatus requestState;
  StateStatus requestStateMAD;
  StateStatus requestStateDelete;
  StateStatus requestStateAdd;
  StateStatus requestStateSigne;
  StateStatus requestStateAbon;
  String errorMessage;
  BeneficiaireEvent currentAction;
  Beneficiaire beneficiaire;
  Beneficiaire beneficiaireMAD;
  var res;

  BeneficiaireStateBloc(
      {this.currentAction,
        this.errorMessage,
        this.requestState,
        this.requestStateMAD,
        this.requestStateDelete,
        this.requestStateAdd,
        this.requestStateSigne,
        this.requestStateAbon,
        this.beneficiaire,
        this.beneficiaireMAD,
        this.res});

  BeneficiaireStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateMAD = StateStatus.NONE;
    this.requestStateDelete = StateStatus.NONE;
    this.requestStateAdd = StateStatus.NONE;
    this.requestStateSigne = StateStatus.NONE;
    this.requestStateAbon = StateStatus.NONE;
    this.errorMessage = "";
    this.beneficiaire = Beneficiaire();
    this.beneficiaireMAD = Beneficiaire();
    this.res = null;
  }
}
