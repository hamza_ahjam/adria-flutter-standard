import 'package:LBA/models/compte.model.dart';

abstract class ChequeEvent<T> {
  T payload;
  ChequeEvent({this.payload});
}

class LoadChequeEvent extends ChequeEvent {
  int page;
  LoadChequeEvent({this.page}) : super();
}

class LoadHistoriqueChequeEvent extends ChequeEvent {
  int page;
  LoadHistoriqueChequeEvent({this.page}) : super();
}

class ResetChequeEvent extends ChequeEvent {
  ResetChequeEvent() : super();
}

class DemandeSaveChequieEvent extends ChequeEvent {
  var context, numChequiers, crossed, endorsable, typeChequier;
  DemandeSaveChequieEvent(
      {this.context,
      this.crossed,
      this.endorsable,
      this.numChequiers,
      this.typeChequier})
      : super();
}

class DemandeSigneChequieEvent extends ChequeEvent {
  var psd;
  DemandeSigneChequieEvent({this.psd}) : super();
}

class ChangeAccountHistoriqueDemandeChiquierEvent extends ChequeEvent {
  Compte compte;
  ChangeAccountHistoriqueDemandeChiquierEvent({this.compte}) : super();
}

class ChangeAccountHistoriqueChequesEvent extends ChequeEvent {
  Compte compte;
  ChangeAccountHistoriqueChequesEvent({this.compte}) : super();
}
