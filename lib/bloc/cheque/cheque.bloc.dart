import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/bloc/cheque/cheque.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/historiqueCheques.dart';
import 'package:LBA/mocks/list_cheque.mock.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/models/historique-chequier.model.dart';
import 'package:LBA/models/hsitoriqueCheque.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/cheque.service.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class ChequeBloc extends Bloc<ChequeEvent, ChequeStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  ChequeService get chequeService => GetIt.I<ChequeService>();

  ChequeBloc(ChequeStateBloc initialState) : super(initialState);
  @override
  Stream<ChequeStateBloc> mapEventToState(ChequeEvent event) async* {
    if (event is LoadChequeEvent) {
      yield ChequeStateBloc(requestState: StateStatus.LOADING);
      try {
        if (modeDemo) {
          final data = HistoriqueChequier.fromJson(listChequeMock);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listChequier: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield chequeStateBloc;
        } else {
          final data = await compteService.getDemandeChequier(
              compteService.currentCompteChequier.identifiantInterne,
              event.page);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listChequier: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield chequeStateBloc;
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is LoadHistoriqueChequeEvent) {
      yield ChequeStateBloc(
        requestStateHisto: StateStatus.LOADING,
        requestState: state.requestState,
        listChequier: state.listChequier,
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = HistoriqueCheque.fromJson(listHistoriqueCheques);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listCheques: data,
              requestStateHisto: StateStatus.LOADED,
              requestState: state.requestState,
              listChequier: state.listChequier,
              currentAction: event);
          yield chequeStateBloc;
        } else {
          final data = await chequeService.getHistoriqueCheque(
              compteService.tokenState,
              compteService.currentCompteChequier.identifiantInterne,
              event.page);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listCheques: data,
              requestStateHisto: StateStatus.LOADED,
              requestState: state.requestState,
              listChequier: state.listChequier,
              currentAction: event);
          yield chequeStateBloc;
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestStateHisto: StateStatus.ERROR,
            requestState: state.requestState,
            listChequier: state.listChequier,
            currentAction: event);
      }
    } else if (event is ChangeAccountHistoriqueDemandeChiquierEvent) {
      yield ChequeStateBloc(
          requestState: StateStatus.LOADING, listChequier: state.listChequier);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = HistoriqueChequier.fromJson(listChequeMock);

          compteService.currentCompteChequier = event.compte;

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listChequier: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield chequeStateBloc;
        } else {
          compteService.currentCompteChequier = event.compte;
          final data = await compteService.getDemandeChequier(
              compteService.currentCompteChequier.identifiantInterne, 1);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listChequier: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield chequeStateBloc;
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountHistoriqueChequesEvent) {
      print('switch account event');
      yield ChequeStateBloc(
        requestStateHisto: StateStatus.LOADING,
        listCheques: state.listCheques,
        requestState: state.requestState,
        listChequier: state.listChequier,
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = HistoriqueCheque.fromJson(listHistoriqueCheques);
          compteService.currentCompteChequier = event.compte;

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listCheques: data,
              requestStateHisto: StateStatus.LOADED,
              requestState: state.requestState,
              listChequier: state.listChequier,
              currentAction: event);
          yield chequeStateBloc;
        } else {
          compteService.currentCompteChequier = event.compte;
          final data = await chequeService.getHistoriqueCheque(
              compteService.tokenState,
              compteService.currentCompteChequier.identifiantInterne,
              1);

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              listCheques: data,
              requestStateHisto: StateStatus.LOADED,
              requestState: state.requestState,
              listChequier: state.listChequier,
              currentAction: event);
          yield chequeStateBloc;
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestStateHisto: StateStatus.ERROR,
            requestState: state.requestState,
            listChequier: state.listChequier,
            currentAction: event);
      }
    } else if (event is DemandeSaveChequieEvent) {
      yield ChequeStateBloc(
          requestState: state.requestState,
          requestStateSave: StateStatus.LOADING,
          listChequier: state.listChequier);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = saveMock["Map"];

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              requestState: state.requestState,
              requestStateSave: StateStatus.LOADED,
              currentAction: event,
              res: data,
              listChequier: state.listChequier);
          yield chequeStateBloc;
        } else {
          final data = await chequeService.saveChequeServie(
              event.context,
              compteService.tokenState,
              event.numChequiers,
              event.crossed,
              event.endorsable,
              compteService.currentCompteChequier.identifiantInterne,
              event.typeChequier);

          if (data["success"]) {
            ChequeStateBloc chequeStateBloc = ChequeStateBloc(
                requestState: state.requestState,
                requestStateSave: StateStatus.LOADED,
                currentAction: event,
                res: data,
                listChequier: state.listChequier);
            yield chequeStateBloc;
          } else {
            ChequeStateBloc chequeStateBloc = ChequeStateBloc(
                requestState: state.requestState,
                requestStateSave: StateStatus.ERROR,
                currentAction: event,
                errorMessage: data["message"],
                listChequier: state.listChequier);
            yield chequeStateBloc;
            // print("data demande cheque *** $data");
          }
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestState: state.requestState,
            requestStateSave: StateStatus.ERROR,
            listChequier: state.listChequier,
            currentAction: event);
      }
    } else if (event is DemandeSigneChequieEvent) {
      yield ChequeStateBloc(
          requestState: state.requestState,
          requestStateSigne: StateStatus.LOADING,
          listChequier: state.listChequier,
          res: state.res);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          ChequeStateBloc chequeStateBloc = ChequeStateBloc(
              requestStateSigne: StateStatus.LOADED,
              requestState: state.requestState,
              currentAction: event,
              res: data,
              listChequier: state.listChequier);
          yield chequeStateBloc;
        } else {
          final data = await chequeService.signeChequeService(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.psd);

          if (data["success"]) {
            ChequeStateBloc chequeStateBloc = ChequeStateBloc(
                requestStateSigne: StateStatus.LOADED,
                requestState: state.requestState,
                currentAction: event,
                res: data,
                listChequier: state.listChequier);
            yield chequeStateBloc;
          } else {
            ChequeStateBloc chequeStateBloc = ChequeStateBloc(
                requestState: state.requestState,
                requestStateSigne: StateStatus.ERROR,
                currentAction: event,
                errorMessage: data["message"],
                listChequier: state.listChequier);
            yield chequeStateBloc;
          }
          // print("data signe *** $data , ${state.requestStateSigne}");
        }
      } catch (e) {
        yield ChequeStateBloc(
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            requestState: state.requestState,
            listChequier: state.listChequier,
            currentAction: event);
      }
    } else if (event is ResetChequeEvent) {
      yield ChequeStateBloc(
          requestState: StateStatus.NONE, requestStateHisto: StateStatus.NONE);
    }
  }
}
