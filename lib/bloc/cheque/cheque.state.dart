import 'package:LBA/bloc/cheque/cheque.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/historique-chequier.model.dart';
import 'package:LBA/models/hsitoriqueCheque.model.dart';

class ChequeStateBloc {
  StateStatus requestState;
  StateStatus requestStateHisto;
  StateStatus requestStateSave;
  StateStatus requestStateSigne;
  String errorMessage;
  ChequeEvent currentAction;
  HistoriqueChequier listChequier;
  HistoriqueCheque listCheques;
  var res;

  ChequeStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStateHisto,
      this.requestStateSave,
      this.requestStateSigne,
      this.listChequier,
      this.listCheques,
      this.res});

  ChequeStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateHisto = StateStatus.NONE;
    this.requestStateSave = StateStatus.NONE;
    this.requestStateSigne = StateStatus.NONE;
    this.errorMessage = "";
    this.listChequier = HistoriqueChequier();
    this.listCheques = HistoriqueCheque();
    this.res = null;
  }
}
