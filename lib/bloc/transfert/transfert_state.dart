import 'package:LBA/bloc/transfert/transfert_event.dart';
import 'package:LBA/enum/stateStatus.dart';


class TransfertState {
  StateStatus requestStateSave;
  StateStatus requestStateSigne;
  String errorMessage;
  String errorMessageAbon;
  TransfertEvent currentAction;
  var res;

  TransfertState({
      this.requestStateSave,
      this.requestStateSigne,
      this.errorMessage,
      this.errorMessageAbon,
      this.currentAction,
      this.res});

  TransfertState.initialState() {
    this.requestStateSave = StateStatus.NONE;
    this.requestStateSigne = StateStatus.NONE;
    this.errorMessage = "";
  }}
