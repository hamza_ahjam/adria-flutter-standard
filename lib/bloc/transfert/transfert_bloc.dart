import 'dart:async';

import 'package:LBA/bloc/transfert/transfert_event.dart';
import 'package:LBA/bloc/transfert/transfert_state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/transfert.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class TransfertBloc extends Bloc<TransfertEvent, TransfertState> {
  CompteService get compteService => GetIt.I<CompteService>();
  TransfertService get transfertService => GetIt.I<TransfertService>();

  TransfertBloc(
      TransfertState initialState,
      ) : super(initialState);

  @override
  Stream<TransfertState> mapEventToState(
    TransfertEvent event,
  ) async* {
    if (event is SaveTransfertEvent){
      yield TransfertState(
        requestStateSave: StateStatus.LOADING,
      );
      try {
        final data = await transfertService.saveTransfert(
          compteService.tokenState,
          compteService.currentCompte.identifiantInterne,
          event.numeroPieceIdentite,
          event.montant,
          event.motif,
          event.gsm,
          event.dateTransfert,
          event.intituleCompte,
          event.numCompteCrediter,
          event.typePieceIdentite,
          event.typeTransfer,
          event.referenceClient,
          compteService.tokenState.getUserDetails()["username"],
          event.nomCre
        );

        if (data["success"]) {
          TransfertState virementStateBloc = TransfertState(
              res: data,
              currentAction: event,
              requestStateSave: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          TransfertState virementStateBloc = TransfertState(
              res: data,
              currentAction: event,
              requestStateSave: StateStatus.ERROR,
              errorMessage: data["message"]);
          yield virementStateBloc;
        }
        // print("from bloc data *** $data");
      } catch (e) {
        // print("bloc err $e");
        yield TransfertState(
            errorMessage: e.toString(),
            requestStateSave: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SigneTransfertEvent) {
      yield TransfertState(
          res: state.res,
          requestStateSigne: StateStatus.LOADING);

      try {
        final data = await transfertService.signeTransfert(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            state.res["id"],
            state.res["taskId"],
            event.password);

        if (data["success"]) {
          TransfertState virementStateBloc = TransfertState(
              res: data,
              currentAction: event,
              requestStateSigne: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          TransfertState virementStateBloc = TransfertState(
              res: data,
              currentAction: event,
              requestStateSigne: StateStatus.ERROR,
              errorMessage: data["message"]);
          yield virementStateBloc;
        }
      } catch (e) {
        yield TransfertState(
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            currentAction: event);
      }
    }
  }
}
