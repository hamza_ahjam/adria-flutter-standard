abstract class TransfertEvent<T>  {
  T payload;
  TransfertEvent({this.payload});
}

class SaveTransfertEvent extends TransfertEvent{
  var montant,
      motif,
      gsm,
      dateTransfert,
      intituleCompte,
      numCompteCrediter,
      typeCompteCredite,
      nomCre,
      numeroPieceIdentite,
      typePieceIdentite,
      typeTransfer,
      referenceClient;

  SaveTransfertEvent({
      this.montant,
      this.motif,
      this.gsm,
      this.dateTransfert,
      this.intituleCompte,
      this.numCompteCrediter,
      this.typeCompteCredite,
      this.nomCre,
      this.numeroPieceIdentite,
      this.typePieceIdentite,
      this.typeTransfer,
      this.referenceClient
      });
}

class SigneTransfertEvent extends TransfertEvent {
  String password;
  SigneTransfertEvent({this.password}) : super();
}
