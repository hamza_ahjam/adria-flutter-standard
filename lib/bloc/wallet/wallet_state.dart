import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/loginDpRS.model.dart';

class WalletState {
  StateStatus requestStatePaiementAchatSave;
  StateStatus requestStatePaiementAchatOtp;
  StateStatus requestStatePaiementAchatSign;
  StateStatus requestStatePaiementAchatAbandonner;
  StateStatus requestStateScanQR;
  StateStatus requestStateEnterEnRelation;
  StateStatus requestStateEnterEnRelationLogin;
  StateStatus requestStateLoginDP;
  StateStatus requestStateDecryptPhone;
  StateStatus requestStateGetBeneficiary;
  StateStatus requestStateGetQRcode;
  String errorMessage;
  WalletEvent currentAction;
  LoginDpRS loginDpRS;
  var res;
  var eerToken;
  var qrRes;
  var decryptedRes;
  var beneficiaryRes;
  var qrCodeAmount;
  var qrCodeMotif;
  var qrCodeData;


  WalletState({
    this.requestStatePaiementAchatSave,
    this.requestStatePaiementAchatOtp,
    this.requestStatePaiementAchatSign,
    this.requestStatePaiementAchatAbandonner,
    this.requestStateScanQR,
    this.requestStateEnterEnRelation,
    this.requestStateEnterEnRelationLogin,
    this.requestStateLoginDP,
    this.requestStateDecryptPhone,
    this.requestStateGetBeneficiary,
    this.requestStateGetQRcode,
    this.errorMessage,
    this.currentAction,
    this.res,
    this.qrRes,
    this.decryptedRes,
    this.eerToken,
    this.loginDpRS,
    this.beneficiaryRes,
    this.qrCodeAmount,
    this.qrCodeMotif,
    this.qrCodeData
  });

  WalletState.initialState() {
    this.requestStatePaiementAchatSave = StateStatus.NONE;
    this.requestStatePaiementAchatOtp = StateStatus.NONE;
    this.requestStatePaiementAchatSign = StateStatus.NONE;
    this.requestStatePaiementAchatAbandonner = StateStatus.NONE;
    this.requestStateScanQR = StateStatus.NONE;
    this.requestStateEnterEnRelation = StateStatus.NONE;
    this.requestStateLoginDP = StateStatus.NONE;
    this.requestStateDecryptPhone = StateStatus.NONE;
    this.requestStateGetBeneficiary = StateStatus.NONE;
    this.requestStateGetQRcode = StateStatus.NONE;
    this.qrCodeAmount = 0.0;
    this.qrCodeMotif = "";
    this.loginDpRS = LoginDpRS();
    this.errorMessage = "";
  }
}