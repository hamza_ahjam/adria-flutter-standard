import 'dart:async';

import 'package:LBA/bloc/wallet/wallet_event.dart';
import 'package:LBA/bloc/wallet/wallet_state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/services/wallet.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

import '../../constants.dart';

class WalletBloc extends Bloc<WalletEvent, WalletState> {

  WalletService get walletService => GetIt.I<WalletService>();

  WalletBloc(
      WalletState initialState,
      ) : super(initialState);

  @override
  Stream<WalletState> mapEventToState(
    WalletEvent event,
  ) async* {
    if (event is SaveWalletPaiementAchatEvent) {
      yield WalletState(
          requestStatePaiementAchatSave: StateStatus.LOADING,
          eerToken: state.eerToken,
          qrRes: state.qrRes,
          decryptedRes: state.decryptedRes
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          WalletState walletState = WalletState(
            res: data,
            requestStatePaiementAchatSave: StateStatus.LOADED,
            currentAction: event,
          );
          yield walletState;
        } else {
          final data = await walletService.saveWalletPaiementAchat(
            event.montant,
            event.telephone,
            event.transactionIndicator,
            event.intituleBeneficiaire
          );

          // await Future.delayed(Duration(seconds: 1));
          // final data = saveMock["Map"];

          if (data["success"] && data["transactionInstance"]["statut"] != "Rejeté") {
            WalletState walletState = WalletState(
              res: data,
              requestStatePaiementAchatSave: StateStatus.LOADED,
              eerToken: state.eerToken,
              currentAction: event,
            );
            yield walletState;

            this.add(SendOtpWalletPaiementAchatEvent());

          } else {
            WalletState walletState = WalletState(
                res: data,
                requestStatePaiementAchatSave: StateStatus.ERROR,
                qrRes: state.qrRes,
                decryptedRes: state.decryptedRes,
                currentAction: event,
                errorMessage: data["success"] ? data['statut'] : data["codeErreur"]);
            yield walletState;
          }
          // print("from bloc data *** $data");
        }
      } catch (e) {
        // print("bloc err $e");
        WalletState walletState = WalletState(
            requestStatePaiementAchatSave: StateStatus.ERROR,
            qrRes: state.qrRes,
            decryptedRes: state.decryptedRes,
            currentAction: event,
            errorMessage: e.toString()
        );
        yield walletState;
      }
    } else if (event is SendOtpWalletPaiementAchatEvent) {
      yield WalletState(
          requestStatePaiementAchatOtp: StateStatus.LOADING,
          res: state.res,
          eerToken: state.eerToken,
          qrRes: state.qrRes,
          decryptedRes: state.decryptedRes
      );
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          WalletState walletState = WalletState(
            res: data,
            requestStatePaiementAchatSave: state.requestStatePaiementAchatSave,
            requestStatePaiementAchatOtp: StateStatus.LOADED,
            eerToken: state.eerToken,
            currentAction: event,
          );
          yield walletState;
        } else {
          final data = await walletService.otpWalletPaiementAchat(
              state.res['id'],
              state.res['taskId'],
              state.res['referenceDemande']
          );

          if (data["success"]) {
            WalletState walletState = WalletState(
              res: state.res,
              eerToken: state.eerToken,
              requestStatePaiementAchatSave: state.requestStatePaiementAchatSave,
              requestStatePaiementAchatOtp: StateStatus.LOADED,
              currentAction: event,
            );
            yield walletState;
          } else {
            WalletState walletState = WalletState(
                res: state.res,
                requestStatePaiementAchatSave: state.requestStatePaiementAchatSave,
                requestStatePaiementAchatOtp: StateStatus.ERROR,
                eerToken: state.eerToken,
                currentAction: event,
                errorMessage: data["message"]);
            yield walletState;
          }
          // print("from bloc data *** $data");
        }
      } catch (e) {
        // print("bloc err $e");
        WalletState walletState = WalletState(
            requestStatePaiementAchatSave: state.requestStatePaiementAchatSave,
            requestStatePaiementAchatOtp: StateStatus.ERROR,
            eerToken: state.eerToken,
            currentAction: event,
            errorMessage: e.toString()
        );
        yield walletState;
      }
    } else if (event is SignWalletPaiementAchatEvent) {
      yield WalletState(
          res: state.res,
          eerToken: state.eerToken,
          requestStatePaiementAchatSign: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = signeMock["Map"];

          WalletState virementStateBloc = WalletState(
              res: data,
              currentAction: event,
              requestStatePaiementAchatSign: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await walletService.signWalletPaiementAchat(
              state.res["id"],
              state.res["taskId"],
              event.password);

          if (data["success"]) {
            WalletState walletState = WalletState(
                res: data,
                currentAction: event,
                requestStatePaiementAchatSign: StateStatus.LOADED);
            yield walletState;
          } else {
            WalletState virementStateBloc = WalletState(
                res: data,
                currentAction: event,
                requestStatePaiementAchatSign: StateStatus.ERROR,
                errorMessage: data["codeErreur"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield WalletState(
            errorMessage: e.toString(),
            requestStatePaiementAchatSign: StateStatus.ERROR,
            currentAction: event);
      }
    }  else if (event is AbandonnerWalletPaiementAchatEvent) {
      yield WalletState(
          res: state.res,
          eerToken: state.eerToken,
          requestStatePaiementAchatAbandonner: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = signeMock["Map"];

          WalletState virementStateBloc = WalletState(
              res: data,
              currentAction: event,
              requestStatePaiementAchatAbandonner: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await walletService.abondonnerWalletPaiementAchat(
              state.res["id"],
              state.res["taskId"]);

          if (data["success"]) {
            WalletState walletState = WalletState(
                res: data,
                currentAction: event,
                requestStatePaiementAchatAbandonner: StateStatus.LOADED);
            yield walletState;
          } else {
            WalletState virementStateBloc = WalletState(
                res: data,
                currentAction: event,
                requestStatePaiementAchatAbandonner: StateStatus.ERROR,
                errorMessage: data["codeErreur"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield WalletState(
            errorMessage: e.toString(),
            requestStatePaiementAchatAbandonner: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ScanQREvent) {
      yield WalletState(
          requestStateScanQR: StateStatus.LOADING, eerToken: state.eerToken);
      try {
          final data = await walletService.scanQr(event.qr, event.scanType);

          if (data["error"] == 'NO_ERROR') {
            WalletState walletState = WalletState(
              qrRes: data,
              requestStateScanQR: StateStatus.LOADED,
              currentAction: event,
            );
            yield walletState;

            if (state.qrRes['valid'] && state.qrRes['phoneAESEncrypted'])
            this.add(
                DecryptPhoneEvent(strToDecrypt: state.qrRes['encryptedPhone']));
        } else {
            WalletState walletState = WalletState(
                qrRes: data,
                requestStateScanQR: StateStatus.ERROR,
                currentAction: event,
                errorMessage: data["errorMessage"]
            );
            yield walletState;
          }
          // print("from bloc data *** $data");
      } catch (e) {
        // print("bloc err $e");
        WalletState walletState = WalletState(
            requestStateScanQR: StateStatus.ERROR,
            currentAction: event,
            errorMessage: e.toString()
        );
        yield walletState;
      }
    } else if (event is DecryptPhoneEvent) {
      yield WalletState(
          eerToken: state.eerToken,
          qrRes: state.qrRes,
          requestStateDecryptPhone: StateStatus.LOADING
      );
      try {
        final data = await walletService.decryptPhone(event.strToDecrypt);

        // await Future.delayed(Duration(seconds: 1));
        // final data = saveMock["Map"];

        if (data["success"]) {
          WalletState walletState = WalletState(
            eerToken: state.eerToken,
            qrRes: state.qrRes,
            requestStateDecryptPhone: StateStatus.LOADED,
            decryptedRes: data,
            currentAction: event,
          );
          yield walletState;
        } else {
          WalletState walletState = WalletState(
              eerToken: state.eerToken,
              requestStateDecryptPhone: StateStatus.ERROR,
              decryptedRes: data,
              currentAction: event,
              errorMessage: data["errorMessage"]
          );
          yield walletState;
        }
        // print("from bloc data *** $data");
      } catch (e) {
        // print("bloc err $e");
        WalletState walletState = WalletState(
            eerToken: state.eerToken,
            requestStateDecryptPhone: StateStatus.ERROR,
            currentAction: event,
            errorMessage: e.toString()
        );
        yield walletState;
      }
    }  else if (event is EntrerEnRelationOfflineEvent) {
      yield WalletState(
          requestStateEnterEnRelation: !event.isLogin ? StateStatus.LOADING : null,
          requestStateEnterEnRelationLogin: event.isLogin ? StateStatus.LOADING : null,
          qrRes: state.qrRes,
          decryptedRes: state.decryptedRes);

      try {
          var data = await walletService.entreEnRelationOffline();
          print('************* ${data['csrfToken']}');

          if (data["success"]) {

            walletService.token = data['csrfToken'];

            WalletState walletState = WalletState(
                eerToken: data['csrfToken'],
                currentAction: event,
                qrRes: state.qrRes,
                decryptedRes: state.decryptedRes,
                requestStateEnterEnRelation: !event.isLogin ? StateStatus.LOADED : null,
                requestStateEnterEnRelationLogin: event.isLogin ? StateStatus.LOADED : null
            );
            yield walletState;
          } else {
            WalletState virementStateBloc = WalletState(
                currentAction: event,
                qrRes: state.qrRes,
                decryptedRes: state.decryptedRes,
                requestStateEnterEnRelation: !event.isLogin ? StateStatus.ERROR : null,
                requestStateEnterEnRelationLogin: event.isLogin ? StateStatus.ERROR : null,
                errorMessage: 'error');
            yield virementStateBloc;
          }
      } catch (e) {
        yield WalletState(
            errorMessage: e.toString(),
            qrRes: state.qrRes,
            decryptedRes: state.decryptedRes,
            requestStateEnterEnRelation: !event.isLogin ? StateStatus.ERROR : null,
            requestStateEnterEnRelationLogin: event.isLogin ? StateStatus.ERROR : null,
            currentAction: event);
      }
    }  else if (event is LoginDPEvent) {
      yield WalletState(
          eerToken: state.eerToken,
          requestStateEnterEnRelation: state.requestStateEnterEnRelation,
          requestStateLoginDP: StateStatus.LOADING
      );

      try {
        final data = await walletService.loginDP(event.id, event.psd);

        if (data.success) {

          WalletState walletState = WalletState(
              eerToken: state.eerToken,
              loginDpRS: data,
              currentAction: event,
              requestStateEnterEnRelation: state.requestStateEnterEnRelation,
              requestStateLoginDP: StateStatus.LOADED
          );
          yield walletState;
        } else {
          print('${state.eerToken} ------------------------------------- error1');
          WalletState walletState = WalletState(
              currentAction: event,
              eerToken: state.eerToken,
              requestStateEnterEnRelation: state.requestStateEnterEnRelation,
              requestStateLoginDP: StateStatus.ERROR,
              errorMessage: data.errorMsg);
          yield walletState;
        }
      } catch (e) {
        print('${state.eerToken} ------------------------------------- error2');
        yield WalletState(
            errorMessage: e.toString(),
            eerToken: state.eerToken,
            requestStateEnterEnRelation: state.requestStateEnterEnRelation,
            requestStateLoginDP: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetWalletEvent) {
      yield WalletState(
        requestStatePaiementAchatSave: StateStatus.NONE,
        requestStatePaiementAchatSign: StateStatus.NONE,
        requestStateScanQR: StateStatus.NONE,
        requestStateEnterEnRelation: StateStatus.NONE,
        requestStateLoginDP: StateStatus.NONE,
        requestStateDecryptPhone: StateStatus.NONE,
        requestStateGetBeneficiary: StateStatus.NONE
      );
    } else if(event is GetBeneficiaryEvent){
      yield WalletState(
        requestStateGetBeneficiary: StateStatus.LOADING
      );

      try{
        final data = await walletService.getBeneficiaryName(event.telephone);

        if(data['name'] != null){

          yield WalletState(
            requestStateGetBeneficiary: StateStatus.LOADED,
            beneficiaryRes: data
          );
        } else {

          yield WalletState(
            requestStateGetBeneficiary: StateStatus.ERROR,
            errorMessage: "Erreur de chargement de nom"
          );
        }

      } catch(e){
        yield WalletState(
            requestStateGetBeneficiary: StateStatus.ERROR,
            errorMessage: "Erreur de chargement de nom"
        );
      }
    } else if(event is GetQRcodeDataEvent){
        yield WalletState(requestStateGetQRcode: StateStatus.LOADING);

        try{

          final data = await walletService.getQrCodeData(amount: event.amount, motif: event.motif);

          if(data['success'] == true){

            yield WalletState(requestStateGetQRcode: StateStatus.LOADED,
              qrCodeData: data['qrCode']
            );

          } else{
            yield WalletState(requestStateGetQRcode: StateStatus.ERROR,
                errorMessage: "Erreur de génération de code"
            );
          }

        }catch(e){
          yield WalletState(requestStateGetQRcode: StateStatus.ERROR,
              errorMessage: "Erreur de génération de code"
          );
        }
    }
  }
}
