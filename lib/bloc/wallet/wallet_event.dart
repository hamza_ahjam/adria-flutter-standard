abstract class WalletEvent<T> {
  T payload;
  WalletEvent({this.payload});
}

class SaveWalletPaiementAchatEvent extends WalletEvent {
  var montant,
      motif,
      telephone,
      transactionIndicator,
      intituleBeneficiaire;

  SaveWalletPaiementAchatEvent({
        this.montant,
        this.motif,
        this.telephone,
        this.transactionIndicator,
        this.intituleBeneficiaire
  }) : super();
}

class SignWalletPaiementAchatEvent extends WalletEvent {
  String password;
  SignWalletPaiementAchatEvent({this.password}) : super();
}

class SendOtpWalletPaiementAchatEvent extends WalletEvent {
  SendOtpWalletPaiementAchatEvent() : super();
}

class AbandonnerWalletPaiementAchatEvent extends WalletEvent {
  AbandonnerWalletPaiementAchatEvent() : super();
}

class ScanQREvent extends WalletEvent {
  String qr;
  String scanType;
  ScanQREvent({this.qr, this.scanType}) : super();
}

class DecryptPhoneEvent extends WalletEvent {
  String strToDecrypt;
  DecryptPhoneEvent({this.strToDecrypt}) : super();
}

class EntrerEnRelationOfflineEvent extends WalletEvent {
  bool isLogin;
  EntrerEnRelationOfflineEvent({this.isLogin = false}) : super();
}

class LoginDPEvent extends WalletEvent {
  String id;
  String psd;
  LoginDPEvent({this.id, this.psd}) : super();
}

class ResetWalletEvent extends WalletEvent {
  ResetWalletEvent() : super();
}

class GetBeneficiaryEvent extends WalletEvent{
  String telephone;
  GetBeneficiaryEvent({this.telephone}) : super();
}

class GetQRcodeDataEvent extends WalletEvent{
  String amount;
  String motif;
  GetQRcodeDataEvent({this.amount, this.motif}) : super();
}