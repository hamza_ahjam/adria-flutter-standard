import 'package:LBA/models/compte.model.dart';

abstract class NomCompteEvent<T> {
  T payload;
  NomCompteEvent({this.payload});
}

class ChangeCurrentAccountEvent extends NomCompteEvent {
  Compte compte;
  ChangeCurrentAccountEvent({this.compte}) : super();
}

class ResetCurrentAccountEvent extends NomCompteEvent {
  ResetCurrentAccountEvent() : super();
}
