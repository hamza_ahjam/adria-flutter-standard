import 'package:LBA/bloc/nom-compte/nom-compte.event.dart';
import 'package:LBA/bloc/nom-compte/nom-compte.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:bloc/bloc.dart';

class NomCompteBloc extends Bloc<NomCompteEvent, NomCompteStateBloc> {
  NomCompteBloc(
    NomCompteStateBloc initialState,
  ) : super(initialState);
  @override
  Stream<NomCompteStateBloc> mapEventToState(NomCompteEvent event) async* {
    if (event is ChangeCurrentAccountEvent) {
      yield NomCompteStateBloc(requestState: StateStatus.LOADING);
      try {
        NomCompteStateBloc compteStateBloc = NomCompteStateBloc(
            currentCompte: event.compte,
            requestState: StateStatus.LOADED,
            currentAction: event);

        yield compteStateBloc;
      } catch (e) {
        yield NomCompteStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetCurrentAccountEvent) {
      yield NomCompteStateBloc(requestState: StateStatus.NONE);
    }
  }
}
