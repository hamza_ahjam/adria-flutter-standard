import 'package:LBA/bloc/nom-compte/nom-compte.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/compte.model.dart';

class NomCompteStateBloc {
  StateStatus requestState;

  String errorMessage;
  NomCompteEvent currentAction;

  Compte currentCompte;

  NomCompteStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.currentCompte,
  });

  NomCompteStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.currentCompte = Compte();
  }
}
