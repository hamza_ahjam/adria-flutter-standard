import 'package:LBA/bloc/token/token.state.dart';

abstract class ContratEvent<T> {
  T payload;
  ContratEvent({this.payload});
}

class LoadContrat extends ContratEvent {
  TokenStateBloc tokenStateBloc;
  LoadContrat({this.tokenStateBloc}) : super();
}

class ResetContratEvent extends ContratEvent {
  ResetContratEvent() : super();
}
