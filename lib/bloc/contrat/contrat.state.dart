import 'package:LBA/bloc/contrat/contrat.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/contrat.model.dart';

class ContratStateBloc {
  StateStatus requestState;
  String errorMessage;
  ContratEvent currentAction;
  Contrat contrat;

  ContratStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.contrat,
  });

  ContratStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.contrat = null;
  }
}
