import 'package:LBA/bloc/contrat/contrat.event.dart';
import 'package:LBA/bloc/contrat/contrat.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class ContratBloc extends Bloc<ContratEvent, ContratStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  ContratBloc(ContratStateBloc initialState) : super(initialState);
  @override
  Stream<ContratStateBloc> mapEventToState(ContratEvent event) async* {
    if (event is LoadContrat) {
      yield ContratStateBloc(
          requestState: StateStatus.LOADING, contrat: state.contrat);
      try {
        final data = await compteService.getContrat(event.tokenStateBloc);

        ContratStateBloc contratStateBloc = ContratStateBloc(
            contrat: data,
            requestState: StateStatus.LOADED,
            currentAction: event);
        yield contratStateBloc;
      } catch (e) {
        yield ContratStateBloc(
            errorMessage: e.message.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ResetContratEvent) {
      yield ContratStateBloc(requestState: StateStatus.NONE);
    }
  }
}
