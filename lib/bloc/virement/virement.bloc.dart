import 'package:LBA/bloc/virement/virement.event.dart';
import 'package:LBA/bloc/virement/virement.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/periodicite.mock.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/models/periodicites.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/virement.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class VirementBloc extends Bloc<VirementEvent, VirementStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  VirementService get viremenrService => GetIt.I<VirementService>();

  VirementBloc(
    VirementStateBloc initialState,
  ) : super(initialState);

  @override
  Stream<VirementStateBloc> mapEventToState(
    VirementEvent event,
  ) async* {
    if (event is SaveVirementBeneficiaireEvent) {
      yield VirementStateBloc(
          periodicites: state.periodicites,
          requestStateSave: StateStatus.LOADING,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateSave: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.saveVirementServie(
            compteService.tokenState,
            compteService.currentCompte.identifiantInterne,
            event.montant,
            event.motif,
            event.dateVir,
            event.dateFin,
            event.benifNom,
            event.intituleCompte,
            event.numCompteCrediter,
            compteService.tokenState.getUserDetails()["username"],
            event.nomCre,
            event.typeCompteCredite,
          );

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSave: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                currentAction: event,
                requestState: state.requestState,
                periodicites: state.periodicites,
                requestStateSave: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          // print("from bloc data *** $data");
        }
      } catch (e) {
        // print("bloc err $e");
        yield VirementStateBloc(
            errorMessage: e.toString(),
            periodicites: state.periodicites,
            requestState: state.requestState,
            requestStateSave: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SaveVirementCompteEvent) {
      yield VirementStateBloc(
          requestStateCmptSave: StateStatus.LOADING,
          periodicites: state.periodicites,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateCmptSave: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.saveVirementCompteServie(
              compteService.tokenState,
              event.selectedAccountIntitule,
              event.montant,
              event.motif,
              event.dateVirement,
              event.dateFinVirement,
              event.benif,
              event.selectedAccountIntitule,
              event.benefIdentifiant,
              compteService.tokenState.getUserDetails()["username"],
              event.benifIntitule);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateCmptSave: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateCmptSave: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          // print("data from bloc *** $data");
        }
      } catch (e) {
        // print("bloc err $e");
        yield VirementStateBloc(
            periodicites: state.periodicites,
            requestState: state.requestState,
            errorMessage: e.toString(),
            requestStateCmptSave: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SigneVirementBeneficiaireEvent) {
      yield VirementStateBloc(
          // requestStateCmptSave: StateStatus.LOADED,
          requestState: state.requestState,
          periodicites: state.periodicites,
          res: state.res,
          requestStateSigne: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateSigne: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.signeVirBeneficiaire(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.password);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSigne: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSigne: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield VirementStateBloc(
            errorMessage: e.toString(),
            periodicites: state.periodicites,
            requestState: state.requestState,
            requestStateSigne: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SigneVirementCompteEvent) {
      yield VirementStateBloc(
          // requestStateCmptSave: StateStatus.LOADED,
          periodicites: state.periodicites,
          requestState: state.requestState,
          res: state.res,
          requestStateCmptSigne: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateCmptSigne: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.signeVirCompte(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.password);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateCmptSigne: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateCmptSigne: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield VirementStateBloc(
            periodicites: state.periodicites,
            requestState: state.requestState,
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is AbandonnerVirComptePhaseSignature) {
      yield VirementStateBloc(
          periodicites: state.periodicites,
          requestState: state.requestState,
          res: state.res,
          requestStateAbon: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateAbon: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.abandonnerVirComptePhaseSignature(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            state.res["id"],
            state.res["taskId"],
          );

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateAbon: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateAbon: StateStatus.ERROR,
                errorMessageAbon: data["message"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield VirementStateBloc(
            errorMessageAbon: e.toString(),
            requestState: state.requestState,
            periodicites: state.periodicites,
            requestStateAbon: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is AbandonnerVirBenefPhaseSignature) {
      yield VirementStateBloc(
          requestState: state.requestState,
          periodicites: state.periodicites,
          res: state.res,
          requestStateAbon: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              periodicites: state.periodicites,
              requestState: state.requestState,
              currentAction: event,
              requestStateAbon: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.abandonnerVirBenefPhaseSignature(
            compteService.tokenState,
            compteService.tokenState.getUserDetails(),
            state.res["id"],
            state.res["taskId"],
          );

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateAbon: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateAbon: StateStatus.ERROR,
                errorMessageAbon: data["message"]);
            yield virementStateBloc;
          }
        }
      } catch (e) {
        yield VirementStateBloc(
            errorMessageAbon: e.toString(),
            periodicites: state.periodicites,
            requestState: state.requestState,
            requestStateAbon: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is GetPeriodicite) {
      yield VirementStateBloc(requestState: StateStatus.LOADING);
      try {
        if (modeDemo) {
          final data = Periodicites.fromJson(periodiciteMock);

          VirementStateBloc virementStateBloc = VirementStateBloc(
              periodicites: data,
              currentAction: event,
              requestState: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data =
              await viremenrService.getPeriodicites(compteService.tokenState);

          VirementStateBloc virementStateBloc = VirementStateBloc(
              periodicites: data,
              currentAction: event,
              requestState: StateStatus.LOADED);
          yield virementStateBloc;
        }
      } catch (e) {
        yield VirementStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SaveVirementPermBeneficiaireEvent) {
      yield VirementStateBloc(
          periodicites: state.periodicites,
          requestStateSave: StateStatus.LOADING,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              requestState: state.requestState,
              periodicites: state.periodicites,
              res: data,
              currentAction: event,
              requestStateSave: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.saveVirementBenefPermServie(
              compteService.tokenState,
              event.numeroCompt,
              event.montant,
              event.motif,
              event.dateVirement,
              event.dateFinVirement,
              event.benifIntitule,
              event.selectedAccountintitule,
              event.benifNumeroCompte,
              event.numeroCompt,
              event.period,
              event.typeBenf,
              compteService.tokenState.getUserDetails()["username"]);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                requestState: state.requestState,
                periodicites: state.periodicites,
                res: data,
                currentAction: event,
                requestStateSave: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSave: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          // print("bloc vir *** $data");
        }
      } catch (e) {
        yield VirementStateBloc(
            periodicites: state.periodicites,
            errorMessage: e.toString(),
            requestState: state.requestState,
            requestStateSave: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SigneVirementPermBeneficiaireEvent) {
      yield VirementStateBloc(
          // requestStateCmptSave: StateStatus.LOADED,
          res: state.res,
          periodicites: state.periodicites,
          requestState: state.requestState,
          requestStateSigne: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              requestState: state.requestState,
              periodicites: state.periodicites,
              currentAction: event,
              requestStateSigne: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.signeVirBeneficiairePerm(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.password);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                requestState: state.requestState,
                periodicites: state.periodicites,
                currentAction: event,
                requestStateSigne: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSigne: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          print("data from bloc ** ${state.res}");
        }
      } catch (e) {
        yield VirementStateBloc(
            periodicites: state.periodicites,
            requestState: state.requestState,
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SaveVirementPermCompteEvent) {
      yield VirementStateBloc(
          periodicites: state.periodicites,
          requestStateSave: StateStatus.LOADING,
          requestState: state.requestState);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = saveMock["Map"];
          VirementStateBloc virementStateBloc = VirementStateBloc(
              requestState: state.requestState,
              periodicites: state.periodicites,
              res: data,
              currentAction: event,
              requestStateSave: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.saveVirementComptePermServie(
              compteService.tokenState,
              event.numeroCompt,
              event.montant,
              event.motif,
              event.dateVirement,
              event.dateFinVirement,
              event.benifIntitule,
              event.selectedAccountintitule,
              event.benifNumeroCompte,
              event.numeroCompt,
              event.period,
              compteService.tokenState.getUserDetails()["username"]);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                requestState: state.requestState,
                periodicites: state.periodicites,
                res: data,
                currentAction: event,
                requestStateSave: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSave: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          print("bloc vir *** $data");
        }
      } catch (e) {
        yield VirementStateBloc(
            periodicites: state.periodicites,
            errorMessage: e.toString(),
            requestState: state.requestState,
            requestStateSave: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is SigneVirementPermCompteEvent) {
      yield VirementStateBloc(
          // requestStateCmptSave: StateStatus.LOADED,
          res: state.res,
          periodicites: state.periodicites,
          requestState: state.requestState,
          requestStateSigne: StateStatus.LOADING);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = signeMock["Map"];

          VirementStateBloc virementStateBloc = VirementStateBloc(
              res: data,
              requestState: state.requestState,
              periodicites: state.periodicites,
              currentAction: event,
              requestStateSigne: StateStatus.LOADED);
          yield virementStateBloc;
        } else {
          final data = await viremenrService.signeVirComptePerm(
              compteService.tokenState,
              compteService.tokenState.getUserDetails(),
              state.res["id"],
              state.res["taskId"],
              event.password);

          if (data["success"]) {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                requestState: state.requestState,
                periodicites: state.periodicites,
                currentAction: event,
                requestStateSigne: StateStatus.LOADED);
            yield virementStateBloc;
          } else {
            VirementStateBloc virementStateBloc = VirementStateBloc(
                res: data,
                periodicites: state.periodicites,
                requestState: state.requestState,
                currentAction: event,
                requestStateSigne: StateStatus.ERROR,
                errorMessage: data["message"]);
            yield virementStateBloc;
          }
          print("data from bloc ** ${state.res}");
        }
      } catch (e) {
        yield VirementStateBloc(
            periodicites: state.periodicites,
            requestState: state.requestState,
            errorMessage: e.toString(),
            requestStateSigne: StateStatus.ERROR,
            currentAction: event);
      }
    }
  }
}
