abstract class VirementEvent<T> {
  T payload;
  VirementEvent({this.payload});
}

class SaveVirementBeneficiaireEvent extends VirementEvent {
  var identifiantInterne,
      montant,
      motif,
      dateVir,
      dateFin,
      benifNom,
      intituleCompte,
      numCompteCrediter,
      typeCompteCredite,
      nomCre;
  SaveVirementBeneficiaireEvent(
      {this.benifNom,
      this.dateFin,
      this.dateVir,
      this.identifiantInterne,
      this.intituleCompte,
      this.montant,
      this.motif,
      this.nomCre,
      this.typeCompteCredite,
      this.numCompteCrediter})
      : super();
}

class SaveVirementCompteEvent extends VirementEvent {
  var montant,
      motif,
      selectedAccount,
      dateVirement,
      dateFinVirement,
      benif,
      selectedAccountIntitule,
      benefIdentifiant,
      benifIntitule;
  SaveVirementCompteEvent(
      {this.montant,
      this.motif,
      this.benif,
      this.benefIdentifiant,
      this.dateFinVirement,
      this.dateVirement,
      this.selectedAccount,
      this.selectedAccountIntitule,
      this.benifIntitule})
      : super();
}

class SigneVirementBeneficiaireEvent extends VirementEvent {
  String password;
  SigneVirementBeneficiaireEvent({this.password}) : super();
}

class SigneVirementCompteEvent extends VirementEvent {
  String password;
  SigneVirementCompteEvent({this.password}) : super();
}

class AbandonnerVirComptePhaseSignature extends VirementEvent {
  AbandonnerVirComptePhaseSignature() : super();
}

class AbandonnerVirBenefPhaseSignature extends VirementEvent {
  AbandonnerVirBenefPhaseSignature() : super();
}

class GetPeriodicite extends VirementEvent {
  GetPeriodicite() : super();
}

class SaveVirementPermBeneficiaireEvent extends VirementEvent {
  var montant,
      motif,
      numeroCompt,
      dateVirement,
      dateFinVirement,
      benifIntitule,
      selectedAccountintitule,
      benifNumeroCompte,
      typeBenf,
      period;
  SaveVirementPermBeneficiaireEvent(
      {this.benifIntitule,
      this.benifNumeroCompte,
      this.numeroCompt,
      this.dateFinVirement,
      this.dateVirement,
      this.montant,
      this.motif,
      this.period,
      this.typeBenf,
      this.selectedAccountintitule})
      : super();
}

class SigneVirementPermBeneficiaireEvent extends VirementEvent {
  String password;
  SigneVirementPermBeneficiaireEvent({this.password}) : super();
}

class SaveVirementPermCompteEvent extends VirementEvent {
  var montant,
      motif,
      numeroCompt,
      dateVirement,
      dateFinVirement,
      benifIntitule,
      selectedAccountintitule,
      benifNumeroCompte,
      period;
  SaveVirementPermCompteEvent(
      {this.benifIntitule,
      this.benifNumeroCompte,
      this.numeroCompt,
      this.dateFinVirement,
      this.dateVirement,
      this.montant,
      this.motif,
      this.period,
      this.selectedAccountintitule})
      : super();
}

class SigneVirementPermCompteEvent extends VirementEvent {
  String password;
  SigneVirementPermCompteEvent({this.password}) : super();
}
