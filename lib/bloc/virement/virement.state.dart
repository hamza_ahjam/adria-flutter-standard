import 'package:LBA/bloc/virement/virement.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/periodicites.model.dart';

class VirementStateBloc {
  StateStatus requestState;
  StateStatus requestStateSave;
  StateStatus requestStateCmptSave;
  StateStatus requestStateSigne;
  StateStatus requestStateCmptSigne;
  StateStatus requestStateAbon;
  String errorMessage;
  String errorMessageAbon;
  VirementEvent currentAction;
  var res;
  Periodicites periodicites;

  VirementStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStateSave,
      this.requestStateCmptSave,
      this.requestStateCmptSigne,
      this.requestStateSigne,
      this.requestStateAbon,
      this.errorMessageAbon,
      this.periodicites,
      this.res});

  VirementStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateSave = StateStatus.NONE;
    this.requestStateSigne = StateStatus.NONE;
    this.requestStateCmptSave = StateStatus.NONE;
    this.requestStateCmptSigne = StateStatus.NONE;
    this.requestStateAbon = StateStatus.NONE;
    this.errorMessage = "";
    this.errorMessageAbon = "";
    this.periodicites = Periodicites();
  }
}
