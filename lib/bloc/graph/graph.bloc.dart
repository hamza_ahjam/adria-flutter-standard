import 'package:LBA/bloc/graph/graph.event.dart';
import 'package:LBA/bloc/graph/graph.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/graph.dart';
import 'package:LBA/models/graphique.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class GraphBloc extends Bloc<GraphEvent, GraphStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  GraphBloc(GraphStateBloc initialState) : super(initialState);

  @override
  Stream<GraphStateBloc> mapEventToState(GraphEvent event) async* {
    if (event is LoadGraphEvent) {
      yield GraphStateBloc(
          requestState: StateStatus.LOADING, graph: state.graph);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = Graphique.fromJson(graphMock);
          GraphStateBloc graphStateBloc = GraphStateBloc(
              graph: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield graphStateBloc;
        } else {
          final data = await compteService.getDataGrpahique();
          GraphStateBloc graphStateBloc = GraphStateBloc(
              graph: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield graphStateBloc;
        }
      } catch (e) {
        yield GraphStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitGraphEvent) {
      yield GraphStateBloc(requestState: StateStatus.NONE);
    }
  }
}
