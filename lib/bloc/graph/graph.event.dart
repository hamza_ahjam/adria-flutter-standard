

abstract class GraphEvent<T> {
  T payload;
  GraphEvent({this.payload});
}

class LoadGraphEvent extends GraphEvent {
  LoadGraphEvent() : super();
}

class InitGraphEvent extends GraphEvent {
  InitGraphEvent() : super();
}
