import 'package:LBA/bloc/graph/graph.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/graphique.model.dart';

class GraphStateBloc {
  StateStatus requestState;
  String errorMessage;
  Graphique graph;
  GraphEvent currentAction;

  GraphStateBloc(
      {this.requestState, this.errorMessage, this.currentAction, this.graph});

  GraphStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.graph = Graphique();
  }
}
