import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/bloc/dat/dat.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/listDAT.dart';
import 'package:LBA/models/dat.model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class DatBloc extends Bloc<DatEvent, DatStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  DatBloc(DatStateBloc initialState) : super(initialState);
  @override
  Stream<DatStateBloc> mapEventToState(DatEvent event) async* {
    if (event is LoadDatEvent) {
      yield DatStateBloc(
          requestState: StateStatus.LOADING, listDat: state.listDat);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = Dat.fromJson(listDat);

          DatStateBloc contratStateBloc = DatStateBloc(
              listDat: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        } else {
          final data = await compteService.getListDat(
              compteService.currentCompteChequier.identifiantInterne,
              event.page);

          DatStateBloc contratStateBloc = DatStateBloc(
              listDat: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        }
      } catch (e) {
        yield DatStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is ChangeAccountLoadDatEvent) {
      yield DatStateBloc(
          requestState: StateStatus.LOADING, listDat: state.listDat);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          final data = Dat.fromJson(listDat);

          compteService.currentCompteChequier = event.compte;

          DatStateBloc contratStateBloc = DatStateBloc(
              listDat: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        } else {
          compteService.currentCompteChequier = event.compte;
          final data = await compteService.getListDat(
              compteService.currentCompteChequier.identifiantInterne, 1);

          DatStateBloc contratStateBloc = DatStateBloc(
              listDat: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield contratStateBloc;
        }
      } catch (e) {
        yield DatStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitDatEvent) {
      yield DatStateBloc(requestState: StateStatus.NONE);
    }
  }
}
