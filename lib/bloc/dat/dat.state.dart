import 'package:LBA/bloc/dat/dat.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/dat.model.dart';

class DatStateBloc {
  StateStatus requestState;
  String errorMessage;
  DatEvent currentAction;
  Dat listDat;

  DatStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.listDat,
  });

  DatStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.listDat = Dat();
  }
}
