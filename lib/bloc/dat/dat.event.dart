import 'package:LBA/models/compte.model.dart';

abstract class DatEvent<T> {
  T payload;
  DatEvent({this.payload});
}

class LoadDatEvent extends DatEvent {
  int page;
  LoadDatEvent({this.page}) : super();
}

class InitDatEvent extends DatEvent {
  InitDatEvent() : super();
}

class ChangeAccountLoadDatEvent extends DatEvent {
  Compte compte;
  ChangeAccountLoadDatEvent({this.compte}) : super();
}
