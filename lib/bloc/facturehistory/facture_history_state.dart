import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/facture_history.dart';

import 'facture_history_event.dart';

class FactureHistoryStateBloc {
  StateStatus requestState;
  String errorMessage;
  FactureHistoryEvent currentAction;
  FacturesHistory listFactures;

  FactureHistoryStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.listFactures,
  });

  FactureHistoryStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
    this.listFactures = FacturesHistory();
  }
}