import 'package:LBA/bloc/facturehistory/facture_history_event.dart';
import 'package:LBA/bloc/facturehistory/facture_history_state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/historique_facture.mock.dart';
import 'package:LBA/models/facture_history.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class FactureHistoryBloc
    extends Bloc<FactureHistoryEvent, FactureHistoryStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  FactureHistoryBloc(FactureHistoryStateBloc initialState)
      : super(initialState);
  @override
  Stream<FactureHistoryStateBloc> mapEventToState(
      FactureHistoryEvent event) async* {
    if (event is LoadFactureHistoryEvent) {
      yield FactureHistoryStateBloc(
          requestState: StateStatus.LOADING, listFactures: state.listFactures);
      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          FacturesHistory data =
              FacturesHistory.fromJson(historiqueFactureMock);

          FactureHistoryStateBloc facturesStateBloc = FactureHistoryStateBloc(
              listFactures: data,
              requestState: StateStatus.LOADED,
              currentAction: event);
          yield facturesStateBloc;
        } else {
          FacturesHistory data = await compteService.getFacturesHistory();

          if (data.map.success) {
            FactureHistoryStateBloc facturesStateBloc = FactureHistoryStateBloc(
                listFactures: data,
                requestState: StateStatus.LOADED,
                currentAction: event);
            yield facturesStateBloc;
          } else {
            yield FactureHistoryStateBloc(
                errorMessage: data.map.codeErreur,
                requestState: StateStatus.ERROR,
                currentAction: event);
          }
        }
      } catch (e) {
        yield FactureHistoryStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    } else if (event is InitFactureHistoryEvent) {
      yield FactureHistoryStateBloc(requestState: StateStatus.NONE);
    }
  }
}
