abstract class FactureHistoryEvent<T> {
  T payload;
  FactureHistoryEvent({this.payload});
}

class LoadFactureHistoryEvent extends FactureHistoryEvent {
  LoadFactureHistoryEvent() : super();
}

class InitFactureHistoryEvent extends FactureHistoryEvent {
  InitFactureHistoryEvent() : super();
}