import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/bloc/facture/facture.state.dart';
import 'package:LBA/constants.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/mocks/create_form_facture.mock.dart';
import 'package:LBA/mocks/get_form_facture.mock.dart';
import 'package:LBA/mocks/list_categorie_creanciers.mock.dart';
import 'package:LBA/mocks/list_creance.mock.dart';
import 'package:LBA/mocks/list_creancier.mock.dart';
import 'package:LBA/mocks/list_favoris_facture.mock.dart';
import 'package:LBA/mocks/save_mock.dart';
import 'package:LBA/mocks/signe_mock.dart';
import 'package:LBA/models/categorieCreancier.model.dart';
import 'package:LBA/models/creanceDetails.model.dart';
import 'package:LBA/models/creanceForm.model.dart';
import 'package:LBA/models/creancier.model.dart';
import 'package:LBA/models/favoris_model.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:LBA/services/facture_service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class FactureBloc extends Bloc<FactureEvent, FactureStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();
  FactureService get factureService => GetIt.I<FactureService>();

  FactureBloc(FactureStateBloc initialState) : super(initialState);
  @override
  Stream<FactureStateBloc> mapEventToState(FactureEvent event) async* {
    if (event is LoadFactureEvent) {
      yield FactureStateBloc(
          requestState: StateStatus.LOADING,
          requestStateFav: state.requestStateFav,
          favoris: state.favoris);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          CategorieCreancier categorie =
              CategorieCreancier.fromJson(listCategoryCreancierMock);

          Creanciers creancier = Creanciers.fromJson(listCreancierMock);

          Map<String, List<ListElement>> facture =
              Map<String, List<ListElement>>();

          categorie.map.categorieCreanciersList.forEach((category) {
            creancier.map.list.forEach((c) {
              if (category.code == c.categorie) {
                if (facture[category.libelle] == null) {
                  facture[category.libelle] = [];
                }
                facture[category.libelle].add(c);
              }
            });
          });

          yield FactureStateBloc(
              facture: facture,
              requestState: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              currentAction: event);
        } else {
          CategorieCreancier categorie =
              await compteService.getCategorieCreanciers();

          Creanciers creancier = await compteService.listCreanciers();

          Map<String, List<ListElement>> facture =
              Map<String, List<ListElement>>();

          categorie.map.categorieCreanciersList.forEach((category) {
            creancier.map.list.forEach((c) {
              if (category.code == c.categorie) {
                if (facture[category.libelle] == null) {
                  facture[category.libelle] = [];
                }
                facture[category.libelle].add(c);
              }
            });
          });

          yield FactureStateBloc(
              facture: facture,
              requestState: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestState: StateStatus.ERROR,
            requestStateFav: state.requestStateFav,
            favoris: state.favoris,
            currentAction: event,
            errorMessage: e.toString());
      }
    } else if (event is LoadCreanceEvent) {
      yield FactureStateBloc(
        requestStateDt: StateStatus.LOADING,
        requestState: state.requestState,
        facture: state.facture,
      );

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          CreanceDetails data = CreanceDetails.fromJson(listCreanceMock);

          yield FactureStateBloc(
              creance: data,
              requestStateDt: StateStatus.LOADED,
              requestState: state.requestState,
              facture: state.facture,
              currentAction: event);
        } else {
          CreanceDetails data =
              await compteService.listCreancierChamp(event.code);

          if (data.map.success) {
            yield FactureStateBloc(
                creance: data,
                requestStateDt: StateStatus.LOADED,
                requestState: state.requestState,
                facture: state.facture,
                currentAction: event);
          } else {
            yield FactureStateBloc(
                requestStateDt: StateStatus.ERROR,
                requestState: state.requestState,
                facture: state.facture,
                errorMessage: "une erreur technique est survenu",
                currentAction: event);
          }
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateDt: StateStatus.ERROR,
            requestState: state.requestState,
            facture: state.facture,
            currentAction: event,
            errorMessage: e.toString());
      }
    } else if (event is LoadCreanceFormEvent) {
      yield FactureStateBloc(
          requestStateForm: StateStatus.LOADING,
          requestStateFav: state.requestStateFav,
          favoris: state.favoris,
          requestState: state.requestState,
          facture: state.facture,
          creance: state.creance,
          requestStateDt: state.requestStateDt);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));
          CreancierForm data = CreancierForm.fromJson(getFormFactureMock);

          yield FactureStateBloc(
              creancierForm: data,
              requestStateForm: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              requestState: state.requestState,
              facture: state.facture,
              creance: state.creance,
              requestStateDt: state.requestStateDt,
              currentAction: event);
        } else {
          CreancierForm data = await compteService.listCreancierForm(
              event.codeCreancier, event.codeCreance);

          yield FactureStateBloc(
              creancierForm: data,
              requestStateForm: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              requestState: state.requestState,
              facture: state.facture,
              creance: state.creance,
              requestStateDt: state.requestStateDt,
              currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateForm: StateStatus.ERROR,
            requestStateFav: state.requestStateFav,
            favoris: state.favoris,
            requestState: state.requestState,
            facture: state.facture,
            currentAction: event,
            creance: state.creance,
            requestStateDt: state.requestStateDt,
            errorMessage: e.toString());
      }
    } else if (event is CreanceFormCreateEvent) {
      yield FactureStateBloc(
          requestStateFormCreate: StateStatus.LOADING,
          requestStateFav: state.requestStateFav,
          favoris: state.favoris,
          requestStateForm: state.requestStateForm,
          creancierForm: state.creancierForm,
          requestState: state.requestState,
          facture: state.facture,
          creance: state.creance,
          requestStateDt: state.requestStateDt);

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = createFormFactureMock;

          yield FactureStateBloc(
              res: data,
              requestStateFormCreate: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              requestStateForm: state.requestStateForm,
              creancierForm: state.creancierForm,
              requestState: state.requestState,
              facture: state.facture,
              creance: state.creance,
              requestStateDt: state.requestStateDt,
              currentAction: event);
        } else {
          var data = await compteService.creancierFormCreate(event.bodyObj);

          if (data["Map"]["success"])
            yield FactureStateBloc(
                res: data,
                requestStateFormCreate: StateStatus.LOADED,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
          else
            yield FactureStateBloc(
                res: data,
                requestStateFormCreate: StateStatus.ERROR,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                errorMessage: data["Map"]["StatusLabel"],
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateFormCreate: StateStatus.ERROR,
            requestStateFav: state.requestStateFav,
            favoris: state.favoris,
            requestStateForm: state.requestStateForm,
            creancierForm: state.creancierForm,
            requestState: state.requestState,
            facture: state.facture,
            currentAction: event,
            creance: state.creance,
            requestStateDt: state.requestStateDt,
            errorMessage: e.toString());
      }
    } else if (event is FormCreateSaveEvent) {
      yield FactureStateBloc(
          requestStateSave: StateStatus.LOADING,
          requestStateFav: state.requestStateFav,
          favoris: state.favoris,
          res: state.res,
          requestStateFormCreate: state.requestStateFormCreate,
          requestStateForm: state.requestStateForm,
          creancierForm: state.creancierForm,
          requestState: state.requestState,
          facture: state.facture,
          creance: state.creance,
          requestStateDt: state.requestStateDt);

      try {
        if (modeDemo) {
          var data = saveMock;
          yield FactureStateBloc(
              saveRes: data,
              requestStateSave: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              res: state.res,
              requestStateFormCreate: state.requestStateFormCreate,
              requestStateForm: state.requestStateForm,
              creancierForm: state.creancierForm,
              requestState: state.requestState,
              facture: state.facture,
              creance: state.creance,
              requestStateDt: state.requestStateDt,
              currentAction: event);
        } else {
          var data = await compteService.saveFacture(
              event.agenceCmp,
              event.codeCreance,
              event.codeCreancier,
              event.codeProduitCompte,
              event.impayesJson,
              event.logo,
              event.referencePartenaire,
              event.type,
              event.intituleCompte,
              event.libelleCreance,
              event.libelleCreancier,
              event.montantTotalTTC,
              event.numeroCompte,
              event.paramsGlobal,
              event.paiementTotal);

          if (data["Map"]["success"])
            yield FactureStateBloc(
                saveRes: data,
                requestStateSave: StateStatus.LOADED,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                res: state.res,
                requestStateFormCreate: state.requestStateFormCreate,
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
          else
            yield FactureStateBloc(
                saveRes: data,
                requestStateSave: StateStatus.ERROR,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                res: state.res,
                requestStateFormCreate: state.requestStateFormCreate,
                errorMessage: data["Map"]["message"],
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateSave: StateStatus.ERROR,
            requestStateFav: state.requestStateFav,
            favoris: state.favoris,
            res: state.res,
            requestStateFormCreate: state.requestStateFormCreate,
            requestStateForm: state.requestStateForm,
            creancierForm: state.creancierForm,
            requestState: state.requestState,
            facture: state.facture,
            currentAction: event,
            creance: state.creance,
            requestStateDt: state.requestStateDt,
            errorMessage: e.toString());
      }
    } else if (event is DeleteFavoriEvent) {
      yield FactureStateBloc(
          requestStateDelete: StateStatus.LOADING,
          requestStateFav: state.requestStateFav,
          favoris: state.favoris,
          res: state.res,
          requestStateFormCreate: state.requestStateFormCreate,
          requestStateForm: state.requestStateForm,
          creancierForm: state.creancierForm,
          requestState: state.requestState,
          facture: state.facture,
          creance: state.creance,
          requestStateDt: state.requestStateDt);

      try {
        if (modeDemo) {
          var data = signeMock;
          yield FactureStateBloc(
              deleteRes: data,
              requestStateDelete: StateStatus.LOADED,
              requestStateFav: state.requestStateFav,
              favoris: state.favoris,
              res: state.res,
              requestStateFormCreate: state.requestStateFormCreate,
              requestStateForm: state.requestStateForm,
              creancierForm: state.creancierForm,
              requestState: state.requestState,
              facture: state.facture,
              creance: state.creance,
              requestStateDt: state.requestStateDt,
              currentAction: event);
        } else {
          var data = await factureService.deleteFavori(
              compteService.tokenState, event.idFav);

          if (data["HashMap"]["success"])
            yield FactureStateBloc(
                deleteRes: data,
                requestStateDelete: StateStatus.LOADED,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                res: state.res,
                requestStateFormCreate: state.requestStateFormCreate,
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
          else
            yield FactureStateBloc(
                deleteRes: data,
                requestStateDelete: StateStatus.ERROR,
                requestStateFav: state.requestStateFav,
                favoris: state.favoris,
                res: state.res,
                requestStateFormCreate: state.requestStateFormCreate,
                errorMessage: data["HashMap"]["message"],
                requestStateForm: state.requestStateForm,
                creancierForm: state.creancierForm,
                requestState: state.requestState,
                facture: state.facture,
                creance: state.creance,
                requestStateDt: state.requestStateDt,
                currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateDelete: StateStatus.ERROR,
            requestStateFav: state.requestStateFav,
            favoris: state.favoris,
            res: state.res,
            requestStateFormCreate: state.requestStateFormCreate,
            requestStateForm: state.requestStateForm,
            creancierForm: state.creancierForm,
            requestState: state.requestState,
            facture: state.facture,
            currentAction: event,
            creance: state.creance,
            requestStateDt: state.requestStateDt,
            errorMessage: e.toString());
      }
    } else if (event is LoadFavorisEvent) {
      yield FactureStateBloc(
        requestStateFav: StateStatus.LOADING,
        facture: state.facture,
        requestState: state.requestState,
      );

      try {
        if (modeDemo) {
          await Future.delayed(Duration(seconds: 1));

          var data = Favoris.fromJson(listFavorisFactureMock);

          if (data.map.success)
            yield FactureStateBloc(
              requestStateFav: StateStatus.LOADED,
              favoris: data,
              facture: state.facture,
              requestState: state.requestState,
            );
        } else {
          var data = await factureService.getFavoris(compteService.tokenState);

          if (data.map.success)
            yield FactureStateBloc(
              requestStateFav: StateStatus.LOADED,
              favoris: data,
              facture: state.facture,
              requestState: state.requestState,
            );
          else
            yield FactureStateBloc(
                requestStateFav: StateStatus.ERROR,
                facture: state.facture,
                requestState: state.requestState,
                errorMessage: data.map.message,
                currentAction: event);
        }
      } catch (e) {
        yield FactureStateBloc(
            requestStateFav: StateStatus.ERROR,
            facture: state.facture,
            requestState: state.requestState,
            errorMessage: e.toString(),
            currentAction: event);
      }
    } else if (event is ResetFactureEvent) {
      yield FactureStateBloc(
        requestStateSave: StateStatus.NONE,
        requestStateDelete: StateStatus.NONE,
        requestStateFav: StateStatus.NONE,
        requestStateFormCreate: StateStatus.NONE,
        requestStateForm: StateStatus.NONE,
        requestState: StateStatus.NONE,
      );
    }
  }
}
