import 'package:LBA/bloc/facture/facture.event.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/models/creanceDetails.model.dart';
import 'package:LBA/models/creanceForm.model.dart';
import 'package:LBA/models/creancier.model.dart';
import 'package:LBA/models/favoris_model.dart';

class FactureStateBloc {
  StateStatus requestState;
  StateStatus requestStateDt;
  StateStatus requestStateForm;
  StateStatus requestStateFormCreate;
  StateStatus requestStateSave;
  StateStatus requestStateFav;
  StateStatus requestStateDelete;
  String errorMessage;
  FactureEvent currentAction;
  CreanceDetails creance;
  CreancierForm creancierForm;
  Favoris favoris;
  Map<String, List<ListElement>> facture;
  var res;
  var saveRes;
  var deleteRes;

  FactureStateBloc(
      {this.currentAction,
      this.errorMessage,
      this.requestState,
      this.requestStateDt,
      this.requestStateForm,
      this.requestStateFormCreate,
      this.requestStateFav,
      this.requestStateDelete,
      this.creancierForm,
      this.facture,
      this.creance,
      this.favoris,
      this.res,
      this.saveRes,
      this.deleteRes,
      this.requestStateSave});

  FactureStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.requestStateDt = StateStatus.NONE;
    this.requestStateForm = StateStatus.NONE;
    this.requestStateFormCreate = StateStatus.NONE;
    this.requestStateSave = StateStatus.NONE;
    this.requestStateFav = StateStatus.NONE;
    this.requestStateDelete = StateStatus.NONE;
    this.errorMessage = "";
    this.facture = Map<String, List<ListElement>>();
    this.creance = CreanceDetails();
    this.creancierForm = CreancierForm();
    this.favoris = Favoris();
  }
}
