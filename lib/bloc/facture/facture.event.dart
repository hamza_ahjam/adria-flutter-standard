abstract class FactureEvent<T> {
  T payload;
  FactureEvent({this.payload});
}

class LoadFactureEvent extends FactureEvent {
  LoadFactureEvent() : super();
}

class LoadCreanceEvent extends FactureEvent {
  String code;
  LoadCreanceEvent({this.code}) : super();
}

class LoadCreanceFormEvent extends FactureEvent {
  String codeCreancier;
  String codeCreance;
  LoadCreanceFormEvent({this.codeCreance, this.codeCreancier}) : super();
}

class CreanceFormCreateEvent extends FactureEvent {
  var bodyObj;
  CreanceFormCreateEvent({this.bodyObj}) : super();
}

class LoadFavorisEvent extends FactureEvent {
  LoadFavorisEvent() : super();
}

class FormCreateSaveEvent extends FactureEvent {
  var agenceCmp,
      codeCreance,
      codeCreancier,
      codeProduitCompte,
      impayesJson,
      logo,
      referencePartenaire,
      type,
      intituleCompte,
      libelleCreance,
      libelleCreancier,
      montantTotalTTC,
      numeroCompte,
      paiementTotal,
      paramsGlobal;
  FormCreateSaveEvent(
      {this.agenceCmp,
      this.codeCreance,
      this.codeCreancier,
      this.codeProduitCompte,
      this.impayesJson,
      this.intituleCompte,
      this.libelleCreance,
      this.libelleCreancier,
      this.logo,
      this.montantTotalTTC,
      this.numeroCompte,
      this.paramsGlobal,
      this.referencePartenaire,
      this.paiementTotal,
      this.type})
      : super();
}

class DeleteFavoriEvent extends FactureEvent {
  var idFav;

  DeleteFavoriEvent({this.idFav}) : super();
}

class ResetFactureEvent extends FactureEvent {

  ResetFactureEvent() : super();
}