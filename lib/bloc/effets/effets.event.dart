abstract class EffetsEvent<T> {
  T payload;
  EffetsEvent({this.payload});
}

class LoadEffetsEvent extends EffetsEvent {
  LoadEffetsEvent() : super();
}
