import 'package:LBA/bloc/effets/effets.event.dart';
import 'package:LBA/enum/stateStatus.dart';

class EffetsStateBloc {
  StateStatus requestState;
  String errorMessage;
  EffetsEvent currentAction;
  dynamic effets;

  EffetsStateBloc({
    this.currentAction,
    this.errorMessage,
    this.requestState,
    this.effets,
  });

  EffetsStateBloc.initialState() {
    this.requestState = StateStatus.NONE;
    this.errorMessage = "";
  }
}
