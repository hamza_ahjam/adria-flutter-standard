import 'dart:async';

import 'package:LBA/bloc/effets/effets.event.dart';
import 'package:LBA/bloc/effets/effets.state.dart';
import 'package:LBA/enum/stateStatus.dart';
import 'package:LBA/services/Compte.service.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class EffetsBloc extends Bloc<EffetsEvent, EffetsStateBloc> {
  CompteService get compteService => GetIt.I<CompteService>();

  EffetsBloc(EffetsStateBloc initialState) : super(initialState);
  @override
  Stream<EffetsStateBloc> mapEventToState(EffetsEvent event) async* {
    if (event is LoadEffetsEvent) {
      yield EffetsStateBloc(
        requestState: StateStatus.LOADING,
      );
      try {
        await Future.delayed(Duration(seconds: 3));

        yield EffetsStateBloc(
            requestState: StateStatus.LOADED, currentAction: event);
      } catch (e) {
        yield EffetsStateBloc(
            errorMessage: e.toString(),
            requestState: StateStatus.ERROR,
            currentAction: event);
      }
    }
  }
}
