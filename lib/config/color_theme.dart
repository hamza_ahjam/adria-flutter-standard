import 'package:flutter/cupertino.dart';

class ColorTheme {
  Color appBarColor;
  Color validationButtonColor;
  Color cancelButtonTextColor;
  Color otherButtonColor;
  Color intituleCmpColor;
  Color numCmpColor;
  Color soldeCmpColor;
  Color graphbodyColor;
  Color graphTitleColor;
  Color graphGradienBodyColor;
  Color mouvementFilterColor;
  Color mouvementtitleColor;
  Color iconsColor;
  Color infoBgColor;
  Color bgKeyboard;
  Color mouvementBodyColor;
  Color bgTitleContainer;
  Color bgMensualiteColor;
  Color colorTab;
  Color infoColor;
  Color inputBackground;
  Color userInfo;
  Color pictoColor;
  Color alertCodeBgColor;

  ColorTheme(
      {this.appBarColor,
        this.cancelButtonTextColor,
        this.otherButtonColor,
        this.validationButtonColor,
        this.intituleCmpColor,
        this.numCmpColor,
        this.graphbodyColor,
        this.soldeCmpColor,
        this.graphTitleColor,
        this.graphGradienBodyColor,
        this.mouvementFilterColor,
        this.iconsColor,
        this.infoBgColor,
        this.bgKeyboard,
        this.mouvementBodyColor,
        this.bgTitleContainer,
        this.bgMensualiteColor,
        this.colorTab,
        this.infoColor,
        this.mouvementtitleColor,
        this.inputBackground,
        this.userInfo,
        this.pictoColor,
        this.alertCodeBgColor
      });
}
